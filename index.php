<?php

$stimer = microtime();
$memstart = memory_get_usage();

define('VNP_APPLICATION', true);
define('APPLICATION_NAME', 'clients');
define('APPLICATION_DIR', 'clients');
define('ENVIRONMENT', 'develop'); // develop, test, publish

define('BASE_PATH', dirname(realpath(__FILE__)) . DIRECTORY_SEPARATOR);
define('APPLICATION_PATH', BASE_PATH . APPLICATION_DIR . DIRECTORY_SEPARATOR);
define('ADMIN_SECTION', false);
#define('GLOBAL_DATA_DIR', '/data/');
#define('GLOBAL_BASE_URL', '/');
define('INIT_DIR', '');

//echo INIT_DIR;

require BASE_PATH . 'base.php';
Boot::ControllerGroup('frontend');
Boot::Start();
Boot::Run();

$time	= microtime() - $stimer;
$mem	= memory_get_usage() - $memstart;
VNP_AdminLogPanel($time, $mem);

?>