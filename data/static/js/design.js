var themeBlocksOrder = new Object();
var deletedBlocks = new Array();
function reGenerateBlockMarkup(docObj) {
    $.each($('.theme-block-mark-up', docObj), function(i,e) {
        var blockObj = $(this);
        blockObj.prepend('<ul class="block-functions" data-block-id="' + blockObj.attr('data-block-id') + '">\
            <li action="remove">Remove</li>\
            <li action="edit">Edit</li>\
            <li action="drag">Drag</li>\
        </ul>');
    });
}
function reOrderBlocks() {
    $('.theme-area-mark-up').each(function(index, element) {
        var _area = $(this).attr('data-area');
        themeBlocksOrder[_area] = new Array();
        $('.theme-block-mark-up', $(this)).each(function(index, element) {
            $(this).attr('data-block-order', index);
            themeBlocksOrder[_area][index] = $(this).attr('data-block-id');
        });
    });
    VNP.Ajax({
        type: 'POST',
        url: 'nTheme/reOrderBlocks',
        data: {blocks_order: JSON.stringify(themeBlocksOrder)},
        //processData: false,
        success: function(result) {
            //console.log(result);
            VNP.Loader.hide();
            if(result != 'OK') alert('Error, can\'t save blocks order information hihi!');
        },
        error: function(requestObject, error, errorThrown) {
            //console.log(error);
            VNP.Loader.hide();
            alert('Error, can\'t save blocks order information!');
        }
    }, 'text');
}
$(document).ready(function() {
    $(document).on('click', '[role="Close_SugarBox"]', function(e) {
        e.preventDefault();
        $('.VNP_Theme_TempBlock').remove();
        VNP.SugarBox.Close();
    });
    reGenerateBlockMarkup(window.document);

    /**** drag block action ****/
    $(function() {
        $("#top-design-blocks .list-blocks li").draggable({
            connectToSortable: '.theme-area-mark-up',
            helper: "clone",
            revert: "invalid"
        }).droppable({});

        $('.theme-area-mark-up').sortable({
            items: '.theme-block-mark-up',
            handle: '[action="drag"]',
            placeholder: 'place-holder-block',
            connectWith: '.theme-area-mark-up',
            start: function(e, ui){
                //ui.placeholder.height(ui.item.height());
                ui.item.height(40);
                //ui.item.width(200);
            },
            stop: function(e, ui) {
                reOrderBlocks();
            },
            receive: function (event, ui) {
                var _sourceBlock = $(event.originalEvent.target);
                var _droppedBlock = $($(this).data().uiSortable.currentItem);
                if (_sourceBlock.hasClass('vnp-ready-block')) {
                    var _droppedArea = $(this).data().area;
                    var _blockFile = _droppedBlock.attr('data-block');

                    _droppedBlock.wrap('<div class="VNP_Theme_TempBlock theme-block-mark-up block-type-' + _blockFile + '"></div>');
                    var boxObj = {title:'Add block', content: '<iframe src="' + VNP.BaseUrl + 'ajax/text/nTheme/editBlock/' + _blockFile + '/' + _droppedArea + '" tabindex="-1" style="width: 100% !important; height:100% !important"></iframe>', footer:''};
                    VNP.SugarBox.Open(boxObj);
                }
            }
        }).disableSelection();
    });
    $('#saveDesignOptions').click(function(e) {
        e.preventDefault();
        BoxObject = {   title:'Save design',
                        content: '' +
                        '<form class="design-save-popup" method="post" action="' + $('#designFormActions').attr('action') + '">' +
                            '<input type="hidden" name="currentState" value="' + $('#currentState').attr('value') + '" />' +
                        '<div><label>' +
                            '<input name="saveForThisPage" type="checkbox" checked="checked" value="thisPage" /> Only this page' +
                        '</label></div>' +
                        '<div><label>' +
                            '<input name="saveForThisRoute" type="checkbox" value="thisRoute" /> All pages of this route' +
                        '</label></div>' +
                        '<div><input type="submit" name="saveDesignOptions" class="btn btn-primary" value="Save change" /></div>' +
                        '</form>',
                        footer:''
        };
        VNP.SugarBox.Open(BoxObject);
        //return false;
    });
});
$(document).on('click', '[action="edit"]', function(e) {
    e.preventDefault();
    var blockID = $(this).parent().attr('data-block-id');
    var boxObj = {title:'Edit block', content: '<iframe src="' + VNP.BaseUrl + 'ajax/text/nTheme/editBlock/' + blockID + '" tabindex="-1" style="width: 100% !important; height:100% !important"></iframe>', footer:''};
    VNP.SugarBox.Open(boxObj);
});

$(document).on('click', '[action="remove"]', function(e) {
    e.preventDefault();
    var blockID = $(this).parent().attr('data-block-id');
    if(confirm('Are you sure to remove this block?')) {
        VNP.Ajax({
            type: 'POST',
            url: 'nTheme/removeBlock',
            data: {bid: blockID},
            success: function(result) {
                VNP.Loader.hide();
                $('#block-' + blockID).remove();
                if(result != 'OK') alert('Error, can\'t remove block!');
            },
            error: function(requestObject, error, errorThrown) {
                console.log(requestObject);
                VNP.Loader.hide();
                alert('Error, can\'t remove block!');
            }
        }, 'text');
    }
});

/**** In block config popup window ****/
if(typeof inBlockConfig != 'undefined') {
    $(document).on('click', '[role="Close_SugarBox"]', function(e) {
        e.preventDefault();
        $('.VNP_Theme_TempBlock', parent.document).remove();
        parent.VNP.SugarBox.Close();
    });

    if(isSaved) {
        if(isSuccessedAddBlock) {
            $('.VNP_Theme_TempBlock', parent.document)
                .addClass('block-id-' + blockData.bid)
                .attr('id', 'block-' + blockData.bid)
                .attr('data-block-id', blockData.bid)
                .html(blockData.content)
                .removeClass('VNP_Theme_TempBlock');
        }
        else
            $('#block-' + blockData.bid, parent.document).html(blockData.content);
        parent.reGenerateBlockMarkup(parent.document);
    }

    if(isCloseBlockConfig) {
        parent.VNP.SugarBox.Close();
    }
}