<?php


/***** Backend *****/
$global_lang['search_in']               = 'Tìm trong';
$global_lang['search_by']               = 'Tìm theo';
$global_lang['sort_by']                 = 'Xếp theo';
$global_lang['status']                  = 'Trạng thái';
$global_lang['show']                    = 'Hiển thị';
$global_lang['action_btn']              = 'Chọn chức năng';
$global_lang['active']                  = 'Kích hoạt';
$global_lang['deactive']                = 'Đình chỉ';
$global_lang['delete']                  = 'Xóa';
$global_lang['edit']                    = 'Chỉnh sửa';


$global_lang['Node not found']          					= 'Không tìm thấy nút';
$global_lang['Success delete node']     					= 'Xóa thành công: ';
$global_lang['Error delete node']       					= 'Đã có lỗi khi xóa: ';
$global_lang['Confirm remove node: ']   					= 'Xác nhận xóa: ';
$global_lang['Successful add node']   						= 'Thêm thành công ';
$global_lang['Successful update node in ']   				= 'Sửa thành công ';
$global_lang['Cannot update node in ']   					= 'Không thể sửa ';
$global_lang['Cannot add node in ']   						= 'Không thể thêm ';
$global_lang['Cannot update, Node not found!']   			= 'Lỗi, không tìm thấy bài!';
$global_lang['Be sure that <em>url</em> didn\'t existed!']  = ' vui lòng kiểm tra bài viết đã tồn tại chưa ';


$global_lang['There is no nodes found!']  					= 'Không có gì cả!';
$global_lang['List nodes for']  							= 'Danh sách';
$global_lang['( Found %s nodes )']  						= '( Tìm thấy %s dòng )';
$global_lang['Add new']  									= 'Thêm mới';


/***** online support *****/
$global_lang['phone_support']           = 'Hotline %s :';

/***** site search *****/
$global_lang['top_search_placeholder']  = 'Tìm kiếm sản phẩm hoặc thương hiệu bạn muốn...';
$global_lang['top_search_whole_pages']  = 'Toàng trang';
$global_lang['all_products']            = 'Tất cả sản phẩm';
$global_lang['all_posts']               = 'Tất cả bài viết';
$global_lang['search_btn']              = 'Search';
$global_lang['search_holder']           = 'Find somethings';
$global_lang['search_title']            = 'Find somethings';
$global_lang['search_result_article']   = 'Search results';
$global_lang['all_categories']          = 'All categories';
$global_lang['search_result_article_found']     = 'Found %d articles for keyword <em>%s</em>';
$global_lang['search_result_for_keyword']       = 'Search result for keyword ';


/***** product lang *****/
$global_lang['product_cart_label']      = 'Có <span>%d</span> sản phẩm';
$global_lang['product_search_keyword_placeholder']      = 'Tên sản phẩm hoặc mã...';
$global_lang['product_all_categories']  = 'Tất cả danh mục';
$global_lang['product_all_groups']      = 'Tất cả nhóm';
$global_lang['product_search_reset']    = 'Làm lại';
$global_lang['product_brand_search']    = 'Xem theo thương hiệu';


/***** User *****/
$global_lang['user_login']              = 'Đăng nhập';
$global_lang['user_logout']             = 'Thoát';
$global_lang['user_register']           = 'Đăng ký';
$global_lang['user_profile']            = 'Trang thành viên';
$global_lang['user_my_orders']          = 'Theo dõi đơn hàng';

/***** comments *****/

$global_lang['comment_has']      = 'This post has <span>%d</span> comments';
$global_lang['leave_a_reply']      = 'Leave a reply';


/***** Tags *****/
$global_lang['relative_keywords']      = 'Relative keywords';


/***** front lang *****/
$global_lang['home']      = 'Home';

/***** tag *****/
$global_lang['tag_detail_title']      = 'Articles for tag: %s';

?>