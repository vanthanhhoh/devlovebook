<?php


/***** Backend *****/
$global_lang['search_in']               = 'Tìm trong';
$global_lang['search_by']               = 'Tìm theo';
$global_lang['sort_by']                 = 'Xếp theo';
$global_lang['status']                  = 'Trạng thái';
$global_lang['show']                    = 'Hiển thị';
$global_lang['action_btn']              = 'Chọn chức năng';
$global_lang['active']                  = 'Kích hoạt';
$global_lang['deactive']                = 'Đình chỉ';
$global_lang['delete']                  = 'Xóa';
$global_lang['edit']                    = 'Chỉnh sửa';


$global_lang['Node not found']          					= 'Không tìm thấy nút';
$global_lang['Success delete node']     					= 'Xóa thành công: ';
$global_lang['Error delete node']       					= 'Đã có lỗi khi xóa: ';
$global_lang['Confirm remove node: ']   					= 'Xác nhận xóa: ';
$global_lang['Successful add node']   						= 'Thêm thành công ';
$global_lang['Successful update node in ']   				= 'Sửa thành công ';
$global_lang['Cannot update node in ']   					= 'Không thể sửa ';
$global_lang['Cannot add node in ']   						= 'Không thể thêm ';
$global_lang['Cannot update, Node not found!']   			= 'Lỗi, không tìm thấy bài!';
$global_lang['Be sure that <em>url</em> didn\'t existed!']  = ' vui lòng kiểm tra bài viết đã tồn tại chưa ';


$global_lang['There is no nodes found!']  					= 'Không có gì cả!';
$global_lang['List nodes for']  							= 'Danh sách';
$global_lang['( Found %s nodes )']  						= '( Tìm thấy %s dòng )';
$global_lang['Add new']  									= 'Thêm mới';


/***** Home *****/
$global_lang['Home']     = 'Trang chủ';

/***** online support *****/
$global_lang['phone_support']           = 'Hotline %s :';

/***** site search *****/
$global_lang['top_search_placeholder']  = 'Tìm kiếm sản phẩm hoặc thương hiệu bạn muốn...';
$global_lang['top_search_whole_pages']  = 'Toàng trang';
$global_lang['all_products']            = 'Tất cả sản phẩm';
$global_lang['all_posts']               = 'Tất cả bài viết';
$global_lang['search_btn']              = 'Tìm kiếm';
$global_lang['search_holder']           = 'Từ khóa tìm kiếm';
$global_lang['search_title']            = 'Tìm những gì bạn muốn';
$global_lang['search_result_article']   = 'Kết quả tìm kiếm';
$global_lang['all_categories']          = 'Tất cả danh mục';
$global_lang['search_result_article_found']     = 'Tìm thấy %d bài viết cho từ khóa <em>%s</em>';
$global_lang['search_result_article_found_in_cat']     = 'Tìm thấy %d bài viết cho từ khóa <em>%s</em> trong chuyên mục <em>%s</em>';
$global_lang['search_result_for_keyword']       = 'Kết quả tìm kiếm cho từ khóa ';


/***** product lang *****/
$global_lang['product_cart_label']      = 'Có <span>%d</span> sản phẩm';
$global_lang['product_search_keyword_placeholder']      = 'Tên sản phẩm hoặc mã...';
$global_lang['product_all_categories']  = 'Tất cả danh mục';
$global_lang['product_all_groups']      = 'Tất cả nhóm';
$global_lang['product_search_reset']    = 'Làm lại';
$global_lang['product_brand_search']    = 'Xem theo thương hiệu';


/***** User *****/
$global_lang['user_login']              = 'Đăng nhập';
$global_lang['user_logout']             = 'Thoát';
$global_lang['user_register']           = 'Đăng ký';
$global_lang['user_profile']            = 'Trang thành viên';
$global_lang['user_my_orders']          = 'Theo dõi đơn hàng';

/***** comments *****/

$global_lang['comment_has']      = 'Có <span>%d</span> bình luận cho bài viết này';
$global_lang['leave_a_reply']      = 'Gửi bình luận của bạn';


/***** Tags *****/
$global_lang['relative_keywords']      = 'Từ khóa liên quan';


/***** front lang *****/
$global_lang['home']      = 'Home';

/***** tag *****/
$global_lang['tag_detail_title']      = 'Bài viết với từ khóa: %s';

/***** news letter *****/
$global_lang['news_letter_label']     = 'Đăng ký nhận tin';

/***** comment *****/
$global_lang['comment_user_name']     = 'Tên bạn';
$global_lang['comment_user_email']     = 'Email';
$global_lang['comment_form_reset']     = 'Làm lại';
$global_lang['comment_do_send']     = 'Gửi bình luận';
$global_lang['comment_allow_notification']     = 'Thông báo khi có phản hồi cho bình luận của bạn';

$global_lang['comment_do_reply']     = 'Trả lời';
$global_lang['comment_do_like']     = 'Thích';
$global_lang['comment_do_share']     = 'Chia sẻ';
$global_lang['comment_do_report']     = 'Báo cáo';

?>