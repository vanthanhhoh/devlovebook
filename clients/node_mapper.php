<?php if(!defined('VNP_APPLICATION')) die('Stop here');
$NodeProfile = array (
  'page' => 
  array (
    'title' => 'Page',
    'split' => '0',
    'code' => '8881',
  ),
  'product_category' => 
  array (
    'title' => 'Danh mục sản phẩm',
    'split' => '0',
    'code' => '2001',
  ),
  'invoice' => 
  array (
    'title' => 'Đơn hàng',
    'split' => '0',
    'code' => '4000',
  ),
  'cupon' => 
  array (
    'title' => 'Mã giảm giá',
    'split' => '0',
    'code' => '7000',
  ),
  'publisher' => 
  array (
    'title' => 'Nhà xuất bản',
    'split' => '0',
    'code' => '3001',
  ),
  'product_group' => 
  array (
    'title' => 'Nhóm sản phẩm',
    'split' => '0',
    'code' => '2002',
  ),
  'ship' => 
  array (
    'title' => 'Vận chuyển',
    'split' => '0',
    'code' => '4001',
  ),
  'author' => 
  array (
    'title' => 'Tác giả',
    'split' => '0',
    'code' => '3000',
  ),
  'menu' => 
  array (
    'title' => 'Menu',
    'split' => '0',
    'code' => '6000',
  ),
  'review' => 
  array (
    'title' => 'Đánh giá',
    'split' => '0',
    'code' => '8000',
  ),
  'slider' => 
  array (
    'title' => 'Slider',
    'split' => '0',
    'code' => '1000',
  ),
  'menu_group' => 
  array (
    'title' => 'Nhóm menu',
    'split' => '0',
    'code' => '6001',
  ),
  'product' => 
  array (
    'title' => 'Sản phẩm',
    'split' => '0',
    'code' => '2000',
  ),
  'payment' => 
  array (
    'title' => 'Thanh toán',
    'split' => '0',
    'code' => '4002',
  ),
  'notification' => 
  array (
    'title' => 'Thông báo',
    'split' => '0',
    'code' => '8002',
  ),
);
$NodeMapper = array (
  8881 => 
  array (
    'controller' => 'page',
    'title' => 'Page',
    'split' => '0',
  ),
  2001 => 
  array (
    'controller' => 'product_category',
    'title' => 'Danh mục sản phẩm',
    'split' => '0',
  ),
  4000 => 
  array (
    'controller' => 'invoice',
    'title' => 'Đơn hàng',
    'split' => '0',
  ),
  7000 => 
  array (
    'controller' => 'cupon',
    'title' => 'Mã giảm giá',
    'split' => '0',
  ),
  3001 => 
  array (
    'controller' => 'publisher',
    'title' => 'Nhà xuất bản',
    'split' => '0',
  ),
  2002 => 
  array (
    'controller' => 'product_group',
    'title' => 'Nhóm sản phẩm',
    'split' => '0',
  ),
  4001 => 
  array (
    'controller' => 'ship',
    'title' => 'Vận chuyển',
    'split' => '0',
  ),
  3000 => 
  array (
    'controller' => 'author',
    'title' => 'Tác giả',
    'split' => '0',
  ),
  6000 => 
  array (
    'controller' => 'menu',
    'title' => 'Menu',
    'split' => '0',
  ),
  8000 => 
  array (
    'controller' => 'review',
    'title' => 'Đánh giá',
    'split' => '0',
  ),
  1000 => 
  array (
    'controller' => 'slider',
    'title' => 'Slider',
    'split' => '0',
  ),
  6001 => 
  array (
    'controller' => 'menu_group',
    'title' => 'Nhóm menu',
    'split' => '0',
  ),
  2000 => 
  array (
    'controller' => 'product',
    'title' => 'Sản phẩm',
    'split' => '0',
  ),
  4002 => 
  array (
    'controller' => 'payment',
    'title' => 'Thanh toán',
    'split' => '0',
  ),
  8002 => 
  array (
    'controller' => 'notification',
    'title' => 'Thông báo',
    'split' => '0',
  ),
)
?>