{do}
    $installed_exts = $site_extensions;
    Theme::JsFooter('admin_ext', APPLICATION_DATA_DIR . 'static/extension/admin.js');
{/do}
<input id="dashboard_key" type="hidden" value="{$ext_token}" />
<div class="extension-dashboard">
    <h1>Quản lý ứng dụng</h1>
    <div class="extension-list-wrapper">
        <ul class="extension-list">
            {for $ext in $extensions as $ext_name}
                <li>
                    <div class="clearfix">
                        <div class="ext-img">
                            <img src="{#APPLICATION_BASE}/extensions/{$ext_name}/icon.png" />
                        </div>
                        <div class="ext-info">
                            <h3>{$ext.name}</h3>
                            <div class="version">
                                Phiên bản: {$ext.version}&nbsp&nbsp&nbsp<b>&#8226;</b>&nbsp&nbsp
                                {if($ext['orders']['price'] == 0)}
                                    Thời hạn: <font color="#3580BE"> Vĩnh viễn</font>
                                {elseif(isset($installed_exts[$ext_name]))}
                                    {if($installed_exts[$ext_name]['bought_time'] == EXT_NOT_BUY)}
                                        {do}
                                            $use_time = CURRENT_TIME - $installed_exts[$ext_name]['installed_time'];
                                            $remain_time = EXT_TRIAL_MONTH*30*24*3600 - $use_time;
                                            $remain_time = floor($remain_time/(3600*24));
                                        {/do}
                                        Còn lại: <font color="#3580BE"> {$remain_time} ngày</font>
                                    {elseif($installed_exts[$ext_name]['bought_time'] != EXT_FREE_BUY)}
                                        {do}
                                            $use_time = CURRENT_TIME - $installed_exts[$ext_name]['bought_time'];
                                            $remain_time = $ext['orders']['order_block']*30*24*3600 - $use_time;
                                            $remain_time = floor($use_time/(3600*24));
                                        {/do}
                                        Còn lại: <font color="#3580BE"> {$remain_time} ngày</font>
                                    {/if}
                                {else}
                                    {do}$remain_time = EXT_TRIAL_MONTH*31{/do}
                                    Thời hạn: <font color="#3580BE"> {$remain_time} ngày</font>
                                {/if}
                            </div>
                            <div class="desc">{$ext.description}</div>
                        </div>
                    </div>
                    <div class="price-info">
                        {if($ext.orders.price == 0)}
                            <div class="free-ext">Miễn phí</div>
                        {else}
                            <div class="ext-price">
                                {$ext.orders.price|intval|Output::PriceFormat} đ / {$ext.orders.order_block} tháng
                                {if(isset($ext.orders.discount_string) && !empty($ext.orders.discount_string))}
                                    <span class="discount-string">
                                        &nbsp<b>&#8226;</b>&nbsp
                                        {$ext.orders.discount_string|BaseExtension::replace_ext_url_shortcode}
                                    </span>
                                {/if}
                            </div>
                        {/if}
                        {if(isset($ext.orders.trial_string) && !empty($ext.orders.trial_string))}
                            <div class="trial-string">
                                {$ext.orders.trial_string}
                            </div>
                        {/if}
                    </div>
                    <ul class="ext-actions clearfix">
                        <li><a href="{do}echo Router::Generate('ControllerParams', array('controller' => 'extension', 'action' => 'info', 'params' => $ext_name)){/do}">Xem chi tiết ...</a></li>
                        {if(isset($ext.admin_pages))}
                            {for $ap in $ext.admin_pages as $apn}
                                <li>
                                    <a href="{do}echo Router::Generate('ControllerParams', array('controller' => 'extension', 'action' => 'explore', 'params' => $ext_name . '/' . $apn)){/do}" title="{$ap.title}">{$ap.title}</a>
                                </li>
                            {/for}
                        {/if}
                    </ul>
                    <div class="install-status">
                        {if(isset($installed_exts[$ext_name]) && $installed_exts[$ext_name]['is_installed'] == 1)}
                            <a class="uninstall" data-ext="{$ext_name}" href="#">Đã cài đặt</a>
                        {else}
                            <a class="install" data-ext="{$ext_name}" href="#">Cài đặt</a>
                        {/if}
                    </div>
                    <div class="active-status">
                        {if(isset($installed_exts[$ext_name]) && $installed_exts[$ext_name]['is_installed'] == 1)}
                            {if($installed_exts[$ext_name]['is_activated'] == 1)}
                                <a class="deactive" data-ext="{$ext_name}" href="#">Tạm dừng</a>
                            {else}
                                <a class="active" data-ext="{$ext_name}" href="#">Kích hoạt</a>
                            {/if}
                        {/if}
                    </div>
                    {if($ext['orders']['price'] > 0 && $installed_exts[$ext_name]['is_installed'] == 1)}
                        <div class="buy-status">
                            {if(isset($installed_exts[$ext_name]) && $installed_exts[$ext_name]['bought_time'] > EXT_NOT_BUY)}
                                <a class="renew" data-ext="{$ext_name}" href="#">Gia hạn</a>
                            {else}
                                <a class="buy" data-ext="{$ext_name}" href="#">Mua</a>
                            {/if}
                        </div>
                    {/if}
                </li>
            {/for}
        </ul>
    </div>
</div>