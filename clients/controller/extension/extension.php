<?php

define('EXT_TRIAL_MONTH', 2);
define('EXT_USE_FOREVER', -1);
define('EXT_FREE_BUY', -1);
define('EXT_NOT_BUY', 0);

class extension extends Controller {

    static $extensions = array();
    static $site_extensions = array();

    public function Construct() {
        self::$extensions = BaseExtension::get_all_extensions();
        self::$site_extensions = unserialize(Option::get('site_extensions'));
    }

    public function rest_api() {
        if(empty(G::$Route['params']['ext_name']) || empty(G::$Route['params']['ext_action'])) {
            Output::ErrorPage(403, 'You are forbidden!');
            die();
        }
        $ext_name = G::$Route['params']['ext_name'];
        $ext_action = 'action_' . G::$Route['params']['ext_action'];
        $ext_params = isset(G::$Params) ? G::$Params : array();
        $ext = BaseExtension::load_extension($ext_name);
        $ext_action_exec = '';
        if(method_exists($ext, $ext_action)) {
            $ext_action_exec = call_user_func_array(array($ext, $ext_action), $ext_params);
        }
        $this->Render($ext_action_exec);
    }

    public function dashboard() {
        Access::RequirePermission(Boot::SMOD_SESSION);
        Access::CheckUserPermission();
        $this->Render(
            $this->View('dashboard')
                ->Assign(
                    array(  'extensions' => self::$extensions,
                            'site_extensions' => self::$site_extensions,
                            'ext_token' => Access::GenerateToken('ext_token')
                    )
                )
                ->Output()
        );
    }

    public function action() {
        Access::RequirePermission(Boot::SMOD_SESSION);
        Access::CheckUserPermission();

        $support_actions = array('install','uninstall','reinstall','buy','renew','active','deactive');
        $data['type'] = Input::Post('type', '');
        $data['token'] = Input::Post('token', '');
        $data['extension'] = Input::Post('extension', '');

        $_POST['ext_token'] = $data['token'];

        $errors = array();
        if(!Access::CheckToken('ext_token', false)) $errors[] = 'Invalid token!';
        if(empty($data['type'])) $errors[] = 'Invalid action!';
        if(empty($data['extension']) || !file_exists(EXTENSION_PATH . $data['extension'] . DIRECTORY_SEPARATOR . $data['extension'] . '.php')) $errors[] = 'Invalid extension!';

        $return = array('status' => 'NOT', 'msg' => 'Unknown error', 'errors' => array());


        if(empty($errors)) {
            switch ($data['type']) {
                case 'install':
                    $return = $this->install_ext($data['extension']);
                    break;
                case 'uninstall':
                    $return = $this->uninstall_ext($data['extension']);
                    break;
                case 'reinstall':
                    $return = $this->reinstall_ext($data['extension']);
                    break;
                case 'update':
                    $return = $this->update_ext($data['extension']);
                    break;
                case 'active':
                    $return = $this->active_ext($data['extension']);
                    break;
                case 'deactive':
                    $return = $this->deactive_ext($data['extension']);
            }
        }
        else {
            $return['msg'] = 'invalid action';
            $return['errors'] = $errors;
        }
        header('Content-Type: application/json');
        echo json_encode($return);
        die();
    }

    private function install_ext($ext_name) {
        Access::RequirePermission(Boot::SMOD_SESSION);
        Access::CheckUserPermission();
        if(isset(self::$extensions[$ext_name])) {
            $ext_data = self::$extensions[$ext_name];
            $db_insert = false;
            if(isset(self::$site_extensions[$ext_name]) && self::$site_extensions[$ext_name]['is_installed'] == 1) {
                return array(
                        'status' => 'NOT',
                        'msg' => 'This extension had been installed!'
                );
            }
            if(isset($ext_data['install_function'])) {
                include EXTENSION_PATH . $ext_name . DIRECTORY_SEPARATOR . $ext_name . '.php';
                if(method_exists($ext_name, $ext_data['install_function'])) {
                    $check = call_user_func(array($ext_name, $ext_data['install_function']));
                    if($check['status']) {
                        $return = $check;
                        $db_insert = true;
                    }
                }
                else
                    $return = array('status' => 'NOT', 'msg' => 'Cannot install this extension', 'error' => 'Invalid install function');
            }
            else {
                $db_insert = true;
                $return = array('status' => 'OK', 'msg' => 'Success install this extension', 'error' => '');
            }

            if($db_insert) {
                if(isset(self::$site_extensions[$ext_name])) {
                    self::$site_extensions[$ext_name]['is_installed'] = 1;
                    self::$site_extensions[$ext_name]['is_activated'] = 1;
                }
                else {
                    $usage_time = ($ext_data['orders']['price'] == 0) ? EXT_USE_FOREVER : EXT_TRIAL_MONTH;
                    $bought_time = ($ext_data['orders']['price'] == 0) ? EXT_FREE_BUY : EXT_NOT_BUY;
                    self::$site_extensions[$ext_name] = array(
                        'is_installed' => 1,
                        'is_activated' => 1,
                        'installed_time' => CURRENT_TIME,
                        'bought_time' => $bought_time,
                        'usage_time' => $usage_time
                    );
                }
                Option::set('site_extensions', serialize(self::$site_extensions));
            }
        }
        return $return;
    }

    private function uninstall_ext($ext_name) {
        Access::RequirePermission(Boot::SMOD_SESSION);
        Access::CheckUserPermission();
        if(isset(self::$extensions[$ext_name])) {
            $ext_data = self::$extensions[$ext_name];
            $db_insert = false;
            if(isset(self::$site_extensions[$ext_name]) && self::$site_extensions[$ext_name]['is_installed'] == 0) {
                return array(
                    'status' => 'NOT',
                    'msg' => 'This hasn\'t been installed!'
                );
            }
            if(isset($ext_data['uninstall_function'])) {
                include EXTENSION_PATH . $ext_name . DIRECTORY_SEPARATOR . $ext_name . '.php';
                if(method_exists($ext_name, $ext_data['uninstall_function'])) {
                    $check = call_user_func(array($ext_name, $ext_data['uninstall_function']));
                    if($check['status']) {
                        $return = $check;
                        $db_insert = true;
                    }
                }
                else
                    $return = array('status' => 'NOT', 'msg' => 'Cannot uninstall this extension', 'error' => 'Invalid install function');
            }
            else {
                $db_insert = true;
                $return = array('status' => 'OK', 'msg' => 'Success uninstall this extension', 'error' => '');
            }

            if($db_insert) {
                $usage_time = ($ext_data['orders']['price'] == 0) ? EXT_USE_FOREVER : EXT_TRIAL_MONTH;
                $bought_time = ($ext_data['orders']['price'] == 0) ? EXT_FREE_BUY : EXT_NOT_BUY;
                self::$site_extensions[$ext_name]['is_installed'] = 0;
                self::$site_extensions[$ext_name]['is_activated'] = 0;
                Option::set('site_extensions', serialize(self::$site_extensions));
            }
        }
        return $return;
    }

    private function reinstall_ext($ext_name) {
        Access::RequirePermission(Boot::SMOD_SESSION);
        Access::CheckUserPermission();
        $u = $this->uninstall_ext($ext_name);
        $i = $this->install_ext($ext_name);
        if($u['status'] && $i['status'])
            $return = array('status' => 'OK', 'msg' => 'Success reinstall this extension', 'error' => '');
        else
            $return = array('status' => 'NOT', 'msg' => 'Cannot reinstall this extension', 'error' => 'Unknow errors');
        return $return;
    }

    private function active_ext($ext_name) {
        Access::RequirePermission(Boot::SMOD_SESSION);
        Access::CheckUserPermission();
        if(isset(self::$extensions[$ext_name])) {
            $ext_data = self::$extensions[$ext_name];
            $db_insert = false;
            if(isset(self::$site_extensions[$ext_name]) && self::$site_extensions[$ext_name]['is_activated'] == 1) {
                return array(
                    'status' => 'NOT',
                    'msg' => 'This had been activated!'
                );
            }
            if(isset($ext_data['active_function'])) {
                include EXTENSION_PATH . $ext_name . DIRECTORY_SEPARATOR . $ext_name . '.php';
                if(method_exists($ext_name, $ext_data['active_function'])) {
                    $check = call_user_func(array($ext_name, $ext_data['active_function']));
                    if($check['status']) {
                        $return = $check;
                        $db_insert = true;
                    }
                }
                else
                    $return = array('status' => 'NOT', 'msg' => 'Cannot active this extension', 'error' => 'Invalid active function');
            }
            else {
                $db_insert = true;
                $return = array('status' => 'OK', 'msg' => 'Success active this extension', 'error' => '');
            }

            if($db_insert) {
                self::$site_extensions[$ext_name]['is_activated'] = 1;
                Option::set('site_extensions', serialize(self::$site_extensions));
            }
        }
        return $return;
    }

    private function deactive_ext($ext_name) {
        Access::RequirePermission(Boot::SMOD_SESSION);
        Access::CheckUserPermission();
        if(isset(self::$extensions[$ext_name])) {
            $ext_data = self::$extensions[$ext_name];
            $db_insert = false;
            if(isset(self::$site_extensions[$ext_name]) && self::$site_extensions[$ext_name]['is_activated'] == 0) {
                return array(
                    'status' => 'NOT',
                    'msg' => 'This hasn\'t been activated!'
                );
            }
            if(isset($ext_data['deactive_function'])) {
                include EXTENSION_PATH . $ext_name . DIRECTORY_SEPARATOR . $ext_name . '.php';
                if(method_exists($ext_name, $ext_data['deactive_function'])) {
                    $check = call_user_func(array($ext_name, $ext_data['deactive_function']));
                    if($check['status']) {
                        $return = $check;
                        $db_insert = true;
                    }
                }
                else
                    $return = array('status' => 'NOT', 'msg' => 'Cannot deactive this extension', 'error' => 'Invalid deactive function');
            }
            else {
                $db_insert = true;
                $return = array('status' => 'OK', 'msg' => 'Success deactive this extension', 'error' => '');
            }

            if($db_insert) {
                self::$site_extensions[$ext_name]['is_activated'] = 0;
                Option::set('site_extensions', serialize(self::$site_extensions));
            }
        }
        return $return;
    }

    public function explore() {
        Access::RequirePermission(Boot::SMOD_SESSION);
        Access::CheckUserPermission();
        if(!empty(G::$Params)) {
            $ext_info = array_keys(G::$Params);
            $ext_name = $ext_info[0];
            $ext_action = G::$Params[$ext_name];

            if(isset(self::$extensions[$ext_name])) {
                $ext_data = self::$extensions[$ext_name];
                if(in_array($ext_action, array_keys($ext_data['admin_pages']))) {
                    $page = $ext_data['admin_pages'][$ext_action];
                    Helper::PageInfo($ext_data['name'] . ' - ' . $page['title']);
                    if($page['type'] == 'option') {

                        $options = $page['options'];
                        $savedOptions = Option::getOptionsOfType('extension', $ext_name . '_' . $ext_action);
                        foreach($savedOptions as $op => $ov) {
                            if(isset($savedOptions[$op]))
                                $options[$op]['value'] = $savedOptions[$op]['value'];
                        }


                        $ext_options = Option::formFromConfig('extension_' . $ext_name . '_' . $ext_action, $options, array(), RECOMPILE_CONFIG_FORM);


                        if(!empty($ext_options['submitedValues'])) {
                            $c = 0;
                            foreach($ext_options['submitedValues'] as $tok => $tov) {
                                $q = Option::set($tok, $tov, $options[$tok]['label'], 'extension', $ext_name . '_' . $ext_action);
                                if($q) $c++;
                            }
                            if($c == sizeof($ext_options['submitedValues']))
                                Helper::Notify('success', 'Success save options');
                            else Helper::Notify('error', 'Some options cannot be saved!');
                        }
                         $this->Render('<div class="box options-page">' . $ext_options['form'] . '</div>');
                        return;
                    }
                    elseif($page['type'] == 'action') {
                        include EXTENSION_PATH . $ext_name . DIRECTORY_SEPARATOR . $ext_name . '.php';
                        if(method_exists($ext_name, $ext_action)) {
                            $this->Render(call_user_func(array($ext_name, $ext_action)));
                            return;
                        }
                    }
                }
            }
        }
        Output::ErrorPage(404);
        return false;
    }
}