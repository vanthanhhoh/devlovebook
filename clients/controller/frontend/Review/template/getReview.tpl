{for $reviews in $review}
    <div class="review_item" itemprop="itemReviewed" itemtype="http://schema.org/Product">
        <div class="row">
            <div class="col-md-2">
                <div class="author_review">
                    {function}
                        $author = $User[$reviews['user_id']];
                    {/function}
                    <img src="{$author.avatar}" alt="{$author.fullname}" width="80px" height="80px"/>
                    <p class="name" itemprop="author">{$author.fullname}</p>
                    <p class="days">{$reviews.add_time|Filter::UnixTimeToFullDate:true}</p>
                </div>
            </div>
            <div class="col-md-10">
                <div class="review_infomation">
                    <div class="rating">
                        <div itemprop="reviewRating" itemtype="http://schema.org/Rating">
                            <meta itemprop="ratingValue" content="{$reviews.rate}">
                        </div>
                        <p class="rating">
                            {function}
                                for($i=1;$i<=5;$i++){
                                if($i<=$reviews['rate']){
                                echo '<i class="fa fa-star on" aria-hidden="true"></i>';
                                                                                      }
                                                                                      else {
                                                                                      echo '<i class="fa fa-star" aria-hidden="true"></i>';
                                }
                                }
                            {/function}
                        </p>
                        <p class="review" itemprop="name">{$reviews.title}</p>
                    </div>
                    <div class="review_detail" itemprop="reviewBody">
                        {$reviews.comment}
                    </div>
                </div>
            </div>
        </div>
    </div>
{/for}