<?php
/**
 * Created by PhpStorm.
 * User: THANH
 * Date: 22/07/2016
 * Time: 11:27 SA
 */

class Review extends Controller {
    public function addReview(){
        $result = array(
            'status'=>0,
            'message'=>''
        );
        if(USER_ID>0){
            $product_id = Input::Post('product',0);
            $checkReview = DB::Query('review')->Where('user_id','=',USER_ID)
                            ->_AND()
                            ->Where('product_id','=',$product_id)
                            ->_AND()
                            ->Where('add_time','>=',CURRENT_TIME-1800)
                            ->Get();
            if($checkReview->status && $checkReview->num_rows==0){

                $product = DB::Query('product')->Where('product_id','=',$product_id)->Get();

                if($product->status && $product->num_rows==1){
                        $product= $product->Result[0];
                        $review = array(
                            'title'=>Input::Post('title',''),
                            'rate'=>Input::Post('rate',1),
                            'comment'=>Input::Post('comment',''),
                            'status'=>1,
                            'product_id'=>$product_id,
                            'user_id'=>USER_ID,
                            'add_time'=>CURRENT_TIME
                        );
                        $insert = DB::Query('review')->Insert($review);
                        if($insert->status && $insert->insert_id > 0){
                            $rate = array(
                              'total_comments' => $product['total_comments']+1,
                              'total_rates'=>$product['total_rates']+1,
                              'rate_count'=>$product['rate_count']+$review['rate']
                            );
                            $update = DB::Query('product')->Where('product_id','=',$product_id)->Update($rate);
                            if($update->status){
                                $result['status']=1;
                                $result['message']='Đã gửi nhận xét thành công! Nhận xét của bạn sẽ hiện thị sau khi chúng tôi kiểm duyệt';
                            }
                        }
                }
                else {
                    $result['status'] = 0;
                    $result['message']='Sản phẩm mà bạn review không có trong hệ thống !';
                }
            }
            else {
                $result['status'] = 0;
                $result['message']='Bạn vừa bình luận cho sản phẩm này! Khoảng thời gian nhận xét cho 1 sản phẩm phải lớn hơn 30 phút';
            }
        }
        else {
            $result['status'] = -1;
            $result['message']='Bạn cần đăng nhập hoặc đăng kí tài khoản để bình luận';
        }
        echo json_encode($result);die();
    }
    public function getReview(){
        $product = Input::Post('product',0);
        $page = Input::Post('page',1);
        G::$Global['page']=$page;
        $review = DB::Query('review')->Where('product_id','=',$product)->Limit(10)->Get('review_id',Output::Paging())->Result;
        $Users = DB::Query('users')->Columns('userid,fullname,email,avatar')->Get('userid')->Result;
        echo $this->View('getReview')->Assign('review',$review)->Assign('User',$Users)->Output();die();
    }
}