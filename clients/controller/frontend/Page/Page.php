<?php

class Page extends Controller {
    public function __construct() {
    }

    public function Detail() {
        $hasPage = false;
        if(isset(G::$Route['params']['page'])) {
            $pageUrl = strip_tags(G::$Route['params']['page']);
            $getPage = DB::Query('page')->Where('url', '=', $pageUrl)->Get();
            if($getPage->status && $getPage->total_rows > 0) {
                $getPage = $getPage->Result[0];
                Helper::State($getPage['title'],Router::GenerateThisRoute());
                Theme::SetTitle($getPage['title']);
                Theme::MetaTag('description',$getPage['description']);
                Theme::MetaTag('keywords',$getPage['title'].',lovebook, nhà sách giao dục lovebook, ôn thi đại học, sách thủ khoa');
                $hasPage = true;
                $this->Render(
                    $this->View('page_view')
                        ->Assign('page', $getPage)
                        ->Output()
                );

            }
        }
        if(!$hasPage)
            Output::ErrorPage(404, 'Page not found!');
    }
}