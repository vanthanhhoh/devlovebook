<?php
/**
 * Created by PhpStorm.
 * User: THANH
 * Date: 18/07/2016
 * Time: 3:03 CH
 */
use OAuth\OAuth2\Service\Facebook;
use OAuth\Common\Storage\Session;
use OAuth\Common\Consumer\Credentials;

class Auth extends Controller {
    public function login(){
        if(USER_ID==0){
            $result = array(
                'status'=>0,
                'message'=>''
            );
            $email = Input::Post('email','');
            $password = Input::Post('password','');
            if(G::$Authorized->CheckLogin($email, $password)){
                $result['status']=1;
                $result['message']='Đăng nhập thành công';
            }
            else {
                $result['status']=0;
                $result['message']='Email hoặc mật khẩu không chính xác';
            }
            echo json_encode($result);die();
        }
    }
    public function register(){
        $result = array(
            'status'=>0,
            'message'=>''
        );
        if(USER_ID==0){
            $User = array (
                'username' => '',
                'email' => '',
                'phone' => '',
                'fullname' => '',
                'avatar' => '',
                'birthday' => '',
                'gender' => '1',
                'status' => 1,
                'password' => '',
                'level' => '1'
            );
            $User['username'] = Input::Post('email','');
            $User['password'] = Input::Post('password','');
            $User['email']    = Input::Post('email','');
            $User['phone']    = Input::Post('phone','');
            $User['fullname'] = Input::Post('fullname','');
            $User['avatar']   = '/clients/data/images/anonymous.png';
            $User['birthday'] = Input::Post('birthday','');
            $User['gender']   = Input::Post('gender','');
            $User['birthday'] = Filter::DateToUnixTime($User['birthday'],0,0);
            $User['regdate']  = CURRENT_TIME;
            if($UserID = Authorized::AddUser($User)) {
                G::$Authorized->CheckLogin($User['email'], $User['password']);
                $result['message'] = 'Đăng kí tài khoản thành công';
                $result['status']  =  1;
            }
            else{
                $result['message'] = 'Đăng kí tài khoản thất bại';
            }
        }
        echo json_encode($result);die();
    }
    public function register_remote(){
        $call_service = G::$Params[ROUTER_EXTRA_KEY];
        $ref = Router::get('ref',APP_DOMAIN);
        if(!isset($_SESSION['ref'])){
            $_SESSION['ref'] = $ref;
        }else{
            $backurl = $_SESSION['ref'];
            unset($_SESSION['ref']);
        }
        $servicesCredentials = G::$Config['remote_login_services'];
        if(in_array($call_service, array_keys($servicesCredentials))) {
            Boot::Library('OAuth');
            $uriFactory = new \OAuth\Common\Http\Uri\UriFactory();
            $currentUri = $uriFactory->createFromSuperGlobalArray($_SERVER);
            $currentUri->setQuery('');
            $serviceFactory = new \OAuth\ServiceFactory();
            $storage = new Session();
            $credentials = new Credentials(
                $servicesCredentials[$call_service]['key'],
                $servicesCredentials[$call_service]['secret'],
                $currentUri->getAbsoluteUri()
            );

            if($call_service == 'facebook') {
                $ActionService = $serviceFactory->createService($call_service, $credentials, $storage, array('email','user_birthday', 'user_location', 'user_website'));
            }
            elseif($call_service == 'google') {
                $ActionService = $serviceFactory->createService($call_service, $credentials, $storage, array('userinfo_email', 'userinfo_profile', 'https://www.googleapis.com/auth/userinfo.profile'));
            }
            $get_code = Router::get('code', '');
            if (!empty($get_code)) {
                $token = $ActionService->requestAccessToken($get_code);
                if($call_service == 'facebook') {
                    $rs = json_decode($ActionService->request('/me?fields=birthday,currency,devices,email,gender,hometown,link,locale,location,name,timezone,website,work,picture,is_verified,first_name,last_name'), true);
                    if(!isset($rs['email'])){
                        $result['email'] = $rs['id'];
                    }
                    else {
                        $result['email'] = $rs['email'];
                    }
                    $result['id'] = $rs['id'];
                    $result['verified_email'] = $rs['is_verified'];
                    $result['name'] = $rs['name'];
                    $result['given_name'] = $rs['first_name'];
                    $result['family_name'] = $rs['last_name'];
                    $result['link'] = $rs['link'];
                    $result['picture'] = 'https://graph.facebook.com/' . $result['id'] . '/picture?width=400&height=400';
                    $result['gender'] = $rs['gender'];
                    $result['locale'] = $rs['locale'];
                    unset($rs);
                }
                elseif($call_service == 'google') {
                    $result = json_decode($ActionService->request('https://www.googleapis.com/oauth2/v1/userinfo'), true);
                    if(!isset($result['birthday'])) $result['birthday'] = '01/01/1970';
                }
                if(!empty($result['email'])) {
                    $site_user = array(
                        'login_service' => $call_service,
                        'service_id'    => $result['id'],
                        'username'      => $call_service . '_' . $result['id'],
                        'email' => $result['email'],
                        'fullname' => $result['name'],
                        'avatar' => $result['picture'],
                        'level' =>1
                    );
                    $site_user['gender'] = ($result['gender'] == 'male') ? 1 : 0;
                    if(isset($result['website'])) $site_user = $result['website'];
                    $system_user = Authorized::getUserByEmailOrUserName($site_user['email']);
                    if($system_user == false) {
                        if($uid = Authorized::AddUser($site_user)) {
                            $system_user = Authorized::getUserByEmailOrUserName($site_user['email']);
                            $system_user['userid'] = $uid;
                        }
                    }
                    if(!empty($system_user)) $_SESSION['UserInfo'] = $system_user;
                    $v = $this->View('remote');
                    $this->Render($v->Output(),array('layout'=>'layout.empty'));
                }
                else {
                    Output::ErrorPage(503, 'Invalid action!');
                    die();
                }
            }
            else {
                $url = $ActionService->getAuthorizationUri();
                header('Location: ' . $url);
            }
        }
    }
}