<?php
/**
 * Created by PhpStorm.
 * User: THANH
 * Date: 22/03/2016
 * Time: 3:29 CH
 */

class Home extends Controller {
    public function Main(){

        Theme::SetTitle('Nhà sách giáo dục Lovebook | Lovebook sách của thủ khoa');
        $v = $this->View('home')
                  ->Assign('slider',$this->slider())
                  ->Assign('popular',$this->popular());
        $this->Render($v->Output());

    }
    public function slider(){
        $slider = DB::Query('slider')->Get()->Result;
        $category = frontend::$product_category;
        $menu = Output::MenuGenerator($category,'product_category_id','parent_id');
        $product = frontend::$product;
        $author = frontend::$author;
        foreach($menu as $key => $item){
            $child = clients::GetChildID($category,$item['product_category_id']);
            $menu[$key]['author'] = array();
            foreach($product as $pitem){
                if(in_array($pitem['product_category'],$child)){
                    $menu[$key]['author'][] = $pitem['author'];
                }
            }
            $menu[$key]['author'] = array_unique($menu[$key]['author']);
        }
        return $this->View('slider')
            ->Assign('slider',$slider)

            ->Assign('menu',$menu)
            ->Assign('author',$author)
            ->Output();
    }
    public function popular(){
        $product = DB::Query('product')->Limit(10)->Get()->Result;
        return $this->View('popular')->Assign('product',$product)->Output();
    }

}