<div class="slider hidden-xs hidden-sm">
    <div class="slider_carousel">
        <ul class="rslides">
            {for $item in $slider}
                <li>
                    <img src="{$item.image}" alt="{$item.title}" width="100%"/>
                </li>
            {/for}
        </ul>
    </div>
    <script type="application/javascript">
        $(function() {
            var slheight = $('ul.rslides li img').width();
            var container = $('.container').width();
            var right = (slheight-container)/2;
            $(".rslides").responsiveSlides({
                speed: 2000
            });
            $('ul.rslides_tabs').css('right',right+'px');
        });
    </script>
    <div class="list-category-wapper">
        <div class="container">
            <div class="row">
                <div class="col-md-3 no-padding-right">
                    <ul class="all-category">
                        {for $categorys in $menu}
                            <li>
                                <a href="#menu{$categorys.product_category_id}" data-toggle="tab"><h3>{$categorys.title}</h3></a>
                                {if($categorys.sub!='')}
                                    <div class="mega_menu">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <div class="col-mega-menu">
                                                    <h4>Danh mục</h4>
                                                    {$categorys.sub}
                                                </div>
                                                <div class="col-mega-menu">
                                                    <h4>Tác giả</h4>
                                                    <ul>
                                                    {for $writer in $categorys.author}
                                                        <li><a href="">{function}echo $author[$writer]['title']{/function}</a></li>
                                                    {/for}
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <img src="{$categorys.mega_image}" alt="{$categorys.title}" width="100%"/>
                                            </div>
                                        </div>
                                    </div>
                                {/if}
                            </li>
                        {/for}
                        <li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</div>