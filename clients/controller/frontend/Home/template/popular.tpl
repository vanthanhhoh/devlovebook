<div class="box-highlight">
    <div class="container">
        <div class="box-title">
            <div class="row">
                <div class="col-md-3 no-padding-right">
                    <h2 class="box-title-text">Sách bán chạy trong tuần</h2>
                </div>
                <div class="col-md-9" style="padding-left: 0">
                    <div class="box-title-line"></div>
                </div>
            </div>
        </div>
        <div class="carousel-book">
            <div class="carousel-book-horizontal">

                <div class="book-item">
                    <div class="book-img pull-left">
                        <img src="images/90%20anh%201.JPG" alt="" width="100%"/>
                    </div>
                    <div class="book-infomation pull-left">
                        <div class="book-title">Chinh phục đề thi THPT Quốc gia môn Tiếng Anh - Tập 1 - Mã: BL8136</div>
                        <div class="book-author">Tác giả: Nhiều tác giả</div>
                        <div class="book-price">
                            <span class="newprice">108.000 đ</span>
                            <span class="oldprice">158.000 đ</span>
                        </div>
                        <div class="book-preview">
                            <div class="star-only" data-number="5" data-score="3"></div>
                            <span class="count-rate">(có 3 đánh giá)</span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="book-active">
                            <a href="" class="book-active-button read-frist">Đọc thử</a>
                            <a href="" class="book-active-button buy-now">Mua ngay</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>