<?php

define('BLOCK_PATH', APPLICATION_PATH . 'blocks' . DIRECTORY_SEPARATOR);

class frontend extends Controller {

    static $product = array();
    static $product_category = array();
    static $product_group = array();
    static $author = array();
    static $publisher;
    static $option;
    static $menu;
    static function Init() {
//        n($_SESSION['cart']);
        Theme::UseJquery();
        Option::getOptionsOfType();
        Option::getOptionsOfType('theme', CURRENT_THEME);
        $op = Option::getPairs();
        self::$option = $op;
        Theme::Assign('Op', $op);
        Theme::Assign('ROUTE_NAME', G::$Route['name']);
        Theme::jsPlugin('fancyBox');
        Theme::jsPlugin('datepicker');
        self::$product = DB::Query('product')->Get('product_id')->Result;
        self::$product_category = DB::Query('product_category')->Get('product_category_id')->Result;
        self::$product_group = DB::Query('product_group')->Get()->Result;
        self::$author = DB::Query('author')->Get('author_id')->Result;
        self::$publisher = DB::Query('publisher')->Get('publisher_id')->Result;

        $menu = Output::MenuGenerator(self::$product_category,'product_category_id','parent_id');
        foreach($menu as $key => $item){
            $child = clients::GetChildID(self::$product_category,$item['product_category_id']);
            $menu[$key]['author'] = array();
            foreach(self::$product as $pitem){
                if(in_array($pitem['product_category'],$child)){
                    $menu[$key]['author'][] = $pitem['author'];
                }
            }
            $menu[$key]['author'] = array_unique($menu[$key]['author']);
        }
        self::$menu = $menu;
        Theme::Assign('menu',$menu);
        Theme::Assign('author',self::$author);

    }
    static function Complete() {
    }

}


?>