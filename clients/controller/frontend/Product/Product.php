<?php
/**
 * Created by PhpStorm.
 * User: THANH
 * Date: 01/07/2016
 * Time: 9:28 CH
 */

class Product extends Controller {
    public function Category(){
        $caturl = G::$Route['params']['category'];
        $category = DB::Query('product_category')->Where('url','=',$caturl)->Get();
        if($category->status && $category->num_rows==1){
            $Filter = array(
                'show' => 24,
                'sort' => 'add_time|DESC',
                'mode' => 'gird',
                'filter' => 0
            );
            $FromForm = Input::Post('Filter', array());
            $Filter = array_merge($Filter, $FromForm);
            if($Filter['filter']==1){
                $url = Router::GenerateThisRoute();
                $page_status = Output::$Paging;
                if($page_status['current_page']>0){
                    $url = $url.'page'.$page_status['bridge'].$page_status['current_page'].$page_status['ext'];
                }
                $url = $url.'?show='.$Filter['show'].'&sort='.$Filter['sort'].'&mode='.$Filter['mode'];
                header('Location:'.$url);
            }
            if(Router::$Router->BackupGetParams!=''){
                $Param =  array(
                  'show' => Router::get('show'),
                  'sort' => Router::get('sort'),
                  'mode' => Router::get('mode')
                );
                $Filter = array_merge($Filter,$Param);
            }
            $Filter['sort'] = explode('|',$Filter['sort']);
            $category = $category->Result[0];
            Theme::SetTitle($category['title']);
            Helper::State($category['title'],Router::GenerateThisRoute());
            $tree = Output::MenuGeneratorDeep(frontend::$product_category,'product_category_id','parent_id');
            $child = clients::GetChildID(frontend::$product_category,$category['product_category_id']);
            $productQ = DB::Query('product')
                ->Where('product_category','IN',$child)
                ->_OR()
                ->Where('product_sub_category','INCLUDE',$category['product_category_id'])
                ->Order($Filter['sort'][0],$Filter['sort'][1])
                ->Limit($Filter['show'])
                ->Get('product_id',Output::Paging());
            $num = $productQ->num_rows;
            $product = $productQ->Result;
            $v = $this->View('category')
                      ->Assign('category',$category)
                      ->Assign('cat',frontend::$product_category)
                      ->Assign('tree',$tree)
                      ->Assign('num',$num)
                      ->Assign('product',$product)
                      ->Assign('author',frontend::$author)
                      ->Assign('Filter',$Filter)
                      ->Assign('Pg',Output::$Paging)
                      ->Assign('getbackup',Router::$Router->BackupGetParams)
                      ->Assign('currenturl',Router::GenerateThisRoute())
                      ->Output();
            $this->Render($v);
        }
        else {
            Helper::Notify('error','Đường dẫn không tồn tại !');
        }
    }
    public function Author(){
        $authorUrl = G::$Route['params']['author'];
        $author = DB::Query('author')->Where('url','=',$authorUrl)->Get();
        if($author->status && $author->num_rows==1){
            $Filter = array(
                'show' => 24,
                'sort' => 'add_time|DESC',
                'mode' => 'gird',
                'filter' => 0
            );
            $FromForm = Input::Post('Filter', array());
            $Filter = array_merge($Filter, $FromForm);
            if($Filter['filter']==1){
                $url = Router::GenerateThisRoute();
                $page_status = Output::$Paging;
                if($page_status['current_page']>0){
                    $url = $url.'page'.$page_status['bridge'].$page_status['current_page'].$page_status['ext'];
                }
                $url = $url.'?show='.$Filter['show'].'&sort='.$Filter['sort'].'&mode='.$Filter['mode'];
                header('Location:'.$url);
            }
            if(Router::$Router->BackupGetParams!=''){
                $Param =  array(
                    'show' => Router::get('show'),
                    'sort' => Router::get('sort'),
                    'mode' => Router::get('mode')
                );
                $Filter = array_merge($Filter,$Param);
            }
            $Filter['sort'] = explode('|',$Filter['sort']);
            $author = $author->Result[0];
            Theme::SetTitle($author['title']);
            Helper::State($author['title'],Router::GenerateThisRoute());
            $tree = Output::MenuGeneratorDeep(frontend::$product_category,'product_category_id','parent_id');
            $productQ = DB::Query('product')
                ->Where('author','=',$author['author_id'])
                ->Order($Filter['sort'][0],$Filter['sort'][1])
                ->Limit($Filter['show'])
                ->Get('product_id',Output::Paging());
            $num = $productQ->num_rows;
            $product = $productQ->Result;
            $v = $this->View('author')
                ->Assign('cat',frontend::$product_category)
                ->Assign('tree',$tree)
                ->Assign('num',$num)
                ->Assign('product',$product)
                ->Assign('author',frontend::$author)
                ->Assign('aut',$author)
                ->Assign('Filter',$Filter)
                ->Assign('Pg',Output::$Paging)
                ->Assign('getbackup',Router::$Router->BackupGetParams)
                ->Assign('currenturl',Router::GenerateThisRoute())
                ->Output();
            $this->Render($v);
        }
        else {
            Helper::Notify('error','Đường dẫn không tồn tại !');
        }
    }
    public function Detail(){
        $purl = G::$Route['params']['product'];
        $pid  = G::$Route['params']['pid'];

        $product = DB::Query('product')->Where('url','=',$purl)->_AND()->Where('product_id','=',$pid)->Get();
        if($product->status && $product->num_rows==1){
            $product = $product->Result[0];
            Theme::SetTitle($product['title']);
            Theme::MetaTag('description',Filter::SubString($product['discription'],160));
            Theme::MetaTag('keywords','lovebook.vn, sách ôn thi đại học, sach on thi dai hoc, sach thu khoa, sách thủ khoa');
            Theme::MetaProperty('og:image',$product['image']);
            Theme::MetaProperty('og:description',Filter::SubString($product['discription'],160));
            Theme::MetaProperty('og:title',$product['title']);
            $cat = frontend::$product_category;
            $category = $cat[$product['product_category']];
            $parent = $this->getParentID($cat,$category['parent_id'],$category['product_category_id']);
            Helper::State($cat[$parent]['title'],Router::Generate('ProductCategory',array('category'=>$cat[$parent]['url'])));
            Helper::State($category['title'],Router::Generate('ProductCategory',array('category'=>$category['url'])));
            Helper::State($product['title'],Router::GenerateThisRoute());


            $khuyenmai = unserialize($product['gift']);
            $levelReview= array(
                1=>0,
                2=>0,
                3=>0,
                4=>0,
                5=>0
            );
            $favorite = DB::Query('favorite')->Where('product_id','=',$pid)->_AND()->Where('user_id','=',USER_ID)->Get()->num_rows;
            $review = DB::Query('review')->Where('product_id','=',$pid)->_AND()->Where('status','=',1)->Limit(10)->Get('review_id',Output::Paging())->Result;
            foreach($review as $rw){
                if(isset($levelReview[$rw['rate']])){
                    $levelReview[$rw['rate']] +=1;
                }
            }
            $Users = DB::Query('users')->Columns('userid,fullname,email,avatar')->Get('userid')->Result;
            $v = $this->View('detail')
                ->Assign('author',frontend::$author)
                ->Assign('public',frontend::$publisher)
                ->Assign('khuyenmai',$khuyenmai)
                ->Assign('product',$product)
                ->Assign('review',$review)
                ->Assign('favorite',$favorite)
                ->Assign('Pg',Output::$Paging)
                ->Assign('level',$levelReview)
                ->Assign('User',$Users)
                ->Assign('p',frontend::$product);
            $this->Render($v->Output());
        }
        else {
            Helper::Notify('error','Đường dẫn không tồn tại !');
        }
    }
    public function getParentID($array,$parent_id,$id){
        if($parent_id>0){
            $parent_ = $array[$parent_id]['parent_id'];
            $id_ = $array[$parent_id]['product_category_id'];
            return getParentID($array,$parent_,$id_);
        }
        else {
            return $array[$id]['product_category_id'];
        }
    }
    public function Review(){
        $purl = G::$Route['params']['product'];
        $pid  = G::$Route['params']['pid'];
        $product = DB::Query('product')->Where('url','=',$purl)->_AND()->Where('product_id','=',$pid)->Get();
        if($product->status && $product->num_rows==1) {
            $product = $product->Result[0];
            $same = DB::Query('product')
                ->Where('author','=',$product['author'])
                ->_AND()->Where('product_id','<>',$pid)
                ->Limit(1)
                ->Get()->Result;
            $v = $this->View('review')
                ->Assign('author',frontend::$author)
                ->Assign('same',$same)
                ->Assign('product',$product);
            $this->Render($v->Output(),array('layout'=>'layout.review'));
        }
    }
}