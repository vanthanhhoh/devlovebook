<div class="category">
    <div class="container">
        <div class="row">
            <div class="col-md-3 no-padding-right hidden-xs">
                <div class="category-left">
                    <div class="box-panel">
                        <div class="box-panel-title">
                            Danh mục sách
                        </div>
                        <div class="box-panel-body">
                            <ul class="categories-accordion">
                                {for $categorys in $tree}
                                    {function}$url=Router::Generate('ProductCategory',array('category'=>$categorys['url'])){/function}
                                    <li class="has-children active">
                                        <a href="{$url}" title="{$categorys.title}">{$categorys.title}</a>
                                        {$categorys.sub}
                                    </li>
                                {/for}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="category-right">

                    <h1 class="category-title">{$aut.title} ({$num})</h1>

                    <div class="filter">
                        <form action="" method="post" id="filter">
                            <div class="view-box pull-right">
                                <div class="btn-group pull-left sort-box page-box hidden-xs">
                                    <select name="Filter[show]" class="filter-input" onchange="submitFilter()">
                                        <option value="16" {if($Filter.show==16)}selected="selected"{/if}>16</option>
                                        <option value="24" {if($Filter.show==24)}selected="selected"{/if}>24</option>
                                        <option value="32" {if($Filter.show==32)}selected="selected"{/if}>32</option>
                                    </select>
                                </div>

                                <div class="btn-group btn-group-sm">
                                    <input type="hidden" name="Filter[mode]" value="gird" id="Mode"/>
                                    <button title="Lưới" type="button" class="product-mode-grid btn btn-default {if($Filter.mode=='gird')}active{/if}" onclick="changeMode($(this))" data-value="gird"><i class="fa fa-th"></i></button>
                                    <button title="Danh sách" type="button" class="product-mode-list btn btn-default {if($Filter.mode=='list')}active{/if}" onclick="changeMode($(this))" data-value="list"><i class="fa fa-th-list"></i></button>
                                </div>
                            </div>
                            <div class="sort-box-holder pull-left" onchange="submitFilter()">
                                <select name="Filter[sort]" class="filter-input">
                                    <option value="price|ASC" {if($Filter.sort.0=='price' && $Filter.sort.1=='ASC')} selected="selected" {/if}>Giá tăng dần</option>
                                    <option value="price|DESC" {if($Filter.sort.0=='price' && $Filter.sort.1=='DESC')} selected="selected" {/if}>Giá giảm dần</option>
                                    <option value="title|ASC" {if($Filter.sort.0=='title' && $Filter.sort.1=='ASC')} selected="selected" {/if}>A-Z</option>
                                    <option value="add_time|DESC" {if($Filter.sort.0=='add_time' && $Filter.sort.1=='DESC')} selected="selected" {/if}>Mới phát hành</option>
                                    <option value="price_sale|DESC" {if($Filter.sort.0=='price_sale' && $Filter.sort.1=='DESC')} selected="selected" {/if}>Giảm giá nhiều nhất</option>
                                </select>
                                <input type="hidden" name="Filter[filter]"  value="1"/>
                            </div>
                        </form>
                    </div>

                    <div class="listbook">
                        <div class="listbook-warpper" {if($Filter.mode=='list')}id="no-wapper"{/if}>
                            {for $item in $product}
                                {function}
                                    $au = $author[$item['author']];
                                    $url = Router::Generate('ProductDetail',array('product'=>$item['url'],'pid'=>$item['product_id']))
                                {/function}
                                <div class="book-item-category" {if($Filter.mode=='list')}id="list-hoz"{/if}>
                                    <a href="{$url}" title="{$item.title}">
                                        <div class="book-image">
                                            <img src="{$item.image|Output::GetThumbLink:400,600}" width="100%" alt="{$item.title}"/>
                                        </div>
                                        <div class="book-category-info">
                                            <h3>{$item.title}</h3>
                                            <p class="book-author-category">Tác giả: {$au.title}</p>
                                            <div class="book-price-category">
                                                <div class="pull-left price_rule">
                                                    {if($item.price_sale>0)}
                                                        <span class="newprice">{$item.price_sale|Filter::NumberFormat} đ</span>
                                                        <span class="oldprice">{$item.price|Filter::NumberFormat} đ</span>
                                                    {else}
                                                        <span class="newprice">{$item.price|Filter::NumberFormat} đ</span>
                                                    {/if}
                                                </div>
                                                {if($item.price_sale>0)}
                                                    {function}
                                                        $disc = $item['price_sale']/$item['price']; $disc = ceil($disc*100);
                                                        $disc = 100-$disc;
                                                    {/function}
                                                    <div class="pull-left price_discount_show">
                                                        <span class="discountceil">GIẢM GIÁ: {$disc}%</span>
                                                    </div>
                                                {/if}
                                            </div>
                                            <div class="description">{$item.discription}</div>
                                            <div class="clearfix"></div>
                                            <div class="add_cart">
                                                <i class="fa fa-shopping-cart" aria-hidden="true"></i> Mua ngay
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            {/for}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="pagging">
                        <div id="page-selection"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{#APPLICATION_DATA_DIR}theme/{#CURRENT_THEME}/js/jquery.bootpag.min.js"></script>
<script>
    function submitFilter(){
        $('#filter').submit();
    }
    function changeMode(e){
        var va = e.data('value');
        $('#Mode').val(va);
        submitFilter();
    }
    {if($Pg.total_pages>1)}
    $('#page-selection').bootpag({
        total: {$Pg.total_pages},
        page: {$Pg.current_page+1},
        maxVisible: 5,
        next: '<i class="fa fa-chevron-right" aria-hidden="true"></i>',
        prev: '<i class="fa fa-chevron-left" aria-hidden="true"></i>'
    }).on('page', function(event, num){
        window.location.href = '{$currenturl}page-'+num+'/?{$getbackup}';
    })
    {/if}
</script>

