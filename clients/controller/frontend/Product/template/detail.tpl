<div class="detail-book">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="book-gallery">
                    <img src="{$product.image}" width="100%" alt="{$product.title}"/>
                    <a href="{function}echo Router::Generate('ProductReview',array('product'=>$product['url'],'pid'=>$product['product_id'])){/function}" class="fancybox">Đọc thử</a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="detail-center">
                    <h1>{$product.title}</h1>
                    <p class="masp">Mã sản phẩm: <span itemprop="sku">{$product.code}</span></p>
                    <p class="sku">SKU: <span itemprop="sku">{$product.sku}</span></p>
                    <p class="p_book">Tác giả:
                        <a href="/tac-gia/{function}echo $author[$product['author']]['url']{/function}/">{function}echo $author[$product['author']]['title']{/function}</a>
                    </p>
                    <p class="p_book">Nhà xuất bản:
                        <a href="">{function}if($product['publisher']>0) echo $public[$product['publisher']]['title']{/function}</a>
                    </p>
                    <p class="p_store">Tình trạng kho:
                        Còn hàng
                    </p>
                    <div class="addthis_native_toolbox"></div>
                    <p class="p_description">
                        {$product.discription}
                    </p>
                    <div class="hr-wrapper">
                        <hr><span>Khuyến mãi</span>
                    </div>
                    <div class="promotion">
                        <div class="row">
                            {if(!empty($khuyenmai))}
                            <div class="col-md-6">
                                <div class="khuyenmai">
                                    {for $item in $khuyenmai}
                                        {function}$pitem= $p[$item['product_id']]{/function}
                                        <div class="km_item">
                                            <div class="pull-left km_item_img">
                                                <img src="{$pitem.image|Output::GetThumbLink:150,150}" alt="{$pitem.title}"/>
                                            </div>
                                            <div class="pull-left km_item_description">
                                                <p class="km_label">Quà tặng miễn phí</p>
                                                <p class="km_title">{$pitem.title}</p>
                                                <p class="km_qty">Số lượng: {$item.qty}</p>
                                                <p class="km_trigia">Trị giá: {$pitem.price|Filter::NumberFormat} đ</p>
                                            </div>
                                        </div>
                                    {/for}
                                </div>
                            </div>
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="thongtinthanhtoan">
                    <div class="thanhtoanbox-title">
                        Thông tin mua hàng
                    </div>
                    <table class="cartbook_table table table-striped">
                        <tbody>
                        <tr>
                            <td>Giá bìa</td>
                            <td><p class="pricebook_bia">{$product.price|Filter::NumberFormat} đ</p></td>
                        </tr>
                        <tr>
                            <td>Giá bán</td>
                            <td>
                                <p class="pricebook_ban">
                                    {if($product.price_sale>0)}
                                    {$product.price_sale|Filter::NumberFormat} đ
                                {/if}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>Tiết kiệm</td>
                            <td>
                                <p class="pricebook_tk">
                                    {if($product.price_sale>0)}
                                    {function}
                                        $disc = $product['price_sale']/$product['price']; $disc = ceil($disc*100);
                                        $disc = 100-$disc;
                                    {/function}
                                    {function}echo Filter::NumberFormat($product['price']-$product['price_sale']){/function} đ
                                    ({$disc}%)
                                {/if}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">

                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <form action="" method="post" id="add_cart">
                        <div class="input-group spinner" id="numberInput">
                            <input type="text" class="form-control" value="1" name="qty" id="qty_addCart">
                            <div class="input-group-btn-vertical">
                                <button class="btn btn-default" type="button"><i class="fa fa-caret-up"></i></button>
                                <button class="btn btn-default" type="button"><i class="fa fa-caret-down"></i></button>
                            </div>
                        </div>
                        <button type="button" onclick="addCart({$product.product_id},$('#qty_addCart').val())"><span><i class="fa fa-shopping-cart"></i></span> Cho vào giỏ</button>
                    </form>
                    <div class="quickbuy">
                        <a class="" href="" onclick="quickBuy({$product.product_id},1)">Mua nhanh</a>
                    </div>
                    <div class="quickbuy">
                        {if($favorite==0)}
                            <button class="add-to-wishlist  is-css" type="button" onclick="addFavorite($(this))" data-product="{$product.product_id}">
                                    <span class="icon">
                                        <i class="fa fa-heart"></i>
                                    </span>
                                    <span class="text">
                                        Thêm Vào Yêu Thích
                                    </span>
                            </button>
                        {else}
                            <button class="add-to-wishlist is-css added" type="button">
                                    <span class="icon">
                                        <i class="fa fa-heart"></i>
                                    </span>
                                    <span class="text">
                                        Đã thêm vào yêu thích
                                    </span>
                            </button>
                        {/if}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <nav id="myScrollspy">
        <ul class="navigtor-scroll nav">
            <li class="active">
                <a class="page-scroll" href="#mota">Mô tả sách</a>
            </li>
            <li>
                <a class="page-scroll" href="#thongtin">Thông tin chi tiết</a>
            </li>

            <li>
                <a class="page-scroll" href="#nhanxet">Nhận xét</a>
            </li>
        </ul>
    </nav>
    <div id="mota" class="box-detail mota-section">
       {$product.content}
    </div>
    <div id="thongtin" class="box-detail thongtin-section">
        <h2>Thông tin chi tiết</h2>
        <table id="chi-tiet" cellspacing="0" class="table table-bordered table-detail table-striped">
            <colgroup>
                <col style="width: 25%;"><col>
            </colgroup>
            <tbody>
            <tr>
                <td>Mã sản phẩm</td>
                <td class="last">
                    {$product.code}
                </td>
            </tr>
            <tr>
                <td>SKU</td>
                <td class="last">
                    {$product.sku}
                </td>
            </tr>
            <tr>
                <td>Nhà xuất bản</td>
                <td class="last">
                    <a href="">{function}if($product['publisher']>0) echo $public[$product['publisher']]['title']{/function}</a>
                </td>
            </tr>
            <tr>
                <td>Tác giả</td>
                <td class="last">
                    <a href="">{function}echo $author[$product['author']]['title']{/function}</a>
                </td>
            </tr>
            {function}$properties = unserialize($product['properties']){/function}
            {for $prope in $properties}
                <tr>
                    <td>{$prope.name}</td>
                    <td class="last">
                        {$prope.value}
                    </td>
                </tr>
            {/for}
            </tbody>
        </table>
    </div>

    <div id="nhanxet" class="box-detail thongtin-section">
        <h2>Khách hàng nhận xét</h2>
        <div class="product-review-sumary">
            <div class="review-sumary-content">
                <div class="row">
                    <div class="col-md-3">
                        <div class="summary-Col">
                            <h4>Đánh giá trung bình</h4>
                            {function}
                                if($product['total_rates']>0) {
                                $rateAvg = ceil($product['rate_count']/$product['total_rates']);
                                }
                                else{
                                $rateAvg=0;
                                }
                            {/function}
                            <p class="total-review-point">{$rateAvg}/5</p>
                            <div class="item-rating" style="text-align: center">
                                <p class="rating">
                                    {function}
                                        for($i=1;$i<=5;$i++){
                                        if($i<=$rateAvg){
                                        echo '<i class="fa fa-star on" aria-hidden="true"></i>';
                                                                                              }
                                                                                              else {
                                                                                              echo '<i class="fa fa-star" aria-hidden="true"></i>';
                                        }
                                        }
                                    {/function}
                                </p>
                                <p class="comments-count">({$product.total_rates} nhận xét)</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="summary-Col">
                            <div class="rate-proccess">
                                <!-- RATING PROGRESS BAR -->
                                <div class="item">
                                    <span class="rating-num">5 sao</span>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success" style="width: {function}if($product['total_rates']>0) {echo ($level[5]/$product['total_rates'])*100 ;}else{echo "0";} {/function}%;">
                                            <span class="sr-only">95% Complete</span>
                                        </div>
                                    </div>
                                    <span class="rating-num-total">{$level.5}</span>
                                </div>
                                <div class="item">
                                    <span class="rating-num">4 sao</span>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success" style="width: {function}if($product['total_rates']>0) {echo ($level[4]/$product['total_rates'])*100 ;}else{echo "0";} {/function}%;">
                                            <span class="sr-only">5% Complete</span>
                                        </div>
                                    </div>
                                    <span class="rating-num-total">{$level.4}</span>
                                </div>
                                <div class="item">
                                    <span class="rating-num">3 sao</span>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success" style="width: {function}if($product['total_rates']>0) {echo ($level[3]/$product['total_rates'])*100 ;}else{echo "0";} {/function}%;">
                                            <span class="sr-only">0% Complete</span>
                                        </div>
                                    </div>
                                    <span class="rating-num-total">{$level.3}</span>
                                </div>
                                <div class="item">
                                    <span class="rating-num">2 sao</span>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success" style="width: {function}if($product['total_rates']>0) {echo ($level[2]/$product['total_rates'])*100 ;}else{echo "0";} {/function}%;">
                                            <span class="sr-only">0% Complete</span>
                                        </div>
                                    </div>
                                    <span class="rating-num-total">{$level.2}</span>
                                </div>
                                <div class="item">
                                    <span class="rating-num">1 sao</span>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success" style="width: {function}if($product['total_rates']>0) {echo ($level[1]/$product['total_rates'])*100 ;}else{echo "0";} {/function}%;">
                                            <span class="sr-only">0% Complete</span>
                                        </div>
                                    </div>
                                    <span class="rating-num-total">{$level.1}</span>
                                </div>
                                <!-- END RATING PROGRESS BAR -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="summary-Col" style="border: none">
                            <h4>Chia sẻ nhận xét về sản phẩm</h4>
                            <button type="button" class="btn btn-default js-customer-button" id="wirteReview" data-status="close">
                                Viết nhận xét của bạn
                            </button>
                        </div>
                    </div>
                </div>

                <div class="form_review">
                    <h2>Gửi nhận xét của bạn</h2>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form_review_content">
                                <script src="{#APPLICATION_DATA_DIR}theme/{#CURRENT_THEME}/js/jquery.validate.min.js"></script>
                                <form action="" method="post" id="review">
                                    <div class="rate">
                                        <label>1. Đánh giá của bạn về sản phẩm này:</label>
                                        <div class="rate_jquery"></div>
                                        <input type="hidden" name="review[rate]" value="" id="score"/>
                                    </div>
                                    <div class="title_review">
                                        <label>
                                            2. Tiêu đề của nhận xét:
                                        </label>
                                        <input type="text" name="review[title]" id="title_review" class="form-control" required=""/>
                                    </div>
                                    <div class="message_review">
                                        <label>
                                            3. Cảm nhận của bạn về sản phẩm
                                        </label>
                                        <textarea class="form-control" name="review[comment]" id="comment_view" rows="5" minlength="100" required=""></textarea>
                                    </div>
                                    <div class="text-right">
                                        <input type="hidden" name="submitReview" value="1"/>
                                        <input type="hidden" name="product_id" id="product_id" value="{$product.product_id}"/>
                                        <button type="button" class="submitReview" id="submitReview" onclick="sendReview()">Gửi nhận xét</button>
                                    </div>
                                </form>
                                <script>
                                    $(document).ready(function() {
                                        $('#review').validate({
                                            errorElement: "span"
                                        });
                                        $('#wirteReview').click(function() {
                                            var status = $(this).data('status');
                                            if(status=='close'){
                                                $('.form_review').show();
                                                $(this).data('status','open');
                                                $(this).html('Đóng bình luận');
                                            }
                                            else {
                                                $('.form_review').hide();
                                                $(this).data('status','close');
                                                $(this).html('Viết nhận xét của bạn');
                                            }
                                        })
                                    })
                                </script>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="introduce_review">
                                {function}
                                    echo Option::get('review_route','');
                                {/function}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <h2>Nhận xét hữu ích</h2>

        <div class="list_review_right">
            {for $reviews in $review}
                <div class="review_item" itemprop="itemReviewed" itemtype="http://schema.org/Product">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="author_review">
                                {function}
                                    $author = $User[$reviews['user_id']];
                                {/function}
                                <img src="{$author.avatar}" alt="{$author.fullname}" width="80px" height="80px"/>
                                <p class="name" itemprop="author">{$author.fullname}</p>
                                <p class="days">{$reviews.add_time|Filter::UnixTimeToFullDate:true}</p>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="review_infomation">
                                <div class="rating">
                                    <div itemprop="reviewRating" itemtype="http://schema.org/Rating">
                                        <meta itemprop="ratingValue" content="{$reviews.rate}">
                                    </div>
                                    <p class="rating">
                                        {function}
                                            for($i=1;$i<=5;$i++){
                                            if($i<=$reviews['rate']){
                                            echo '<i class="fa fa-star on" aria-hidden="true"></i>';
                                                                                                  }
                                                                                                  else {
                                                                                                  echo '<i class="fa fa-star" aria-hidden="true"></i>';
                                            }
                                            }
                                        {/function}
                                    </p>
                                    <p class="review" itemprop="name">{$reviews.title}</p>
                                </div>
                                <div class="review_detail" itemprop="reviewBody">
                                    {$reviews.comment}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            {/for}
        </div>
        <div id="page-selection"></div>
    </div>
</div>
<script src="{#APPLICATION_DATA_DIR}theme/{#CURRENT_THEME}/js/jquery.bootpag.min.js"></script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56ae44f40c56fd06"></script>
<script>
    (function ($) {
        $('.spinner .btn:first-of-type').on('click', function() {
            $('.spinner input').val( parseInt($('.spinner input').val(), 10) + 1);
        });
        $('.spinner .btn:last-of-type').on('click', function() {
            $('.spinner input').val( parseInt($('.spinner input').val(), 10) - 1);
        });
    })(jQuery);
    $(document).ready(function(){
        var container = $('.container').width();
        $(window).scroll(function() {
            if ($(window).scrollTop() > 650) {
                $(".navigtor-scroll").addClass("fix");
                $(".navigtor-scroll").css("width",container+"px");
            } else {
                $(".navigtor-scroll").removeClass("fix");
            }
        });
        {if($Pg.total_pages>1)}
            $('#page-selection').bootpag({
                total: {$Pg.total_pages},
                page: {$Pg.current_page+1},
                maxVisible: 5,
                next: '<i class="fa fa-chevron-right" aria-hidden="true"></i>',
                prev: '<i class="fa fa-chevron-left" aria-hidden="true"></i>'
            }).on('page', function(event, num){
                VNP.Ajax({
                    url:'Review/getReview',
                    type: 'post',
                    data:{page:num,product:{$product.product_id}},
                    success: function(res){
                        $('.list_review_right').html(res);
                        VNP.Loader.hide();
                    }
                },'text')
            });
        {/if}
    })
</script>
<script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Product",
  "name": "{$product.title}",
  "image": "{$product.image}",
  "description": "{$product.discription}",
  "brand": {
    "@type": "Thing",
    "name": "Lovebook"
  },
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "{function}if($product['rate_count']>0) { echo ceil($product['rate_count']/$product['total_rates']) ;}else{ echo "1";}{/function}",
    "reviewCount": "{$product.total_rates}"
  },
  "offers": {
    "@type": "Offer",
    "priceCurrency": "VND",
    "price": "{$product.price}",
    "priceValidUntil": "2016-07-29",
    "itemCondition": "http://schema.org/UsedCondition",
    "availability": "http://schema.org/InStock",
    "seller": {
      "@type": "Organization",
      "name": "Lovebook"
    }
  }
}
</script>