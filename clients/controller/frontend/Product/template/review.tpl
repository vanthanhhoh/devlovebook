<div class="review">
    <div class="header">
        <h2>Đọc thử sách {$product.title}</h2>
    </div>
    <div class="content-review">
        <div class="book-mark">
            <div class="sumary-book">
                <div class="pull-left image">
                    <img src="{$product.image}" width="100%" alt="{$product.title}"/>
                </div>
                <div class="pull-left infomation">
                    <h2>{$product.title}</h2>
                    <p class="code">Mã: {$product.code}</p>
                    <p class="author">Tác giả:
                        {function}
                            if($product['author']>0){
                            $author[$product['author']]['title'];
                            }
                        {/function}
                    </p>
                    <p class="giabia">Giá bìa: {$product.price|Filter::NumberFormat} đ</p>
                    <p class="giaban">
                        Giá bán:
                        {if($product.price_sale>0)}
                        {$product.price_sale|Filter::NumberFormat} đ
                            {else}
                            {$product.price|Filter::NumberFormat} đ
                        {/if}
                    </p>

                </div>
                <div class="clearfix"></div>
                <p class="description">
                    {$product.discription}
                </p>
                <div class="add_cart" style="cursor: pointer">
                    <a href="/Cart/addCart/{$product.product_id}" target="_parent"> <i class="fa fa-shopping-cart" aria-hidden="true"></i> Mua ngay</a>
                </div>
            </div>
            <hr>
            <div class="box-title">Sách cùng tác giả</div>
            <div class="apdaper">
                <ul class="crosual" id="crosual">
                    {for $item in $same}
                        {function}$url = Router::Generate('ProductDetail',array('product'=>$item['url'],'pid'=>$item['product_id'])){/function}
                    <li>
                        <a href="{$url}" target="_parent">
                            <div class="pull-left image">
                                <img src="{$item.image}" width="100%" alt="{$item.title}"/>
                            </div>
                            <div class="pull-left infomation">
                                <h2>{$item.title}</h2>
                                <p class="code">Mã: {$item.code}</p>
                                <p class="giabia">Giá bìa: {$item.price|Filter::NumberFormat} đ</p>
                                <p class="giaban">
                                    Giá bán:
                                    {if($item.price_sale>0)}
                                        {$item.price_sale|Filter::NumberFormat} đ
                                    {else}
                                        {$item.price|Filter::NumberFormat} đ
                                    {/if}
                                </p>

                            </div>
                        </a>
                    </li>
                    {/for}
                </ul>
            </div>
        </div>
        <div class="main-read">
            <iframe src="/pdfview/web/viewer.html?file={$product.trailer}" class="box-read" frameborder="0" width="100%" height="100%"/>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#crosual').lightSlider({
            item:1,
            loop:false,
            slideMove:1,
            easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
            speed:600
        });
    });
    function quickBuyRv(id,qty){
        $.ajax({
            url:'Cart/addCart',
            type: 'post',
            dataType:'json',
            data:{id:id,qty:qty},
            success: function(res){
                if(res.status==1){
                    window.location.href='/gio-hang.html';
                }
            }
        });
    }
</script>