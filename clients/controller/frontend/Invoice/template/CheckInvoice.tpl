<div class="tracking-order-form">
    <div class="container">
        <div class="tracking-inner text-center">
            <h1>Tra cứu theo mã đơn hàng</h1>
            <p>Điền vào các thông tin bên dưới để xem tình trạng vắn tắt của Đơn hàng</p>
        </div>
        <div class="tracking-input">
            <form action="" method="post" id="tracking-form">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Số điện thoại nhận hàng</label>
                            <input type="text" name="phone" class="form-control" required=""/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Mã đơn hàng</label>
                            <input type="text" name="code" class="form-control" required=""/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>&nbsp</label>
                            <input type="hidden" name="Tracking" value="1"/>
                            <button type="submit" class="checkTracking">Kiểm tra</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>