<?php
/**
 * Created by PhpStorm.
 * User: THANH
 * Date: 29/07/2016
 * Time: 3:23 CH
 */

class Invoice extends Controller {
    public function CheckInvoice(){
        Theme::SetTitle('Tra cứu trạng thái đơn hàng | lovebook.vn');
        Theme::MetaTag('description','Tra cứu trạng thái đơn hàng lovebook.vn');
        Theme::MetaTag('keywords','lovebook.vn');
        Helper::State('Tra cứu trạng thái đơn hàng',Router::GenerateThisRoute());

        $v = $this->View('CheckInvoice');
        if(Input::Post('Tracking',0)==1){
            $phone = Input::Post('phone','');
            $code  = Input::Post('code','');
            $invoice = DB::Query('invoice')->Where('code','=',$code)->_AND()->Where('phone','=',$phone)->Get();
            if($invoice->status && $invoice->num_rows==1){
                $invoice = $invoice->Result[0];
                $status = array(
                    1=>'Đang chờ xác nhận',
                    2=>'Đang chờ thanh toán',
                    3=>'Đã thanh toán online',
                    4=>'Đã xác nhận',
                    5=>'Đang vận chuyển',
                    6=>'Thành công'
                );
                Helper::Notify('success','Trạng thái đơn hàng :'. $invoice['code'].' : '.$status[$invoice['invoice_status']]);
            }
            else {
                Helper::Notify('error','Đơn hàng không tồn tại!');
            }
        }
        $this->Render($v->Output());
    }
}