<?php
/**
 * Created by PhpStorm.
 * User: THANH
 * Date: 29/07/2016
 * Time: 5:29 CH
 */

class Search extends Controller {
    public function Main(){
        Theme::SetTitle('Tìm kiếm | lovebook.vn');
        Theme::MetaTag('description','Tìm kiếm sản phẩm trên lovebook.vn');
        Helper::State('Tìm kiếm',APP_DOMAIN.'/tim-kiem.html');
        $keywork = Router::get('s','');
        $keywork = urldecode($keywork);

        $Filter = array(
            'show' => 24,
            'sort' => 'add_time|DESC',
            'mode' => 'gird',
            'filter' => 0,
            's'=>$keywork
        );
        $FromForm = Input::Post('Filter', array());
        $Filter = array_merge($Filter, $FromForm);
        if($Filter['filter']==1){
            $url = Router::GenerateThisRoute();
            $page_status = Output::$Paging;
            if($page_status['current_page']>0){
                $url = $url.'page'.$page_status['bridge'].$page_status['current_page'].$page_status['ext'];
            }
            $url = $url.'?s='.urlencode($keywork).'&show='.$Filter['show'].'&sort='.$Filter['sort'].'&mode='.$Filter['mode'];
            header('Location:'.$url);
        }
        if(Router::get('show','')!='' && Router::get('sort','')!='' && Router::get('mode','')!=''){
            $Param =  array(
                'show' => Router::get('show'),
                'sort' => Router::get('sort'),
                'mode' => Router::get('mode')
            );
            $Filter = array_merge($Filter,$Param);
        }
        $Filter['sort'] = explode('|',$Filter['sort']);

        $tree = Output::MenuGeneratorDeep(frontend::$product_category,'product_category_id','parent_id');
        $productQ = DB::Query('product')
            ->Where('title','LIKE',$Filter['s'])
            ->Order($Filter['sort'][0],$Filter['sort'][1])
            ->Limit($Filter['show'])
            ->Get('product_id',Output::Paging());
        $num = $productQ->num_rows;
        $product = $productQ->Result;
        $v = $this->View('main')
            ->Assign('cat',frontend::$product_category)
            ->Assign('tree',$tree)
            ->Assign('num',$num)
            ->Assign('product',$product)
            ->Assign('author',frontend::$author)
            ->Assign('Filter',$Filter)
            ->Assign('keyword',$keywork)
            ->Assign('Pg',Output::$Paging)
            ->Assign('getbackup',Router::$Router->BackupGetParams)
            ->Assign('currenturl',Router::GenerateThisRoute())
            ->Output();
        $this->Render($v);
    }
}