<?php
/**
 * Created by PhpStorm.
 * User: THANH
 * Date: 23/07/2016
 * Time: 3:36 CH
 */

class Favorite extends Controller {
    public function addFavorite(){
        $result = array(
            'status'=>0,
            'message'=>''
        );
        if(USER_ID>0){
            $product = Input::Post('product',0);
            $favorite = DB::Query('favorite')->Where('user_id','=',USER_ID)
                        ->_AND()->Where('product_id','=',$product)->Get();
            if($favorite->status && $favorite->num_rows==0){
                $insert = DB::Query('favorite')->Insert(array('product_id'=>$product,'user_id'=>USER_ID,'favorite_time'=>CURRENT_TIME));
                if($insert->status && $insert->insert_id>0){
                    $result['status']=1;$result['message']='Bạn đã thêm vào danh sách yêu thích của bạn';
                }
            }
            elseif($favorite->status && $favorite->num_rows>0){
                $result['message']='Sản phẩm đã có trong danh sách yêu thích của bạn';
            }
            else{
                $result['message']='Lỗi kết nối CSDL! Xin vui lòng thử lại ở 1 thời điểm khác';
            }
        }
        else {
            $result['status'] = -1;
            $result['message']='Bạn cần đăng nhập hoặc đăng kí tài khoản để bình luận';
        }
        echo json_encode($result);die();
    }
    public function removeFavorite(){
        $result = array(
            'status'=>0,
            'message'=>''
        );
        if(USER_ID>0){
            $id = Input::Post('id',0);
            $delete = DB::Query('favorite')->Where('favorite_id','=',$id)->_AND()->Where('user_id','=',USER_ID)->Delete();
            if($delete->status ){
                $result['status']=1;
                $result['message'] ='Đã xóa thành công khỏi yêu thích';
            }
            else {
                $result['message'] ='Lỗi kết nối CSDL! Xin vui lòng thử lại ở 1 thời điểm khác';
            }
        }
        else {
            $result['status'] = -1;
            $result['message']='Bạn cần đăng nhập hoặc đăng kí tài khoản để bình luận';
        }
        echo json_encode($result);die();
    }
}