<a class="sidr-class-sidr-button-close" onclick="closeSdir()" >Close</a>
<div class="nav_left_header">
    {if(USER_ID>0)}
        <img src="{$User.avatar}" alt="{$User.email}" width="50" height="50" class="nav_left_avatar"/>
        <div class="nav_user_name">Xin chào ! <strong><a href="/Account/Profile">{$User.fullname} <i class="fa fa-angle-right" aria-hidden="true"></i></a> </strong></div>
        <a href="/logout" class="nav_logout">Đăng xuất</a>
    {else}
        <img src="{$Op.logomobile}" alt="lovebook" class="logo_nav_left"/>
        <ul>
            <li>
                <a data-toggle="modal" data-target="#login">
                    Đăng nhập
                </a>
            </li>
            <li>
                <a data-toggle="modal" data-target="#registerModal">
                    Đăng kí
                </a>
            </li>
        </ul>
    {/if}
</div>
<div class="list_menu_nav">
    <ul>
        {for $categorys in $menu}
            <li>
                <a href="#nav_left{$categorys.product_category_id}" data-toggle="tab"><h3>{$categorys.title}</h3></a>
                {$categorys.sub}
            </li>
        {/for}
    </ul>
</div>