<?php
/**
 * Created by PhpStorm.
 * User: THANH
 * Date: 25/06/2016
 * Time: 7:09 CH
 */

class ServiceSys extends Controller {

    public function suggetProduct(){
        $query = Input::Post('query','');
        $query = urldecode($query);
        $product = DB::Query('product')->Columns('product_id,title,code,price,price_sale')->Where('title','LIKE',$query)->Get()->Result;
        header('Content-type: application/json');
        echo json_encode($product);die();
    }

    public function getDistrict(){
        $province = Input::Post('province',0);
        $district = getLocation('district');
        $result = array();
        foreach($district as $item){
            if($item['province']==$province) $result[]=$item;
        }
        echo json_encode($result);die();
    }
    public function getCurrentDistrict(){
        if(Input::Post('province',0)!=0 && Input::Post('district',0)!=0){
            $result =array();
            $district = getLocation('district');
            foreach($district as $item){
                if($item['province']==Input::Post('province',0)) {
                    $result[]=$item;
                }
            }
            echo $this->View('get_current_district')->Assign('district',$result)->Assign('current',Input::Post('district',0))->Output();
            die();
        }
        if(isset($_SESSION['ship'])){
            $result =array();
            $district = getLocation('district');
            foreach($district as $item){
                if($item['province']==$_SESSION['ship']['province']) {
                    $result[]=$item;
                }
            }
            echo $this->View('get_current_district')->Assign('district',$result)->Assign('current',$_SESSION['ship']['district'])->Output();
            die();
        }
        if(isset($_SESSION['UserInfo'])){
            $result =array();
            $district = getLocation('district');
            foreach($district as $item){
                if($item['province']==$_SESSION['UserInfo']['province']) {
                    $result[]=$item;
                }
            }
            echo $this->View('get_current_district')->Assign('district',$result)->Assign('current',$_SESSION['UserInfo']['district'])->Output();
            die();
        }
    }
    public function searchSugget(){
        $result=array();
        $query = Router::get('query','');
        $product = DB::Query('product')->Where('title','LIKE',urldecode($query))->Columns('title,code,url,product_id,image')->Get()->Result;
        $i =1;
        foreach($product as $key => $item){
            $result[$i]['name'] = $item['title'];
            $result[$i]['type']    = 'Sách';
            $result[$i]['url']     = Router::Generate('ProductDetail',array('product'=>$item['url'],'pid'=>$item['product_id']));
            $i = $i+1;
        }
        $category = DB::Query('product_category')->Where('title','LIKE',urldecode($query))->Columns('title,url')->Get()->Result;
        foreach($category as $key => $item) {
            $result[$i]['name'] = $item['title'];
            $result[$i]['type']    = 'Danh mục';
            $result[$i]['url']     = Router::Generate('ProductCategory',array('category'=>$item['url']));
            $i = $i+1;
        }
        $author = DB::Query('author')->Where('title','LIKE',urldecode($query))->Columns('title,url')->Get()->Result;
        foreach($author as $key => $item) {
            $result[$i]['name'] = $item['title'];
            $result[$i]['type']    = 'Tác giả';
            $result[$i]['url']     = Router::Generate('ProductAuthor',array('author'=>$item['url']));
            $i = $i+1;
        }
        header('Content-type: application/json');
        echo json_encode($result);die();
    }
    public function ship_address(){
        $result = array(
            'status' => 0,
            'message'=>'Thất bại!'
        );
        if(USER_ID>0){
            $infomation = array(
                'name' => Input::Post('name',''),
                'address'=> Input::Post('address',''),
                'phone'  => Input::Post('phone',''),
                'province' => Input::Post('province',''),
                'district' => Input::Post('district'),
                'user_id'   => USER_ID
            );
            $check = DB::Query('shipaddress')->Where('user_id','=',USER_ID)->Get();
            if($check->status && $check->num_rows==1){
                $current = $check->Result[0];
                $update = DB::Query('shipaddress')->Where('user_id','=',USER_ID)
                              ->_AND()->Where('shipaddress_id','=',$current['shipaddress_id'])
                              ->Update($infomation);
                if($update->status){
                    $result['status']=1;$result['message'] = 'Đã ghi nhớ cho lần đặt hàng kế tiếp! Đây là địa chỉ giao hàng mặc định của bạn';
                }
            }
            elseif($check->num_rows==0){
                $add = DB::Query('shipaddress')->Insert($infomation);
                if($add->status && $add->insert_id>0){
                    $result['status']=1;$result['message'] = 'Đã nhớ thông tin nhận hàng';
                }
            }
        }
        echo json_encode($result);die();
    }
    public function suggetpageURL(){
        $query = Input::Post('q','');
        $query = urldecode($query);

        $Result = array();

        $page = DB::Query('page')->Where('title','LIKE',$query)->Get()->Result;

        foreach($page as $item){
            $Result[] = array(
                'type' => 'Trang',
                'title' => $item['title'],
                'url'  => Router::Generate('Page',array('page'=>$item['url']))
            );
        }
        header('Content-type: application/json');
        echo json_encode($Result);die();
    }
    public function generateMobilemenu(){
        $menu = frontend::$menu;
        $v = $this->View('mobile_menu')->Assign('Op',frontend::$option);
        if(USER_ID>0) $v = $v->Assign('User',$_SESSION['UserInfo']);
        $v  = $v->Assign('menu',$menu);
        echo $v->Output();die();
    }
}