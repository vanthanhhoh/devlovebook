<?php
/**
 * Created by PhpStorm.
 * User: THANH
 * Date: 18/07/2016
 * Time: 11:39 CH
 */

class Account extends Controller {
    public function __construct(){
        if(USER_ID==0) header('Location:'.APP_DOMAIN);
    }
    public function managerInvoice(){
        Theme::SetTitle('Đơn hàng của tôi | lovebook.vn');
        Helper::State('Đơn hàng của tôi',Router::GenerateThisRoute());
        $invoice = DB::Query('invoice')->Where('user_id','=',USER_ID)->Get()->Result;

        $v = $this->View('managerInvoice')
                  ->Assign('invoice',$invoice)
                  ->Assign('p',frontend::$product)
                  ->Assign('sidebar',$this->Sidebar());
        $this->Render($v->Output());
    }
    public function detailInvoice(){
        if(isset(G::$Registry['Params'][ROUTER_EXTRA_KEY])) {
            $id = G::$Registry['Params'][ROUTER_EXTRA_KEY];
            $invoice = DB::Query('invoice')->Where('invoice_id','=',$id)
                        ->_AND()
                        ->Where('user_id','=',USER_ID)
                        ->Get();
            if($invoice->status && $invoice->num_rows==1){
                Theme::SetTitle('Đơn hàng của tôi | lovebook.vn');
                Helper::State('Đơn hàng của tôi',Router::GenerateThisRoute());
                $shipMethod = DB::Query('ship')->Where('ship_id','=',$invoice->Result[0]['ship'])->Get()->Result[0];
                $paymentMethod = DB::Query('payment')->Where('payment_id','=',$invoice->Result[0]['payment'])->Get()->Result[0];
                $order = DB::Query('order')->Where('order_id','INCLUDE',$invoice->Result[0]['order_id'])->Get()->Result;
                $v = $this->View('detailInvoice')
                    ->Assign('p',frontend::$product)
                    ->Assign('order',$order)
                    ->Assign('invoice',$invoice->Result[0])
                    ->Assign('province',getLocation('province'))
                    ->Assign('district',getLocation('district'))
                    ->Assign('shipMethod',$shipMethod)
                    ->Assign('paymentMethod',$paymentMethod)
                    ->Assign('sidebar',$this->Sidebar());
                $this->Render($v->Output());
            }
            else {
                header('Location:'.'/Account/managerInvoice');
            }
        }
        else {
            header('Location:'.'/Account/managerInvoice');
        }
    }
    public function Profile(){
        Theme::SetTitle('Thông tin tài khoản');
        Helper::State('Thông tin tài khoản','');
        $province = getLocation('province');
        $district = getLocation('district');
        $User = $_SESSION['UserInfo'];
        if(Input::Post('submitProfile','')==1){
            if(Access::CheckToken('changeProfile',false)){
                $User = Input::Post('Profile');
                $file = false;
                if(isset($_FILES['avatar']) && $_FILES['avatar']['error']==0 && $_FILES['avatar']['name']!=''){
                    $file = true;
                    if($_FILES['avatar']['size'] > 2148576) {
                        Helper::Notify('error','Kích thước lớn hơn 2Mb');
                    }
                    $file_allow = array('image/jpeg','image/png','image/JPEG','image/PNG');
                    if(!in_array($_FILES['avatar']['type'],$file_allow)){
                        Helper::Notify('error','Định dạng không hợp lệ ');
                    }
                }
                if(Helper::NotifyCount('error')==0) {
                    $User['username'] = $_SESSION['UserInfo']['username'];
                    $User['email'] = $_SESSION['UserInfo']['email'];
                    if ($file) {
                        Boot::library('Upload');
                        $image = Upload::Start($_FILES['avatar']);
                        if ($image->uploaded) {
                            $image->file_new_name_body = Filter::CleanUrlString($_SESSION['UserInfo']['email'] . $_SESSION['UserInfo']['userid'] . $_FILES['avatar']['name']);
                            $image->file_name_body_pre = 'avatar_';
                            $image->image_convert = 'png';
                            $image->process(DATA_PATH . 'uploads' . DIRECTORY_SEPARATOR . 'avatar' . DIRECTORY_SEPARATOR);
                            if ($image->processed) {
                                $User['avatar'] = APPLICATION_DATA_DIR . 'uploads/' . 'avatar' . '/' . $image->file_dst_name;
                                $image->clean();
                            }
                        }
                    }
                    $update = DB::Query('users')->Where('userid','=',USER_ID)->Update($User);
                    if($update->status){
                        $new = Authorized::getUserByEmailOrUserName($User['username']);
                        $_SESSION['UserInfo'] = $new;
                        Helper::Notify('success','Cập nhật thành công!');
                        header( "refresh:1.5;url=".Router::GenerateThisRoute());
                    }
                    else Helper::Notify('error', 'Không thành công!');
                }
            }
            else {
                Helper::Notify('error','Token quá hạn, xin thử lại');
            }
        }
        $v = $this->View('profile')
            ->Assign('User',$User)
            ->Assign('province',$province)
            ->Assign('district',$district)
            ->Assign('changeProfile',Access::GenerateToken('changeProfile'))
            ->Assign('sidebar',$this->Sidebar());
        $this->Render($v->Output());
    }
    public function Favorite(){
        Theme::SetTitle('Danh sách yêu thích | lovebook.vn');
        Helper::State('Danh sách yêu thích',Router::GenerateThisRoute());
        $favorite = DB::Query('favorite')->Where('user_id','=',USER_ID)->Limit(10)->Get('favorite_id',Output::Paging())->Result;
        $product = frontend::$product;
        $v = $this->View('favorite')
                  ->Assign('favorite',$favorite)
                  ->Assign('product',$product)
                  ->Assign('sidebar',$this->Sidebar());
        $this->Render($v->Output());
    }
    public function Review(){
        Theme::SetTitle('Đánh giá của tôi | lovebook.vn');
        Helper::State('Đánh giá của tôi',Router::GenerateThisRoute());
        $review = DB::Query('review')
            ->Order('add_time','DESC')
            ->Where('user_id','=',USER_ID)
            ->Limit(10)
            ->Get('review_id',Output::Paging())
            ->Result;
        $product = frontend::$product;
        $v = $this->View('review')
            ->Assign('review',$review)
            ->Assign('product',$product)
            ->Assign('currenturl',Router::GenerateThisRoute())
            ->Assign('Pg',Output::$Paging)
            ->Assign('sidebar',$this->Sidebar());
        $this->Render($v->Output());
    }
    public function Coin(){
        Helper::Notify('success','Chức năng đang được phát triển');
    }
    public function Notification(){
        Theme::SetTitle('Thông báo của tôi | Lovebook.vn');
        Helper::State('Thông báo của tôi',Router::GenerateThisRoute());
        $notification = DB::Query('message')->Where('to_id','=',USER_ID)
            ->Order('add_time','DESC')
            ->Limit(10)
            ->Get('message_id',Output::Paging())->Result;
        $v = $this->View('notification')
            ->Assign('sidebar',$this->Sidebar())
            ->Assign('message',$notification);
        $this->Render($v->Output());
    }
    public function Sidebar(){
        $User = $_SESSION['UserInfo'];
        return $this->View('sidebar')->Assign('User',$User)->Output();
    }
}