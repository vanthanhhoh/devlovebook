<div class="account">
    <div class="container">
        <div class="accountInner">
            <div class="row">
                <div class="col-md-3 no-padding-right">
                    {$sidebar}
                </div>
                <div class="col-md-9">
                    <div class="accountContent">
                        <h1>Danh sách đơn hàng của tôi</h1>
                        <table class="table table-hover table-condensed table-responsive">
                            <colgroup>
                                <col class="col-xs-2"/>
                                <col class="col-xs-2"/>
                                <col class="col-xs-4"/>
                                <col class="col-xs-2"/>
                                <col class="col-xs-2"/>
                            </colgroup>
                            <thead>
                                <tr>
                                   <th>Mã đơn hàng</th>
                                   <th>Ngày đặt hàng</th>
                                   <th>Sản phẩm</th>
                                   <th>Tổng tiền</th>
                                   <th>Trạng thái đơn hàng</th>
                                </tr>
                            </thead>
                            <tbody>
                                {for $item in $invoice}
                                    <tr>
                                        <td>
                                            <a href="/Account/detailInvoice/{$item.invoice_id}">
                                                {$item.code}
                                            </a>
                                        </td>
                                        <td>{$item.add_time|Filter::UnixTimeToDate:true}</td>
                                        <td>
                                            {function}
                                                $listName = array();
                                                $listP = explode(',',$item['product_id']);
                                                foreach($listP as $itemp){
                                                    $listName[] = $p[$itemp]['title'];
                                                }
                                                $listName = implode(',',$listName);
                                            {/function}
                                            {$listName|Filter::SubString:100}
                                        </td>
                                        <td>{$item.total|Filter::NumberFormat} đ</td>
                                        {function}
                                            $status = array(
                                                1=>'Đang chờ xác nhận',
                                                2=>'Đang chờ thanh toán',
                                                3=>'Đã thanh toán online',
                                                4=>'Đã xác nhận',
                                                5=>'Đang vận chuyển',
                                                6=>'Thành công'
                                            );
                                        {/function}
                                        <td>
                                            {function}echo $status[$item['invoice_status']]{/function}
                                        </td>
                                    </tr>
                                {/for}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>