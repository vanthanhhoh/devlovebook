
<div class="account">
    <div class="container">
        <div class="accountInner">
            <div class="row">
                <div class="col-md-3 no-padding-right">
                    {$sidebar}
                </div>
                <div class="col-md-9">
                    <div class="accountContent">
                        <h1>Đánh giá của tôi</h1>
                        <div class="listMyreview">
                            <table class="table table-hover table-condensed table-responsive">
                                <colgroup>
                                    <col class="col-xs-2"/>
                                    <col class="col-xs-10"/>
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th>Sách</th>
                                        <th>Nội dung đánh giá</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {for $item in $review}
                                    {function}
                                        $itemP = $product[$item['product_id']];
                                    {/function}
                                    <tr>
                                        <td>
                                            <img src="{$itemP.image}" alt="{$itemP.title}" width="90%"/>
                                        </td>
                                        <td>
                                            <div class="item-rating" style="text-align: left">
                                                <p class="rating">
                                                    {function}
                                                        for($i=1;$i<=5;$i++){
                                                        if($i<=$item['rate']){
                                                        echo '<i class="fa fa-star on" aria-hidden="true"></i>';
                                                        }
                                                        else {
                                                        echo '<i class="fa fa-star" aria-hidden="true"></i>';
                                                        }
                                                        }
                                                    {/function}
                                                </p>
                                            </div>
                                            <h3 class="reviewTitle">{$item.title}</h3>
                                            <p class="reviewDescription">
                                                {$item.comment}
                                            </p>
                                        </td>
                                    </tr>
                                {/for}
                                </tbody>
                            </table>
                            <div id="page-selection"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{#APPLICATION_DATA_DIR}theme/{#CURRENT_THEME}/js/jquery.bootpag.min.js"></script>
<script>
    $(document).ready(function(){
        {if($Pg.total_pages>1)}
        $('#page-selection').bootpag({
            total: {$Pg.total_pages},
            page: {$Pg.current_page+1},
            maxVisible: 5,
            next: '<i class="fa fa-chevron-right" aria-hidden="true"></i>',
            prev: '<i class="fa fa-chevron-left" aria-hidden="true"></i>'
        }).on('page', function(event, num){
            window.location.href = '{$currenturl}page-'+num;
        });
        {/if}
    })
</script>
