<div class="account">
    <div class="container">
        <div class="accountInner">
            <div class="row">
                <div class="col-md-3 no-padding-right">
                    {$sidebar}
                </div>
                <div class="col-md-9">
                    <div class="accountContent">
                        <h1>Danh sách yêu thích</h1>
                        <table class="table table-hover table-condensed table-responsive">
                            <colgroup>
                                <col class="col-xs-2"/>
                                <col class="col-xs-5"/>
                                <col class="col-xs-2"/>
                                <col class="col-xs-2"/>
                                <col class="col-xs-1"/>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>Hình ảnh</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Giá gốc</th>
                                    <th>Giá KM</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {for $item in $favorite}
                                    {function}
                                        $itemP= $product[$item['product_id']];
                                        $url = Router::Generate('ProductDetail',array('product'=>$itemP['url'],'pid'=>$itemP['product_id']));
                                    {/function}
                                    <tr>
                                        <td>
                                            <img src="{$itemP.image|Output::GetThumbLink:80,80}"/>
                                        </td>
                                        <td><a href="{$url}">{$itemP.title}</a></td>
                                        <td>{$itemP.price|Filter::NumberFormat} đ</td>
                                        <td>
                                            {$itemP.price_sale|Filter::NumberFormat} đ
                                        </td>
                                        <td>
                                            <button class="removeFavorite" data-id="{$item.favorite_id}" onclick="removeFavorite($(this))">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </button>
                                        </td>
                                    </tr>
                                {/for}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>