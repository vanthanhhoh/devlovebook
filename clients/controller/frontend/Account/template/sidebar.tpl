<div class="accountSidebar">
    <div class="profileAmazing">
        <div class="pull-left profileAva">
            <img src="{$User.avatar}" alt="{$User.fullname}" />
        </div>
        <div class="pull-left profileName">
            <small>Tài khoản của</small>
            <p>{$User.fullname}</p>
        </div>
    </div>
    <ul class="accountListFunction">
        <li><a href="/Account/managerInvoice"><i class="fa fa-credit-card" aria-hidden="true"></i> Đơn hàng của tôi</a></li>
        <li><a href="/Account/Notification"><i class="fa fa-rss" aria-hidden="true"></i> Thông báo của tôi</a></li>
        <li><a href="/Account/Profile"><i class="fa fa-key" aria-hidden="true"></i> Tài khoản của tôi</a></li>
        <li><a href="/Account/Favorite"><i class="fa fa-bookmark-o" aria-hidden="true"></i> Danh sách yêu thích</a></li>
        <li><a href="/Account/Review"><i class="fa fa-commenting-o" aria-hidden="true"></i> Đánh giá của tôi</a></li>
        <li><a href="/Account/Coin"><i class="fa fa-usd" aria-hidden="true"></i> Lovebook xu</a></li>
        <li><a href="/logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Thoát tài khoản</a></li>
    </ul>
</div>