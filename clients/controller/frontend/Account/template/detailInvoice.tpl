<div class="account">
    <div class="container">
        <div class="accountInner">
            <div class="row">
                <div class="col-md-3 no-padding-right">
                    {$sidebar}
                </div>
                <div class="col-md-9">
                    <div class="accountContent">
                        <h1>Đơn hàng #{$invoice.code}</h1>
                        <p class="date">Ngày đặt hàng:  {$invoice.add_time|Filter::UnixTimeToFullDate:true}</p>
                        <div class="address-1 accountBlock">
                            <h3>Địa chỉ người nhận</h3>
                            <p class="name_ship">{$invoice.name}</p>
                            <p class="name_phone">Số điện thoại người nhận: {$invoice.phone}</p>
                            <p class="addres_ship">{$invoice.address}, {function}echo $district[$invoice['district']]['title'] . ', '.$province[$invoice['province']]['title']{/function}</p>
                        </div>
                        <div class="method">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="shipMethod accountBlock">
                                        <h3>Phương thức nhận hàng</h3>
                                        <p>{$shipMethod.title}</p>
                                        <p>{$shipMethod.description}</p>
                                        <p>{$shipMethod.fee|Filter::NumberFormat} đ</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="paymentMethod accountBlock">
                                        <h3>Phương thức thanh toán</h3>
                                        <p>{$paymentMethod.title}</p>
                                        <p>{$paymentMethod.description}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="listOrder">
                            <table class="table table-bordered">
                                <colgroup>
                                    <col class="col-xs-4"/>
                                    <col class="col-xs-2"/>
                                    <col class="col-xs-2"/>
                                    <col class="col-xs-2"/>
                                    <col class="col-xs-2"/>
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th>Tên sản phẩm</th>
                                        <th>Giá gốc</th>
                                        <th>Giá khuyến mãi</th>
                                        <th>Số lượng</th>
                                        <th>Tổng cộng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {$giagoc = 0}
                                    {$dagiam=0}
                                    {for $item in $order}
                                        {function}
                                            $product = $p[$item['product_id']];
                                            $url = Router::Generate('ProductDetail',array('product'=>$product['url'],'pid'=>$product['product_id']));
                                            $giagoc = $giagoc+($item['price_old']*$item['qty']);
                                            $dagiam+=$item['total'];
                                        {/function}
                                        <tr>
                                            <td>
                                                <a href="{$url}">{$product.title}</a>
                                                {if($item.type==2)}
                                                    <i>Quà tặng</i>
                                                {/if}
                                            </td>
                                            <td>{$item.price_old|Filter::NumberFormat} đ</td>
                                            <td>{$item.price|Filter::NumberFormat} đ</td>
                                            <td>{$item.qty}</td>
                                            <td>{$item.total|Filter::NumberFormat} đ</td>
                                        </tr>
                                    {/for}
                                    <tr>
                                        {function}
                                            $giamgia = $giagoc-$dagiam;
                                            $giamgiacupon =0;
                                            if($invoice['cupon']!='' && $invoice['cupon_discount']>0 && $invoice['cupon_type']!=''){
                                                if($invoice['cupon_type']==1){
                                                    $giamgiacupon = $invoice['cupon_discount'];
                                                }
                                                else {
                                                    $giamgiacupon = $dagiam*$invoice['cupon_discount']/100; $giamgiacupon = ceil($giamgiacupon);
                                                }
                                            }
                                        {/function}
                                        <td colspan="5" align="right" class="sumaryTable">
                                            <p>
                                                <span>Tổng chưa giảm:</span> <strong>{$giagoc|Filter::NumberFormat} đ</strong>
                                            </p>
                                            <p>
                                                <span>Giảm giá:</span> <strong>-{$giamgia|Filter::NumberFormat} đ</strong>
                                            </p>
                                            <p>
                                                <span>Tổng đã giảm:</span> <strong>{$dagiam|Filter::NumberFormat} đ</strong>
                                            </p>
                                            <p>
                                                <span>Giảm giá theo mã {$invoice.cupon}:</span> <strong>-{$giamgiacupon|Filter::NumberFormat} đ</strong>
                                            </p>
                                            <p>
                                                <span>Phí vận chuyển:</span> <strong>{$shipMethod.fee|Filter::NumberFormat} đ</strong>
                                            </p>
                                            <hr/>
                                            <p>
                                               {function}$tongcong = $dagiam-$giamgiacupon+$shipMethod['fee']{/function}
                                               Tổng cộng: <strong>{$tongcong|Filter::NumberFormat} đ</strong>
                                            </p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="functionInvoice">
                            {if($paymentMethod.online==1 && $invoice.invoice_status<3)}
                                <a href="/CheckOut/rePay/{$invoice.invoice_id}" class="thanhtoanlai">Thanh toán lại online</a>
                            {/if}
                            <a href="" class="removedonhang">Xóa đơn hàng</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>