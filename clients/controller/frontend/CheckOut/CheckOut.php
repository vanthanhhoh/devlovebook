<?php
/**
 * Created by PhpStorm.
 * User: THANH
 * Date: 13/07/2016
 * Time: 11:28 SA
 */
class CheckOut extends Controller {
    public function Main(){
        if(isset($_SESSION['cart'])){
            if(!empty($_SESSION['cart'])){
                if(USER_ID > 0 && !isset($_SESSION['ship'])){
                    $this->checkShip();
                }
                elseif(USER_ID>0 && isset($_SESSION['ship'])){
                    $this->checkPayment();
                }
                else {
                    $this->checkLogin();
                }

            }
            else {
                header('Location:'.APP_DOMAIN);
            }
        }
        else {
            header('Location:'.APP_DOMAIN);
        }
    }
    public function checkLogin(){
        if(isset($_SESSION['cart'])){
            if(!empty($_SESSION['cart'])){
                $ship = 0;
                $p = frontend::$product;
                $Cupon = array();
                if(isset($_SESSION['cupon'])){
                    if(!empty($_SESSION['cupon'])){
                        $Cupon = $_SESSION['cupon'];
                    }
                }
                if(Input::Post('login',0)==1){ // Checklogin
                    $username = Input::Post('email','');
                    $password = Input::Post('password','');
                    if(G::$Authorized->CheckLogin($username, $password)){
                        header('Location:'.'/CheckOut/Main');
                    }
                }
                if(Input::Post('resgiter',0)==1){
                    $User = array (
                        'username' => '',
                        'email' => '',
                        'phone' => '',
                        'fullname' => '',
                        'avatar' => '/clients/data/images/anonymous.png',
                        'birthday' => '',
                        'regdate'=>CURRENT_TIME,
                        'gender' => '1',
                        'status' => 1,
                        'password' => '',
                        'level' => '1'
                    );
                    $User['username']=Input::Post('email');
                    $User['email']=Input::Post('email');
                    $User['password']=Input::Post('password','');
                    $User['phone']    = Input::Post('phone','');
                    $User['fullname'] = Input::Post('fullname','');
                    $User['birthday'] = Input::Post('birthday','');
                    $User['gender']   = Input::Post('gender','');
                    $User['birthday'] = Filter::DateToUnixTime($User['birthday'],0,0);
                    if($UserID = Authorized::AddUser($User)) {
                        G::$Authorized->CheckLogin($User['email'], $User['password']);
                        header('Location:'.'/CheckOut/Main');
                    }
                    else{
                        Helper::Notify('error','Đăng kí thất bại');
                    }
                }
                Theme::Assign('step',1);
                $v = $this->View('check_login');
                $v = $v ->Assign('cart',$_SESSION['cart'])
                    ->Assign('p',$p)
                    ->Assign('Cupon',$Cupon)
                    ->Assign('ship',$ship);
                Theme::SetTitle('Đăng nhập | lovebook.vn');
                Theme::jsPlugin('icheck');
                $this->Render($v->Output(),array('layout'=>'layout.checkout'));
            }
            else {
                header('Location:'.APP_DOMAIN);
            }
        }
        else {
            header('Location:'.APP_DOMAIN);
        }
    }
    public function checkShip(){
        $p = frontend::$product;
        $ship = 0;
        $Cupon = array();
        if(isset($_SESSION['cupon'])){
            if(!empty($_SESSION['cupon'])){
                $Cupon = $_SESSION['cupon'];
            }
        }
        Theme::SetTitle('Địa chỉ giao hàng');
        Theme::Assign('step',2);
        $province = getLocation('province');
        if(Input::Post('shipSubmit',0)==1){
            $_SESSION['ship']=Input::Post('ship');
            header('Location:'.'/CheckOut/Main');
        }
        $ship_address = array(
            'name'      =>  '',
            'phone'     =>  '',
            'province'  =>  '',
            'district'  =>  '',
            'address'   =>  '',
            'note'      => ''
        );
        $getshipAddres = DB::Query('shipaddress')->Where('user_id','=',USER_ID)->Get();
        if($getshipAddres->status && $getshipAddres->num_rows==1) {
            $ship_address = array_merge($ship_address,$getshipAddres->Result[0]);
        }
        if(isset($_SESSION['ship'])){
            $ship_address = array_merge($ship_address,$_SESSION['ship']);
        }
        $v = $this->View('check_ship')
            ->Assign('cart',$_SESSION['cart'])
            ->Assign('p',$p)
            ->Assign('Cupon',$Cupon)
            ->Assign('province',$province)
            ->Assign('ship_address',$ship_address)
            ->Assign('ship',$ship);
        $this->Render($v->Output(),array('layout'=>'layout.checkout'));
    }
    public function checkPayment(){

        $p = frontend::$product;
        Theme::SetTitle('Thông tin thanh toán | lovebook.vn');
        Theme::Assign('step',3);
        $shipMethod = DB::Query('ship')->Get()->Result;
        $paymentMethod = DB::Query('payment')->Get()->Result;
        $Cupon = array();
        if(isset($_SESSION['cupon'])){
            if(!empty($_SESSION['cupon'])){
                $Cupon = $_SESSION['cupon'];
            }
        }
        // Nếu ở hà nội default gói ship miễn phí nội thành
        if($_SESSION['ship']['province']==2){
            $ship_choose = 2;
            $ship= $shipMethod[1]['fee'];
        }
        else {
            $ship_choose = $shipMethod[0]['ship_id'];
            $ship = $shipMethod[0]['fee'];
        }
        //End
        //Change Shipmethod
        if(Input::Post('submitShip',0)==1){
            $ship_id = Input::Post('ship_id',1);
            foreach($shipMethod as $shipM){
                if($shipM['ship_id']==$ship_id) {
                    $ship=$shipM['fee'];
                    $ship_choose = $ship_id;
                }
            }
        }
        //End
        Theme::jsPlugin('icheck');
        if(Input::Post('finsh',0)==1){
            $invoice = $_SESSION['ship'];
            $invoice['ship'] = Input::Post('ship',0);
            $invoice['payment'] = Input::Post('payment',0);
            $moneySend =0;
            $orderID = array();
            $productID =array();
            foreach($_SESSION['cart'] as $item){
                $item['price_old'] = $p[$item['product_id']]['price'];
                $moneySend = $moneySend+$item['total'];
                $productID[]= $item['product_id'];
                $insertOrder = DB::Query('order')->Insert($item);
                if($insertOrder->status && $insertOrder->insert_id>0){
                    $orderID[] = $insertOrder->insert_id;
                }
            }
            $discountCupon = 0;
            if(isset($_SESSION['cupon'])){
                if(!empty($_SESSION['cupon'])){
                    $invoice['cupon']=$_SESSION['cupon']['code'];
                    $invoice['cupon_discount']=$_SESSION['cupon']['discount'];
                    $invoice['cupon_type']=$_SESSION['cupon']['discount_type'];
                    if($_SESSION['cupon']['discount_type']==1){
                        $discountCupon = $_SESSION['cupon']['discount'];
                    }
                    else {
                        $discountCupon = $moneySend*$_SESSION['cupon']['discount']/100;
                        $discountCupon = ceil($discountCupon);
                    }
                }
            }
            $moneySend = $moneySend-$discountCupon;                 // Trừ giảm giá cupon
            $shipFee = DB::Query('ship')->Where('ship_id','=',$invoice['ship'])->Get();
            if($shipFee->status && $shipFee->num_rows==1){
                $moneySend = $moneySend+$shipFee->Result[0]['fee']; // Cộng thêm phí ship
            }


            $invoice['total'] = $moneySend;
            $invoice['order_id'] = implode(',',$orderID);
            $invoice['product_id'] = implode(',',$productID);
            $invoice['invoice_status']= 1;
            $invoice['add_time']=CURRENT_TIME;
            $invoice['edit_time']=CURRENT_TIME;
            $invoice['user_id']= USER_ID;
            $invoice['code']= 'HD'.getNextNumber();

            $insertInvoice = DB::Query('invoice')->Insert($invoice);
            if($insertInvoice->status && $insertInvoice->insert_id>0){
                foreach($orderID as $ID){
                    DB::Query('order')->Where('order_id','=',$ID)->Update(array('invoice_id'=>$insertInvoice->insert_id));
                }
                unset($_SESSION['cart']);unset($_SESSION['ship']);unset($_SESSION['cupon']);
                $paymentModule = DB::Query('payment')->Where('payment_id','=',$invoice['payment'])->Get();
                if($paymentModule->status && $paymentModule->num_rows==1)
                {
                    $paymentModule = $paymentModule->Result[0];
                    if($paymentModule['online']==1){
                        $paymentModule = $paymentModule['module'];
                        if($paymentModule!=''){
                            header('Location: /paymentModule/'.$paymentModule.'?total='.$moneySend.'&invoice='.$insertInvoice->insert_id.'&bankcode='.Input::Post('bankcode','VCB'));
                        }
                    }
                    else {
                        header('Location: /CheckOut/Success');
                    }
                }
            }
        }
        $province = getLocation('province');
        $district = getLocation('district');
        $v = $this->View('check_payment');
        $v = $v->Assign('shipMethod',$shipMethod)
            ->Assign('payment',$paymentMethod)
            ->Assign('cart',$_SESSION['cart'])
            ->Assign('ship_address',$_SESSION['ship'])
            ->Assign('province',$province)
            ->Assign('district',$district)
            ->Assign('p',$p)
            ->Assign('ship',$ship)
            ->Assign('Cupon',$Cupon)
            ->Assign('ship_choose',$ship_choose);
        $this->Render($v->Output(),array('layout'=>'layout.checkout'));
    }
    public function rePay(){
        if(isset(G::$Registry['Params'][ROUTER_EXTRA_KEY])) {
            $id = G::$Registry['Params'][ROUTER_EXTRA_KEY];
            $invoice = DB::Query('invoice')->Where('invoice_id','=',$id)->Get();
            if($invoice->status && $invoice->num_rows==1){
                $invoice = $invoice->Result[0];
                $order = DB::Query('order')->Where('order_id','INCLUDE',$invoice['order_id'])->Get()->Result;
                $shipMethod = DB::Query('ship')->Get()->Result;
                $paymentMethod = DB::Query('payment')->Get()->Result;
                $ship_choose = $invoice['ship'];
                if(Input::Post('submitShip',0)==1){
                    $ship_id = Input::Post('ship_id',1);
                    foreach($shipMethod as $shipM){
                        if($shipM['ship_id']==$ship_id) {
                            $ship=$shipM['fee'];
                            $ship_choose = $ship_id;
                        }
                    }
                }
                else  $ship = DB::Query('ship')->Where('ship_id','=',$invoice['ship'])->Get()->Result[0]['fee'];
                $p = frontend::$product;
                Theme::jsPlugin('icheck');
                Theme::SetTitle('Đơn hàng của tôi | lovebook.vn');
                Theme::Assign('step',3);
                if(Input::Post('finsh',0)==1){
                    $invoice['ship'] = Input::Post('ship',1);
                    $invoice['payment'] = Input::Post('payment',1);
                    $invoice['edit_time']= CURRENT_TIME;
                    $update = DB::Query('invoice')->Where('invoice_id','=',$id)->Update($invoice);
                    if($update->status){
                        $paymentModule = DB::Query('payment')->Where('payment_id','=',$invoice['payment'])->Get();
                        if($paymentModule->status && $paymentModule->num_rows==1){
                            $paymentModule = $paymentModule->Result[0]['module'];
                            if($paymentModule!=''){

                                header('Location: /paymentModule/'.$paymentModule.'?invoice='.$id.'&bankcode='.Input::Post('bankcode','VCB'));

                            }
                        }
                    }
                }
                $v = $this->View('repay');
                $v = $v->Assign('cart',$order)
                        ->Assign('shipMethod',$shipMethod)
                        ->Assign('invoice',$invoice)->Assign('p',$p)
                        ->Assign('province',getLocation('province'))
                        ->Assign('district',getLocation('district'))
                        ->Assign('ship',$ship)
                        ->Assign('ship_choose',$ship_choose)
                        ->Assign('payment',$paymentMethod);
                $this->Render($v->Output(),array('layout'=>'layout.checkout'));
            }
        }
    }

    public function Failed(){

    }
    public function Success(){
        Theme::SetTitle('Thanh toán thành công!');
        $v= $this->View('success');
        $this->Render($v->Output());
    }
}