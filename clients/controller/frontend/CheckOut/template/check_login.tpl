<div class="login-require">
    <div class="container">
        <h1 class="title-login">Khách hàng mới / Đăng nhập</h1>
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="form-area">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-radio">
                                    <ul>
                                        <li>
                                            <input type="radio" class="method icheck" value="login" name="method" checked/>
                                            <label>Tôi là thành viên Lovebook</label>
                                        </li>
                                        <li>
                                            <input type="radio" class="method icheck" value="register" name="method"/>
                                            <label>Tôi là khách hàng mới</label>
                                        </li>
                                    </ul>
                                </div>
                                <script src="{#APPLICATION_DATA_DIR}theme/{#CURRENT_THEME}/js/jquery.validate.min.js"></script>
                                <div class="panel-body">
                                    <div class="form-login">
                                        <form id="login" method="post" action="">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input name="email" type="email" class="form-control" placeholder="abc@gmail.com" required=""/>
                                            </div>
                                            <div class="form-group">
                                                <label>Mật khẩu</label>
                                                <input name="password" type="password" class="form-control" placeholder="Nhập mật khẩu ở đây" required=""/>
                                            </div>
                                            <input type="hidden" name="login" value="1"/>
                                            <button id="login" type="submit">Đăng nhập</button>
                                        </form>
                                        <form id="register" method="post" action="" style="display: none">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input name="email" type="email" class="form-control" placeholder="abc@gmail.com" required=""/>
                                            </div>
                                            <div class="form-group">
                                                <label>Họ tên</label>
                                                <input name="fullname" type="text" class="form-control" placeholder="Họ tên" required=""/>
                                            </div>
                                            <div class="form-group">
                                                <label>Mật khẩu</label>
                                                <input name="password" type="password" class="form-control" placeholder="Nhập mật khẩu ở đây" id="NewPassword" required=""/>
                                            </div>
                                            <div class="form-group">
                                                <label>Nhập lại mật khẩu</label>
                                                <input type="password" class="form-control" placeholder="Nhập lại mật khẩu" equalTo="#NewPassword"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Số điện thoại</label>
                                                <input type="text" minlength="10" maxlength="11" name="phone" class="form-control" placeholder="Số điện thoại"/>
                                            </div>
                                            <div class="form-group">
                                                <div class="sliptTwo">
                                                    <div class="pull-left halfPart">
                                                        <label>Ngày sinh</label>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" value="" id="datepicker" name="birthday" placeholder="Ngày sinh">
                                                            <div class="input-group-addon">
                                                                <span class="glyphicon glyphicon-th"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="pull-left halfPart">
                                                        <label>Giới tính</label>
                                                        <select name="gender" class="form-control">
                                                            <option value="1">Boy</option>
                                                            <option value="2">Girl</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="resgiter" value="1"/>
                                            <button id="resgiter" type="submit">Đăng kí</button>
                                        </form>
                                    </div>
                                </div>

                                <script>
                                    $(document).ready(function(){
                                        $('input.icheck').iCheck({
                                            checkboxClass: 'icheckbox_minimal',
                                            radioClass: 'iradio_minimal',
                                            increaseArea: '20%' // optional,
                                        });
                                        $('input.icheck').on('ifChecked', function(event){
                                            if(event.delegateTarget.value=='login'){
                                                $('form#register').hide();
                                                $('form#login').show();
                                            }
                                            else {
                                                $('form#register').show();
                                                $('form#login').hide();
                                            }
                                        });
                                        $('#login').validate({
                                            errorElement: "span"
                                        });
                                        $('#register').validate({
                                            errorElement: "span"
                                        });
                                    });
                                </script>
                            </div>
                            <div class="col-md-6">
                                <p class="easyLogin">Đăng nhập nhanh chóng tiện lợi với facebook</p>
                                <a class="btn btn-block btn-social btn-facebook user-name-loginfb" title="Đăng nhập bằng Facebook" onclick="openPopup()">
                                    <i class="fa fa-facebook"></i>
                                    <span>Đăng nhập bằng</span><span> Facebook</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 nopadding">
                <div class="panel panel-default">
                    <div class="review_cart">
                        <div class="review_cart_title">Đơn hàng gồm có <strong>{function}echo count($cart){/function}</strong> sản phẩm</div>
                        <table>
                            <tbody>
                                {$tamtinh=0}
                                {for $item in $cart}
                                    {function}
                                        $pitem = $p[$item['product_id']];
                                        $tamtinh = $tamtinh+$item['total'];
                                        $url = Router::Generate('ProductDetail',array('product'=>$pitem['url'],'pid'=>$pitem['product_id']));
                                    {/function}
                                    <tr>
                                        <td class="title">
                                            <strong>{$item.qty}x</strong> <a href="{$url}">{$pitem.title}</a>
                                        </td>
                                        <td class="total" align="right">
                                            {$item.total|Filter::NumberFormat} đ
                                        </td>
                                    </tr>
                                {/for}
                            </tbody>
                        </table>
                        {function}
                            $discount = 0;
                            $thanhtien= $tamtinh;
                            if(!empty($Cupon)){
                                if($Cupon['discount_type']==1) {
                                    $thanhtien = $tamtinh- $Cupon['discount'];
                                    $discount = $Cupon['discount'];
                                }
                                else {
                                    $discount = $tamtinh*($Cupon['discount']/100);
                                    $discount = ceil($discount);
                                    $thanhtien = $tamtinh-$discount;
                                }
                            }
                        {/function}
                        <p class="tamtinh">
                            <span class="title">Tạm tính</span>
                            <span class="value">{$tamtinh|Filter::NumberFormat} đ</span>
                        </p>
                        {if($discount>0)}
                            <p class="tamtinh">
                                <span class="title">Giảm giá theo mã</span>
                                <span class="value">-{$discount|Filter::NumberFormat} đ</span>
                            </p>
                        {/if}
                        <p class="tamtinh">
                            <span class="title">Phí vận chuyển</span>
                            <span class="value">Chưa có</span>
                        </p>
                        <hr>
                        <p class="tamtinh">
                            <span class="title thanhtien">Thành tiền</span>
                            <span class="value thanhtien">
                                {$thanhtien|Filter::NumberFormat} đ
                            </span>
                        </p>
                        <a href="/gio-hang.html" title="Sửa đơn hàng" class="editCart">Sửa đơn hàng</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function openPopup(){
        var w = window.open("/Auth/register_remote/facebook","_blank","toolbar=yes, location=yes, directories=no, status=no, menubar=yes, scrollbars=yes, resizable=no, copyhistory=yes, width=400, height=400");
    }
</script>