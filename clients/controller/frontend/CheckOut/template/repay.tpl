<div class="login-require">
    <div class="container">
        <h1 class="title-login">Thông tin thanh toán</h1>

        <div class="row">
            <div class="col-md-9">
                <form id="shipMethod" method="post" action="">
                    <input type="hidden" name="submitShip" value="1"/>
                    <input type="hidden" name="ship_id" id="ship_id" value="1"/>
                </form>
                <form id="payment" method="post" action="">
                    <div class="panel panel-default">
                        <div class="form-area">
                            <div class="selectShip">
                                <p>Chọn gói giao hàng:</p>
                                <ul>
                                    {for $item in $shipMethod as $key}
                                        <li>
                                            <input type="radio" class="method icheck ship" value="{$item.ship_id}" name="ship" {if($item.ship_id==$ship_choose)}checked{/if}/>
                                            <label>{$item.title}</label>
                                        </li>
                                    {/for}
                                </ul>
                            </div>
                            <div class="selectPayment">
                                <p>Chọn phương thức thanh toán:</p>
                                <ul>
                                    {for $item in $payment as $key}
                                        <li>
                                            <input type="radio" class="{if($item.module !='')}payment
                                        {/if} icheck" value="{$item.payment_id}" name="payment" {if($key==0)}checked{/if} data-method="{$item.module}"/>
                                            <label>{$item.title}</label>
                                            <div id="content_payment_{$item.payment_id}" class="content_payment">

                                            </div>
                                        </li>
                                    {/for}
                                </ul>
                            </div>
                            <input type="hidden" value="1" name="finsh"/>
                            <button type="submit" class="finsh">Thanh toán lại</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-3 nopadding">
                <div class="panel panel-default">
                    <div class="review_cart">
                        <div class="review_cart_title">Địa chỉ nhận hàng</div>
                        <p class="recive_name">{$invoice.name}</p>
                        <p class="recive_address">{$invoice.address} , {function}echo  $district[$invoice['district']]['title'].', '.$province[$invoice['province']]['title']{/function}</p>
                        <p class="recive_phone">{$invoice.phone}</p>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="review_cart">
                        <div class="review_cart_title">Đơn hàng gồm có <strong>{function}echo count($cart){/function}</strong> sản phẩm</div>
                        <table>
                            <tbody>
                            {$tamtinh=0}
                            {for $item in $cart}
                                {function}
                                    $pitem = $p[$item['product_id']];
                                    $tamtinh = $tamtinh+$item['total'];
                                    $url = Router::Generate('ProductDetail',array('product'=>$pitem['url'],'pid'=>$pitem['product_id']));
                                {/function}
                                <tr>
                                    <td class="title">
                                        <strong>{$item.qty}x</strong> <a href="{$url}">{$pitem.title}</a>
                                    </td>
                                    <td class="total" align="right">
                                        {$item.total|Filter::NumberFormat} đ
                                    </td>
                                </tr>
                            {/for}
                            </tbody>
                        </table>
                        {$discountCupon = 0}
                        {if($invoice.cupon!='' && $invoice['cupon_discount'] >0 && $invoice['cupon_type']!='')}
                            {function}
                                if($invoice['cupon_type']==1) {
                                    $discountCupon = $invoice['cupon_discount'];
                                }
                                else {
                                    $discountCupon = ceil($tamtinh*$invoice['cupon_discount']/100);
                                }
                            {/function}
                        {/if}
                        <p class="tamtinh">
                            <span class="title">Tạm tính</span>
                            <span class="value">{$tamtinh|Filter::NumberFormat} đ</span>
                        </p>
                        {if($discountCupon>0)}
                        <p class="tamtinh">
                            <span class="title">Giảm giá theo mã</span>
                            <span class="value">-{$discountCupon|Filter::NumberFormat} đ</span>
                        </p>
                        {/if}
                        {function}$tamtinh = $tamtinh-$discountCupon{/function}
                        <p class="tamtinh">
                            <span class="title">Phí vận chuyển</span>
                            <span class="value">{if($ship>0)} {$ship|Filter::NumberFormat} đ {else}Chưa có{/if}</span>
                        </p>
                        <hr>
                        <p class="tamtinh">
                            <span class="title thanhtien">Thành tiền</span>
                            <span class="value thanhtien">
                                {if($ship>0)}
                                    {function}$thanhtien = $tamtinh+$ship{/function}
                                    {$thanhtien|Filter::NumberFormat} đ
                                {else}
                                    {$tamtinh|Filter::NumberFormat} đ
                                {/if}
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('input.icheck').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '20%' // optional,
        });
        $('input.icheck.payment').on('ifChecked', function(event){
            var method = event.delegateTarget.attributes;
            method =method['data-method']['value'];
            $.ajax({
                url: '/paymentModule/'+method,
                type:'post',
                dataType:'text',
                data:{get_content:1},
                success: function(res){
                    $('#content_payment_'+event.delegateTarget.value).html(res);
                }
            });
            var near = $(this).siblings();
            near.each(function() {
                console.log($(this).parents('li'));
            })
        });
        $('input.icheck.ship').on('ifChecked', function(event){
            var id =event.delegateTarget.value;
            $('#ship_id').val(id);
            $('#shipMethod').submit();
        });

    });
</script>