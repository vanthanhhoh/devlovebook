<div class="completeOrder">
    <div class="bgBigCheck"></div>
    <h1>Đặt hàng thành công</h1>
    <h3>Cảm ơn quý khách đã đặt hàng! Chúng tôi sẽ liên hệ quý khách ngay để xác nhận đơn hàng</h3>
    <div class="text-center">
        <a href="/Account/managerInvoice" class="btn btn-success buttonCart">Quản lý đơn hàng</a>
        <a href="{#BASE_DIR}" class="btn btn-success buttonCart">Về trang chủ</a>
    </div>
</div>