<div class="account">
    <div class="container">
        <div class="accountInner">
            <div class="row">
                <div class="col-md-3 no-padding-right">
                    <div class="accountSidebar">
                        <div class="profileAmazing">
                            <div class="pull-left profileAva">
                                <img src="{$User.avatar}" alt="{$User.fullname}" />
                            </div>
                            <div class="pull-left profileName">
                                <small>Tài khoản của</small>
                                <p>{$User.fullname}</p>
                            </div>
                        </div>
                        <ul class="accountListFunction">
                            <li><a href="#"><i class="fa fa-credit-card" aria-hidden="true"></i> Đơn hàng của tôi</a></li>
                            <li><a href="#"><i class="fa fa-rss" aria-hidden="true"></i> Thông báo của tôi</a></li>
                            <li><a href="#"><i class="fa fa-key" aria-hidden="true"></i> Tài khoản của tôi</a></li>
                            <li><a href="#"><i class="fa fa-bookmark-o" aria-hidden="true"></i> Danh sách yêu thích</a></li>
                            <li><a href="#"><i class="fa fa-commenting-o" aria-hidden="true"></i> Đánh giá của tôi</a></li>
                            <li><a href="#"><i class="fa fa-usd" aria-hidden="true"></i> Lovebook xu</a></li>
                            <li><a href="#"><i class="fa fa-sign-out" aria-hidden="true"></i> Thoát tài khoản</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="accountContent">
                        <h1>Thông báo của tôi</h1>
                        <table class="table table-hover table-condensed table-responsive">
                            <colgroup>
                                <col class="col-xs-2"/>
                                <col class="col-xs-7"/>
                                <col class="col-xs-3"/>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>Thời gian</th>
                                    <th>Nội dung</th>
                                    <th class="text-center">Đánh dấu</th>
                                </tr>
                            </thead>
                            <tbody>
                                {for $item in $message}
                                    <tr>
                                        <td>
                                            {$item.add_time|Filter::UnixTimeToDate:true}
                                            {if($item.status==0)}
                                                <p class="new_notification">thông báo mới</p>
                                            {/if}
                                        </td>
                                        <td>{$item.content}</td>
                                        <td class="text-center">
                                            {if($item.status==0)}
                                                <button class="readedNotification" onclick="readedNotification($(this))" data-id="{$item.message_id}">Đánh dấu đã đọc</button>
                                            {/if}
                                        </td>
                                    </tr>
                                {/for}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>