<div class="cart">
    <div class="container">

        <div class="cart-content">
            <div class="row">
                <div class="col-md-9">
                    <div class="cart-header-table">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                Hiện tại có <strong>{$num}</strong> sản phẩm trong giỏ hàng
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                Giá mua
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                Số lượng
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                Thành tiền
                            </div>
                        </div>
                    </div>
                    <div class="list-item">
                        {for $item in $cart}
                            {if($item.type==1)}
                                {function}
                                    $product = $p[$item['product_id']];
                                    $gift = unserialize($product['gift']);
                                {/function}
                                <div class="item-cart">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                            <div class="item-cart-image">
                                                <img src="{$product.image}" alt="{$product.title}"/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <div class="item-cart-infomation">
                                                <h3>{$product.title}</h3>
                                                {if($product.price_sale>0)}
                                                    {function}
                                                        $disc = $product['price_sale']/$product['price']; $disc = ceil($disc*100);
                                                        $disc = 100-$disc;
                                                    {/function}
                                                    <div class="pull-left">
                                                        <span class="discount-icon">GIẢM GIÁ: {$disc}%</span>
                                                    </div>
                                                {/if}
                                                {if(!empty($gift))}
                                                    <div class="pull-left" style="margin-left: 10px">
                                                        <span class="gift-icon"><i class="fa fa-gift" aria-hidden="true"></i> Có quà tặng kèm</span>
                                                    </div>
                                                {/if}
                                                <div class="clearfix"></div>
                                                <button class="btn btn-default" onclick="removeItem($(this))" data-id="{$product.product_id}">
                                                    <i class="fa fa-trash" aria-hidden="true"></i> Xóa sản phẩm này
                                                 </button>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                            <div class="giamua">
                                                {$cprice=0}
                                                {if($product.price_sale>0)}
                                                    {$cprice = $product['price_sale']}
                                                    <span class="newprice">{$product.price_sale|Filter::NumberFormat} đ</span>
                                                    <span class="oldprice">{$product.price|Filter::NumberFormat} đ</span>
                                                {else}
                                                    {$cprice = $product['price']}
                                                    <span class="newprice">{$product.price|Filter::NumberFormat} đ</span>
                                                {/if}
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                            <div class="quanlity">
                                                <select class="quantity" data-id="{$product.product_id}" data-qty="{$item.qty}" onchange="changeItem($(this))">
                                                    <option value="1" {if($item.qty==1)} selected="selected"{/if}>1</option>
                                                    <option value="2" {if($item.qty==2)} selected="selected"{/if}>2</option>
                                                    <option value="3" {if($item.qty==3)} selected="selected"{/if}>3</option>
                                                    <option value="4" {if($item.qty==4)} selected="selected"{/if}>4</option>
                                                    <option value="5" {if($item.qty==5)} selected="selected"{/if}>5</option>
                                                    <option value="6" {if($item.qty==6)} selected="selected"{/if}>6</option>
                                                    <option value="7" {if($item.qty==7)} selected="selected"{/if}>7</option>
                                                    <option value="8" {if($item.qty==8)} selected="selected"{/if}>8</option>
                                                    <option value="9" {if($item.qty==9)} selected="selected"{/if}>9</option>
                                                    <option value="10" {if($item.qty==10)} selected="selected"{/if}>10</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                            <div class="thanhtien">
                                                {function}$total = $cprice*$item['qty']{/function}
                                            </div>
                                            <strong>{$total|Filter::NumberFormat} đ</strong>
                                        </div>
                                    </div>
                                </div>
                            {/if}
                        {/for}
                    </div>
                    <div class="list-gift">
                        <div class="list-gift-header">Quà tặng kèm theo: </div>
                        <div class="row">
                            {for $item in $cart}
                                {if($item.type==2)}
                                    {function}$product = $p[$item['product_id']]{/function}
                                    <div class="col-md-3">
                                        <div class="gift-item">
                                            <img src="{$product.image|Output::GetThumbLink:200,200}" alt="{$product.title}"/>
                                            <span class="icon"><i class="fa fa-gift" aria-hidden="true"></i></span>
                                            <div class="title-gift">
                                                <h3>{$product.title}</h3>
                                                <span class="qty">Số lượng: {$item.qty} - Giá: {$product.price|Filter::NumberFormat} đ</span>
                                            </div>
                                        </div>
                                    </div>
                                {/if}
                            {/for}
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="cart-sumary panel-default panel">
                        <div class="panel-body">
                            <p class="p_sum">Tổng giá: <strong>{$ptotal|Filter::NumberFormat} đ</strong></p>
                            <p class="p_discount">Giảm giá: <strong>-{$giamgia|Filter::NumberFormat} đ</strong></p>
                            {if($discountCupon>0)}<p class="p_discount">Giảm giá theo mã: <strong>-{$discountCupon|Filter::NumberFormat} đ</strong></p> {/if}
                            <p class="p_total">Thành tiền: <strong>{$phaitra|Filter::NumberFormat} đ</strong></p>
                        </div>
                    </div>
                    <a href="/check-out.html" id="goCheckout">Tiến hành đặt hàng</a>
                    <div class="panel-group coupon" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne3" class="" aria-expanded="true">
                                        Mã giảm giá / Quà tặng
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne3" class="panel-collapse collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    {if(empty($Cupon))}
                                        <div class="input-group">
                                            <input id="coupon" placeholder="Nhập ở đây.." type="text" class="form-control" value="">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default btn-coupon" type="button" onclick="addCupon()">Đồng ý</button>
                                            </span>
                                        </div>
                                        {else}
                                        <div class="input-group">
                                            <input id="coupon" type="text" class="form-control" value="{$Cupon.code}" disabled>
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-coupon" type="button" onclick="removeCupon()">Xóa mã</button>
                                        </span>
                                        </div>
                                        <p class="usercode">Bạn đã sử dụng mã: {$Cupon.code}.
                                            {if($Cupon.discount_type==1)}
                                                Bạn được giảm : {$Cupon.discount|Filter::NumberFormat} đ
                                                {else}
                                                Bạn được giảm : {$Cupon.discount} % tổng đơn hàng
                                            {/if}
                                        </p>
                                    {/if}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <p class="lovebook_xu">
                                Với mỗi 100.000 trong đơn hàng các bạn sẽ tích lũy được 1000 xu trong tài khoản của các ban.
                                Xem thông tin về <a href="">Lovebook Xu</a>
                            </p>
                        </div>
                    </div>
                    -->
                </div>
            </div>
        </div>
    </div>
</div>