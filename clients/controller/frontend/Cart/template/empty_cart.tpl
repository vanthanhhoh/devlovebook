<div class="empty_cart">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-md-offset-3 col-sm-3 col-sm-offset-3 col-xs-6">
                <div class="bg_empty"></div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <h1 class="title_empty">Giỏ hàng của bạn<br><span>đang rỗng</span></h1>
                <a class="returnbuy" href="{#BASE_DIR}">Click vào đây để tiếp tục mua hàng</a>
            </div>
        </div>
    </div>
</div>