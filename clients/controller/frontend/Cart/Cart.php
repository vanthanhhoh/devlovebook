<?php
/**
 * Created by PhpStorm.
 * User: THANH
 * Date: 09/07/2016
 * Time: 10:18 SA
 */

class Cart extends Controller {

    public function viewCart(){
        Theme::SetTitle('Giỏ hàng | lovebook.vn');
        Helper::State('Giỏ hàng',Router::GenerateThisRoute());

        if(isset($_SESSION['cart'])){
            if(!empty($_SESSION['cart'])){
                $p = frontend::$product;
                $total = 0; // Giá gốc
                $phaitra= 0; // Giá đã tính giảm giá
                $discountCupon =0; // Giảm giá cupon
                foreach($_SESSION['cart'] as $item){
                    $isP = $p[$item['product_id']];
                    $total = $total+($isP['price']*$item['qty']);
                    $phaitra = $phaitra+$item['total'];
                }
                if(isset($_SESSION['cupon'])){
                    $Cupon = $_SESSION['cupon'];
                    if($Cupon['discount_type']==1){
                        $phaitra = $phaitra-$Cupon['discount'];
                        $discountCupon = $Cupon['discount'];
                    }
                    else {
                        $discountCupon = ceil($phaitra*($Cupon['discount']/100));
                        $phaitra = $phaitra-$discountCupon;
                    }
                }
                else $Cupon = array();
                $v = $this->View('cart')
                          ->Assign('cart',$_SESSION['cart'])
                          ->Assign('p',$p)
                          ->Assign('Cupon',$Cupon)
                          ->Assign('ptotal',$total)
                          ->Assign('phaitra',$phaitra)
                          ->Assign('discountCupon',$discountCupon)
                          ->Assign('giamgia',$total-$phaitra)
                          ->Assign('num',count($_SESSION['cart']));
                $this->Render($v->Output());
            }
            else {
                $v = $this->View('empty_cart');
                $this->Render($v->Output());
            }
        }
        else {
            $v = $this->View('empty_cart');
            $this->Render($v->Output());
        }
    }
    public function addCart(){
        $id = Input::Post('id',0);
        $qty = Input::Post('qty',0);
        if(isset(G::$Registry['Params'][ROUTER_EXTRA_KEY])) {
            $id = G::$Registry['Params'][ROUTER_EXTRA_KEY];
            $qty =1;
        }
        $result = array(
            'status' =>0,
            'message'=>'',
            'total'  =>0,
            'add_on'=>0
        );
        $product = DB::Query('product')->Where('product_id','=',$id)->Get();
        if($product->status && $product->num_rows==1){
            $product = $product->Result[0];
            $gift = array();
            if($product['gift']!=''){ // Check sản phẩm có quà tặng không
                $gift = unserialize($product['gift']); // Lấy ra data quà tặng
            }
            if($product['price_sale']>0){
                $cprice = $product['price_sale'];
            }
            else $cprice = $product['price'];


            //Check cart Session empty
            if(!isset($_SESSION['cart'])){
                $_SESSION['cart'] = array();
                $_SESSION['cart'][]=array(
                    'product_id' => $id,
                    'qty'=>$qty,
                    'type'=>1,
                    'price'=>$cprice,
                    'total'=>$cprice*$qty,
                    'parent_id'=>''
                );
                if(!empty($gift)){
                     foreach($gift as $key => $item){
                         $_SESSION['cart'][] = array(
                             'product_id' => $item['product_id'],
                             'qty'=>$item['qty'],
                             'type'=>2,
                             'price'=>0,
                             'total'=>0,
                             'parent_id'=>$id
                         );
                     }
                }
            }
            //Have cart session cart
            else {
                $hsItem = false;
                foreach($_SESSION['cart'] as $skey => $sitem){
                    if($sitem['product_id']==$id && $sitem['type']==1){
                        $hsItem = true;
                        // Cập nhật thêm số liệu
                        $_SESSION['cart'][$skey]['qty']+=$qty;
                        $_SESSION['cart'][$skey]['price']=$cprice;
                        $_SESSION['cart'][$skey]['total']+=$cprice*$qty;
                    }
                }
                if(!$hsItem){
                    // Chưa có sản phẩm trong cart
                    $_SESSION['cart'][] = array(
                        'product_id' => $id,
                        'qty'=>$qty,
                        'type'=>1,
                        'price'=>$cprice,
                        'total'=>$cprice*$qty,
                        'parent_id'=>''
                    );
                }
                if(!empty($gift)){ //Product has Gift
                    $hsGift = false;
                    foreach($gift as $key => $item){
                        foreach($_SESSION['cart'] as $gkey => $gitem){
                            if($gitem['product_id']==$key && $gitem['type']==2){
                                $_SESSION['cart'][$gkey]['qty']+=$item['qty'];
                                $hsGift = true;
                            }
                        }
                    }
                    if(!$hsGift){
                        foreach($gift as $key => $item){
                            $_SESSION['cart'][] = array(
                                'product_id' => $item['product_id'],
                                'qty'=>$item['qty'],
                                'type'=>2,
                                'price'=>0,
                                'total'=>0,
                                'parent_id'=>$id
                            );
                        }
                    }
                }
            }
            //return
            $result['total'] = count($_SESSION['cart']);
            $result['message'] ='Thêm vào giỏ hàng thành công';
            $result['status']=1;
        }
        else {
            $result['message'] ='Sản phẩm không tồn tại trong hệ thống';
        }
        if(isset(G::$Registry['Params'][ROUTER_EXTRA_KEY])) {
            header('location:'.'/gio-hang.html');
        }
        sleep(1);
        echo json_encode($result);die();
    }
    public function removeItem(){
        $id = Input::Post('id',0);
        $result= array(
            'status'=>0,
            'message'=>'',
            'total'=>0
        );
        if(isset($_SESSION['cart'])){ // Make sure has session cart.
            if(!empty($_SESSION['cart'])){
                $hs = false;
                foreach($_SESSION['cart'] as $cKey => $cItem){
                    if($cItem['product_id']==$id && $cItem['type']==1) {
                        unset($_SESSION['cart'][$cKey]);
                        $hs = true;
                        $result['total'] = count($_SESSION['cart']);
                    }
                    if($cItem['parent_id']==$id && $cItem['type']==2) {
                        unset($_SESSION['cart'][$cKey]);
                        $hs = true;
                        $result['total'] = count($_SESSION['cart']);
                    }
                }
                if($hs){
                    $result['message']='Xóa sản phẩm thành công!';
                    $result['status']=1;
                }
            }
            else {
                $result['message']='Không tồn tại giỏ hàng này';
            }

        }
        else{
            $result['message']='Không tồn tại giỏ hàng này';
        }
        sleep(1);
        echo json_encode($result);die();
    }
    public function removeCart(){

    }
    public function changeItem(){
        $id = Input::Post('id');
        $qty = Input::Post('qty');
        $result = array(
            'status' =>0,
            'message'=>'',
            'total'  =>0,
            'add_on'=>0
        );
        $product = DB::Query('product')->Where('product_id','=',$id)->Get();
        if($product->status && $product->num_rows==1){
            $product = $product->Result[0];
            $gift = array();
            if($product['gift']!=''){
                $gift = unserialize($product['gift']);
            }
            $giftID= array();
            foreach($gift as $gKey => $gItem){
                $giftID[]=$gItem['product_id'];
            }


            if(isset($_SESSION['cart'])){
                if(!empty($_SESSION['cart'])){
                    $hs = false;
                    foreach($_SESSION['cart'] as $skey => $sitem){
                        if($sitem['product_id']==$id){
                            $_SESSION['cart'][$skey]['qty'] = $qty;
                            $_SESSION['cart'][$skey]['total'] = $qty*$sitem['price'];
                            $hs = true;
                        }
                        if(in_array($sitem['product_id'],$giftID) && $sitem['type']==2){
                            $_SESSION['cart'][$skey]['qty'] = $qty*$gift[$sitem['product_id']]['qty'];
                        }
                    }
                    if($hs){
                        $result['status']=1;
                        $result['message']='Cập nhật thành công';
                        $result['total']=count($_SESSION['cart']);
                    }
                }
                else {
                    $result['message'] ='Không tồn tại giỏ hàng';
                }
            }
            else {
                $result['message'] ='Không tồn tại giỏ hàng';
            }

        }
        else {
            $result['message'] ='Sản phẩm không tồn tại trong hệ thống';
        }
        sleep(3);
        echo json_encode($result);die();
    }

    public function addCupon(){
        $code = Input::Post('cupon','');
        $cupon = DB::Query('cupon')->Where('code','=',$code)->Get();
        $result = array(
            'status'=>0,
            'message'=>''
        );
        if(USER_ID>0){
            if($cupon->status && $cupon->num_rows==1){
                $cupon= $cupon->Result[0];
                if($cupon['date_exp']>CURRENT_TIME){
                    $userAllow = explode(',',$cupon['apply']);
                    if(in_array(-1,$userAllow) || in_array(USER_ID,$userAllow)){
                        $checkUser = DB::Query('invoice')->Where('user_id','=',USER_ID)->_AND()->Where('cupon','=',$code)->Get();
                        if($checkUser->status && $checkUser->num_rows==0){
                            $result['status']=1;
                            if($cupon['discount_type']==1){
                                $result['message']= 'Đã áp dụng mã giảm giá '.$cupon['code'].'! Bạn được giảm giá '.Filter::NumberFormat($cupon['discount']) .'đ';
                            }
                            else {
                                $result['message']= 'Đã áp dụng mã giảm giá '.$cupon['code'].'! Bạn được giảm giá '.$cupon['discount'].'%';
                            }
                            $_SESSION['cupon']= array(
                                'code'=>$cupon['code'],
                                'discount'=>$cupon['discount'],
                                'discount_type'=>$cupon['discount_type']
                            );
                        }
                        else {
                            $result['message']='Bạn đã sửa dụng mã này! Mã giảm giá chỉ được áp dụng một lần duy nhất cho một tài khoản';
                        }
                    }
                    else {
                        $result['message']='Bạn không thuộc nhóm được áp dụng mã giảm giá này!';
                    }
                }
                else {
                    $result['message']='Mã giảm giá không còn hiệu lực!';
                }
            }
            else {
                $result['message']='Mã giảm giá không tồn tại.! Vui lòng nhập mã khác';
            }
        }
        else {
            $result['status'] = -1;
            $result['message']='Bạn cần phải đăng nhập để sử dụng mã giảm giá';
        }
        sleep(1);
        echo json_encode($result);die();
    }
}