<?php
/**
 * Created by PhpStorm.
 * User: THANH
 * Date: 27/06/2016
 * Time: 4:10 CH
 */

class paymentModule extends Controller {
    public function thenoidia(){
        if(Input::Post('get_content',0)==1){
            echo $this->View('thenoidia')->Output();
            die();
        }
        if(Router::get('invoice',0)>0){
            $invoice = DB::Query('invoice')
                ->Where('invoice_id','=',Router::get('invoice',0))
                ->_AND()
                ->Where('user_id','=',USER_ID)->Get();
            if($invoice->status && $invoice->num_rows==1){
                $invoice= $invoice->Result[0];
                $total = 0;
                $order = DB::Query('order')->Where('order_id','INCLUDE',$invoice['order_id'])->Get()->Result;
                $array_items = array();
                foreach($order as $orders){
                    $total = $total+$orders['total'];
                }
                $discountCupon =0 ;
                $ship = DB::Query('ship')->Where('ship_id','=',$invoice['ship'])->Get()->Result[0]['fee'];
                if($invoice['cupon']!='' && $invoice['cupon_discount']>0 && $invoice['cupon_type']!='' ){
                    if($invoice['cupon_type']==1){
                        $discountCupon = $invoice['cupon_discount'];
                    }
                    else {
                        $discountCupon = $total*($invoice['cupon_discount']/100);
                        $discountCupon = ceil($discountCupon);
                    }
                }
                $total = $total - $discountCupon;
                Boot::Library('NL_CheckOutV3');
                define('URL_API','https://www.nganluong.vn/checkout.api.nganluong.post.php'); // Đường dẫn gọi api
                define('RECEIVER','demo@nganluong.vn'); // Email tài khoản ngân lượng
                define('MERCHANT_ID', '36680'); // Mã merchant kết nối
                define('MERCHANT_PASS', 'matkhauketnoi'); // Mật khẩu kết nôi
                $nlcheckout= new NL_CheckOutV3(MERCHANT_ID,MERCHANT_PASS,RECEIVER,URL_API);
                $total_amount=$total;
                $bank_code = Router::get('bankcode','VCB');
                $order_code = $invoice['code'];
                $payment_type ='';
                $discount_amount =0;
                $order_description='Thanh toán hóa đơn : '.$invoice['code'];
                $tax_amount=0;
                $fee_shipping=$ship;
                //$verifyKey = Access::
                $return_url =APP_DOMAIN.'/CheckOut/Success/'.$invoice['invoice_id'];
                $cancel_url =urlencode(APP_DOMAIN.'/CheckOut/Failed/'.$invoice['invoice_id']) ;
                $buyer_fullname = $invoice['name'];
                $buyer_mobile =$invoice['phone'];
                $buyer_address =$invoice['address'];
                $buyer_email = $_SESSION['UserInfo']['email'];
                $nl_result= $nlcheckout->BankCheckout($order_code,$total_amount,$bank_code,$payment_type,$order_description,$tax_amount,
                    $fee_shipping,$discount_amount,$return_url,$cancel_url,$buyer_fullname,$buyer_email,$buyer_mobile,
                    $buyer_address,$array_items) ;
                n($nl_result);
                if ($nl_result->error_code =='00'){
                    header('Location:'.$nl_result->checkout_url);
                }
            }
        }
    }
    public function thequocte(){

    }
    public function chuyenkhoan(){
        if(Input::Post('get_content',0)==1){
            echo $this->View('chuyenkhoan')->Output();
            die();
        }
    }
}