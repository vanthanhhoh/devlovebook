<?php

class comments extends Controller {
    static $cmtObj = NULL;
    public function __construct() {
        $this->ControllerInitializer(__FILE__);
    }
    static function getInstance() {
        if(self::$cmtObj == NULL) self::$cmtObj = new self();
        return self::$cmtObj;
    }
    static function mainForm($nodeID, $nodeType) {
        $cmtObj = self::getInstance();
        $photo = (USER_ID > 0) ? G::$MyData['avatar'] : '';
        return $cmtObj->View('main_form')
            ->Assign(
                array(
                    'nodeType' => $nodeType,
                    'nodeID' => $nodeID,
                    'photo' => $photo)
            )
            ->Output();
    }
    static function listComment($nodeID, $nodeType) {
        $cmtObj = self::getInstance();
        $comments = $firstLevelComments = $firstLevelCmtIDTs = array();

        /***** Get first level comments *****/
        $getFirstLevelComments = DB::CustomQuery(
            'SELECT c.cid, c.name, c.mail, c.website, c.photo, c. content, c.identity, c.add_time, c.total_likes, c.reports, c.total_shares, u.userid, u.username, u.fullname, u.avatar, u.email FROM ' . DB::Prefix() . 'comments c LEFT JOIN ' . DB::Prefix() . 'users u ON c.userid = u.userid Where c.node_type = \'' . Filter::cleanString($nodeType) . '\' AND c.nodeid = ' . intval($nodeID) . ' AND c.level = 0 ORDER BY c.add_time DESC;'
        );

        while($cmt = $getFirstLevelComments->fetch_assoc()) {
            if($cmt['userid'] > 0) {
                $cmt['mail'] = $cmt['email'];
                $cmt['name'] = !empty($cmt['fullname']) ? $cmt['fullname'] : $cmt['username'];
                $cmt['photo'] = $cmt['avatar'];
            }
            $cmt['reports'] = sizeof(unserialize($cmt['reports']));
            $cmt['level'] = sizeof(array_filter(explode('.', $cmt['identity']))) - 1;
            $firstLevelComments[$cmt['identity']] = $cmt;
            $firstLevelCmtIDTs[] = "c.identity REGEXP '^" . $cmt['identity'] . "\\\.'";
            //\'^/cuisine/stir-fried-tender-beef-with-capsicum-bo-xao-ot-xanh-LPHnnJ\\,\'
        }

        if(!empty($firstLevelCmtIDTs)) {
            $firstLevelCmtIDTs = implode(" OR ", $firstLevelCmtIDTs);
            $sql = '
            SELECT
              c.cid, c.name, c.mail, c.website, c.photo, c. content, c.identity, c.add_time, c.total_likes, c.reports, c.total_shares, u.userid, u.username, u.fullname, u.avatar, u.email
            FROM
              ' . DB::Prefix() . 'comments c
            LEFT JOIN
                ' . DB::Prefix() . 'users u
            ON c.userid = u.userid
            Where
                c.node_type = \'' . Filter::cleanString($nodeType) . '\'
            AND
                c.nodeid = ' . intval($nodeID) . '
            AND
                (' . $firstLevelCmtIDTs . ')
            ORDER BY c.identity ASC;';
            $getComments = DB::CustomQuery($sql);
            while($cmt = $getComments->fetch_assoc()) {
                if($cmt['userid'] > 0) {
                    $cmt['mail'] = $cmt['email'];
                    $cmt['name'] = !empty($cmt['fullname']) ? $cmt['fullname'] : $cmt['username'];
                    $cmt['photo'] = $cmt['avatar'];
                }
                $cmt['reports'] = sizeof(unserialize($cmt['reports']));
                $cmt['level'] = sizeof(array_filter(explode('.', $cmt['identity']))) - 1;

                $identity = explode('.', $cmt['identity']);
                $identity = $identity[0];
                $firstLevelComments[$identity]['sub_comments'][$cmt['identity']] = $cmt;
            }
        }
        return $cmtObj->View('list_cmts')
            ->Assign(
                array(
                    'client_comment_likes' => G::$Session->Get('client_comment_likes', array()),
                    'nodeType' => $nodeType,
                    'nodeID' => $nodeID,
                    'comments' => $firstLevelComments)
            )
            ->Output();
    }
    static function getCommentIdentity($cmtID, $parent_id = 0, $parent_identity = '', $parent_child_nums = 0, $parent_level = 0) {
        if($parent_identity != '') $parent_identity .= '.';
        $cmt_identity = $parent_identity . str_pad($cmtID, 6, '0', STR_PAD_LEFT);
        return $cmt_identity;
    }
    public function post() {
        $cmt = Input::Post('cmt', array());
        $error = array();

        $cmt['nodeid'] = intval($cmt['nodeid']);
        $cmt['parent_id'] = intval($cmt['parent_id']);
        if(isset($cmt['name']))
            $cmt['name'] = Filter::cleanString($cmt['name']);
        else $cmt['name'] = '';
        if(isset($cmt['mail']))
            $cmt['mail'] = Filter::cleanString($cmt['mail']);
        else $cmt['mail'];
        if(isset($cmt['website']))
            $cmt['website'] = Filter::cleanString($cmt['website']);
        else $cmt['website'] = '';
        $cmt['content'] = Filter::cleanString($cmt['content']);
        $cmt['node_type'] = Filter::cleanString($cmt['node_type']);

        if($cmt['parent_id'] != 0) {
            $getParent = DB::Query('comments')->Where('cid', '=', $cmt['parent_id'])->Get();
            if($getParent->status && $getParent->num_rows == 1) {
                $parent_cmt = $getParent->Result[0];
                $cmt['level'] = intval($parent_cmt['level']) + 1;
            }
            else $cmt['parent_id'] = 0;
        }
        else $cmt['level'] = 0;

        if(USER_ID > 0) {
            $cmt['userid'] = USER_ID;
            unset($cmt['name'], $cmt['email']);
        }
        else {
            if(empty($cmt['name'])) $error[] = 'Name không thể để trống';
            if(empty($cmt['mail'])) $error[] = 'Email không thể để trống';
            elseif(!Filter::CheckValidEmail($cmt['mail'])) $error[] = 'Email is not valid';
        }
        if(empty($cmt['content'])) $error[] = 'Content không thể để trống';

        if(empty($error)) {
            $cmt['add_time'] = CURRENT_TIME;
            $cmt['client_ip'] = Request::getClientIP();
            $cmt['reports'] = 'a:0:{}';
            $addCmt = DB::Query('comments')->Insert($cmt);
            if($addCmt->status && $addCmt->insert_id > 0) {
                $cmtID = $addCmt->insert_id;
                if($cmt['parent_id'] == 0)
                    $cmt['identity'] = self::getCommentIdentity($cmtID);
                else
                    $cmt['identity'] = self::getCommentIdentity($cmtID, $cmt['parent_id'], $parent_cmt['identity']);
                DB::Query('comments')
                    ->Where('cid', '=', $cmtID)
                    ->Update(array('identity' => $cmt['identity']));
                if(defined('IS_AJAX') && IS_AJAX == 1) {

                }
                else {
                    $refUrl = Request::getEnv('HTTP_REFERER');
                    header('LOCATION: ' . $refUrl . '#comment-' . $addCmt->insert_id);
                }
            }
        }
    }

    public function reply() {
        //n(G::$MyData);
        $node_type = Input::Post('node_type', '');
        $node_id = Input::Post('node_id');
        $parent_id = Input::Post('parent_id');
        $photo = (USER_ID > 0) ? G::$MyData['avatar'] : '';
        echo $this->View('reply')
            ->Assign(
                array(
                    'node_type' => $node_type,
                    'node_id' =>$node_id,
                    'parent_id' => $parent_id,
                    'photo' => $photo
                )
            )->Output();
        die();
    }

    public function like() {
        $cid = Input::Post('cid');
        $ud = false;
        $likes = G::$Session->Get('client_comment_likes', array());
        if(!in_array($cid, $likes)) {
            $getCmt = DB::Query('comments')->Where('cid', '=', $cid)->Get();
            if($getCmt->status && $getCmt->num_rows == 1) {
                $uc = DB::Query('comments')->Where('cid', '=', $cid)
                    ->Update(array('total_likes' => 1), array('total_likes' => 'INC'));
                if($uc->status && $uc->affected_rows = 1) {
                    $ud = true;
                    $likes[] = $cid;
                    G::$Session->Set('client_comment_likes', array_unique(array_filter($likes)));
                }
            }
        }
        if($ud)
            echo json_encode(
                array(
                    'status' => 'OK',
                    'like' => $getCmt->Result[0]['total_likes'] + 1
                )
            );
        else echo json_encode(
            array('status' => 'NOT')
        );
        die();
    }
}