{do}
    $noImage = APPLICATION_DATA_DIR . 'images/anonymous.png';
    Theme::JsFooter('comment', APPLICATION_DATA_DIR . 'static/js/comment.js');
{/do}
{if(!empty($comments))}
    {do}
        $total_cmts = sizeof($comments);
        foreach($comments as $__C) {
            if(isset($__C['sub_comments'])) $total_cmts += sizeof($__C['sub_comments']);
        }
    {/do}
    <h4>{{lang::comment_has:$total_cmts}}</h4>
{/if}
<ul class="comments-list" id="comments">
        {for $cmt in $comments as $idt}
        {do}
            $avatar = !empty($cmt['photo']) ? $cmt['photo'] : $noImage;
            $time = Filter::UnixTimeToDate($cmt['add_time']);
        {/do}
        <li class="level-{$cmt.level}" id="comment-{$cmt.cid}">
            <img class="cmt-photo" src="{$avatar|Output::GetThumbLink:42,42}" />
            <div class="cmt-content">
                <a href="{$cmt.website}" rel="nofollow" class="cmt-username">{$cmt.name}</a>
                {$cmt.content}
                <div class="cmt-meta">
                    <span class="cmt-time">{$time.hour}:{$time.minute} - {$time.date}</span>
                    <a href="#reply" class="cmt-reply" data-nodetype="{$nodeType}" data-nodeid="{$nodeID}" data-parent="{$cmt.cid}">{{lang::comment_do_reply}}</a>|
                    <a href="#like" class="cmt-like" data-comment="{$cmt.cid}"{if(in_array($cmt.cid, $client_comment_likes))} disable="true"{/if}>{{lang::comment_do_like}}<span>{$cmt.total_likes}</span></a>|
                    <a href="#share" class="cmt-share" data-nodetype="{$nodeType}" data-nodeid="{$nodeID}" data-parent="{$cmt.cid}">{{lang::comment_do_share}}<span>{$cmt.total_shares}</span></a>|
                    <a href="#report" class="cmt-report" data-nodetype="{$nodeType}" data-nodeid="{$nodeID}" data-parent="{$cmt.cid}">{{lang::comment_do_report}}<span>{$cmt.reports}</span></a>
                </div>
            </div>
            {if(isset($cmt['sub_comments']) && !empty($cmt['sub_comments']))}
                <ul class="cmt-subreply">
                {for $scmt in $cmt.sub_comments}
                    {do}
                        $avatar = !empty($scmt['photo']) ? $scmt['photo'] : $noImage;
                        $time = Filter::UnixTimeToDate($cmt['add_time']);
                    {/do}
                    <li class="level-{$scmt.level}" id="comment-{$scmt.cid}" style="padding-left: {do}echo $scmt['level']*50{/do}px">
                        <img class="cmt-photo" src="{$avatar|Output::GetThumbLink:42,42}" />
                        <div class="cmt-content">
                            <a href="{$scmt.website}" rel="nofollow" class="cmt-username">{$scmt.name}</a>
                            {$scmt.content}
                            <div class="cmt-meta">
                                <span class="cmt-time">{$time.hour}:{$time.minute} - {$time.date}</span>
                                <a href="#reply" class="cmt-reply" data-nodetype="{$nodeType}" data-nodeid="{$nodeID}" data-parent="{$scmt.cid}">Reply</a>
                                <a href="#like" class="cmt-like" data-comment="{$cmt.cid}"{if(in_array($cmt.cid, $client_comment_likes))} disable="true"{/if}>Like<span>{$cmt.total_likes}</span></a>|
                                <a href="#share" class="cmt-share" data-nodetype="{$nodeType}" data-nodeid="{$nodeID}" data-parent="{$cmt.cid}">Share<span>{$cmt.total_shares}</span></a>|
                                <a href="#report" class="cmt-report" data-nodetype="{$nodeType}" data-nodeid="{$nodeID}" data-parent="{$cmt.cid}">Report<span>{$cmt.reports}</span></a>
                            </div>
                        </div>
                    </li>
                {/for}
                </ul>
            {/if}
        </li>
    {/for}
</ul>