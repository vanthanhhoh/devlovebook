<div class="comment-form">
    <h4>{{lang::leave_a_reply}}</h4>
    <form action="{do}echo Router::Generate('ControllerAction', array('controller' => 'comments', 'action' => 'post')){/do}" method="post" class="vform">
        <input type="hidden" name="cmt[node_type]" value="{$nodeType}">
        <input type="hidden" name="cmt[nodeid]" value="{$nodeID}">
        <input type="hidden" name="cmt[parent_id]" value="0">
        <div class="form-row cmt-user-content clearfix">
            {do}if(empty($photo)) $photo = APPLICATION_DATA_DIR . 'images/anonymous.png';{/do}
            <img class="cmt-photo" src="{$photo|Output::GetThumbLink:70,70}" />
            <div class="cmt-content">
                <textarea id="cmt-content-form" name="cmt[content]"></textarea>
            </div>
        </div>

        {if(USER_ID == 0)}
        <div class="toggle-cmt-info">
            <div class="form-row">
                <label>
                    <span>{{lang::comment_user_name}}<code class="require">*</code></span>
                    <input type="text" name="cmt[name]" />
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span>{{lang::comment_user_email}}<code class="require">*</code></span>
                    <input type="text" name="cmt[mail]" />
                </label>
            </div>
            <!--
            <div class="form-row">
                <label>
                    <span>Photo</span>
                    <input type="file" name="cmt[photo]" />
                </label>
            </div>
            -->
            <div class="form-row cmt-features">
                <input type="reset" value="{{lang::comment_form_reset}}" />
                <input type="submit" value="{{lang::comment_do_send}}" />
                <input type="checkbox" checked="checked" name="cmt[allow_notify]" value="1" />
                <span>{{lang::comment_allow_notification}}</span>
            </div>
        </div>
        <div class="form-row cmt-features">
            <input id="send-cmt" type="button" value="{{lang::comment_do_send}}" />
        </div>
        {else}
            <div class="form-row cmt-features">
                <input type="reset" value="{{lang::comment_form_reset}}" />
                <input type="submit" value="{{lang::comment_do_send}}" />
                <input type="checkbox" checked="checked" name="cmt[allow_notify]" value="1" />
                <span>{{lang::comment_allow_notification}}</span>
            </div>
        {/if}
        <!--
        <div class="form-row">
            <label>
                <span>Website</span>
                <input type="text" name="cmt[website]" />
            </label>
        </div>
        -->
    </form>
</div>