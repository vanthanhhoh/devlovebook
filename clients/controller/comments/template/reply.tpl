<form action="{do} echo Router::Generate('ControllerAction', array('controller' => 'comments', 'action' => 'post'), false){/do}" method="post" class="cmt-active-reply-form vform" id="cmt-reply-form">
    <input type="hidden" name="cmt[node_type]" value="{$node_type}">
    <input type="hidden" name="cmt[nodeid]" value="{$node_id}">
    <input type="hidden" name="cmt[parent_id]" value="{$parent_id}">
    <div class="form-row">
        {do}if(empty($photo)) $photo = APPLICATION_DATA_DIR . 'images/anonymous.png';{/do}
        <img class="cmt-photo" src="{$photo|Output::GetThumbLink:70,70}" />
        <div class="cmt-content">
            <textarea name="cmt[content]"></textarea>
        </div>
    </div>
    {if(USER_ID == 0)}
    <div class="form-row">
        <label>
            <span>{{lang::comment_user_name}}<code class="require">*</code></span>
            <input type="text" name="cmt[name]" />
        </label>
    </div>
    <div class="form-row">
        <label>
            <span>{{lang::comment_user_email}}<code class="require">*</code></span>
            <input type="text" name="cmt[mail]" />
        </label>
    </div>
    <!--
    <div class="form-row">
        <label>
            <span>Photo</span>
            <input type="file" name="cmt[photo]" />
        </label>
    </div>
    <div class="form-row">
        <label>
            <span>Website</span>
            <input type="text" name="cmt[website]" />
        </label>
    </div>
    -->
    {/if}
    <div class="form-row cmt-features">
        <input type="reset" value="{{lang::comment_form_reset}}" />
        <input type="submit" value="{{lang::comment_do_send}}" />
        <div style="margin-top:5px">
            <input type="checkbox" checked="checked" name="cmt[allow_notify]" value="1" />
            <span>{{lang::comment_allow_notification}}</span>
        </div>
    </div>
</form>