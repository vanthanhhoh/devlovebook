{$cssCpns = 'Glyphicons,Buttons,Labels,InputGroups,InputGroups,Tables,ButtonGroups,Dropdowns,Code'}
{$cssCpns|Theme::AddCssComponent}
<script type="text/javascript">
    var allThemes = {$allThemes|json_encode}
</script>
<script type="text/javascript">
    var routesLib = {$Routes|json_encode}
</script>
<div class="bs-example clearfix" style="padding-top: 15px">
    <div class="FeaturedPanel clearfix">
        <span class="VNP_PageInfo">Add new route profile</span>
    </div>
    <form class="form-horizontal" action="" method="post">
        <input type="hidden" name="saveRouteProfile" value="1" />
        <div class="input-group" style="margin-bottom: 10px">
            <span class="input-group-addon">Profile name</span>
            <input name="profile[name]" class="form-control" type="text"/>
        </div>
        <div class="input-group" style="margin-bottom: 10px">
            <span class="input-group-addon">From origin</span>
            <select name="profile[route_name]" class="form-control" id="routeSlector">
                <option value="">Select origin route</option>
                {for $route in $Routes as $rname}
                    {if(!Filter::startsWith('Ajax_', $rname) && !in_array($rname, array('ThumbnailHandler', 'LoginAction', 'LogoutAction')))}
                        <option value="{$rname}">{$rname}</option>
                    {/if}
                {/for}
            </select>
            <span class="input-group-addon">Language</span>
            <select name="profile[route_lang]" class="form-control">
                <option value="default">Default language</option>
                {for $lang in $Langs}
                    <option value="{$lang.code}">{$lang.name}</option>
                {/for}
            </select>
        </div>
        <div class="input-group">
            <span class="input-group-addon">Route theme</span>
            <select name="profile[route_theme]" id="routeThemes" class="form-control">
                <option value="default">Default theme</option>
                {for $theme in $allThemes as $tName}
                    <option value="{$tName}">{$theme.name}</option>
                {/for}
            </select>
            <span class="input-group-addon">Route layout</span>
            <select name="profile[route_layout]" class="form-control" id="routeLayouts">
                <option value="layout.default">Default layout</option>
            </select>
        </div>
        <div id="paramsContainer">

        </div>
        <div class="form-group">
            <center><button class="btn btn-primary" type="submit">Add route profile</button></center>
        </div>
    </form>
</div>
<div class="highlight table-responsive" style="padding-top: 15px;">
    <table class="table table-bordered table-striped table-hover">
        <colgroup>
            <col class="col-xs-2">
            <col class="col-xs">
            <col class="col-xs-6">
            <col class="col-xs">
            <col class="col-xs">
            <col class="col-xs">
        </colgroup>
        <thead>
        <tr>
            <td><strong>Name</strong></td>
            <td><strong>Origin</strong></td>
            <td><strong>Route</strong></td>
            <td><strong>Language</strong></td>
            <td><strong>Theme</strong></td>
            <td><strong>Layout</strong></td>
            <td><strong><span class="glyphicon glyphicon-trash"></span></strong></td>
        </tr>
        </thead>
        <tbody >
        {for $profile in $allProfiles}
            {function}
                if(isset($Langs[$profile['route_lang']]))
                $lang = $Langs[$profile['route_lang']]['name'];
                else $lang = $profile['route_lang'];

                if(isset($allThemes[$profile['route_theme']]))
                $theme = $allThemes[$profile['route_theme']]['name'];
                else $theme = $profile['route_lang'];
            {/function}
            <tr class="routeProfile-{$profile.name}">
                <td><label class="label label-success" style="font-size: 12px">{$profile.name}</label></td>
                <td><label class="label label-danger" style="font-size: 12px">{$profile.route_name}</label></td>
                <td><code>{$profile.route_name|Router::Generate:$profile['params'], false}</code></td>
                <td><label class="label label-primary" style="font-size: 12px">{$lang}</label></td>
                <td><label class="label label-info" style="font-size: 12px">{$theme}</label></td>
                <td><label class="label label-success" style="font-size: 12px">{$profile.route_layout}</label></td>
                <td><a href="#" data-profile="{$profile.name}" class="removeProfile"><span class="glyphicon glyphicon-trash"></span></a></td>
            </tr>
        {/for}
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $('#routeSlector').change(function(e) {
        e.preventDefault();
        var routeName = $(this).val();
        var extraParams = '';
        if(typeof(routesLib[routeName]) !== 'undefined') {
            var routeProfile = routesLib[routeName];
            extraParams += '<div class="input-group" style="margin-top:10px; font-size: 16px">' +
            '<span class="label label-danger">' + routeProfile.origin + '</span></div>';
            if(typeof(routeProfile.params) !== 'undefined') {
                var routeParams = routeProfile.params;
                $.each(routeParams, function(route_name, route_type) {
                    extraParams += '<div class="input-group" style="margin-top:10px">\
                    <span class="input-group-addon">' + route_name + '</span>\
                    <input type="text" name="profile[params][' + route_name + ']" class="form-control">\
                </div>';
                });
            }
        }
        $('#paramsContainer').html(extraParams);
    });
    $('#routeThemes').change(function(e) {
        e.preventDefault();
        var sltedTheme = $(this).val();
        var layoutOptions = '<option value="default">Default layout</option>';
        if(sltedTheme !== 'default') {
            $.each(allThemes[sltedTheme].layout, function (i, elm) {
                layoutOptions += '<option value="' + i + '">' + elm + '</option>';
            });
        }
        $('#routeLayouts').html(layoutOptions);
    });
    $('.removeProfile').click(function(e) {
        e.preventDefault();
        var profileName = $(this).attr('data-profile');
        if(confirm('Confirm remove this profile?'))
        VNP.Ajax({
            type: 'POST',
            data: {profile: profileName},
            url: 'Setting/removeRoute',
            success: function(res) {
                if(res.status == 'OK') {
                    $('.routeProfile-' + profileName).remove();
                    VNP.Loader.TextNotify('Success remove route profile', 'success');
                }
                else
                    VNP.Loader.TextNotify('Cannot remove route profile', 'error');
            },
            error: function(res) {
                VNP.Loader.TextNotify('Cannot remove route profile', 'error');
            }
        }, 'json')
    })
</script>