<form action="" class="" id="" method="post">
    <input type="hidden" name="saveOptions" value="1" />
    {for $option in $options as $opName}
    <div class="form-group FieldWrap FieldType_text Field_phone">
        <label class="col-sm-3 control-label" for="ID_Field[{$opName}]">{$option.title}</label>
        <div class="col-sm-9">
            {if($opName == 'default_lang')}
                <select name="Field[{$opName}]" id="ID_Field[{$opName}]" class="form-control">
                {for $lang in $Langs}
                    <option value="{$lang.code}"{if($option.value == $lang.code)} selected="selected" {/if}>{$lang.name}</option>
                {/for}
                </select>
            {elseif($opName == 'default_layout')}
                <select name="Field[{$opName}]" id="ID_Field[{$opName}]" class="form-control">
                    {for $layout in $layouts as $lName}
                        <option value="{$lName}"{if($option.value == $lName)} selected="selected" {/if}>{$layout}</option>
                    {/for}
                </select>
            {elseif($opName == 'user_active_mod')}
                <select name="Field[{$opName}]" id="ID_Field[{$opName}]" class="form-control">
                    <option value="auto"{if($option.value == 'auto')} selected="selected" {/if}>Always active</option>
                    <option value="send_email"{if($option.value == 'send_email')} selected="selected" {/if}>Send email</option>
                    <option value="admin_review"{if($option.value == 'admin_review')} selected="selected" {/if}>Admin review</option>
                </select>
            {elseif($opName == 'comment_approve')}
                <select name="Field[{$opName}]" id="ID_Field[{$opName}]" class="form-control">
                    <option value="auto"{if($option.value == 'auto')} selected{/if}>Auto</option>
                    <option value="author"{if($option.value == 'author')} selected{/if}>Node Author</option>
                    <option value="mod"{if($option.value == 'mod')} selected{/if}>Moderator</option>
                    <option value="smod"{if($option.value == 'smod')} selected{/if}>Super Moderator</option>
                    <option value="admin"{if($option.value == 'admin')} selected{/if}>Administrator</option>
                </select>
            {elseif($opName == 'email_method')}
                <select name="Field[{$opName}]" id="ID_Field[{$opName}]" class="form-control">
                    <option value="smtp"{if($option.value == 'smtp')} selected{/if}>SMTP</option>
                    <option value="sendmail"{if($option.value == 'sendmail')} selected{/if}>SendMail (Linux mail)</option>
                    <option value="mail"{if($option.value == 'mail')} selected{/if}>Mail (PHP Mail)</option>
                </select>
            {elseif($opName == 'email_smtp_secure')}
                <select name="Field[{$opName}]" id="ID_Field[{$opName}]" class="form-control">
                    <option value="none"{if($option.value == 'none')} selected{/if}>None</option>
                    <option value="ssl"{if($option.value == 'ssl')} selected{/if}>SSL</option>
                    <option value="tls"{if($option.value == 'tls')} selected{/if}>TLS</option>
                </select>
            {else}
                <input type="text" name="Field[{$opName}]" id="ID_Field[{$opName}]" class="form-control" value="{$option.value}" />
            {/if}
        </div>
    </div>
    {/for}
    <input type="submit" value="Save" class="btn btn-primary">
</form>