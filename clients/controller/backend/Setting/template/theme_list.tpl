{$cssCpns = 'Glyphicons,Buttons,Labels,InputGroups,InputGroups,Tables,ButtonGroups,Dropdowns,Code'}
{$cssCpns|Theme::AddCssComponent}
<div class="selectTheme">
    <ul class="themeList">
        {for $theme in $allThemes as $tn}
            <li class="theme-{$tn}{if($currentTheme == $tn)} theme-actived{/if}">
                <div class="theme-item">
                    <img src="/Thumbnail/200_240{#APPLICATION_DATA_DIR}theme/{$tn}/sc.jpg" />
                    <div class="theme-info">
                        <span class="theme-name">{$theme.name}</span>
                        <div class="theme-action">
                            <a href="#" class="vnp-theme-active" data-theme="{$tn}"><span class="glyphicon glyphicon-cog"></span> Active</a>&nbsp&nbsp&nbsp
                            <a href="#" class="vnp-theme-delete" data-theme="{$tn}"><span class="glyphicon glyphicon-trash"></span> Delete</a>
                        </div>
                    </div>
                </div>
            </li>
        {/for}
    </ul>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.vnp-theme-active').click(function(event) {
            event.preventDefault();
            var theme = $(this).attr('data-theme');
            VNP.Ajax({
                type: 'POST',
                url: 'Setting/theme/active',
                data: {theme_name: theme},
                success: function(res) {
                    if(res.status == 'OK') {
                        $('.themeList li').removeClass('theme-actived');
                        $('.theme-' + theme).addClass('theme-actived');
                        VNP.Loader.TextNotify(res.msg, 'success');
                    }
                    else VNP.Loader.TextNotify(res.msg, 'error');
                },
                error: function() {
                    VNP.Loader.TextNotify('An error has occured, please again latter', 'error');
                }
            }, 'json');
        });
        $('.vnp-theme-delete').click(function(event) {
            event.preventDefault();
            if(confirm('Are you sure?')) {
                var theme = $(this).attr('data-theme');
                VNP.Ajax({
                    type: 'POST',
                    url: 'Setting/theme/delete',
                    data: {theme_name: theme},
                    success: function(res) {
                        if(res.status == 'OK') {
                            $('.themeList li.theme-' + theme).remove();
                            VNP.Loader.TextNotify(res.msg, 'success');
                        }
                        else VNP.Loader.TextNotify(res.msg, 'error');
                    },
                    error: function() {
                        VNP.Loader.TextNotify('An error has occured, please again latter', 'error');
                    }
                }, 'json');
            }
        });
    });
</script>