<?php

if( !defined('VNP_SYSTEM') && !defined('VNP_APPLICATION') && !defined('ADMIN_AREA') ) die('Access denied!');
Access::RequirePermission(Boot::ADMIN_SESSION);
class Setting extends Controller {
    public function Main() {
        $this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups');
        $allThemes = $this->getThemeLayouts();
        $currentTheme = Option::get('default_theme');
        if(Input::Post('saveOptions', array())) {
            $settings = Input::Post('Field');
            $i = 0;
            foreach($settings as $field => $value) {
                $u = DB::Query('options')
                        ->Where('name', '=', $field)->_AND()
                        ->Where('type', '=', 'global')
                        ->Update(array('value' => $value));
                if($u->status) $i++;
            }
            if($i == sizeof($settings))
                Helper::Notify('success', 'Success update settings');
            else
                Helper::Notify('error', 'Some field cannot be update, please try again!');
        }
        $this->Render(
            $this->View('global_setting')
            ->Assign(
                array(
                    'options' => Option::getOptionsOfType('global'),
                    'Langs' => G::$Config['lang_support'],
                    'layouts' => $allThemes[$currentTheme]['layout']
                )
            )
            ->Output()
        );
    }
    public function glob() {

    }
    public function route() {
        $allThemes = $this->getThemeLayouts();
        $routes = Router::GetRoutes(true, true);
        if(Input::Post('saveRouteProfile') == 1) {
            $routeProfile = Input::Post('profile');
            if(!isset($routeProfile['name']) || empty($routeProfile['name']))
                Helper::Notify('error', 'Empty route profile name');
            if(!isset($routeProfile['name']) || empty($routeProfile['name']))
                Helper::Notify('error', 'Empty route profile name');
            if(Helper::NotifyCount('error') == 0) {
                if(!isset($routeProfile['params'])) $routeProfile['params'] = array();
                $routeProfile['params'] = serialize($routeProfile['params']);
                if(Router::addProfile($routeProfile))
                    Helper::Notify('success', 'Success add profile!');
                else Helper::Notify('error', 'Cannot add profile, check if name existed!');
            }
        }
        $allProfiles = Router::getAllProfiles();
        $this->Render(
            $this->View('route')
                ->Assign('Routes', $routes)
                ->Assign('allThemes', $allThemes)
                ->Assign('Langs', G::$Config['lang_support'])
                ->Assign('allProfiles', $allProfiles)
                ->Output()
        );
    }

    private function getThemeLayouts() {
        $themeDir = APPLICATION_PATH . DATA_DIR . DIRECTORY_SEPARATOR . 'theme' . DIRECTORY_SEPARATOR . '*';
        $themes = array_filter(glob($themeDir), 'is_dir');
        $themeLibs = array();
        foreach($themes as $t) {
            if(file_exists($t . DIRECTORY_SEPARATOR . 'theme.ini') && basename($t) != 'admin-vnp') {
                $a = Input::parseInitFile($t . DIRECTORY_SEPARATOR . 'theme.ini', true);
                $themeLibs[basename($t)] = $a;
            }

        }
        return $themeLibs;
    }

    public function removeRoute() {
        $profileName = Input::Post('profile');
        $stt = array('status' => 'NOT');
        if(Router::removeProfile($profileName)) {
            $stt['status'] = 'OK';
        }
        echo json_encode($stt);
        die();
    }

    public function theme() {
        define('THEME_ACTION', true);
        $allThemes = $this->getThemeLayouts();
        require __DIR__ . DIRECTORY_SEPARATOR . 'theme.php';
        $theme = new themeAction($allThemes);

        $themeAction = '';
        if(isset(G::$Params[ROUTER_EXTRA_KEY])) $themeAction = G::$Params[ROUTER_EXTRA_KEY];
        $currentTheme = Option::get('default_theme');
        if(in_array($themeAction, array('active', 'delete', 'options'))) {
            $theme->$themeAction($currentTheme);
            return;
        }
        $theme->themeList($currentTheme);
    }
}