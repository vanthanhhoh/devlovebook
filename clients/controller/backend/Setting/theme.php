<?php

if(!defined('THEME_ACTION') || THEME_ACTION == false) die('Stop here!');

class themeAction extends Controller {
    private $allThemes;
    public function __construct($allThemes) {
        $this->allThemes = $allThemes;
        parent::ControllerInitializer(__FILE__);
    }

    public function options($currentTheme) {
        $themeData = $this->allThemes[$currentTheme];
        $layouts = $themeData['layout'];
        $layoutOptions = array();
        foreach($layouts as $lname => $ltext) $layoutOptions[] = array('value' => $lname, 'text' => $ltext, 'prefix' => '');
        $Form = new Form('theme_options', true);

        $Form->AddFormElement(
            $Form->select('default_layout')
                ->Label('Default layout')
                ->Value(Option::get('default_layout'))
                ->Options($layoutOptions)
        );
        //$this->Render($Form->Render());
        $themeOptions = $themeData['options'];
        $savedOptions = Option::getOptionsOfType('theme', $currentTheme);
        foreach($themeOptions as $op => $ov) {
            if(isset($savedOptions[$op]))
            $themeOptions[$op]['value'] = $savedOptions[$op]['value'];
        }
        $foptions = Option::formFromConfig('theme_options_' . $currentTheme, $themeOptions, array(), RECOMPILE_CONFIG_FORM);
        if(!empty($foptions['submitedValues'])) {
            //n($foptions['submitedValues']);
            $c = 0;
            foreach($foptions['submitedValues'] as $tok => $tov) {
                $q = Option::set($tok, $tov, $themeOptions[$tok]['label'], 'theme', $currentTheme);
                if($q) $c++;
            }
            if($c == sizeof($foptions['submitedValues']))
                Helper::Notify('success', 'Success save options');
            else Helper::Notify('error', 'Some options cannot be saved!');
        }
        $this->Render($foptions['form']);
    }

    public function themeList($currentTheme) {
        $this->Render(
            $this->View('theme_list')
                ->Assign('allThemes', $this->allThemes)
                ->Assign('currentTheme', $currentTheme)
                ->Output()
        );
    }

    public function active($currentTheme) {
        $themeName = Input::Post('theme_name', '');
        $rs = array('status' => 'NOT', 'msg' => 'Error');
        if(!empty($themeName) && array_key_exists($themeName, $this->allThemes)) {
            if($currentTheme == $themeName)
                $rs = array('status' => 'OK', 'msg' => 'Nothing was changed');
            else {
                $st = Option::set('default_theme', $themeName);
                $rs = $st ? array('status' => 'OK', 'msg' => 'Success change theme') : array('status' => 'NOT', 'msg' => 'Cannot active this theme');;
            }
        }
        echo json_encode($rs);
        die();
    }

    public function delete($currentTheme) {
        $themeName = Input::Post('theme_name', '');
        $rs = array('status' => 'NOT', 'msg' => 'Error');
        if(!empty($themeName) && array_key_exists($themeName, $this->allThemes)) {
            if($currentTheme == $themeName)
                $rs = array('status' => 'NOT', 'msg' => 'You cannot delete default theme');
            else {
                $themeDir = APPLICATION_PATH . DATA_DIR . DIRECTORY_SEPARATOR . 'theme' . DIRECTORY_SEPARATOR . $themeName;
                File::RemoveDirectory($themeDir);
                if(file_exists($themeDir))
                    $rs = array('status' => 'NOT', 'msg' => 'Cannot delete default theme');
                else $rs = array('status' => 'OK', 'msg' => 'Success delete default theme');
            }
        }
        echo json_encode($rs);
        die();
    }
}
?>