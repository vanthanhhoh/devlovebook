<div class="table-responsive">

	<form class="form-horizontal" action="{$SA}" method="post">
		<input type="hidden" name="SearchUsers" value="1">
		<table class="table table-bordered table-striped table-hover">
	        <colgroup>
	        	<col class="col-xs-2">
	            <col class="col-xs-1">
	            <col class="col-xs-2">
	            <col class="col-xs">
	            <col class="col-xs-2">
	            <col class="col-xs">
	            <col class="col-xs-1">
	            <col class="col-xs">
	            <col class="col-xs-1">
	            <col class="col-xs-1">
	        </colgroup>
	        <tbody>
	        	<tr>
	        		<td><input type="text" name="Search[q]" placeholder="Search keyword" class="form-control" value="{$Search.q}" /></td>
	        		<td><label class="control-label">{{lang::search_by}}</label></td>
	        		<td>
	        			{function}$SearchBy = array('title' => 'Title','url' => 'Url','meta_title' => 'Meta title', 'meta_desc' => 'Meta description', 'description' => 'Description'){/function}
	        			<select class="form-control" name="Search[searchby]">
	        				{for $SB in $SearchBy as $SK}
	        				<option value="{$SK}"{if($SK == $Search.searchby)} selected{/if}>{$SB}</option>
	        				{/for}
	        			</select>
	        		</td>
	        		<td><label class="control-label">Items</label></td>
	        		<td>
	        			<input type="number" name="Search[total_items]" class="form-control" value="{$Search.total_items}" />
	        		</td>
	        		<td><label class="control-label">Status</label></td>
	        		<td>
	        			<select class="form-control" name="Search[status]">
		        			<option value="all"{if($Search.status == 'all')} selected{/if}>All</option>
	        				<option value="1"{if($Search.status == '1')} selected{/if}>Active</option>
	        				<option value="0"{if($Search.status == '0')} selected{/if}>Inactive</option>
	        			</select>
	        		</td>
	        		<td><label class="control-label">Show</label></td>
	        		<td><input type="number" class="form-control" value="{$Search.limit}" name="Search[limit]" /></td>
	        		<td><input type="submit" class="btn btn-primary" value="Search" /></td>
				</tr>
			</tbody>
		</table>
	</form>


	{if(!empty($Tags))}
	<div class="btn-group" style="margin-bottom: 5px">
		<div class="btn-group">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				{{lang::action_btn}}
				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu" role="menu">
				<li><a href="#" id="VNP_DeactiveTag"><span class="glyphicon glyphicon-remove"></span>&nbsp&nbsp{{lang::deactive}}</a></li>
				<li><a href="#" id="VNP_ActiveTag"><span class="glyphicon glyphicon-ok"></span>&nbsp&nbsp{{lang::active}}</a></li>
				<li><a href="#" id="VNP_DeleteTag"><span class="glyphicon glyphicon-trash"></span>&nbsp&nbsp{{lang::delete}}</a></li>
			</ul>
		</div>
	</div>
    <table class="table table-bordered table-striped table-hover">
        <colgroup>
        	<col class="col-xs">
            <col class="col-xs">
        	<col class="col-xs">
        	<col class="col-xs">
        	<col class="col-xs">
        	<col class="col-xs">
        	<col class="col-xs">
        	<col class="col-xs">
        </colgroup>
        <thead>
	        <tr>
	        	<td><input id="toggle-all" value="1" type="checkbox" /></td>
	        	<td><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'Node', 'action' => 'Tags', 'params' => 'sortby/tid/order/' . $Search['re_order'])){/function}" >ID</a></td>
	        	<td><strong><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'Node', 'action' => 'Tags', 'params' => 'sortby/title/order/' . $Search['re_order'])){/function}" >Title</a></strong></td>
	        	<td><strong><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'Node', 'action' => 'Tags', 'params' => 'sortby/url/order/' . $Search['re_order'])){/function}" >Url</a></strong></td>
	        	<td><strong><strong><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'Node', 'action' => 'Tags', 'params' => 'sortby/meta_title/order/' . $Search['re_order'])){/function}" >Meta title</a></strong></strong></td>
	        	<td><strong><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'Node', 'action' => 'Tags', 'params' => 'sortby/meta_desc/order/' . $Search['re_order'])){/function}" >Meta description</a></strong></td>
	        	<td><strong><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'Node', 'action' => 'Tags', 'params' => 'sortby/image/order/' . $Search['re_order'])){/function}" >Image</a></strong></td>
	        	<td><strong><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'Node', 'action' => 'Tags', 'params' => 'sortby/total_items/order/' . $Search['re_order'])){/function}" >Items</a></strong></td>
	        	<td><strong>Featured</strong></td>
	        </tr>
	    </thead>
	    <tbody style="font-size:12px">
	    	{for $Tag in $Tags as $TID}
	    	<tr class="{if($Tag.status == 0)}Node_Inactive{else}Node_Active{/if}" id="tag-{$TID}">
	    		<td><input type="checkbox" value="{$TID}" data-title="{$TID} - {$Tag.url}" class="item-toggle" /></td>
	    		<td>{$TID}</td>
	    		<td>{$Tag.title}</td>
	    		<td>{$Tag.url}</td>
	    		<td>{$Tag.meta_title}</td>
	    		<td>{$Tag.meta_desc}</td>
	    		<td><img src="{$Tag.image|Output::GetThumbLink:50,50}" /></td>
	    		<td>{$Tag.total_items}</td>
	    		<td>
                	<a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'Node', 'action' => 'Tags', 'params' => 'AddNew/' . $TID)){/function}"><span class="glyphicon glyphicon-edit"></span>&nbsp{{lang::edit}}</a>&nbsp;&nbsp;
                    <a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'Node', 'action' => 'Tags', 'params' => 'Remove/' . $TID)){/function}"><span class="glyphicon glyphicon-trash"></span>&nbsp{{lang::delete}}</a>
              	</td>
	    	</tr>
	    	{/for}
	    </tbody>
    </table>
    {/if}
</div>
<script type="text/javascript">
function haha() {
}
function VNP_DeactiveTag(checkedInputs, titleData) {
	if(checkedInputs.length == 0) alert('You must select at least one item');
	else {
		if(confirm('Do you want to do this?')) {
			VNP.Ajax({
				url: 'Node/Action',
				type: 'POST',
				data: {action: 'deactive_tag',ids: checkedInputs},
				success: function(res) {
					if(res.status == 'ok') {
						$.each(res.items, function(i,v) {
							$('#tag-' + v).removeClass('Node_Active');
							$('#tag-' + v).addClass('Node_Inactive');
						});
						VNP.Loader.TextNotify('Success deactive tags!');
					}
				}
			}, 'json');
		}
	}
}
function VNP_ActiveTag(checkedInputs, titleData) {
	if(checkedInputs.length == 0) alert('You must select at least one item');
	else {
		if(confirm('Do you want to do this?')) {
			VNP.Ajax({
				url: 'Node/Action',
				type: 'POST',
				data: {action: 'active_tag',ids: checkedInputs},
				success: function(res) {
					if(res.status == 'ok') {
						$.each(res.items, function(i,v) {
							$('#tag-' + v).addClass('Node_Active');
							$('#tag-' + v).removeClass('Node_Inactive');
						});
						VNP.Loader.TextNotify('Success active tags!');
					}
				}
			}, 'json');
		}
	}
}
function VNP_DeleteTag(checkedInputs, titleData) {
	if(checkedInputs.length == 0) alert('You must select at least one item');
	else {
		if(confirm('Do you want to do this?')) {
			VNP.Ajax({
				url: 'Node/Action',
				type: 'POST',
				data: {action: 'delete_tag',ids: checkedInputs},
				success: function(res) {
					if(res.status == 'ok') {
						$.each(res.items, function(i,v) {
							$('#tag-' + v).remove();
						})
					}
					VNP.Loader.TextNotify('Success delete users!');
				}
			}, 'json');
		}
	}
}
$(document).ready(function() {
	var checkedInputs = '';
	var titleData = '';
	$('#toggle-all').InputToggle({
		childInput: '.item-toggle',
		dataAttribute: 'data-title',
		storageVar: 'checkedInputs',
		titleData:	'titleData',
		featureAction: [
			{container: '#VNP_DeactiveTag', callback: "VNP_DeactiveTag(checkedInputs, titleData)" },
			{container: '#VNP_ActiveTag', callback: "VNP_ActiveTag(checkedInputs, titleData)" },
			{container: '#VNP_DeleteTag', callback: "VNP_DeleteTag(checkedInputs, titleData)" }
		],
		callBackFunction: 'haha(checkedInputs)',
		enableCookie: true
	});
});
</script>