<form class="form-horizontal" action="" method="post">
	<input type="hidden" name="SaveTagSubmit" value="1"/>
	<input type="hidden" name="TID" value="{$TID}">
	<div class="FormPart FormPartCol-6">
		<div class="form-group FieldWrap RequiredField">
			<label class="col-sm-3 control-label">Title<span class="RequireField">*</span></label>
		    <div class="col-sm-9">
				<input type="text" name="Tag[title]" class="form-control FieldType_title RequiredField" value="{$Tag.title}">
		 	</div>
		</div>
		<div class="form-group FieldWrap RequiredField">
			<label class="col-sm-3 control-label">Url<span class="RequireField">*</span></label>
		    <div class="col-sm-9">
				<input type="text" name="Tag[url]" class="form-control RequiredField" value="{$Tag.url}">
		 	</div>
		</div>
		<div class="form-group FieldWrap RequiredField">
			<label class="col-sm-3 control-label" for="Tag_Image">Image</label>
		    <div class="col-sm-6">
				<input type="text" name="Tag[image]" id="Tag_Image" class="form-control RequiredField" value="{$Tag.image}">
		 	</div>
		 	<div class="col-sm-3">
				<button class="btn btn-primary" type="button" onclick="VNP.SugarBox.Open('FileManager','Tag_Image'); return false;">Browse</button>
				<button class="btn btn-danger" onclick="document.getElementById('Tag_Image').value = ''; document.getElementById('Thumb_ID_Tag_Image').setAttribute('src', 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=='); return false;">Delete</button>
			</div>
		    <div class="col-sm-8 FieldImagePreviewer"><img id="Thumb_Tag_Image"></div>
		</div>
		<div class="form-group FieldWrap RequiredField">
			<label class="col-sm-3 control-label">Keywords</label>
			<div class="col-sm-9">
				<textarea class="form-control clearfix" name="Tag[keyword]" id="Tag_Keyword">{$Tag.keyword}</textarea>
			</div>
		</div>
	</div>
	<div class="FormPart FormPartCol-4">
		<div class="form-group FieldWrap RequiredField">
			<label class="col-sm-3 control-label">Meta title</label>
		    <div class="col-sm-9">
				<input type="text" name="Tag[meta_title]" class="form-control FieldType_meta_title RequiredField" value="{$Tag.meta_title}">
		 	</div>
		</div>
		<div class="form-group FieldWrap RequiredField">
			<label class="col-sm-3 control-label">Meta description</label>
		    <div class="col-sm-9">
				<textarea name="Tag[meta_desc]" class="form-control FieldType_meta_title RequiredField">{$Tag.meta_desc}</textarea>
		 	</div>
		</div>
		<div class="form-group FieldWrap RequiredField">
			<label class="col-sm-3 control-label">Status<span class="RequireField">*</span></label>
		    <div class="col-sm-9">
		    	<select name="Tag[status]" class="form-control RequiredField">
		    		<option value="1"{if($Tag.status == 1)} selected{/if}>Active</option>
		    		<option value="0"{if($Tag.status == 0)} selected{/if}>Inactive</option>
				</select>
		 	</div>
		</div>
	</div>
	<div class="form-group FieldWrap RequiredField">
		<label class="col-sm-12 control-label" style="text-align:left;padding: 5px 15px">Description</label>
		<div class="col-sm-12">
			<textarea class="form-control clearfix" name="Tag[description]" id="Tag_Description">{$Tag.description}</textarea>
		</div>
	</div>
	<div class="clearfix"></div>
	<center><input type="reset" class="btn btn-info" value="Reset" />
	<input type="submit" class="btn btn-primary" value="Save" /></center>
</form>