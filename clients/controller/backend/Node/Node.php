<?php

if( !defined('VNP_SYSTEM') && !defined('VNP_APPLICATION') && !defined('ADMIN_AREA') ) die('Access denied!');

define('CONTROLLER_BASE_URL', Router::Generate('Controller',array('controller'=> 'Node')));

class Node extends Controller {

	public function Main() {

	}
	public function GetTags() {
		$Key = Input::Post('TagQ');
		//$Key = 'php';
		$Tags = DB::Query('global_tags')->Columns('title');
		if(!empty($Key))
			$Tags->Where('title', 'SEARCH', $Key)->_OR()->Where('title', 'LIKE', $Key);
		$Tags = $Tags->Get()->Result;
		$RT = array();
		foreach($Tags as $Tag) $RT[] = $Tag['title'];
		echo json_encode($RT);
		die();
	}
	public function Tags() {
		$TagActions = array('AddNew', 'Remove');
		$ActionKeys = array_keys(G::$Registry['Params']);
		if(isset(G::$Registry['Params']) && isset($ActionKeys[0]) && in_array($ActionKeys[0], $TagActions)) {
			$Action = $ActionKeys[0] . '_Tag';
			$this->$Action(G::$Registry['Params'][$ActionKeys[0]]);
			return;
		}
		$Search = array('sortby' 	=> 'tid',
						'order'		=> 'desc',
						'q'			=> '',
						'searchby'	=> 'title',
						'status'	=> 'all',
						'total_items'=> 0,
						'limit'		=> 20);
		$FromForm = Input::Post('Search', array());

		$Search = array_merge($Search, G::$Registry['Params']);
		$Search = array_merge($Search, $FromForm);
		Router::BuildParamsString($Search, true);

		$Search['re_order'] = ($Search['order'] == 'desc') ? 'asc' : 'desc';

		G::$Session->Set('tags_sort', $Search['order']);

		Helper::PageInfo('List Tags');
		Helper::State('List tags', Router::Generate('ControllerAction', array('controller' => 'Node', 'action' => 'Tags')));
		Helper::FeaturedPanel(lang('Add new') . ' tag',BASE_DIR . 'Node/Tags/AddNew/0', 'plus');
		$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups,InputGroups,Tables,ButtonGroups,Dropdowns');
		Theme::JqueryPlugin('InputToggle');

		$GetTags = DB::Query('global_tags')->Order($Search['sortby'], $Search['order'])->Limit($Search['limit']);
		$GetTags = $GetTags->Where('tid', '>', 0);
		if($Search['status'] != 'all')
			$GetTags = $GetTags->_AND()->Where('status', '=', $Search['status']);
		if($Search['total_items'] > 0)
			$GetTags = $GetTags->_AND()->Where('total_items', '>=', $Search['total_items']);

		if(!empty($Search['q']))
			$GetTags = $GetTags->_AND()
								->WhereGroupOpen()
									->Where($Search['searchby'], 'SEARCH', $Search['q'])->_OR()
									->Where($Search['searchby'], 'LIKE', $Search['q'])
								->WhereGroupClose();

		$GetTags = $GetTags->Get('tid', Output::Paging());

		if($GetTags->num_rows == 0) Helper::Notify('info', 'There is no tags!');
		$v = $this->View('tags_list');
		$v->Assign('Search', $Search);
		$v->Assign('SA', Router::GenerateThisRoute());
		$v->Assign('Tags', $GetTags->Result);
		$this->Render($v->Output());
	}

	public function Action() {
		$action = Input::Post('action');
		$ids = Input::Post('ids');
		$ids = array_filter(explode(',', $ids));
		$ids = array_map('intval', $ids);

		$rt = array('status' => 'not');
		if($action == 'deactive_tag') {
			$Deactive = DB::Query('global_tags')->Where('tid', 'IN', $ids)->Update(array('status' => 0));
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		if($action == 'active_tag') {
			$Deactive = DB::Query('global_tags')->Where('tid', 'IN', $ids)->Update(array('status' => 1));
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		if($action == 'delete_tag') {
			$Deactive = DB::Query('global_tags')->Where('tid', 'IN', $ids)->Delete();
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		echo json_encode($rt);
		die();
	}

	public function AddNew_Tag($TID = 0) {
		$Tag = array(	'title' => '',
						'url' => '',
						'meta_title' => '',
						'meta_desc' => '',
						'image' => '',
						'description' => '',
						'status' => 1
					);
		if($TID > 0) {
			$CheckTag = DB::Query('global_tags')->Where('tid', '=', $TID)->Get();
			if($CheckTag->num_rows > 0)
				$Tag = $CheckTag->Result[0];
		}
		if(Input::Post('SaveTagSubmit') == 1) {
			$Tag = Input::Post('Tag');
			if(empty($Tag['title'])) Helper::Notify('error', 'Tag title is empty!');
			if(empty($Tag['url'])) $Tag['url'] = $Tag['title'];
			$Tag['url'] = Filter::CleanUrlString($Tag['url']);

			if(Helper::NotifyCount('error') == 0) {
				if($TID == 0) {
					$CheckTagExisted = DB::Query('global_tags')
											->Where('title', '=', $Tag['title'])->_OR()
											->Where('url', '=', $Tag['url'])
											->Get();
					if($CheckTagExisted->num_rows == 0) {
						$IT = DB::Query('global_tags')->Insert($Tag);
						if($IT->status && $IT->insert_id > 0)
							Helper::Notify('success', 'Success add tag!');
						else Helper::Notify('error', 'Cannot add tag, please check your database');
					}
					Helper::Notify('error', 'Cannot add tag, check if title or url existed!');
				}
				else {
					$IT = DB::Query('global_tags')->Where('tid', '=', $TID)->Update($Tag);
					if($IT->status > 0)
						Helper::Notify('success', 'Success update tag!');
					else Helper::Notify('error', 'Cannot update tag, please check your database');
				}
			}
		}
		Helper::PageInfo('Add Tag');
		Helper::State('List tags', Router::Generate('ControllerAction', array('controller' => 'Node', 'action' => 'Tags')));
		Helper::State('Add tag', Router::Generate('ControllerParams', array('controller' => 'Node', 'action' => 'Tags', 'params' => 'AddNew/0')));
		Helper::FeaturedPanel(lang('Add new') . ' tag',BASE_DIR . 'Node/Tags/AddNew/0', 'plus');
		$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups,InputGroups,ButtonGroups');
		Boot::Library('Editor');
		Editor::AddEditor('#Tag_Description');
		Editor::Replace();

		$v = $this->View('add_tag');
		$v->Assign('TID', $TID);
		$v->Assign('Tag', $Tag);
		$this->Render($v->Output());
	}

    public function getNodesFrame() {
        $ref_table = G::$Registry['Params']['table'];
        Theme::JsFooter('getNodesFrame', 'var isGetNodesFrame = true; targetField = \'' . G::$Registry['Params']['target'] . '\'; ref_table = \'' . $ref_table . '\'; ref_field = \'' . G::$Registry['Params']['field'] . '\';', 'inline');
		$ControllerFile = CONTROLLER_PATH . Boot::$ControllerGroup . $ref_table . DIRECTORY_SEPARATOR . $ref_table . '.php';
        require($ControllerFile);
        TPL::Config(	BASE_DIR,
            CONTROLLER_PATH . Boot::$ControllerGroup . $ref_table . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR,
            CACHE_PATH . 'compiled' . DIRECTORY_SEPARATOR,
            CACHE_PATH . 'html' . DIRECTORY_SEPARATOR,
            CACHE_PATH . 'compiled' . DIRECTORY_SEPARATOR
        );
        $nodeCtler = new $ref_table();
		$nodeCtler->ControllerInitializer($ControllerFile);
        $nodeCtler->Main();
    }

    public function getNodesJson() {
        $table = Input::Post('ref_table', '');
        $fields = Input::Post('ref_field', '');
        $nodeIds = Input::Post('nodeIds', '');
        $nodeIds = implode(',', array_filter(explode(',', $nodeIds)));
        $return = array('status' => 'NOT', 'nodes' => array());
        if(!empty($table)) {
            $fields = array_filter(explode(',', $fields));
            $fields[] = $table . '_id';
            $getNodes = DB::Query($table)
                            ->Columns($fields)
                            ->Where($table . '_id', 'IN', $nodeIds)
                            ->Get();
            if($getNodes->status && $getNodes->num_rows > 0) {
                $return = array('status' => 'OK', 'nodes' => $getNodes->Result);
            }
        }
        echo json_encode($return);
        die();
    }
}