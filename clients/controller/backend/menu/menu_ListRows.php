<?php
if(!class_exists('menu')) die('Stop here!');
$Search = array('sortby' 	=> 'menu_id',
				'order'	=> 'DESC',
				'q'		=> '',
				'searchby'=> 'title',
				'status'	=> 'all',
				'limit'	=> 10);
$Search['menu_group'] = 0;
$SortBy = array (
  'title' => 'Tiêu đề',
  'menu_group' => 'Nhóm',
);
$SearchBy = array (
  'title' => 'Tiêu đề',
);
$_RefFilter = array (
  'menu_group' => 
  array (
    'label' => 'Nhóm',
    'nodetype' => 'menu_group',
    'nodefield' => 'title',
  ),
);
$FromForm = Input::Post('Search', array());
$Search = array_merge($Search, G::$Registry['Params']);
$Search = array_merge($Search, $FromForm);
$Search['q'] = urldecode($Search['q']);
Router::BuildParamsString($Search, true);
$Search['re_order'] = ($Search['order'] == 'desc') ? 'asc' : 'desc';
G::$Session->Set('user_sort', $Search['order']);
Helper::State('List nodes', Router::GenerateThisRoute());
$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups,InputGroups,Tables,ButtonGroups,Dropdowns');
Theme::JqueryPlugin('InputToggle');
Theme::JsFooter('ListNodeLib', APPLICATION_DATA_DIR . 'static/Node/list.js');
Theme::JsFooter('ControllerName', 'var NodeController = \'menu\';', 'inline');
$Adapters['vnp_title_menu_group'] = NodeBase::getNodes_Option('menu_group', 'title', 'menu_group_id');
$Adapters['vnp_title_menu_group_id_key'] = NodeBase::getNodes_IDKey('menu_group', 'title', 'menu_group_id');
$ShowFields = array (
  0 => 'title',
  1 => 'url',
  2 => 'menu_group',
  3 => 'menu_id',
  4 => 'status',
);
$GetNodes = DB::Query('menu')->Columns($ShowFields);
$GetNodes = $GetNodes->Limit($Search['limit']*1);
$GetNodes = $GetNodes->Order($Search['sortby'], $Search['order']);
$GetNodes = $GetNodes->Where('menu_id', '>', 0);
if($Search['status'] != 'all')
$GetNodes = $GetNodes->_AND()->Where('status', '=', $Search['status']);
foreach($_RefFilter as $RF => $RFD) {
		if(isset($Search[$RF]) && $Search[$RF] > 0)
		$GetNodes = $GetNodes->_AND()
								->WhereGroupOpen()
									->Where($RF, '=', intval($Search[$RF]))->_OR()
									->Where($RF, 'INCLUDE', $Search[$RF])
								->WhereGroupClose();
}
if(!empty($Search['q']))
$GetNodes = $GetNodes->_AND()
								->WhereGroupOpen()
									->Where($Search['searchby'], 'SEARCH', $Search['q'])->_OR()
									->Where($Search['searchby'], 'LIKE', $Search['q'])
								->WhereGroupClose();
$GetNodes = $GetNodes->Get('menu_id', Output::Paging());
$v = $this->View('list_nodes');
$v->Assign('Search', $Search);
$v->Assign('SortBy', $SortBy);
$v->Assign('SA', Router::GenerateThisRoute());
$v->Assign('SearchBy', $SearchBy);
$v->Assign('Adapters', $Adapters);
if($GetNodes->num_rows > 0) {
	$v->Assign('Nodes', $GetNodes->Result);
} else Helper::Notify('info', 'There is no nodes found!');
$this->Render($v->Output());
Helper::PageInfo('List nodes for Menu ( Found ' . $GetNodes->num_rows . ' nodes )');
Helper::FeaturedPanel('Add new Menu',BASE_DIR . 'menu/AddNode', 'plus');