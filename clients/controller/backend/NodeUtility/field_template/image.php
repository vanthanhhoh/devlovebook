<div class="form-group FieldWrap [@@FieldClass@@]">
	<label class="col-sm-2 control-label" for="ID_[@@FieldName@@]">[@@FieldLabel@@]</label>
    <div class="col-sm-7">
    	<input type="file" name="[@@FieldName@@]" id="ID_[@@FieldName@@]" class="form-control [@@FieldClass@@]"/>
   	</div>
    <div class="col-sm-3">
		<button class="btn btn-primary" onclick="VNP.SugarBox.Open('FileManager','ID_[@@FieldName@@]'); return false;">Browse</button>
		<button class="btn btn-danger" onclick="document.getElementById('ID_[@@FieldName@@]').value = ''; document.getElementById('PreviewImage').setAttribute('src', 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=='); return false;">Delete</button>
	</div>
    <div class="FieldImagePreviewer"><img id="PreviewImage"></div>
</div>