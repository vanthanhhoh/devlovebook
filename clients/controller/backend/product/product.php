<?php

class product extends Controller {
	public $NodeTypeName;
	public function __construct() {
		$this->NodeTypeName = 'product';
	}
	public function Main() {
		require(CONTROLLER_PATH . Boot::$ControllerGroup . 'product' . DIRECTORY_SEPARATOR . 'product_ListRows.php');
	}
	public function AddNode() {
		Theme::UseJquery();
		Theme::JsFooter('AddNodeLib', APPLICATION_DATA_DIR . 'static/Node/add.js');
		$NodeID = 0;
		$FormValue = array();
		if(isset(G::$Registry['Params'][ROUTER_EXTRA_KEY])) {
			$NodeID = G::$Registry['Params'][ROUTER_EXTRA_KEY];
			$GetNode = DB::Query('product')->Where('product_id', '=', $NodeID)->Get();
			if($GetNode->num_rows == 1) {
				$FormValue = $GetNode->Result[0];
				DB::Query('product')->Where('product_id', '=', $NodeID)->Update(array('editing_id' => USER_ID));
			}
			else $NodeID = 0;
		}
		$FV = $this->SaveNodeAction($NodeID, $FormValue);
		if(!empty($FV)) $FormValue = array_merge($FormValue, $FV);
		$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups');
		$Fields = array(
            'code' => array('value' => isset($FormValue['code']) ? $FormValue['code'] : ''),
            'title' => array('value' => isset($FormValue['title']) ? $FormValue['title'] : ''),
            'sku' => array('value' => isset($FormValue['sku']) ? $FormValue['sku'] : ''),
            'url' => array('value' => isset($FormValue['url']) ? $FormValue['url'] : ''),
            'image' => array('value' => isset($FormValue['image']) ? $FormValue['image'] : ''),
            'other_images' => array('value' => isset($FormValue['other_images']) ? $FormValue['other_images'] : ''),
            'price' => array('value' => isset($FormValue['price']) ? $FormValue['price'] : ''),
            'price_sale' => array('value' => isset($FormValue['price_sale']) ? $FormValue['price_sale'] : ''),
            'properties' => array('value' => isset($FormValue['properties']) ? $FormValue['properties'] : ''),
            'trailer' => array('value' => isset($FormValue['trailer']) ? $FormValue['trailer'] : ''),
            'gift' => array('value' => isset($FormValue['gift']) ? $FormValue['gift'] : ''),
            'author' => array('value' => isset($FormValue['author']) ? $FormValue['author'] : ''),
            'product_category' => array('value' => isset($FormValue['product_category']) ? $FormValue['product_category'] : ''),
            'product_sub_category' => array('value' => isset($FormValue['product_sub_category']) ? $FormValue['product_sub_category'] : ''),
            'product_group' => array('value' => isset($FormValue['product_group']) ? $FormValue['product_group'] : ''),
            'content' => array('value' => isset($FormValue['content']) ? $FormValue['content'] : ''),
            'discription' => array('value' => isset($FormValue['discription']) ? $FormValue['discription'] : ''),
            'publisher' => array('value' => isset($FormValue['publisher']) ? $FormValue['publisher'] : '')
        );
		$vnp_title_author = NodeBase::getNodes_Option('author', 'title', 'author_id');
		$Fields['author']['Options'] = $vnp_title_author;
        $vnp_title_publisher = NodeBase::getNodes_Option('publisher', 'title', 'publisher_id');
        $Fields['publisher']['Options'] = $vnp_title_publisher;
		$vnp_title_product_category = NodeBase::getNodes_Option('product_category', 'title', 'product_category_id');
		$Fields['product_category']['Options'] = $vnp_title_product_category;
		$vnp_title_product_group = NodeBase::getNodes_Option('product_group', 'title', 'product_group_id');
		$Fields['product_group']['Options'] = $vnp_title_product_group;
		Boot::Library('Editor');
		Editor::AddEditor('#ID_Field_content');
		Editor::Replace();
		Theme::JqueryUI('sortable');
		
		Backend::$NodeSettings['status'] = isset($FormValue['status']) ? $FormValue['status'] : 1;
		Backend::$NodeSettings['priority'] = isset($FormValue['priority']) ? $FormValue['priority'] : 0;
		Backend::$NodeSettings['schedule'] = Filter::UnixTimeToDate(isset($FormValue['schedule']) ? $FormValue['schedule'] : 0);
		Backend::$NodeSettings['exprired'] = Filter::UnixTimeToDate(isset($FormValue['exprired']) ? $FormValue['exprired'] : 0);
		if(isset($FormValue['node_settings'])) Backend::$NodeSettings['extra'] = $FormValue['node_settings'];
		ob_start();
		echo '<form class="form-horizontal" action="" method="post">';
		echo '<input type="hidden" name="SaveNodeSubmit" value="1"/>';
		echo '<input type="hidden" name="NodeID" value="' . $NodeID . '"/>';
		include Form::$CompiledPath . 'Controller_product_InsertNode.php';
		echo '<div class="clearfix"></div><div class="clearfix" style="text-align:center;margin:10px 0 15px 0"><input type="submit" class="btn btn-primary" value="Save"/></div>';
		echo '</form>';
		$Form = ob_get_clean();
		$this->Render($Form);
	}

	public function Action() {
		$action = Input::Post('action');
		$ids = Input::Post('ids');
		$ids = array_filter(explode(',', $ids));
		$ids = array_map('intval', $ids);

		$rt = array('status' => 'not');
		if($action == 'deactive') {
			$Deactive = DB::Query('product')->Where('product_id', 'IN', $ids)->Update(array('status' => 0));
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		if($action == 'active') {
			$Deactive = DB::Query('product')->Where('product_id', 'IN', $ids)->Update(array('status' => 1));
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		if($action == 'delete') {
			$Deactive = DB::Query('product')->Where('product_id', 'IN', $ids)->Delete();
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		echo json_encode($rt);
		die();
	}

	public function RemoveNode() {
		$NodeID = 0;
		if(isset(G::$Registry['Params'][ROUTER_EXTRA_KEY])) {
			$NodeID = G::$Registry['Params'][ROUTER_EXTRA_KEY];
			$GetNode = DB::Query('product')->Where('product_id', '=', $NodeID)->Get();
			if($GetNode->num_rows == 0)
				return Helper::Notify('error', 'Node not found');
		}
		if(Input::Post('RemoveNodeSubmit') == 1) {
			$DeleteNode = DB::Query('product')->Where('product_id', '=', $NodeID)->Delete();
			if($DeleteNode->affected_rows == 1) {
				Helper::Notify('success', 'Success delete node ' .  $GetNode->Result[0]['title']);
				Header('Refresh: 1.5; url=' . Router::Generate('Controller', array('controller' => 'product')));
			}
			else Helper::Notify('error', 'Error delete node ' .  $GetNode->Result[0]['title']);
		}
		else {
			$config = array('action'	=> Router::GenerateThisRoute(),
							'tokens'	=> array(array('name' => 'NodeID', 'value' => $NodeID), array('name' => 'RemoveNodeSubmit', 'value' => 1))
							);
			$v = Access::Confirm('Confirm remove node: ' . $GetNode->Result[0]['title'], $config);
			$this->Render($v);
		}
	}

	public function SaveNodeAction($RealNodeID, $OldFormValue = array()) {
		if(Input::Post('SaveNodeSubmit') == 1) {
			$FormValue = Input::Post('Field');
			
			if(!isset($FormValue['code']) || $FormValue['code'] == '')
				Helper::Notify('error', 'Mã sản phẩm  không thể để trống');

			if(!isset($FormValue['title']) || $FormValue['title'] == '')
				Helper::Notify('error', 'Tên sản phẩm không thể để trống');

			if(isset($FormValue['title']) && (!isset($FormValue['url']) || $FormValue['url'] == ''))
				$FormValue['url'] = $FormValue['title'];

			if(!isset($FormValue['url']) || $FormValue['url'] == '')
				Helper::Notify('error', 'Đường dẫn không thể để trống');

			$FormValue['url'] = strtolower(Filter::CleanUrlString($FormValue['url']));

			if(!isset($FormValue['image']) || $FormValue['image'] == '')
				Helper::Notify('error', 'Ảnh đại diện không thể để trống');

			$FormValue['other_images'] = array_filter($FormValue['other_images']);
			$FormValue['other_images'] = implode(',', $FormValue['other_images']);
            $FormValue['product_sub_category'] = array_filter($FormValue['product_sub_category']);
            $FormValue['product_sub_category'] = implode(',', $FormValue['product_sub_category']);
            $FormValue['product_group'] = array_filter($FormValue['product_group']);
            $FormValue['product_group'] = implode(',', $FormValue['product_group']);
			if(!isset($FormValue['price']) || $FormValue['price'] == '')
				Helper::Notify('error', 'Giá bìa không thể để trống');

			$FormValue['properties'] = serialize($FormValue['properties']);

			if(!isset($FormValue['author']) || $FormValue['author'] == '')
				Helper::Notify('error', 'Tác giả không thể để trống');

			$FormValue['schedule'] = Filter::DateToUnixTime($FormValue['schedule']['date'], $FormValue['schedule']['hour'], $FormValue['schedule']['minute']);
			if($FormValue['schedule'] < CURRENT_TIME) $FormValue['schedule'] = CURRENT_TIME;
			$FormValue['exprired'] = Filter::DateToUnixTime($FormValue['exprired']['date'], $FormValue['exprired']['hour'], $FormValue['exprired']['minute']);
			if($FormValue['exprired'] < CURRENT_TIME) $FormValue['exprired'] = 0;
			$FormValue['node_settings'] = Backend::BuildNodePermission(Input::Post('VNP_Settings'));
            $gift = array();
            if(!empty($FormValue['gift'])){
                foreach($FormValue['gift'] as $key => $item){
                    $gift[$key]['product_id'] = $key;
                    $gift[$key]['qty']        = $item;
                }
            }
            $FormValue['gift'] = serialize($gift);
			if(Helper::NotifyCount('error') == 0) {
				$NodeID = Input::Post('NodeID');
				if(empty($NodeID)) {
					$CheckUnique = array();
					$NodeExisted = false;

						if(!$NodeExisted) {
							$FormValue['lang'] = LANG;
							$FormValue['user_id'] = USER_ID;
							$FormValue['last_edit_id'] = USER_ID;
							$FormValue['add_time'] = CURRENT_TIME;
							$FormValue['edit_time'] = CURRENT_TIME;
							$NodeQuery = DB::Query('product')->Insert($FormValue);
							if($NodeQuery->status && $NodeQuery->insert_id > 0) {
								Helper::Notify('success', 'Successful add node in Sản phẩm');
							}
							else Helper::Notify('error', 'Cannot add node in Sản phẩm');
						}
						else Helper::Notify('error', 'Cannot add node in Sản phẩm. Be sure that <em></em> didn\'t existed!');
				}
				else {
					//$CheckExisted = DB::Query('product')->Where('product_id', '=', $NodeID)->Get();
					//if($CheckExisted->num_rows == 1) {
					if($RealNodeID == $NodeID) {
						$FormValue['last_edit_id'] = USER_ID;
						$FormValue['edit_time'] = CURRENT_TIME;
						$FormValue['editing_id'] = 0;
						$NodeQuery = DB::Query('product')->Where('product_id', '=', $NodeID)->Update($FormValue);
						if($NodeQuery->status && $NodeQuery->affected_rows > 0) {
							Helper::Notify('success', 'Successful update node in Sản phẩm');
						}
						else Helper::Notify('error', 'Cannot update node in Sản phẩm');
					}
					else {
						Helper::Notify('error', 'Cannot update, Node not found!');
					}
				}
			}

			return $FormValue;
		}
	}
}