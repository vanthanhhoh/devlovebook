<?php
if(!class_exists('product')) die('Stop here!');
$Search = array('sortby' 	=> 'product_id',
				'order'	=> 'DESC',
				'q'		=> '',
				'searchby'=> 'code',
				'status'	=> 'all',
				'limit'	=> 20);
$Search['product_category'] = 0;
$SortBy = array (
  'code' => 'Mã sản phẩm ',
  'title' => 'Tên sản phẩm',
  'product_category' => 'Danh mục',
);
$SearchBy = array (
  'code' => 'Mã sản phẩm ',
  'title' => 'Tên sản phẩm',
);
$_RefFilter = array (
  'product_category' => 
  array (
    'label' => 'Danh mục',
    'nodetype' => 'product_category',
    'nodefield' => 'title',
  ),
);
$FromForm = Input::Post('Search', array());
$Search = array_merge($Search, G::$Registry['Params']);
$Search = array_merge($Search, $FromForm);
$Search['q'] = urldecode($Search['q']);
Router::BuildParamsString($Search, true);
$Search['re_order'] = ($Search['order'] == 'desc') ? 'asc' : 'desc';
G::$Session->Set('user_sort', $Search['order']);
Helper::State('List nodes', Router::GenerateThisRoute());
$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups,InputGroups,Tables,ButtonGroups,Dropdowns');
Theme::JqueryPlugin('InputToggle');
Theme::JsFooter('ListNodeLib', APPLICATION_DATA_DIR . 'static/Node/list.js');
Theme::JsFooter('ControllerName', 'var NodeController = \'product\';', 'inline');
$Adapters['vnp_title_author'] = NodeBase::getNodes_Option('author', 'title', 'author_id');
$Adapters['vnp_title_author_id_key'] = NodeBase::getNodes_IDKey('author', 'title', 'author_id');
$Adapters['vnp_title_product_category'] = NodeBase::getNodes_Option('product_category', 'title', 'product_category_id');
$Adapters['vnp_title_product_category_id_key'] = NodeBase::getNodes_IDKey('product_category', 'title', 'product_category_id');
$Adapters['vnp_title_product_group'] = NodeBase::getNodes_Option('product_group', 'title', 'product_group_id');
$Adapters['vnp_title_product_group_id_key'] = NodeBase::getNodes_IDKey('product_group', 'title', 'product_group_id');
$ShowFields = array (
  0 => 'code',
  1 => 'title',
  2 => 'image',
  3 => 'product_category',
  4 => 'product_id',
  5 => 'status',
);
$GetNodes = DB::Query('product')->Columns($ShowFields);
$GetNodes = $GetNodes->Limit($Search['limit']*1);
$GetNodes = $GetNodes->Order($Search['sortby'], $Search['order']);
$GetNodes = $GetNodes->Where('product_id', '>', 0);
if($Search['status'] != 'all')
$GetNodes = $GetNodes->_AND()->Where('status', '=', $Search['status']);
foreach($_RefFilter as $RF => $RFD) {
		if(isset($Search[$RF]) && $Search[$RF] > 0)
		$GetNodes = $GetNodes->_AND()
								->WhereGroupOpen()
									->Where($RF, '=', intval($Search[$RF]))->_OR()
									->Where($RF, 'INCLUDE', $Search[$RF])
								->WhereGroupClose();
}
if(!empty($Search['q']))
$GetNodes = $GetNodes->_AND()
								->WhereGroupOpen()
									->Where($Search['searchby'], 'SEARCH', $Search['q'])->_OR()
									->Where($Search['searchby'], 'LIKE', $Search['q'])
								->WhereGroupClose();
$GetNodes = $GetNodes->Get('product_id', Output::Paging());
$v = $this->View('list_nodes');
$v->Assign('Search', $Search);
$v->Assign('SortBy', $SortBy);
$v->Assign('SA', Router::GenerateThisRoute());
$v->Assign('SearchBy', $SearchBy);
$v->Assign('Adapters', $Adapters);
if($GetNodes->num_rows > 0) {
	$v->Assign('Nodes', $GetNodes->Result);
} else Helper::Notify('info', 'There is no nodes found!');
$this->Render($v->Output());
Helper::PageInfo('List nodes for Sản phẩm ( Found ' . $GetNodes->num_rows . ' nodes )');
Helper::FeaturedPanel('Add new Sản phẩm',BASE_DIR . 'product/AddNode', 'plus');