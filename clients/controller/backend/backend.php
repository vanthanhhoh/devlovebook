<?php

Theme::Config(
    array(
        'theme_root'		=> DATA_PATH . 'theme' . DIRECTORY_SEPARATOR,
        'default_theme'		=> 'admin-vnp',
        'default_layout'	=> 'left.body'
    )
);

class Backend extends Controller {
	static $NodeProfile = array();
	static $NodeMapper = array();
	static $ExtraOpts = array(	'allow_like' => array('type' => 'common', 'value' => 'default'),
								'allow_rating' => array('type' => 'common', 'value' => 'default'),
								'allow_comment' => array('type' => 'common', 'value' => 'default'),
								'review_opts' => 'default',
								'comment_approve' => 'default',
								'view_permission' => array('type' => 'common', 'value' => 'default')
							);
	static function Init() {
		Theme::CssHeader('checkbox', APPLICATION_DATA_DIR . 'static/css/checkbox.css');
		Theme::AddCssComponent('Pagination,Pager');
		Theme::UseJquery();
		Theme::Assign('MyData', G::$MyData);
		Theme::Assign('Controller', G::$Controller);
		Backend::$NodeSettings['extra'] = serialize(self::$ExtraOpts);
		if(file_exists(APPLICATION_PATH . 'node_mapper.php')) {
			include APPLICATION_PATH . 'node_mapper.php';
			self::$NodeProfile = $NodeProfile;
			self::$NodeMapper = $NodeMapper;
		}
		unset($NodeProfile,$NodeMapper);
		Hook::_extends(array(
			'before_add_product',
			'before_edit_product',
			'after_product_saved'
		));
	}
	static $NodeSettings = array(	'status'   => 1,
									'priority' => 0,
									'schedule' => array('date' => 0, 'hour' => 0, 'minute' => 0),
									'exprired' => array('date' => 0, 'hour' => 0, 'minute' => 0),
									'extra'	   => ''
								);
	static $NodePermission = array(	'default' => array('name' => 'Default'),
									'none' => array('name' => 'None'),
									'all' => array('name' => 'All'),
									'logged' => array('name' => 'Logged user'),
									'groups' => array('name' => 'Special user groups')
								);
	static function NodeExtraSettings() {
		$Controller = G::$Registry['Controller'];
		if(!is_array(self::$NodeSettings['extra']) && self::$NodeSettings['extra'] != '')
			self::$NodeSettings['extra'] = unserialize(self::$NodeSettings['extra']);

		$Groups = array();
		$GetGroups = DB::Query('user_groups')->Get('gid');
		if($GetGroups->num_rows > 0)
			$Groups = Filter::BuildLevelList($GetGroups->Result, 'gid', 'parent_id');
		return TPL::File('node_extra_settings')
				->SetDir('TPLFileDir', TEMPLATE_PATH)
				->SetDir('CompiledDir', CACHE_PATH . 'compiled' . DIRECTORY_SEPARATOR)
				->SetDir('CacheDir', CACHE_PATH . 'html' . DIRECTORY_SEPARATOR)
				->Assign('PGs', self::$NodePermission)
				->Assign('Data', self::$NodeSettings)
				->Assign('UserGroups', $Groups)
				->Output();
	}

	static function BuildPermissionTemplate() {
		Boot::Library('NodeBuilder');
		$CachePath = CACHE_PATH . 'NodeBuilder' . DIRECTORY_SEPARATOR . NodeBuilder::$NodeBuilderCachePath . md5('VNP_MERGED_PROFILE_1906') . '_merge' . NodeBuilder::CACHE_FILE_EXTENSION;
		$NodeTypeData = File::GetContent($CachePath);
		$NodeTypeData = unserialize($NodeTypeData);
		return TPL::File('permission_setting')
				->SetDir('TPLFileDir', TEMPLATE_PATH)
				->SetDir('CompiledDir', CACHE_PATH . 'compiled' . DIRECTORY_SEPARATOR)
				->SetDir('CacheDir', CACHE_PATH . 'html' . DIRECTORY_SEPARATOR)
				->Assign('NodeTypeData', $NodeTypeData)
				->Output();
	}
	static function BuildNodePermission($Settings) {
		$allow_like = $Settings['allow_like'];
		if($allow_like == 'groups') {
			$Settings['allow_like'] = array();
			$Settings['allow_like']['type'] = 'groups';
			if(isset($Settings['allow_like_groups'])) {
				$Settings['allow_like']['value'] = $Settings['allow_like_groups'];
				unset($Settings['allow_like_groups']);
			}
			else $Settings['allow_like']['value'] = array('default');
		}
		else $Settings['allow_like'] = array('type' => 'common', 'value' => $allow_like);

		$allow_rating = $Settings['allow_rating'];
		if($allow_rating == 'groups') {
			$Settings['allow_rating'] = array();
			$Settings['allow_rating']['type'] = 'groups';
			if(isset($Settings['allow_rating_groups'])) {
				$Settings['allow_rating']['value'] = $Settings['allow_rating_groups'];
				unset($Settings['allow_rating_groups']);
			}
			else $Settings['allow_rating']['value'] = array('default');
		}
		else $Settings['allow_rating'] = array('type' => 'common', 'value' => $allow_rating);

		$allow_comment = $Settings['allow_comment'];
		if($allow_comment == 'groups') {
			$Settings['allow_comment'] = array();
			$Settings['allow_comment']['type'] = 'groups';
			if(isset($Settings['allow_comment_groups'])) {
				$Settings['allow_comment']['value'] = $Settings['allow_comment_groups'];
				unset($Settings['allow_comment_groups']);
			}
			else $Settings['allow_comment']['value'] = array('default');
		}
		else $Settings['allow_comment'] = array('type' => 'common', 'value' => $allow_comment);

		$view_permission = $Settings['view_permission'];
		if($allow_comment == 'groups') {
			$Settings['view_permission'] = array();
			$Settings['view_permission']['type'] = 'groups';
			if(isset($Settings['view_permission_groups'])) {
				$Settings['view_permission']['value'] = $Settings['view_permission_groups'];
				unset($Settings['view_permission_groups']);
			}
			else $Settings['view_permission']['value'] = array('default');
		}
		else $Settings['view_permission'] = array('type' => 'common', 'value' => $view_permission);
		return serialize($Settings);
	}
	static function BuildBoardMenu() {
		$BoardMenuItems = array(
			'post' => array(
				'url' => BASE_DIR . 'post',
				'title' => 'Bài viết',
				'sub' => array(
					BASE_DIR . 'post/AddNode' => 'Thêm bài viết',
					BASE_DIR . 'post_category' => 'Danh mục bài viết',
					BASE_DIR . 'post_group' => 'Nhóm bài viết'
				)
			),
			'content' => array(
				'url' => BASE_DIR . 'content',
				'title' => 'Quản lý nội dung',
				'sub' => array(
					BASE_DIR . 'extension/dashboard' => 'Quản lý ứng dụng',
					BASE_DIR . 'menus' => 'Quản lý menu',
					//BASE_DIR . 'slider' => 'Quản lý slide',
					BASE_DIR . 'Node/tags' => 'Quản lý tags',
				)
			)
		);
		return $BoardMenuItems;
		//return self::$NodeProfile;
	}
	static function UpdateTags($NodeID, $NodeTypeID, $Tags, $OldTags = '') {
		if(empty($Tags)) return;
		$TagsArray = array_filter(explode(',', $Tags));
		$RemoveTags = array_diff(explode(',',$OldTags),$TagsArray);

		/**** Check new Tags ****/
		$ExistedTags = array();
		if(!empty($TagsArray)) {
			$ExistedTags = array_keys(DB::Query('global_tags')->Where('title', 'IN', $TagsArray)->Get('title')->Result);
			$NewTags = array_diff($TagsArray,$ExistedTags);
		}
		else $NewTags = $TagsArray;
		/**** End check new Tags ****/

		/**** Insert new Tags ****/
		$InsertNewTags = array();
		foreach($NewTags as $Tag)
			$InsertNewTags[] = array('title' => $Tag, 'url' => Filter::CleanUrlString($Tag), 'total_items' => 0);
		$TagQuery = DB::Query('global_tags');
		if(!empty($InsertNewTags))
			call_user_func_array(array($TagQuery, 'Insert'), $InsertNewTags);
		/**** End insert new Tags ****/

		/**** Check node new Tags ****/
		if(!empty($ExistedTags))
			$NodeExistedTags = array_keys(DB::Query('global_tags')
									->Where('title', 'IN', $ExistedTags)->_AND()
									->Where('items', 'INCLUDE', $NodeTypeID . '-' . $NodeID)
									->Get('title')->Result);
		else $NodeExistedTags = array();

		$NodeNewTags = array_merge(array_diff($ExistedTags,$NodeExistedTags), $NewTags);
		if(!empty($NodeNewTags)) {
			$UpdateTag = array('items' => ',' . $NodeTypeID . '-' . $NodeID, 'total_items' => 1);
			$SpecialUpdates = array('items' => 'CONCAT', 'total_items' => 'INC');
			DB::Query('global_tags')->Where('title', 'IN', $NodeNewTags)
									->Update($UpdateTag, $SpecialUpdates);
		}
		if(!empty($RemoveTags)) {
			$UpdateTag = array('items' => array(',' . $NodeTypeID . '-' . $NodeID, ''), 'total_items' => 1);
			$SpecialUpdates = array('items' => 'REPLACE', 'total_items' => 'DEC');
			DB::Query('global_tags')->Where('title', 'IN', $RemoveTags)
									->Update($UpdateTag, $SpecialUpdates);
		}
		//n($NodeNewTags);
	}
    static function Complete() {
    }
}

?>