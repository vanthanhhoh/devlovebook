<?php
if(!class_exists('invoice')) die('Stop here!');
$Search = array('sortby' 	=> 'invoice_id',
				'order'	=> 'DESC',
				'q'		=> '',
				'searchby'=> 'name',
				'status'	=> 'all',
				'limit'	=> 20);
$SortBy = array (
  'name' => 'Tên người nhận',
  'phone' => 'Số điện thoại',
  'invoice_status' => 'Tình trạng đơn hàng',
  'code' => 'Mã đơn hàng',
);
$SearchBy = array (
  'name' => 'Tên người nhận',
  'phone' => 'Số điện thoại',
  'invoice_status' => 'Tình trạng đơn hàng',
  'code' => 'Mã đơn hàng',
);
$_RefFilter = array (
);
$FromForm = Input::Post('Search', array());
$Search = array_merge($Search, G::$Registry['Params']);
$Search = array_merge($Search, $FromForm);
$Search['q'] = urldecode($Search['q']);
Router::BuildParamsString($Search, true);
$Search['re_order'] = ($Search['order'] == 'desc') ? 'asc' : 'desc';
G::$Session->Set('user_sort', $Search['order']);
Helper::State('List nodes', Router::GenerateThisRoute());
$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups,InputGroups,Tables,ButtonGroups,Dropdowns');
Theme::JqueryPlugin('InputToggle');
Theme::JsFooter('ListNodeLib', APPLICATION_DATA_DIR . 'static/Node/list.js');
Theme::JsFooter('ControllerName', 'var NodeController = \'invoice\';', 'inline');
$Adapters['vnp_title_ship'] = NodeBase::getNodes_Option('ship', 'title', 'ship_id');
$Adapters['vnp_title_ship_id_key'] = NodeBase::getNodes_IDKey('ship', 'title', 'ship_id');
$Adapters['vnp_title_payment'] = NodeBase::getNodes_Option('payment', 'title', 'payment_id');
$Adapters['vnp_title_payment_id_key'] = NodeBase::getNodes_IDKey('payment', 'title', 'payment_id');
$Options['invoice_status'] = array (
  1 => 'Đang chờ xác nhận',
  2 => 'Đã chờ thanh toán',
  3 => 'Đã xác nhận',
  4 => 'Đang vận chuyển',
  5 => 'Thành công',
);
$ShowFields = array (
  0 => 'name',
  1 => 'phone',
  2 => 'invoice_status',
  3 => 'code',
  4 => 'invoice_id',
  5 => 'status',
  6 => 'total',
  7 => 'add_time'
);
$GetNodes = DB::Query('invoice')->Columns($ShowFields);
$GetNodes = $GetNodes->Limit($Search['limit']*1);
$GetNodes = $GetNodes->Order($Search['sortby'], $Search['order']);
$GetNodes = $GetNodes->Where('invoice_id', '>', 0);
if($Search['status'] != 'all')
$GetNodes = $GetNodes->_AND()->Where('status', '=', $Search['status']);
foreach($_RefFilter as $RF => $RFD) {
		if(isset($Search[$RF]) && $Search[$RF] > 0)
		$GetNodes = $GetNodes->_AND()
								->WhereGroupOpen()
									->Where($RF, '=', intval($Search[$RF]))->_OR()
									->Where($RF, 'INCLUDE', $Search[$RF])
								->WhereGroupClose();
}
if(!empty($Search['q']))
$GetNodes = $GetNodes->_AND()
								->WhereGroupOpen()
									->Where($Search['searchby'], 'SEARCH', $Search['q'])->_OR()
									->Where($Search['searchby'], 'LIKE', $Search['q'])
								->WhereGroupClose();
$GetNodes = $GetNodes->Get('invoice_id', Output::Paging());
$v = $this->View('list_nodes');
$v->Assign('Search', $Search);
$v->Assign('SortBy', $SortBy);
$v->Assign('SA', Router::GenerateThisRoute());
$v->Assign('SearchBy', $SearchBy);
$v->Assign('Adapters', $Adapters);
if($GetNodes->num_rows > 0) {
	$v->Assign('Nodes', $GetNodes->Result);
$v->Assign('Options', $Options);
} else Helper::Notify('info', 'There is no nodes found!');
$this->Render($v->Output());
Helper::PageInfo('List nodes for Đơn hàng ( Found ' . $GetNodes->num_rows . ' nodes )');
Helper::FeaturedPanel('Export excel',BASE_DIR . 'invoice/exportInvoice', 'plus');