<script>
    $(document).ready(function(){
        $('#paymentStatus').change(function() {
            var id = $(this).data('id');
            var status = $(this).val();
            $.ajax({
                url:'/admincp/invoice/changeStatus',
                type:'post',
                dataType:'text',
                data: {id:id,status:status},
                success: function(res){
                    if(res=='ok'){
                        alert('Thay đổi thành công!');
                        location.reload();
                    }
                    else {
                        alert(res);
                    }
                }
            });
        });
    });
</script>
<div class="row">
    <div class="col-xs-12" id="printarea">
        <!-- PAGE CONTENT BEGINS -->
        <div class="space-6"></div>

        <div class="widget-box transparent">
            <div class="widget-header widget-header-large">
                <h3 class="widget-title grey lighter">
                    <i class="ace-icon fa fa-leaf green"></i>
                    {$node.name}
                </h3>

                <div class="widget-toolbar no-border invoice-info">
                    <span class="invoice-info-label">Invoice:</span>
                    <span class="red">#{$node.code}</span>
                    <br />
                    <span class="invoice-info-label">Date:</span>
                    <span class="blue">{$node.add_time|Filter::UnixTimeToDate:true}</span>
                    <span class="invoice-info-label">Update:</span>
                    <span class="blue">
                        {if($node.edit_time>0)}

                            {$node.edit_time|Filter::UnixTimeToDate:true}
                            {else}
                            -------
                        {/if}
                    </span>
                </div>
                <div class="widget-toolbar">
                    <a id="print" style="cursor: pointer" onclick="printDiv('printarea')">
                        <i class="ace-icon fa fa-print"></i>
                    </a>
                </div>
                {function}
                    $orderStatus = array(
                    1=>'Đang chờ xác nhận',
                    2=>'Đang chờ thanh toán',
                    3=>'Đã xác nhận',
                    4=>'Đang vận chuyển',
                    5=>'Thành công'
                    );
                {/function}
                <div class="widget-toolbar">
                    <select id="paymentStatus" class="form-control" data-id="{$node.invoice_id}">
                        {for $status in $orderStatus as $sid}
                            <option value="{$sid}"{if($sid==$node.invoice_status)}selected{/if}>{$status}</option>
                        {/for}
                    </select>
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main">
                    <div>
                        <ul class="list-unstyled  spaced">
                            <li>
                                <i class="ace-icon fa fa-caret-right green"></i>Địa chỉ : {$node.address} , {function}echo $district[$node['district']]['title']{/function} , {function}echo $province[$node['province']]['title']{/function}
                            </li>
                            <li>
                                <i class="ace-icon fa fa-caret-right green"></i>Điện thoại : {$node.phone}
                            </li>
                            <li>
                                <i class="ace-icon fa fa-caret-right green"></i>Lưu ý : {$node.note}
                            </li>
                            <li>

                                <i class="ace-icon fa fa-caret-right green"></i>Trạng thái đơn hàng :
                                <strong>
                                {function}echo $orderStatus[$node['invoice_status']];{/function}
                                </strong>
                            </li>
                            <li>
                                <i class="ace-icon fa fa-caret-right green"></i>Hình thức thanh toán :
                                {function}echo $payment[$node['payment']]['title']{/function}
                            </li>
                            <li>
                                <i class="ace-icon fa fa-caret-right green"></i>Hình thức vận chuyển :
                                {function}echo $ship[$node['ship']]['title']{/function}
                            </li>

                        </ul>
                    </div>

                    <div class="space"></div>

                    <div>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Tên sản phẩm</th>
                                    <th>Giá gốc</th>
                                    <th>Giá khuyến mãi</th>
                                    <th>Số lượng</th>
                                    <th>Tổng cộng</th>
                                </tr>
                            </thead>
                            <tbody>
                            {$giagoc = 0}
                            {$dagiam=0}
                            {for $item in $order}
                                {function}
                                    $product = $p[$item['product_id']];
                                    $giagoc = $giagoc+($item['price_old']*$item['qty']);
                                    $dagiam+=$item['total'];
                                {/function}
                                <tr>
                                    <td>
                                        <a href="">{$product.title}</a>
                                        {if($item.type==2)}
                                            <i>Quà tặng</i>
                                        {/if}
                                    </td>
                                    <td>{$item.price_old|Filter::NumberFormat} đ</td>
                                    <td>{$item.price|Filter::NumberFormat} đ</td>
                                    <td>{$item.qty}</td>
                                    <td>{$item.total|Filter::NumberFormat} đ</td>
                                </tr>
                            {/for}
                            <tr>
                                {function}
                                    $giamgia = $giagoc-$dagiam;
                                    $giamgiacupon =0;
                                    if($node['cupon']!='' && $node['cupon_discount']>0 && $node['cupon_type']!=''){
                                    if($node['cupon_type']==1){
                                    $giamgiacupon = $node['cupon_discount'];
                                    }
                                    else {
                                    $giamgiacupon = $dagiam*$node['cupon_discount']/100; $giamgiacupon = ceil($giamgiacupon);
                                    }
                                    }
                                {/function}
                                <td colspan="5" align="right" class="sumaryTable">
                                    <p>
                                        <span>Tổng chưa giảm:</span> <strong>{$giagoc|Filter::NumberFormat} đ</strong>
                                    </p>
                                    <p>
                                        <span>Giảm giá:</span> <strong>-{$giamgia|Filter::NumberFormat} đ</strong>
                                    </p>
                                    <p>
                                        <span>Tổng đã giảm:</span> <strong>{$dagiam|Filter::NumberFormat} đ</strong>
                                    </p>
                                    <p>
                                        <span>Giảm giá theo mã {$node.cupon}:</span> <strong>-{$giamgiacupon|Filter::NumberFormat} đ</strong>
                                    </p>
                                    <p>
                                        <span>Phí vận chuyển:</span> <strong></strong>
                                    </p>
                                    <hr/>
                                    <p>
                                        {function}$tongcong = $dagiam-$giamgiacupon{/function}
                                        Tổng cộng: <strong>{$tongcong|Filter::NumberFormat} đ</strong>
                                    </p>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div>

                    <div class="hr hr8 hr-double hr-dotted"></div>


                    <div class="space-6"></div>
                    <div class="well">
                       Cảm ơn đã sử dụng !
                    </div>
                </div>
            </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div>
<script>

    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>