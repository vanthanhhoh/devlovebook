<div class="table-responsive">
	<form class="form-horizontal" action="{$SA}" method="post">
		<input type="hidden" name="SearchUsers" value="1">
		<table class="table table-bordered table-striped table-hover">
	        <colgroup>
	        	<col class="col-xs-2">
	            <col class="col-xs-1">
	            <col class="col-xs-2">
	            <col class="col-xs-1">
	            <col class="col-xs-2">
	            <col class="col-xs">
	            <col class="col-xs-1">
	            <col class="col-xs">
	            <col class="col-xs-1">
	            <col class="col-xs-1">
	        </colgroup>
	        <tbody>
	        		<tr>
	        		<td><input type="text" name="Search[q]" placeholder="Search keyword" class="form-control" value="{$Search.q}" /></td>
	        		<td><label class="control-label">Tìm theo</label></td>
	        		<td>
	        			<select class="form-control" name="Search[searchby]">
	        				
	        				{for $SB in $SearchBy as $SK}
	        				<option value="{$SK}"{if($SK == $Search.searchby)} selected{/if}>{$SB}</option>
	        				{/for}
	        				
	        			</select>
	        		</td>
	        		<td><label class="control-label">Xếp theo</label></td>
	        		<td>
	        			
	        			<select class="form-control" name="Search[sortby]">
	        				{for $SB in $SortBy as $SK}
		        			<option value="{$SK}"{if($Search.sortby == $SK)} selected{/if}>{$SB}</option>
	        				{/for}
	        			</select>
	        			
	        		</td>
	        		<td><label class="control-label">Status</label></td>
	        		<td>
	        			
	        			<select class="form-control" name="Search[status]">
		        			<option value="all"{if($Search.status == 'all')} selected{/if}>All</option>
	        				<option value="1"{if($Search.status == '1')} selected{/if}>Active</option>
	        				<option value="0"{if($Search.status == '0')} selected{/if}>Inactive</option>
	        			</select>
	        			
	        		</td>
	        		<td><label class="control-label">Show</label></td>
	        		<td><input type="number" class="form-control" value="{$Search.limit}" name="Search[limit]" /></td>
	        		<td><input type="submit" class="btn btn-primary" value="Search" /></td>
				</tr>
			</tbody>
		</table>
	</form>


	<div class="btn-group" style="margin-bottom: 5px">
		<div class="btn-group">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				Chọn chức năng				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu" role="menu">
				<li><a href="#" id="VNP_DeactiveNode"><span class="glyphicon glyphicon-remove"></span>&nbsp&nbspĐình chỉ</a></li>
				<li><a href="#" id="VNP_ActiveNode"><span class="glyphicon glyphicon-ok"></span>&nbsp&nbspKích hoạt</a></li>
				<li><a href="#" id="VNP_DeleteNode"><span class="glyphicon glyphicon-trash"></span>&nbsp&nbspXóa</a></li>
			</ul>
		</div>
	</div>
    <table class="table table-bordered table-striped table-hover">
        <colgroup>
        	<col class="col-xs" id="col-field-toggle">
            <col class="col-xs" id="col-field-code">
            <col class="col-xs" id="col-field-name">
            <col class="col-xs" id="col-field-phone">
            <col class="col-xs" id="col-field-invoice_status">
            <col class="col-xs" id="col-field-total">
            <col class="col-xs" id="col-functions">
        </colgroup>
        <thead>
	        <tr>
	        	<td>
	        		<div class="checkbox checkbox-success">
                        <input type="checkbox" id="VNP_ToggleAll" id="VNP_ToggleAll" value="1" />
                        <label for="VNP_ToggleAll"></label>
                    </div>
	        	</td>
	        	<td><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'invoice', 'action' => 'Main', 'params' => 'sortby/invoice_id/order/' . $Search['re_order'])){/function}"><strong>ID</strong></a></td>
	        	<td><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'invoice', 'action' => 'Main', 'params' => 'sortby/code/order/' . $Search['re_order'])){/function}"><strong>Mã đơn hàng</strong></a></td>
		    	<td><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'invoice', 'action' => 'Main', 'params' => 'sortby/name/order/' . $Search['re_order'])){/function}"><strong>Tên người nhận</strong></a></td>
		    	<td><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'invoice', 'action' => 'Main', 'params' => 'sortby/phone/order/' . $Search['re_order'])){/function}"><strong>Số điện thoại</strong></a></td>
		    	<td><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'invoice', 'action' => 'Main', 'params' => 'sortby/invoice_status/order/' . $Search['re_order'])){/function}"><strong>Tình trạng đơn hàng</strong></a></td>
		    	<td><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'invoice', 'action' => 'Main', 'params' => 'sortby/total/order/' . $Search['re_order'])){/function}"><strong>Tổng tiền</strong></a></td>
		    	<td><strong>Functions</strong></td>
	        </tr>
	    </thead>
	    <tbody>
	    	{if(!empty($Nodes))}
	    	{for $Node in $Nodes as $NodeID}
	    	<tr id="node-{$Node.invoice_id}" class="{if($Node.status)}Node_Active{else}Node_Inactive{/if}">
	    		<td>
	    			<div class="checkbox checkbox-success">
                        <input type="checkbox" name="VNP_ToggleItem[]" class="VNP_ToggleItem" id="ToggleItem-{$Node.invoice_id}" data-title="{$Node.invoice_id}" value="{$Node.invoice_id}" />
                        <label for="ToggleItem-{$Node.invoice_id}"></label>
                    </div>
	    		</td>
	    		<td>{$Node.invoice_id}</td>
	    		<td>{$Node.code}</td>
			    <td>{$Node.name}</td>
			    <td>{$Node.phone}</td>
			    {$FieldIdt = $Node['invoice_status']; $Opt = $Options['invoice_status']}
				<td>
                    {$Opt[$FieldIdt]}
                    <div class="timeAdd" style="margin-top:10px ">
                        <span class="label label-sm label-warning">{$Node.add_time|Filter::UnixTimeToFullDate:true}</span>
                    </div>
                </td>
				<td>{$Node.total|Filter::NumberFormat} đ</td>
				<td>
                    <a href="/admincp/invoice/printInvoice/{$NodeID}"><i class="fa fa-print"></i> Xem đặt hàng</a>&nbsp;
                	<a href="{function}echo Router::EditNode('invoice', $NodeID){/function}"><span class="glyphicon glyphicon-edit"></span>&nbsp{{lang::edit}}</a>&nbsp;&nbsp;
                    <a href="{function}echo Router::RemoveNode('invoice', $NodeID){/function}"><span class="glyphicon glyphicon-trash"></span>&nbsp{{lang::delete}}</a>
              	</td>
	    	</tr>
	    	{/for}
	    	{/if}
	    </tbody>
    </table>
</div>