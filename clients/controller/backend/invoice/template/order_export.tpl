<form action="" method="post">
    <div class="">
        <input type="radio" name="all" value="1" id="addTime"/> Toàn thời gian
    </div>
    <div class="">

        <input type="text" id="from" name="from" placeholder="from">
        <i class="fa fa-arrow-right"></i>
        <input type="text" id="to" name="to" placeholder="to">
    </div>
    <input type="hidden" name="export" value="1"/>
    <button type="submit" class="btn btn-success" style="margin-top: 20px">Export</button>
</form>
<script>
    $(function() {
        $( "#from" ).datepicker({
            defaultDate: "+1w",
            changeMonth: false,
            dateFormat: "dd/mm/yy",
            onClose: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
                $('#addTime').prop('checked', false);
            }
        });
        $( "#to" ).datepicker({
            defaultDate: "+1w",
            changeMonth: false,
            dateFormat: "dd/mm/yy",
            onClose: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
                $('#addTime').prop('checked', false);
            }
        });
    });
</script>