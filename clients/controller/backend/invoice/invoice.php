<?php

class invoice extends Controller {
	public $NodeTypeName;
	public function __construct() {
		$this->NodeTypeName = 'invoice';
	}
	public function Main() {
		require(CONTROLLER_PATH . Boot::$ControllerGroup . 'invoice' . DIRECTORY_SEPARATOR . 'invoice_ListRows.php');
	}
	public function AddNode() {
		Theme::UseJquery();
		Theme::JsFooter('AddNodeLib', APPLICATION_DATA_DIR . 'static/Node/add.js');
		$NodeID = 0;
		$FormValue = array();
		if(isset(G::$Registry['Params'][ROUTER_EXTRA_KEY])) {
			$NodeID = G::$Registry['Params'][ROUTER_EXTRA_KEY];
			$GetNode = DB::Query('invoice')->Where('invoice_id', '=', $NodeID)->Get();
			if($GetNode->num_rows == 1) {
				$FormValue = $GetNode->Result[0];
				DB::Query('invoice')->Where('invoice_id', '=', $NodeID)->Update(array('editing_id' => USER_ID));
			}
			else $NodeID = 0;
		}
		$FV = $this->SaveNodeAction($NodeID, $FormValue);
		if(!empty($FV)) $FormValue = array_merge($FormValue, $FV);
		$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups');
		$Fields = array(
            'name' => array('value' => isset($FormValue['name']) ? $FormValue['name'] : ''),
            'address' => array('value' => isset($FormValue['address']) ? $FormValue['address'] : ''),
            'phone' => array('value' => isset($FormValue['phone']) ? $FormValue['phone'] : ''),
            'province' => array('value' => isset($FormValue['province']) ? $FormValue['province'] : ''),
            'district' => array('value' => isset($FormValue['district']) ? $FormValue['district'] : ''),
            'order_id' => array('value' => isset($FormValue['order_id']) ? $FormValue['order_id'] : ''),
            'gift' => array('value' => isset($FormValue['gift']) ? $FormValue['gift'] : ''),
            'ship' => array('value' => isset($FormValue['ship']) ? $FormValue['ship'] : ''),
            'note' => array('value' => isset($FormValue['note']) ? $FormValue['note'] : ''),
            'invoice_status' => array('value' => isset($FormValue['invoice_status']) ? $FormValue['invoice_status'] : ''),
            'code' => array('value' => isset($FormValue['code']) ? $FormValue['code'] : ''),
            'payment' => array('value' => isset($FormValue['payment']) ? $FormValue['payment'] : ''),
            'product_id' => array('value' => isset($FormValue['product_id']) ? $FormValue['product_id'] : ''),
            'total' => array('value' => isset($FormValue['total']) ? $FormValue['total'] : ''))
        ;
		$vnp_title_ship = NodeBase::getNodes_Option('ship', 'title', 'ship_id');
		$Fields['ship']['Options'] = $vnp_title_ship;
		$vnp_title_payment = NodeBase::getNodes_Option('payment', 'title', 'payment_id');
		$Fields['payment']['Options'] = $vnp_title_payment;
		$order  = DB::Query('order')->Where('order_id','IN',$Fields['order_id']['value'])->Get()->Result;
        $product = DB::Query('product')->Where('product_id','IN',$Fields['product_id']['value'])->Get('product_id')->Result;
		
		Backend::$NodeSettings['status'] = isset($FormValue['status']) ? $FormValue['status'] : 1;
		Backend::$NodeSettings['priority'] = isset($FormValue['priority']) ? $FormValue['priority'] : 0;
		Backend::$NodeSettings['schedule'] = Filter::UnixTimeToDate(isset($FormValue['schedule']) ? $FormValue['schedule'] : 0);
		Backend::$NodeSettings['exprired'] = Filter::UnixTimeToDate(isset($FormValue['exprired']) ? $FormValue['exprired'] : 0);
		if(isset($FormValue['node_settings'])) Backend::$NodeSettings['extra'] = $FormValue['node_settings'];
		ob_start();
		echo '<form class="form-horizontal" action="" method="post">';
		echo '<input type="hidden" name="SaveNodeSubmit" value="1"/>';
		echo '<input type="hidden" name="NodeID" value="' . $NodeID . '"/>';
		include Form::$CompiledPath . 'Controller_invoice_InsertNode.php';
		echo '<div class="clearfix"></div><div class="clearfix" style="text-align:center;margin:10px 0 15px 0"><input type="submit" class="btn btn-primary" value="Save"/></div>';
		echo '</form>';
		$Form = ob_get_clean();
		$this->Render($Form);
	}

	public function Action() {
		$action = Input::Post('action');
		$ids = Input::Post('ids');
		$ids = array_filter(explode(',', $ids));
		$ids = array_map('intval', $ids);

		$rt = array('status' => 'not');
		if($action == 'deactive') {
			$Deactive = DB::Query('invoice')->Where('invoice_id', 'IN', $ids)->Update(array('status' => 0));
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		if($action == 'active') {
			$Deactive = DB::Query('invoice')->Where('invoice_id', 'IN', $ids)->Update(array('status' => 1));
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		if($action == 'delete') {
			$Deactive = DB::Query('invoice')->Where('invoice_id', 'IN', $ids)->Delete();
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		echo json_encode($rt);
		die();
	}

	public function RemoveNode() {
		$NodeID = 0;
		if(isset(G::$Registry['Params'][ROUTER_EXTRA_KEY])) {
			$NodeID = G::$Registry['Params'][ROUTER_EXTRA_KEY];
			$GetNode = DB::Query('invoice')->Where('invoice_id', '=', $NodeID)->Get();
			if($GetNode->num_rows == 0)
				return Helper::Notify('error', 'Node not found');
		}
		if(Input::Post('RemoveNodeSubmit') == 1) {
			$DeleteNode = DB::Query('invoice')->Where('invoice_id', '=', $NodeID)->Delete();
			if($DeleteNode->affected_rows == 1) {
				Helper::Notify('success', 'Success delete node ' .  $GetNode->Result[0]['title']);
				Header('Refresh: 1.5; url=' . Router::Generate('Controller', array('controller' => 'invoice')));
			}
			else Helper::Notify('error', 'Error delete node ' .  $GetNode->Result[0]['title']);
		}
		else {
			$config = array('action'	=> Router::GenerateThisRoute(),
							'tokens'	=> array(array('name' => 'NodeID', 'value' => $NodeID), array('name' => 'RemoveNodeSubmit', 'value' => 1))
							);
			$v = Access::Confirm('Confirm remove node: ' . $GetNode->Result[0]['title'], $config);
			$this->Render($v);
		}
	}

	public function SaveNodeAction($RealNodeID, $OldFormValue = array()) {
		if(Input::Post('SaveNodeSubmit') == 1) {
			$FormValue = Input::Post('Field');
			
			if(!isset($FormValue['name']) || $FormValue['name'] == '')
				Helper::Notify('error', 'Tên người nhận không thể để trống');

			if(!isset($FormValue['code']) || $FormValue['code'] == '')
				Helper::Notify('error', 'Mã đơn hàng không thể để trống');

			$FormValue['schedule'] = Filter::DateToUnixTime($FormValue['schedule']['date'], $FormValue['schedule']['hour'], $FormValue['schedule']['minute']);
			if($FormValue['schedule'] < CURRENT_TIME) $FormValue['schedule'] = CURRENT_TIME;
			$FormValue['exprired'] = Filter::DateToUnixTime($FormValue['exprired']['date'], $FormValue['exprired']['hour'], $FormValue['exprired']['minute']);
			if($FormValue['exprired'] < CURRENT_TIME) $FormValue['exprired'] = 0;
			$FormValue['node_settings'] = Backend::BuildNodePermission(Input::Post('VNP_Settings'));

			if(Helper::NotifyCount('error') == 0) {
				$NodeID = Input::Post('NodeID');
				if(empty($NodeID)) {
					$CheckUnique = array();
					$NodeExisted = false;

						if(!$NodeExisted) {
							$FormValue['lang'] = LANG;
							$FormValue['user_id'] = USER_ID;
							$FormValue['last_edit_id'] = USER_ID;
							$FormValue['add_time'] = CURRENT_TIME;
							$FormValue['edit_time'] = CURRENT_TIME;
							$NodeQuery = DB::Query('invoice')->Insert($FormValue);
							if($NodeQuery->status && $NodeQuery->insert_id > 0) {
								Helper::Notify('success', 'Successful add node in Đơn hàng');
							}
							else Helper::Notify('error', 'Cannot add node in Đơn hàng');
						}
						else Helper::Notify('error', 'Cannot add node in Đơn hàng. Be sure that <em></em> didn\'t existed!');
				}
				else {
					//$CheckExisted = DB::Query('invoice')->Where('invoice_id', '=', $NodeID)->Get();
					//if($CheckExisted->num_rows == 1) {
					if($RealNodeID == $NodeID) {
						$FormValue['last_edit_id'] = USER_ID;
						$FormValue['edit_time'] = CURRENT_TIME;
						$FormValue['editing_id'] = 0;
						$NodeQuery = DB::Query('invoice')->Where('invoice_id', '=', $NodeID)->Update($FormValue);
						if($NodeQuery->status && $NodeQuery->affected_rows > 0) {
							Helper::Notify('success', 'Successful update node in Đơn hàng');
						}
						else Helper::Notify('error', 'Cannot update node in Đơn hàng');
					}
					else {
						Helper::Notify('error', 'Cannot update, Node not found!');
					}
				}
			}

			return $FormValue;
		}
	}
    public function printInvoice(){
        if(isset(G::$Registry['Params'][ROUTER_EXTRA_KEY])) {
            $NodeID = G::$Registry['Params'][ROUTER_EXTRA_KEY];
            $invoice = DB::Query('invoice')->Where('invoice_id','=',$NodeID)->Get();
            if($invoice->status && $invoice->num_rows==1){
                $Node = $invoice->Result[0];
                Helper::State('Đơn hàng : '. $Node['code'],'');
                $province = getLocation('province');
                $district = getLocation('district');
                $payment = DB::Query('payment')->Get('payment_id')->Result;
                $ship = DB::Query('ship')->Get('ship_id')->Result;
                $order = DB::Query('order')->Where('order_id','INCLUDE',$Node['order_id'])->Get()->Result;
                $product = DB::Query('product')->Get('product_id')->Result;
                $v = $this->View('invoice')
                    ->Assign('node',$Node)
                    ->Assign('province',$province)
                    ->Assign('district',$district)
                    ->Assign('payment',$payment)
                    ->Assign('ship',$ship)
                    ->Assign('p',$product)
                    ->Assign('order',$order)
                    ->Output();
                $this->Render($v);
            }
            else {
                Helper::Notify('error','Không tồn tại hóa đơn này');
            }
        }
    }
    public function exportInvoice(){
        Helper::State('Order export','');
        $v = $this->View('order_export');
        $province = getLocation('province');
        $district = getLocation('district');
        $orderStatus = array(
            1=>'Đang chờ xác nhận',
            2=>'Đang chờ thanh toán',
            3=>'Đã xác nhận',
            4=>'Đang vận chuyển',
            5=>'Thành công'
        );
        if(Input::Post('export','')==1){
            if(Input::Post('all','')==1){
                $order = DB::Query('invoice')->Get('invoice_id')->Result;
            }
            elseif(Input::Post('from','')!='' && Input::Post('to','')!=''){
                $from = Filter::DateToUnixTime(Input::Post('from',''),0,0);
                $to = Filter::DateToUnixTime(Input::Post('to',''),0,0);
                $order = DB::Query('invoice')->Where('add_time','<=',$to)->_AND()->Where('add_time','>',$from)->Get('invoice_id')->Result;
            }
            $product = DB::Query('product')->Get('product_id')->Result;
            Boot::Library('PHPExcel');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);
            $i=1;
            $objPHPExcel->getActiveSheet()
                ->SetCellValue('A'.$i,'Mã đặt');
            $objPHPExcel->getActiveSheet()
                ->SetCellValue('B'.$i,'Ngày đặt');
            $objPHPExcel->getActiveSheet()
                ->SetCellValue('C'.$i,'Sản phẩm');
            $objPHPExcel->getActiveSheet()
                ->SetCellValue('D'.$i,'Tổng tiền');
            $objPHPExcel->getActiveSheet()
                ->SetCellValue('F'.$i,'Họ Tên');
            $objPHPExcel->getActiveSheet()
                ->SetCellValue('G'.$i,'Số điện thoại');
            $objPHPExcel->getActiveSheet()
                ->SetCellValue('H'.$i,'Địa chỉ');
            foreach($order as $key => $item){
                $objPHPExcel->getActiveSheet()
                    ->SetCellValue('A'.$i,$item['code']);
                $objPHPExcel->getActiveSheet()
                    ->SetCellValue('B'.$i,Filter::UnixTimeToDate($item['add_time'],true));
                $objPHPExcel->getActiveSheet()
                    ->SetCellValue('C'.$i,'');
                $objPHPExcel->getActiveSheet()
                    ->SetCellValue('D'.$i,$item['total']);
                $item['product_id'] = explode(',',$item['product_id']);
                $pString =  array();
                foreach($item['product_id'] as $pitem){
                    $pString[] = $product[$pitem]['title'];
                }
                $objPHPExcel->getActiveSheet()
                    ->SetCellValue('E'.$i,implode(',',$pString));
                $objPHPExcel->getActiveSheet()
                    ->SetCellValue('F'.$i,$item['name']);
                $objPHPExcel->getActiveSheet()
                    ->SetCellValue('G'.$i,$item['phone']);
                $objPHPExcel->getActiveSheet()
                    ->SetCellValue('H'.$i,$item['address'].',' .$district[$item['district']]['title'].','.$province[$item['province']]['title']);
                $objPHPExcel->getActiveSheet()
                    ->SetCellValue('I'.$i,$orderStatus[$item['invoice_status']]);
                $i= $i+1;
            }
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="invoice_'.date('h-s_d-m-y').'.xls"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
        }
        $this->Render($v->Output());
    }
    public function changeStatus(){

        $id = Input::Post('id',0);
        $status = Input::Post('status',1);
        $checkinvoice = DB::Query('invoice')->Where('invoice_id','=',$id)->Get();
        if($checkinvoice->status && $checkinvoice->num_rows==1){
            $invoice = $checkinvoice->Result[0];
            $update = DB::Query('invoice')->Where('invoice_id','=',$id)->Update(array('invoice_status'=>$status));
            if($update->status && $update->affected_rows>0) {
                $log = array(
                    'invoice_id'=>$invoice['code'],
                    'edit_time' =>CURRENT_TIME,
                    'status'    => $status,
                );
                $insert = DB::Query('invoice_log')->Insert($log);
                echo "ok";
            }
        }
        die();

    }
}