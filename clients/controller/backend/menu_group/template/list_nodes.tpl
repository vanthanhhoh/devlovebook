<div class="table-responsive">
	<form class="form-horizontal" action="{$SA}" method="post">
		<input type="hidden" name="SearchUsers" value="1">
		<table class="table table-bordered table-striped table-hover">
	        <colgroup>
	        	<col class="col-xs-2">
	            <col class="col-xs-1">
	            <col class="col-xs-2">
	            <col class="col-xs-1">
	            <col class="col-xs-2">
	            <col class="col-xs">
	            <col class="col-xs-1">
	            <col class="col-xs">
	            <col class="col-xs-1">
	            <col class="col-xs-1">
	        </colgroup>
	        <tbody>
	        		        	<tr>
	        		<td><input type="text" name="Search[q]" placeholder="Search keyword" class="form-control" value="{$Search.q}" /></td>
	        		<td><label class="control-label">Tìm theo</label></td>
	        		<td>
	        			<select class="form-control" name="Search[searchby]">
	        				
	        				{for $SB in $SearchBy as $SK}
	        				<option value="{$SK}"{if($SK == $Search.searchby)} selected{/if}>{$SB}</option>
	        				{/for}
	        				
	        			</select>
	        		</td>
	        		<td><label class="control-label">Xếp theo</label></td>
	        		<td>
	        			
	        			<select class="form-control" name="Search[sortby]">
	        				{for $SB in $SortBy as $SK}
		        			<option value="{$SK}"{if($Search.sortby == $SK)} selected{/if}>{$SB}</option>
	        				{/for}
	        			</select>
	        			
	        		</td>
	        		<td><label class="control-label">Status</label></td>
	        		<td>
	        			
	        			<select class="form-control" name="Search[status]">
		        			<option value="all"{if($Search.status == 'all')} selected{/if}>All</option>
	        				<option value="1"{if($Search.status == '1')} selected{/if}>Active</option>
	        				<option value="0"{if($Search.status == '0')} selected{/if}>Inactive</option>
	        			</select>
	        			
	        		</td>
	        		<td><label class="control-label">Show</label></td>
	        		<td><input type="number" class="form-control" value="{$Search.limit}" name="Search[limit]" /></td>
	        		<td><input type="submit" class="btn btn-primary" value="Search" /></td>
				</tr>
			</tbody>
		</table>
	</form>


	<div class="btn-group" style="margin-bottom: 5px">
		<div class="btn-group">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				Chọn chức năng				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu" role="menu">
				<li><a href="#" id="VNP_DeactiveNode"><span class="glyphicon glyphicon-remove"></span>&nbsp&nbspĐình chỉ</a></li>
				<li><a href="#" id="VNP_ActiveNode"><span class="glyphicon glyphicon-ok"></span>&nbsp&nbspKích hoạt</a></li>
				<li><a href="#" id="VNP_DeleteNode"><span class="glyphicon glyphicon-trash"></span>&nbsp&nbspXóa</a></li>
			</ul>
		</div>
	</div>
    <table class="table table-bordered table-striped table-hover">
        <colgroup>
        	<col class="col-xs" id="col-field-toggle">        	<col class="col-xs" id="col-field-title">
        <col class="col-xs" id="col-functions">
        </colgroup>
        <thead>
	        <tr>
	        	<td>
	        		<div class="checkbox checkbox-success">
                        <input type="checkbox" id="VNP_ToggleAll" id="VNP_ToggleAll" value="1" />
                        <label for="VNP_ToggleAll"></label>
                    </div>
	        	</td>
	        	<td><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'menu_group', 'action' => 'Main', 'params' => 'sortby/menu_group_id/order/' . $Search['re_order'])){/function}"><strong>ID</strong></a></td>
	        		        		        				    	<td><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'menu_group', 'action' => 'Main', 'params' => 'sortby/title/order/' . $Search['re_order'])){/function}"><strong>Tiêu đề</strong></a></td>
		    		        		        	<td><strong>Functions</strong></td>
	        </tr>
	    </thead>
	    <tbody>
	    	{if(!empty($Nodes))}
	    	{for $Node in $Nodes as $NodeID}
	    	<tr id="node-{$Node.menu_group_id}" class="{if($Node.status)}Node_Active{else}Node_Inactive{/if}">
	    		<td>
	    			<div class="checkbox checkbox-success">
                        <input type="checkbox" name="VNP_ToggleItem[]" class="VNP_ToggleItem" id="ToggleItem-{$Node.menu_group_id}" data-title="{$Node.menu_group_id}" value="{$Node.menu_group_id}" />
                        <label for="ToggleItem-{$Node.menu_group_id}"></label>
                    </div>
	    		</td>
	    		<td>{$Node.menu_group_id}</td>
	    				    				    					    	<td>{$Node.title}</td>
			    		    			    		<td>
                	<a href="{function}echo Router::EditNode('menu_group', $NodeID){/function}"><span class="glyphicon glyphicon-edit"></span>&nbsp{{lang::edit}}</a>&nbsp;&nbsp;
                    <a href="{function}echo Router::RemoveNode('menu_group', $NodeID){/function}"><span class="glyphicon glyphicon-trash"></span>&nbsp{{lang::delete}}</a>
              	</td>
	    	</tr>
	    	{/for}
	    	{/if}
	    </tbody>
    </table>
</div>