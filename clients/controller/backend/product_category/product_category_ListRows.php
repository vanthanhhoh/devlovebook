<?php
if(!class_exists('product_category')) die('Stop here!');
$Search = array('sortby' 	=> 'product_category_id',
				'order'	=> 'DESC',
				'q'		=> '',
				'searchby'=> 'title',
				'status'	=> 'all',
				'limit'	=> 20);
$Search['parent_id'] = 0;
$SortBy = array (
  'title' => 'Tiêu đề',
  'parent_id' => 'Danh mục cha',
);
$SearchBy = array (
  'title' => 'Tiêu đề',
);
$_RefFilter = array (
  'parent_id' => 
  array (
    'label' => 'Danh mục cha',
    'nodetype' => 'product_category',
    'nodefield' => 'title',
  ),
);
$FromForm = Input::Post('Search', array());
$Search = array_merge($Search, G::$Registry['Params']);
$Search = array_merge($Search, $FromForm);
$Search['q'] = urldecode($Search['q']);
Router::BuildParamsString($Search, true);
$Search['re_order'] = ($Search['order'] == 'desc') ? 'asc' : 'desc';
G::$Session->Set('user_sort', $Search['order']);
Helper::State('List nodes', Router::GenerateThisRoute());
$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups,InputGroups,Tables,ButtonGroups,Dropdowns');
Theme::JqueryPlugin('InputToggle');
Theme::JsFooter('ListNodeLib', APPLICATION_DATA_DIR . 'static/Node/list.js');
Theme::JsFooter('ControllerName', 'var NodeController = \'product_category\';', 'inline');
$Adapters['vnp_title_product_category'] = NodeBase::getNodes_Option('product_category', 'title', 'product_category_id');
$Adapters['vnp_title_product_category_id_key'] = NodeBase::getNodes_IDKey('product_category', 'title', 'product_category_id');
$ShowFields = array (
  0 => 'title',
  1 => 'parent_id',
  2 => 'product_category_id',
  3 => 'status',
);
$GetNodes = DB::Query('product_category')->Columns($ShowFields);
$GetNodes = $GetNodes->Limit($Search['limit']*1);
$GetNodes = $GetNodes->Order($Search['sortby'], $Search['order']);
$GetNodes = $GetNodes->Where('product_category_id', '>', 0);
if($Search['status'] != 'all')
$GetNodes = $GetNodes->_AND()->Where('status', '=', $Search['status']);
foreach($_RefFilter as $RF => $RFD) {
		if(isset($Search[$RF]) && $Search[$RF] > 0)
		$GetNodes = $GetNodes->_AND()
								->WhereGroupOpen()
									->Where($RF, '=', intval($Search[$RF]))->_OR()
									->Where($RF, 'INCLUDE', $Search[$RF])
								->WhereGroupClose();
}
if(!empty($Search['q']))
$GetNodes = $GetNodes->_AND()
								->WhereGroupOpen()
									->Where($Search['searchby'], 'SEARCH', $Search['q'])->_OR()
									->Where($Search['searchby'], 'LIKE', $Search['q'])
								->WhereGroupClose();
$GetNodes = $GetNodes->Get('product_category_id', Output::Paging());
$v = $this->View('list_nodes');
$v->Assign('Search', $Search);
$v->Assign('SortBy', $SortBy);
$v->Assign('SA', Router::GenerateThisRoute());
$v->Assign('SearchBy', $SearchBy);
$v->Assign('Adapters', $Adapters);
if($GetNodes->num_rows > 0) {
	$v->Assign('Nodes', $GetNodes->Result);
} else Helper::Notify('info', 'There is no nodes found!');
$this->Render($v->Output());
Helper::PageInfo('List nodes for Danh mục sản phẩm ( Found ' . $GetNodes->num_rows . ' nodes )');
Helper::FeaturedPanel('Add new Danh mục sản phẩm',BASE_DIR . 'product_category/AddNode', 'plus');