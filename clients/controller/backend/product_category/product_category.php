<?php

class product_category extends Controller {
	public $NodeTypeName;
	public function __construct() {
		$this->NodeTypeName = 'product_category';
	}
	public function Main() {
		require(CONTROLLER_PATH . Boot::$ControllerGroup . 'product_category' . DIRECTORY_SEPARATOR . 'product_category_ListRows.php');
	}
	public function AddNode() {
		Theme::UseJquery();
		Theme::JsFooter('AddNodeLib', APPLICATION_DATA_DIR . 'static/Node/add.js');
		$NodeID = 0;
		$FormValue = array();
		if(isset(G::$Registry['Params'][ROUTER_EXTRA_KEY])) {
			$NodeID = G::$Registry['Params'][ROUTER_EXTRA_KEY];
			$GetNode = DB::Query('product_category')->Where('product_category_id', '=', $NodeID)->Get();
			if($GetNode->num_rows == 1) {
				$FormValue = $GetNode->Result[0];
				DB::Query('product_category')->Where('product_category_id', '=', $NodeID)->Update(array('editing_id' => USER_ID));
			}
			else $NodeID = 0;
		}
		$FV = $this->SaveNodeAction($NodeID, $FormValue);
		if(!empty($FV)) $FormValue = array_merge($FormValue, $FV);
		$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups');
		$Fields = array('title' => array('value' => isset($FormValue['title']) ? $FormValue['title'] : ''),'url' => array('value' => isset($FormValue['url']) ? $FormValue['url'] : ''),'banner' => array('value' => isset($FormValue['banner']) ? $FormValue['banner'] : ''),'mega_image' => array('value' => isset($FormValue['mega_image']) ? $FormValue['mega_image'] : ''),'parent_id' => array('value' => isset($FormValue['parent_id']) ? $FormValue['parent_id'] : ''));
		$vnp_title_product_category = NodeBase::getNodes_Option('product_category', 'title', 'product_category_id');
		$Fields['parent_id']['Options'] = $vnp_title_product_category;
		
		
		Backend::$NodeSettings['status'] = isset($FormValue['status']) ? $FormValue['status'] : 1;
		Backend::$NodeSettings['priority'] = isset($FormValue['priority']) ? $FormValue['priority'] : 0;
		Backend::$NodeSettings['schedule'] = Filter::UnixTimeToDate(isset($FormValue['schedule']) ? $FormValue['schedule'] : 0);
		Backend::$NodeSettings['exprired'] = Filter::UnixTimeToDate(isset($FormValue['exprired']) ? $FormValue['exprired'] : 0);
		if(isset($FormValue['node_settings'])) Backend::$NodeSettings['extra'] = $FormValue['node_settings'];
		ob_start();
		echo '<form class="form-horizontal" action="" method="post">';
		echo '<input type="hidden" name="SaveNodeSubmit" value="1"/>';
		echo '<input type="hidden" name="NodeID" value="' . $NodeID . '"/>';
		include Form::$CompiledPath . 'Controller_product_category_InsertNode.php';
		echo '<div class="clearfix"></div><div class="clearfix" style="text-align:center;margin:10px 0 15px 0"><input type="submit" class="btn btn-primary" value="Save"/></div>';
		echo '</form>';
		$Form = ob_get_clean();
		$this->Render($Form);
	}

	public function Action() {
		$action = Input::Post('action');
		$ids = Input::Post('ids');
		$ids = array_filter(explode(',', $ids));
		$ids = array_map('intval', $ids);

		$rt = array('status' => 'not');
		if($action == 'deactive') {
			$Deactive = DB::Query('product_category')->Where('product_category_id', 'IN', $ids)->Update(array('status' => 0));
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		if($action == 'active') {
			$Deactive = DB::Query('product_category')->Where('product_category_id', 'IN', $ids)->Update(array('status' => 1));
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		if($action == 'delete') {
			$Deactive = DB::Query('product_category')->Where('product_category_id', 'IN', $ids)->Delete();
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		echo json_encode($rt);
		die();
	}

	public function RemoveNode() {
		$NodeID = 0;
		if(isset(G::$Registry['Params'][ROUTER_EXTRA_KEY])) {
			$NodeID = G::$Registry['Params'][ROUTER_EXTRA_KEY];
			$GetNode = DB::Query('product_category')->Where('product_category_id', '=', $NodeID)->Get();
			if($GetNode->num_rows == 0)
				return Helper::Notify('error', 'Node not found');
		}
		if(Input::Post('RemoveNodeSubmit') == 1) {
			$DeleteNode = DB::Query('product_category')->Where('product_category_id', '=', $NodeID)->Delete();
			if($DeleteNode->affected_rows == 1) {
				Helper::Notify('success', 'Success delete node ' .  $GetNode->Result[0]['title']);
				Header('Refresh: 1.5; url=' . Router::Generate('Controller', array('controller' => 'product_category')));
			}
			else Helper::Notify('error', 'Error delete node ' .  $GetNode->Result[0]['title']);
		}
		else {
			$config = array('action'	=> Router::GenerateThisRoute(),
							'tokens'	=> array(array('name' => 'NodeID', 'value' => $NodeID), array('name' => 'RemoveNodeSubmit', 'value' => 1))
							);
			$v = Access::Confirm('Confirm remove node: ' . $GetNode->Result[0]['title'], $config);
			$this->Render($v);
		}
	}

	public function SaveNodeAction($RealNodeID, $OldFormValue = array()) {
		if(Input::Post('SaveNodeSubmit') == 1) {
			$FormValue = Input::Post('Field');
			
			if(!isset($FormValue['title']) || $FormValue['title'] == '')
				Helper::Notify('error', 'Tiêu đề không thể để trống');

			if(isset($FormValue['title']) && (!isset($FormValue['url']) || $FormValue['url'] == ''))
				$FormValue['url'] = $FormValue['title'];

			if(!isset($FormValue['url']) || $FormValue['url'] == '')
				Helper::Notify('error', 'Đường dẫn không thể để trống');

			$FormValue['url'] = strtolower(Filter::CleanUrlString($FormValue['url']));

			$FormValue['banner'] = array_filter($FormValue['banner']);
			$FormValue['banner'] = implode(',', $FormValue['banner']);



			$FormValue['schedule'] = Filter::DateToUnixTime($FormValue['schedule']['date'], $FormValue['schedule']['hour'], $FormValue['schedule']['minute']);
			if($FormValue['schedule'] < CURRENT_TIME) $FormValue['schedule'] = CURRENT_TIME;
			$FormValue['exprired'] = Filter::DateToUnixTime($FormValue['exprired']['date'], $FormValue['exprired']['hour'], $FormValue['exprired']['minute']);
			if($FormValue['exprired'] < CURRENT_TIME) $FormValue['exprired'] = 0;
			$FormValue['node_settings'] = Backend::BuildNodePermission(Input::Post('VNP_Settings'));

			if(Helper::NotifyCount('error') == 0) {
				$NodeID = Input::Post('NodeID');
				if(empty($NodeID)) {
					$CheckUnique = array();
					$NodeExisted = false;

						if(!$NodeExisted) {
							$FormValue['lang'] = LANG;
							$FormValue['user_id'] = USER_ID;
							$FormValue['last_edit_id'] = USER_ID;
							$FormValue['add_time'] = CURRENT_TIME;
							$FormValue['edit_time'] = CURRENT_TIME;
							$NodeQuery = DB::Query('product_category')->Insert($FormValue);
							if($NodeQuery->status && $NodeQuery->insert_id > 0) {
								Helper::Notify('success', 'Successful add node in Danh mục sản phẩm');
							}
							else Helper::Notify('error', 'Cannot add node in Danh mục sản phẩm');
						}
						else Helper::Notify('error', 'Cannot add node in Danh mục sản phẩm. Be sure that <em></em> didn\'t existed!');
				}
				else {
					//$CheckExisted = DB::Query('product_category')->Where('product_category_id', '=', $NodeID)->Get();
					//if($CheckExisted->num_rows == 1) {
					if($RealNodeID == $NodeID) {
						$FormValue['last_edit_id'] = USER_ID;
						$FormValue['edit_time'] = CURRENT_TIME;
						$FormValue['editing_id'] = 0;
						$NodeQuery = DB::Query('product_category')->Where('product_category_id', '=', $NodeID)->Update($FormValue);
						if($NodeQuery->status && $NodeQuery->affected_rows > 0) {
							Helper::Notify('success', 'Successful update node in Danh mục sản phẩm');
						}
						else Helper::Notify('error', 'Cannot update node in Danh mục sản phẩm');
					}
					else {
						Helper::Notify('error', 'Cannot update, Node not found!');
					}
				}
			}

			return $FormValue;
		}
	}
}