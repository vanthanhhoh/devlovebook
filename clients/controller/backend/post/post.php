<?php

class post extends Controller {
	public $NodeTypeName;
	public function __construct() {
		$this->NodeTypeName = 'post';
	}
	public function Main() {
		require(CONTROLLER_PATH . Boot::$ControllerGroup . 'post' . DIRECTORY_SEPARATOR . 'post_ListRows.php');
	}
	public function AddNode() {
		Theme::UseJquery();
		Theme::JsFooter('AddNodeLib', APPLICATION_DATA_DIR . 'static/Node/add.js');
		$NodeID = 0;
		$FormValue = array();
		if(isset(G::$Registry['Params'][ROUTER_EXTRA_KEY])) {
			$NodeID = G::$Registry['Params'][ROUTER_EXTRA_KEY];
			$GetNode = DB::Query('post')->Where('post_id', '=', $NodeID)->Get();
			if($GetNode->num_rows == 1) {
				$FormValue = $GetNode->Result[0];
				DB::Query('post')->Where('post_id', '=', $NodeID)->Update(array('editing_id' => USER_ID));
			}
			else $NodeID = 0;
		}
		$FV = $this->SaveNodeAction($NodeID, $FormValue);
		if(!empty($FV)) $FormValue = array_merge($FormValue, $FV);
		$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups');
		$Fields = array('title' => array('value' => isset($FormValue['title']) ? $FormValue['title'] : ''),'url' => array('value' => isset($FormValue['url']) ? $FormValue['url'] : ''),'image' => array('value' => isset($FormValue['image']) ? $FormValue['image'] : ''),'description' => array('value' => isset($FormValue['description']) ? $FormValue['description'] : ''),'meta_des' => array('value' => isset($FormValue['meta_des']) ? $FormValue['meta_des'] : ''),'meta_key' => array('value' => isset($FormValue['meta_key']) ? $FormValue['meta_key'] : ''),'post_category' => array('value' => isset($FormValue['post_category']) ? $FormValue['post_category'] : ''),'content' => array('value' => isset($FormValue['content']) ? $FormValue['content'] : ''));
		$vnp_title_post_category = NodeBase::getNodes_Option('post_category', 'title', 'post_category_id');
		$Fields['post_category']['Options'] = $vnp_title_post_category;
		Boot::Library('Editor');
		Editor::AddEditor('#ID_Field_content');
		Editor::Replace();
		
		
		Backend::$NodeSettings['status'] = isset($FormValue['status']) ? $FormValue['status'] : 1;
		Backend::$NodeSettings['priority'] = isset($FormValue['priority']) ? $FormValue['priority'] : 0;
		Backend::$NodeSettings['schedule'] = Filter::UnixTimeToDate(isset($FormValue['schedule']) ? $FormValue['schedule'] : 0);
		Backend::$NodeSettings['exprired'] = Filter::UnixTimeToDate(isset($FormValue['exprired']) ? $FormValue['exprired'] : 0);
		if(isset($FormValue['node_settings'])) Backend::$NodeSettings['extra'] = $FormValue['node_settings'];
		ob_start();
		echo '<form class="form-horizontal" action="" method="post">';
		echo '<input type="hidden" name="SaveNodeSubmit" value="1"/>';
		echo '<input type="hidden" name="NodeID" value="' . $NodeID . '"/>';
		include Form::$CompiledPath . 'Controller_post_InsertNode.php';
		echo '<div class="clearfix"></div><div class="clearfix" style="text-align:center;margin:10px 0 15px 0"><input type="submit" class="btn btn-primary" value="Save"/></div>';
		echo '</form>';
		$Form = ob_get_clean();
		$this->Render($Form);
	}

	public function Action() {
		$action = Input::Post('action');
		$ids = Input::Post('ids');
		$ids = array_filter(explode(',', $ids));
		$ids = array_map('intval', $ids);

		$rt = array('status' => 'not');
		if($action == 'deactive') {
			$Deactive = DB::Query('post')->Where('post_id', 'IN', $ids)->Update(array('status' => 0));
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		if($action == 'active') {
			$Deactive = DB::Query('post')->Where('post_id', 'IN', $ids)->Update(array('status' => 1));
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		if($action == 'delete') {
			$Deactive = DB::Query('post')->Where('post_id', 'IN', $ids)->Delete();
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		echo json_encode($rt);
		die();
	}

	public function RemoveNode() {
		$NodeID = 0;
		if(isset(G::$Registry['Params'][ROUTER_EXTRA_KEY])) {
			$NodeID = G::$Registry['Params'][ROUTER_EXTRA_KEY];
			$GetNode = DB::Query('post')->Where('post_id', '=', $NodeID)->Get();
			if($GetNode->num_rows == 0)
				return Helper::Notify('error', 'Node not found');
		}
		if(Input::Post('RemoveNodeSubmit') == 1) {
			$DeleteNode = DB::Query('post')->Where('post_id', '=', $NodeID)->Delete();
			if($DeleteNode->affected_rows == 1) {
				Helper::Notify('success', 'Success delete node ' .  $GetNode->Result[0]['title']);
				Header('Refresh: 1.5; url=' . Router::Generate('Controller', array('controller' => 'post')));
			}
			else Helper::Notify('error', 'Error delete node ' .  $GetNode->Result[0]['title']);
		}
		else {
			$config = array('action'	=> Router::GenerateThisRoute(),
							'tokens'	=> array(array('name' => 'NodeID', 'value' => $NodeID), array('name' => 'RemoveNodeSubmit', 'value' => 1))
							);
			$v = Access::Confirm('Confirm remove node: ' . $GetNode->Result[0]['title'], $config);
			$this->Render($v);
		}
	}

	public function SaveNodeAction($RealNodeID, $OldFormValue = array()) {
		if(Input::Post('SaveNodeSubmit') == 1) {
			$FormValue = Input::Post('Field');
			
			if(!isset($FormValue['title']) || $FormValue['title'] == '')
				Helper::Notify('error', 'Tiêu đề không thể để trống');

			if(isset($FormValue['title']) && (!isset($FormValue['url']) || $FormValue['url'] == ''))
				$FormValue['url'] = $FormValue['title'];

			if(!isset($FormValue['url']) || $FormValue['url'] == '')
				Helper::Notify('error', 'Đường dẫn không thể để trống');

			$FormValue['url'] = strtolower(Filter::CleanUrlString($FormValue['url']));

			if(!isset($FormValue['image']) || $FormValue['image'] == '')
				Helper::Notify('error', 'Hình đại diện không thể để trống');

			if(!isset($FormValue['content']) || $FormValue['content'] == '')
				Helper::Notify('error', 'Nội dung không thể để trống');

			$FormValue['schedule'] = Filter::DateToUnixTime($FormValue['schedule']['date'], $FormValue['schedule']['hour'], $FormValue['schedule']['minute']);
			if($FormValue['schedule'] < CURRENT_TIME) $FormValue['schedule'] = CURRENT_TIME;
			$FormValue['exprired'] = Filter::DateToUnixTime($FormValue['exprired']['date'], $FormValue['exprired']['hour'], $FormValue['exprired']['minute']);
			if($FormValue['exprired'] < CURRENT_TIME) $FormValue['exprired'] = 0;
			$FormValue['node_settings'] = Backend::BuildNodePermission(Input::Post('VNP_Settings'));

			if(Helper::NotifyCount('error') == 0) {
				$NodeID = Input::Post('NodeID');
				if(empty($NodeID)) {
					$CheckUnique = array();
					$NodeExisted = false;

						if(!$NodeExisted) {
							$FormValue['lang'] = LANG;
							$FormValue['user_id'] = USER_ID;
							$FormValue['last_edit_id'] = USER_ID;
							$FormValue['add_time'] = CURRENT_TIME;
							$FormValue['edit_time'] = CURRENT_TIME;
							$NodeQuery = DB::Query('post')->Insert($FormValue);
							if($NodeQuery->status && $NodeQuery->insert_id > 0) {
								Helper::Notify('success', 'Successful add node in Bài viết');
							}
							else Helper::Notify('error', 'Cannot add node in Bài viết');
						}
						else Helper::Notify('error', 'Cannot add node in Bài viết. Be sure that <em></em> didn\'t existed!');
				}
				else {
					//$CheckExisted = DB::Query('post')->Where('post_id', '=', $NodeID)->Get();
					//if($CheckExisted->num_rows == 1) {
					if($RealNodeID == $NodeID) {
						$FormValue['last_edit_id'] = USER_ID;
						$FormValue['edit_time'] = CURRENT_TIME;
						$FormValue['editing_id'] = 0;
						$NodeQuery = DB::Query('post')->Where('post_id', '=', $NodeID)->Update($FormValue);
						if($NodeQuery->status && $NodeQuery->affected_rows > 0) {
							Helper::Notify('success', 'Successful update node in Bài viết');
						}
						else Helper::Notify('error', 'Cannot update node in Bài viết');
					}
					else {
						Helper::Notify('error', 'Cannot update, Node not found!');
					}
				}
			}

			return $FormValue;
		}
	}
}