<?php
if(!class_exists('post')) die('Stop here!');
$Search = array('sortby' 	=> 'post_id',
				'order'	=> 'DESC',
				'q'		=> '',
				'searchby'=> 'title',
				'status'	=> 'all',
				'limit'	=> 20);
$Search['post_category'] = 0;
$SortBy = array (
  'title' => 'Tiêu đề',
  'post_category' => 'Danh mục',
);
$SearchBy = array (
  'title' => 'Tiêu đề',
);
$_RefFilter = array (
  'post_category' => 
  array (
    'label' => 'Danh mục',
    'nodetype' => 'post_category',
    'nodefield' => 'title',
  ),
);
$FromForm = Input::Post('Search', array());
$Search = array_merge($Search, G::$Registry['Params']);
$Search = array_merge($Search, $FromForm);
$Search['q'] = urldecode($Search['q']);
Router::BuildParamsString($Search, true);
$Search['re_order'] = ($Search['order'] == 'desc') ? 'asc' : 'desc';
G::$Session->Set('user_sort', $Search['order']);
Helper::State('List nodes', Router::GenerateThisRoute());
$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups,InputGroups,Tables,ButtonGroups,Dropdowns');
Theme::JqueryPlugin('InputToggle');
Theme::JsFooter('ListNodeLib', APPLICATION_DATA_DIR . 'static/Node/list.js');
Theme::JsFooter('ControllerName', 'var NodeController = \'post\';', 'inline');
$Adapters['vnp_title_post_category'] = NodeBase::getNodes_Option('post_category', 'title', 'post_category_id');
$Adapters['vnp_title_post_category_id_key'] = NodeBase::getNodes_IDKey('post_category', 'title', 'post_category_id');
$ShowFields = array (
  0 => 'title',
  1 => 'url',
  2 => 'image',
  3 => 'post_category',
  4 => 'post_id',
  5 => 'status',
);
$GetNodes = DB::Query('post')->Columns($ShowFields);
$GetNodes = $GetNodes->Limit($Search['limit']*1);
$GetNodes = $GetNodes->Order($Search['sortby'], $Search['order']);
$GetNodes = $GetNodes->Where('post_id', '>', 0);
if($Search['status'] != 'all')
$GetNodes = $GetNodes->_AND()->Where('status', '=', $Search['status']);
foreach($_RefFilter as $RF => $RFD) {
		if(isset($Search[$RF]) && $Search[$RF] > 0)
		$GetNodes = $GetNodes->_AND()
								->WhereGroupOpen()
									->Where($RF, '=', intval($Search[$RF]))->_OR()
									->Where($RF, 'INCLUDE', $Search[$RF])
								->WhereGroupClose();
}
if(!empty($Search['q']))
$GetNodes = $GetNodes->_AND()
								->WhereGroupOpen()
									->Where($Search['searchby'], 'SEARCH', $Search['q'])->_OR()
									->Where($Search['searchby'], 'LIKE', $Search['q'])
								->WhereGroupClose();
$GetNodes = $GetNodes->Get('post_id', Output::Paging());
$v = $this->View('list_nodes');
$v->Assign('Search', $Search);
$v->Assign('SortBy', $SortBy);
$v->Assign('SA', Router::GenerateThisRoute());
$v->Assign('SearchBy', $SearchBy);
$v->Assign('Adapters', $Adapters);
if($GetNodes->num_rows > 0) {
	$v->Assign('Nodes', $GetNodes->Result);
} else Helper::Notify('info', 'There is no nodes found!');
$this->Render($v->Output());
Helper::PageInfo('List nodes for Bài viết ( Found ' . $GetNodes->num_rows . ' nodes )');
Helper::FeaturedPanel('Add new Bài viết',BASE_DIR . 'post/AddNode', 'plus');