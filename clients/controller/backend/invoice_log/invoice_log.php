<?php
/**
 * Created by PhpStorm.
 * User: THANH
 * Date: 22/08/2016
 * Time: 2:11 CH
 */

class invoice_log extends Controller {
    public function listLog(){
        $Search = array(
            'sortby' 	=> 'invoice_id',
            'order'	=> 'DESC',
            'q'		=> '',
            'searchby'=> 'invoice_id',
            'status'	=> 'all',
            'limit'	=> 20);
        $SortBy = array (
            'invoice_id' => 'Mã đơn hàng',
            'edit_time' => 'Thời gian',
            'status' => 'Tình trạng đơn hàng'
        );
        $SearchBy = array (
            'invoice_id' => 'Mã đơn hàng'
        );
        $_RefFilter = array (
        );
        $FromForm = Input::Post('Search', array());

        $Search = array_merge($Search, G::$Registry['Params']);
        $Search = array_merge($Search, $FromForm);
        $Search['q'] = urldecode($Search['q']);
        Router::BuildParamsString($Search, true);
        $Search['re_order'] = ($Search['order'] == 'desc') ? 'asc' : 'desc';
        G::$Session->Set('user_sort', $Search['order']);
        Helper::State('List nodes', Router::GenerateThisRoute());
        $this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups,InputGroups,Tables,ButtonGroups,Dropdowns');
        Theme::JqueryPlugin('InputToggle');
        Theme::JsFooter('ListNodeLib', APPLICATION_DATA_DIR . 'static/Node/list.js');
        Theme::JsFooter('ControllerName', 'var NodeController = \'invoice\';', 'inline');

        $Options['invoice_status'] = array (
            1 => 'Đang chờ xác nhận',
            2 => 'Đã chờ thanh toán',
            3 => 'Đã xác nhận',
            4 => 'Đang vận chuyển',
            5 => 'Thành công',
        );
        $ShowFields = array (
            0 => 'invoice_log_id',
            1 => 'invoice_id',
            2 => 'edit_time',
            3 => 'status'
        );
        $GetNodes = DB::Query('invoice_log')->Columns($ShowFields);
        $GetNodes = $GetNodes->Limit($Search['limit']*1);
        $GetNodes = $GetNodes->Order($Search['sortby'], $Search['order']);
        $GetNodes = $GetNodes->Where('invoice_log_id', '>', 0);
        if($Search['status'] != 'all')
            $GetNodes = $GetNodes->_AND()->Where('status', '=', $Search['status']);

        if(!empty($Search['q']))
            $GetNodes = $GetNodes->_AND()
                ->Where($Search['searchby'], '=', $Search['q']);
        $GetNodes = $GetNodes->Get('invoice_log_id', Output::Paging());
        $v = $this->View('list_nodes');
        $v->Assign('Search', $Search);
        $v->Assign('SortBy', $SortBy);
        $v->Assign('SA', Router::GenerateThisRoute());
        $v->Assign('SearchBy', $SearchBy);
        if($GetNodes->num_rows > 0) {
            $v->Assign('Nodes', $GetNodes->Result);
            $v->Assign('Options', $Options);
        } else Helper::Notify('info', 'There is no nodes found!');
        $this->Render($v->Output());
        Helper::PageInfo('List nodes for Đơn hàng ( Found ' . $GetNodes->num_rows . ' nodes )');
    }
}