<div class="table-responsive">
    <form class="form-horizontal" action="{$SA}" method="post">
        <input type="hidden" name="SearchUsers" value="1">
        <table class="table table-bordered table-striped table-hover">
            <colgroup>
                <col class="col-xs-2">
                <col class="col-xs-1">
                <col class="col-xs-2">
                <col class="col-xs-1">
                <col class="col-xs-2">
                <col class="col-xs">
                <col class="col-xs">
                <col class="col-xs">
                <col class="col-xs">
                <col class="col-xs">
            </colgroup>
            <tbody>
                <tr>
                    <td><input type="text" name="Search[q]" placeholder="Search keyword" class="form-control" value="{$Search.q}" /></td>
                    <td><label class="control-label">Tìm theo</label></td>
                    <td>
                        <select class="form-control" name="Search[searchby]">

                            {for $SB in $SearchBy as $SK}
                                <option value="{$SK}"{if($SK == $Search.searchby)} selected{/if}>{$SB}</option>
                            {/for}

                        </select>
                    </td>
                    <td><label class="control-label">Status</label></td>
                    <td>

                        <select class="form-control" name="Search[status]">
                            <option value="all"{if($Search.status == 'all')} selected{/if}>All</option>
                            <option value="1"{if($Search.status == '1')} selected{/if}>Đang chờ xác nhận</option>
                            <option value="2"{if($Search.status == '2')} selected{/if}>Đang chờ thanh toán</option>
                            <option value="3"{if($Search.status == '3')} selected{/if}>Đã xác nhận</option>
                            <option value="4"{if($Search.status == '4')} selected{/if}>Đang vận chuyển</option>
                            <option value="5"{if($Search.status == '5')} selected{/if}>Thành công</option>
                        </select>

                    </td>
                    <td><label class="control-label">Show</label></td>
                    <td><input type="number" class="form-control" value="{$Search.limit}" name="Search[limit]" /></td>
                    <td><input type="submit" class="btn btn-primary" value="Search" /></td>
                </tr>
            </tbody>
        </table>
    </form>

    <table class="table table-bordered table-striped table-hover">
        <colgroup>
            <col class="col-xs" id="col-field-id">
            <col class="col-xs" id="col-field-code">
            <col class="col-xs" id="col-field-edit_time">
            <col class="col-xs" id="col-field-status">
        </colgroup>
        <thead>
        <tr>
            <td><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'invoice_log', 'action' => 'listLog', 'params' => 'sortby/invoice_log_id/order/' . $Search['re_order'])){/function}"><strong>ID</strong></a></td>
            <td><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'invoice_log', 'action' => 'listLog', 'params' => 'sortby/invoice_id/order/' . $Search['re_order'])){/function}"><strong>Mã đơn hàng</strong></a></td>
            <td><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'invoice_log', 'action' => 'listLog', 'params' => 'sortby/edit_time/order/' . $Search['re_order'])){/function}"><strong>Thời gian</strong></a></td>
            <td><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'invoice_log', 'action' => 'listLog', 'params' => 'sortby/status/order/' . $Search['re_order'])){/function}"><strong>Trạng thái</strong></a></td>
        </tr>
        </thead>
        <tbody>
        {function}
            $orderStatus = array(
            1=>'Đang chờ xác nhận',
            2=>'Đang chờ thanh toán',
            3=>'Đã xác nhận',
            4=>'Đang vận chuyển',
            5=>'Thành công'
            );
        {/function}
        {if(!empty($Nodes))}
            {for $Node in $Nodes as $NodeID}
                <tr id="node-{$Node.invoice_log_id}">
                    <td>{$Node.invoice_log_id}</td>
                    <td>{$Node.invoice_id}</td>
                    <td>{$Node.edit_time|Filter::UnixTimeToFullDate:true}</td>
                    <td>{function}echo $orderStatus[$Node['status']]{/function}</td>
                </tr>
            {/for}
        {/if}
        </tbody>
    </table>
</div>