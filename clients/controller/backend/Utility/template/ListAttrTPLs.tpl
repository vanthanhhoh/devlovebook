<div class="table-responsive ListAttrsTpls">
    <table class="table table-bordered table-striped table-hover" style="width:100%">
        <colgroup>
        <col class="col-xs-1">
        <col class="col-xs-9">
        </colgroup>
        <thead>
            <tr>
                <th><strong>Select Attributes Template</strong></th>
                <th><strong>Attributes Template name</strong></th>
                <th><Strong>Actions</Strong></th>
            </tr>
        </thead>
        <tbody>
        	{for $TPL in $TPLs as $TPLName}
        	<tr>
            	<td><input class="VNP_AttrsTPL" type="radio" name="VNP_AttrsTPL" value="{$TPLName}"></td>
                <td>{$TPLName}</td>
                <td><a class="Remove_AttrsTemplate" href="#" data-attrs-tpl="{$TPLName}">Remove</a></td>
          	</tr>
            {/for}
     	</tbody>
	</table>
</div>
<style type="text/css">
div.ListAttrsTpls {font-size: 13px !important}
.VNP_AttrsTPL {margin-top: 4px !important; font-size: 12px !important}
a.Remove_AttrsTemplate {color: #428bca !important; font-size: 12px !Important;}
</style>