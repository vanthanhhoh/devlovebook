<?php

class Utility extends  Controller {
	private $AttributesTemplatePath = CACHE_PATH;
	public function __construct() {
		$this->AttributesTemplatePath = CACHE_PATH . 'attributes_template' . DIRECTORY_SEPARATOR;
	}

	public function Main() {}

	public function SaveAttributesTemplate() {
		$TPLContent = serialize(Input::Post('attr_template'));
		$TPLName = Input::Post('tpl_name');
		$overwrite = Input::Post('overwrite') ? true : false;
		if(!empty($TPLName)) {
			$TPLPath = $this->AttributesTemplatePath . str_replace('.', '', strip_tags($TPLName)) . '.txt';
			if(!file_exists($TPLPath) || $overwrite) {
				File::Create($this->AttributesTemplatePath . $TPLName . '.txt', $TPLContent)
				? $rt = array('status' => 'success', 'content' => 'Success save attributes template')
				: $rt = array('status' => 'error', 'content' => 'Can not save attributes template');
			}
			else $rt = array('status' => 'existed');
		}
		else $rt = array('status' => 'error', 'content' => 'Attributes template name can not be empty!');
		echo json_encode($rt);
		die();
	}

	public function RemoveAttributesTemplate() {
		$TPLName = Input::Post('tpl_name');
		if(!empty($TPLName)) {
			$TPLPath = $this->AttributesTemplatePath . str_replace('.', '', strip_tags($TPLName)) . '.txt';
			if(unlink($TPLPath))
				$rt = array('status' => 'success', 'content' => 'Success remove Attribute Templates!');
			else $rt = array('status' => 'error', 'content' => 'Can not remove Attribute Templates!');
		}
		else $rt = array('status' => 'error', 'content' => 'Attributes template name can not be empty!');
		echo json_encode($rt);
		die();
	}

	public function LoadAttributesTemplates() {
		$AttrTemplates = glob($this->AttributesTemplatePath . '*.txt');
		$TAs = array();
		foreach($AttrTemplates as $TPL) $TAs[str_replace('.txt', '', (basename($TPL)))] = json_encode(unserialize(File::GetContent($TPL)));
		Theme::JsHeader('AttrTemplates', 'var VNP_AttributesTemplates = ' . json_encode($TAs) . ';', 'inline');
		$this->UseCssComponents('Tables');
		$v = $this->View('ListAttrTPLs');
		$v->Assign('TPLs', $TAs);
		$this->Render($v->Output());
	}
}

?>