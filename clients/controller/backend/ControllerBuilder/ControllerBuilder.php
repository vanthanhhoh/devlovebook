<?php

if( !defined('VNP_SYSTEM') && !defined('VNP_APPLICATION') && !defined('ADMIN_AREA') ) die('Access denied!');

Boot::Library('NodeBuilder');

define('CONTROLLER_BASE_URL', Router::Generate('Controller',array('controller'=> 'ControllerBuilder')));

class ControllerBuilder extends Controller {
	private $MergedProfilesFilePath;
	private $AdapterFunctions = array();
	private $NodeTypes = array();
	private $NodeType;
	function __construct() {
		$this->MergedProfilesFilePath = NODE_CACHE_FILE . NodeBuilder::CACHE_FILE_EXTENSION;
		if(file_exists($this->MergedProfilesFilePath))
			$this->NodeTypes = unserialize(File::GetContent($this->MergedProfilesFilePath));
	}
	private function GetNodeTypeInfo() {
		$NodeType = $this->Registry['Params'][ROUTER_EXTRA_KEY];
		if(isset($this->NodeTypes['NodeTypes'][$NodeType]))
			$this->NodeType = $this->NodeTypes['NodeTypes'][$NodeType];
		else {
			Helper::Notify('error', 'Invalid node type!');
			return false;
		}
	}
	public function Main() {
	}
	public function Build() {
		$Build = Input::Post('Build');
		Theme::JqueryUI('sortable');
		if(isset($Build['All'])) $this->BuildAll();
		elseif(isset($Build['Backend'])) $this->BuildBackend($Build['Backend']);
		elseif(isset($Build['Frontend'])) $this->BuildFrontend($Build['Frontend']);
		else {
			$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups,InputGroups,Tables');
			$v = $this->View('build');
			$this->Render($v->Output());
		}
	}
	private function BuildBackend($Backend) {
		$Actions = array_keys($Backend);
		if($Actions[0] == 'All') {
			$this->Build_Backend_InsertNode();
			$this->Build_Backend_ListNode();
		}
		else
		foreach($Actions as $a) {
			$a = 'Build_Backend_' . $a;
			$this->$a();
		}
	}
	private function BuildFrontend($Backend) {
		$Actions = array_keys($Backend);
		if($Actions[0] == 'All') {
			$this->Build_Frontend_List();
			$this->Build_Frontend_Detail();
		}
		else
		foreach($Actions as $a) {
			$a = 'Build_Frontend_' . $a;
			$this->$a();
		}
	}
	public function Build_Backend_ListNode() {
		$this->GetNodeTypeInfo();
		if(Input::Post('do_build') == 1) {
			$Settings = Input::Post('Settings');
			if(isset($Settings['fields_order'])) {
				$Settings['fields_order'] = array_flip($Settings['fields_order']);
				ksort($Settings['fields_order']);
				$this->Backend_ListNode_Template($Settings);
			}
			else {
				Helper::Notify('error', 'You have to choose at least one field to show!');
			}
		}
		Theme::UseJquery();
		Theme::JsFooter('CTL_BUILDER_JS', APPLICATION_DATA_DIR . 'static/js/ctl_builder.js');
		$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups,InputGroups,Tables');
		Helper::State('Build Backend list nodes', Router::GenerateThisRoute());
		$v = $this->view('backend_listnode');
		$v->Assign('NodeType', $this->NodeType['NodeTypeInfo']);
		$v->Assign('NodeFields', $this->NodeType['NodeFields']);
		$this->Render($v->Output());
	}
	private function Backend_ListNode_Template($Settings = array()) {
		//n($this->NodeType);
		$NodeTypeName = $this->NodeType['NodeTypeInfo']['name'];
		$NodeTypeTitle = $this->NodeType['NodeTypeInfo']['title'];
		$NodeFields = $this->NodeType['NodeFields'];
		//n($NodeFields);
		if(!file_exists(CONTROLLER_PATH . Boot::$ControllerGroup . $NodeTypeName))
			mkdir(CONTROLLER_PATH . Boot::$ControllerGroup . $NodeTypeName);
		if(!file_exists(CONTROLLER_PATH . Boot::$ControllerGroup . $NodeTypeName . DIRECTORY_SEPARATOR . 'template'))
			mkdir(CONTROLLER_PATH . Boot::$ControllerGroup . $NodeTypeName . DIRECTORY_SEPARATOR . 'template');
		$v = $this->View('list_nodes_template');
		$v->Assign('NodeFields', $this->NodeType['NodeFields']);
		$v->Assign('NodeTypeName', $NodeTypeName);
		$v->Assign('SortBy', $Settings['sort_by']);

		$TPL[] = '<?php' . PHP_EOL . 'if(!class_exists(\'' . $NodeTypeName . '\')) die(\'Stop here!\');';
		$TPL['search_prepare'] = '$Search = array(\'sortby\' 	=> \'' . $Settings['default_order'] . '\',
				\'order\'	=> \'' . $Settings['sort'] . '\',
				\'q\'		=> \'\',
				\'searchby\'=> \'' . $Settings['advanced_filters'][0] . '\',
				\'status\'	=> \'all\',
				\'limit\'	=> ' . $Settings['limit'] . ');';
		$_SortBy = array();
		foreach($Settings['sort_by'] as $__Field) {
			$_SortBy[$__Field] = $NodeFields[$__Field]['label'];
		}
		$TPL[] = '$SortBy = ' . var_export($_SortBy, true) . ';';

		$_RefFilter = $_SearchBy = $Settings['ref_filters'] = array();
		foreach($Settings['advanced_filters'] as $__Field) {
			if($NodeFields[$__Field]['type'] == 'referer') {
				$_RefFilter[$__Field] = array(	'label' => $NodeFields[$__Field]['label'],
												'nodetype' => $NodeFields[$__Field]['referer']['node_type'],
												'nodefield' => $NodeFields[$__Field]['referer']['node_field']
											);
				$Settings['ref_filters'][$__Field] = array(	'label' => $NodeFields[$__Field]['label'],
												'nodetype' => $NodeFields[$__Field]['referer']['node_type'],
												'nodefield' => $NodeFields[$__Field]['referer']['node_field']
											);
				$TPL['search_prepare'] .= "\n" . '$Search[\'' . $__Field . '\'] = 0;';
				unset($Settings['advanced_filters'][$__Field]);
			}
			else $_SearchBy[$__Field] = $NodeFields[$__Field]['label'];
		}
		$TPL[] = '$SearchBy = ' . var_export($_SearchBy, true) . ';';
		$TPL[] = '$_RefFilter = ' . var_export($_RefFilter, true) . ';';
		foreach($Settings['advanced_filters'] as $vnpf) {
			$__Field = $this->NodeType['NodeFields'][$vnpf];
			if($__Field['type'] != 'referer' || (isset($__Field['db_config']) && in_array($__Field['db_config']['type'], array('VARCHAR', 'TEXT', 'MEDIUMTEXT', 'LONGTEXT', 'CHAR'))))
				DB::CustomQuery('ALTER TABLE  `' . $NodeTypeName . '` ADD FULLTEXT (`' . $vnpf . '`);');
		}

		$v->Assign('S', $Settings);
		FILE::Create(CONTROLLER_PATH . Boot::$ControllerGroup . $NodeTypeName . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR . 'list_nodes.tpl', $v->Output());

		$TPL[] = '$FromForm = Input::Post(\'Search\', array());';

		$TPL[] = '$Search = array_merge($Search, G::$Registry[\'Params\']);';
		$TPL[] = '$Search = array_merge($Search, $FromForm);';
		$TPL[] = '$Search[\'q\'] = urldecode($Search[\'q\']);';
		$TPL[] = 'Router::BuildParamsString($Search, true);';

		$TPL[] = '$Search[\'re_order\'] = ($Search[\'order\'] == \'desc\') ? \'asc\' : \'desc\';';

		$TPL[] = 'G::$Session->Set(\'user_sort\', $Search[\'order\']);';

		$TPL[] = 'Helper::State(\'List nodes\', Router::GenerateThisRoute());';
		$TPL[] = '$this->UseCssComponents(\'Glyphicons,Buttons,Labels,InputGroups,InputGroups,Tables,ButtonGroups,Dropdowns\');';
		$TPL[] = 'Theme::JqueryPlugin(\'InputToggle\');';
		$TPL[] = 'Theme::JsFooter(\'ListNodeLib\', APPLICATION_DATA_DIR . \'static/Node/list.js\');';
		$TPL[] = 'Theme::JsFooter(\'ControllerName\', \'var NodeController = \\\'' . $NodeTypeName . '\\\';\', \'inline\');';
		$Adapters = array();
		$AdapterFunctions = array();
		$PrepareOptions = array();
		foreach($NodeFields as $_FieldName => $_Field) {
			if($_Field['type'] == 'referer') {
				$_AdtName = 'vnp_' . $_Field['referer']['node_field'] . '_' . $_Field['referer']['node_type'];
				$Adapters[$_AdtName] = $_AdtName;
                $AdapterFunctions[$_AdtName] = '$Adapters[\'' . $_AdtName . '\'] = NodeBase::getNodes_Option(\'' . $_Field['referer']['node_type'] . '\', \'' . $_Field['referer']['node_field'] . '\', \'' . $_Field['referer']['node_type'] . '_id\');' .
                "\n" .
                '$Adapters[\'' . $_AdtName . '_id_key\'] = NodeBase::getNodes_IDKey(\'' . $_Field['referer']['node_type'] . '\', \'' . $_Field['referer']['node_field'] . '\', \'' . $_Field['referer']['node_type'] . '_id\');';
                /*
				$AdapterFunctions[$_AdtName] = '$Adapters[\'' . $_AdtName . '\'] = DB::Query(\'' . $_Field['referer']['node_type'] . '\');';
				if($_Field['referer']['node_type'] == $NodeTypeName)
					$AdapterFunctions[$_AdtName] .= "\n" . '$Adapters[\'' . $_AdtName . '\'] = $Adapters[\'' . $_AdtName . '\']->Columns(array(\'' . $_Field['referer']['node_type'] . '_id\',\'' . $_Field['referer']['node_field'] . '\', \'' . $_FieldName . '\'));';
				else $AdapterFunctions[$_AdtName] .= '
					$Adapters[\'' . $_AdtName . '\'] = $Adapters[\'' . $_AdtName . '\']->Columns(array(\'' . $_Field['referer']['node_type'] . '_id\',\'' . $_Field['referer']['node_field'] . '\'));';
				$AdapterFunctions[$_AdtName] .= "\n" . '$Adapters[\'' . $_AdtName . '\'] = $Adapters[\'' . $_AdtName . '\']->Get(\'' . $_Field['referer']['node_type'] . '_id\')->Result;';
				$AdapterFunctions[$_AdtName] .= "\n" . '$Adapters[\'' . $_AdtName . '\'][0][\'' . $_Field['referer']['node_field'] . '\'] = \'none\';';
				$AdapterFunctions[$_AdtName] .= "\n" . '$Adapters[\'' . $_AdtName . '\'][0][\'' . $_Field['referer']['node_type'] . '_id\'] = 0;';
				if($_Field['referer']['node_type'] == $NodeTypeName)
					$AdapterFunctions[$_AdtName] .= "\n" . '$Adapters[\'' . $_AdtName . '\'][0][\'' . $_FieldName . '\'] = 0;';
				$AdapterFunctions[$_AdtName] .= "\n" . 'ksort($Adapters[\'' . $_AdtName . '\']);';

				if($_Field['referer']['node_type'] == $NodeTypeName)
					$AdapterFunctions[$_AdtName] .= "\n" . '$Adapters[\'' . $_AdtName . '\'] = Filter::BuildLevelList($Adapters[\'' . $_AdtName . '\'], \'' . $_Field['referer']['node_type'] . '_id\', \'' . $_FieldName . '\');';
				else
                    $AdapterFunctions[$_AdtName] .= "\n" . '$Adapters[\'' . $_AdtName . '\'] = Filter::BuildLevelList($Adapters[\'' . $_AdtName . '\'], \'' . $_Field['referer']['node_type'] . '_id\', \'\');';
                */
			}
			elseif($_Field['type'] == 'single_value' || $_Field['type'] == 'multi_value') {
				$_Options = $_Field['options'];
				$_O = array();
				foreach($_Options as $_Opt) $_O[$_Opt['value']] = $_Opt['text'];
				$PrepareOptions[] = '$Options[\'' . $_FieldName . '\'] = ' . var_export($_O, true) . ';';
			}
		}
		$TPL = array_merge($TPL, $AdapterFunctions);
		$TPL = array_merge($TPL, $PrepareOptions);
		//$TPL[] = '$Adapters = ' . var_export($Adapters, true) . ';';
		$Settings['show_fields'][] = $NodeTypeName . '_id';
		$Settings['show_fields'][] = 'status';
		$TPL[] = '$ShowFields = ' . var_export($Settings['show_fields'], true) . ';';
		$TPL[] = '$GetNodes = DB::Query(\'' . $NodeTypeName . '\')->Columns($ShowFields);';
		$TPL[] = '$GetNodes = $GetNodes->Limit($Search[\'limit\']*1);';
		$TPL[] = '$GetNodes = $GetNodes->Order($Search[\'sortby\'], $Search[\'order\']);';

		$TPL[] = '$GetNodes = $GetNodes->Where(\'' . $NodeTypeName . '_id\', \'>\', 0);';
		$TPL[] = 'if($Search[\'status\'] != \'all\')';
		$TPL[] = '$GetNodes = $GetNodes->_AND()->Where(\'status\', \'=\', $Search[\'status\']);';

		$TPL[] = 'foreach($_RefFilter as $RF => $RFD) {
		if(isset($Search[$RF]) && $Search[$RF] > 0)
		$GetNodes = $GetNodes->_AND()
								->WhereGroupOpen()
									->Where($RF, \'=\', intval($Search[$RF]))->_OR()
									->Where($RF, \'INCLUDE\', $Search[$RF])
								->WhereGroupClose();
}';

		$TPL[] = 'if(!empty($Search[\'q\']))';
		$TPL[] = '$GetNodes = $GetNodes->_AND()
								->WhereGroupOpen()
									->Where($Search[\'searchby\'], \'SEARCH\', $Search[\'q\'])->_OR()
									->Where($Search[\'searchby\'], \'LIKE\', $Search[\'q\'])
								->WhereGroupClose();';

		$TPL[] = '$GetNodes = $GetNodes->Get(\'' . $NodeTypeName . '_id\', Output::Paging());';

		$TPL[] = '$v = $this->View(\'list_nodes\');';
		$TPL[] = '$v->Assign(\'Search\', $Search);';
		$TPL[] = '$v->Assign(\'SortBy\', $SortBy);';
		$TPL[] = '$v->Assign(\'SA\', Router::GenerateThisRoute());';
		$TPL[] = '$v->Assign(\'SearchBy\', $SearchBy);';
		if(!empty($AdapterFunctions)) $TPL[] = '$v->Assign(\'Adapters\', $Adapters);';
		$TPL[] = 'if($GetNodes->num_rows > 0) {';
		$TPL[] = "\t" . '$v->Assign(\'Nodes\', $GetNodes->Result);';
		if(!empty($PrepareOptions)) $TPL[] = '$v->Assign(\'Options\', $Options);';
		$TPL[] = '} else Helper::Notify(\'info\', \'There is no nodes found!\');';
		$TPL[] = '$this->Render($v->Output());';
		$TPL[] = 'Helper::PageInfo(\'List nodes for ' . $NodeTypeTitle . ' ( Found \' . $GetNodes->num_rows . \' nodes )\');';
		$TPL[] = 'Helper::FeaturedPanel(\'Add new ' . $NodeTypeTitle . '\',BASE_DIR . \'' . $NodeTypeName . '/AddNode\', \'plus\');';
		$TPL = implode(PHP_EOL, $TPL);

		FILE::Create(CONTROLLER_PATH . Boot::$ControllerGroup . $NodeTypeName . DIRECTORY_SEPARATOR . $NodeTypeName . '_ListRows.php', $TPL);
	}
	public function Build_Backend_InsertNode() {
		$DetectSubmit = Input::Post('build_form');
		if(empty($DetectSubmit) || $DetectSubmit != 1) {
			$this->GetNodeTypeInfo();
			Theme::UseJquery();
			Theme::JqueryUI('sortable');
			Theme::JsFooter('CTL_BUILDER_JS', APPLICATION_DATA_DIR . 'static/js/ctl_builder.js');
			//n($this->NodeType['NodeFields']);
			$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups,InputGroups,Tables');
			Helper::State('Build Backend insert node', Router::GenerateThisRoute());
			$v = $this->view('backend_insertnode');
			$v->Assign('NodeType', $this->NodeType['NodeTypeInfo']);
			$v->Assign('NodeFields', $this->NodeType['NodeFields']);
			$this->Render($v->Output());
			return;
		}

		$NodeType = $this->Registry['Params'][ROUTER_EXTRA_KEY];

		//Boot::Library('Filter');
		$FormElements = array(	'text'			=> 'input',
								'number'		=> 'input',
								'textarea'		=> 'textarea',
								'html'			=> 'html',
								'image'			=> 'image',
								'multi_image'	=> 'multi_image',
								'attributes'	=> 'attributes',
								'tags'			=> 'tags',
                                'list'          => 'list_select',
								'meta_description' => 'textarea'
							);
		$NodeType = $this->Registry['Params'][ROUTER_EXTRA_KEY];

		$FormVariables = $PrepareOptions = array();
		$FVIndex = 0;
		$this->NodeType = $this->NodeTypes['NodeTypes'][$NodeType];

		$Form = new Form('Controller_' . $NodeType . '_InsertNode', true);
		$Form->SetArrayVar('Field');
		$Form->TemplateDir(CONTROLLER_PATH . Boot::$ControllerGroup . 'ControllerBuilder' . DIRECTORY_SEPARATOR . 'field_template' . DIRECTORY_SEPARATOR);
		$HtmlEditor = array();
		$Sortable = $TagsTextText = '';

		$FormPartFields = Input::Post('sort_data');
		(sizeof($FormPartFields) == 1) ? $FullSize = true : $FullSize = false;

		foreach($FormPartFields as $Col => $FieldsCollection) {
			if($FullSize) $C = 10; else $C = $Col;
			$FormPart = $this->FormFieldsBuilder($C, $FieldsCollection, $Form, $HtmlEditor, $Sortable, $TagsTextText, $FVIndex, $PrepareOptions, $FormVariables, $FormElements);
			$Form = $FormPart['Form'];
			$HtmlEditor = $FormPart['HtmlEditor'];
			$Sortable = $FormPart['Sortable'];
			$TagsTextText = $FormPart['TagsTextText'];
			$FVIndex = $FormPart['FVIndex'];
			$PrepareOptions = $FormPart['PrepareOptions'];
			$FormVariables = $FormPart['FormVariables'];
		}

		if(!empty($HtmlEditor)) {
			$PrepareOptions[] = 'Boot::Library(\'Editor\');';
			$PrepareOptions = array_merge($PrepareOptions, $HtmlEditor);
			$PrepareOptions[] = 'Editor::Replace();';
		}
		$PrepareOptions[] = $Sortable;
		$PrepareOptions[] = $TagsTextText;
		$this->NodeInsertTemplate($NodeType, $FormVariables, $PrepareOptions);
		$Form->Render(false, true);
		//$v = $this->View('InsertRow');
		//$v->Assign('FormElements', $Form->Render());
		//$this->Render($v->Output());
		Helper::Notify('info', 'Build Insertpage proccess successed, returning...');
		header('REFRESH: 1.2; url=' . Router::GenerateThisRoute());
	}

	private function FormFieldsBuilder($Col = 4, $Fields, $Form, $HtmlEditor, $Sortable, $TagsTextText, $FVIndex, $PrepareOptions, $FormVariables, $FormElements) {
		$NodeTypeName = $this->NodeType['NodeTypeInfo']['name'];
		$Form->AddFormElement($Form->custom('OpenFormPart')->Content('<div class="FormPart FormPartCol-' . $Col . '">'));
		foreach($Fields as $FieldName) {
			if($FieldName == 'VNP_SettingsForm') {
				$Form->AddFormElement($Form->custom('Settings')->Content('<?php echo Backend::NodeExtraSettings(); ?>'));
				continue;
			}
			$Field = $this->NodeType['NodeFields'][$FieldName];
			//$FieldName = $Field['name'];
			if(isset($FormValue[$Field['name']])) $Field['value'] = $FormValue[$Field['name']];
			$FVIndex++;
			$FormVariables[$Field['name']]['value'] = $Field['value'];
			$FieldTemplate = isset($FormElements[$Field['type']]) ? $FormElements[$Field['type']] : 'input';
			$FieldObj = $Form->$FieldTemplate($FieldName)
								->Label($Field['label'])
								->Value($Field['value'])
								->Required($Field['require'])
								->FieldClass('FieldType_' . $Field['type'] . ' Field_' . $Field['name']);
			if(in_array($Field['type'], array('number', 'text', 'file'))) {
				$FieldObj->Type($Field['type']);
			}
			if(in_array($Field['type'], array('single_value', 'multi_value'))) {
				if($Field['display'] == 'single_selectbox') $T = 'select';
				if($Field['display'] == 'multi_selectbox') $T = 'multi_select';
				if($Field['display'] == 'radio') $T = 'radio';
				if($Field['display'] == 'checkbox') $T = 'checkbox';
				if(!$Field['require']) array_unshift($Field['options'], array('text' => 'Select', 'value' => ''));
				$FieldObj = $Form->$T($Field['name'])
									->Label($Field['label'])
									->Value($Field['value'])
									->FieldClass('FieldType_' . $Field['type'] . ' Field_' . $Field['name'])
									;
				$FieldObj->Options($Field['options'])->StaticOptions(true);
			}
			if($Field['type'] == 'referer') {
				$T = $Field['display'];
				if($Field['display'] == 'single_selectbox') $T = 'select';
				if($Field['display'] == 'multi_selectbox') $T = 'multi_select';
				$FieldObj = $Form->$T($Field['name'])
									->Label($Field['label'])
									->Value($Field['value'])
									->FieldClass('FieldType_' . $Field['type'] . ' Field_' . $Field['name']);
				$RefererTable = $Field['referer']['node_type'];
				$DisplayField = $Field['referer']['node_field'];
				$Options = DB::Query($RefererTable)
								->Columns(array($RefererTable . '_id', $DisplayField))
								->Get()->Result;
				$_O = array();
				foreach($Options as $Opt)
					$_O[] = array('value' => $Opt[$RefererTable . '_id'], 'text' => $Opt[$DisplayField]);
				unset($Options);
				$AdapterFunctionName = 'vnp_' . $DisplayField . '_' . $RefererTable;
				if(!in_array($AdapterFunctionName, $this->AdapterFunctions)) {
					$this->AdapterFunctions[] = $AdapterFunctionName;
                    $PrepareOptions[] = '$' . $AdapterFunctionName . ' = NodeBase::getNodes_Option(\'' . $RefererTable . '\', \'' . $DisplayField . '\', \'' . $RefererTable . '_id\');';
                    /*
					if($Field['referer']['node_type'] == $NodeTypeName)
						$PrepareOptions[] = 'function CustomDisplay_' . $AdapterFunctionName . '($Items) {
							return Filter::BuildLevelList($Items, \'' . $NodeTypeName . '_id\', \'' . $Field['name'] . '\');
						}';
					else
						$PrepareOptions[] = 'function CustomDisplay_' . $AdapterFunctionName . '($Items) {
							return Filter::BuildLevelList($Items, \'' . $NodeTypeName . '_id\', \'\');
						}';
					if($Field['referer']['node_type'] == $NodeTypeName)
						$PrepareOptions[] = 'function PrepareOptions_' . $AdapterFunctionName . '($Row) {
						return array(	\'text\' => $Row[\'' . $DisplayField . '\'],
										\'value\' => $Row[\'' . $RefererTable . '_id\'],
										\'' . $RefererTable . '_id\' => $Row[\'' . $RefererTable . '_id\'],
										\'' . $Field['name'] . '\' => $Row[\'' . $Field['name'] . '\']);
					}';
					else $PrepareOptions[] = 'function PrepareOptions_' . $AdapterFunctionName . '($Row) {
						return array(	\'text\' => $Row[\'' . $DisplayField . '\'],
										\'value\' => $Row[\'' . $RefererTable . '_id\'],
										\'' . $RefererTable . '_id\' => $Row[\'' . $RefererTable . '_id\']);
					}';

					if($Field['referer']['node_type'] == $NodeTypeName)
						$PrepareOptions[] = '$' . $AdapterFunctionName . ' = DB::Query(\'' . $RefererTable . '\')
										->Columns(array(\'' . $RefererTable . '_id\',\'' . $DisplayField . '\', \'' . $Field['name'] . '\'))
										->Adapter(\'PrepareOptions_' . $AdapterFunctionName . '\')
										->Get()->Result;';
					else
						$PrepareOptions[] = '$' . $AdapterFunctionName . ' = DB::Query(\'' . $RefererTable . '\')
										->Columns(array(\'' . $RefererTable . '_id\',\'' . $DisplayField . '\'))
										->Adapter(\'PrepareOptions_' . $AdapterFunctionName . '\')
										->Get()->Result;';
                    */
				}
				//$PrepareOptions[] = '$' . $AdapterFunctionName .' = CustomDisplay_' . $AdapterFunctionName . '($' . $AdapterFunctionName .');';
				$PrepareOptions[] = '$Fields[\'' . $Field['name'] . '\'][\'Options\'] = $' . $AdapterFunctionName .';';
				if(!$Field['require']) {
					if($Field['referer']['node_type'] == $NodeTypeName) {
						//array_unshift($_O, array('text' => 'Select', 'value' => '', 'prefix' => '', 'suffix' => ''));
						//$PrepareOptions[] = 'array_unshift($Fields[\'' . $Field['name'] . '\'][\'Options\'], array(\'text\' => \'Select\', \'value\' => \'\', \'prefix\' => \'\', \'suffix\' => \'\'));';
					}
					else {
						//array_unshift($_O, array('text' => 'Select', 'value' => ''));
						//$PrepareOptions[] = 'array_unshift($Fields[\'' . $Field['name'] . '\'][\'Options\'], array(\'text\' => \'Select\', \'value\' => \'\', \'prefix\' => \'\', \'suffix\' => \'\'));';
					}
				}
				$FieldObj->Options($_O);
			}

            if($Field['type'] == 'list') {
                $RefererTable = $Field['referer']['node_type'];
                $DisplayField = $Field['referer']['node_field'];
                $FieldObj->AddVar(array('ref_table' => $RefererTable, 'ref_field' => $DisplayField));
            }

			if($Field['type'] == 'html')
				$HtmlEditor[] = 'Editor::AddEditor(\'#ID_Field_' . $Field['name'] . '\');';
			if($Field['type'] == 'attributes')
				$Sortable = 'Theme::JqueryUI(\'sortable\');';
			if($Field['type'] == 'tags') {
				$TagsTextText = 'Theme::JqueryUI(\'autocomplete\');' . PHP_EOL;
				$TagsTextText .= "\t\t" . 'Theme::JqueryPlugin(\'TagsInput\');' . PHP_EOL;
				$TagsTextText .= "\t\t" . 'Theme::CssHeader(\'jquery-ui-autocomplete\', GLOBAL_DATA_DIR . \'library/jquery-ui/jquery-ui-1.10.4.autocomplete.css\');' . PHP_EOL;
				$TagsTextText .= "\t\t" . 'Theme::CssHeader(\'jquery-plugin-TagsInput\', GLOBAL_DATA_DIR . \'library/jquery-plugins/TagsInput/jquery.tagsinput.css\');' . PHP_EOL;
				$TagsTextText .= "\t\t" . '$TagsIniStr = \'$(\\\'.VNP_TagsField\\\').tagsInput({
		  					autocomplete_url: function(request, response) {
	  							console.log(request.term);
						        VNP.Ajax({
						          type: \\\'post\\\',
						          url: \\\'Node/GetTags\\\',
						          dataType: "jsonp",
						          data: {
						            TagQ: request.term
						          },
						          success: function( data ) {
						            response( data );
						          },
						          error: function( data ) {
						          	console.log(data);
						          },
						          complete: function() {
						          	VNP.Loader.hide();
						          }
						        }, \\\'json\\\');
							}
						});\';' . PHP_EOL;

				$TagsTextText .= "\t\t" . 'Theme::JsFooter(\'IniTagsObject-\', $TagsIniStr, \'inline\');';
			}
			$Form->AddFormElement($FieldObj);
		}
		$Form->AddFormElement($Form->custom('CloseFormPart')->Content('</div>'));

		$FormPart['Form'] = $Form;
		$FormPart['HtmlEditor'] = $HtmlEditor;
		$FormPart['Sortable'] = $Sortable;
		$FormPart['TagsTextText'] = $TagsTextText;
		$FormPart['FVIndex'] = $FVIndex;
		$FormPart['PrepareOptions'] = $PrepareOptions;
		$FormPart['FormVariables'] = $FormVariables;
		return $FormPart;
	}

	private function NodeInsertTemplate($NodeType, $VarArray = array(), $PrepareOptions = array()) {
		$NodeFields = $this->NodeType['NodeFields'];
		$PrepareFields = array();
		$CheckUnique = array();
		$TagsField = array();

		foreach($this->NodeType['NodeFields'] as $Field) {
			if(preg_match('/\[\@([a-zA-Z0-9_\-]+)\]/', $Field['value'], $MF)) {
				$PrepareFields[] = '
			if(isset($FormValue[\'' . $MF[1] . '\']) && (!isset($FormValue[\'' . $Field['name'] . '\']) || $FormValue[\'' . $Field['name'] . '\'] == \'\'))
				$FormValue[\'' . $Field['name'] . '\'] = $FormValue[\'' . $MF[1] . '\'];';
			}

			if($Field['type'] == 'tags') $TagsField[] = $Field['name'];
			if($Field['type'] == 'multi_value' || ($Field['type'] == 'referer' && in_array($Field['display'],array('multi_selectbox', 'checkbox'))))
				$PrepareFields[] = '
			if(isset($FormValue[\'' . $Field['name'] . '\']))
				$FormValue[\'' . $Field['name'] . '\'] = implode(\',\', $FormValue[\'' . $Field['name'] . '\']);';

			if($Field['type'] == 'attributes')
				$PrepareFields[] = '
			$FormValue[\'' . $Field['name'] . '\'] = serialize($FormValue[\'' . $Field['name'] . '\']);';

			if(in_array($Field['type'],array('multi_image', 'list')))
				$PrepareFields[] = '
			$FormValue[\'' . $Field['name'] . '\'] = array_filter($FormValue[\'' . $Field['name'] . '\']);
			$FormValue[\'' . $Field['name'] . '\'] = implode(\',\', $FormValue[\'' . $Field['name'] . '\']);';

			if($Field['require']) {
				$PrepareFields[] = '
			if(!isset($FormValue[\'' . $Field['name'] . '\']) || $FormValue[\'' . $Field['name'] . '\'] == \'\')
				Helper::Notify(\'error\', \'' . $Field['label'] . ' không thể để trống\');';
			}
			if($Field['filter']) {
				$PrepareFields[] = '
			$FormValue[\'' . $Field['name'] . '\'] = ' . Filter::FunctionBuilder($Field['filter'], '$FormValue[\'' . $Field['name'] . '\']') . ';';
				//$CheckUnique[$Field['name']] = $FormValue[$Field['name']];
			}
			if($Field['db_config']['is_unique'])
				$CheckUnique[] = $Field['name'];
		}

		$TagsMerged = $TagsMergedOld = array();
		if(!empty($TagsField)) {
			$_T1 = $_T2 = array();
			foreach($TagsField as $__TF) {
				$_T1[] = '$FormValue[\'' . $__TF . '\']';
				$_T2[] = '$OldFormValue[\'' . $__TF . '\']';
			}
			sizeof($_T1) > 1 ? $TagsMerged = 'array_merge(\',\',' . implode($_T1) . ')' : $TagsMerged = $_T1[0];
			sizeof($_T1) > 1 ? $TagsMergedOld = 'array_merge(\',\',' . implode($_T2) . ')' : $TagsMergedOld = $_T2[0];
		}

		$NodeTypeName = $this->NodeType['NodeTypeInfo']['name'];

		$PrepareFields[] = '
		' . "\t" . '$FormValue[\'schedule\'] = Filter::DateToUnixTime($FormValue[\'schedule\'][\'date\'], $FormValue[\'schedule\'][\'hour\'], $FormValue[\'schedule\'][\'minute\']);
		' . "\t" . 'if($FormValue[\'schedule\'] < CURRENT_TIME) $FormValue[\'schedule\'] = CURRENT_TIME;
		' . "\t" . '$FormValue[\'exprired\'] = Filter::DateToUnixTime($FormValue[\'exprired\'][\'date\'], $FormValue[\'exprired\'][\'hour\'], $FormValue[\'exprired\'][\'minute\']);
		' . "\t" . 'if($FormValue[\'exprired\'] < CURRENT_TIME) $FormValue[\'exprired\'] = 0;
		' . "\t" . '$FormValue[\'node_settings\'] = Backend::BuildNodePermission(Input::Post(\'VNP_Settings\'));';

		$PrepareFields[] = '
			if(Helper::NotifyCount(\'error\') == 0) {
				$NodeID = Input::Post(\'NodeID\');
				if(empty($NodeID)) {
					$CheckUnique = array();
					$NodeExisted = false;';
					if(!empty($CheckUnique)) {
					$PrepareFields[] = '
						$CheckExisted = DB::Query(\'' . $NodeTypeName . '\')->WhereGroupOpen();';
					$i = 0;
					foreach($CheckUnique as $FField) {
						if($i > 0) $PrepareFields[] = '
						$CheckExisted = $CheckExisted->_OR();';
						$PrepareFields[] = '
						$CheckExisted = $CheckExisted->Where(\'' . $FField . '\', \'=\', $FormValue[\'' . $FField . '\']);';
						$i++;
					}
					$PrepareFields[] = '
						$CheckExisted = $CheckExisted->WhereGroupClose();
						$CheckExisted = $CheckExisted->Get();
						if($CheckExisted->num_rows > 0) $NodeExisted = true;';
				}
				$PrepareFields[] = '
						if(!$NodeExisted) {
							$FormValue[\'lang\'] = LANG;
							$FormValue[\'user_id\'] = USER_ID;
							$FormValue[\'last_edit_id\'] = USER_ID;
							$FormValue[\'add_time\'] = CURRENT_TIME;
							$FormValue[\'edit_time\'] = CURRENT_TIME;
							$NodeQuery = DB::Query(\'' . $this->NodeType['NodeTypeInfo']['name'] . '\')->Insert($FormValue);
							if($NodeQuery->status && $NodeQuery->insert_id > 0) {';
							if(Backend::$NodeProfile[$this->NodeType['NodeTypeInfo']['name']]['split'] > 0) {
								$PrepareFields[] = '
								$TableSuf = floor($NodeQuery->insert_id/' . Backend::$NodeProfile[$this->NodeType['NodeTypeInfo']['name']]['split'] . ') + 1;
								$TableName = \'' . $this->NodeType['NodeTypeInfo']['name'] . '_\' . $TableSuf;
								$CheckTable = DB::CustomQuery(\'SHOW TABLES LIKE \\\'\' . $TableName . \'\\\'\');
								if($CheckTable->num_rows == 0)
									$CreateTable = DB::CustomQuery(\'CREATE TABLE \' . $TableName . \' LIKE ' . $this->NodeType['NodeTypeInfo']['name'] . '\');

								$FormValue[\'' . $this->NodeType['NodeTypeInfo']['name'] . '_id\'] = $NodeQuery->insert_id;
								$NodeQuery2 = DB::Query($TableName)->Insert($FormValue);
								if($NodeQuery2->status) {';
								if(!empty($TagsMerged))
									$PrepareFields[] = str_repeat("\t", 7) . 'Backend::UpdateTags($NodeID, \'' . Backend::$NodeProfile[$this->NodeType['NodeTypeInfo']['name']]['code'] . '\', ' . $TagsMerged . ');';
								$PrepareFields[] = str_repeat("\t", 7) . 'Helper::Notify(\'success\', \'Successful add node in ' . $this->NodeType['NodeTypeInfo']['title'] . '\');
								}
								else {
									DB::Query(\'' . $this->NodeType['NodeTypeInfo']['name'] . '\')->Where(\'' . $this->NodeType['NodeTypeInfo']['name'] . '_\', \'=\',$NodeQuery->insert_id )->Delete();
									Helper::Notify(\'error\', \'Cannot add node in ' . $this->NodeType['NodeTypeInfo']['title'] . ', check if subtable existed!\');
								}';
							}
							else {
								if(!empty($TagsMerged))
									$PrepareFields[] = str_repeat("\t", 8) . 'Backend::UpdateTags($NodeID, \'' . Backend::$NodeProfile[$this->NodeType['NodeTypeInfo']['name']]['code'] . '\', ' . $TagsMerged . ');';
							}
							$PrepareFields[] = str_repeat("\t", 8) .'Helper::Notify(\'success\', \'Successful add node in ' . $this->NodeType['NodeTypeInfo']['title'] . '\');';
							$PrepareFields[] = str_repeat("\t", 7) . '}
							else Helper::Notify(\'error\', \'Cannot add node in ' . $this->NodeType['NodeTypeInfo']['title'] . '\');
						}
						else Helper::Notify(\'error\', \'Cannot add node in ' . $this->NodeType['NodeTypeInfo']['title'] . '. Be sure that <em>' . implode(', ', $CheckUnique) . '</em> didn\\\'t existed!\');
				}
				else {
					//$CheckExisted = DB::Query(\'' . $NodeTypeName . '\')->Where(\'' . $NodeTypeName . '_id\', \'=\', $NodeID)->Get();
					//if($CheckExisted->num_rows == 1) {
					if($RealNodeID == $NodeID) {
						$FormValue[\'last_edit_id\'] = USER_ID;
						$FormValue[\'edit_time\'] = CURRENT_TIME;
						$FormValue[\'editing_id\'] = 0;
						$NodeQuery = DB::Query(\'' . $NodeTypeName . '\')->Where(\'' . $NodeTypeName . '_id\', \'=\', $NodeID)->Update($FormValue);
						if($NodeQuery->status && $NodeQuery->affected_rows > 0) {';
						if(Backend::$NodeProfile[$this->NodeType['NodeTypeInfo']['name']]['split'] > 0) {
							$PrepareFields[] = '
							$TableSuf = floor($NodeID/' . Backend::$NodeProfile[$this->NodeType['NodeTypeInfo']['name']]['split'] . ') + 1;
							$TableName = \'' . $this->NodeType['NodeTypeInfo']['name'] . '_\' . $TableSuf;

							$NodeQuery2 = DB::Query($TableName)->Where(\'' . $this->NodeType['NodeTypeInfo']['name'] . '_id\', \'=\', $NodeID)->Update($FormValue);
							if($NodeQuery2->status && $NodeQuery2->affected_rows > 0) {';
							if(!empty($TagsMerged))
								$PrepareFields[] = str_repeat("\t", 7) . 'Backend::UpdateTags($NodeID, \'' . Backend::$NodeProfile[$this->NodeType['NodeTypeInfo']['name']]['code'] . '\', ' . $TagsMerged . ', ' . $TagsMergedOld . ');';
							$PrepareFields[] = str_repeat("\t", 7) . 'Helper::Notify(\'success\', \'Successful update node in ' . $this->NodeType['NodeTypeInfo']['title'] . '\');
							}
							else {
								$OldData = DB::Query($TableName)->Where(\'' . $this->NodeType['NodeTypeInfo']['name'] . '\', \'=\', $NodeID)->Get()->Result[0];
								DB::Query(\'' . $this->NodeType['NodeTypeInfo']['name'] . '\')->Where(\'' . $this->NodeType['NodeTypeInfo']['name'] . '_id\', \'=\',$NodeID)->Update($OldData);
								Helper::Notify(\'error\', \'Cannot add node in ' . $this->NodeType['NodeTypeInfo']['title'] . ', check if subtable existed!\');
							}';
						}
						else {
							if(!empty($TagsMerged))
								$PrepareFields[] = str_repeat("\t", 7) . 'Backend::UpdateTags($NodeID, \'' . Backend::$NodeProfile[$this->NodeType['NodeTypeInfo']['name']]['code'] . '\', ' . $TagsMerged . ', ' . $TagsMergedOld . ');';
							$PrepareFields[] = str_repeat("\t", 7) . 'Helper::Notify(\'success\', \'Successful update node in ' . $this->NodeType['NodeTypeInfo']['title'] . '\');';
						}
						$PrepareFields[] = str_repeat("\t", 6) . '}
						else Helper::Notify(\'error\', \'Cannot update node in ' . $this->NodeType['NodeTypeInfo']['title'] . '\');
					}
					else {
						Helper::Notify(\'error\', \'Cannot update, Node not found!\');
					}
				}
			}';
		$PrepareFields[] = '
			return $FormValue;';

		$TPL = '<?php

class ' . $NodeType . ' extends Controller {
	public $NodeTypeName;
	public function __construct() {
		$this->NodeTypeName = \'' . $NodeType . '\';
	}
	public function Main() {
		require(CONTROLLER_PATH . Boot::$ControllerGroup . \'' . $NodeType . '\' . DIRECTORY_SEPARATOR . \'' . $NodeType . '_ListRows.php\');
	}
	public function AddNode() {
		Theme::UseJquery();
		Theme::JsFooter(\'AddNodeLib\', APPLICATION_DATA_DIR . \'static/Node/add.js\');
		$NodeID = 0;
		$FormValue = array();
		if(isset(G::$Registry[\'Params\'][ROUTER_EXTRA_KEY])) {
			$NodeID = G::$Registry[\'Params\'][ROUTER_EXTRA_KEY];
			$GetNode = DB::Query(\'' . $NodeType . '\')->Where(\'' . $NodeType . '_id\', \'=\', $NodeID)->Get();
			if($GetNode->num_rows == 1) {
				$FormValue = $GetNode->Result[0];
				DB::Query(\'' . $NodeType . '\')->Where(\'' . $NodeType . '_id\', \'=\', $NodeID)->Update(array(\'editing_id\' => USER_ID));
			}
			else $NodeID = 0;
		}
		$FV = $this->SaveNodeAction($NodeID, $FormValue);
		if(!empty($FV)) $FormValue = array_merge($FormValue, $FV);
		$this->UseCssComponents(\'Glyphicons,Buttons,Labels,InputGroups\');';
		$VA = array();
		foreach($VarArray as $Key => $Var) {
			//$VA[] = '\'' . $Key . '\' => array(\'value\' => isset($FormValue[\'' . $Key . '\']) ? $FormValue[\'' . $Key . '\'] : \'' . $Var['value'] . '\')';
			$VA[] = '\'' . $Key . '\' => array(\'value\' => isset($FormValue[\'' . $Key . '\']) ? $FormValue[\'' . $Key . '\'] : \'\')';
		}
		$TPL .= '
		$Fields = array(' . implode(',', $VA) . ');
		' . implode(PHP_EOL . "\t\t", $PrepareOptions) . '
		Backend::$NodeSettings[\'status\'] = isset($FormValue[\'status\']) ? $FormValue[\'status\'] : 1;
		Backend::$NodeSettings[\'priority\'] = isset($FormValue[\'priority\']) ? $FormValue[\'priority\'] : 0;
		Backend::$NodeSettings[\'schedule\'] = Filter::UnixTimeToDate(isset($FormValue[\'schedule\']) ? $FormValue[\'schedule\'] : 0);
		Backend::$NodeSettings[\'exprired\'] = Filter::UnixTimeToDate(isset($FormValue[\'exprired\']) ? $FormValue[\'exprired\'] : 0);
		if(isset($FormValue[\'node_settings\'])) Backend::$NodeSettings[\'extra\'] = $FormValue[\'node_settings\'];
		ob_start();
		echo \'<form class="form-horizontal" action="" method="post">\';
		echo \'<input type="hidden" name="SaveNodeSubmit" value="1"/>\';
		echo \'<input type="hidden" name="NodeID" value="\' . $NodeID . \'"/>\';
		include Form::$CompiledPath . \'Controller_' . $NodeType . '_InsertNode.php\';
		echo \'<div class="clearfix"></div><div class="clearfix" style="text-align:center;margin:10px 0 15px 0"><input type="submit" class="btn btn-primary" value="Save"/></div>\';
		echo \'</form>\';
		$Form = ob_get_clean();
		$this->Render($Form);
	}

	public function Action() {
		$action = Input::Post(\'action\');
		$ids = Input::Post(\'ids\');
		$ids = array_filter(explode(\',\', $ids));
		$ids = array_map(\'intval\', $ids);

		$rt = array(\'status\' => \'not\');
		if($action == \'deactive\') {
			$Deactive = DB::Query(\'' . $NodeType . '\')->Where(\'' . $NodeType . '_id\', \'IN\', $ids)->Update(array(\'status\' => 0));
			if($Deactive->status) $rt = array(\'status\' => \'ok\', \'items\' => $ids);
		}
		if($action == \'active\') {
			$Deactive = DB::Query(\'' . $NodeType . '\')->Where(\'' . $NodeType . '_id\', \'IN\', $ids)->Update(array(\'status\' => 1));
			if($Deactive->status) $rt = array(\'status\' => \'ok\', \'items\' => $ids);
		}
		if($action == \'delete\') {
			$Deactive = DB::Query(\'' . $NodeType . '\')->Where(\'' . $NodeType . '_id\', \'IN\', $ids)->Delete();
			if($Deactive->status) $rt = array(\'status\' => \'ok\', \'items\' => $ids);
		}
		echo json_encode($rt);
		die();
	}

	public function RemoveNode() {
		$NodeID = 0;
		if(isset(G::$Registry[\'Params\'][ROUTER_EXTRA_KEY])) {
			$NodeID = G::$Registry[\'Params\'][ROUTER_EXTRA_KEY];
			$GetNode = DB::Query(\'' . $NodeType . '\')->Where(\'' . $NodeType . '_id\', \'=\', $NodeID)->Get();
			if($GetNode->num_rows == 0)
				return Helper::Notify(\'error\', \'Node not found\');
		}
		if(Input::Post(\'RemoveNodeSubmit\') == 1) {
			$DeleteNode = DB::Query(\'' . $NodeType . '\')->Where(\'' . $NodeType . '_id\', \'=\', $NodeID)->Delete();
			if($DeleteNode->affected_rows == 1) {
				Helper::Notify(\'success\', \'Success delete node \' .  $GetNode->Result[0][\'title\']);
				Header(\'Refresh: 1.5; url=\' . Router::Generate(\'Controller\', array(\'controller\' => \'' . $NodeType . '\')));
			}
			else Helper::Notify(\'error\', \'Error delete node \' .  $GetNode->Result[0][\'title\']);
		}
		else {
			$config = array(\'action\'	=> Router::GenerateThisRoute(),
							\'tokens\'	=> array(array(\'name\' => \'NodeID\', \'value\' => $NodeID), array(\'name\' => \'RemoveNodeSubmit\', \'value\' => 1))
							);
			$v = Access::Confirm(\'Confirm remove node: \' . $GetNode->Result[0][\'title\'], $config);
			$this->Render($v);
		}
	}

	public function SaveNodeAction($RealNodeID, $OldFormValue = array()) {
		if(Input::Post(\'SaveNodeSubmit\') == 1) {
			$FormValue = Input::Post(\'Field\');
			' .
			implode(PHP_EOL, $PrepareFields). '
		}
	}
}';
		if(!file_exists(CONTROLLER_PATH . Boot::$ControllerGroup . $NodeType))
			mkdir(CONTROLLER_PATH . Boot::$ControllerGroup . $NodeType);
		FILE::Create(CONTROLLER_PATH . Boot::$ControllerGroup . $NodeType . DIRECTORY_SEPARATOR . $NodeType . '.php', $TPL);
	}
}

?>