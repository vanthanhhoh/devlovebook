<div class="table-responsive">
	<form class="form-horizontal" action="{skip}{$SA}{/skip}" method="post">
		<input type="hidden" name="SearchUsers" value="1">
		<table class="table table-bordered table-striped table-hover">
	        <colgroup>
	        	<col class="col-xs-2">
	            <col class="col-xs-1">
	            <col class="col-xs-2">
	            <col class="col-xs-1">
	            <col class="col-xs-2">
	            <col class="col-xs">
	            <col class="col-xs-1">
	            <col class="col-xs">
	            <col class="col-xs-1">
	            <col class="col-xs-1">
	        </colgroup>
	        <tbody>
	        	{if(!empty($S.ref_filters))}
	        	<tr>
	        		<td><strong>{{lang::search_in}}</strong></td>
	        		{for $RefFilter in $S.ref_filters as $RefField}
	        		<td><strong>{$RefFilter.label}</strong></td>
	        		<td>
	        			<select class="form-control" name="Search[{$RefField}]">
	        			{skip}
	        			{function}
	        			foreach($Adapters['vnp_{/skip}{$RefFilter.nodefield}_{$RefFilter.nodetype}{skip}'] as $Option) {{/function}
	        			<option value="{$Option.{/skip}{$RefFilter.nodetype}{skip}_id}"{function}echo ($Option['{/skip}{$RefFilter.nodetype}{skip}_id'] == $Search['{/skip}{$RefField}{skip}']) ? ' selected' : ''{/function}
	        			>{$Option.prefix}{$Option.{/skip}{$RefFilter.nodefield}{skip}}</option>
	        			{function}}{/function}
	        			{/skip}
	        			</select>
	        		</td>
	        		{/for}
	        	</tr>
	        	{/if}
	        	<tr>
	        		<td><input type="text" name="Search[q]" placeholder="Search keyword" class="form-control" value="{skip}{$Search.q}{/skip}" /></td>
	        		<td><label class="control-label">{{lang::search_by}}</label></td>
	        		<td>
	        			<select class="form-control" name="Search[searchby]">
	        				{skip}
	        				{for $SB in $SearchBy as $SK}
	        				<option value="{$SK}"{if($SK == $Search.searchby)} selected{/if}>{$SB}</option>
	        				{/for}
	        				{/skip}
	        			</select>
	        		</td>
	        		<td><label class="control-label">{{lang::sort_by}}</label></td>
	        		<td>
	        			{skip}
	        			<select class="form-control" name="Search[sortby]">
	        				{for $SB in $SortBy as $SK}
		        			<option value="{$SK}"{if($Search.sortby == $SK)} selected{/if}>{$SB}</option>
	        				{/for}
	        			</select>
	        			{/skip}
	        		</td>
	        		<td><label class="control-label">Status</label></td>
	        		<td>
	        			{skip}
	        			<select class="form-control" name="Search[status]">
		        			<option value="all"{if($Search.status == 'all')} selected{/if}>All</option>
	        				<option value="1"{if($Search.status == '1')} selected{/if}>Active</option>
	        				<option value="0"{if($Search.status == '0')} selected{/if}>Inactive</option>
	        			</select>
	        			{/skip}
	        		</td>
	        		<td><label class="control-label">Show</label></td>
	        		<td><input type="number" class="form-control" value="{skip}{$Search.limit}{/skip}" name="Search[limit]" /></td>
	        		<td><input type="submit" class="btn btn-primary" value="Search" /></td>
				</tr>
			</tbody>
		</table>
	</form>


	<div class="btn-group" style="margin-bottom: 5px">
		<div class="btn-group">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				{{lang::action_btn}}
				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu" role="menu">
				<li><a href="#" id="VNP_DeactiveNode"><span class="glyphicon glyphicon-remove"></span>&nbsp&nbsp{{lang::deactive}}</a></li>
				<li><a href="#" id="VNP_ActiveNode"><span class="glyphicon glyphicon-ok"></span>&nbsp&nbsp{{lang::active}}</a></li>
				<li><a href="#" id="VNP_DeleteNode"><span class="glyphicon glyphicon-trash"></span>&nbsp&nbsp{{lang::delete}}</a></li>
			</ul>
		</div>
	</div>
    <table class="table table-bordered table-striped table-hover">
        <colgroup>
        	<col class="col-xs" id="col-field-toggle">{for $Field in $S.fields_order}
        	<col class="col-xs" id="col-field-{$Field}">
        {/for}<col class="col-xs" id="col-functions">
        </colgroup>
        <thead>
	        <tr>
	        	<td>
	        		<div class="checkbox checkbox-success">
                        <input type="checkbox" id="VNP_ToggleAll" id="VNP_ToggleAll" value="1" />
                        <label for="VNP_ToggleAll"></label>
                    </div>
	        	</td>
	        	<td><a href="{skip}{function}echo Router::Generate('ControllerParams', array('controller' => '{/skip}{$NodeTypeName}{skip}', 'action' => 'Main', 'params' => 'sortby/{/skip}{$NodeTypeName}{skip}_id/order/' . $Search['re_order'])){/function}{/skip}"><strong>ID</strong></a></td>
	        	{for $Field in $S.fields_order}
	        	{$Field = $NodeFields[$Field]}
	        	{if(in_array($Field.name, $SortBy))}
			    	<td><a href="{skip}{function}echo Router::Generate('ControllerParams', array('controller' => '{/skip}{$NodeTypeName}{skip}', 'action' => 'Main', 'params' => 'sortby/{/skip}{$Field.name}{skip}/order/' . $Search['re_order'])){/function}{/skip}"><strong>{$Field.label}</strong></a></td>
		    	{else}
			    	<td><strong>{$Field.label}</strong></td>
	    		{/if}
	        	{/for}
	        	<td><strong>Functions</strong></td>
	        </tr>
	    </thead>
	    <tbody>{skip}
	    	{if(!empty($Nodes))}
	    	{for $Node in $Nodes as $NodeID}
	    	<tr id="node-{$Node.{/skip}{$NodeTypeName}_id{skip}}" class="{if($Node.status)}Node_Active{else}Node_Inactive{/if}">
	    		<td>
	    			<div class="checkbox checkbox-success">
                        <input type="checkbox" name="VNP_ToggleItem[]" class="VNP_ToggleItem" id="ToggleItem-{$Node.{/skip}{$NodeTypeName}_id{skip}}" data-title="{$Node.{/skip}{$NodeTypeName}_id{skip}}" value="{$Node.{/skip}{$NodeTypeName}_id{skip}}" />
                        <label for="ToggleItem-{$Node.{/skip}{$NodeTypeName}_id{skip}}"></label>
                    </div>
	    		</td>
	    		<td>{$Node.{/skip}{$NodeTypeName}_id{skip}}</td>{/skip}
	    		{for $Field in $S.fields_order}
		    		{$FieldName = $Field; $Field = $NodeFields[$Field]}
		    		{if($Field.type == 'referer')}
		    			{$RefererField = $Field['referer']['node_field']}
		    			{skip}
		    			{$FieldIdt = $Node['{/skip}{$FieldName}{skip}'];}
                        {function}if($FieldIdt == '') $FieldIdt = 0{/function}
		    			{/skip}
		    			{if($Field.display == 'single_selectbox')}
			    			{skip}
			    			{$Adapter = $Adapters['{/skip}vnp_{$Field.referer.node_field}_{$Field.referer.node_type}_id_key'][{skip}$FieldIdt{/skip}{skip}]}
				    		<td>{$Adapter.{/skip}{$RefererField}{skip}}</td>
				    		{/skip}
				    	{else}
				    		{skip}
				    		{$FieldIdts = array_map('trim', explode(',',$FieldIdt)); $Adapter = array()}
				    		{for $FieldIdt in $FieldIdts}
				    		{function}if(isset($Adapters['{/skip}vnp_{$Field.referer.node_field}_{$Field.referer.node_type}'][{skip}$FieldIdt{/skip}{skip}]['{/skip}{$RefererField}{skip}']))
				    		array_push($Adapter, $Adapters['{/skip}vnp_{$Field.referer.node_field}_{$Field.referer.node_type}'][{skip}$FieldIdt{/skip}{skip}]['{/skip}{$RefererField}{skip}'])
				    		{/function}
				    		{/for}
				    		<td>{function}echo implode(',', $Adapter){/function}</td>
				    		{/skip}
				    	{/if}
                    {elseif($Field.type == 'list')}
                        <td>
                        {skip}
                            {function}$refNodes = NodeBase::getNodesIn('{/skip}{$Field.referer.node_type}{skip}', '{/skip}{$Field.referer.node_field}{skip}', $Node['{/skip}{$FieldName}{skip}']){/function}
                            {for $rn in $refNodes}
                                {$rn.{/skip}{$Field.referer.node_field}{skip}},
                            {/for}
                        {/skip}
                        </td>
				    {elseif($Field.type == 'single_value')}
					    {skip}
					    {$FieldIdt = $Node['{/skip}{$FieldName}{skip}']; $Opt = $Options['{/skip}{$FieldName}{skip}']}
					    <td>{$Opt[$FieldIdt]}</td>
					    {/skip}
					{elseif($Field.type == 'image')}
					    {skip}
					    <td><img src="{#THUMB_BASE}50_50{$Node.{/skip}{$FieldName}{skip}}"></td>
					    {/skip}
				    {else}{skip}
			    	<td>{$Node.{/skip}{$FieldName}{skip}}</td>
			    	{/skip}{/if}
	    		{/for}{skip}
	    		<td>
                	<a href="{function}echo Router::EditNode({/skip}'{$NodeTypeName}', {skip}$NodeID){/function}"><span class="glyphicon glyphicon-edit"></span>&nbsp{{lang::edit}}</a>&nbsp;&nbsp;
                    <a href="{function}echo Router::RemoveNode({/skip}'{$NodeTypeName}', {skip}$NodeID){/function}"><span class="glyphicon glyphicon-trash"></span>&nbsp{{lang::delete}}</a>
              	</td>
	    	</tr>
	    	{/for}
	    	{/if}
	    {/skip}</tbody>
    </table>
</div>