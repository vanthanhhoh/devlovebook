<form action="" id="Build_AddNodeForm" method="post">
	<input type="hidden" name="Build[Backend][InsertNode]" value="1" />
	<input type="hidden" name="build_form" value="1" />
	<input type="submit" class="btn btn-primary" value="Submit" />
	{for $Field in $NodeFields}
	<input class="SortedFields" type="hidden" name="sort_data[6][]" value="{$Field.name}" />
	{/for}
	<input class="SortedFields" type="hidden" name="sort_data[4][]" value="VNP_SettingsForm" />
</form>
<div class="FeaturedPanel clearfix">
	<span class="VNP_PageInfo">Build insert node page</span>
		<ul class="FeaturedButtons">
			<li>
            </li>
       	</ul>
</div>
<div id="Build_InsertNodeForm" class="clearfix">
	<ul class="VNP_FormPart" data-cols="6">
		{for $Field in $NodeFields}
		<li class="FieldItem Move_Option" data-field-name="{$Field.name}">{$Field.label}</li>
		{/for}
	</ul>
	<ul class="VNP_FormPart" data-cols="4">
		<li class="FieldItem Move_Option" data-field-name="VNP_SettingsForm">Settings</li>
	</ul>
	<div class="clearfix"></div>
	<ul class="VNP_FormPart" data-cols="10" style="width:100%; margin: 15px 0"></ul>
</div>