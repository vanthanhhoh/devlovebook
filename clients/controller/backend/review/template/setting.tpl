<form action="" class="" id="" method="post">
    <input type="hidden" name="saveOptions" value="1" />
    {for $option in $options as $opName}
        <div class="form-group FieldWrap FieldType_text Field_phone">
            <label class="col-sm-3 control-label" for="ID_Field[{$opName}]">{$option.title}</label>
            <div class="col-sm-9">
                    <textarea type="text" name="Field[{$opName}]" class="transfer" class="form-control" >
                        {$option.value}
                    </textarea>
            </div>
        </div>
    {/for}
    <input type="submit" value="Save" class="btn btn-primary">
</form>