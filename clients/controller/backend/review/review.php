<?php

class review extends Controller {
	public $NodeTypeName;
	public function __construct() {
		$this->NodeTypeName = 'review';
	}
	public function Main() {
		require(CONTROLLER_PATH . Boot::$ControllerGroup . 'review' . DIRECTORY_SEPARATOR . 'review_ListRows.php');
	}
	public function AddNode() {
		Theme::UseJquery();
		Theme::JsFooter('AddNodeLib', APPLICATION_DATA_DIR . 'static/Node/add.js');
		$NodeID = 0;
		$FormValue = array();
		if(isset(G::$Registry['Params'][ROUTER_EXTRA_KEY])) {
			$NodeID = G::$Registry['Params'][ROUTER_EXTRA_KEY];
			$GetNode = DB::Query('review')->Where('review_id', '=', $NodeID)->Get();
			if($GetNode->num_rows == 1) {
				$FormValue = $GetNode->Result[0];
				DB::Query('review')->Where('review_id', '=', $NodeID)->Update(array('editing_id' => USER_ID));
			}
			else $NodeID = 0;
		}
		$FV = $this->SaveNodeAction($NodeID, $FormValue);
		if(!empty($FV)) $FormValue = array_merge($FormValue, $FV);
		$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups');
		$Fields = array('title' => array('value' => isset($FormValue['title']) ? $FormValue['title'] : ''),'rate' => array('value' => isset($FormValue['rate']) ? $FormValue['rate'] : ''),'comment' => array('value' => isset($FormValue['comment']) ? $FormValue['comment'] : ''),'product_id' => array('value' => isset($FormValue['product_id']) ? $FormValue['product_id'] : ''));
		
		
		Backend::$NodeSettings['status'] = isset($FormValue['status']) ? $FormValue['status'] : 1;
		Backend::$NodeSettings['priority'] = isset($FormValue['priority']) ? $FormValue['priority'] : 0;
		Backend::$NodeSettings['schedule'] = Filter::UnixTimeToDate(isset($FormValue['schedule']) ? $FormValue['schedule'] : 0);
		Backend::$NodeSettings['exprired'] = Filter::UnixTimeToDate(isset($FormValue['exprired']) ? $FormValue['exprired'] : 0);
		if(isset($FormValue['node_settings'])) Backend::$NodeSettings['extra'] = $FormValue['node_settings'];
		ob_start();
		echo '<form class="form-horizontal" action="" method="post">';
		echo '<input type="hidden" name="SaveNodeSubmit" value="1"/>';
		echo '<input type="hidden" name="NodeID" value="' . $NodeID . '"/>';
		include Form::$CompiledPath . 'Controller_review_InsertNode.php';
		echo '<div class="clearfix"></div><div class="clearfix" style="text-align:center;margin:10px 0 15px 0"><input type="submit" class="btn btn-primary" value="Save"/></div>';
		echo '</form>';
		$Form = ob_get_clean();
		$this->Render($Form);
	}

	public function Action() {
		$action = Input::Post('action');
		$ids = Input::Post('ids');
		$ids = array_filter(explode(',', $ids));
		$ids = array_map('intval', $ids);

		$rt = array('status' => 'not');
		if($action == 'deactive') {
			$Deactive = DB::Query('review')->Where('review_id', 'IN', $ids)->Update(array('status' => 0));
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		if($action == 'active') {
			$Deactive = DB::Query('review')->Where('review_id', 'IN', $ids)->Update(array('status' => 1));
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		if($action == 'delete') {
			$Deactive = DB::Query('review')->Where('review_id', 'IN', $ids)->Delete();
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		echo json_encode($rt);
		die();
	}

	public function RemoveNode() {
		$NodeID = 0;
		if(isset(G::$Registry['Params'][ROUTER_EXTRA_KEY])) {
			$NodeID = G::$Registry['Params'][ROUTER_EXTRA_KEY];
			$GetNode = DB::Query('review')->Where('review_id', '=', $NodeID)->Get();
			if($GetNode->num_rows == 0)
				return Helper::Notify('error', 'Node not found');
		}
		if(Input::Post('RemoveNodeSubmit') == 1) {
			$DeleteNode = DB::Query('review')->Where('review_id', '=', $NodeID)->Delete();
			if($DeleteNode->affected_rows == 1) {
				Helper::Notify('success', 'Success delete node ' .  $GetNode->Result[0]['title']);
				Header('Refresh: 1.5; url=' . Router::Generate('Controller', array('controller' => 'review')));
			}
			else Helper::Notify('error', 'Error delete node ' .  $GetNode->Result[0]['title']);
		}
		else {
			$config = array('action'	=> Router::GenerateThisRoute(),
							'tokens'	=> array(array('name' => 'NodeID', 'value' => $NodeID), array('name' => 'RemoveNodeSubmit', 'value' => 1))
							);
			$v = Access::Confirm('Confirm remove node: ' . $GetNode->Result[0]['title'], $config);
			$this->Render($v);
		}
	}

	public function SaveNodeAction($RealNodeID, $OldFormValue = array()) {
		if(Input::Post('SaveNodeSubmit') == 1) {
			$FormValue = Input::Post('Field');
			
			if(!isset($FormValue['title']) || $FormValue['title'] == '')
				Helper::Notify('error', 'Title không thể để trống');

			if(!isset($FormValue['rate']) || $FormValue['rate'] == '')
				Helper::Notify('error', 'Đánh giá không thể để trống');

			if(!isset($FormValue['comment']) || $FormValue['comment'] == '')
				Helper::Notify('error', 'Comment không thể để trống');

			if(!isset($FormValue['product_id']) || $FormValue['product_id'] == '')
				Helper::Notify('error', 'Sản phẩm không thể để trống');

			$FormValue['schedule'] = Filter::DateToUnixTime($FormValue['schedule']['date'], $FormValue['schedule']['hour'], $FormValue['schedule']['minute']);
			if($FormValue['schedule'] < CURRENT_TIME) $FormValue['schedule'] = CURRENT_TIME;
			$FormValue['exprired'] = Filter::DateToUnixTime($FormValue['exprired']['date'], $FormValue['exprired']['hour'], $FormValue['exprired']['minute']);
			if($FormValue['exprired'] < CURRENT_TIME) $FormValue['exprired'] = 0;
			$FormValue['node_settings'] = Backend::BuildNodePermission(Input::Post('VNP_Settings'));

			if(Helper::NotifyCount('error') == 0) {
				$NodeID = Input::Post('NodeID');
				if(empty($NodeID)) {
					$CheckUnique = array();
					$NodeExisted = false;

						if(!$NodeExisted) {
							$FormValue['lang'] = LANG;
							$FormValue['user_id'] = USER_ID;
							$FormValue['last_edit_id'] = USER_ID;
							$FormValue['add_time'] = CURRENT_TIME;
							$FormValue['edit_time'] = CURRENT_TIME;
							$NodeQuery = DB::Query('review')->Insert($FormValue);
							if($NodeQuery->status && $NodeQuery->insert_id > 0) {
								Helper::Notify('success', 'Successful add node in Đánh giá');
							}
							else Helper::Notify('error', 'Cannot add node in Đánh giá');
						}
						else Helper::Notify('error', 'Cannot add node in Đánh giá. Be sure that <em></em> didn\'t existed!');
				}
				else {
					//$CheckExisted = DB::Query('review')->Where('review_id', '=', $NodeID)->Get();
					//if($CheckExisted->num_rows == 1) {
					if($RealNodeID == $NodeID) {
						$FormValue['last_edit_id'] = USER_ID;
						$FormValue['edit_time'] = CURRENT_TIME;
						$FormValue['editing_id'] = 0;
						$NodeQuery = DB::Query('review')->Where('review_id', '=', $NodeID)->Update($FormValue);
						if($NodeQuery->status && $NodeQuery->affected_rows > 0) {
							Helper::Notify('success', 'Successful update node in Đánh giá');
						}
						else Helper::Notify('error', 'Cannot update node in Đánh giá');
					}
					else {
						Helper::Notify('error', 'Cannot update, Node not found!');
					}
				}
			}

			return $FormValue;
		}
	}
    public function settingReview(){
        Helper::State('Setting','');
        Boot::Library('Editor');
        Editor::AddEditor('.transfer');
        Editor::Replace();
        if(Input::Post('saveOptions', array())) {
            $settings = Input::Post('Field');
            $i = 0;
            foreach($settings as $field => $value) {
                $u = DB::Query('options')
                    ->Where('name', '=', $field)->_AND()
                    ->Where('type', '=', 'review')
                    ->Update(array('value' => $value));
                if($u->status) $i++;
            }
            if($i == sizeof($settings))
                Helper::Notify('success', 'Success update settings');
            else
                Helper::Notify('error', 'Some field cannot be update, please try again!');
        }
        $this->Render(
            $this->View('setting')
                ->Assign(
                    array(
                        'options' => Option::getOptionsOfType('review'),
                        'Langs' => G::$Config['lang_support']
                    )
                )
                ->Output()
        );
    }
}