<form class="form-horizontal" action="" method="post">
	<input type="hidden" name="SaveUserSubmit" value="1"/>
	<input type="hidden" name="UID" value="{$UID}">
	<div class="FormPart FormPartCol-6"/>
		<div class="form-group FieldWrap RequiredField">
			<label class="col-sm-2 control-label">Username<span class="RequireField">*</span></label>
		    <div class="col-sm-10">
				<input type="text" name="User[username]" class="form-control RequiredField" value="{$User.username}">
		 	</div>
		</div>
		<div class="form-group FieldWrap RequiredField">
			<label class="col-sm-2 control-label">Email<span class="RequireField">*</span></label>
		    <div class="col-sm-10">
				<input type="text" name="User[email]" class="form-control RequiredField" value="{$User.email}">
			</div>
		</div>
		<div class="form-group FieldWrap RequiredField">
			<label class="col-sm-2 control-label">Fullname</label>
		    <div class="col-sm-10">
				<input type="text" name="User[fullname]" class="form-control RequiredField" value="{$User.fullname}">
			</div>
		</div>
		<div class="form-group FieldWrap FieldType_image Field_image clearfix">
			<label class="col-sm-2 control-label" for="ID_Field[image]">Avatar</label>
		    <div class="col-sm-7">
		    	<input type="text" name="User[avatar]" id="ID_Field[image]" class="form-control FieldType_image Field_image" value="{$User.avatar}">
		   	</div>
		    <div class="col-sm-3">
				<button class="btn btn-primary" type="button" onclick="VNP.SugarBox.Open('FileManager','ID_Field[image]'); return false;">Browse</button>
				<button class="btn btn-danger" onclick="document.getElementById('ID_Field[image]').value = ''; document.getElementById('Thumb_ID_Field[image]').setAttribute('src', '{$User.avatar}'); return false;">Delete</button>
			</div>
		    <div class="col-sm-8 FieldImagePreviewer"><img id="Thumb_ID_Field[image]" src="{$User.avatar_thumb}" /></div>
		</div>
		<div class="form-group FieldWrap RequiredField">
			<label class="col-sm-2 control-label">Birthday</label>
			<div class="col-sm-10 DatePickerCtner">
		    	<div class="input-group date">
                    <input type="text" id="datepicker"  name="User[birthday]" class="form-control hasDatepicker datepicker">
                    <span class="input-group-addon">
					    <i class="ace-icon fa fa-calendar"></i>
					</span>
				</div>

		 	</div>
		</div>
		<div class="form-group FieldWrap RequiredField">
			<label class="col-sm-2 control-label">Gender</label>
			<div class="col-sm-10">
				<select name="User[gender]" class="form-control">
					<option value="-1"{if($User.gender == -1)} selected{/if}>N/A</option>
					<option value="0"{if($User.gender == 0)} selected{/if}>Female</option>
					<option value="1"{if($User.gender == 1)} selected{/if}>Male</option>
				</select>
			</div>
		</div>
		<div class="form-group FieldWrap RequiredField">
			<label class="col-sm-2 control-label">Status</label>
			<div class="col-sm-10">
				<select name="User[status]" class="form-control">
					<option value="1"{if($User.status == 1)} selected{/if}>Active</option>
					<option value="0"{if($User.status == 0)} selected{/if}>Inactive</option>
				</select>
			</div>
		</div>
		<div class="FormSeperator clearfix"></div>
		<div class="form-group FieldWrap RequiredField">
			<label class="col-sm-2 control-label">Password<span class="RequireField">*</span></label>
		    <div class="col-sm-10">
				<input type="password" name="User[password]" class="form-control RequiredField" value="">
			</div>
		</div>
		<div class="form-group FieldWrap RequiredField">
			<label class="col-sm-2 control-label">RePassword<span class="RequireField">*</span></label>
		    <div class="col-sm-10">
				<input type="password" name="User[re_pass]" class="form-control RequiredField" value="">
			</div>
		</div>
	</div>
	<div class="FormPart FormPartCol-4"/>
		<div class="form-group FieldWrap RequiredField">
			<label class="col-sm-2 control-label">Level</label>
			<select name="User[level]" class="form-control">
				{for $Level in $Levels as $LevelName}
				<option value="{$LevelName}"{if($User.level == $LevelName)} selected{/if}>{$Level.name}</option>
				{/for}
			</select>
		</div>
		<div class="form-group FieldWrap RequiredField">
			<label class="col-sm-2 control-label">Permission</label>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                Browser
            </button>
		</div>
	</div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Permission</h4>
                </div>
                <div class="modal-body">
                    <div class="permision" style="overflow: hidden">
                        {for $NodeMaps in $NodeMap}
                            <div class="col-xs-12 col-sm-4">
                                <div class="control-group">
                                    <label class="control-label bolder blue">{$NodeMaps.title}</label>
                                    <div class="checkbox user_permission">
                                        <label>
                                            <input name="User[permision][{$NodeMaps.controller}]" class="ace ace-checkbox-2" type="checkbox" {if(in_array($NodeMaps.controller,$User.permision))} checked {/if}>
                                            <span class="lbl">Cho phép</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        {/for}
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="clearfix"></div>
	<center><input type="reset" class="btn btn-info" value="Reset" />
	<input type="submit" class="btn btn-primary" value="Save" /></center>
</form>
<style>
    .checkbox.user_permission label::before {
        display: none!important;
    }
    .checkbox.user_permission label{
        padding-left: 10px!important;
    }
</style>