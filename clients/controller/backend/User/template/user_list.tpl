<div class="table-responsive">

	<form class="form-horizontal" action="{$SA}" method="post">
		<input type="hidden" name="SearchUsers" value="1">
		<table class="table table-bordered table-striped table-hover">
	        <colgroup>
	        	<col class="col-xs-2">
	            <col class="col-xs-1">
	            <col class="col-xs-2">
	            <col class="col-xs">
	            <col class="col-xs-2">
	            <col class="col-xs">
	            <col class="col-xs-1">
	            <col class="col-xs">
	            <col class="col-xs-1">
	            <col class="col-xs-1">
	        </colgroup>
	        <tbody>
	        	<tr>
	        		<td><input type="text" name="Search[q]" placeholder="Search keyword" class="form-control" value="{$Search.q}" /></td>
	        		<td><label class="control-label">{{lang::search_by}}</label></td>
	        		<td>
	        			{function}$SearchBy = array('username' => 'Username','fullname' => 'Full name','email' => 'Email'){/function}
	        			<select class="form-control" name="Search[searchby]">
	        				{for $SB in $SearchBy as $SK}
	        				<option value="{$SK}"{if($SK == $Search.searchby)} selected{/if}>{$SB}</option>
	        				{/for}
	        			</select>
	        		</td>
	        		<td><label class="control-label">Level</label></td>
	        		<td>
	        			<select class="form-control" name="Search[level]">
		        			<option value="-1"{if($Search.level == '-1')} selected{/if}>All</option>
		        			{for $Level in $Levels as $LevelName}
	        				<option value="{$LevelName}"{if($Search.level == $LevelName)} selected{/if}>{$Level.name}</option>
	        				{/for}
	        			</select>
	        		</td>
	        		<td><label class="control-label">Status</label></td>
	        		<td>
	        			<select class="form-control" name="Search[status]">
		        			<option value="all"{if($Search.status == 'all')} selected{/if}>All</option>
	        				<option value="1"{if($Search.status == '1')} selected{/if}>Active</option>
	        				<option value="0"{if($Search.status == '0')} selected{/if}>Inactive</option>
	        			</select>
	        		</td>
	        		<td><label class="control-label">Show</label></td>
	        		<td><input type="number" class="form-control" value="{$Search.limit}" name="Search[limit]" /></td>
	        		<td><input type="submit" class="btn btn-primary" value="Search" /></td>
				</tr>
			</tbody>
		</table>
	</form>


	{if(!empty($Users))}
	<div class="btn-group" style="margin-bottom: 5px">
		<div class="btn-group">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				{{lang::action_btn}}
				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu" role="menu">
				<li><a href="#" id="VNP_DeactiveUser"><span class="glyphicon glyphicon-remove"></span>&nbsp&nbsp{{lang::deactive}}</a></li>
				<li><a href="#" id="VNP_ActiveUser"><span class="glyphicon glyphicon-ok"></span>&nbsp&nbsp{{lang::active}}</a></li>
				<li><a href="#" id="VNP_ChangeUserLevel"><span class="glyphicon glyphicon-star-empty"></span>&nbsp&nbspChange level</a></li>
				<li><a href="#" id="VNP_DeleteUser"><span class="glyphicon glyphicon-trash"></span>&nbsp&nbsp{{lang::delete}}</a></li>
			</ul>
		</div>
	</div>
    <table class="table table-bordered table-striped table-hover">
        <colgroup>
        	<col class="col-xs">
            <col class="col-xs">
        	<col class="col-xs">
        	<col class="col-xs">
        	<col class="col-xs">
        	<col class="col-xs">
        	<col class="col-xs">
        	<col class="col-xs">
        	<col class="col-xs">
        </colgroup>
        <thead>
	        <tr>
	        	<td>
	        		<div class="checkbox checkbox-success">
                        <input id="toggle-all" value="1" type="checkbox" />
                        <label for="toggle-all"></label>
                    </div>
	        	</td>
	        	<td><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'User', 'action' => 'Main', 'params' => 'sortby/userid/order/' . $Search['re_order'])){/function}" >ID</a></td>
	        	<td><strong><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'User', 'action' => 'Main', 'params' => 'sortby/username/order/' . $Search['re_order'])){/function}" >Username</a></strong></td>
	        	<td><strong><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'User', 'action' => 'Main', 'params' => 'sortby/fullname/order/' . $Search['re_order'])){/function}" >Full name</a></strong></td>
	        	<td><strong><strong><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'User', 'action' => 'Main', 'params' => 'sortby/email/order/' . $Search['re_order'])){/function}" >Email</a></strong></strong></td>
	        	<td><strong><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'User', 'action' => 'Main', 'params' => 'sortby/level/order/' . $Search['re_order'])){/function}" >Level</a></strong></td>
	        	<td><strong><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'User', 'action' => 'Main', 'params' => 'sortby/regdate/order/' . $Search['re_order'])){/function}" >Registered</a></strong></td>
	        	<td><strong><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'User', 'action' => 'Main', 'params' => 'sortby/last_activity/order/' . $Search['re_order'])){/function}" >Last activity</a></strong></td>
	        	<td><strong>Featured</strong></td>
	        </tr>
	    </thead>
	    <tbody style="font-size:12px">
	    	{for $User in $Users as $UID}
	    	{$Level = $Levels[$User['level']]}
	    	<tr class="{if($User.status == 0)}User_Inactive{else}User_Active{/if} {$Level.class}" id="user-{$UID}">
	    		<td>
	    			<div class="checkbox checkbox-success">
                        <input type="checkbox" value="{$UID}" id="checkuser-{$UID}" data-title="{$UID} - {$User.username}" class="item-toggle" />
                        <label for="checkuser-{$UID}"></label>
                    </div>
	    		</td>
	    		<td>{$UID}</td>
	    		<td>{$User.username}</td>
	    		<td>{$User.fullname}</td>
	    		<td>{$User.email}</td>
	    		<td>{$Level.name}</td>
	    		<td>{$User.regdate|Filter::UnixTimeToDate:true}</td>
	    		<td>{function}echo Filter::UnixTimeToDate($User['last_activity'], true){/function}</td>
	    		<td>
                	<a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'User', 'action' => 'AddUser', 'params' => $UID)){/function}"><span class="glyphicon glyphicon-edit"></span>&nbsp{{lang::edit}}</a>&nbsp;&nbsp;
                    <a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'User', 'action' => 'Remove', 'params' => 'user/' . $UID)){/function}"><span class="glyphicon glyphicon-trash"></span>&nbsp{{lang::delete}}</a>
              	</td>
	    	</tr>
	    	{/for}
	    </tbody>
    </table>
    {/if}
</div>
<script type="text/javascript">
function haha() {
}
function VNP_DeactiveUser(checkedInputs, titleData) {
	if(checkedInputs.length == 0) alert('You must select at least one item');
	else {
		if(confirm('Do you want to do this?')) {
			VNP.Ajax({
				url: 'User/Action',
				type: 'POST',
				data: {action: 'deactive',ids: checkedInputs},
				success: function(res) {
					if(res.status == 'ok') {
						$.each(res.items, function(i,v) {
							$('#user-' + v).removeClass('User_Active');
							$('#user-' + v).addClass('User_Inactive');
						});
						VNP.Loader.TextNotify('Success deactive users!');
					}
					//VNP.Loader.hide();
				}
			}, 'json');
		}
	}
}
function VNP_ActiveUser(checkedInputs, titleData) {
	if(checkedInputs.length == 0) alert('You must select at least one item');
	else {
		if(confirm('Do you want to do this?')) {
			VNP.Ajax({
				url: 'User/Action',
				type: 'POST',
				data: {action: 'active',ids: checkedInputs},
				success: function(res) {
					if(res.status == 'ok') {
						$.each(res.items, function(i,v) {
							$('#user-' + v).addClass('User_Active');
							$('#user-' + v).removeClass('User_Inactive');
						});
						VNP.Loader.TextNotify('Success active users!');
					}
				}
			}, 'json');
		}
	}
}
function VNP_DeleteUser(checkedInputs, titleData) {
	if(checkedInputs.length == 0) alert('You must select at least one item');
	else {
		if(confirm('Do you want to do this?')) {
			VNP.Ajax({
				url: 'User/Action',
				type: 'POST',
				data: {action: 'delete',ids: checkedInputs},
				success: function(res) {
					if(res.status == 'ok') {
						$.each(res.items, function(i,v) {
							$('#user-' + v).remove();
						})
					}
					VNP.Loader.TextNotify('Success delete users!');
				}
			}, 'json');
		}
	}
}
$(document).ready(function() {
	var checkedInputs = '';
	var titleData = '';
	$('#toggle-all').InputToggle({
		childInput: '.item-toggle',
		dataAttribute: 'data-title',
		storageVar: 'checkedInputs',
		titleData:	'titleData',
		featureAction: [
			{container: '#VNP_DeactiveUser', callback: "VNP_DeactiveUser(checkedInputs, titleData)" },
			{container: '#VNP_ActiveUser', callback: "VNP_ActiveUser(checkedInputs, titleData)" },
			{container: '#VNP_DeleteUser', callback: "VNP_DeleteUser(checkedInputs, titleData)" }
		],
		callBackFunction: 'haha(checkedInputs)',
		enableCookie: true
	});
});
</script>