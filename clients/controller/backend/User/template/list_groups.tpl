<div class="table-responsive">
	<form class="form-horizontal" action="" method="post">
		<input type="hidden" name="SaveGroupSubmit" value="1">
		<input type="hidden" name="GID" value="{$GID}">
		<table class="table table-bordered table-striped table-hover">
	        <colgroup>
	        	<col class="col-xs">
	            <col class="col-xs-2">
	            <col class="col-xs-3">
	            <col class="col-xs-2">
	            <col class="col-xs-4">
	        </colgroup>
	        <tbody>
	        	<tr>
	        		<td rowspan="3">
	        			<input type="submit" name="SaveGroup" class="btn btn-primary" value="Save" />
	        		</td>
	        	</tr>
	        	<tr>
	        		<td>Group Name</td>
	        		<td>
	        			<input type="text" name="group[name]" class="form-control" value="{$Group.name}" />
					</td>
					<td>Parent group</td>
	        		<td>
	        			<select name="group[parent_id]" class="form-control">
	        				<option value="0">None</option>
	        				{for $G in $Groups}
	        				<option value="{$G.gid}"{if($Group.parent_id == $G.gid)} selected{/if}>{$G.prefix}{$G.name}</option>
	        				{/for}
	        			</select>
					</td>
				</tr>
				<tr>
					<td>Level</td>
	        		<td>
	        			<select name="group[level]" class="form-control">
	        				{for $Level in $Levels as $LevelName}
	        				<option value="{$LevelName}"{if($Group.level == $LevelName)} selected{/if}>{$Level.name}</option>
	        				{/for}
	        			</select>
					</td>
	        		<td>Group Description</td>
	        		<td colspan="5">
	        			<textarea name="group[description]" class="form-control">{$Group.description}</textarea>
					</td>
				</tr>
			</tbody>
		</table>
	</table>
	{if(!empty($Groups))}
    <table class="table table-bordered table-striped table-hover">
        <colgroup>
        	<col class="col-xs">
            <col class="col-xs">
        	<col class="col-xs">
        </colgroup>
        <thead>
	        <tr>
	        	<td><strong>Group name</strong></td>
	        	<td><strong>Description</strong></td>
	        	<td><strong>Functions</strong></td>
	        </tr>
	    </thead>
	    <tbody>
	    	{for $Group in $Groups as $GID}
	    	<tr>
	    		<td>{$Group.prefix}{$Group.name}</td>
	    		<td>{$Group.description}</td>
	    		<td>
                	<a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'User', 'action' => 'Group', 'params' => $Group['gid'])){/function}"><span class="glyphicon glyphicon-edit"></span>&nbsp{{lang::edit}}</a>&nbsp;&nbsp;
                    <a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'User', 'action' => 'Remove', 'params' => 'group/' . $Group['gid'])){/function}"><span class="glyphicon glyphicon-trash"></span>&nbsp{{lang::delete}}</a>
              	</td>
	    	</tr>
	    	{/for}
	    </tbody>
    </table>
    {/if}
</div>