<?php

if( !defined('VNP_SYSTEM') && !defined('VNP_APPLICATION') && !defined('ADMIN_AREA') ) die('Access denied!');
Access::RequirePermission(Boot::SMOD_SESSION);
class User extends Controller {
	private $StaticGroups = array(1,2,3,4);
	public function __construct() {
	}
	public function Main() {
		$Search = array('sortby' 	=> 'userid',
						'order'		=> 'desc',
						'q'			=> '',
						'searchby'	=> 'username',
						'level'		=> '-1',
						'status'	=> 'all',
						'limit'		=> 20);
		$FromForm = Input::Post('Search', array());

		$Search = array_merge($Search, G::$Registry['Params']);
		$Search = array_merge($Search, $FromForm);
		Router::BuildParamsString($Search, true);

		$Search['re_order'] = ($Search['order'] == 'desc') ? 'asc' : 'desc';

		G::$Session->Set('user_sort', $Search['order']);

		Helper::PageInfo('List users');
		Helper::State('List users', Router::Generate('Controller', array('controller' => 'User')));
		Helper::FeaturedPanel(lang('Add new') . ' user',BASE_DIR . 'User/AddUser', 'plus');
		$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups,InputGroups,Tables,ButtonGroups,Dropdowns');
		Theme::JqueryPlugin('InputToggle');

		$GetUsers = DB::Query('users')->Order($Search['sortby'], $Search['order'])->Limit($Search['limit']);
		$GetUsers = $GetUsers->Where('userid', '>', 0);
		if($Search['status'] != 'all')
			$GetUsers = $GetUsers->_AND()->Where('status', '=', $Search['status']);
		if($Search['level'] != '-1')
			$GetUsers = $GetUsers->_AND()->Where('level', '=', $Search['level']);

		if(!empty($Search['q']))
			$GetUsers = $GetUsers->_AND()
								->WhereGroupOpen()
									->Where($Search['searchby'], 'SEARCH', $Search['q'])->_OR()
									->Where($Search['searchby'], 'LIKE', $Search['q'])
								->WhereGroupClose();

		$GetUsers = $GetUsers->Get('userid', Output::Paging());

		if($GetUsers->num_rows == 0) Helper::Notify('info', 'There is no user!');
		$v = $this->View('user_list');
		$v->Assign('Search', $Search);
		$v->Assign('SA', Router::GenerateThisRoute());
		$v->Assign('Levels', $this->UserLevels);
		$v->Assign('Levels', $this->UserLevels);
		$v->Assign('Users', $GetUsers->Result);
		$this->Render($v->Output());
	}

	public function Action() {
		$action = Input::Post('action');
		$ids = Input::Post('ids');
		$ids = array_filter(explode(',', $ids));
		$ids = array_map('intval', $ids);

		$rt = array('status' => 'not');
		if($action == 'deactive') {
			$Deactive = DB::Query('users')->Where('userid', 'IN', $ids)->Update(array('status' => 0));
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		if($action == 'active') {
			$Deactive = DB::Query('users')->Where('userid', 'IN', $ids)->Update(array('status' => 1));
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		if($action == 'delete') {
			$Deactive = DB::Query('users')->Where('userid', 'IN', $ids)->Delete();
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		echo json_encode($rt);
		die();
	}

	public function AddUser() {
		$UID = 0;
		if(isset(G::$Registry['Params'][ROUTER_EXTRA_KEY])) $UID = G::$Registry['Params'][ROUTER_EXTRA_KEY];
		$User = array (	'username' => '',
						'email' => '',
						'fullname' => '',
						'avatar' => 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==',
						'birthday' => 0,
						'gender' => '-1',
						'status' => '1',
						'password' => '',
						're_pass' => '',
						'level' => '1',
						'groups' => '',
                        'permision' => ''
					);
		$GetUser = DB::Query('users')->Where('userid', '=', $UID)->Get();
		if($GetUser->status && $GetUser->num_rows == 1) {
			$GetUser = $GetUser->Result[0];
			unset($GetUser['password']);
			$User = array_merge($User, $GetUser);
		}
		if(Input::Post('SaveUserSubmit') == 1) {
			$PostUser = Input::Post('User');
			$UID = Input::Post('UID');
			$User = array_merge($User, $PostUser);
			if(empty($User['username'])) Helper::Notify('error', 'Username is empty!');
			if(empty($User['email'])) Helper::Notify('error', 'Email is empty!');
			else {
				if(!Filter::CheckValidEmail($User['email']))
					Helper::Notify('error', 'Invalid email!');
			}
			if(!empty($User['password'])) {
				if(empty($User['re_pass']) || $User['re_pass'] != $User['password'])
					Helper::Notify('error', 'Password does not match!');
			}
			elseif(empty($User['password']) && $UID == 0)
				Helper::Notify('error', 'Password is empty!');
			else unset($User['password']);
			unset($User['re_pass']);
			if(is_array($User['groups']))
				$User['groups'] = implode(',', $User['groups']);
			$User['permision'] = serialize($User['permision']);
			$User['regdate'] = CURRENT_TIME;
			$User['last_activity'] = CURRENT_TIME;
            if(Helper::NotifyCount('error') == 0) {
				if($UID == 0) {
					if(Authorized::AddUser($User))
						Helper::Notify('success', 'Success add user');
					else Helper::Notify('error', 'Cannot add user');
				}
				else {
					if(Authorized::UpdateUser($User, $UID))
						Helper::Notify('success', 'Success update user');
					else Helper::Notify('error', 'Cannot update user');
				}
			}
		}

		$User['groups'] = explode(',', $User['groups']);
		if(strlen($User['permision'])>2){
            $User['permision'] = unserialize($User['permision']);
            $User['permision'] = array_keys($User['permision']);
        }
        else {
            $User['permision']= array();
        }

		if(!empty($User['avatar']) && $User['avatar'] != 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==')
			$User['avatar_thumb'] = Output::GetThumbLink($User['avatar']);
		else $User['avatar_thumb'] = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';

		Helper::PageInfo('Add user');
		Helper::State('List users', Router::Generate('Controller', array('controller' => 'User')));
		Helper::State('Add user', Router::Generate('ControllerAction', array('controller' => 'User', 'action' => 'AddUser')));
		Helper::FeaturedPanel('List users',BASE_DIR . 'User', 'list');
		Helper::FeaturedPanel(lang('Add new') . ' user',BASE_DIR . 'User/AddUser', 'plus');
		$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups,InputGroups,Tables');

		$v = $this->View('add_user');
		$v->Assign('Groups', DB::Query('user_groups')->Get('gid')->Result);
		$v->Assign('Levels', $this->UserLevels);
		$v->Assign('User', $User);
		$v->Assign('UID', $UID)->Assign('NodeMap',Backend::$NodeMapper);
		$this->Render($v->Output());
	}

	public function GetGroups() {
		$Groups = array();
		$GetGroups = DB::Query('user_groups')->Get('gid');
		if($GetGroups->num_rows > 0)
			$Groups = Filter::BuildLevelList($GetGroups->Result, 'gid', 'parent_id');
		if(IS_AJAX) {
			echo json_encode($Groups);
			die();
		}
		else return $Groups;
	}

	public function Group() {
		$GID = 0;
		if(isset(G::$Registry['Params'][ROUTER_EXTRA_KEY])) $GID = G::$Registry['Params'][ROUTER_EXTRA_KEY];

		$Group = array('name' => '', 'parent_id' => 0, 'description' => '', 'level' => '');

		if($GID > 0) {
			$GetGroup = DB::Query('user_groups')->Where('gid', '=', $GID)->Get();
			if($GetGroup->status && $GetGroup->num_rows == 1) {
				$Group = $GetGroup->Result[0];
				Helper::Notify('info', 'Editing group ' . $Group['name']);
			}
			else {
				Helper::Notify('error', 'Group not found!');
				return false;
			}
		}
		if(Input::Post('SaveGroupSubmit') == 1) {
			$GID = Input::Post('GID', 0);
			$Group = Input::Post('group');
			if($Group['name'] == '') Helper::Notify('error', 'Group name is empty!');
			if(Helper::NotifyCount('error') == 0) {
				if($GID == 0) {
					$AddGroup = DB::Query('user_groups')->Insert($Group);
					if($AddGroup->status && $AddGroup->insert_id > 0)
						Helper::Notify('success', 'Successful add group!');
					else Helper::Notify('error', 'Cannot add group, check if group existed!');
				}
				else {
					$AddGroup = DB::Query('user_groups')->Where('gid', '=', $GID)->Update($Group);
					if($AddGroup->status) {
						($AddGroup->affected_rows > 0) ?
						Helper::Notify('success', 'Successful update group!') :
						Helper::Notify('info', 'Nothing was changed!');
					}
					else Helper::Notify('error', 'Cannot update group, check if group existed!');
				}
			}
		}
		Helper::PageInfo('List user groups');
		Helper::State('List user groups', Router::Generate('ControllerAction', array('controller' => 'User', 'action' => 'Group')));
		$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups,InputGroups,Tables');
		$v = $this->View('list_groups');
		$v->Assign('GID', $GID);
		$v->Assign('Group', $Group);
		$v->Assign('Levels', $this->UserLevels);

		$GetGroups = DB::Query('user_groups')->Get('gid');
		if($GetGroups->num_rows > 0) $v->Assign('Groups', Filter::BuildLevelList($GetGroups->Result, 'gid', 'parent_id'));
		else {
			$v->Assign('Groups', array());
			Helper::Notify('info', 'There is no groups found!');
		}
		$this->Render($v->Output());
	}

	public function Remove() {
		$Keys = array_keys(G::$Registry['Params']);
		if(in_array($Keys[0], array('group', 'user'))) {
			$ID = G::$Registry['Params'][$Keys[0]];
			if($Keys[0] == 'group') {
				Helper::State('List user groups', Router::Generate('ControllerAction', array('controller' => 'User', 'action' => 'Group')));
				$KeyID = 'gid';
				$FormKey = 'RemoveGroupSubmit';
				$Table = 'user_groups';
				$Action = 'Group';
			}
			if($Keys[0] == 'user') {
				Helper::State('List users', Router::Generate('Controller', array('controller' => 'User')));
				$KeyID = 'userid';
				$FormKey = 'RemoveUserSubmit';
				$Table = 'users';
				$Action = 'Main';
			}

			if(Input::Post($FormKey) == 1) {
				$DeleteNode = DB::Query($Table)->Where($KeyID, '=', $ID)->Delete();
				if($DeleteNode->affected_rows == 1) {
					Helper::Notify('success', 'Success delete ' . $Keys[0] . ' ' .  $ID);
					Header('Refresh: 1.5; url=' . Router::Generate('ControllerAction', array('controller' => 'User', 'action' => $Action)));
				}
				else Helper::Notify('error', 'Error delete ' . $Keys[0] . ' ' .  $ID);
			}
			else {
				$config = array('action'	=> Router::GenerateThisRoute(),
								'tokens'	=> array(array('name' => $KeyID, 'value' => $ID), array('name' => $FormKey, 'value' => 1))
								);
				$v = Access::Confirm('Confirm remove ' . $Keys[0] . ': ' . $Keys[0] . ' ' . $ID, $config);
				$this->Render($v);
			}
		}
	}
    public function suggetUser(){
        $query = Input::Post('query','');
        $query = urldecode($query);
        $product = DB::Query('users')->Columns('userid,email')->Where('email','LIKE',$query)->Get()->Result;
        header('Content-type: application/json');
        echo json_encode($product);die();
    }
}


?>