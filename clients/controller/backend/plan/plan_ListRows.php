<?php
if(!class_exists('plan')) die('Stop here!');
$Search = array('sortby' 	=> 'plan_id',
				'order'	=> 'DESC',
				'q'		=> '',
				'searchby'=> 'course',
				'status'	=> 'all',
				'limit'	=> 20);
$Search['course'] = 0;
$SortBy = array (
  'course' => 'Khóa học',
  'title' => 'Tên lớp',
  'khaigiang' => 'Khai giảng',
);
$SearchBy = array (
  'title' => 'Tên lớp',
);
$_RefFilter = array (
  'course' => 
  array (
    'label' => 'Khóa học',
    'nodetype' => 'course',
    'nodefield' => 'title',
  ),
);
$FromForm = Input::Post('Search', array());
$Search = array_merge($Search, G::$Registry['Params']);
$Search = array_merge($Search, $FromForm);
$Search['q'] = urldecode($Search['q']);
Router::BuildParamsString($Search, true);
$Search['re_order'] = ($Search['order'] == 'desc') ? 'asc' : 'desc';
G::$Session->Set('user_sort', $Search['order']);
Helper::State('List nodes', Router::GenerateThisRoute());
$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups,InputGroups,Tables,ButtonGroups,Dropdowns');
Theme::JqueryPlugin('InputToggle');
Theme::JsFooter('ListNodeLib', APPLICATION_DATA_DIR . 'static/Node/list.js');
Theme::JsFooter('ControllerName', 'var NodeController = \'plan\';', 'inline');
$Adapters['vnp_title_course'] = NodeBase::getNodes_Option('course', 'title', 'course_id');
$Adapters['vnp_title_course_id_key'] = NodeBase::getNodes_IDKey('course', 'title', 'course_id');
$ShowFields = array (
  0 => 'course',
  1 => 'title',
  2 => 'khaigiang',
  3 => 'plan_id',
  4 => 'status',
);
$GetNodes = DB::Query('plan')->Columns($ShowFields);
$GetNodes = $GetNodes->Limit($Search['limit']*1);
$GetNodes = $GetNodes->Order($Search['sortby'], $Search['order']);
$GetNodes = $GetNodes->Where('plan_id', '>', 0);
if($Search['status'] != 'all')
$GetNodes = $GetNodes->_AND()->Where('status', '=', $Search['status']);
foreach($_RefFilter as $RF => $RFD) {
		if(isset($Search[$RF]) && $Search[$RF] > 0)
		$GetNodes = $GetNodes->_AND()
								->WhereGroupOpen()
									->Where($RF, '=', intval($Search[$RF]))->_OR()
									->Where($RF, 'INCLUDE', $Search[$RF])
								->WhereGroupClose();
}
if(!empty($Search['q']))
$GetNodes = $GetNodes->_AND()
								->WhereGroupOpen()
									->Where($Search['searchby'], 'SEARCH', $Search['q'])->_OR()
									->Where($Search['searchby'], 'LIKE', $Search['q'])
								->WhereGroupClose();
$GetNodes = $GetNodes->Get('plan_id', Output::Paging());
$v = $this->View('list_nodes');
$v->Assign('Search', $Search);
$v->Assign('SortBy', $SortBy);
$v->Assign('SA', Router::GenerateThisRoute());
$v->Assign('SearchBy', $SearchBy);
$v->Assign('Adapters', $Adapters);
if($GetNodes->num_rows > 0) {
	$v->Assign('Nodes', $GetNodes->Result);
} else Helper::Notify('info', 'There is no nodes found!');
$this->Render($v->Output());
Helper::PageInfo('List nodes for Kế hoạch ( Found ' . $GetNodes->num_rows . ' nodes )');
Helper::FeaturedPanel('Add new Kế hoạch',BASE_DIR . 'plan/AddNode', 'plus');