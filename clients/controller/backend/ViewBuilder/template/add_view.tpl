<form class="form-horizontal" action="" method="post">
    <div class="form-group">
        <label class="col-sm-4 control-label">Build view for node type<span class="RequireField">*</span></label>
        <div class="col-sm-8">
            <select class="form-control" onchange="javascript:window.location += this.value">
                <option value="">Choose node type</option>
                {for $nt in $nodeTypes as $nt_name}
                    <option value="/{$nt_name}">{$nt.NodeTypeInfo.title}</option>
                {/for}
            </select>
        </div>
    </div>
</form>