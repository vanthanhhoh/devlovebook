<script src="{#GLOBAL_DATA_DIR}library/ace/ace.js"></script>
<!-- load ace modelist extension -->
<script src="{#GLOBAL_DATA_DIR}library/ace/ext-language_tools.js"></script>
<style type="text/css" media="screen">
    .ace_editor {
        position: relative !important;
        border: 1px solid lightgray;
        margin: 0 !important;
        height: 400px;
        width: 100%;
        border: 3px solid #000;
        font-size: 14px;
    }
    ul#fieldExtra {
        margin: 0;
        padding: 0;
        list-style: none;
    }
    ul#fieldExtra li {
        display: inline-block;
        font-size: 13px;
        background: #185E17;
        margin: 3px 5px;
        padding: 1px 2px;
        color: #FFF;
        border-radius: 3px;
        font-family: Courier New;
        cursor: pointer;
    }
    ul.editorBar {
        margin: 0;
        padding: 6px 0 2px 5px;
        list-style: none;
        background: #000;
    }
    ul.editorBar li {
        display: inline-block;
        margin: 0 3px 0 0;
    }
    ul.editorBar li a {
        display: block;
        padding: 2px 5px;
        color: #000;
        font-size: 11px;
        text-decoration: none;
        background: #A9A9A9;
        border-radius: 1px;
        transition:all 0.2s
    }
    ul.editorBar li a:hover, ul.editorBar li a.active {
        background: #F4F4F4;
    }
</style>
<form class="form-horizontal" id="add_view_form" action="" method="post">
    <input name="createViewSubmit" type="hidden" value="1" />
    <table class="table table-bordered table-striped table-hover">
        <colgroup>
            <col class="col-xs-2">
            <col class="col-xs-10">
        </colgroup>
        <thead>
        <tr>
            <th><strong>Fields</strong></th>
            <th><strong>Template</strong></th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <table class="table table-bordered table-striped table-hover">
                        {for $f in $nodeType.NodeFields}
                            <tr>
                                <td>
                                    <div class="checkbox checkbox-success">
                                        <input data-field="{$f.name}" type="checkbox" id="view-{$f.name}" name="view[fields][]" value="{$f.name}"{if(in_array($f.name, $view.fields))} checked{/if}>
                                        <label class="getfield" data-field="{$f.name}" for="view-{$f.name}">&nbsp{$f.label}</label>
                                    </div>
                                </td>
                            </tr>
                        {/for}
                    </table>
                </td>
                <td>
                    <ul id="fieldExtra"></ul>
                    <textarea id="view-template-input" name="view[template]" style="display: none">{$view.template}</textarea>
                    <ul class="editorBar">
                        <li><a href="#" class="active" data-action="readonly" data-value="true">Read only on</a></li>
                        <li><a href="#" data-action="readonly" data-value="false">Read only off</a></li>
                    </ul>
                    <textarea id="view-template-ctner">
                    </textarea>
                </td>
            </tr>
        <tr>
            <td colspan="2" align="center"><input class="btn btn-primary" type="submit" value="Save" /></td>
        </tr>
        </tbody>
    </table>
</form>
<script>
    var nodeTypeName = '{$nodeType.NodeTypeInfo.name}'
    // trigger extension
    ace.require("ace/ext/language_tools");
    var editor = ace.edit("view-template-ctner");
    editor.session.setMode("ace/mode/html");
    editor.setTheme("ace/theme/monokai");
    // enable autocompletion and snippets
    editor.setOptions({
        enableBasicAutocompletion: true,
        enableSnippets: true,
        enableLiveAutocompletion: false
    });
    $(document).ready(function() {
        editor.setValue($('#view-template-input').val());
        editor.gotoLine(3,9,true);
        editor.setReadOnly(true);
        $('#add_view_form').submit(function(e) {
            $('#view-template-input').html(editor.getValue());
            return true;
        });

        var checkedFields = new Array();
        $('.getfield').click(function(e) {
            var _field = $(this).attr('data-field');
            if($.inArray(_field, checkedFields) == -1) {
                checkedFields.push(_field);
                $('#fieldExtra').append('<li id="slt-' + _field + '">{$' + 'node.' + _field + '}');
                if(editor.getReadOnly()) editor.gotoLine(3,9,true);
                editor.insert('\n\t\t<div class="' + nodeTypeName + '-' + _field + '">{$' + 'node.' + _field + '}</div>');
            }
            else {
                var currentCursoPos = editor.selection.getCursor();
                checkedFields = $.grep(checkedFields, function(value) {
                    return value != _field;
                });
                $('#slt-' + _field).remove();
                editor.find('\n\t\t<div class="' + nodeTypeName + '-' + _field + '">{$' + 'node.' + _field + '}</div>');
                editor.replaceAll('');
                editor.clearSelection();
                editor.gotoLine(currentCursoPos.row,currentCursoPos.column,true);
            }
        });

        $('.editorBar li a').click(function(e) {
            e.preventDefault();
            var action = $(this);
            if(action.attr('data-action') == 'readonly') {
                $('.editorBar li a[data-action="readonly"]').removeClass('active');
                if(action.attr('data-value') == 'true') {
                    editor.setReadOnly(true);
                    $('.editorBar li a[data-value="true"]').addClass("active");
                }
                else {
                    editor.setReadOnly(false);
                    editor.focus();
                    $('.editorBar li a[data-value="false"]').addClass("active");
                }
            }
        });
    });
</script>