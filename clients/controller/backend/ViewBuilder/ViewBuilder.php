<?php

if( !defined('VNP_SYSTEM') && !defined('VNP_APPLICATION') && !defined('ADMIN_AREA') ) die('Access denied!');
Boot::Library('NodeBuilder');

define('CONTROLLER_BASE_URL', Router::Generate('Controller',array('controller'=> 'ViewBuilder')));

class ViewBuilder extends Controller {
    private $NodeTypes = array();
    function __construct() {
        if(file_exists(NODE_CACHE_FILE . NodeBuilder::CACHE_FILE_EXTENSION)) {
            $this->NodeTypes = unserialize(File::GetContent(NODE_CACHE_FILE . NodeBuilder::CACHE_FILE_EXTENSION));
            $this->NodeTypes = $this->NodeTypes['NodeTypes'];
        }
    }

    public function Main() {

    }

    public function AddView() {

        Helper::PageInfo('Add view');
        Helper::FeaturedPanel(lang('Add new') . ' view',BASE_DIR . 'ViewBuilder/AddView/0', 'plus');
        $this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups,InputGroups,ButtonGroups,Tables');

        if(isset(G::$Route['params']['params']) && in_array(G::$Route['params']['params'], array_keys($this->NodeTypes))) {
            $NodeType = G::$Route['params']['params'];

            $view = array(
                'fields' => array(),
                'template' => '<ul class="' . $NodeType . 'List">
    {for $node in $nodes}
    <li>
    </li>
    {/for}
</ul>'
            );

            if(Input::Post('createViewSubmit') == 1) {
                $view = array_merge($view, Input::Post('view', array()));
                File::CreateDirectory(GLOBAL_VIEWS_PATH . $NodeType);
                File::Create(GLOBAL_VIEWS_PATH . $NodeType . DIRECTORY_SEPARATOR . $NodeType . '.tpl', $view['template']);
            }


            Helper::PageInfo('Add view for ' . $this->NodeTypes[$NodeType]['NodeTypeInfo']['title']);
            Helper::State('Add view', Router::Generate('ControllerAction', array('controller' => 'ViewBuilder', 'action' => 'AddView')));
            Helper::State('Add view for ' . $this->NodeTypes[$NodeType]['NodeTypeInfo']['title'], Router::Generate('ControllerParams', array('controller' => 'ViewBuilder', 'action' => 'AddView', 'params' => $NodeType)));

            $this->Render(
                $this->View('add_view_action')
                    ->Assign('nodeType', $this->NodeTypes[$NodeType])
                    ->Assign('view', $view)
                    ->Output()
            );
        }
        else
            $this->Render($this->View('add_view')->Assign('nodeTypes', $this->NodeTypes)->Output());
    }
}

?>