<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover">
        <colgroup>
            <col class="col-xs">
        	<col class="col-xs">
            <col class="col-xs-2">
            <col class="col-xs-2">
            <col class="col-xs-2">
            <col class="col-xs">
            <col class="col-xs-2">
            <col class="col-xs-2">
            <col class="col-xs-2">
        </colgroup>
        <thead>
            <tr>
                <th></th>
            	<th><strong>IF</strong></th>
                <th><strong>Label</strong></th>
                <th><strong>Name</strong></th>
                <th><strong>Type</strong></th>
                <th><strong>R</strong></th>
                <th><strong>Filter</strong></th>
                <th><strong>Default value</strong></th>
                <th><strong>Actions</strong></th>
            </tr>
        </thead>
        <tbody id="NodeStructor">
        	{for $NodeField in $NodeType.NodeFields}
            <tr class="NodeType_Field" data-field-name="{$NodeField.name}">
                <td><span class="Move_Option"><span class="glyphicon glyphicon-resize-vertical"></span></span></td>
            	<td><span class="glyphicon glyphicon-{if($NodeField.inform == 1)}ok{else}ban{/if}-circle"></span></td>
            	<td><a href="{$FieldAction}/EditField/{$NodeField.name}">{$NodeField.label}</a></td>
                <td>{$NodeField.name}</td>
                <td>{$NodeField.type}</td>
                <td><span class="glyphicon glyphicon-{if($NodeField.require == 1)}ok{else}ban{/if}-circle"></span></td>
                <td>{$NodeField.filter}</td>
                <td>{$NodeField.value}</td>
                <td>
                	<a href="{$FieldAction}/EditField/{$NodeField.name}"><span class="glyphicon glyphicon-edit"></span>&nbsp;Change</a>&nbsp;&nbsp;
                   	<a href="{$FieldAction}/DropField/{$NodeField.name}"><span class="glyphicon glyphicon-minus-sign"></span>&nbsp;Drop</a>
              	</td>
            </tr>
            {/for}
        </tbody>
 	</table>
</div>
