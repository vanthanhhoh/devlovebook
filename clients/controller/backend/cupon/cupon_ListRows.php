<?php
if(!class_exists('cupon')) die('Stop here!');
$Search = array('sortby' 	=> 'cupon_id',
				'order'	=> 'DESC',
				'q'		=> '',
				'searchby'=> 'title',
				'status'	=> 'all',
				'limit'	=> 10);
$SortBy = array (
  'title' => 'Tiêu đề',
  'code' => 'Mã giảm giá',
  'discount' => 'Giảm giá',
);
$SearchBy = array (
  'title' => 'Tiêu đề',
  'code' => 'Mã giảm giá',
  'discount' => 'Giảm giá',
);
$_RefFilter = array (
);
$FromForm = Input::Post('Search', array());
$Search = array_merge($Search, G::$Registry['Params']);
$Search = array_merge($Search, $FromForm);
$Search['q'] = urldecode($Search['q']);
Router::BuildParamsString($Search, true);
$Search['re_order'] = ($Search['order'] == 'desc') ? 'asc' : 'desc';
G::$Session->Set('user_sort', $Search['order']);
Helper::State('List nodes', Router::GenerateThisRoute());
$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups,InputGroups,Tables,ButtonGroups,Dropdowns');
Theme::JqueryPlugin('InputToggle');
Theme::JsFooter('ListNodeLib', APPLICATION_DATA_DIR . 'static/Node/list.js');
Theme::JsFooter('ControllerName', 'var NodeController = \'cupon\';', 'inline');
$Options['apply'] = array (
  -1 => 'Tất cả',
);
$Options['discount_type'] = array (
  1 => 'Giảm số tiền',
  2 => 'Giảm theo %',
);
$ShowFields = array (
  0 => 'title',
  1 => 'code',
  2 => 'discount',
  3 => 'cupon_id',
  4 => 'status',
);
$GetNodes = DB::Query('cupon')->Columns($ShowFields);
$GetNodes = $GetNodes->Limit($Search['limit']*1);
$GetNodes = $GetNodes->Order($Search['sortby'], $Search['order']);
$GetNodes = $GetNodes->Where('cupon_id', '>', 0);
if($Search['status'] != 'all')
$GetNodes = $GetNodes->_AND()->Where('status', '=', $Search['status']);
foreach($_RefFilter as $RF => $RFD) {
		if(isset($Search[$RF]) && $Search[$RF] > 0)
		$GetNodes = $GetNodes->_AND()
								->WhereGroupOpen()
									->Where($RF, '=', intval($Search[$RF]))->_OR()
									->Where($RF, 'INCLUDE', $Search[$RF])
								->WhereGroupClose();
}
if(!empty($Search['q']))
$GetNodes = $GetNodes->_AND()
								->WhereGroupOpen()
									->Where($Search['searchby'], 'SEARCH', $Search['q'])->_OR()
									->Where($Search['searchby'], 'LIKE', $Search['q'])
								->WhereGroupClose();
$GetNodes = $GetNodes->Get('cupon_id', Output::Paging());
$v = $this->View('list_nodes');
$v->Assign('Search', $Search);
$v->Assign('SortBy', $SortBy);
$v->Assign('SA', Router::GenerateThisRoute());
$v->Assign('SearchBy', $SearchBy);
if($GetNodes->num_rows > 0) {
	$v->Assign('Nodes', $GetNodes->Result);
$v->Assign('Options', $Options);
} else Helper::Notify('info', 'There is no nodes found!');
$this->Render($v->Output());
Helper::PageInfo('List nodes for Mã giảm giá ( Found ' . $GetNodes->num_rows . ' nodes )');
Helper::FeaturedPanel('Add new Mã giảm giá',BASE_DIR . 'cupon/AddNode', 'plus');