<?php
if(!class_exists('post_category')) die('Stop here!');
$Search = array('sortby' 	=> 'post_category_id',
				'order'	=> 'DESC',
				'q'		=> '',
				'searchby'=> 'title',
				'status'	=> 'all',
				'limit'	=> 10);
$Search['parent_id'] = 0;
$SortBy = array (
  'title' => 'Tiêu đề',
  'parent_id' => 'Danh mục cha',
);
$SearchBy = array (
  'title' => 'Tiêu đề',
);
$_RefFilter = array (
  'parent_id' => 
  array (
    'label' => 'Danh mục cha',
    'nodetype' => 'post_category',
    'nodefield' => 'title',
  ),
);
$FromForm = Input::Post('Search', array());
$Search = array_merge($Search, G::$Registry['Params']);
$Search = array_merge($Search, $FromForm);
$Search['q'] = urldecode($Search['q']);
Router::BuildParamsString($Search, true);
$Search['re_order'] = ($Search['order'] == 'desc') ? 'asc' : 'desc';
G::$Session->Set('user_sort', $Search['order']);
Helper::State('List nodes', Router::GenerateThisRoute());
$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups,InputGroups,Tables,ButtonGroups,Dropdowns');
Theme::JqueryPlugin('InputToggle');
Theme::JsFooter('ListNodeLib', APPLICATION_DATA_DIR . 'static/Node/list.js');
Theme::JsFooter('ControllerName', 'var NodeController = \'post_category\';', 'inline');
$Adapters['vnp_title_post_category'] = NodeBase::getNodes_Option('post_category', 'title', 'post_category_id');
$Adapters['vnp_title_post_category_id_key'] = NodeBase::getNodes_IDKey('post_category', 'title', 'post_category_id');
$ShowFields = array (
  0 => 'title',
  1 => 'url',
  2 => 'parent_id',
  3 => 'post_category_id',
  4 => 'status',
);
$GetNodes = DB::Query('post_category')->Columns($ShowFields);
$GetNodes = $GetNodes->Limit($Search['limit']*1);
$GetNodes = $GetNodes->Order($Search['sortby'], $Search['order']);
$GetNodes = $GetNodes->Where('post_category_id', '>', 0);
if($Search['status'] != 'all')
$GetNodes = $GetNodes->_AND()->Where('status', '=', $Search['status']);
foreach($_RefFilter as $RF => $RFD) {
		if(isset($Search[$RF]) && $Search[$RF] > 0)
		$GetNodes = $GetNodes->_AND()
								->WhereGroupOpen()
									->Where($RF, '=', intval($Search[$RF]))->_OR()
									->Where($RF, 'INCLUDE', $Search[$RF])
								->WhereGroupClose();
}
if(!empty($Search['q']))
$GetNodes = $GetNodes->_AND()
								->WhereGroupOpen()
									->Where($Search['searchby'], 'SEARCH', $Search['q'])->_OR()
									->Where($Search['searchby'], 'LIKE', $Search['q'])
								->WhereGroupClose();
$GetNodes = $GetNodes->Get('post_category_id', Output::Paging());
$v = $this->View('list_nodes');
$v->Assign('Search', $Search);
$v->Assign('SortBy', $SortBy);
$v->Assign('SA', Router::GenerateThisRoute());
$v->Assign('SearchBy', $SearchBy);
$v->Assign('Adapters', $Adapters);

if($GetNodes->num_rows > 0) {
    if(isset($Search[$RF]) && $Search[$RF] > 0){
        $v->Assign('Nodes',$GetNodes->Result);
    }
    else {
        $v->Assign('Nodes', Filter::BuildLevelList($GetNodes->Result,'post_category_id','parent_id'))->Assign('tree',1);
    }
} else Helper::Notify('info', 'There is no nodes found!');
$this->Render($v->Output());
Helper::PageInfo('List nodes for Danh mục bài viết ( Found ' . $GetNodes->num_rows . ' nodes )');
Helper::FeaturedPanel('Add new Danh mục bài viết',BASE_DIR . 'post_category/AddNode', 'plus');