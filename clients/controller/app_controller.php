<?php

class Controller extends BaseController {
	protected $UserLevels = array(
									'1' => array('name' => 'Authorized', 'class' => 'user_authorized'),
									'4' => array('name' => 'Administrtor', 'class' => 'user_admin'),
									'3' => array('name' => 'Super moderator', 'class' => 'user_smod'),
									'2' => array('name' => 'Moderator', 'class' => 'user_mod',),
									'0' => array('name' => 'Pending', 'class' => 'user_pending'),
								);
	protected $permissions_template = array(
				'allow_like' 		=> array(	'type'		=> 'common',
												'value' 	=> 'default',
												'includes' 	=> array(),
												'excludes'	=> array()
											),
				'allow_rating' 		=> array(	'type'		=> 'common',
												'value' 	=> 'default',
												'includes' 	=> array(),
												'excludes'	=> array()
											),
				'allow_comment' 	=> array(	'type'		=> 'common',
												'value' 	=> 'default',
												'includes' 	=> array(),
												'excludes'	=> array()
											),
				'view_permission'	=> array(	'type'		=> 'common',
												'value' 	=> 'default',
												'includes' 	=> array(),
												'excludes'	=> array()
											),
				'review_opts'		=> array(	'type'		=> 'common',
												'value' 	=> 'default',
												'includes' 	=> array(),
												'excludes'	=> array()
											),
				'comment_approve'	=> array(	'type'		=> 'common',
												'value' 	=> 'default',
												'includes' 	=> array(),
												'excludes'	=> array()
											)
			);
	public function __construct() {
	}
	public function Main() {
	}
}


?>