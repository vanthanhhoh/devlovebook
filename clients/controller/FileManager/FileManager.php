<?php

if( !defined('VNP_SYSTEM') && !defined('VNP_APPLICATION') && !defined('ADMIN_AREA') ) die('Access denied!');
define('FILE_BASE', Router::Generate('Controller',array('controller'=> 'FileManager')));
if(!defined('UPLOAD_PATH')) define('UPLOAD_PATH', DATA_PATH . 'uploads' . DIRECTORY_SEPARATOR);
if(!defined('UPLOAD_BASE')) define('UPLOAD_BASE', APPLICATION_DATA_DIR . 'uploads/');

class FileManager extends Controller {
	private $FileID = 0;
	public function __construct()
	{
		Access::RequirePermission(Boot::SMOD_SESSION);
		Access::CheckUserPermission();
		Theme::UseJquery();
		Theme::JsFooter('BASE_DIR', 'var ThumBase = ' . THUMB_BASE . ';', 'inline');
		Boot::library('Upload');
		Helper::State('File manager', FILE_BASE);
		Theme::SetTitle('File Manager');
        if(!file_exists(UPLOAD_PATH)) File::CreateDirectory(UPLOAD_PATH, 755, true);
	}
	public function Main() {
		Theme::AddCssComponent('Forms,InputGroups,Tables,ButtonGroups,ProgressBars,Dropdowns');
		Theme::JqueryPlugin('InputToggle');
		Theme::JsFooter('editor_files', APPLICATION_DATA_DIR . 'static/FileManager/files.js');
		Theme::JsFooter('file_update', 'FileManager.QuickUpdate();', 'inline');
		Helper::State('Files manager', Router::GenerateThisRoute());
		Helper::PageInfo('Medias');
		Helper::FeaturedPanel(lang('Add new') . ' medias',BASE_DIR . 'FileManager/upload', 'plus');

		$Search = array('sortby' 	=> 'media_id',
						'order'		=> 'desc',
						'q'			=> '',
						'searchby'	=> 'media_name',
						'status'	=> 'all',
						'limit'		=> 10,
						'group_id'	=> 0,
						'gal_id'	=> 0);
		$FromForm = Input::Post('Search', array());

		$Search = array_merge($Search, G::$Registry['Params']);
		$Search = array_merge($Search, $FromForm);
		Router::BuildParamsString($Search, true);

		$Search['re_order'] = ($Search['order'] == 'desc') ? 'asc' : 'desc';

		G::$Session->Set('tags_sort', $Search['order']);

		$Files = DB::Query('media');
		$Files = $Files->Limit($Search['limit'])->Order($Search['sortby'], $Search['order']);
		$Files = $Files->Where('media_id', '>', 0);
		if($Search['status'] != 'all')
			$Files = $Files->_AND()->Where('status', '=', $Search['status']);
		if($Search['group_id'] > 0)
			$Files = $Files->_AND()->Where('group_id', '=', $Search['group_id']);
		if($Search['gal_id'] > 0)
			$Files = $Files->_AND()->Where('gal_id', '=', $Search['gal_id']);

		if(!empty($Search['q']))
			$Files = $Files->_AND()
						->WhereGroupOpen()
							->Where($Search['searchby'], 'SEARCH', $Search['q'])->_OR()
							->Where($Search['searchby'], 'LIKE', $Search['q'])
						->WhereGroupClose();
		$Files = $Files->Get('file_name', Output::Paging())->Result;
		$v = $this->View('list_files');
		$v->Assign('Files', $Files);
		$v->Assign('Search', $Search);
		$v->Assign('SA', Router::GenerateThisRoute());
		$v->Assign('UPLOAD_BASE', UPLOAD_BASE);

		$GetGroups = DB::Query('media_groups')->Get('gid');
		if($GetGroups->num_rows > 0) $v->Assign('Groups', Filter::BuildLevelList($GetGroups->Result, 'gid', 'parent_id'));
		else $v->Assign('Groups', array());
		$GetGalleries = DB::Query('media_galleries')->Get('gid');
		if($GetGalleries->num_rows > 0) $v->Assign('Galleries', $GetGalleries->Result);
		else $v->Assign('Galleries', array());

		$this->Render($v->Output());
	}
	public function Action() {
		$action = Input::Post('action');
		$ids = Input::Post('ids');
		$ids = array_filter(explode(',', $ids));
		$ids = array_map('intval', $ids);

		$rt = array('status' => 'not');
		if($action == 'deactive') {
			$Deactive = DB::Query('media')->Where('media_id', 'IN', $ids)->Update(array('status' => 0));
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		if($action == 'active') {
			$Deactive = DB::Query('media')->Where('media_id', 'IN', $ids)->Update(array('status' => 1));
			if($Deactive->status) $rt = array('status' => 'ok', 'items' => $ids);
		}
		if($action == 'delete') {
            $File  = DB::Query('media')->Where('media_id', 'IN', $ids)->Get()->Result;
            $listFile = array();
            foreach($File as $item){
                $listFile[] = $_SERVER['DOCUMENT_ROOT'].$item['media_path'].$item['media_name'];
            }
            $Deactive = DB::Query('media')->Where('media_id', 'IN', $ids)->Delete();
            if($Deactive->status) {
                $rt = array('status' => 'ok', 'items' => $ids);
                foreach($listFile as $deleteFile){
                    unlink($deleteFile);
                }
            };
		}
		echo json_encode($rt);
		die();
	}
	public function QuickUpdate() {
		$rt = array('status' => 'error', 'content' => 'Cannot update medias!');
		$FileDatas = Input::Post('file_datas', '');
		if(!empty($FileDatas)) {
			$FileDatas = json_decode($FileDatas);
			$UpdateOK = 0;
			foreach($FileDatas as $FID => $FileData) {
				$UF = DB::Query('media')->Where('media_id', '=', $FID)->Update($FileData);
				if($UF->status > 0) $UpdateOK++;
			}
			if($UpdateOK = sizeof($FileDatas)) $rt = array('status' => 'success', 'content' => 'Success update medias!');
			else $rt = array('status' => 'warn', 'content' => 'Some medias cannot be updated!');
		}
		echo json_encode($rt);
		die();
	}
	public function list_files()
	{
		$ImageFileDefining = '';
		if(isset(G::$Registry['Params']['field']) && !empty(G::$Registry['Params']['field'])) {
			Theme::JsHeader('SendFileToField', 'var TargetField = "' . G::$Registry['Params']['field'] . '";', 'inline');
			$ImageFileDefining = '/field/' . G::$Registry['Params']['field'];
		}
		if(isset(G::$Registry['Params']['multi_images']) && !empty(G::$Registry['Params']['multi_images'])) {
			Theme::JsHeader('MultiImages', 'var MultiImagesField = "' . G::$Registry['Params']['multi_images'] . '";', 'inline');
			$ImageFileDefining = '/multi_images/' . G::$Registry['Params']['multi_images'];
		}
		Theme::UseJquery();
		Theme::JsFooter('editor_files', APPLICATION_DATA_DIR . 'static/FileManager/files.js');
		Theme::JqueryPlugin('ContextMenu');
		$js = '$(".Media_Item").contextMenu({
		    menuSelector: "#contextMenu",
		    availableCommand: ".Media_Functions",
		    storageObj: FileManager,
		    storageProperty: "MediaHasContextMenu",
		    onContextMenu: function (sltObj,e) {
		        FileManager.SetActive(sltObj,false);
                FileManager.MediaHasContextMenu =  sltObj;
                FileManager.CheckButtonsStatus();
                $("#VNP_MediaTitle").val(sltObj.attr("data-file-title"));
                $("#VNP_MediaAlt").val(sltObj.attr("data-file-alt"));
		    }
		});';
		Theme::JsFooter('contextMenu_ini', $js, 'inline');
		Theme::CssHeader('editor_files', APPLICATION_DATA_DIR . 'static/FileManager/files.css');
		Theme::AddCssComponent('GridSystem,ReponsiveUtilities,Buttons,Pagination,Pager,Glyphicons,Typography,Forms,InputGroups,Tables,ButtonGroups,Navs,Dropdowns');
		Helper::Header('Files manager');
		$Files = DB::Query('media');
		$Files = $Files->Limit(36)->Order('media_id', 'DESC')->Get('media_id', Output::Paging());
		$Files = $Files->Result;
		$File = $this->View('editor_list_files');
		$File->Assign('Files', $Files);
		$File->Assign('UPLOAD_BASE');
		if(defined('ADMIN_AREA'))
			Output::$Paging['base_url'] = BASE_DIR . 'ajax/text/FileManager/list_files' . $ImageFileDefining;
		else
			Output::$Paging['base_url'] = BASE_DIR . 'ajax/text/FileManager/list_files' . $ImageFileDefining;
		Output::$Paging['ext'] = '';
		$File->Assign('Page', Output::Page());
		$File->Assign('TargetField', $ImageFileDefining);

		$this->Render($File->Output());
	}
	public function upload()
	{
		$ImageFileDefining = '';
		if(isset(G::$Registry['Params']['field']) && !empty(G::$Registry['Params']['field'])) {
			Theme::JsHeader('SendFileToField', 'var TargetField = "' . G::$Registry['Params']['field'] . '";', 'inline');
			$ImageFileDefining = '/field/' . G::$Registry['Params']['field'];
		}
		if(isset(G::$Registry['Params']['multi_images']) && !empty(G::$Registry['Params']['multi_images'])) {
			Theme::JsHeader('MultiImages', 'var MultiImagesField = "' . G::$Registry['Params']['multi_images'] . '";', 'inline');
			$ImageFileDefining = 'multi_images/' . G::$Registry['Params']['multi_images'];
		}

		if(isset(G::$Registry['Params']['group_id']) && G::$Registry['Params']['group_id'] > 0){
            $group_id = G::$Registry['Params']['group_id'];
            $group = DB::Query('media_groups')->Where('gid','=',$group_id)->Get()->Result[0];
            $path  = DATA_PATH . 'uploads' . DIRECTORY_SEPARATOR . $group['dir'] . DIRECTORY_SEPARATOR . getYear() . '-' . getMonth() . DIRECTORY_SEPARATOR;
            $base  = APPLICATION_DATA_DIR . 'uploads/' .$group['dir']. '/' . getYear() . '-' . getMonth() . '/';
        }
		else
        {
            $group_id = 0;
            $path = UPLOAD_PATH;
            $base = UPLOAD_BASE;
        }
		if(isset(G::$Registry['Params']['gal_id']) && G::$Registry['Params']['gal_id'] > 0)
			$gal_id = G::$Registry['Params']['gal_id'];
		else $gal_id = 0;

		if(defined('IS_AJAX') && IS_AJAX == 'text') {
			Theme::AddCssComponent('GridSystem,ReponsiveUtilities,Buttons,Pagination,Pager,Glyphicons,Typography,Forms,InputGroups,Tables,ButtonGroups,Navs,ProgressBars');
			Theme::JsFooter('editor_files', APPLICATION_DATA_DIR . 'static/FileManager/files.js');
			Theme::CssHeader('editor_files', APPLICATION_DATA_DIR . 'static/FileManager/files.css');
		}
		else Theme::AddCssComponent('Forms,InputGroups,Tables,ButtonGroups,ProgressBars');
		Helper::State('Upload files', FILE_BASE . '/upload');
		Helper::Header('Drag and drop to upload', 'or click into dropbox to choose your files');
		Theme::SetTitle('Upload files');
		Theme::UseJquery();
		Theme::JsFooter('js_upload', APPLICATION_DATA_DIR . 'static/FileManager/upload.js');
		if(Input::Post('Upload_Form') == 1) {
			$File = Upload::Start($_FILES['VNP_Files']);
			if($File->uploaded) {
				$UploadResult = array('status' => 'not', 'data' => '');
                $File->file_new_name_body=Filter::CleanUrlString($_FILES['VNP_Files']['name']);
				$File->process($path);
				if($File->processed) {
					$MediaInfo = array(	'media_name'	=> $File->file_dst_name,
										'media_path'	=> $base,
										'media_type'	=> $File->file_is_image ? 'image' : '',
										'media_mime'	=> $File->file_src_mime,
										'media_size'	=> $File->file_src_size,
										'media_ext'		=> $File->file_dst_name_ext,
										'group_id'		=> $group_id,
										'gal_id'		=> $gal_id,
										'uploaded_time'	=> time(),
										'user_id'		=> USER_ID
									);
					$_I = DB::Query('media')->Insert($MediaInfo);
					$MediaInfo['media_id'] = $_I->insert_id;
					$MediaInfo['media_location'] = 'data/uploads/' . $MediaInfo['media_name'];
					$File->clean();
					if(defined('IS_AJAX')) $UploadResult = array('status' => 'ok', 'data' => $MediaInfo);
					else Helper::Notify('success', 'Upload success ' . $MediaInfo['media_name']);
				}
				else {
					if(defined('IS_AJAX')) $UploadResult = array('status' => 'not', 'data' => $File->error);
					else Helper::Notify('error',$File->error);
				}
			}
			else {
				if(defined('IS_AJAX')) $UploadResult = array('status' => 'not', 'data' => $File->error);
				else Helper::Notify('error',$File->error);Helper::Notify('error',$File->error);
			}
			if(defined('IS_AJAX')) {
				header('Content-Type: application/json');
				echo json_encode($UploadResult);
				exit();
			}
		}
		else {
			if(defined('IS_AJAX') && IS_AJAX == 'text')	$TempFile = 'editor_upload';
			else $TempFile = 'upload';
			$v = $this->View($TempFile);
			$v->Assign('TargetField', $ImageFileDefining);
			$GetGroups = DB::Query('media_groups')->Get('gid');
			if($GetGroups->num_rows > 0) $v->Assign('Groups', Filter::BuildLevelList($GetGroups->Result, 'gid', 'parent_id'));
			else $v->Assign('Groups', array());
			$GetGalleries = DB::Query('media_galleries')->Get('gid');
			if($GetGalleries->num_rows > 0) $v->Assign('Galleries', $GetGalleries->Result);
			else $v->Assign('Galleries', array());
			$v->Assign('gal_id', $gal_id);
			$v->Assign('group_id', $group_id);
			$this->Render($v->Output());
		}
	}
	public function manage()
	{
		$AllowedActions = array('edit','remove','remove_group','remove_gallery');
		$ActionKeys = array_keys(G::$Registry['Params']);
		if(isset($ActionKeys[0]) && in_array($ActionKeys[0], $AllowedActions)) {
			$this->$ActionKeys[0](G::$Registry['Params']);
		}
	}
	private function edit($Params)
	{
		$this->FileID = G::$Registry['Params']['edit'];
		Helper::State('Edit file', FILE_BASE . '/manage/edit/' . $this->FileID . '/');

		$GetFile = DB::Query('media')->Where('media_id', '=', $this->FileID)->Get();
		if($GetFile->num_rows == 1)
		{
			$File = $GetFile->Result[0];
			if(Input::Post('FileID') == $this->FileID)
			{
				$_File = Input::Post('File');
				$_U = DB::Query('media')->Where('media_id', '=', $this->FileID)
										->Update($_File);
				if($_U->status)
				{
					$File = array_merge($File, $_File);
					Helper::Notify('success', 'Update ok!');
				}
				else Helper::Notify('errot', 'Update failed!');
			}
			Theme::AddCssComponent('Forms,InputGroups,Tables,ButtonGroups');
			Helper::Header('Edit file ' . $File['media_name']);
			Theme::SetTitle('Edit file ' . $File['media_name']);
			$Edit = $this->View('edit_file');
			$Edit->Assign('File', $File);
			$this->Render($Edit->Output());
		}
		else Helper::Notify('error', 'File not found!');
	}

    private function remove() {
        $FileID = G::$Registry['Params']['remove'];
        $ShowConfirmForm = false;
        Helper::State('Remove File', FILE_BASE . '/manage/remove/' . $FileID);
        Theme::AddCssComponent('Forms,Buttons,Panels');

        $GetFile = DB::Query('media')->Where('media_id', '=', $FileID)->Get();
        if($GetFile->num_rows == 1) {
            $File = $GetFile->Result[0];
            $link = $_SERVER['DOCUMENT_ROOT'].$File['media_path'].$File['media_name'];
            if(Input::Post('FileID') == $FileID) {
                $RemoveFile = DB::Query('media')->Where('media_id', '=', $FileID)->Delete();

                if($RemoveFile->status && $RemoveFile->affected_rows == 1) {
                    Helper::Notify('success', 'Success, Redirecting to file list...');
                    if (!unlink($link))
                    {
                        Helper::Notify('error','File không tồn tại');
                    }
                    else
                    {
                        Helper::Notify('success','Xóa file trên ổ đĩa');
                    }
                    header('Refresh: 0.5; url=' . FILE_BASE);
                }
                else {
                    Helper::Notify('error', 'Cannot remove this file!');
                    $ShowConfirmForm = true;
                }
            }
            else $ShowConfirmForm = true;

            if($ShowConfirmForm) {
                $FormAction = FILE_BASE . '/manage/remove/' . $FileID;
                $ConfirmString = '
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Confirm delete this file?</h3>
					</div>
					<div class="panel-body">
						<form method="post" action="' . $FormAction . '">
							<input type="hidden" value="' . $FileID . '" name="FileID" />
							<a href="javascript:window.history.go(-1); return false;" class="btn btn-default">Cancel</a>&nbsp;&nbsp;
							<input type="submit" class="btn btn-danger" value="Confirm" />
						</form>
					</div>
				</div>';
                $this->Render($ConfirmString);
                Helper::Header('Remove file: ' . $File['media_name']);
                Theme::SetTitle('Remove file ' . $File['media_name']);
            }
        }
        else Helper::Notify('error', 'File not found!');
    }

	private function remove_group() {
		Theme::AddCssComponent('Forms,Buttons,Panels');

		if(isset(G::$Registry['Params']['remove_group'])) {
			$GID = G::$Registry['Params']['remove_group'];
			Helper::State('Remove group', FILE_BASE . '/manage/remove_group/' . $GID);
			$GetNode = DB::Query('media_groups')->Where('gid', '=', $GID)->Get();
			if($GetNode->num_rows == 0)
				return Helper::Notify('error', 'Group not found');
		}
		else return Header('Location: ' . Router::Generate('ControllerAction', array('controller' => 'FileManager', 'action' => 'Group')));

		if(Input::Post('RemoveGroupSubmit') == 1) {
			$DeleteNode = DB::Query('media_groups')->Where('gid', '=', $GID)->Delete();
			if($DeleteNode->affected_rows == 1) {
				//File::RemoveDirectory(UPLOAD_PATH . $GetNode->Result[0]['dir']);
				Helper::Notify('success', 'Success delete group ' .  $GetNode->Result[0]['name']);
				Header('Refresh: 1.5; url=' . Router::Generate('ControllerAction', array('controller' => 'FileManager', 'action' => 'Group')));
			}
			else Helper::Notify('error', 'Error delete group ' .  $GetNode->Result[0]['name']);
		}
		else {
			$config = array('action'	=> Router::GenerateThisRoute(),
							'tokens'	=> array(array('name' => 'NodeID', 'value' => $GID), array('name' => 'RemoveGroupSubmit', 'value' => 1))
							);
			$v = Access::Confirm('Confirm remove group: ' . $GetNode->Result[0]['name'], $config);
			$this->Render($v);
		}
	}

	private function remove_gallery() {
		Theme::AddCssComponent('Forms,Buttons,Panels');

		if(isset(G::$Registry['Params']['remove_gallery'])) {
			$GID = G::$Registry['Params']['remove_gallery'];
			Helper::State('Remove group', FILE_BASE . '/manage/remove_gallery/' . $GID);
			$GetNode = DB::Query('media_galleries')->Where('gid', '=', $GID)->Get();
			if($GetNode->num_rows == 0)
				return Helper::Notify('error', 'Gallery not found');
		}
		else return Header('Location: ' . Router::Generate('ControllerAction', array('controller' => 'FileManager', 'action' => 'Gallery')));

		if(Input::Post('RemoveGallerySubmit') == 1) {
			$DeleteNode = DB::Query('media_galleries')->Where('gid', '=', $GID)->Delete();
			if($DeleteNode->affected_rows == 1) {
				//File::RemoveDirectory(UPLOAD_PATH . $GetNode->Result[0]['dir']);
				Helper::Notify('success', 'Success delete gallery ' .  $GetNode->Result[0]['name']);
				Header('Refresh: 1.5; url=' . Router::Generate('ControllerAction', array('controller' => 'FileManager', 'action' => 'Gallery')));
			}
			else Helper::Notify('error', 'Error delete gallery ' .  $GetNode->Result[0]['name']);
		}
		else {
			$config = array('action'	=> Router::GenerateThisRoute(),
							'tokens'	=> array(array('name' => 'NodeID', 'value' => $GID), array('name' => 'RemoveGallerySubmit', 'value' => 1))
							);
			$v = Access::Confirm('Confirm remove gallery: ' . $GetNode->Result[0]['name'], $config);
			$this->Render($v);
		}
	}

	public function Group() {
		$GID = 0;
		if(isset(G::$Registry['Params'][ROUTER_EXTRA_KEY])) $GID = G::$Registry['Params'][ROUTER_EXTRA_KEY];

		$Group = array('name' => '', 'parent_id' => 0, 'description' => '', 'dir' => '');

		if($GID > 0) {
			$GetGroup = DB::Query('media_groups')->Where('gid', '=', $GID)->Get();
			if($GetGroup->status && $GetGroup->num_rows == 1) {
				$Group = $GetGroup->Result[0];
				Helper::Notify('info', 'Editing group ' . $Group['name']);
			}
			else {
				Helper::Notify('error', 'Group not found!');
				return false;
			}
		}
		if(Input::Post('SaveGroupSubmit') == 1) {
			$GID = Input::Post('GID', 0);
			$Group = Input::Post('group');
			if($Group['name'] == '') Helper::Notify('error', 'Group name is empty!');
			else {
				if($Group['dir'] == '') $Group['dir'] = $Group['name'];
			}
			$Group['dir'] = Filter::CleanUrlString($Group['dir']);
			if($Group['dir'] == '') Helper::Notify('error', 'Group dir is empty!');
			if(Helper::NotifyCount('error') == 0) {
				File::CreateDirectory(UPLOAD_PATH . $Group['dir']);
				if($GID == 0) {
					$AddGroup = DB::Query('media_groups')->Insert($Group);
					if($AddGroup->status && $AddGroup->insert_id > 0) {
						Helper::Notify('success', 'Successful add group!');
					}
					else Helper::Notify('error', 'Cannot add group, check if group existed!');
				}
				else {
					$AddGroup = DB::Query('media_groups')->Where('gid', '=', $GID)->Update($Group);
					if($AddGroup->status) {
						($AddGroup->affected_rows > 0) ?
						Helper::Notify('success', 'Successful update group!') :
						Helper::Notify('info', 'Nothing was changed!');
					}
					else Helper::Notify('error', 'Cannot update group, please try again later!');
				}
			}
		}
		Helper::PageInfo('List media groups');
		Helper::State('List media groups', Router::Generate('ControllerAction', array('controller' => 'FileManager', 'action' => 'Group')));
		$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups,InputGroups,Tables');
		$v = $this->View('list_groups');
		$v->Assign('GID', $GID);
		$v->Assign('Group', $Group);

		$GetGroups = DB::Query('media_groups')->Get('gid');
		if($GetGroups->num_rows > 0) $v->Assign('Groups', Filter::BuildLevelList($GetGroups->Result, 'gid', 'parent_id'));
		else {
			$v->Assign('Groups', array());
			Helper::Notify('info', 'There is no groups found!');
		}
		$this->Render($v->Output());
	}

	public function Gallery() {
		$GID = 0;
		if(isset(G::$Registry['Params'][ROUTER_EXTRA_KEY])) $GID = G::$Registry['Params'][ROUTER_EXTRA_KEY];

		$Gallery = array('name' => '', 'description' => '', 'dir' => '', 'content' => '');

		if($GID > 0) {
			$GetGallery = DB::Query('media_galleries')->Where('gid', '=', $GID)->Get();
			if($GetGallery->status && $GetGallery->num_rows == 1) {
				$Gallery = $GetGallery->Result[0];
				Helper::Notify('info', 'Editing gallery ' . $Gallery['name']);
			}
			else {
				Helper::Notify('error', 'Gallery not found!');
				return false;
			}
		}
		if(Input::Post('SaveGallerySubmit') == 1) {
			$GID = Input::Post('GID', 0);
			$Gallery = Input::Post('Gallery');
			if($Gallery['name'] == '') Helper::Notify('error', 'Gallery name is empty!');
			else {
				if($Gallery['dir'] == '') $Gallery['dir'] = $Gallery['name'];
			}
			$Gallery['dir'] = Filter::CleanUrlString($Gallery['dir']);
			if($Gallery['dir'] == '') Helper::Notify('error', 'Gallery dir is empty!');
			if(Helper::NotifyCount('error') == 0) {
				File::CreateDirectory(UPLOAD_PATH . DIRECTORY_SEPARATOR . 'gallery' . DIRECTORY_SEPARATOR . $Gallery['dir']);
				if($GID == 0) {
					$AddGallery = DB::Query('media_galleries')->Insert($Gallery);
					if($AddGallery->status && $AddGallery->insert_id > 0) {
						Helper::Notify('success', 'Successful add gallery!');
					}
					else Helper::Notify('error', 'Cannot add gallery, check if gallery existed!');
				}
				else {
					$AddGallery = DB::Query('media_galleries')->Where('gid', '=', $GID)->Update($Gallery);
					if($AddGallery->status) {
						($AddGallery->affected_rows > 0) ?
						Helper::Notify('success', 'Successful update gallery!') :
						Helper::Notify('info', 'Nothing was changed!');
					}
					else Helper::Notify('error', 'Cannot update gallery, please try again later!');
				}
			}
		}
		Helper::PageInfo('List media galleries');
		Helper::State('List media galleries', Router::Generate('ControllerAction', array('controller' => 'FileManager', 'action' => 'Gallery')));
		$this->UseCssComponents('Glyphicons,Buttons,Labels,InputGroups,InputGroups,Tables');
		$v = $this->View('list_galleries');
		$v->Assign('GID', $GID);
		$v->Assign('Gallery', $Gallery);

		$GetGalleries = DB::Query('media_galleries')->Get('gid');
		if($GetGalleries->num_rows > 0) $v->Assign('Galleries', $GetGalleries->Result);
		else {
			$v->Assign('Galleries', array());
			Helper::Notify('info', 'There is no galleries found!');
		}
		$this->Render($v->Output());
	}

	public function Settings() {
	}

	public function Help() {
	}
}
?>