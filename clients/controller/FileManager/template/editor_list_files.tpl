<div id="Files_Container">
	<div id="File_Actions">
        <ul class="nav nav-pills">
        	<li class="disabled" id="AddToEditor"><a href=""><span class="glyphicon glyphicon-plus"></span>&nbsp;Add to editor</a></li>
            <!--
            <li class="disabled" id="EditFile"><a href="" target="_blank"><span class="glyphicon glyphicon-pencil"></span>&nbsp{{lang::edit}}</a></li>
            <li class="disabled" id="DeleteFile"><a href="" target="_blank"><span class="glyphicon glyphicon-remove"></span>&nbsp{{lang::delete}}</a></li>
            <li class="disabled" id="OpenImageEditor"><a href="" target="_blank"><span class="glyphicon glyphicon-picture"></span>&nbsp;Open in image editor</a></li>
            <li class="disabled"><a href="#">File manager featured</a></li>
            -->
            <li><a href="{#BASE_DIR}ajax/text/FileManager/upload{$TargetField}"><span class="glyphicon glyphicon-upload"></span>&nbsp;Open uploader</a></li>
            <li><a href="#" title="Refresh" onclick="window.location=window.location;return false;"><span class="glyphicon glyphicon-refresh"></span>&nbsp;Refresh</a></li>
      	</ul>
        {$Page}
    </div>
    <div class="Main_Files" id="Main_Files">
        {for $File in $Files}
        <div class="Media_Item" id="Media_{$File.media_id}" data-selected="0" data-file-id="{$File.media_id}" data-file-location="{$File.media_path}{$File.media_name}" data-file-alt="{$File.media_alt}" data-file-title="{$File.media_title}">
            <div class="File_Thumbnail">
                <img class="media-object" src="{#THUMB_BASE}68_68{$File.media_path}{$File.media_name}" />
            </div>
            <div class="File_Name">{$File.media_name}</div>
        </div>
        {/for}
    </div>
</div>
<ul id="contextMenu" class="dropdown-menu" role="menu" style="display:none; width: 220px">
    <li><a tabindex="-1" href="#" class="Media_Functions" data-callback="FileManager_SendFile"><span class="glyphicon glyphicon-plus"></span>  Select this media</a></li>
    <li><a tabindex="-1" href="#" class="Media_Functions" data-callback="FileManager_ViewFile"><span class="glyphicon glyphicon-download-alt"></span>  View/Download this media</a></li>
    <li><a tabindex="-1" href="#" class="Media_Functions" data-callback="FileManager_FullEdit"><span class="glyphicon glyphicon-pencil"></span>  Full info edit</a></li>
    <li><a tabindex="-1" href="#" class="Media_Functions" data-callback="FileManager_ImageEditor"><span class="glyphicon glyphicon-picture"></span>  Open in file editor</a></li>
    <li><a tabindex="-1" href="#" class="Media_Functions" data-callback="FileManager_RemoveMedia"><span class="glyphicon glyphicon-remove"></span>  Delete this media</a></li>
    <li class="divider"></li>
    <li class="clearfix">
        <div class="QuickEditInput"><lable class="fl" style="width:35px; line-height:26px">Title</lable><input id="VNP_MediaTitle" type="text" class="form-control" style="width:160px; height: 26px; line-height: 16px;"/></div>
    </li>
    <li class="clearfix">
        <div class="QuickEditInput" style="margin-bottom:0"><lable class="fl" style="width:35px; line-height:26px">Alt</lable><input id="VNP_MediaAlt" type="text" class="form-control" style="width:160px; height: 26px; line-height: 16px;"/></div>
    </li>
    <li class="divider"></li>
    <li class="clearfix">
        <div class="QuickEditInput"><button data-callback="FileManager_UpdateMedia" style="width:100%" class="btn btn-primary Media_Functions">Save</button></div>
    </li>
</ul>
<style type="text/css">
#contextMenu .QuickEditInput {padding: 0 10px; margin-bottom: 5px}
#contextMenu li a span {margin-right: 8px; color: #888}
</style>