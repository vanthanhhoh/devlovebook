<div class="table-responsive">
    <form class="form-horizontal" action="{$SA}" method="post">
        <input type="hidden" name="SearchUsers" value="1">
        <table class="table table-bordered table-striped table-hover">
            <colgroup>
                <col class="col-xs-2">
                <col class="col-xs-1">
                <col class="col-xs-2">
                <col class="col-xs-1">
                <col class="col-xs-2">
                <col class="col-xs-1">
                <col class="col-xs-1">
                <col class="col-xs-1">
            </colgroup>
            <tbody>
                <tr>
                    <td><label class="control-label">{{lang::search_in}}</label></td>
                    <td><label class="control-label">Group</label></td>
                    <td>
                        <select name="Search[group_id]" class="form-control">
                            <option value="0">None</option>
                            {for $G in $Groups}
                            <option value="{$G.gid}"{if($Search.group_id == $G.gid)} selected{/if}>{$G.prefix}{$G.name}</option>
                            {/for}
                        </select>
                    </td>
                    <td><label class="control-label">Gallery</label></td>
                    <td>
                        <select name="Search[gal_id]" class="form-control">
                            <option value="0">None</option>
                            {for $G in $Galleries}
                            <option value="{$G.gid}"{if($Search.gal_id == $G.gid)} selected{/if}>{$G.name}</option>
                            {/for}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><input type="text" name="Search[q]" placeholder="Search keyword" class="form-control" value="{$Search.q}" /></td>
                    <td><label class="control-label">{{lang::search_by}}</label></td>
                    <td>
                        {function}$SearchBy = array('media_name' => 'Name','media_ext' => 'Extension','media_title' => 'Title', 'media_alt' => 'Alt', 'media_caption' => 'Caption'){/function}
                        <select class="form-control" name="Search[searchby]">
                            {for $SB in $SearchBy as $SK}
                            <option value="{$SK}"{if($SK == $Search.searchby)} selected{/if}>{$SB}</option>
                            {/for}
                        </select>
                    </td>
                    <td><label class="control-label">Status</label></td>
                    <td>
                        <select class="form-control" name="Search[status]">
                            <option value="all"{if($Search.status == 'all')} selected{/if}>All</option>
                            <option value="1"{if($Search.status == '1')} selected{/if}>Active</option>
                            <option value="0"{if($Search.status == '0')} selected{/if}>Inactive</option>
                        </select>
                    </td>
                    <td><label class="control-label">Show</label></td>
                    <td><input type="number" class="form-control" value="{$Search.limit}" name="Search[limit]" /></td>
                    <td><input type="submit" class="btn btn-primary" value="Search" /></td>
                </tr>
            </tbody>
        </table>
    </form>

    <div class="btn-group" style="margin-bottom: 5px">
        <div class="btn-group">
            <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                {{lang::action_btn}}
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#" id="VNP_DeactiveFile"><span class="glyphicon glyphicon-remove"></span>&nbsp&nbsp{{lang::deactive}}</a></li>
                <li><a href="#" id="VNP_ActiveFile"><span class="glyphicon glyphicon-ok"></span>&nbsp&nbsp{{lang::active}}</a></li>
                <li><a href="#" id="VNP_DeleteFile"><span class="glyphicon glyphicon-trash"></span>&nbsp&nbsp{{lang::delete}}</a></li>
            </ul>
        </div>
        <button type="button" class="btn btn-success" onclick="javascript:window.location=window.location">Reload</button>
        <button type="button" id="VNP_SaveFile" class="btn btn-primary">Save</button>
        <div class="VNP_PageTop">{function}echo Output::Page(){/function}</div>
    </div>
    <form action="" method="post" name="UpdateListMedias" class="FileList">
        <table class="table table-bordered table-striped table-hover">
            <colgroup>
            <col class="col-xs">
            <col class="col-xs">
            <col class="col-xs">
            <col class="col-xs-1">
            <col class="col-xs-4">
            <col class="col-xs-4">
            <col class="col-xs">
            <col class="col-xs">
            <col class="col-xs-2">
            </colgroup>
            <thead>
                <tr>
                    <td>
                        <div class="checkbox checkbox-success">
                            <input id="toggle-all" value="1" type="checkbox" />
                            <label for="toggle-all"></label>
                        </div>
                    </td>
                    <td><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'FileManager', 'action' => 'Main', 'params' => 'sortby/media_id/order/' . $Search['re_order'])){/function}" >ID</a></td>
                    <th>View</th>
                    <td><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'FileManager', 'action' => 'Main', 'params' => 'sortby/media_name/order/' . $Search['re_order'])){/function}" ><strong>Media name</strong></a></td>
                    <td><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'FileManager', 'action' => 'Main', 'params' => 'sortby/media_title/order/' . $Search['re_order'])){/function}" ><strong>Media title</strong></a></td>
                    <td><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'FileManager', 'action' => 'Main', 'params' => 'sortby/media_alt/order/' . $Search['re_order'])){/function}" ><strong>Media alt</strong></a></td>
                    <td><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'FileManager', 'action' => 'Main', 'params' => 'sortby/media_mime/order/' . $Search['re_order'])){/function}" ><strong>Type</strong></a></td>
                    <td><a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'FileManager', 'action' => 'Main', 'params' => 'sortby/uploaded_time/order/' . $Search['re_order'])){/function}" ><strong>Time</strong></a></td>
                    <th>Featured</th>
                </tr>
            </thead>
            <tbody>
            	{for $File in $Files}
                <tr class="{if($File.status == 0)}Node_Inactive{else}Node_Active{/if}" id="media-{$File.media_id}">
                    <td>
                        <div class="checkbox checkbox-success">
                            <input type="checkbox" id="check_node-{$File.media_id}" value="{$File.media_id}" data-title="{$File.media_id} - {$File.media_name}" class="item-toggle" />
                            <label for="check_node-{$File.media_id}"></label>
                        </div>
                    </td>
                    <td>{$File.media_id}</td>
                	<td><img class="media-object" src="{#THUMB_BASE}50_50{$File.media_path}{$File.media_name}" width="50" height="50" /></td>
                	<td><a href="{$File.media_path}/{$File.media_name}" title="{$File.media_name}" alt="{$File.media_name}" target="_blank">{function}echo Filter::SubString($File['media_name'],20){/function}</a></td>
                    <td><input class="form-control QuickUpdateFile media_title" data-field="media_title" data-media="{$File.media_id}" type="text" name="Media[{$File.media_id}][media_title]" value="{$File.media_title}" /></td>
                    <td><input class="form-control QuickUpdateFile media_alt" data-field="media_alt" data-media="{$File.media_id}" type="text" name="Media[{$File.media_id}][media_alt]" value="{$File.media_alt}" /></td>
                    <td>{$File.media_mime}</td>
                    <td>{$File.uploaded_time|Filter::UnixTimeToDate:true}</td>
                    <td>
                    	<span class="glyphicon glyphicon-pencil"></span>&nbsp;
                        <a href="{#FILE_BASE}/manage/edit/{$File.media_id}">Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;
                        <span class="glyphicon glyphicon-remove"></span>&nbsp;
                        <a href="{#FILE_BASE}/manage/remove/{$File.media_id}">Remove</a>
                    </td>
                </tr>
                {/for}
          	</tbody>
      	</table>
    </form>
</div>
<style type="text/css">.FileList{font-size: 12px}.FileList input[type="text"]{background:#F3F3F3}.FileList input[type="text"]:focus{background:#FFF}</style>
<script type="text/javascript">
$(document).ready(function() {
    var checkedInputs = '';
    var titleData = '';
    $('#toggle-all').InputToggle({
        childInput: '.item-toggle',
        dataAttribute: 'data-title',
        storageVar: 'checkedInputs',
        titleData:  'titleData',
        featureAction: [
            {container: '#VNP_DeactiveFile', callback: "FileManager.DeactiveFile(checkedInputs)" },
            {container: '#VNP_ActiveFile', callback: "FileManager.ActiveFile(checkedInputs)" },
            {container: '#VNP_DeleteFile', callback: "FileManager.DeleteFile(checkedInputs)" },
            {container: '#VNP_SaveFile', callback: "FileManager.SaveFileUpdate()" }
        ],
        enableCookie: true
    });
});
</script>