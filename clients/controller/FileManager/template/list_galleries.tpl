<div class="table-responsive">
	<form class="form-horizontal" action="" method="post">
		<input type="hidden" name="SaveGallerySubmit" value="1">
		<input type="hidden" name="GID" value="{$GID}">
		<table class="table table-bordered table-striped table-hover">
	        <colGallery>
	        	<col class="col-xs">
	            <col class="col-xs-2">
	            <col class="col-xs-3">
	            <col class="col-xs-2">
	            <col class="col-xs-4">
	        </colGallery>
	        <tbody>
	        	<tr>
	        		<td rowspan="3">
	        			<input type="submit" name="SaveGallery" class="btn btn-primary" value="Save" />
	        		</td>
	        	</tr>
	        	<tr>
	        		<td>Gallery Name</td>
	        		<td>
	        			<input type="text" name="Gallery[name]" class="form-control" value="{$Gallery.name}" />
					</td>
					<td>Dir name</td>
	        		<td>
	        			<input type="text" name="Gallery[dir]" class="form-control" value="{$Gallery.dir}" />
					</td>
				</tr>
				<tr>
	        		<td>Gallery Description</td>
	        		<td colspan="5">
	        			<textarea name="Gallery[description]" class="form-control">{$Gallery.description}</textarea>
					</td>
				</tr>
			</tbody>
		</table>
	</table>
	{if(!empty($Galleries))}
    <table class="table table-bordered table-striped table-hover">
        <colGallery>
        	<col class="col-xs">
            <col class="col-xs">
        	<col class="col-xs">
        </colGallery>
        <thead>
	        <tr>
	        	<td><strong>Gallery name</strong></td>
	        	<td><strong>Description</strong></td>
	        	<td><strong>Functions</strong></td>
	        </tr>
	    </thead>
	    <tbody>
	    	{for $Gallery in $Galleries as $GID}
	    	<tr>
	    		<td>{$Gallery.name}</td>
	    		<td>{$Gallery.description}</td>
	    		<td>
                	<a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'FileManager', 'action' => 'Gallery', 'params' => $Gallery['gid'])){/function}"><span class="glyphicon glyphicon-edit"></span>&nbsp{{lang::edit}}</a>&nbsp;&nbsp;
                    <a href="{function}echo Router::Generate('ControllerParams', array('controller' => 'FileManager', 'action' => 'manage', 'params' => 'remove_gallery/' . $Gallery['gid'])){/function}"><span class="glyphicon glyphicon-trash"></span>&nbsp{{lang::delete}}</a>
              	</td>
	    	</tr>
	    	{/for}
	    </tbody>
    </table>
    {/if}
</div>