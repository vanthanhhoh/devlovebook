<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover">
        <colgroup>
            <col class="col-xs-2">
            <col class="col-xs-2">
            <col class="col-xs-2">
            <col class="col-xs-2">
        </colgroup>
        <tbody>
            <tr>
                <td><label class="control-label">Group</label></td>
                <td>
                    <select id="VNP_UploadGroup" class="form-control">
                        <option value="0">None</option>
                        {for $G in $Groups}
                        <option value="{$G.gid}"{if($G.gid == $group_id)} selected{/if}>{$G.prefix}{$G.name}</option>
                        {/for}
                    </select>
                </td>
                <td><label class="control-label">Gallery</label></td>
                <td>
                    <select id="VNP_UploadGallery" class="form-control">
                        <option value="0">None</option>
                        {for $G in $Galleries}
                        <option value="{$G.gid}"{if($G.gid == $gal_id)} selected{/if}>{$G.name}</option>
                        {/for}
                    </select>
                </td>
                <td><button class="btn btn-primary" id="VNP_SetUploadParams">Set upload group & gallery</button></td>
            </tr>
        </tbody>
    </table>
</div>
<div id="File_Uploader">
    <form action="{#FILE_BASE}/upload/group_id/{$group_id}/gal_id/{$gal_id}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="Upload_Form" value="1" />
        <input name="VNP_Files" id="Files_Input" type="file" />
        <div id="Files_Holder"></div>
        <button type="submit" name="Upload_Submiter" id="Start_Upload" class="btn btn-primary">
            <span class="glyphicon glyphicon-upload"></span>&nbsp;&nbsp;Upload
        </button>
        <button name="Pause_Resume" id="Pause_Resume" class="btn btn-warning">
            <span class="glyphicon glyphicon-pause"></span>&nbsp;&nbsp;Pause
        </button>
        <button name="Clear_Form" id="Clear_Form" class="btn btn-info">
            <span class="glyphicon glyphicon-refresh"></span>&nbsp;&nbsp;Reset
        </button>
        <button name="Cancel_Upload" id="Cancel_Upload" class="btn btn-danger">
            <span class="glyphicon glyphicon-ban-circle"></span>&nbsp;&nbsp;Cancel
        </button>
        <button name="Debug" id="Debug" class="btn btn-default">
            Debug
        </button>
    </form>
</div>
<div id="Upload_Progress" class="clearfix">
</div>
<style type="text/css">
.Single_Upload {
	cursor: pointer
}
</style>
<script type="text/javascript">var UploadExtraParams = '/group_id/{$group_id}/gal_id/{$gal_id}'; var ajaxFrame = false;var BaseField = '{$TargetField}';</script>