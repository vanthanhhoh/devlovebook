<?php

if( !defined('VNP_SYSTEM') && !defined('VNP_APPLICATION') ) die('Access denied!');
Access::RequirePermission(Boot::SMOD_SESSION);

class nTheme extends Controller {
    public function __construct() {}

    private function getWorkingPage() {
        $refUrl = Request::getEnv('HTTP_REFERER');
        $pageUri = Request::getEnv('REQUEST_SCHEME') . '://' . Request::getEnv('SERVER_NAME') . rtrim(BASE_DIR, '/');

        return substr($refUrl, strlen($pageUri) - strlen($refUrl));
    }

    public function enableDesign() {
        $workingPage = $this->getWorkingPage();
        if(Input::Post('switchMod') != '') {
            if(G::$Session->Get('designMode') == 1) {
                G::$Session->Set('designMode', 0);
            }
            else {
                G::$Session->Set('designMode', 1);
                $blockOrders = G::$Session->Get('design_blockOrders', '');
                if(empty($blockOrders)) {
                    G::$Session->Set('design_blockOrders', array());
                }
            }
            header('LOCATION: ' . Input::Post('currentState'));
        }
        if(Input::Post('discardChanged') != '') {
            //$workingPage = $this->getWorkingPage();
            //$blockOrders = G::$Session->Get('design_blockOrders', array());
            //if(isset($blockOrders[$workingPage])) unset($blockOrders[$workingPage]);
            //G::$Session->Set('design_blockOrders', $blockOrders);
            G::$Session->Set('design_blockOrders', array());
            header('LOCATION: ' . Input::Post('currentState'));
        }
        if(Input::Post('removePageDesign') != '') {
            $blockOrders = G::$Session->Get('design_blockOrders', array());
            if(isset($blockOrders[$workingPage])) unset($blockOrders[$workingPage]);
            G::$Session->Set('design_blockOrders', $blockOrders);
            header('LOCATION: ' . Input::Post('currentState'));
        }
        if(Input::Post('saveChanged') != '') {
            $blockOrders = G::$Session->Get('design_blockOrders', array());
            $currentOrders = unserialize(Option::get('blocks_order'));
            $currentOrders[$workingPage] = $blockOrders[$workingPage];
            Option::set('blocks_order', serialize($currentOrders), '', 'theme', CURRENT_THEME);
            //G::$Session->Set('design_blockOrders', array());
            header('LOCATION: ' . Input::Post('currentState'));
        }
        if(Input::Post('saveDesignOptions') != '') {
            $blockOrders = G::$Session->Get('design_blockOrders', array());
            $currentOrders = unserialize(Option::get('blocks_order'));
            if(Input::Post('saveForThisPage') == 'thisPage')
                $currentOrders[$workingPage] = $blockOrders[$workingPage];
            if(Input::Post('saveForThisRoute') == 'thisRoute') {
                $matches = Router::Match(RECOMPILE_ROUTE, $workingPage);
                if(!empty($matches) && isset($matches['name']))
                    $currentOrders[$matches['name']] = $blockOrders[$workingPage];
            }
            Option::set('blocks_order', serialize($currentOrders), '', 'theme', CURRENT_THEME);
            header('LOCATION: ' . Input::Post('currentState'));
        }
    }

    public function addBlock() {
        if(isset(G::$Params[ROUTER_EXTRA_KEY])) {
            $blockFile = G::$Params[ROUTER_EXTRA_KEY];
            echo $blockFile;
        }
        die();
    }

    public function editBlock() {

        /**** Prepare block config ****/
        /**** add css components for alert ****/
        $this->UseCssComponents('Alerts');
        /**** prepare variables: check valid block, check if "save and close" button is pressed ****/
        $check = false;
        $saveAndClose = $isSaved = $isSuccessedAddBlock = 'false';
        $blockArea = '';
        /**** init return data for "design" section ****/
        $returnBlockData = array('bid' => 0, 'content' => '');

        if(!empty(G::$Params) && !isset(G::$Params[ROUTER_EXTRA_KEY])) {
            $configParams = G::$Params;
            $paramsKeys = array_keys($configParams);
            G::$Params[ROUTER_EXTRA_KEY] = $paramsKeys[0];
            if(is_numeric($paramsKeys[0])) {
                $isSaved = $isSuccessedAddBlock = 'true';
                if(G::$Params[$paramsKeys[0]] == 'addAndClose')
                    $saveAndClose = 'true';
                Helper::Notify('success', 'Success add block!');
            }
            else {
                $blockArea = G::$Params[$paramsKeys[0]];
            }

            unset(G::$Params[$paramsKeys[0]]);
        }
        if(isset(G::$Params[ROUTER_EXTRA_KEY]) && !empty(G::$Params[ROUTER_EXTRA_KEY])) {
            $BIDorBlock = G::$Params[ROUTER_EXTRA_KEY];
            if(is_numeric($BIDorBlock)) {
                $BID = (int)$BIDorBlock;
                $getBlocks = DB::Query('blocks')->Where('bid', '=', $BID)->Get();
                if($getBlocks->status && $getBlocks->num_rows == 1) {
                    $check = true;
                    $block = $getBlocks->Result[0];
                    $blockParams = unserialize($block['params']);
                    $returnBlockData['bid'] = $BID;
                    $returnBlockData['content'] = Theme::checkBlock($block['file'], $blockParams);
                }
                else return;
            }
            else {
                $BID = 0;
                if(file_exists(APPLICATION_PATH . 'blocks' . DIRECTORY_SEPARATOR . $BIDorBlock . DIRECTORY_SEPARATOR . $BIDorBlock . '.ini') ||
                    file_exists(APPLICATION_PATH . 'blocks' . DIRECTORY_SEPARATOR . $BIDorBlock . DIRECTORY_SEPARATOR . $BIDorBlock . '.php')) {
                    $check = true;
                    $blockParams = array('template' => '');
                    $blockFile = $BIDorBlock;

                    $block = array( 'file' => $BIDorBlock,
                                    'title' => 'New block ' . $BIDorBlock,
                                    'link' => '',
                                    'params' => array(),
                                    'area' => $blockArea,
                                    'routes' => '',
                                    'include_urls' => '',
                                    'exclude_urls' => '',
                                    'themes' => 'all',
                                    'setting' => 'a:0:{}',
                                    'status' => 1);
                }
            }

            $blockFile = $block['file'];

            /**** parse block settings ****/
            $hook['form_id'] = 'theme-block-config';
            $hook['form_before'] = '<input type="hidden" name="bid" value="' . $BID . '" /><input type="hidden" name="block" value="' . $blockFile . '" />';
            /**** add action buttons to form ****/
            $hook['form_after'] = '<input type="button" role="Close_SugarBox" id="block-config-close" class="btn btn-danger" value="Close" />
<input type="submit" name="save-close" id="block-config-save-close" class="btn btn-success" value="Save and close" />';

            /**** Parse current block params to form inputs ****/
            $blockData = $this->getBlockConfig($block, $blockParams, $blockFile);
            //n($blockData['config']['fields']);
            /**** get form from inpt data ****/
            $blockOptions = Option::formFromConfig('block_options_' . $blockFile, $blockData['config']['fields'], $hook, RECOMPILE_CONFIG_FORM);

            /**** check if user save block ****/
            if(!empty($blockOptions['submitedValues'])) {
                $smtedValue = $blockOptions['submitedValues'];
                if(!isset($smtedValue['link'])) $smtedValue['link'] = '';
                $blockDataToUpdate =
                    array(  'title' => $smtedValue['title'],
                            'link' => $smtedValue['link'],
                            'routes' => $smtedValue['routes'],
                            'include_urls' => str_replace(PHP_EOL, ',', $smtedValue['include_urls']),
                            'exclude_urls' => str_replace(PHP_EOL, ',', $smtedValue['exclude_urls']),);
                unset($smtedValue['title'], $smtedValue['link'], $smtedValue['routes'],$smtedValue['include_urls'],$smtedValue['exclude_urls']);
                $blockDataToUpdate['params'] = serialize($smtedValue);

                $blockDataToUpdate = array_merge($block, $blockDataToUpdate);

                if(is_numeric(Input::Post('bid')) && Input::Post('bid') > 0) {
                    unset($blockDataToUpdate['bid']);
                    /**** update block data ****/
                    $updateBlock = DB::Query('blocks')
                        ->Where('bid', '=', $BID)
                        ->Update($blockDataToUpdate);
                    if($updateBlock->status) {
                        Helper::Notify('success', 'Success update block!');
                        $isSaved = 'true';
                        $returnBlockData['content'] = Theme::checkBlock($blockFile, $blockOptions['submitedValues']);
                        if(Input::Post('save-close', '') != '')
                            $saveAndClose = 'true';
                    }
                    else Helper::Notify('error', 'Cannot update block!');
                }
                else {
                    unset($blockDataToUpdate['bid']);
                    $blockDataToUpdate['file'] = Input::Post('block');

                    $addBlock = DB::Query('blocks')->Insert($blockDataToUpdate);
                    if($addBlock->status && $addBlock->insert_id > 0) {
                        Helper::Notify('success', 'Success add block!');
                        //$isSaved = 'true';
                        //$returnBlockData['content'] = Theme::checkBlock($blockFile, $blockOptions['submitedValues']);
                        //if(Input::Post('save-close', '') != '')
                            //$saveAndClose = 'true';
                        $paramsToRedirect = G::$Route['params'];
                        $paramsToRedirect['params'] = $addBlock->insert_id;
                        $redirectUrl = Router::Generate('ControllerParams', $paramsToRedirect);
                        if(Input::Post('save-close', '') != '')
                            header('LOCATION:' . $redirectUrl . '/addAndClose');
                        else
                            header('LOCATION:' . $redirectUrl . '/addSuccess');
                    }
                    else Helper::Notify('error', 'Cannot add block!');
                }
            }
            $configContent = $blockOptions['form'];

            /**** parse data for "design" section ****/
            $configContent .= '<script type="text/javascript">var inBlockConfig = true; var blockData = ' . json_encode($returnBlockData) . '; var isSaved = ' . $isSaved . '; var isCloseBlockConfig = ' . $saveAndClose . '; var isSuccessedAddBlock = ' . $isSuccessedAddBlock . ';</script>';
            $configContent = '<div class="block-config-wrapper">' . $configContent . '</div>';
            $this->Render($configContent);
        }

        if(!$check) {
            $this->Render('');
            Helper::Notify('error', 'Invalid block!');
            return;
        }
    }

    private function getBlockConfig($block, $blockParams, $blockFile) {
        $blockPath = APPLICATION_PATH . 'blocks' . DIRECTORY_SEPARATOR . $blockFile . DIRECTORY_SEPARATOR . $blockFile . '.ini';

        /**** get default block config from ini file ****/
        $blockData = Input::parseInitFile($blockPath, true);

        $titleField = array('title' => array('type' => 'text',
                            'label' => 'Block title',
                            'value' => $block['title']));
        if(!isset($block['link'])) $block['link'] = '';
        $linkField = array('link' => array('type' => 'text',
            'label' => 'Block url',
            'value' => $block['link']));
        if(!isset($blockData['config']['fields'])) $blockData['config']['fields'] = array();
        $blockData['config']['fields'] = $titleField + $linkField + $blockData['config']['fields'];

        /**** Add template to block config ****/
        $blockData['config']['fields']['template'] =
            array(  'type' => 'single_value',
                    'label' => 'Template',
                    'value' => $blockParams['template'],
                    'options' => $blockData['config']['templates'],
                    'require' => 1
            );
        /**** End add template to block config ****/

        /**** Add routes configs to block ****/
        $allProfiles = Router::GetRoutes(true, true);
        $excludeRoutes = array('ThumbnailHandler');
        $routesConfig = array();
        $routesConfig['all'] = 'All';
        $routesConfig['current_page'] = 'Current page';
        foreach($allProfiles as $routeName => $routeData) {
            if(!Filter::startsWith('Ajax', $routeName) &&
                !in_array($routeName, $excludeRoutes)) {
                $routesConfig[$routeName] = $routeName;
            }
        }
        if(is_array($block['routes'])) $block['routes'] = implode(',', $block['routes']);
        $blockData['config']['fields']['routes'] =
            array(  'type' => 'multi_value',
                    'display' => 'checkbox',
                    'label' => 'Routes',
                    'value' => $block['routes'],
                    'options' => $routesConfig,
                    'require' => 1
            );

        $blockData['config']['fields']['include_urls'] =
            array(  'type' => 'textarea',
                'label' => 'Include urls',
                'value' => str_replace(',', PHP_EOL, $block['include_urls'])
            );
        $blockData['config']['fields']['exclude_urls'] =
            array(  'type' => 'textarea',
                'label' => 'Exclude urls',
                'value' => str_replace(',', PHP_EOL, $block['exclude_urls'])
            );
        /**** End add routes configs to block ****/

        foreach($blockData['config']['fields'] as $op => $ov) {
            if(isset($blockParams[$op]))
                $blockData['config']['fields'][$op]['value'] = $blockParams[$op];
        }
        return $blockData;
    }

    public function removeBlock() {
        $BID = Input::Post('bid', 0);
        if($BID > 0) {
            $removeBlock = DB::Query('blocks')->Where('bid', '=', $BID)->Delete();
            if($removeBlock->status)
                echo 'OK';
            else echo 'NOT';
        }
        die();
    }

    public function reOrderBlocks() {
        $blockOrders = Input::Post('blocks_order', '');

        $blockOrderPage = $this->getWorkingPage();

        $SessionBlockOrders = G::$Session->Get('design_blockOrders', '');
        $SessionBlockOrders[$blockOrderPage] = json_decode($blockOrders);
        G::$Session->Set('design_blockOrders', $SessionBlockOrders);
        echo G::$Session->Get('design_blockOrders', '') == $SessionBlockOrders ? 'OK' : 'NOT';
        //echo json_encode(G::$Session->Get('design_blockOrders', ''));
        die();
    }
}