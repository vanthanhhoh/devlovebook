<div class="form-group FieldWrap FieldType_text Field_title">
	<label class="col-sm-3 control-label" for="ID_Field[title]">Block title</label>
    <div class="col-sm-9">
		<input type="text" name="Field[title]" id="ID_Field[title]" class="form-control FieldType_text Field_title" value="<?php echo $Fields['title']['value'] ?>" />
 	</div>
</div>
<div class="form-group FieldWrap FieldType_text Field_link">
	<label class="col-sm-3 control-label" for="ID_Field[link]">Block url</label>
    <div class="col-sm-9">
		<input type="text" name="Field[link]" id="ID_Field[link]" class="form-control FieldType_text Field_link" value="<?php echo $Fields['link']['value'] ?>" />
 	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_Field[gid]">Menu group</label>
    <div class="col-sm-9">
        <select name="Field[gid]" id="ID_Field[gid]" class="form-control FieldType_referer Field_gid" >
            <?php foreach($Fields['gid']['Options'] as $Options):?>
			<option value="<?php echo $Options['value'] ?>"<?php if($Options['value'] == $Fields['gid']['value']): ?> selected="selected"<?php endif ?>><?php echo $Options['prefix'] . $Options['text'] ?></option>
			<?php endforeach ?>
        </select>
    </div>
</div>
<div class="form-group FieldWrap FieldType_text Field_class">
	<label class="col-sm-3 control-label" for="ID_Field[class]">Class container</label>
    <div class="col-sm-9">
		<input type="text" name="Field[class]" id="ID_Field[class]" class="form-control FieldType_text Field_class" value="<?php echo $Fields['class']['value'] ?>" />
 	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_Field[template]">Template</label>
    <div class="col-sm-9">
        <select name="Field[template]" id="ID_Field[template]" class="form-control FieldType_single_value Field_template" >
            <option value="main"<?php if('main' == $Fields['template']['value']): ?> selected="selected"<?php endif ?>>Main</option>
<option value="rows"<?php if('rows' == $Fields['template']['value']): ?> selected="selected"<?php endif ?>>Menu rows</option>
        </select>
    </div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_Field[routes]">Routes</label>
    <div class="col-sm-9">
    	<?php $ArrValue = explode(',',$Fields['routes']['value']); ?><div class="VNP_CheckBoxOpts">

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_all" class="FieldType_multi_value Field_routes" value="all"<?php if(in_array('all', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_all">
					All</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_current_page" class="FieldType_multi_value Field_routes" value="current_page"<?php if(in_array('current_page', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_current_page">
					Current page</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_Controller" class="FieldType_multi_value Field_routes" value="Controller"<?php if(in_array('Controller', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_Controller">
					Controller</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_ControllerAction" class="FieldType_multi_value Field_routes" value="ControllerAction"<?php if(in_array('ControllerAction', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_ControllerAction">
					ControllerAction</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_ControllerParams" class="FieldType_multi_value Field_routes" value="ControllerParams"<?php if(in_array('ControllerParams', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_ControllerParams">
					ControllerParams</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_ExtensionRestApi" class="FieldType_multi_value Field_routes" value="ExtensionRestApi"<?php if(in_array('ExtensionRestApi', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_ExtensionRestApi">
					ExtensionRestApi</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_ExtensionRestApiParams" class="FieldType_multi_value Field_routes" value="ExtensionRestApiParams"<?php if(in_array('ExtensionRestApiParams', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_ExtensionRestApiParams">
					ExtensionRestApiParams</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_LoginAction" class="FieldType_multi_value Field_routes" value="LoginAction"<?php if(in_array('LoginAction', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_LoginAction">
					LoginAction</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_LogoutAction" class="FieldType_multi_value Field_routes" value="LogoutAction"<?php if(in_array('LogoutAction', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_LogoutAction">
					LogoutAction</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_UserRegister" class="FieldType_multi_value Field_routes" value="UserRegister"<?php if(in_array('UserRegister', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_UserRegister">
					UserRegister</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_short_route_captcha" class="FieldType_multi_value Field_routes" value="short_route_captcha"<?php if(in_array('short_route_captcha', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_short_route_captcha">
					short_route_captcha</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_short_route_post_rss" class="FieldType_multi_value Field_routes" value="short_route_post_rss"<?php if(in_array('short_route_post_rss', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_short_route_post_rss">
					short_route_post_rss</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_short_route_post_sitemap" class="FieldType_multi_value Field_routes" value="short_route_post_sitemap"<?php if(in_array('short_route_post_sitemap', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_short_route_post_sitemap">
					short_route_post_sitemap</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_short_route_post_sitemap_type" class="FieldType_multi_value Field_routes" value="short_route_post_sitemap_type"<?php if(in_array('short_route_post_sitemap_type', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_short_route_post_sitemap_type">
					short_route_post_sitemap_type</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_Home" class="FieldType_multi_value Field_routes" value="Home"<?php if(in_array('Home', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_Home">
					Home</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_Page" class="FieldType_multi_value Field_routes" value="Page"<?php if(in_array('Page', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_Page">
					Page</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_Cart" class="FieldType_multi_value Field_routes" value="Cart"<?php if(in_array('Cart', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_Cart">
					Cart</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_Checkout" class="FieldType_multi_value Field_routes" value="Checkout"<?php if(in_array('Checkout', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_Checkout">
					Checkout</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_CheckInvoice" class="FieldType_multi_value Field_routes" value="CheckInvoice"<?php if(in_array('CheckInvoice', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_CheckInvoice">
					CheckInvoice</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_SearchMain" class="FieldType_multi_value Field_routes" value="SearchMain"<?php if(in_array('SearchMain', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_SearchMain">
					SearchMain</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_ProductCategory" class="FieldType_multi_value Field_routes" value="ProductCategory"<?php if(in_array('ProductCategory', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_ProductCategory">
					ProductCategory</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_ProductAuthor" class="FieldType_multi_value Field_routes" value="ProductAuthor"<?php if(in_array('ProductAuthor', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_ProductAuthor">
					ProductAuthor</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_ProductDetail" class="FieldType_multi_value Field_routes" value="ProductDetail"<?php if(in_array('ProductDetail', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_ProductDetail">
					ProductDetail</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_ProductReview" class="FieldType_multi_value Field_routes" value="ProductReview"<?php if(in_array('ProductReview', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_ProductReview">
					ProductReview</label>
				</div>
</div>
  	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_<?php echo 'Field_include_urls' ?>">Include urls</label>
    <div class="col-sm-9">
    	<textarea name="Field[include_urls]" id="ID_<?php echo 'Field_include_urls' ?>" class="form-control FieldType_textarea Field_include_urls"><?php echo $Fields['include_urls']['value'] ?></textarea>
  	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_<?php echo 'Field_exclude_urls' ?>">Exclude urls</label>
    <div class="col-sm-9">
    	<textarea name="Field[exclude_urls]" id="ID_<?php echo 'Field_exclude_urls' ?>" class="form-control FieldType_textarea Field_exclude_urls"><?php echo $Fields['exclude_urls']['value'] ?></textarea>
  	</div>
</div>
