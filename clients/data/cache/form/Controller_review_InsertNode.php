<div class="FormPart FormPartCol-6">
<div class="form-group FieldWrap FieldType_text Field_title RequiredField">
	<label class="col-sm-3 control-label" for="ID_Field[title]">Title<span class="RequireField">*</span></label>
    <div class="col-sm-9">
		<input type="text" name="Field[title]" id="ID_Field[title]" class="form-control FieldType_text Field_title RequiredField" value="<?php echo $Fields['title']['value'] ?>" />
 	</div>
</div>
<div class="form-group FieldWrap FieldType_number Field_rate RequiredField">
	<label class="col-sm-3 control-label" for="ID_Field[rate]">Đánh giá<span class="RequireField">*</span></label>
    <div class="col-sm-9">
		<input type="number" name="Field[rate]" id="ID_Field[rate]" class="form-control FieldType_number Field_rate RequiredField" value="<?php echo $Fields['rate']['value'] ?>" />
 	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_<?php echo 'Field_comment' ?>">Comment<span class="RequireField">*</span></label>
    <div class="col-sm-9">
    	<textarea name="Field[comment]" id="ID_<?php echo 'Field_comment' ?>" class="form-control FieldType_textarea Field_comment RequiredField"><?php echo $Fields['comment']['value'] ?></textarea>
  	</div>
</div>
<div class="form-group FieldWrap FieldType_number Field_product_id RequiredField">
	<label class="col-sm-3 control-label" for="ID_Field[product_id]">Sản phẩm<span class="RequireField">*</span></label>
    <div class="col-sm-9">
		<input type="number" name="Field[product_id]" id="ID_Field[product_id]" class="form-control FieldType_number Field_product_id RequiredField" value="<?php echo $Fields['product_id']['value'] ?>" />
 	</div>
</div>
</div>
<div class="FormPart FormPartCol-4">
<?php echo Backend::NodeExtraSettings(); ?>
</div>
