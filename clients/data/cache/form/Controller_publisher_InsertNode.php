<div class="FormPart FormPartCol-6">
<div class="form-group FieldWrap FieldType_text Field_title RequiredField">
	<label class="col-sm-3 control-label" for="ID_Field[title]">Tiêu đề<span class="RequireField">*</span></label>
    <div class="col-sm-9">
		<input type="text" name="Field[title]" id="ID_Field[title]" class="form-control FieldType_text Field_title RequiredField" value="<?php echo $Fields['title']['value'] ?>" />
 	</div>
</div>
<div class="form-group FieldWrap FieldType_url Field_url RequiredField">
	<label class="col-sm-3 control-label" for="ID_Field[url]">Đường dẫn<span class="RequireField">*</span></label>
    <div class="col-sm-9">
		<input type="" name="Field[url]" id="ID_Field[url]" class="form-control FieldType_url Field_url RequiredField" value="<?php echo $Fields['url']['value'] ?>" />
 	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_<?php echo 'Field_description' ?>">Mô tả</label>
    <div class="col-sm-9">
    	<textarea name="Field[description]" id="ID_<?php echo 'Field_description' ?>" class="form-control FieldType_textarea Field_description"><?php echo $Fields['description']['value'] ?></textarea>
  	</div>
</div>
</div>
<div class="FormPart FormPartCol-4">
<?php echo Backend::NodeExtraSettings(); ?>
</div>
