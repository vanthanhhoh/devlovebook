<div class="FormPart FormPartCol-6">
<div class="form-group FieldWrap FieldType_text Field_title RequiredField">
	<label class="col-sm-3 control-label" for="ID_Field[title]">Tên phương thức<span class="RequireField">*</span></label>
    <div class="col-sm-9">
		<input type="text" name="Field[title]" id="ID_Field[title]" class="form-control FieldType_text Field_title RequiredField" value="<?php echo $Fields['title']['value'] ?>" />
 	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_<?php echo 'Field_description' ?>">Mô tả</label>
    <div class="col-sm-9">
    	<textarea name="Field[description]" id="ID_<?php echo 'Field_description' ?>" class="form-control FieldType_textarea Field_description"><?php echo $Fields['description']['value'] ?></textarea>
  	</div>
</div>
<div class="form-group FieldWrap FieldType_text Field_module">
	<label class="col-sm-3 control-label" for="ID_Field[module]">Module áp dụng</label>
    <div class="col-sm-9">
        <select name="Field[module]" id="ID_Field[module]" class="form-control FieldType_single_value Field_module" >
            <option value="">None</option>
           <?php foreach($paymentMethod as $item) { ?>
                <option value="<?php echo $item ?>" <?php if($item==$Fields['module']['value']) { ?>selected="selected"<?php } ?> ><?php echo $item ?></option>
           <?php } ?>
        </select>
 	</div>
</div>

    <div class="form-group FieldWrap FieldType_text Field_online">
        <label class="col-sm-3 control-label" for="ID_Field[module]">Thanh toán online</label>
        <div class="col-sm-9">
            <select name="Field[online]" id="ID_Field[online]" class="form-control FieldType_single_value Field_module" >
                <option value="0" <?php if($Fields['online']['value']==0) { ?>selected="selected"<?php } ?>>Không</option>
                <option value="1" <?php if($Fields['online']['value']==1) { ?>selected="selected"<?php } ?>>Có</option>
            </select>
        </div>
    </div>
</div>
<div class="FormPart FormPartCol-4">
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_Field[apply]">Áp dụng</label>
    <div class="col-sm-9">
        <select name="Field[apply]" id="ID_Field[apply]" class="form-control FieldType_single_value Field_apply" >

            <option value="1"<?php if('1' == $Fields['apply']['value']): ?> selected="selected"<?php endif ?>>Bật</option>
            <option value="2"<?php if('2' == $Fields['apply']['value']): ?> selected="selected"<?php endif ?>>Tắt</option>
        </select>
    </div>
</div>
<?php echo Backend::NodeExtraSettings(); ?>
</div>
