<div class="form-group FieldWrap FieldType_image Field_logo clearfix">
	<label class="col-sm-3 control-label" for="ID_Field[logo]">Logo</label>
    <div class="col-sm-6">
    	<input type="text" name="Field[logo]" id="ID_Field[logo]" class="form-control FieldType_image Field_logo" value="<?php echo $Fields['logo']['value'] ?>"/>
   	</div>
    <div class="col-sm-3">
		<button class="btn btn-primary" type="button" onclick="VNP.SugarBox.Open('FileManager','ID_Field[logo]'); return false;">Browse</button>
		<button class="btn btn-danger" onclick="document.getElementById('ID_Field[logo]').value = ''; document.getElementById('Thumb_ID_Field[logo]').setAttribute('src', 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=='); return false;">Delete</button>
	</div>
    <div class="col-sm-8 FieldImagePreviewer"><img id="Thumb_ID_Field[logo]"></div>
</div>
<div class="form-group FieldWrap FieldType_image Field_logomobile clearfix">
	<label class="col-sm-3 control-label" for="ID_Field[logomobile]">Logo mobile</label>
    <div class="col-sm-6">
    	<input type="text" name="Field[logomobile]" id="ID_Field[logomobile]" class="form-control FieldType_image Field_logomobile" value="<?php echo $Fields['logomobile']['value'] ?>"/>
   	</div>
    <div class="col-sm-3">
		<button class="btn btn-primary" type="button" onclick="VNP.SugarBox.Open('FileManager','ID_Field[logomobile]'); return false;">Browse</button>
		<button class="btn btn-danger" onclick="document.getElementById('ID_Field[logomobile]').value = ''; document.getElementById('Thumb_ID_Field[logomobile]').setAttribute('src', 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=='); return false;">Delete</button>
	</div>
    <div class="col-sm-8 FieldImagePreviewer"><img id="Thumb_ID_Field[logomobile]"></div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_<?php echo 'Field_introfotter' ?>">Giới thiệu chân trang</label>
    <div class="col-sm-9">
    	<textarea name="Field[introfotter]" id="ID_<?php echo 'Field_introfotter' ?>" class="form-control FieldType_textarea Field_introfotter"><?php echo $Fields['introfotter']['value'] ?></textarea>
  	</div>
</div>
