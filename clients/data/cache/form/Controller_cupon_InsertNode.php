<link rel="stylesheet" type="text/css" href="/data/library/js-plugins/tokenize/jquery.tokenize.css"/>
<script type="text/javascript" src="/data/library/js-plugins/tokenize/jquery.tokenize.js"></script>
<div class="FormPart FormPartCol-6">
<div class="form-group FieldWrap FieldType_text Field_title RequiredField">
	<label class="col-sm-3 control-label" for="ID_Field[title]">Tiêu đề<span class="RequireField">*</span></label>
    <div class="col-sm-9">
		<input type="text" name="Field[title]" id="ID_Field[title]" class="form-control FieldType_text Field_title RequiredField" value="<?php echo $Fields['title']['value'] ?>" />
 	</div>
</div>
<div class="form-group FieldWrap FieldType_text Field_code RequiredField">
	<label class="col-sm-3 control-label" for="ID_Field[code]">Mã giảm giá<span class="RequireField">*</span></label>
    <div class="col-sm-9">
		<input type="text" name="Field[code]" id="ID_Field[code]" class="form-control FieldType_text Field_code RequiredField" value="<?php echo $Fields['code']['value'] ?>" />
 	</div>
</div>
<div class="form-group FieldWrap FieldType_number Field_discount RequiredField">
	<label class="col-sm-3 control-label" for="ID_Field[discount]">Giảm giá<span class="RequireField">*</span></label>
    <div class="col-sm-9">
		<input type="number" name="Field[discount]" id="ID_Field[discount]" class="form-control FieldType_number Field_discount RequiredField" value="<?php echo $Fields['discount']['value'] ?>" />
 	</div>
</div>
    <div class="form-group FieldWrap FieldType_text Field_date_exp RequiredField">
        <label class="col-sm-3 control-label" for="ID_Field[date_exp]">Ngày hết hạn<span class="RequireField">*</span></label>
        <div class="col-sm-9">

            <div class="DatePickerCtner">
                <div class="input-group date" style="width:60%;float: left;margin-right: 5px" id="VNP_Nodeexprired_time" data-date="<?php echo $date_exp['date'] ?>" data-date-format="dd/mm/yyyy" data-date-viewmode="years">
                    <input type="text"  name="Field[date_exp]" class="form-control input-append datepicker" value="<?php echo $date_exp['date'] ?>" />
                    <span class="input-group-addon add-on" style="width: 28px;padding: 5px 10px;"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="FormPart FormPartCol-4">
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_Field[discount_type]">Hình thức giảm</label>
    <div class="col-sm-9">
        <select name="Field[discount_type]" id="ID_Field[discount_type]" class="form-control FieldType_single_value Field_discount_type" >
            <option value=""<?php if('' == $Fields['discount_type']['value']): ?> selected="selected"<?php endif ?>>Select</option>
<option value="1"<?php if('1' == $Fields['discount_type']['value']): ?> selected="selected"<?php endif ?>>Giảm số tiền</option>
<option value="2"<?php if('2' == $Fields['discount_type']['value']): ?> selected="selected"<?php endif ?>>Giảm theo %</option>
        </select>
    </div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_Field[apply]">Áp dụng cho</label>
    <div class="col-sm-9">
        <select name="Field[apply][]" id="ID_Field_apply" class="multiSelect" multiple="multiple">
            <option value="-1"<?php if(in_array(-1,explode(',',$Fields['apply']['value']))): ?> selected="selected"<?php endif ?>>Tất cả</option>
            <?php
                foreach ($User as $Users) {
            ?>
            <option value="<?php echo $Users['userid']; ?>" <?php if(in_array($Users['userid'],explode(',',$Fields['apply']['value']))): ?> selected="selected"<?php endif ?>><?php echo $Users['email'] ?></option>
            <?php } ?>
        </select>
    </div>
</div>
    <script type="text/javascript">
        $('#ID_Field_apply').tokenize({newElements:true});
    </script>
    <style>
        .multiSelect.Tokenize {
            width: 100%;
        }
    </style>
<?php echo Backend::NodeExtraSettings(); ?>
</div>
