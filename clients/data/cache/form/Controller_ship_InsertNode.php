<div class="FormPart FormPartCol-6">
<div class="form-group FieldWrap FieldType_text Field_title RequiredField">
	<label class="col-sm-3 control-label" for="ID_Field[title]">Tên phương thức<span class="RequireField">*</span></label>
    <div class="col-sm-9">
		<input type="text" name="Field[title]" id="ID_Field[title]" class="form-control FieldType_text Field_title RequiredField" value="<?php echo $Fields['title']['value'] ?>" />
 	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_<?php echo 'Field_description' ?>">Mô tả</label>
    <div class="col-sm-9">
    	<textarea name="Field[description]" id="ID_<?php echo 'Field_description' ?>" class="form-control FieldType_textarea Field_description"><?php echo $Fields['description']['value'] ?></textarea>
  	</div>
</div>
<div class="form-group FieldWrap FieldType_number Field_fee RequiredField">
	<label class="col-sm-3 control-label" for="ID_Field[fee]">Phí<span class="RequireField">*</span></label>
    <div class="col-sm-9">
		<input type="number" name="Field[fee]" id="ID_Field[fee]" class="form-control FieldType_number Field_fee RequiredField" value="<?php echo $Fields['fee']['value'] ?>" />
 	</div>
</div>
</div>
<div class="FormPart FormPartCol-4">
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_Field[apply]">Áp dụng</label>
    <div class="col-sm-9">
        <select name="Field[apply]" id="ID_Field[apply]" class="form-control FieldType_single_value Field_apply" >
            <option value=""<?php if('' == $Fields['apply']['value']): ?> selected="selected"<?php endif ?>>Select</option>
<option value="1"<?php if('1' == $Fields['apply']['value']): ?> selected="selected"<?php endif ?>>Bật</option>
<option value="2"<?php if('2' == $Fields['apply']['value']): ?> selected="selected"<?php endif ?>>Tắt</option>
        </select>
    </div>
</div>
<?php echo Backend::NodeExtraSettings(); ?>
</div>
