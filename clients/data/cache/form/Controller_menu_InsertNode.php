<script src="<?php echo APPLICATION_DATA_DIR ?>theme/admin-vnp/js/bootstrap-typeahead.min.js"></script>
<div class="FormPart FormPartCol-6">
<div class="form-group FieldWrap FieldType_text Field_title RequiredField">
	<label class="col-sm-3 control-label" for="ID_Field[title]">Tiêu đề<span class="RequireField">*</span></label>
    <div class="col-sm-9">
		<input type="text" name="Field[title]" id="ID_Field[title]" class="form-control FieldType_text Field_title RequiredField" value="<?php echo $Fields['title']['value'] ?>" />
 	</div>
</div>
<div class="form-group FieldWrap FieldType_text Field_url">
	<label class="col-sm-3 control-label" for="ID_Field[url]">Đường dẫn</label>
    <div class="col-sm-9">
		<input type="text" name="Field[url]" id="suggetURL" class="form-control FieldType_text Field_url" value="<?php echo $Fields['url']['value'] ?>" />
 	</div>
</div>
<div class="form-group FieldWrap FieldType_text Field_name">
	<label class="col-sm-3 control-label" for="ID_Field[name]">Name</label>
    <div class="col-sm-9">
		<input type="text" name="Field[name]" id="ID_Field[name]" class="form-control FieldType_text Field_name" value="<?php echo $Fields['name']['value'] ?>" />
 	</div>
</div>
</div>
<div class="FormPart FormPartCol-4">
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_Field[menu_group]">Nhóm</label>
    <div class="col-sm-9">
        <select name="Field[menu_group]" id="ID_Field[menu_group]" class="form-control FieldType_referer Field_menu_group" >
            <?php foreach($Fields['menu_group']['Options'] as $Options):?>
			<option value="<?php echo $Options['value'] ?>"<?php if($Options['value'] == $Fields['menu_group']['value']): ?> selected="selected"<?php endif ?>><?php echo $Options['prefix'] . $Options['text'] ?></option>
			<?php endforeach ?>
        </select>
    </div>
</div>
<?php echo Backend::NodeExtraSettings(); ?>
</div>
<script>
    $(document).ready(function(){
        $('#suggetURL').typeahead({
            source: function (q, process) {
                return $.post('/ServiceSys/suggetpageURL', {
                    query: q
                }, function (response) {
                    var data = [];
                    for (var i in response) {
                        data.push(response[i].type + "#" + response[i].title+'#'+response[i].url);
                    }
                    data.pop();
                    return process(data);
                });
            },
            highlighter: function (item) {
                var parts = item.split('#'),
                    html = '<div class="typeahead">';

                html += '<div class="">';
                html += '<div class="text-left"><strong>' + parts[0] + '</strong></div>';
                html += '<div class="text-left">' + parts[1] + '</div>';
                html += '</div>';
                html += '<div class="clearfix"></div>';
                html += '</div>';
                return html;
            },
            updater: function (item) {
                var parts = item.split('#');

                return parts[2];
            }
        });
    });
</script>