<div class="FormPart FormPartCol-6">
<div class="form-group FieldWrap FieldType_text Field_title RequiredField">
	<label class="col-sm-3 control-label" for="ID_Field[title]">Tiêu đề<span class="RequireField">*</span></label>
    <div class="col-sm-9">
		<input type="text" name="Field[title]" id="ID_Field[title]" class="form-control FieldType_text Field_title RequiredField" value="<?php echo $Fields['title']['value'] ?>" />
 	</div>
</div>
<div class="form-group FieldWrap FieldType_image Field_image RequiredField clearfix">
	<label class="col-sm-3 control-label" for="ID_Field[image]">Hình ảnh<span class="RequireField">*</span></label>
    <div class="col-sm-6">
    	<input type="text" name="Field[image]" id="ID_Field[image]" class="form-control FieldType_image Field_image RequiredField" value="<?php echo $Fields['image']['value'] ?>"/>
   	</div>
    <div class="col-sm-3">
		<button class="btn btn-primary" type="button" onclick="VNP.SugarBox.Open('FileManager','ID_Field[image]'); return false;">Browse</button>
		<button class="btn btn-danger" onclick="document.getElementById('ID_Field[image]').value = ''; document.getElementById('Thumb_ID_Field[image]').setAttribute('src', 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=='); return false;">Delete</button>
	</div>
    <div class="col-sm-9 col-sm-offset-3 FieldImagePreviewer"><img id="Thumb_ID_Field[image]"></div>
</div>
<div class="form-group FieldWrap FieldType_text Field_url">
	<label class="col-sm-3 control-label" for="ID_Field[url]">Link</label>
    <div class="col-sm-9">
		<input type="text" name="Field[url]" id="ID_Field[url]" class="form-control FieldType_text Field_url" value="<?php echo $Fields['url']['value'] ?>" />
 	</div>
</div>
</div>
<div class="FormPart FormPartCol-4">
<?php echo Backend::NodeExtraSettings(); ?>
</div>
