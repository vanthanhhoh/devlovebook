<link rel="stylesheet" type="text/css" href="/data/library/js-plugins/tokenize/jquery.tokenize.css"/>
<script type="text/javascript" src="/data/library/js-plugins/tokenize/jquery.tokenize.js"></script>
<div class="FormPart FormPartCol-6">
<div class="form-group FieldWrap FieldType_text Field_title RequiredField">
	<label class="col-sm-3 control-label" for="ID_Field[title]">Tiêu đề<span class="RequireField">*</span></label>
    <div class="col-sm-9">
		<input type="text" name="Field[title]" id="ID_Field[title]" class="form-control FieldType_text Field_title RequiredField" value="<?php echo $Fields['title']['value'] ?>" />
 	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_<?php echo 'Field_content' ?>">Nội dung thông báo<span class="RequireField">*</span></label>
    <div class="col-sm-9">
    	<textarea name="Field[content]" id="ID_<?php echo 'Field_content' ?>" class="form-control FieldType_textarea Field_content RequiredField"><?php echo $Fields['content']['value'] ?></textarea>
  	</div>
</div>
</div>
<div class="FormPart FormPartCol-4">
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_Field[receiver]">Người nhận</label>
    <div class="col-sm-9">
        <select name="Field[receiver][]" id="ID_Field_receiver" multiple="multiple" class="multiSelect" >
            <?php $ArrValue = explode(',',$Fields['receiver']['value']); ?>
            <option value="-1"<?php if(in_array('-1',$ArrValue)): ?> selected="selected"<?php endif ?>>Tất cả</option>
            <?php foreach($User as $item){?>
                <option value="<?php echo $item['userid']; ?>"<?php if(in_array($item['userid'],$ArrValue)): ?> selected="selected"<?php endif ?>><?php echo $item['email']; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<?php echo Backend::NodeExtraSettings(); ?>
</div>
<script type="text/javascript">
    $('#ID_Field_receiver').tokenize({
        newElements:false
    });
</script>
<style>
    .multiSelect.Tokenize {
        width: 100%;
    }
</style>