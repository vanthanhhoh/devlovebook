<script src="<?php echo APPLICATION_DATA_DIR ?>theme/admin-vnp/js/bootstrap-typeahead.min.js"></script>
<div class="FormPart FormPartCol-6">
    <div class="form-group FieldWrap FieldType_text Field_name RequiredField">
        <label class="col-sm-3 control-label" for="ID_Field[name]">Tên người nhận<span class="RequireField">*</span></label>
        <div class="col-sm-9">
            <input type="text" name="Field[name]" id="ID_Field[name]" class="form-control FieldType_text Field_name RequiredField" value="<?php echo $Fields['name']['value'] ?>" />
        </div>
    </div>
    <div class="form-group FieldWrap FieldType_text Field_address">
        <label class="col-sm-3 control-label" for="ID_Field[address]">Địa chỉ nhận sách</label>
        <div class="col-sm-9">
            <input type="text" name="Field[address]" id="ID_Field[address]" class="form-control FieldType_text Field_address" value="<?php echo $Fields['address']['value'] ?>" />
        </div>
    </div>
    <div class="form-group FieldWrap FieldType_text Field_phone">
        <label class="col-sm-3 control-label" for="ID_Field[phone]">Số điện thoại</label>
        <div class="col-sm-9">
            <input type="text" name="Field[phone]" id="ID_Field[phone]" class="form-control FieldType_text Field_phone" value="<?php echo $Fields['phone']['value'] ?>" />
        </div>
    </div>
    <div class="form-group FieldWrap FieldType_text Field_province">
        <label class="col-sm-3 control-label" for="ID_Field[province]">Tỉnh thành</label>
        <div class="col-sm-9">
            <input type="text" name="Field[province]" id="ID_Field[province]" class="form-control FieldType_text Field_province" value="<?php echo $Fields['province']['value'] ?>" />
        </div>
    </div>
    <div class="form-group FieldWrap FieldType_text Field_district">
        <label class="col-sm-3 control-label" for="ID_Field[district]">Quận huyện</label>
        <div class="col-sm-9">
            <input type="text" name="Field[district]" id="ID_Field[district]" class="form-control FieldType_text Field_district" value="<?php echo $Fields['district']['value'] ?>" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ID_<?php echo 'Field_note' ?>">Lưu ý</label>
        <div class="col-sm-9">
            <textarea name="Field[note]" id="ID_<?php echo 'Field_note' ?>" class="form-control FieldType_textarea Field_note"><?php echo $Fields['note']['value'] ?></textarea>
        </div>
    </div>
    <div class="edit_order">
        <div class="input_group">
            <span class="input-group-addon" id="sizing-addon1">Thêm sản phẩm</span>
            <input type="text" class="autocomplete form-control" id="autocomplete" placeholder="Gợi ý tên, mã sản phẩm ..vv"/>
        </div>
        <table class="table table-bordered table-striped table-hover table-responsive" id="cart_table">
            <thead>
            <tr>
                <th>
                    Tên sản phẩm
                </th>
                <th>
                    Số lượng
                </th>
                <th>
                    Giá
                </th>
                <th>
                    Tổng
                </th>
                <th>
                    Xóa
                </th>
            </tr>
            </thead>
            <tbody>
                <?php foreach($order as $item) : $proItem = $product[$item['product_id']]; ?>
                    <tr id="order_<?php echo $item['order_id'] ?>">
                        <td>
                            <?php echo $proItem['title'] ?>
                            <?php if($item['type']==2) : ?>
                            <span class="gift">Quà tặng</span>
                            <?php endif; ?>
                        </td>
                        <td>
                            <input type="number" min="1" style="width: 50px" data-id="<?php echo $item['order_id'] ?>" value="<?php echo $item['qty'] ?>" name="order[<?php echo $item['order_id'] ?>][qty]" onchange="changEditcart($(this))"/>
                        </td>
                        <td class="hidden_input">
                            <?php echo Filter::NumberFormat($item['price']) ?> đ
                            <input type="hidden" name="order[<?php echo $item['order_id'] ?>][price]" value="<?php echo $item['price'] ?>" id="order_price_<?php echo $item['order_id'] ?>"/>
                            <input type="hidden" name="order[<?php echo $item['order_id'] ?>][price_old]" value="<?php echo $item['price_old'] ?>" id="order_price_old_<?php echo $item['order_id'] ?>"/>
                            <input type="hidden" name="order[<?php echo $item['order_id'] ?>][total]" value="<?php echo $item['total'] ?>" id="order_total_input_<?php echo $item['order_id'] ?>" class="order_total_input"/>
                            <input type="hidden" name="order[<?php echo $item['order_id'] ?>][product_id]" value="<?php echo $item['product_id'] ?>" id="order_product_id_<?php echo $item['order_id'] ?>"/>
                            <span style="text-decoration: line-through"><?php echo Filter::NumberFormat($item['price_old']) ?> đ</span>
                        </td>
                        <td id="order_total_<?php echo $item['order_id'] ?>" class="total_rows">
                            <?php echo Filter::NumberFormat($item['total']) ?> đ
                        </td>
                        <td>
                            <a href="#">Xóa</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td>Tổng giá trị đơn hàng : </td>
                    <td colspan="4"><strong id="sum_rows"><?php echo Filter::NumberFormat($Fields['total']['value'])  ?> đ</strong></td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<div class="FormPart FormPartCol-4">
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_Field[invoice_status]">Tình trạng đơn hàng</label>
    <div class="col-sm-9">
        <select name="Field[invoice_status]" id="ID_Field[invoice_status]" class="form-control FieldType_single_value Field_invoice_status" >
            <option value=""<?php if('' == $Fields['invoice_status']['value']): ?> selected="selected"<?php endif ?>>Select</option>
<option value="1"<?php if('1' == $Fields['invoice_status']['value']): ?> selected="selected"<?php endif ?>>Đang chờ xác nhận</option>
<option value="2"<?php if('2' == $Fields['invoice_status']['value']): ?> selected="selected"<?php endif ?>>Đang chờ thanh toán</option>
<option value="3"<?php if('3' == $Fields['invoice_status']['value']): ?> selected="selected"<?php endif ?>>Đã xác nhận</option>
<option value="4"<?php if('4' == $Fields['invoice_status']['value']): ?> selected="selected"<?php endif ?>>Đang vận chuyển</option>
<option value="5"<?php if('5' == $Fields['invoice_status']['value']): ?> selected="selected"<?php endif ?>>Thành công</option>
        </select>
    </div>
</div>

<div class="form-group FieldWrap FieldType_text Field_code RequiredField">
	<label class="col-sm-3 control-label" for="ID_Field[code]">Mã đơn hàng<span class="RequireField">*</span></label>
    <div class="col-sm-9">
		<input type="text" name="Field[code]" id="ID_Field[code]" class="form-control FieldType_text Field_code RequiredField" value="<?php echo $Fields['code']['value'] ?>" />
 	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_Field[payment]">Thanh toán</label>
    <div class="col-sm-9">
        <select name="Field[payment]" id="ID_Field[payment]" class="form-control FieldType_referer Field_payment" >
            <?php foreach($Fields['payment']['Options'] as $Options):?>
			<option value="<?php echo $Options['value'] ?>"<?php if($Options['value'] == $Fields['payment']['value']): ?> selected="selected"<?php endif ?>><?php echo $Options['prefix'] . $Options['text'] ?></option>
			<?php endforeach ?>
        </select>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label" for="ID_Field[ship]">Vận chuyển</label>
    <div class="col-sm-9">
         <select name="Field[ship]" id="ID_Field[ship]" class="form-control FieldType_referer Field_ship" >
             <?php foreach($Fields['ship']['Options'] as $Options):?>
                    <option value="<?php echo $Options['value'] ?>"<?php if($Options['value'] == $Fields['ship']['value']): ?> selected="selected"<?php endif ?>><?php echo $Options['prefix'] . $Options['text'] ?></option>
             <?php endforeach ?>
         </select>
    </div>
</div>
<?php echo Backend::NodeExtraSettings(); ?>
</div>
<style>
    span.gift {
        color: #dc8812;
    }
</style>

<script>
    function changEditcart(e){
        var id = e.data('id');
        var price = $('#order_price_'+id).val();
        var total = price* e.val();
        $('#order_total_input_'+id).val(total);
        $('#order_total_'+id).html(addCommas(total)+'đ');
        checkSum();
    }
    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
    function checkSum(){
        var rows = $('#cart_table > tbody > tr > td.hidden_input > input.order_total_input');
        var sum = 0;
        $.each(rows,function(i,v){
           sum+=parseInt($(this).val());
        });
        $('#sum_rows').html(addCommas(sum)+ 'đ');
    }
    $('#autocomplete').typeahead({
        source: function (q, process) {
            return $.post('/ServiceSys/searchSugget', {
                query: q
            }, function (response) {
                var data = [];
                for (var i in response) {
                    data.push(response[i].name + "#" +response[i].url+'#'+response[i].type);
                }
                data.pop();
                return process(data);
            });
        },
        highlighter: function (item) {
            var parts = item.split('#'),
                html = '<div class="typeahead">';
            html += '<div class="suggetItem">';
            html += '<div class="suggetName"><div class="boldName"> ' + parts[0] + '<div><div class="smallType">'+parts[2]+'</div></div>';
            html += '</div>';
            html += '<div class="clearfix"></div>';
            html += '</div>';
            return html;
        },
        updater: function (item) {
            var parts = item.split('#');
            window.location.href= parts[1];
        }

    });
</script>