<div class="form-group FieldWrap FieldType_text Field_title">
	<label class="col-sm-3 control-label" for="ID_Field[title]">Block title</label>
    <div class="col-sm-9">
		<input type="text" name="Field[title]" id="ID_Field[title]" class="form-control FieldType_text Field_title" value="<?php echo $Fields['title']['value'] ?>" />
 	</div>
</div>
<div class="form-group FieldWrap FieldType_text Field_link">
	<label class="col-sm-3 control-label" for="ID_Field[link]">Block url</label>
    <div class="col-sm-9">
		<input type="text" name="Field[link]" id="ID_Field[link]" class="form-control FieldType_text Field_link" value="<?php echo $Fields['link']['value'] ?>" />
 	</div>
</div>
<div class="form-group FieldWrap FieldType_text Field_block_class">
	<label class="col-sm-3 control-label" for="ID_Field[block_class]">Block class</label>
    <div class="col-sm-9">
		<input type="text" name="Field[block_class]" id="ID_Field[block_class]" class="form-control FieldType_text Field_block_class" value="<?php echo $Fields['block_class']['value'] ?>" />
 	</div>
</div>
<div class="form-group FieldWrap FieldType_html Field_value">
    <div class="col-sm-12">
    	<label class="col-sm-12 control-label" for="ID_<?php echo 'Field_value' ?>">Input html</label>
    	<textarea name="Field[value]" id="ID_<?php echo 'Field_value' ?>" class="form-control FieldType_html Field_value"><?php echo $Fields['value']['value'] ?></textarea>
  	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_Field[template]">Template</label>
    <div class="col-sm-9">
        <select name="Field[template]" id="ID_Field[template]" class="form-control FieldType_single_value Field_template" >
            <option value="bordered"<?php if('bordered' == $Fields['template']['value']): ?> selected="selected"<?php endif ?>>Bordered block</option>
<option value="noborder"<?php if('noborder' == $Fields['template']['value']): ?> selected="selected"<?php endif ?>>No border block</option>
        </select>
    </div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_Field[routes]">Routes</label>
    <div class="col-sm-9">
    	<?php $ArrValue = explode(',',$Fields['routes']['value']); ?><div class="VNP_CheckBoxOpts">

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_all" class="FieldType_multi_value Field_routes" value="all"<?php if(in_array('all', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_all">
					All</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_current_page" class="FieldType_multi_value Field_routes" value="current_page"<?php if(in_array('current_page', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_current_page">
					Current page</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_Controller" class="FieldType_multi_value Field_routes" value="Controller"<?php if(in_array('Controller', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_Controller">
					Controller</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_ControllerAction" class="FieldType_multi_value Field_routes" value="ControllerAction"<?php if(in_array('ControllerAction', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_ControllerAction">
					ControllerAction</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_ControllerParams" class="FieldType_multi_value Field_routes" value="ControllerParams"<?php if(in_array('ControllerParams', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_ControllerParams">
					ControllerParams</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_ExtensionRestApi" class="FieldType_multi_value Field_routes" value="ExtensionRestApi"<?php if(in_array('ExtensionRestApi', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_ExtensionRestApi">
					ExtensionRestApi</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_ExtensionRestApiParams" class="FieldType_multi_value Field_routes" value="ExtensionRestApiParams"<?php if(in_array('ExtensionRestApiParams', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_ExtensionRestApiParams">
					ExtensionRestApiParams</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_LoginAction" class="FieldType_multi_value Field_routes" value="LoginAction"<?php if(in_array('LoginAction', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_LoginAction">
					LoginAction</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_LogoutAction" class="FieldType_multi_value Field_routes" value="LogoutAction"<?php if(in_array('LogoutAction', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_LogoutAction">
					LogoutAction</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_UserRegister" class="FieldType_multi_value Field_routes" value="UserRegister"<?php if(in_array('UserRegister', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_UserRegister">
					UserRegister</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_short_route_captcha" class="FieldType_multi_value Field_routes" value="short_route_captcha"<?php if(in_array('short_route_captcha', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_short_route_captcha">
					short_route_captcha</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_short_route_post_rss" class="FieldType_multi_value Field_routes" value="short_route_post_rss"<?php if(in_array('short_route_post_rss', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_short_route_post_rss">
					short_route_post_rss</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_short_route_post_sitemap" class="FieldType_multi_value Field_routes" value="short_route_post_sitemap"<?php if(in_array('short_route_post_sitemap', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_short_route_post_sitemap">
					short_route_post_sitemap</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_short_route_post_sitemap_type" class="FieldType_multi_value Field_routes" value="short_route_post_sitemap_type"<?php if(in_array('short_route_post_sitemap_type', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_short_route_post_sitemap_type">
					short_route_post_sitemap_type</label>
				</div>

				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[routes][]" id="ID_routes_Home" class="FieldType_multi_value Field_routes" value="Home"<?php if(in_array('Home', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_routes_Home">
					Home</label>
				</div>
</div>
  	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_<?php echo 'Field_include_urls' ?>">Include urls</label>
    <div class="col-sm-9">
    	<textarea name="Field[include_urls]" id="ID_<?php echo 'Field_include_urls' ?>" class="form-control FieldType_textarea Field_include_urls"><?php echo $Fields['include_urls']['value'] ?></textarea>
  	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_<?php echo 'Field_exclude_urls' ?>">Exclude urls</label>
    <div class="col-sm-9">
    	<textarea name="Field[exclude_urls]" id="ID_<?php echo 'Field_exclude_urls' ?>" class="form-control FieldType_textarea Field_exclude_urls"><?php echo $Fields['exclude_urls']['value'] ?></textarea>
  	</div>
</div>
