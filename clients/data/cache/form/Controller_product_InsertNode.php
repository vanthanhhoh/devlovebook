<script src="<?php echo APPLICATION_DATA_DIR ?>theme/admin-vnp/assets/js/bootstrap-typeahead.min.js"></script>
<link rel="stylesheet" type="text/css" href="/data/library/js-plugins/tokenize/jquery.tokenize.css"/>
<script type="text/javascript" src="/data/library/js-plugins/tokenize/jquery.tokenize.js"></script>
<div class="FormPart FormPartCol-6">
<div class="form-group FieldWrap FieldType_text Field_code RequiredField">
	<label class="col-sm-3 control-label" for="ID_Field[code]">Mã sản phẩm <span class="RequireField">*</span></label>
    <div class="col-sm-9">
		<input type="text" name="Field[code]" id="ID_Field[code]" class="form-control FieldType_text Field_code RequiredField" value="<?php echo $Fields['code']['value'] ?>" />
 	</div>
</div>
<div class="form-group FieldWrap FieldType_text Field_title RequiredField">
	<label class="col-sm-3 control-label" for="ID_Field[title]">Tên sản phẩm<span class="RequireField">*</span></label>
    <div class="col-sm-9">
		<input type="text" name="Field[title]" id="ID_Field[title]" class="form-control FieldType_text Field_title RequiredField" value="<?php echo $Fields['title']['value'] ?>" />
 	</div>
</div>
<div class="form-group FieldWrap FieldType_text Field_sku">
	<label class="col-sm-3 control-label" for="ID_Field[sku]">SKU</label>
    <div class="col-sm-9">
		<input type="text" name="Field[sku]" id="ID_Field[sku]" class="form-control FieldType_text Field_sku" value="<?php echo $Fields['sku']['value'] ?>" />
 	</div>
</div>
<div class="form-group FieldWrap FieldType_url Field_url RequiredField">
	<label class="col-sm-3 control-label" for="ID_Field[url]">Đường dẫn<span class="RequireField">*</span></label>
    <div class="col-sm-9">
		<input type="" name="Field[url]" id="ID_Field[url]" class="form-control FieldType_url Field_url RequiredField" value="<?php echo $Fields['url']['value'] ?>" />
 	</div>
</div>
<div class="form-group FieldWrap FieldType_image Field_image RequiredField clearfix">
	<label class="col-sm-3 control-label" for="ID_Field[image]">Ảnh đại diện<span class="RequireField">*</span></label>
    <div class="col-sm-6">
    	<input type="text" name="Field[image]" id="ID_Field[image]" class="form-control FieldType_image Field_image RequiredField" value="<?php echo $Fields['image']['value'] ?>"/>
   	</div>
    <div class="col-sm-3">
		<button class="btn btn-primary" type="button" onclick="VNP.SugarBox.Open('FileManager','ID_Field[image]'); return false;">Browse</button>
		<button class="btn btn-danger" onclick="document.getElementById('ID_Field[image]').value = ''; document.getElementById('Thumb_ID_Field[image]').setAttribute('src', 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=='); return false;">Delete</button>
	</div>
    <div class="col-sm-9 col-sm-offset-3 FieldImagePreviewer"><img id="Thumb_ID_Field[image]"></div>
</div>

<div class="form-group FieldWrap FieldType_multi_image Field_other_images">
    <label for="ID_Field[other_images]" class="col-sm-3 control-label">Hình ảnh khác</label>
   	<input data-field-type="multi_image" type="hidden" class="form-control NodeField_MultiImages FieldType_multi_image Field_other_images" name="Field[other_images][]" id="ID_other_images" value=""/>
    <div class="col-sm-9">
    	<div class="clearfix">
            <button class="btn btn-primary" onclick="VNP.SugarBox.Open('MultiImages','other_images'); return false;">Browse</button>
            <button class="btn btn-danger" onclick="document.getElementById('MultiImages_other_images').innerHTML = ''; return false;">Delete all</button>
      	</div>
        <div class="clearfix MultiImages_Container" id="MultiImages_other_images">
        	<?php $Images = explode(',', $Fields['other_images']['value']); foreach($Images as $_Img) {
	            if(!empty($_Img)): ?>
	            <div class="MultiImages_Item">
	            	<input type="hidden" name="Field[other_images][]" value="<?php echo $_Img ?>" />
	                <img src="<?php echo THUMB_BASE ?>60_60<?php echo $_Img ?>" width="60"/>
	                <a href="#" onclick="this.parentNode.parentNode.removeChild(this.parentNode); return false;">Delete</a>
	          	</div>
	            <?php endif;
            } ?>
        </div>
    </div>
</div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ID_<?php echo 'Field_discription' ?>">Mô tả</label>
        <div class="col-sm-9">
            <textarea name="Field[discription]" id="ID_<?php echo 'Field_discription' ?>" class="form-control FieldType_textarea Field_description"><?php echo $Fields['discription']['value'] ?></textarea>
        </div>
    </div>
<!-- Đọc thử    -->
<div class="form-group FieldWrap FieldType_image Field_trailer clearfix">
        <label class="col-sm-3 control-label" for="ID_Field[trailer]">Đọc thử</label>
        <div class="col-sm-6">
            <input type="text" name="Field[trailer]" id="ID_Field[trailer]" class="form-control FieldType_image Field_trailer" value="<?php echo $Fields['trailer']['value'] ?>"/>
        </div>
        <div class="col-sm-3">
            <button class="btn btn-primary" type="button" onclick="VNP.SugarBox.Open('FileManager','ID_Field[trailer]'); return false;">Browse</button>
            <button class="btn btn-danger" onclick="document.getElementById('ID_Field[trailer]').value = ''; document.getElementById('Thumb_ID_Field[trailer]').setAttribute('src', 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=='); return false;">Delete</button>
        </div>
        <div class="col-sm-9 col-sm-offset-3 FieldImagePreviewer"><img id="Thumb_ID_Field[trailer]"></div>
</div>

<!-- Thuộc tính   -->

<div class="form-group FieldWrap FieldType_attributes Field_properties Field_Artibutes">
        <label class="col-sm-3 control-label" for="ID_Field[properties]">Thuộc tính</label>
        <div class="col-sm-9 clearfix" id="properties_FieldAttribute_Ctner">
            <button class="btn btn-success save_attr_tpl" data-field-name="properties" type="button"><span class="glyphicon glyphicon-floppy-save"></span>&nbsp Save Attributes template</button>
            <button class="btn btn-primary load_attr_tpl" data-field-name="properties" type="button"><span class="glyphicon glyphicon-floppy-open"></span>&nbsp Load Attributes template</button>
            <input type="hidden" name="Field[properties]" id="ID_Field[properties]" class="form-control FieldType_attributes Field_properties" value=""/>
            <!-- Option to clone -->
            <div class="Option_To_Clone" style="display:none" data-field-name="properties">
                <div class="input-group NodeField_Option">
                <span class="input-group-addon Move_Option">
                    <span class="glyphicon glyphicon-resize-vertical"></span>
                </span>
                    <span class="input-group-addon">Name</span>
                    <input type="text" class="form-control FieldOption_Name" name="" placeholder="Attribute Name">
                    <span class="input-group-addon">Value</span>
                    <input type="text" class="form-control FieldOption_Value" name="" placeholder="Attribute Value">
                    <div class="input-group-btn">
                        <button class="btn btn-danger Remove_Option" data-field-name="properties"><span class="glyphicon glyphicon-remove"></span></button>
                    </div>
                </div>
            </div>
            <!-- End option to clone -->
            <?php if(empty($Fields['properties']['value'])): ?>
                <div class="Field_properties_attribute FieldAttributeCtner" data-field-name="properties">
                    <div class="input-group NodeField_Option">
                <span class="input-group-addon Move_Option">
                    <span class="glyphicon glyphicon-resize-vertical"></span>
                </span>
                        <span class="input-group-addon">Name</span>
                        <input type="text" class="form-control FieldOption_Name" name="Field[properties][0][name]" placeholder="Attribute Name">
                        <span class="input-group-addon">Value</span>
                        <input type="text" class="form-control FieldOption_Value" name="Field[properties][0][value]" placeholder="Attribute Value">
                        <div class="input-group-btn">
                            <button class="btn btn-primary Add_Option" data-field-name="properties"><span class="glyphicon glyphicon-plus"></span></button>
                        </div>
                    </div>
                </div>
            <?php else:
                $Attributes = unserialize($Fields['properties']['value']); foreach($Attributes as $i => $att) { ?>
                <div class="Field_properties_attribute FieldAttributeCtner" data-field-name="properties">
                    <div class="input-group NodeField_Option">
	                <span class="input-group-addon Move_Option">
	                    <span class="glyphicon glyphicon-resize-vertical"></span>
	                </span>
                        <span class="input-group-addon">Name</span>
                        <input type="text" class="form-control FieldOption_Name" name="Field[properties][<?php echo $i ?>][name]" value="<?php echo $att['name'] ?>" placeholder="Attribute Name">
                        <span class="input-group-addon">Value</span>
                        <input type="text" class="form-control FieldOption_Value" name="Field[properties][<?php echo $i ?>][value]" value="<?php echo $att['value'] ?>" placeholder="Attribute Value">
                        <div class="input-group-btn">
                            <?php if($i == 0): ?>
                                <button class="btn btn-primary Add_Option" data-field-name="properties"><span class="glyphicon glyphicon-plus"></span></button>
                            <?php else: ?>
                                <button class="btn btn-danger Remove_Option" data-field-name="properties"><span class="glyphicon glyphicon-remove"></span></button>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            <?php } endif ?>
        </div>
</div>

<!-- Giá bìa -->

<div class="form-group FieldWrap FieldType_number Field_price RequiredField">
	<label class="col-sm-3 control-label" for="ID_Field[price]">Giá bìa<span class="RequireField">*</span></label>
    <div class="col-sm-9">
		<input type="number" name="Field[price]" id="ID_Field[price]" class="form-control FieldType_number Field_price RequiredField" value="<?php echo $Fields['price']['value'] ?>" />
 	</div>
</div>
<div class="form-group FieldWrap FieldType_number Field_price_sale">
	<label class="col-sm-3 control-label" for="ID_Field[price_sale]">Giá khuyến mãi</label>
    <div class="col-sm-9">
		<input type="number" name="Field[price_sale]" id="ID_Field[price_sale]" class="form-control FieldType_number Field_price_sale" value="<?php echo $Fields['price_sale']['value'] ?>" />
 	</div>
</div>

<!--End Giá bìa-->

<div class="form-group FieldWrap FieldType_text Field_gift">
	<label class="col-sm-3 control-label" for="ID_Field[gift]">Sản phẩm tặng kèm</label>
    <div class="col-sm-9">
        <div class="auto input-group" style="width: 50%">
            <input type="text" id="autocomplete" placeholder="Nhập tên hoặc mã sản phẩm" class="form-control" autocomplete="off"/>
            <span class="input-group-addon">
				<i class="ace-icon fa fa-plus"></i>
			</span>
        </div>
        <div class="listGift" style="margin-top: 5px">
            <table class="table table-striped table-bordered table-hover">
                <colgroup>
                    <col class="col-xs"/>
                    <col class="col-xs"/>
                    <col class="col-xs"/>
                </colgroup>
                <thead>
                    <tr>
                        <th>Sản phẩm</th>
                        <th>Số lượng</th>
                        <th>Xóa</th>
                    </tr>
                </thead>
                <?php
                    if($Fields['gift']['value']!='') {
                    $gift = unserialize($Fields['gift']['value'])
                ?>
                <tbody id="list_gift_inner">
                    <?php foreach($gift as $id => $item) { ?>
                        <?php $product = DB::Query('product')->Where('product_id','=',$item['product_id'])->Get()->Result[0] ?>
                        <tr id="itemId<?php echo $id; ?>">
                            <td>
                                <?php echo $product['title'].'-'.$product['code'] ?>
                            </td>
                            <td>
                                <input type="number" name="Field[gift][<?php echo $item['product_id'] ?>]" value="<?php echo $item['qty'] ?>" class="form-control FieldType_numbe" min="1" max="10"/>
                            </td>
                            <td>
                                <button class="removeItem" data-id="<?php echo $id; ?>" onclick="RemoveItem($(this))">Xóa</button>
                            </td>
                        </tr>
                    <?php } }?>
                </tbody>
            </table>
        </div>
 	</div>
</div>
</div>


<!--Part 4-->

<div class="FormPart FormPartCol-4">
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_Field[author]">Tác giả</label>
    <div class="col-sm-9">
        <select name="Field[author]" id="ID_Field[author]" class="form-control FieldType_referer Field_author" >
            <?php foreach($Fields['author']['Options'] as $Options):?>
			<option value="<?php echo $Options['value'] ?>"<?php if($Options['value'] == $Fields['author']['value']): ?> selected="selected"<?php endif ?>><?php echo $Options['prefix'] . $Options['text'] ?></option>
			<?php endforeach ?>
        </select>
    </div>
</div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ID_Field[publisher]">Nhà xuất bản</label>
        <div class="col-sm-9">
            <select name="Field[publisher]" id="ID_Field[publisher]" class="form-control FieldType_referer Field_publisher" >
                <?php foreach($Fields['publisher']['Options'] as $Options):?>
                    <option value="<?php echo $Options['value'] ?>"<?php if($Options['value'] == $Fields['publisher']['value']): ?> selected="selected"<?php endif ?>><?php echo $Options['prefix'] . $Options['text'] ?></option>
                <?php endforeach ?>
            </select>
        </div>
    </div>
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_Field[product_category]">Danh mục</label>
    <div class="col-sm-9">
        <select name="Field[product_category]" id="ID_Field[product_category]" class="form-control FieldType_referer Field_product_category" >
            <?php foreach($Fields['product_category']['Options'] as $Options):?>
			<option value="<?php echo $Options['value'] ?>"<?php if($Options['value'] == $Fields['product_category']['value']): ?> selected="selected"<?php endif ?>><?php echo $Options['prefix'] . $Options['text'] ?></option>
			<?php endforeach ?>
        </select>
    </div>
</div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ID_Field_Subcategory">Danh mục phụ</label>
        <div class="col-sm-9">
            <select name="Field[product_sub_category][]" multiple="multiple" id="ID_Field_Subcategory" class="multiSelect" >
                <?php $ArrValue = explode(',',$Fields['product_sub_category']['value']); foreach($Fields['product_category']['Options'] as $Options):?>
                    <option value="<?php echo $Options['value'] ?>"<?php if(in_array($Options['value'],$ArrValue)): ?> selected="selected"<?php endif ?>><?php echo $Options['prefix'] . $Options['text'] ?></option>
                <?php endforeach ?>
            </select>

        </div>
    </div>
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_Field[product_group]">Nhóm sản phẩm</label>
    <div class="col-sm-9">
        <select name="Field[product_group][]" id="ID_Field_product_group"  multiple="multiple" class="multiSelect">
            <?php $ArrValue = explode(',',$Fields['product_group']['value']); foreach($Fields['product_group']['Options'] as $Options):?>
                <option value="<?php echo $Options['value'] ?>"<?php if(in_array($Options['value'],$ArrValue)): ?> selected="selected"<?php endif ?>><?php echo $Options['prefix'] . $Options['text'] ?></option>
            <?php endforeach ?>
        </select>
    </div>
</div>
    <script type="text/javascript">
        $('#ID_Field_Subcategory').tokenize({newElements:true});
        $('#ID_Field_product_group').tokenize({newElements:true});
    </script>
    <style>
        .multiSelect.Tokenize {
            width: 100%;
        }
    </style>
<?php echo Backend::NodeExtraSettings(); ?>
</div>
<div class="FormPart FormPartCol-10">
<div class="form-group FieldWrap FieldType_html Field_content">
    <div class="col-sm-12">
    	<label class="col-sm-12 control-label" for="ID_<?php echo 'Field_content' ?>">Nội dung mô tả</label>
    	<textarea name="Field[content]" id="ID_<?php echo 'Field_content' ?>" class="form-control FieldType_html Field_content"><?php echo $Fields['content']['value'] ?></textarea>
  	</div>
</div>
</div>


<script>
    $(document).ready(function(){
        $('#autocomplete').typeahead({
            source: function (q, process) {
                return $.post('/ServiceSys/suggetProduct',
                {
                    query: q
                }, function (response) {
                    var data = [];
                    for (var i in response) {
                        data.push(response[i].title + "#" + response[i].code+'#'+response[i].product_id);
                    }
                    data.pop();
                    return process(data);
                });
            },
            highlighter: function (item) {
                var parts = item.split('#'),
                html =  '<div class="typeahead">';
                html += parts[0]+'-'+parts[1];
                html += '</div>';
                return html;
            },
            updater: function (item) {
                var parts = item.split('#');
                var html  = '';
                html += '<tr>';
                html += '<td>'+parts[0]+'-'+parts[1]+'</td>';
                html += '<td>'+'<input name="Field[gift]['+parts[2]+']" value="1" min="1" max="10" class="form-control FieldType_numbe"/>' + '</td>';
                html += '</tr>';
                $('#list_gift_inner').append(html);
                return parts[0];
            }

        });
    });
    function RemoveItem(e){
        var id = e.data('id');
        $('#itemId'+id).remove();
    }
</script>