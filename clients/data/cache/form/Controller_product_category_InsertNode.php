<div class="FormPart FormPartCol-6">
<div class="form-group FieldWrap FieldType_text Field_title RequiredField">
	<label class="col-sm-3 control-label" for="ID_Field[title]">Tiêu đề<span class="RequireField">*</span></label>
    <div class="col-sm-9">
		<input type="text" name="Field[title]" id="ID_Field[title]" class="form-control FieldType_text Field_title RequiredField" value="<?php echo $Fields['title']['value'] ?>" />
 	</div>
</div>
<div class="form-group FieldWrap FieldType_url Field_url RequiredField">
	<label class="col-sm-3 control-label" for="ID_Field[url]">Đường dẫn<span class="RequireField">*</span></label>
    <div class="col-sm-9">
		<input type="" name="Field[url]" id="ID_Field[url]" class="form-control FieldType_url Field_url RequiredField" value="<?php echo $Fields['url']['value'] ?>" />
 	</div>
</div>
<div class="form-group FieldWrap FieldType_multi_image Field_banner RequiredField">
    <label for="ID_Field[banner]" class="col-sm-3 control-label">Banner</label>
   	<input data-field-type="multi_image" type="hidden" class="form-control NodeField_MultiImages FieldType_multi_image Field_banner RequiredField" name="Field[banner][]" id="ID_banner" value=""/>
    <div class="col-sm-9">
    	<div class="clearfix">
            <button class="btn btn-primary" onclick="VNP.SugarBox.Open('MultiImages','banner'); return false;">Browse</button>
            <button class="btn btn-danger" onclick="document.getElementById('MultiImages_banner').innerHTML = ''; return false;">Delete all</button>
      	</div>
        <div class="clearfix MultiImages_Container" id="MultiImages_banner">
        	<?php $Images = explode(',', $Fields['banner']['value']); foreach($Images as $_Img) {
	            if(!empty($_Img)): ?>
	            <div class="MultiImages_Item">
	            	<input type="hidden" name="Field[banner][]" value="<?php echo $_Img ?>" />
	                <img src="<?php echo THUMB_BASE ?>60_60<?php echo $_Img ?>" width="60"/>
	                <a href="#" onclick="this.parentNode.parentNode.removeChild(this.parentNode); return false;">Delete</a>
	          	</div>
	            <?php endif;
            } ?>
        </div>
    </div>
</div>
<div class="form-group FieldWrap FieldType_image Field_mega_image clearfix">
	<label class="col-sm-3 control-label" for="ID_Field[mega_image]">Mega Menu Image</label>
    <div class="col-sm-6">
    	<input type="text" name="Field[mega_image]" id="ID_Field[mega_image]" class="form-control FieldType_image Field_mega_image" value="<?php echo $Fields['mega_image']['value'] ?>"/>
   	</div>
    <div class="col-sm-3">
		<button class="btn btn-primary" type="button" onclick="VNP.SugarBox.Open('FileManager','ID_Field[mega_image]'); return false;">Browse</button>
		<button class="btn btn-danger" onclick="document.getElementById('ID_Field[mega_image]').value = ''; document.getElementById('Thumb_ID_Field[mega_image]').setAttribute('src', 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=='); return false;">Delete</button>
	</div>
    <div class="col-sm-9 col-sm-offset-3 FieldImagePreviewer"><img id="Thumb_ID_Field[mega_image]"></div>
</div>
</div>
<div class="FormPart FormPartCol-4">
<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_Field[parent_id]">Danh mục cha</label>
    <div class="col-sm-9">
        <select name="Field[parent_id]" id="ID_Field[parent_id]" class="form-control FieldType_referer Field_parent_id" >
            <?php foreach($Fields['parent_id']['Options'] as $Options):?>
			<option value="<?php echo $Options['value'] ?>"<?php if($Options['value'] == $Fields['parent_id']['value']): ?> selected="selected"<?php endif ?>><?php echo $Options['prefix'] . $Options['text'] ?></option>
			<?php endforeach ?>
        </select>
    </div>
</div>
<?php echo Backend::NodeExtraSettings(); ?>
</div>
