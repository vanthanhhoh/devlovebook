<div class="table-responsive">

	<form class="form-horizontal" action="<?php echo $SA ?>" method="post">
		<input type="hidden" name="SearchUsers" value="1">
		<table class="table table-bordered table-striped table-hover">
	        <colgroup>
	        	<col class="col-xs-2">
	            <col class="col-xs-1">
	            <col class="col-xs-2">
	            <col class="col-xs">
	            <col class="col-xs-2">
	            <col class="col-xs">
	            <col class="col-xs-1">
	            <col class="col-xs">
	            <col class="col-xs-1">
	            <col class="col-xs-1">
	        </colgroup>
	        <tbody>
	        	<tr>
	        		<td><input type="text" name="Search[q]" placeholder="Search keyword" class="form-control" value="<?php echo $Search['q'] ?>" /></td>
	        		<td><label class="control-label"><?php echo Lang::get_string('search_by') ?></label></td>
	        		<td>
	        			<?php $SearchBy = array('username' => 'Username','fullname' => 'Full name','email' => 'Email') ?>
	        			<select class="form-control" name="Search[searchby]">
	        				<?php foreach($SearchBy as $SK => $SB) { ?>
	        				<option value="<?php echo $SK ?>"<?php if($SK == $Search['searchby']) { ?> selected<?php } ?>><?php echo $SB ?></option>
	        				<?php } ?>
	        			</select>
	        		</td>
	        		<td><label class="control-label">Level</label></td>
	        		<td>
	        			<select class="form-control" name="Search[level]">
		        			<option value="-1"<?php if($Search['level'] == '-1') { ?> selected<?php } ?>>All</option>
		        			<?php foreach($Levels as $LevelName => $Level) { ?>
	        				<option value="<?php echo $LevelName ?>"<?php if($Search['level'] == $LevelName) { ?> selected<?php } ?>><?php echo $Level['name'] ?></option>
	        				<?php } ?>
	        			</select>
	        		</td>
	        		<td><label class="control-label">Status</label></td>
	        		<td>
	        			<select class="form-control" name="Search[status]">
		        			<option value="all"<?php if($Search['status'] == 'all') { ?> selected<?php } ?>>All</option>
	        				<option value="1"<?php if($Search['status'] == '1') { ?> selected<?php } ?>>Active</option>
	        				<option value="0"<?php if($Search['status'] == '0') { ?> selected<?php } ?>>Inactive</option>
	        			</select>
	        		</td>
	        		<td><label class="control-label">Show</label></td>
	        		<td><input type="number" class="form-control" value="<?php echo $Search['limit'] ?>" name="Search[limit]" /></td>
	        		<td><input type="submit" class="btn btn-primary" value="Search" /></td>
				</tr>
			</tbody>
		</table>
	</form>


	<?php if(!empty($Users)) { ?>
	<div class="btn-group" style="margin-bottom: 5px">
		<div class="btn-group">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				<?php echo Lang::get_string('action_btn') ?>
				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu" role="menu">
				<li><a href="#" id="VNP_DeactiveUser"><span class="glyphicon glyphicon-remove"></span>&nbsp&nbsp<?php echo Lang::get_string('deactive') ?></a></li>
				<li><a href="#" id="VNP_ActiveUser"><span class="glyphicon glyphicon-ok"></span>&nbsp&nbsp<?php echo Lang::get_string('active') ?></a></li>
				<li><a href="#" id="VNP_ChangeUserLevel"><span class="glyphicon glyphicon-star-empty"></span>&nbsp&nbspChange level</a></li>
				<li><a href="#" id="VNP_DeleteUser"><span class="glyphicon glyphicon-trash"></span>&nbsp&nbsp<?php echo Lang::get_string('delete') ?></a></li>
			</ul>
		</div>
	</div>
    <table class="table table-bordered table-striped table-hover">
        <colgroup>
        	<col class="col-xs">
            <col class="col-xs">
        	<col class="col-xs">
        	<col class="col-xs">
        	<col class="col-xs">
        	<col class="col-xs">
        	<col class="col-xs">
        	<col class="col-xs">
        	<col class="col-xs">
        </colgroup>
        <thead>
	        <tr>
	        	<td>
	        		<div class="checkbox checkbox-success">
                        <input id="toggle-all" value="1" type="checkbox" />
                        <label for="toggle-all"></label>
                    </div>
	        	</td>
	        	<td><a href="<?php echo Router::Generate('ControllerParams', array('controller' => 'User', 'action' => 'Main', 'params' => 'sortby/userid/order/' . $Search['re_order'])) ?>" >ID</a></td>
	        	<td><strong><a href="<?php echo Router::Generate('ControllerParams', array('controller' => 'User', 'action' => 'Main', 'params' => 'sortby/username/order/' . $Search['re_order'])) ?>" >Username</a></strong></td>
	        	<td><strong><a href="<?php echo Router::Generate('ControllerParams', array('controller' => 'User', 'action' => 'Main', 'params' => 'sortby/fullname/order/' . $Search['re_order'])) ?>" >Full name</a></strong></td>
	        	<td><strong><strong><a href="<?php echo Router::Generate('ControllerParams', array('controller' => 'User', 'action' => 'Main', 'params' => 'sortby/email/order/' . $Search['re_order'])) ?>" >Email</a></strong></strong></td>
	        	<td><strong><a href="<?php echo Router::Generate('ControllerParams', array('controller' => 'User', 'action' => 'Main', 'params' => 'sortby/level/order/' . $Search['re_order'])) ?>" >Level</a></strong></td>
	        	<td><strong><a href="<?php echo Router::Generate('ControllerParams', array('controller' => 'User', 'action' => 'Main', 'params' => 'sortby/regdate/order/' . $Search['re_order'])) ?>" >Registered</a></strong></td>
	        	<td><strong><a href="<?php echo Router::Generate('ControllerParams', array('controller' => 'User', 'action' => 'Main', 'params' => 'sortby/last_activity/order/' . $Search['re_order'])) ?>" >Last activity</a></strong></td>
	        	<td><strong>Featured</strong></td>
	        </tr>
	    </thead>
	    <tbody style="font-size:12px">
	    	<?php foreach($Users as $UID => $User) { ?>
	    	<?php $Level = $Levels[$User['level']] ?>
	    	<tr class="<?php if($User['status'] == 0) { ?>User_Inactive<?php }else{ ?>User_Active<?php } ?> <?php echo $Level['class'] ?>" id="user-<?php echo $UID ?>">
	    		<td>
	    			<div class="checkbox checkbox-success">
                        <input type="checkbox" value="<?php echo $UID ?>" id="checkuser-<?php echo $UID ?>" data-title="<?php echo $UID ?> - <?php echo $User['username'] ?>" class="item-toggle" />
                        <label for="checkuser-<?php echo $UID ?>"></label>
                    </div>
	    		</td>
	    		<td><?php echo $UID ?></td>
	    		<td><?php echo $User['username'] ?></td>
	    		<td><?php echo $User['fullname'] ?></td>
	    		<td><?php echo $User['email'] ?></td>
	    		<td><?php echo $Level['name'] ?></td>
	    		<td><?php echo Filter::UnixTimeToDate($User['regdate'],true) ?></td>
	    		<td><?php echo Filter::UnixTimeToDate($User['last_activity'], true) ?></td>
	    		<td>
                	<a href="<?php echo Router::Generate('ControllerParams', array('controller' => 'User', 'action' => 'AddUser', 'params' => $UID)) ?>"><span class="glyphicon glyphicon-edit"></span>&nbsp<?php echo Lang::get_string('edit') ?></a>&nbsp;&nbsp;
                    <a href="<?php echo Router::Generate('ControllerParams', array('controller' => 'User', 'action' => 'Remove', 'params' => 'user/' . $UID)) ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp<?php echo Lang::get_string('delete') ?></a>
              	</td>
	    	</tr>
	    	<?php } ?>
	    </tbody>
    </table>
    <?php } ?>
</div>
<script type="text/javascript">
function haha() {
}
function VNP_DeactiveUser(checkedInputs, titleData) {
	if(checkedInputs.length == 0) alert('You must select at least one item');
	else {
		if(confirm('Do you want to do this?')) {
			VNP.Ajax({
				url: 'User/Action',
				type: 'POST',
				data: {action: 'deactive',ids: checkedInputs},
				success: function(res) {
					if(res.status == 'ok') {
						$.each(res.items, function(i,v) {
							$('#user-' + v).removeClass('User_Active');
							$('#user-' + v).addClass('User_Inactive');
						});
						VNP.Loader.TextNotify('Success deactive users!');
					}
					//VNP.Loader.hide();
				}
			}, 'json');
		}
	}
}
function VNP_ActiveUser(checkedInputs, titleData) {
	if(checkedInputs.length == 0) alert('You must select at least one item');
	else {
		if(confirm('Do you want to do this?')) {
			VNP.Ajax({
				url: 'User/Action',
				type: 'POST',
				data: {action: 'active',ids: checkedInputs},
				success: function(res) {
					if(res.status == 'ok') {
						$.each(res.items, function(i,v) {
							$('#user-' + v).addClass('User_Active');
							$('#user-' + v).removeClass('User_Inactive');
						});
						VNP.Loader.TextNotify('Success active users!');
					}
				}
			}, 'json');
		}
	}
}
function VNP_DeleteUser(checkedInputs, titleData) {
	if(checkedInputs.length == 0) alert('You must select at least one item');
	else {
		if(confirm('Do you want to do this?')) {
			VNP.Ajax({
				url: 'User/Action',
				type: 'POST',
				data: {action: 'delete',ids: checkedInputs},
				success: function(res) {
					if(res.status == 'ok') {
						$.each(res.items, function(i,v) {
							$('#user-' + v).remove();
						})
					}
					VNP.Loader.TextNotify('Success delete users!');
				}
			}, 'json');
		}
	}
}
$(document).ready(function() {
	var checkedInputs = '';
	var titleData = '';
	$('#toggle-all').InputToggle({
		childInput: '.item-toggle',
		dataAttribute: 'data-title',
		storageVar: 'checkedInputs',
		titleData:	'titleData',
		featureAction: [
			{container: '#VNP_DeactiveUser', callback: "VNP_DeactiveUser(checkedInputs, titleData)" },
			{container: '#VNP_ActiveUser', callback: "VNP_ActiveUser(checkedInputs, titleData)" },
			{container: '#VNP_DeleteUser', callback: "VNP_DeleteUser(checkedInputs, titleData)" }
		],
		callBackFunction: 'haha(checkedInputs)',
		enableCookie: true
	});
});
</script>