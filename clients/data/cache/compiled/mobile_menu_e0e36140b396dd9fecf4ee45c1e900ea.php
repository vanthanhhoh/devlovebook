<a class="sidr-class-sidr-button-close" onclick="closeSdir()" >Close</a>
<div class="nav_left_header">
    <?php if(USER_ID>0) { ?>
        <img src="<?php echo $User['avatar'] ?>" alt="<?php echo $User['email'] ?>" width="50" height="50" class="nav_left_avatar"/>
        <div class="nav_user_name">Xin chào ! <strong><a href="/Account/Profile"><?php echo $User['fullname'] ?> <i class="fa fa-angle-right" aria-hidden="true"></i></a> </strong></div>
        <a href="/logout" class="nav_logout">Đăng xuất</a>
    <?php }else{ ?>
        <img src="<?php echo $Op['logomobile'] ?>" alt="lovebook" class="logo_nav_left"/>
        <ul>
            <li>
                <a data-toggle="modal" data-target="#login">
                    Đăng nhập
                </a>
            </li>
            <li>
                <a data-toggle="modal" data-target="#registerModal">
                    Đăng kí
                </a>
            </li>
        </ul>
    <?php } ?>
</div>
<div class="list_menu_nav">
    <ul>
        <?php foreach($menu as $categorys) { ?>
            <li>
                <a href="#nav_left<?php echo $categorys['product_category_id'] ?>" data-toggle="tab"><h3><?php echo $categorys['title'] ?></h3></a>
                <?php echo $categorys['sub'] ?>
            </li>
        <?php } ?>
    </ul>
</div>