<div class="table-responsive">
	<form class="form-horizontal" action="<?php echo $SA ?>" method="post">
		<input type="hidden" name="SearchUsers" value="1">
		<table class="table table-bordered table-striped table-hover">
	        <colgroup>
	        	<col class="col-xs-2">
	            <col class="col-xs-1">
	            <col class="col-xs-2">
	            <col class="col-xs-1">
	            <col class="col-xs-2">
	            <col class="col-xs">
	            <col class="col-xs-1">
	            <col class="col-xs">
	            <col class="col-xs-1">
	            <col class="col-xs-1">
	        </colgroup>
	        <tbody>
                <tr>
                    <td>
                        <strong>Tìm trong</strong>
                    </td>
                    <td colspan="2">
                        <input type="text" class="form-control" placeholder="Theo sản phẩm"  id="searchByproduct"/>
                        <input type="hidden" class="form-control" name="Search[product_id]" id="product_id_value"/>
                    </td>
                    <td colspan="2">
                        <input type="text" class="form-control" placeholder="Theo thành viên"  id="searchByUser"/>
                        <input type="hidden" class="form-control"  name="Search[user_id]" id="user_id_value"/>
                    </td>
                </tr>
	        	<tr>
	        		<td><input type="text" name="Search[q]" placeholder="Search keyword" class="form-control" value="<?php echo $Search['q'] ?>" /></td>
	        		<td><label class="control-label">Tìm theo</label></td>
	        		<td>
	        			<select class="form-control" name="Search[searchby]">
	        				<?php foreach($SearchBy as $SK => $SB) { ?>
	        				<option value="<?php echo $SK ?>"<?php if($SK == $Search['searchby']) { ?> selected<?php } ?>><?php echo $SB ?></option>
	        				<?php } ?>
	        			</select>
	        		</td>
	        		<td><label class="control-label">Xếp theo</label></td>
	        		<td>
	        			
	        			<select class="form-control" name="Search[sortby]">
	        				<?php foreach($SortBy as $SK => $SB) { ?>
		        			<option value="<?php echo $SK ?>"<?php if($Search['sortby'] == $SK) { ?> selected<?php } ?>><?php echo $SB ?></option>
	        				<?php } ?>
	        			</select>
	        			
	        		</td>
	        		<td><label class="control-label">Status</label></td>
	        		<td>
	        			
	        			<select class="form-control" name="Search[status]">
		        			<option value="all"<?php if($Search['status'] == 'all') { ?> selected<?php } ?>>All</option>
	        				<option value="1"<?php if($Search['status'] == '1') { ?> selected<?php } ?>>Active</option>
	        				<option value="0"<?php if($Search['status'] == '0') { ?> selected<?php } ?>>Inactive</option>
	        			</select>
	        			
	        		</td>
	        		<td><label class="control-label">Show</label></td>
	        		<td><input type="number" class="form-control" value="<?php echo $Search['limit'] ?>" name="Search[limit]" /></td>
	        		<td><input type="submit" class="btn btn-primary" value="Search" /></td>
				</tr>
			</tbody>
		</table>
	</form>


	<div class="btn-group" style="margin-bottom: 5px">
		<div class="btn-group">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				Chọn chức năng				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu" role="menu">
				<li><a href="#" id="VNP_DeactiveNode"><span class="glyphicon glyphicon-remove"></span>&nbsp&nbspĐình chỉ</a></li>
				<li><a href="#" id="VNP_ActiveNode"><span class="glyphicon glyphicon-ok"></span>&nbsp&nbspKích hoạt</a></li>
				<li><a href="#" id="VNP_DeleteNode"><span class="glyphicon glyphicon-trash"></span>&nbsp&nbspXóa</a></li>
			</ul>
		</div>
	</div>
    <table class="table table-bordered table-striped table-hover">
        <colgroup>
        	<col class="col-xs" id="col-field-toggle">
            <col class="col-xs" id="col-field-title">
            <col class="col-xs-2" id="col-field-rate">
            <col class="col-xs-1" id="col-field-comment">
            <col class="col-xs-5" id="col-functions">
            <col class="col-xs">
        </colgroup>
        <thead>
	        <tr>
	        	<td>
	        		<div class="checkbox checkbox-success">
                        <input type="checkbox" id="VNP_ToggleAll" id="VNP_ToggleAll" value="1" />
                        <label for="VNP_ToggleAll"></label>
                    </div>
	        	</td>
	        	<td><a href="<?php echo Router::Generate('ControllerParams', array('controller' => 'review', 'action' => 'Main', 'params' => 'sortby/review_id/order/' . $Search['re_order'])) ?>"><strong>ID</strong></a></td>
	        		        		        				    	<td><a href="<?php echo Router::Generate('ControllerParams', array('controller' => 'review', 'action' => 'Main', 'params' => 'sortby/title/order/' . $Search['re_order'])) ?>"><strong>Title</strong></a></td>
		    		        		        		        				    	<td><a href="<?php echo Router::Generate('ControllerParams', array('controller' => 'review', 'action' => 'Main', 'params' => 'sortby/rate/order/' . $Search['re_order'])) ?>"><strong>Đánh giá</strong></a></td>
		    		        		        		        				    	<td><strong>Comment</strong></td>
	    			        		        	<td><strong>Functions</strong></td>
	        </tr>
	    </thead>
	    <tbody>
	    	<?php if(!empty($Nodes)) { ?>
	    	<?php foreach($Nodes as $NodeID => $Node) { ?>
	    	<tr id="node-<?php echo $Node['review_id'] ?>" class="<?php if($Node['status']) { ?>Node_Active<?php }else{ ?>Node_Inactive<?php } ?>">
	    		<td>
	    			<div class="checkbox checkbox-success">
                        <input type="checkbox" name="VNP_ToggleItem[]" class="VNP_ToggleItem" id="ToggleItem-<?php echo $Node['review_id'] ?>" data-title="<?php echo $Node['review_id'] ?>" value="<?php echo $Node['review_id'] ?>" />
                        <label for="ToggleItem-<?php echo $Node['review_id'] ?>"></label>
                    </div>
	    		</td>
	    		    <td><?php echo $Node['review_id'] ?></td>
	    			<td><?php echo $Node['title'] ?></td>
			    	<td><?php echo $Node['rate'] ?></td>
			    	<td><?php echo Filter::SubString($Node['comment'],100) ?></td>
                    <td>
                        <a href="<?php echo Router::EditNode('review', $NodeID) ?>"><span class="glyphicon glyphicon-edit"></span>&nbsp<?php echo Lang::get_string('edit') ?></a>&nbsp;&nbsp;
                        <a href="<?php echo Router::RemoveNode('review', $NodeID) ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp<?php echo Lang::get_string('delete') ?></a>
                    </td>
	    	</tr>
	    	<?php } ?>
	    	<?php } ?>
	    </tbody>
    </table>
</div>
<script src="<?php echo APPLICATION_DATA_DIR ?>theme/admin-vnp/assets/js/bootstrap-typeahead.min.js"></script>
<script>
    $(document).ready(function(){
        $('#searchByproduct').typeahead({
            source: function (q, process) {
                return $.post('/ServiceSys/suggetProduct',
                        {
                            query: q
                        }, function (response) {
                            var data = [];
                            for (var i in response) {
                                data.push(response[i].title + "#" + response[i].code+'#'+response[i].product_id);
                            }
                            data.pop();
                            return process(data);
                        });
            },
            highlighter: function (item) {
                var parts = item.split('#'),
                        html =  '<div class="typeahead">';
                html += parts[0]+'-'+parts[1];
                html += '</div>';
                return html;
            },
            updater: function (item) {
                var parts = item.split('#');
                $('#product_id_value').val(parts[2]);
                return parts[0];
            }

        });
        $('#searchByUser').typeahead({
            source: function (q, process) {
                return $.post('/admincp/User/suggetUser',
                        {
                            query: q
                        }, function (response) {
                            var data = [];
                            for (var i in response) {
                                data.push(response[i].userid + "#" + response[i].email);
                            }
                            data.pop();
                            return process(data);
                        });
            },
            highlighter: function (item) {
                var parts = item.split('#'),
                        html =  '<div class="typeahead">';
                html +=parts[1];
                html += '</div>';
                return html;
            },
            updater: function (item) {
                var parts = item.split('#');
                $('#user_id_value').val(parts[0]);
                return parts[1];
            }

        });
    })
</script>