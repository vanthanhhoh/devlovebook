<div class="table-responsive">
    <form class="form-horizontal" action="<?php echo $SA ?>" method="post">
        <input type="hidden" name="SearchUsers" value="1">
        <table class="table table-bordered table-striped table-hover">
            <colgroup>
                <col class="col-xs-2">
                <col class="col-xs-1">
                <col class="col-xs-2">
                <col class="col-xs-1">
                <col class="col-xs-2">
                <col class="col-xs">
                <col class="col-xs">
                <col class="col-xs">
                <col class="col-xs">
                <col class="col-xs">
            </colgroup>
            <tbody>
                <tr>
                    <td><input type="text" name="Search[q]" placeholder="Search keyword" class="form-control" value="<?php echo $Search['q'] ?>" /></td>
                    <td><label class="control-label">Tìm theo</label></td>
                    <td>
                        <select class="form-control" name="Search[searchby]">

                            <?php foreach($SearchBy as $SK => $SB) { ?>
                                <option value="<?php echo $SK ?>"<?php if($SK == $Search['searchby']) { ?> selected<?php } ?>><?php echo $SB ?></option>
                            <?php } ?>

                        </select>
                    </td>
                    <td><label class="control-label">Status</label></td>
                    <td>

                        <select class="form-control" name="Search[status]">
                            <option value="all"<?php if($Search['status'] == 'all') { ?> selected<?php } ?>>All</option>
                            <option value="1"<?php if($Search['status'] == '1') { ?> selected<?php } ?>>Đang chờ xác nhận</option>
                            <option value="2"<?php if($Search['status'] == '2') { ?> selected<?php } ?>>Đang chờ thanh toán</option>
                            <option value="3"<?php if($Search['status'] == '3') { ?> selected<?php } ?>>Đã xác nhận</option>
                            <option value="4"<?php if($Search['status'] == '4') { ?> selected<?php } ?>>Đang vận chuyển</option>
                            <option value="5"<?php if($Search['status'] == '5') { ?> selected<?php } ?>>Thành công</option>
                        </select>

                    </td>
                    <td><label class="control-label">Show</label></td>
                    <td><input type="number" class="form-control" value="<?php echo $Search['limit'] ?>" name="Search[limit]" /></td>
                    <td><input type="submit" class="btn btn-primary" value="Search" /></td>
                </tr>
            </tbody>
        </table>
    </form>

    <table class="table table-bordered table-striped table-hover">
        <colgroup>
            <col class="col-xs" id="col-field-id">
            <col class="col-xs" id="col-field-code">
            <col class="col-xs" id="col-field-edit_time">
            <col class="col-xs" id="col-field-status">
        </colgroup>
        <thead>
        <tr>
            <td><a href="<?php echo Router::Generate('ControllerParams', array('controller' => 'invoice_log', 'action' => 'listLog', 'params' => 'sortby/invoice_log_id/order/' . $Search['re_order'])) ?>"><strong>ID</strong></a></td>
            <td><a href="<?php echo Router::Generate('ControllerParams', array('controller' => 'invoice_log', 'action' => 'listLog', 'params' => 'sortby/invoice_id/order/' . $Search['re_order'])) ?>"><strong>Mã đơn hàng</strong></a></td>
            <td><a href="<?php echo Router::Generate('ControllerParams', array('controller' => 'invoice_log', 'action' => 'listLog', 'params' => 'sortby/edit_time/order/' . $Search['re_order'])) ?>"><strong>Thời gian</strong></a></td>
            <td><a href="<?php echo Router::Generate('ControllerParams', array('controller' => 'invoice_log', 'action' => 'listLog', 'params' => 'sortby/status/order/' . $Search['re_order'])) ?>"><strong>Trạng thái</strong></a></td>
        </tr>
        </thead>
        <tbody>
        <?php 
            $orderStatus = array(
            1=>'Đang chờ xác nhận',
            2=>'Đang chờ thanh toán',
            3=>'Đã xác nhận',
            4=>'Đang vận chuyển',
            5=>'Thành công'
            );
         ?>
        <?php if(!empty($Nodes)) { ?>
            <?php foreach($Nodes as $NodeID => $Node) { ?>
                <tr id="node-<?php echo $Node['invoice_log_id'] ?>">
                    <td><?php echo $Node['invoice_log_id'] ?></td>
                    <td><?php echo $Node['invoice_id'] ?></td>
                    <td><?php echo Filter::UnixTimeToFullDate($Node['edit_time'],true) ?></td>
                    <td><?php echo $orderStatus[$Node['status']] ?></td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>