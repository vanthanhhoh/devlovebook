<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $TITLE ?></title>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?php echo APP_DOMAIN.Router::GenerateThisRoute() ?>"/>
    <meta name="geo.region" content="VN-HN" />
    <meta name="geo.placename" content="Hà Nội" />
    <?php if(!empty($VNP_SiteImage)) { ?><meta property="og:image" content="<?php echo $VNP_SiteImage ?>"/><?php } ?>
    <meta content="INDEX, FOLLOW" name="ROBOTS">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicons
    ================================================== -->
    <?php foreach($META as $mk => $met) { ?><meta name="<?php echo $mk ?>" content="<?php echo $met ?>"/><?php } ?>
    <?php foreach($META_PROPERTY as $mk => $met) { ?><meta property="<?php echo $mk ?>" content="<?php echo $met ?>"/><?php } ?>
    <?php echo $Hook['header'] ?>
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css"  href="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/css/bootstrap.min.v3.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/css/lightslider.min.css"/>
    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css"  href="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/css/checkout.css"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/js/lightslider.min.js"></script>
</head>
<body>

<?php echo $BODY ?>

<div class="wapper_loader">

</div>
<script src="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/js/bootstrap.min.js"></script>
<script src="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/js/bootstrap-notify.min.js"></script>
<?php echo $Hook['footer'] ?>
</body>
</html>
