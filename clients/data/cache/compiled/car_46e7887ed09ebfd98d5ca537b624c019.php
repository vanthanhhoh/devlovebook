<div class="cart">
    <div class="container">

        <div class="cart-content">
            <div class="row">
                <div class="col-md-9">
                    <div class="cart-header-table">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                Hiện tại có <strong><?php echo $num ?></strong> sản phẩm trong giỏ hàng
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                Giá mua
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                Số lượng
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                Thành tiền
                            </div>
                        </div>
                    </div>
                    <div class="list-item">
                        <?php foreach($cart as $item) { ?>
                            <?php if($item['type']==1) { ?>
                                <?php 
                                    $product = $p[$item['product_id']];
                                    $gift = unserialize($product['gift']);
                                 ?>
                                <div class="item-cart">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                            <div class="item-cart-image">
                                                <img src="<?php echo $product['image'] ?>" alt="<?php echo $product['title'] ?>"/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <div class="item-cart-infomation">
                                                <h3><?php echo $product['title'] ?></h3>
                                                <?php if($product['price_sale']>0) { ?>
                                                    <?php 
                                                        $disc = $product['price_sale']/$product['price']; $disc = ceil($disc*100);
                                                        $disc = 100-$disc;
                                                     ?>
                                                    <div class="pull-left">
                                                        <span class="discount-icon">GIẢM GIÁ: <?php echo $disc ?>%</span>
                                                    </div>
                                                <?php } ?>
                                                <?php if(!empty($gift)) { ?>
                                                    <div class="pull-left" style="margin-left: 10px">
                                                        <span class="gift-icon"><i class="fa fa-gift" aria-hidden="true"></i> Có quà tặng kèm</span>
                                                    </div>
                                                <?php } ?>
                                                <div class="clearfix"></div>
                                                <button class="btn btn-default" onclick="removeItem($(this))" data-id="<?php echo $product['product_id'] ?>">
                                                    <i class="fa fa-trash" aria-hidden="true"></i> Xóa sản phẩm này
                                                 </button>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                            <div class="giamua">
                                                <?php $cprice=0 ?>
                                                <?php if($product['price_sale']>0) { ?>
                                                    <?php $cprice = $product['price_sale'] ?>
                                                    <span class="newprice"><?php echo Filter::NumberFormat($product['price_sale']) ?> đ</span>
                                                    <span class="oldprice"><?php echo Filter::NumberFormat($product['price']) ?> đ</span>
                                                <?php }else{ ?>
                                                    <?php $cprice = $product['price'] ?>
                                                    <span class="newprice"><?php echo Filter::NumberFormat($product['price']) ?> đ</span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                            <div class="quanlity">
                                                <select class="quantity" data-id="<?php echo $product['product_id'] ?>" data-qty="<?php echo $item['qty'] ?>" onchange="changeItem($(this))">
                                                    <option value="1" <?php if($item['qty']==1) { ?> selected="selected"<?php } ?>>1</option>
                                                    <option value="2" <?php if($item['qty']==2) { ?> selected="selected"<?php } ?>>2</option>
                                                    <option value="3" <?php if($item['qty']==3) { ?> selected="selected"<?php } ?>>3</option>
                                                    <option value="4" <?php if($item['qty']==4) { ?> selected="selected"<?php } ?>>4</option>
                                                    <option value="5" <?php if($item['qty']==5) { ?> selected="selected"<?php } ?>>5</option>
                                                    <option value="6" <?php if($item['qty']==6) { ?> selected="selected"<?php } ?>>6</option>
                                                    <option value="7" <?php if($item['qty']==7) { ?> selected="selected"<?php } ?>>7</option>
                                                    <option value="8" <?php if($item['qty']==8) { ?> selected="selected"<?php } ?>>8</option>
                                                    <option value="9" <?php if($item['qty']==9) { ?> selected="selected"<?php } ?>>9</option>
                                                    <option value="10" <?php if($item['qty']==10) { ?> selected="selected"<?php } ?>>10</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                            <div class="thanhtien">
                                                <?php $total = $cprice*$item['qty'] ?>
                                            </div>
                                            <strong><?php echo Filter::NumberFormat($total) ?> đ</strong>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="list-gift">
                        <div class="list-gift-header">Quà tặng kèm theo: </div>
                        <div class="row">
                            <?php foreach($cart as $item) { ?>
                                <?php if($item['type']==2) { ?>
                                    <?php $product = $p[$item['product_id']] ?>
                                    <div class="col-md-3">
                                        <div class="gift-item">
                                            <img src="<?php echo Output::GetThumbLink($product['image'],200,200) ?>" alt="<?php echo $product['title'] ?>"/>
                                            <span class="icon"><i class="fa fa-gift" aria-hidden="true"></i></span>
                                            <div class="title-gift">
                                                <h3><?php echo $product['title'] ?></h3>
                                                <span class="qty">Số lượng: <?php echo $item['qty'] ?> - Giá: <?php echo Filter::NumberFormat($product['price']) ?> đ</span>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="cart-sumary panel-default panel">
                        <div class="panel-body">
                            <p class="p_sum">Tổng giá: <strong><?php echo Filter::NumberFormat($ptotal) ?> đ</strong></p>
                            <p class="p_discount">Giảm giá: <strong>-<?php echo Filter::NumberFormat($giamgia) ?> đ</strong></p>
                            <?php if($discountCupon>0) { ?><p class="p_discount">Giảm giá theo mã: <strong>-<?php echo Filter::NumberFormat($discountCupon) ?> đ</strong></p> <?php } ?>
                            <p class="p_total">Thành tiền: <strong><?php echo Filter::NumberFormat($phaitra) ?> đ</strong></p>
                        </div>
                    </div>
                    <a href="/check-out.html" id="goCheckout">Tiến hành đặt hàng</a>
                    <div class="panel-group coupon" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne3" class="" aria-expanded="true">
                                        Mã giảm giá / Quà tặng
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne3" class="panel-collapse collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <?php if(empty($Cupon)) { ?>
                                        <div class="input-group">
                                            <input id="coupon" placeholder="Nhập ở đây.." type="text" class="form-control" value="">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default btn-coupon" type="button" onclick="addCupon()">Đồng ý</button>
                                            </span>
                                        </div>
                                        <?php }else{ ?>
                                        <div class="input-group">
                                            <input id="coupon" type="text" class="form-control" value="<?php echo $Cupon['code'] ?>" disabled>
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-coupon" type="button" onclick="removeCupon()">Xóa mã</button>
                                        </span>
                                        </div>
                                        <p class="usercode">Bạn đã sử dụng mã: <?php echo $Cupon['code'] ?>.
                                            <?php if($Cupon['discount_type']==1) { ?>
                                                Bạn được giảm : <?php echo Filter::NumberFormat($Cupon['discount']) ?> đ
                                                <?php }else{ ?>
                                                Bạn được giảm : <?php echo $Cupon['discount'] ?> % tổng đơn hàng
                                            <?php } ?>
                                        </p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <p class="lovebook_xu">
                                Với mỗi 100.000 trong đơn hàng các bạn sẽ tích lũy được 1000 xu trong tài khoản của các ban.
                                Xem thông tin về <a href="">Lovebook Xu</a>
                            </p>
                        </div>
                    </div>
                    -->
                </div>
            </div>
        </div>
    </div>
</div>