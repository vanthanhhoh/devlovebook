<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>User Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="<?php echo GLOBAL_DATA_DIR ?>library/bootstrap/bootstrap.min.v3.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo APPLICATION_DATA_DIR ?>template/css/login.css" />
        <link href="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
    	<div class="container">
        	<div class="row col-md-4 col-md-offset-4" id="LoginForm">
                <form name="UserLogin" method="post" action="login" class="form-horizontal" role="form">
                    <h3 class="form-title"><i class="fa fa-lock"></i> Đăng nhập vào quản trị</h3>
                	<div class="form-group">
                        <span class="icon"><i class="fa fa-user"></i></span>
                        <input name="username" type="text" class="form-control" id="Username" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <span class="icon"><i class="fa fa-unlock-alt"></i></span>
                        <input name="password" type="password" class="form-control" id="Password" placeholder="Password">
                    </div>
                    <input type="hidden" name="LoginSubmit" value="1"/>
                    <div style="text-align:center">
                    	<input type="submit" class="btn btn-primary" value="Submit"/>
                  	</div>
                </form>
          	</div>
      	</div>
        <div class="background"></div>
    </body>
</html>