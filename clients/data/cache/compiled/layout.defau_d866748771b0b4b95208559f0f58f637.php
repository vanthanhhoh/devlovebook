<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $TITLE ?></title>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?php echo APP_DOMAIN.Router::GenerateThisRoute() ?>"/>
    <?php foreach($META_PROPERTY as $mk => $met) { ?><meta property="<?php echo $mk ?>" content="<?php echo $met ?>"/>
    <?php } ?>

    <meta name="geo.region" content="VN-HN" />
    <meta name="geo.placename" content="Hà Nội" />
    <meta name="geo.position" content="20.999552;105.824976" />
    <meta name="ICBM" content="20.999552, 105.824976" />
    <?php if(!empty($VNP_SiteImage)) { ?><meta property="og:image" content="<?php echo $VNP_SiteImage ?>"/><?php } ?>

    <meta content="INDEX, FOLLOW" name="ROBOTS">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php foreach($META as $mk => $met) { ?><meta name="<?php echo $mk ?>" content="<?php echo $met ?>"/>
    <?php } ?>

    <?php echo $Hook['header'] ?>

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css"  href="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/css/bootstrap.min.v3.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/css/font-awesome.min.css"/>

    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css"  href="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/css/style.css"/>
    <link rel="stylesheet" type="text/css"  href="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/css/responsive.css"/>
    <link rel="stylesheet" href="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/libs/raty/jquery.raty.css"/>
    <link rel="stylesheet" href="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/css/lightslider.min.css"/>
    <link rel="stylesheet" href="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/css/jquery.sidr.css"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body data-spy="scroll" data-target="#myScrollspy" data-offset="20">
<?php echo $Hook['before_body'] ?>
<div class="header_bar">
    <div class="container">
        <div class="user-entry">
            <h1>Sách ôn thi Đại học - Lovebook</h1>
        </div>
        <div class="quick-menu">
            <?php echo Theme::getArea('HEADER_BAR') ?>
        </div>
    </div>
</div>
<div class="header hidden-sm hidden-xs">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-6 no-padding-right">
                <div class="logo">
                    <a href="<?php echo BASE_DIR ?>" title="Lovebook">
                        <img src="<?php echo $Op['logo'] ?>" alt="lovebook.vn"/>
                    </a>
                </div>
            </div>
            <div class="col-md-5 col-sm-5 hidden-xs">
                <div class="seach-box">
                    <div class="search-input">
                        <input type="text" placeholder="Nhập từ khóa cần tìm kiếm" class="autocompleteSearch"/>
                        <button id="search-submit">Tìm kiếm</button>
                    </div>
                    <div class="seach-sugget">
                        Phổ biến: sách ôn thi, sách ôn thi đại học, ôn tập đại học
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 hidden-xs">
                <div class="active-button shopping">
                    <a href="/gio-hang.html" rel="nofollow" title="Giỏ hàng">
                        <div class="active-button-inner">
                            <div class="addcart-goods-num">
                                <?php 
                                    if(isset($_SESSION['cart'])) {
                                    echo count($_SESSION['cart']);
                                    }
                                    else {
                                    echo "0";
                                    }
                                 ?>
                            </div>
                            <div class="pull-left active-button-icon">

                            </div>
                            <div class="pull-left active-button-name">
                                Mua Hàng
                            </div>
                        </div>
                    </a>
                </div>
                <?php if(USER_ID==0) { ?>
                    <div class="active-button auth" data-toggle="modal" data-target="#login">
                        <div class="active-button-inner">
                            <div class="pull-left active-button-icon">

                            </div>
                            <div class="pull-left active-button-name">
                                Đăng nhập
                            </div>
                        </div>
                    </div>
                    <?php }else{ ?>
                    <?php $User=$_SESSION['UserInfo'] ?>
                    <div class="user-name js-header-user-name dropdown">
                        <div class="user-name-link user-ajax-link"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" id="dropdownMenu1">
                            <div class="user-avatar">
                                <img width="34" height="34" src="<?php echo $User['avatar'] ?>" alt="<?php echo $User['fullname'] ?>"/>
                            </div>
                            <ul>
                                <li class="user-name-short"><span>Chào, <?php echo Filter::SubString($User['fullname'],10) ?></span></li>
                                <li class="user-name-account">
                                    <span>Tài khoản</span>
                                    <span>&amp; Đơn hàng</span>
                                </li>
                            </ul>
                            <span class="caret"></span>
                        </div>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="/Account/managerInvoice">Đơn hàng của tôi</a></li>
                            <li><a href="/Account/Notification">Thông báo của tôi</a></li>
                            <li><a href="/Account/Profile">Tài khoản của tôi</a></li>
                            <li><a href="/Account/Favorite">Danh sách yêu thích</a></li>
                            <li><a href="/Account/Review">Đánh giá của tôi</a></li>
                            <li><a href="/Account/Coin">Lovebook xu</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/logout">Thoát tài khoản</a></li>
                        </ul>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>
</div>
<div id="nav_left_inner"></div>
<div class="header_mobile hidden-md hidden-lg">
   <div class="container">
       <div class="header_mobile_inner">
           <div class="nav_mobile_left">
               <a href="#nav_left_inner" id="nav_left"></a>
           </div>
           <div class="logo-mobile">
               <a href="<?php echo BASE_DIR ?>" title="Lovebook">
                   <img src="<?php echo $Op['logomobile'] ?>" alt="lovebook.vn"/>
               </a>
           </div>
           <div class="nav_mobile_right">
               <a href="/gio-hang.html"></a>
           </div>
       </div>
       <div class="mobile_search">
           <div class="search-input">
               <input type="text" placeholder="Nhập từ khóa cần tìm kiếm" class="autocompleteSearch"/>
               <button id="search-submit">Tìm kiếm</button>
           </div>
       </div>
   </div>
</div>
<div class="navigator hidden-xs hidden-sm">
    <div class="container">
        <div class="row">
            <div class="col-md-3 no-padding-right">
                <div class="menu-navigator">
                    <i></i>
                    <span <?php if($ROUTE_NAME!='Home') { ?>id="category_list_show" class="dropdown-toggle" data-toggle="dropdown"<?php } ?>>Danh mục</span>
                    <?php if($ROUTE_NAME!='Home') { ?>
                        <div class="list-category-wapper dropdown-menu" id="menu-out-home">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-3 no-padding-right">
                                        <ul class="all-category">
                                            <?php foreach($menu as $categorys) { ?>
                                                <li>
                                                    <a href="#menu<?php echo $categorys['product_category_id'] ?>" data-toggle="tab"><h3><?php echo $categorys['title'] ?></h3></a>
                                                    <?php if($categorys['sub']!='') { ?>
                                                        <div class="mega_menu">
                                                            <div class="row">
                                                                <div class="col-md-9">
                                                                    <div class="col-mega-menu">
                                                                        <h4>Danh mục</h4>
                                                                        <?php echo $categorys['sub'] ?>
                                                                    </div>
                                                                    <div class="col-mega-menu">
                                                                        <h4>Tác giả</h4>
                                                                        <ul>
                                                                            <?php foreach($categorys['author'] as $writer) { ?>
                                                                                <?php $url = Router::Generate('ProductAuthor',array('author'=>$author[$writer]['url'])) ?>
                                                                                <li>
                                                                                    <a href="<?php echo $url ?>"><?php echo $author[$writer]['title'] ?></a>
                                                                                </li>
                                                                            <?php } ?>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <img src="<?php echo $categorys['mega_image'] ?>" alt="<?php echo $categorys['title'] ?>" width="100%"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </li>
                                            <?php } ?>
                                            <li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-9" style="padding-left: 0">
                <div class="bg-inner gray">
                    <div class="site-menu">
                        <?php echo Theme::getArea('MAIN_MENU') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="after_header">
    <?php if($ROUTE_NAME!='Home') { ?>
        <div class="state">
            <div class="container">
                <?php if(!empty($Helper['States'])) { ?>
                    <ol class="breadcrumb" id="board-breadcrumbs">
                        <li><a href="<?php echo APP_DOMAIN ?>">Trang chủ</a></li>
                        <?php foreach($Helper['States'] as $StateItem) { ?>
                            <li><a href="<?php echo $StateItem['route'] ?>"><?php echo $StateItem['title'] ?></a></li>
                        <?php } ?>
                    </ol>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <div class="notify">
        <div class="container">
            <?php echo $Notify ?>
        </div>
    </div>
    <?php echo $BODY ?>
    <?php echo Theme::getArea('HOME1') ?>
    <?php echo Theme::getArea('HOME2') ?>
    <?php echo Theme::getArea('HOME3') ?>
    <?php echo Theme::getArea('HOME4') ?>
    <?php echo Theme::getArea('HOME5') ?>
    <?php echo $Hook['footer'] ?>
    <div class="introduction_footer">
        <div class="container">
            <h4 class="title_block">Mua sách tại lovebook.vn</h4>
            <div class="introduction_inner">
                <div class="row">
                    <div class="col-md-1 col-sm-4 col-xs-12 text-center">
                        <img src="/clients/data/uploads/2016-7/logo-empty-png.png" alt="mua hàng ở lovebook.vn"/>
                    </div>
                    <div class="col-md-11 col-sm-8 col-xs-12 introduction-text">
                        <?php echo $Op['introfotter'] ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <?php echo Theme::getArea('FOOTER_1') ?>
                </div>
                <div class="col-md-3 col-sm-3">
                    <?php echo Theme::getArea('FOOTER_2') ?>
                </div>
                <div class="col-md-3 col-sm-3">
                    <?php echo Theme::getArea('FOOTER_3') ?>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="fb-page" data-href="https://www.facebook.com/Lovebook.vn" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-width="100%"><blockquote cite="https://www.facebook.com/sinhvienbachkhoa/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Lovebook.vn">Lovebook</a></blockquote></div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="lovebookInfomation">
                        <p class="strong">© 2016 - Bản quyền của Công Ty Cổ Phần Giáo Dục Trực tuyến Việt Nam Vedu</p>
                        <p>
                            Văn phòng HN: 101 Nguyễn Ngọc Nại, Thanh Xuân, Hà Nội<br>
                            ĐT: 043.888.6666 - Email: info@vedu.vn
                        </p>
                    </div>
                </div>
                <div class="col-md-6 text-right">
                    <div class="appect_payment">
                        <img src="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/images/appect_payment.png" alt="Chấp nhận thanh toán"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="after_header_wapper"></div>
</div>
<script src="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/js/jquery.validate.min.js"></script>
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="lovebook-close"></i>
                </button>
                <div class="head">
                    <h2>Đăng nhập</h2>
                    <p>
                        <span>Bạn chưa có tài khoản? </span>
                        <a href="#" data-toggle="modal" data-target="#registerModal" data-dismiss="modal">Đăng ký</a>
                    </p>
                </div>
            </div>
            <div class="modal-body">
                <div class="login_form">
                    <form action="" method="post" id="loginModal">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" value="" class="form-control" placeholder="Nhập địa chỉ email"/>
                        </div>
                        <div class="form-group">
                            <label>Mật khẩu</label>
                            <input type="password" name="password" value="" class="form-control" placeholder="Nhập mật khẩu"/>
                        </div>
                        <input type="hidden" name="ref" value="<?php echo Router::GenerateThisRoute(true) ?>"/>
                        <input type="hidden" name="login" value="1"/>
                        <p class="reset">Quên mật khẩu? Nhấn vào <a data-toggle="modal" data-target="#reset-password-form" href="#">đây</a></p>
                        <button type="button" class="login_modal">Đăng nhập</button>
                    </form>
                    <a onclick="openPopup()" class="remote_login"><i class="fa fa-facebook-square"></i> Đăng nhập bằng facebook</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="lovebook-close"></i>
                </button>
                <div class="head">
                    <h2>Đăng Kí</h2>
                    <p>
                        <span>Bạn đã có tài khoản rồi </span>
                        <a href="#" data-toggle="modal" data-target="#login" data-dismiss="modal">Đăng nhập</a>
                    </p>
                </div>
            </div>
            <div class="modal-body">
                <div class="login_form">
                    <form action="" method="post" id="registerFormModal">
                        <div class="form-group">
                            <input type="email" name="email" value="" class="form-control" placeholder="Địa chỉ email" required=""/>
                        </div>
                        <div class="form-group">
                            <input type="text" name="fullname" value="" class="form-control" placeholder="Họ tên đầy đủ" required=""/>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" id="Npassword" value="" class="form-control" placeholder="Nhập mật khẩu" required=""/>
                        </div>
                        <div class="form-group">
                            <input type="password"  value="" class="form-control" placeholder="Nhập lại mật khẩu" equalTo="#Npassword" required="" />
                        </div>
                        <div class="form-group">
                            <input type="text" minlength="10" maxlength="11" name="phone" class="form-control" placeholder="Số điện thoại"/>
                        </div>
                        <div class="form-group">
                            <div class="sliptTwo">
                                <div class="pull-left halfPart">
                                    <div class="input-group">
                                        <input type="text" class="form-control" value="" id="datepicker" name="birthday" placeholder="Ngày sinh">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="pull-left halfPart">
                                    <select name="gender" class="form-control">
                                        <option value="1">Boy</option>
                                        <option value="2">Girl</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="tokenregister" id="token_register" value=""/>
                        <button type="button" class="register_modal">Đăng kí tài khoản</button>
                    </form>
                    <a href="" class="remote_login"><i class="fa fa-facebook-square"></i> Đăng nhập bằng facebook</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wapper_loader">

</div>
<div class="wapper_sdir">

</div>
<script src="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/js/bootstrap.min.js"></script>
<script src="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/js/responsiveslides.min.js"></script>
<script src="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/js/lightslider.min.js"></script>
<script src="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/js/bootstrap-notify.min.js"></script>
<script src="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/libs/raty/jquery.raty.js"></script>
<script src="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/js/bootstrap-typeahead.min.js"></script>
<script src="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/js/jquery.sidr.min.js"></script>
<script src="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/js/function.js"></script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=262927320745197";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","url":"http:\/\/lovebook.vn\/","name":"Nhà sách giáo dục lovebook","alternateName":"Nhà sách giáo dục lovebook","potentialAction":{"@type":"SearchAction","target":"http:\/\/lovebook.vn\/tim-kiem.html?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"Person","url":"http:\/\/lovebook.vn\/","sameAs":["https:\/\/facebook.com\/Lovebook.vn","https:\/\/plus.google.com\/+NhàsáchLOVEBOOK","https:\/\/www.youtube.com\/channel\/UCslwNv1ZnhlidwXSxRX2dwg"],"name":"Lovebook.vn"}</script>
</body>
</html>
