<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover">
        <colgroup>
            <col class="col-xs">
        	<col class="col-xs">
            <col class="col-xs-2">
            <col class="col-xs-2">
            <col class="col-xs-2">
            <col class="col-xs">
            <col class="col-xs-2">
            <col class="col-xs-2">
            <col class="col-xs-2">
        </colgroup>
        <thead>
            <tr>
                <th></th>
            	<th><strong>IF</strong></th>
                <th><strong>Label</strong></th>
                <th><strong>Name</strong></th>
                <th><strong>Type</strong></th>
                <th><strong>R</strong></th>
                <th><strong>Filter</strong></th>
                <th><strong>Default value</strong></th>
                <th><strong>Actions</strong></th>
            </tr>
        </thead>
        <tbody id="NodeStructor">
        	<?php foreach($NodeType['NodeFields'] as $NodeField) { ?>
            <tr class="NodeType_Field" data-field-name="<?php echo $NodeField['name'] ?>">
                <td><span class="Move_Option"><span class="glyphicon glyphicon-resize-vertical"></span></span></td>
            	<td><span class="glyphicon glyphicon-<?php if($NodeField['inform'] == 1) { ?>ok<?php }else{ ?>ban<?php } ?>-circle"></span></td>
            	<td><a href="<?php echo $FieldAction ?>/EditField/<?php echo $NodeField['name'] ?>"><?php echo $NodeField['label'] ?></a></td>
                <td><?php echo $NodeField['name'] ?></td>
                <td><?php echo $NodeField['type'] ?></td>
                <td><span class="glyphicon glyphicon-<?php if($NodeField['require'] == 1) { ?>ok<?php }else{ ?>ban<?php } ?>-circle"></span></td>
                <td><?php echo $NodeField['filter'] ?></td>
                <td><?php echo $NodeField['value'] ?></td>
                <td>
                	<a href="<?php echo $FieldAction ?>/EditField/<?php echo $NodeField['name'] ?>"><span class="glyphicon glyphicon-edit"></span>&nbsp;Change</a>&nbsp;&nbsp;
                   	<a href="<?php echo $FieldAction ?>/DropField/<?php echo $NodeField['name'] ?>"><span class="glyphicon glyphicon-minus-sign"></span>&nbsp;Drop</a>
              	</td>
            </tr>
            <?php } ?>
        </tbody>
 	</table>
</div>
