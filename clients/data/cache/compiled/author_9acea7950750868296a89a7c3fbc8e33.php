<div class="book-category-style-1">
    <div class="container">
        <div class="box-title">
            <div class="row">
                <div class="col-md-3 no-padding-right">
                    <h2 class="box-title-text"><?php echo $data['block_title'] ?></h2>
                </div>
                <div class="col-md-9 no-padding-left-element" style="padding-left: 0">
                    <div class="box-title-line">
                        <ul class="sub-category hidden-xs">
                            <?php foreach($child as $cats) { ?>
                                <?php if($cats!=$category['product_category_id']) { ?>
                                    <li>
                                        <a href="/<?php echo $cat[$cats]['url'] ?>/"><?php echo $cat[$cats]['title'] ?></a>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="category-content">
            <div class="row">
                <div class="col-md-9 no-padding-right">
                    <div class="slider-category hidden-xs">
                        <?php 
                            if($category['banner']!=''){
                                $banner = explode(',',$category['banner']);
                            }
                            else $banner = array();
                         ?>
                        <ul class="slider-small-adapter">
                            <?php foreach($banner as $banners) { ?>
                                <li>
                                    <img src="<?php echo $banners ?>" width="100%" alt="<?php echo $category['title'] ?>"/>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="list-author">
                        <ul>
                            <?php foreach($authorOf as $item) { ?>
                                <?php 
                                    $auth = $author[$item];
                                    $url = Router::Generate('ProductAuthor',array('author'=>$auth['url']));
                                 ?>
                                <li>
                                    <a href="<?php echo $url ?>">
                                        <div class="pull-left author-image">
                                            <img src="<?php echo Output::GetThumbLink($auth['image'],80,80) ?>" alt="<?php echo $auth['title'] ?>" width="100%"/>
                                        </div>
                                        <div class="pull-left author-name">
                                            <p class="name"><?php echo $auth['title'] ?></p>
                                            <p class="avchievement"><?php echo $auth['description'] ?></p>
                                        </div>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="book-in-category">
            <ul class="group-tabs" role="tablist">
                <?php foreach($group as $gid => $item) { ?>
                    <li role="presentation" <?php if($gid==0) { ?>class="active"<?php } ?>><a href="#<?php echo $item['url'] ?>" aria-controls="<?php echo $item['url'] ?>" role="tab" data-toggle="tab"><?php echo $item['title'] ?></a></li>
                <?php } ?>
            </ul>

            <div class="tab-content">
                <?php foreach($group as $gid => $item) { ?>
                    <div role="tabpanel" class="tab-pane <?php if($gid==0) { ?>active in<?php } ?>" id="<?php echo $item['url'] ?>">
                        <div id="slider-<?php echo $item['url'] ?>">
                            <?php foreach($item['product'] as $book) { ?>
                                <?php 
                                    $read = Router::Generate('ProductReview',array('product'=>$book['url'],'pid'=>$book['product_id']));
                                    $url  = Router::Generate('ProductDetail',array('product'=>$book['url'],'pid'=>$book['product_id']));
                                 ?>
                                <div class="book-item">
                                    <a href="<?php echo $url ?>" title="<?php echo $book['title'] ?>">
                                        <div class="book-img pull-left">
                                            <img src="<?php echo Output::GetThumbLink($book['image'],300,425) ?>" alt="<?php echo $book['title'] ?>" width="100%"/>
                                        </div>
                                        <div class="book-infomation pull-left">
                                            <div class="book-title"><?php echo $book['title'] ?></div>
                                            <div class="book-author">TG: <?php echo $author[$book['author']]['title'] ?></div>
                                            <div class="book-price">
                                                <?php if($book['price_sale']>0) { ?>
                                                    <span class="newprice"><?php echo Filter::NumberFormat($book['price_sale']) ?> đ</span>
                                                    <span class="oldprice"><?php echo Filter::NumberFormat($book['price']) ?> đ</span>
                                                <?php }else{ ?>
                                                    <span class="newprice"><?php echo Filter::NumberFormat($book['price']) ?> đ</span>
                                                <?php } ?>
                                            </div>
                                            <div class="book-preview hidden-xs">
                                                <div class="star-only" data-number="5" data-score="3"></div>
                                                <span class="count-rate">(có 3 đánh giá)</span>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="book-active hidden-sm hidden-xs">
                                                <a href="<?php echo $read ?>" class="book-active-button read-frist fancybox">Đọc thử</a>
                                                <a href="<?php echo $url ?>" class="book-active-button buy-now">Mua ngay</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <a href="" class="view_all">Xem tất cả</a>
    </div>
</div>