<div class="table-responsive">
	<form action="" method="post">
    	<input type="hidden" name="RescanNodeTypes" value="1" />
        <p>
            <button type="submit" class="btn btn-success">
                <span class="glyphicon glyphicon-refresh"></span>&nbsp;Rescan NodeTypes
            </button>
      	</p>
    </form>
    <table class="table table-bordered table-striped table-hover">
        <colgroup>
        <col class="col-xs">
        <col class="col-xs-2">
        <col class="col-xs-2">
        <col class="col-xs">
        <col class="col-xs-6">
        </colgroup>
        <thead>
            <tr>
                <th><strong>ID</strong></th>
                <th><strong>Node type name</strong></th>
                <th><strong>Author</strong></th>
                <th><strong>Note require</strong></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        	<?php foreach($NodeTypes as $NodeTypeName => $NodeType) { ?>
        	<tr>
                <td><?php echo $NodeType['NodeTypeInfo']['code'] ?></td>
            	<td><?php echo $NodeType['NodeTypeInfo']['title'] ?></td>
                <td><?php echo $NodeType['NodeTypeInfo']['author'] ?></td>
                <td><?php echo $NodeType['NodeTypeInfo']['require']['node_type'] ?></td>
                <td>
                    <?php $url = Router::Generate('ControllerParams', array('controller' => 'NodeUtility', 'action' => 'InsertRow', 'params' => $NodeTypeName)) ?>
                	<a href="<?php echo $url ?>"><span class="glyphicon glyphicon-plus"></span>&nbsp;Insert</a>&nbsp;&nbsp;
                	<a href="#"><span class="glyphicon glyphicon-edit"></span>&nbsp<?php echo Lang::get_string('edit') ?></a>&nbsp;&nbsp;
                	<a href="<?php echo $StructureUrl ?>/Structure/<?php echo $NodeTypeName ?>"><span class="glyphicon glyphicon-certificate"></span>&nbsp;Structure</a>&nbsp;&nbsp;
                    <a href="<?php echo BASE_DIR ?>ControllerBuilder/Build/<?php echo $NodeTypeName ?>"><span class="glyphicon glyphicon-dashboard"></span>&nbsp;Build</a>&nbsp;&nbsp;
                    <a href="#"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Browse</a>&nbsp;&nbsp;
                    <a href="#"><span class="glyphicon glyphicon-trash"></span>&nbsp;Empty</a>&nbsp;&nbsp;
                   	<a href="#"><span class="glyphicon glyphicon-minus-sign"></span>&nbsp;Drop</a>
              	</td>
          	</tr>
            <?php } ?>
     	</tbody>
	</table>
</div>