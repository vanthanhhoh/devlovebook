<div class="review">
    <div class="header">
        <h2>Đọc thử sách <?php echo $product['title'] ?></h2>
    </div>
    <div class="content-review">
        <div class="book-mark">
            <div class="sumary-book">
                <div class="pull-left image">
                    <img src="<?php echo $product['image'] ?>" width="100%" alt="<?php echo $product['title'] ?>"/>
                </div>
                <div class="pull-left infomation">
                    <h2><?php echo $product['title'] ?></h2>
                    <p class="code">Mã: <?php echo $product['code'] ?></p>
                    <p class="author">Tác giả:
                        <?php 
                            if($product['author']>0){
                            $author[$product['author']]['title'];
                            }
                         ?>
                    </p>
                    <p class="giabia">Giá bìa: <?php echo Filter::NumberFormat($product['price']) ?> đ</p>
                    <p class="giaban">
                        Giá bán:
                        <?php if($product['price_sale']>0) { ?>
                        <?php echo Filter::NumberFormat($product['price_sale']) ?> đ
                            <?php }else{ ?>
                            <?php echo Filter::NumberFormat($product['price']) ?> đ
                        <?php } ?>
                    </p>

                </div>
                <div class="clearfix"></div>
                <p class="description">
                    <?php echo $product['discription'] ?>
                </p>
                <div class="add_cart" style="cursor: pointer">
                    <a href="/Cart/addCart/<?php echo $product['product_id'] ?>" target="_parent"> <i class="fa fa-shopping-cart" aria-hidden="true"></i> Mua ngay</a>
                </div>
            </div>
            <hr>
            <div class="box-title">Sách cùng tác giả</div>
            <div class="apdaper">
                <ul class="crosual" id="crosual">
                    <?php foreach($same as $item) { ?>
                        <?php $url = Router::Generate('ProductDetail',array('product'=>$item['url'],'pid'=>$item['product_id'])) ?>
                    <li>
                        <a href="<?php echo $url ?>" target="_parent">
                            <div class="pull-left image">
                                <img src="<?php echo $item['image'] ?>" width="100%" alt="<?php echo $item['title'] ?>"/>
                            </div>
                            <div class="pull-left infomation">
                                <h2><?php echo $item['title'] ?></h2>
                                <p class="code">Mã: <?php echo $item['code'] ?></p>
                                <p class="giabia">Giá bìa: <?php echo Filter::NumberFormat($item['price']) ?> đ</p>
                                <p class="giaban">
                                    Giá bán:
                                    <?php if($item['price_sale']>0) { ?>
                                        <?php echo Filter::NumberFormat($item['price_sale']) ?> đ
                                    <?php }else{ ?>
                                        <?php echo Filter::NumberFormat($item['price']) ?> đ
                                    <?php } ?>
                                </p>

                            </div>
                        </a>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="main-read">
            <iframe src="/pdfview/web/viewer.html?file=<?php echo $product['trailer'] ?>" class="box-read" frameborder="0" width="100%" height="100%"/>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#crosual').lightSlider({
            item:1,
            loop:false,
            slideMove:1,
            easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
            speed:600
        });
    });
    function quickBuyRv(id,qty){
        $.ajax({
            url:'Cart/addCart',
            type: 'post',
            dataType:'json',
            data:{id:id,qty:qty},
            success: function(res){
                if(res.status==1){
                    window.location.href='/gio-hang.html';
                }
            }
        });
    }
</script>