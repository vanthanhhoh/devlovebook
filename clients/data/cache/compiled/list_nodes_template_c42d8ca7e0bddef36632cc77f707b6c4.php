<div class="table-responsive">
	<form class="form-horizontal" action="{$SA}" method="post">
		<input type="hidden" name="SearchUsers" value="1">
		<table class="table table-bordered table-striped table-hover">
	        <colgroup>
	        	<col class="col-xs-2">
	            <col class="col-xs-1">
	            <col class="col-xs-2">
	            <col class="col-xs-1">
	            <col class="col-xs-2">
	            <col class="col-xs">
	            <col class="col-xs-1">
	            <col class="col-xs">
	            <col class="col-xs-1">
	            <col class="col-xs-1">
	        </colgroup>
	        <tbody>
	        	<?php if(!empty($S['ref_filters'])) { ?>
	        	<tr>
	        		<td><strong><?php echo Lang::get_string('search_in') ?></strong></td>
	        		<?php foreach($S['ref_filters'] as $RefField => $RefFilter) { ?>
	        		<td><strong><?php echo $RefFilter['label'] ?></strong></td>
	        		<td>
	        			<select class="form-control" name="Search[<?php echo $RefField ?>]">
	        			
	        			{function}
	        			foreach($Adapters['vnp_<?php echo $RefFilter['nodefield'] ?>_<?php echo $RefFilter['nodetype'] ?>'] as $Option) {{/function}
	        			<option value="{$Option.<?php echo $RefFilter['nodetype'] ?>_id}"{function}echo ($Option['<?php echo $RefFilter['nodetype'] ?>_id'] == $Search['<?php echo $RefField ?>']) ? ' selected' : ''{/function}
	        			>{$Option.prefix}{$Option.<?php echo $RefFilter['nodefield'] ?>}</option>
	        			{function}}{/function}
	        			
	        			</select>
	        		</td>
	        		<?php } ?>
	        	</tr>
	        	<?php } ?>
	        	<tr>
	        		<td><input type="text" name="Search[q]" placeholder="Search keyword" class="form-control" value="{$Search.q}" /></td>
	        		<td><label class="control-label"><?php echo Lang::get_string('search_by') ?></label></td>
	        		<td>
	        			<select class="form-control" name="Search[searchby]">
	        				
	        				{for $SB in $SearchBy as $SK}
	        				<option value="{$SK}"{if($SK == $Search.searchby)} selected{/if}>{$SB}</option>
	        				{/for}
	        				
	        			</select>
	        		</td>
	        		<td><label class="control-label"><?php echo Lang::get_string('sort_by') ?></label></td>
	        		<td>
	        			
	        			<select class="form-control" name="Search[sortby]">
	        				{for $SB in $SortBy as $SK}
		        			<option value="{$SK}"{if($Search.sortby == $SK)} selected{/if}>{$SB}</option>
	        				{/for}
	        			</select>
	        			
	        		</td>
	        		<td><label class="control-label">Status</label></td>
	        		<td>
	        			
	        			<select class="form-control" name="Search[status]">
		        			<option value="all"{if($Search.status == 'all')} selected{/if}>All</option>
	        				<option value="1"{if($Search.status == '1')} selected{/if}>Active</option>
	        				<option value="0"{if($Search.status == '0')} selected{/if}>Inactive</option>
	        			</select>
	        			
	        		</td>
	        		<td><label class="control-label">Show</label></td>
	        		<td><input type="number" class="form-control" value="{$Search.limit}" name="Search[limit]" /></td>
	        		<td><input type="submit" class="btn btn-primary" value="Search" /></td>
				</tr>
			</tbody>
		</table>
	</form>


	<div class="btn-group" style="margin-bottom: 5px">
		<div class="btn-group">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				<?php echo Lang::get_string('action_btn') ?>
				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu" role="menu">
				<li><a href="#" id="VNP_DeactiveNode"><span class="glyphicon glyphicon-remove"></span>&nbsp&nbsp<?php echo Lang::get_string('deactive') ?></a></li>
				<li><a href="#" id="VNP_ActiveNode"><span class="glyphicon glyphicon-ok"></span>&nbsp&nbsp<?php echo Lang::get_string('active') ?></a></li>
				<li><a href="#" id="VNP_DeleteNode"><span class="glyphicon glyphicon-trash"></span>&nbsp&nbsp<?php echo Lang::get_string('delete') ?></a></li>
			</ul>
		</div>
	</div>
    <table class="table table-bordered table-striped table-hover">
        <colgroup>
        	<col class="col-xs" id="col-field-toggle"><?php foreach($S['fields_order'] as $Field) { ?>
        	<col class="col-xs" id="col-field-<?php echo $Field ?>">
        <?php } ?><col class="col-xs" id="col-functions">
        </colgroup>
        <thead>
	        <tr>
	        	<td>
	        		<div class="checkbox checkbox-success">
                        <input type="checkbox" id="VNP_ToggleAll" id="VNP_ToggleAll" value="1" />
                        <label for="VNP_ToggleAll"></label>
                    </div>
	        	</td>
	        	<td><a href="{function}echo Router::Generate('ControllerParams', array('controller' => '<?php echo $NodeTypeName ?>', 'action' => 'Main', 'params' => 'sortby/<?php echo $NodeTypeName ?>_id/order/' . $Search['re_order'])){/function}"><strong>ID</strong></a></td>
	        	<?php foreach($S['fields_order'] as $Field) { ?>
	        	<?php $Field = $NodeFields[$Field] ?>
	        	<?php if(in_array($Field['name'], $SortBy)) { ?>
			    	<td><a href="{function}echo Router::Generate('ControllerParams', array('controller' => '<?php echo $NodeTypeName ?>', 'action' => 'Main', 'params' => 'sortby/<?php echo $Field['name'] ?>/order/' . $Search['re_order'])){/function}"><strong><?php echo $Field['label'] ?></strong></a></td>
		    	<?php }else{ ?>
			    	<td><strong><?php echo $Field['label'] ?></strong></td>
	    		<?php } ?>
	        	<?php } ?>
	        	<td><strong>Functions</strong></td>
	        </tr>
	    </thead>
	    <tbody>
	    	{if(!empty($Nodes))}
	    	{for $Node in $Nodes as $NodeID}
	    	<tr id="node-{$Node.<?php echo $NodeTypeName ?>_id}" class="{if($Node.status)}Node_Active{else}Node_Inactive{/if}">
	    		<td>
	    			<div class="checkbox checkbox-success">
                        <input type="checkbox" name="VNP_ToggleItem[]" class="VNP_ToggleItem" id="ToggleItem-{$Node.<?php echo $NodeTypeName ?>_id}" data-title="{$Node.<?php echo $NodeTypeName ?>_id}" value="{$Node.<?php echo $NodeTypeName ?>_id}" />
                        <label for="ToggleItem-{$Node.<?php echo $NodeTypeName ?>_id}"></label>
                    </div>
	    		</td>
	    		<td>{$Node.<?php echo $NodeTypeName ?>_id}</td>
	    		<?php foreach($S['fields_order'] as $Field) { ?>
		    		<?php $FieldName = $Field; $Field = $NodeFields[$Field] ?>
		    		<?php if($Field['type'] == 'referer') { ?>
		    			<?php $RefererField = $Field['referer']['node_field'] ?>
		    			
		    			{$FieldIdt = $Node['<?php echo $FieldName ?>'];}
                        {function}if($FieldIdt == '') $FieldIdt = 0{/function}
		    			
		    			<?php if($Field['display'] == 'single_selectbox') { ?>
			    			
			    			{$Adapter = $Adapters['vnp_<?php echo $Field['referer']['node_field'] ?>_<?php echo $Field['referer']['node_type'] ?>_id_key'][$FieldIdt]}
				    		<td>{$Adapter.<?php echo $RefererField ?>}</td>
				    		
				    	<?php }else{ ?>
				    		
				    		{$FieldIdts = array_map('trim', explode(',',$FieldIdt)); $Adapter = array()}
				    		{for $FieldIdt in $FieldIdts}
				    		{function}if(isset($Adapters['vnp_<?php echo $Field['referer']['node_field'] ?>_<?php echo $Field['referer']['node_type'] ?>'][$FieldIdt]['<?php echo $RefererField ?>']))
				    		array_push($Adapter, $Adapters['vnp_<?php echo $Field['referer']['node_field'] ?>_<?php echo $Field['referer']['node_type'] ?>'][$FieldIdt]['<?php echo $RefererField ?>'])
				    		{/function}
				    		{/for}
				    		<td>{function}echo implode(',', $Adapter){/function}</td>
				    		
				    	<?php } ?>
                    <?php } elseif($Field['type'] == 'list') { ?>
                        <td>
                        
                            {function}$refNodes = NodeBase::getNodesIn('<?php echo $Field['referer']['node_type'] ?>', '<?php echo $Field['referer']['node_field'] ?>', $Node['<?php echo $FieldName ?>']){/function}
                            {for $rn in $refNodes}
                                {$rn.<?php echo $Field['referer']['node_field'] ?>},
                            {/for}
                        
                        </td>
				    <?php } elseif($Field['type'] == 'single_value') { ?>
					    
					    {$FieldIdt = $Node['<?php echo $FieldName ?>']; $Opt = $Options['<?php echo $FieldName ?>']}
					    <td>{$Opt[$FieldIdt]}</td>
					    
					<?php } elseif($Field['type'] == 'image') { ?>
					    
					    <td><img src="{#THUMB_BASE}50_50{$Node.<?php echo $FieldName ?>}"></td>
					    
				    <?php }else{ ?>
			    	<td>{$Node.<?php echo $FieldName ?>}</td>
			    	<?php } ?>
	    		<?php } ?>
	    		<td>
                	<a href="{function}echo Router::EditNode('<?php echo $NodeTypeName ?>', $NodeID){/function}"><span class="glyphicon glyphicon-edit"></span>&nbsp{{lang::edit}}</a>&nbsp;&nbsp;
                    <a href="{function}echo Router::RemoveNode('<?php echo $NodeTypeName ?>', $NodeID){/function}"><span class="glyphicon glyphicon-trash"></span>&nbsp{{lang::delete}}</a>
              	</td>
	    	</tr>
	    	{/for}
	    	{/if}
	    </tbody>
    </table>
</div>