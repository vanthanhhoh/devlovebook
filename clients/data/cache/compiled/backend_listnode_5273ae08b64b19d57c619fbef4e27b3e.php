<div class="table-responsive">
	<form action="" method="post" class="form-horizontal">
		<input name="Build[Backend][ListNode]" type="hidden" value="Build List" />
		<input name="do_build" type="hidden" value="1" />
	    <table class="table table-bordered table-striped table-hover">
	        <colgroup>
	        	<col class="col-xs-6">
		        <col class="col-xs-2">
		        <col class="col-xs">
		        <col class="col-xs">
		        <col class="col-xs">
		        <col class="col-xs">
	        </colgroup>
	        <thead>
	            <tr>
	            	<th><strong>Settings</strong></th>
	                <th><strong>Fields</strong></th>
	                <th><strong>Show</strong></th>
	                <th><strong><?php echo Lang::get_string('sort_by') ?></strong></th>
	                <th><strong>Adv filters</strong></th>
	                <th><strong>Quick edit</strong></th>
	            </tr>
	        </thead>
	        <tbody>
	        	<tr>
	        		<?php $Rowspan = sizeof($NodeFields); $Rowspan++ ?>
	        		<td rowspan="<?php echo $Rowspan ?>">
	        			<table class="table table-bordered table-striped table-hover">
	        				<colgroup>
					        	<col class="col-xs-3">
						        <col class="col-xs-7">
					        </colgroup>
	        				<tr>
	        					<td><label class="col-sm-2 control-label" for="Settings_sort">Sort</label></td>
	        					<td>
									<select name="Settings[sort]" id="Settings_sort" class="form-control">
										<option value="DESC">DESC</option>
										<option value="ASC">ASC</option>
									</select>
								</td>
							</tr>
							<tr>
	        					<td><label class="col-sm-2 control-label" for="Settings_default_order">Default order</label></td>
	        					<td>
									<select name="Settings[default_order]" id="Settings_default_order" class="form-control">
										<option value="<?php echo $NodeType['name'] ?>_id" selected="selected"><?php echo $NodeType['title'] ?> ID</option>
										<?php foreach($NodeFields as $Field) { ?>
										<option value="<?php echo $Field['name'] ?>"><?php echo $Field['label'] ?></option>
										<?php } ?>
									</select>
								</td>
							</tr>
							<tr>
	        					<td><label class="col-sm control-label" for="Settings_limit">Number nodes</label></td>
	        					<td>
									<input name="Settings[limit]" id="Settings_limit" type="number" class="form-control" value="10" />
								</td>
							</tr>
							<tr>
	        					<td><label class="col-sm control-label">Fields order</label></td>
	        					<td><div id="FieldsOrder"></div></td>
							</tr>
						</table>
						<center><input type="submit" class="btn btn-primary" value="Build" /></center>
	        		</td>
	        	</tr>
	        	<?php foreach($NodeFields as $i => $Field) { ?>
	        	<tr class="list_node">
	        		<td><strong><?php echo $Field['label'] ?></strong></td>
	        		<td>
	        			<div class="checkbox checkbox-success">
	                        <input type="checkbox" id="show_fields-<?php echo $i ?>" class="CTL_ShowFields" name="Settings[show_fields][]" data-field-label="<?php echo $Field['label'] ?>" value="<?php echo $Field['name'] ?>" />
	                        <label for="show_fields-<?php echo $i ?>"></label>
	                    </div>
	        		</td>
	        		<td>
	        			<div class="checkbox checkbox-success">
	                        <input type="checkbox" id="sort_by-<?php echo $i ?>" name="Settings[sort_by][]" value="<?php echo $Field['name'] ?>" />
	                        <label for="sort_by-<?php echo $i ?>"></label>
	                    </div>
	        		</td>
	        		<td>
	        			<div class="checkbox checkbox-success">
	                        <input type="checkbox" id="advanced_filters-<?php echo $i ?>" name="Settings[advanced_filters][]" value="<?php echo $Field['name'] ?>" />
	                        <label for="advanced_filters-<?php echo $i ?>"></label>
	                    </div>
	        		</td>
	        		<td>
	        			<div class="checkbox checkbox-success">
	                        <input type="checkbox" id="quick_edit-<?php echo $i ?>" name="Settings[quick_edit][]" value="<?php echo $Field['name'] ?>" />
	                        <label for="quick_edit-<?php echo $i ?>"></label>
	                    </div>
	        		</td>
	        	</tr>
	        	<?php } ?>
	        </tbody>
	  	</table>
	</form>
</div>