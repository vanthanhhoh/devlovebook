<script>
    $(document).ready(function(){
        $('#paymentStatus').change(function() {
            var id = $(this).data('id');
            var status = $(this).val();
            $.ajax({
                url:'/admincp/invoice/changeStatus',
                type:'post',
                dataType:'text',
                data: {id:id,status:status},
                success: function(res){
                    if(res=='ok'){
                        alert('Thay đổi thành công!');
                        location.reload();
                    }
                    else {
                        alert(res);
                    }
                }
            });
        });
    });
</script>
<div class="row">
    <div class="col-xs-12" id="printarea">
        <!-- PAGE CONTENT BEGINS -->
        <div class="space-6"></div>

        <div class="widget-box transparent">
            <div class="widget-header widget-header-large">
                <h3 class="widget-title grey lighter">
                    <i class="ace-icon fa fa-leaf green"></i>
                    <?php echo $node['name'] ?>
                </h3>

                <div class="widget-toolbar no-border invoice-info">
                    <span class="invoice-info-label">Invoice:</span>
                    <span class="red">#<?php echo $node['code'] ?></span>
                    <br />
                    <span class="invoice-info-label">Date:</span>
                    <span class="blue"><?php echo Filter::UnixTimeToDate($node['add_time'],true) ?></span>
                    <span class="invoice-info-label">Update:</span>
                    <span class="blue">
                        <?php if($node['edit_time']>0) { ?>

                            <?php echo Filter::UnixTimeToDate($node['edit_time'],true) ?>
                            <?php }else{ ?>
                            -------
                        <?php } ?>
                    </span>
                </div>
                <div class="widget-toolbar">
                    <a id="print" style="cursor: pointer" onclick="printDiv('printarea')">
                        <i class="ace-icon fa fa-print"></i>
                    </a>
                </div>
                <?php 
                    $orderStatus = array(
                    1=>'Đang chờ xác nhận',
                    2=>'Đang chờ thanh toán',
                    3=>'Đã xác nhận',
                    4=>'Đang vận chuyển',
                    5=>'Thành công'
                    );
                 ?>
                <div class="widget-toolbar">
                    <select id="paymentStatus" class="form-control" data-id="<?php echo $node['invoice_id'] ?>">
                        <?php foreach($orderStatus as $sid => $status) { ?>
                            <option value="<?php echo $sid ?>"<?php if($sid==$node['invoice_status']) { ?>selected<?php } ?>><?php echo $status ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main">
                    <div>
                        <ul class="list-unstyled  spaced">
                            <li>
                                <i class="ace-icon fa fa-caret-right green"></i>Địa chỉ : <?php echo $node['address'] ?> , <?php echo $district[$node['district']]['title'] ?> , <?php echo $province[$node['province']]['title'] ?>
                            </li>
                            <li>
                                <i class="ace-icon fa fa-caret-right green"></i>Điện thoại : <?php echo $node['phone'] ?>
                            </li>
                            <li>
                                <i class="ace-icon fa fa-caret-right green"></i>Lưu ý : <?php echo $node['note'] ?>
                            </li>
                            <li>

                                <i class="ace-icon fa fa-caret-right green"></i>Trạng thái đơn hàng :
                                <strong>
                                <?php echo $orderStatus[$node['invoice_status']]; ?>
                                </strong>
                            </li>
                            <li>
                                <i class="ace-icon fa fa-caret-right green"></i>Hình thức thanh toán :
                                <?php echo $payment[$node['payment']]['title'] ?>
                            </li>
                            <li>
                                <i class="ace-icon fa fa-caret-right green"></i>Hình thức vận chuyển :
                                <?php echo $ship[$node['ship']]['title'] ?>
                            </li>

                        </ul>
                    </div>

                    <div class="space"></div>

                    <div>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Tên sản phẩm</th>
                                    <th>Giá gốc</th>
                                    <th>Giá khuyến mãi</th>
                                    <th>Số lượng</th>
                                    <th>Tổng cộng</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $giagoc = 0 ?>
                            <?php $dagiam=0 ?>
                            <?php foreach($order as $item) { ?>
                                <?php 
                                    $product = $p[$item['product_id']];
                                    $giagoc = $giagoc+($item['price_old']*$item['qty']);
                                    $dagiam+=$item['total'];
                                 ?>
                                <tr>
                                    <td>
                                        <a href=""><?php echo $product['title'] ?></a>
                                        <?php if($item['type']==2) { ?>
                                            <i>Quà tặng</i>
                                        <?php } ?>
                                    </td>
                                    <td><?php echo Filter::NumberFormat($item['price_old']) ?> đ</td>
                                    <td><?php echo Filter::NumberFormat($item['price']) ?> đ</td>
                                    <td><?php echo $item['qty'] ?></td>
                                    <td><?php echo Filter::NumberFormat($item['total']) ?> đ</td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <?php 
                                    $giamgia = $giagoc-$dagiam;
                                    $giamgiacupon =0;
                                    if($node['cupon']!='' && $node['cupon_discount']>0 && $node['cupon_type']!=''){
                                    if($node['cupon_type']==1){
                                    $giamgiacupon = $node['cupon_discount'];
                                    }
                                    else {
                                    $giamgiacupon = $dagiam*$node['cupon_discount']/100; $giamgiacupon = ceil($giamgiacupon);
                                    }
                                    }
                                 ?>
                                <td colspan="5" align="right" class="sumaryTable">
                                    <p>
                                        <span>Tổng chưa giảm:</span> <strong><?php echo Filter::NumberFormat($giagoc) ?> đ</strong>
                                    </p>
                                    <p>
                                        <span>Giảm giá:</span> <strong>-<?php echo Filter::NumberFormat($giamgia) ?> đ</strong>
                                    </p>
                                    <p>
                                        <span>Tổng đã giảm:</span> <strong><?php echo Filter::NumberFormat($dagiam) ?> đ</strong>
                                    </p>
                                    <p>
                                        <span>Giảm giá theo mã <?php echo $node['cupon'] ?>:</span> <strong>-<?php echo Filter::NumberFormat($giamgiacupon) ?> đ</strong>
                                    </p>
                                    <p>
                                        <span>Phí vận chuyển:</span> <strong></strong>
                                    </p>
                                    <hr/>
                                    <p>
                                        <?php $tongcong = $dagiam-$giamgiacupon ?>
                                        Tổng cộng: <strong><?php echo Filter::NumberFormat($tongcong) ?> đ</strong>
                                    </p>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div>

                    <div class="hr hr8 hr-double hr-dotted"></div>


                    <div class="space-6"></div>
                    <div class="well">
                       Cảm ơn đã sử dụng !
                    </div>
                </div>
            </div>
        </div>

        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div>
<script>

    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>