<form action="" id="Build_AddNodeForm" method="post">
	<input type="hidden" name="Build[Backend][InsertNode]" value="1" />
	<input type="hidden" name="build_form" value="1" />
	<input type="submit" class="btn btn-primary" value="Submit" />
	<?php foreach($NodeFields as $Field) { ?>
	<input class="SortedFields" type="hidden" name="sort_data[6][]" value="<?php echo $Field['name'] ?>" />
	<?php } ?>
	<input class="SortedFields" type="hidden" name="sort_data[4][]" value="VNP_SettingsForm" />
</form>
<div class="FeaturedPanel clearfix">
	<span class="VNP_PageInfo">Build insert node page</span>
		<ul class="FeaturedButtons">
			<li>
            </li>
       	</ul>
</div>
<div id="Build_InsertNodeForm" class="clearfix">
	<ul class="VNP_FormPart" data-cols="6">
		<?php foreach($NodeFields as $Field) { ?>
		<li class="FieldItem Move_Option" data-field-name="<?php echo $Field['name'] ?>"><?php echo $Field['label'] ?></li>
		<?php } ?>
	</ul>
	<ul class="VNP_FormPart" data-cols="4">
		<li class="FieldItem Move_Option" data-field-name="VNP_SettingsForm">Settings</li>
	</ul>
	<div class="clearfix"></div>
	<ul class="VNP_FormPart" data-cols="10" style="width:100%; margin: 15px 0"></ul>
</div>