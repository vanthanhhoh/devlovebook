<div class="slider hidden-xs hidden-sm">
    <div class="slider_carousel">
        <ul class="rslides">
            <?php foreach($slider as $item) { ?>
                <li>
                    <img src="<?php echo $item['image'] ?>" alt="<?php echo $item['title'] ?>" width="100%"/>
                </li>
            <?php } ?>
        </ul>
    </div>
    <script type="application/javascript">
        $(function() {
            var slheight = $('ul.rslides li img').width();
            var container = $('.container').width();
            var right = (slheight-container)/2;
            $(".rslides").responsiveSlides({
                speed: 2000
            });
            $('ul.rslides_tabs').css('right',right+'px');
        });
    </script>
    <div class="list-category-wapper">
        <div class="container">
            <div class="row">
                <div class="col-md-3 no-padding-right">
                    <ul class="all-category">
                        <?php foreach($menu as $categorys) { ?>
                            <li>
                                <a href="#menu<?php echo $categorys['product_category_id'] ?>" data-toggle="tab"><h3><?php echo $categorys['title'] ?></h3></a>
                                <?php if($categorys['sub']!='') { ?>
                                    <div class="mega_menu">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <div class="col-mega-menu">
                                                    <h4>Danh mục</h4>
                                                    <?php echo $categorys['sub'] ?>
                                                </div>
                                                <div class="col-mega-menu">
                                                    <h4>Tác giả</h4>
                                                    <ul>
                                                    <?php foreach($categorys['author'] as $writer) { ?>
                                                        <li><a href=""><?php echo $author[$writer]['title'] ?></a></li>
                                                    <?php } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <img src="<?php echo $categorys['mega_image'] ?>" alt="<?php echo $categorys['title'] ?>" width="100%"/>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </li>
                        <?php } ?>
                        <li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</div>