<div class="table-responsive">
	<form class="form-horizontal" action="<?php echo $SA ?>" method="post">
		<input type="hidden" name="SearchUsers" value="1">
		<table class="table table-bordered table-striped table-hover">
	        <colgroup>
	        	<col class="col-xs-2">
	            <col class="col-xs-1">
	            <col class="col-xs-2">
	            <col class="col-xs-1">
	            <col class="col-xs-2">
	            <col class="col-xs">
	            <col class="col-xs-1">
	            <col class="col-xs">
	            <col class="col-xs-1">
	            <col class="col-xs-1">
	        </colgroup>
	        <tbody>
	        		        	<tr>
	        		<td><strong>Tìm trong</strong></td>
	        			        		<td><strong>Danh mục</strong></td>
	        		<td>
	        			<select class="form-control" name="Search[product_category]">
	        			
	        			<?php 
	        			foreach($Adapters['vnp_title_product_category'] as $Option) { ?>
	        			<option value="<?php echo $Option['product_category_id'] ?>"<?php echo ($Option['product_category_id'] == $Search['product_category']) ? ' selected' : '' ?>
	        			><?php echo $Option['prefix'] ?><?php echo $Option['title'] ?></option>
	        			<?php } ?>
	        			
	        			</select>
	        		</td>
	        			        	</tr>
	        		        	<tr>
	        		<td><input type="text" name="Search[q]" placeholder="Search keyword" class="form-control" value="<?php echo $Search['q'] ?>" /></td>
	        		<td><label class="control-label">Tìm theo</label></td>
	        		<td>
	        			<select class="form-control" name="Search[searchby]">
	        				
	        				<?php foreach($SearchBy as $SK => $SB) { ?>
	        				<option value="<?php echo $SK ?>"<?php if($SK == $Search['searchby']) { ?> selected<?php } ?>><?php echo $SB ?></option>
	        				<?php } ?>
	        				
	        			</select>
	        		</td>
	        		<td><label class="control-label">Xếp theo</label></td>
	        		<td>
	        			
	        			<select class="form-control" name="Search[sortby]">
	        				<?php foreach($SortBy as $SK => $SB) { ?>
		        			<option value="<?php echo $SK ?>"<?php if($Search['sortby'] == $SK) { ?> selected<?php } ?>><?php echo $SB ?></option>
	        				<?php } ?>
	        			</select>
	        			
	        		</td>
	        		<td><label class="control-label">Status</label></td>
	        		<td>
	        			
	        			<select class="form-control" name="Search[status]">
		        			<option value="all"<?php if($Search['status'] == 'all') { ?> selected<?php } ?>>All</option>
	        				<option value="1"<?php if($Search['status'] == '1') { ?> selected<?php } ?>>Active</option>
	        				<option value="0"<?php if($Search['status'] == '0') { ?> selected<?php } ?>>Inactive</option>
	        			</select>
	        			
	        		</td>
	        		<td><label class="control-label">Show</label></td>
	        		<td><input type="number" class="form-control" value="<?php echo $Search['limit'] ?>" name="Search[limit]" /></td>
	        		<td><input type="submit" class="btn btn-primary" value="Search" /></td>
				</tr>
			</tbody>
		</table>
	</form>


	<div class="btn-group" style="margin-bottom: 5px">
		<div class="btn-group">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				Chọn chức năng				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu" role="menu">
				<li><a href="#" id="VNP_DeactiveNode"><span class="glyphicon glyphicon-remove"></span>&nbsp&nbspĐình chỉ</a></li>
				<li><a href="#" id="VNP_ActiveNode"><span class="glyphicon glyphicon-ok"></span>&nbsp&nbspKích hoạt</a></li>
				<li><a href="#" id="VNP_DeleteNode"><span class="glyphicon glyphicon-trash"></span>&nbsp&nbspXóa</a></li>
			</ul>
		</div>
	</div>
    <table class="table table-bordered table-striped table-hover">
        <colgroup>
        	<col class="col-xs" id="col-field-toggle">        	<col class="col-xs" id="col-field-code">
                	<col class="col-xs" id="col-field-title">
                	<col class="col-xs" id="col-field-image">
                	<col class="col-xs" id="col-field-product_category">
        <col class="col-xs" id="col-functions">
        </colgroup>
        <thead>
	        <tr>
	        	<td>
	        		<div class="checkbox checkbox-success">
                        <input type="checkbox" id="VNP_ToggleAll" id="VNP_ToggleAll" value="1" />
                        <label for="VNP_ToggleAll"></label>
                    </div>
	        	</td>
	        	<td><a href="<?php echo Router::Generate('ControllerParams', array('controller' => 'product', 'action' => 'Main', 'params' => 'sortby/product_id/order/' . $Search['re_order'])) ?>"><strong>ID</strong></a></td>
	        		        		        				    	<td><a href="<?php echo Router::Generate('ControllerParams', array('controller' => 'product', 'action' => 'Main', 'params' => 'sortby/code/order/' . $Search['re_order'])) ?>"><strong>Mã sản phẩm </strong></a></td>
		    		        		        		        				    	<td><a href="<?php echo Router::Generate('ControllerParams', array('controller' => 'product', 'action' => 'Main', 'params' => 'sortby/title/order/' . $Search['re_order'])) ?>"><strong>Tên sản phẩm</strong></a></td>
		    		        		        		        				    	<td><strong>Ảnh đại diện</strong></td>
	    			        		        		        				    	<td><a href="<?php echo Router::Generate('ControllerParams', array('controller' => 'product', 'action' => 'Main', 'params' => 'sortby/product_category/order/' . $Search['re_order'])) ?>"><strong>Danh mục</strong></a></td>
		    		        		        	<td><strong>Functions</strong></td>
	        </tr>
	    </thead>
	    <tbody>
	    	<?php if(!empty($Nodes)) { ?>
	    	<?php foreach($Nodes as $NodeID => $Node) { ?>
	    	<tr id="node-<?php echo $Node['product_id'] ?>" class="<?php if($Node['status']) { ?>Node_Active<?php }else{ ?>Node_Inactive<?php } ?>">
	    		<td>
	    			<div class="checkbox checkbox-success">
                        <input type="checkbox" name="VNP_ToggleItem[]" class="VNP_ToggleItem" id="ToggleItem-<?php echo $Node['product_id'] ?>" data-title="<?php echo $Node['product_id'] ?>" value="<?php echo $Node['product_id'] ?>" />
                        <label for="ToggleItem-<?php echo $Node['product_id'] ?>"></label>
                    </div>
	    		</td>
	    		<td><?php echo $Node['product_id'] ?></td>
	    				    				    					    	<td><?php echo $Node['code'] ?></td>
			    		    				    				    					    	<td><?php echo $Node['title'] ?></td>
			    		    				    				    							    
					    <td><img src="<?php echo THUMB_BASE ?>50_50<?php echo $Node['image'] ?>"></td>
					    
				    	    				    				    				    					    			
		    			<?php $FieldIdt = $Node['product_category']; ?>
                        <?php if($FieldIdt == '') $FieldIdt = 0 ?>
		    			
		    						    			
			    			<?php $Adapter = $Adapters['vnp_title_product_category_id_key'][$FieldIdt] ?>
				    		<td><?php echo $Adapter['title'] ?></td>
				    		
				    	                    	    			    		<td>
                	<a href="<?php echo Router::EditNode('product', $NodeID) ?>"><span class="glyphicon glyphicon-edit"></span>&nbsp<?php echo Lang::get_string('edit') ?></a>&nbsp;&nbsp;
                    <a href="<?php echo Router::RemoveNode('product', $NodeID) ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp<?php echo Lang::get_string('delete') ?></a>
              	</td>
	    	</tr>
	    	<?php } ?>
	    	<?php } ?>
	    </tbody>
    </table>
</div>