<div class="footerCol">
    <h5 class="footerColtitle"><?php echo $data['block_title'] ?></h5>
    <ul>
        <?php foreach($menu as $item) { ?>
            <li>
                <a href="<?php echo $item['url'] ?>" title="<?php echo $item['title'] ?>"><?php echo $item['title'] ?></a>
            </li>
        <?php } ?>
    </ul>
</div>