<div class="table-responsive">
	<form action="" method="post">
	    <table class="table table-bordered table-striped table-hover">
	        <colgroup>
		        <col class="col-xs-2">
		        <col class="col-xs-8">
	        </colgroup>
	        <thead>
	            <tr>
	                <th><strong>Group</strong></th>
	                <th><strong>Functions</strong></th>
	           	</tr>
	       	</thead>
	       	<tbody>
	       		<tr>
	       			<td>Backend</td>
	       			<td>
	       				<input name="Build[Backend][ListNode]" type="submit" value="Build List" class="btn btn-primary" />
						<input name="Build[Backend][InsertNode]" type="submit" value="Build Add Form" class="btn btn-primary" />
	       			</td>
	       		</tr>
	       		<tr>
	       			<td>Frontend</td>
	       			<td>
	       				<input name="Build[Frontend][List]" type="submit" value="Build List page" class="btn btn-primary" />
						<input name="Build[Frontend][Detail]" type="submit" value="Build Detail page" class="btn btn-primary" />
	       			</td>
	       		</tr>
	       	</tbody>
	  	</table>
	</form>
</div>