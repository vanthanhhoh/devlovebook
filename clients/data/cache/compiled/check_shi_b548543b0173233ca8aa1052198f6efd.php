<script src="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/js/jquery.validate.min.js"></script>
<div class="login-require">
    <div class="container">
        <h1 class="title-login">Địa chỉ nhận hàng</h1>
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="form-area">
                        <div class="row">
                            <form id="ship" method="post" action="">
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="require">Họ tên</label>
                                        <input type="text" name="ship[name]" class="form-control" required="" value="<?php echo $ship_address['name'] ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="require">Số điện thoại</label>
                                        <input type="text" name="ship[phone]" class="form-control" required="" value="<?php echo $ship_address['phone'] ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Lưu ý vận chuyển:</label>
                                        <textarea class="form-control" name="ship[note]" value="<?php echo $ship_address['note'] ?>"></textarea>
                                    </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="require">Tỉnh thành phố</label>
                                    <select name="ship[province]" class="form-control" onchange="getDistrict($(this))" required="">
                                        <option value="">Chọn Tỉnh thành phố</option>
                                        <?php foreach($province as $provinces) { ?>
                                            <option value="<?php echo $provinces['province_id'] ?>" <?php if($provinces['province_id']==$ship_address['province']) { ?>selected<?php } ?>><?php echo $provinces['title'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="require">Huyện quận</label>
                                    <select name="ship[district]" class="form-control" id="district" required="">
                                        <option value="">Chọn Huyện quận</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Địa chỉ:</label>
                                    <textarea class="form-control" name="ship[address]" id="textarea_address" required=""><?php echo $ship_address['address'] ?></textarea>
                                </div>
                                <input type="hidden" name="shipSubmit" value="1"/>
                                <button id="login" type="submit">Gửi hàng đến địa chỉ này</button>
                            </div>
                            </form>
                        </div>
                        <button id="save_ship_infomation">Nhớ thông tin nhận hàng</button>
                    </div>
                </div>
            </div>
            <div class="col-md-3 nopadding">
                <div class="panel panel-default">
                    <div class="review_cart">
                        <div class="review_cart_title">Đơn hàng gồm có <strong><?php echo count($cart) ?></strong> sản phẩm</div>
                        <table>
                            <tbody>
                            <?php $tamtinh=0 ?>
                            <?php foreach($cart as $item) { ?>
                                <?php 
                                    $pitem = $p[$item['product_id']];
                                    $tamtinh = $tamtinh+$item['total'];
                                    $url = Router::Generate('ProductDetail',array('product'=>$pitem['url'],'pid'=>$pitem['product_id']));
                                 ?>
                                <tr>
                                    <td class="title">
                                        <strong><?php echo $item['qty'] ?>x</strong> <a href="<?php echo $url ?>"><?php echo $pitem['title'] ?></a>
                                    </td>
                                    <td class="total" align="right">
                                        <?php echo Filter::NumberFormat($item['total']) ?> đ
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <?php 
                            $discount = 0;
                            $thanhtien= $tamtinh;
                            if(!empty($Cupon)){
                            if($Cupon['discount_type']==1) {
                            $thanhtien = $tamtinh- $Cupon['discount'];
                            $discount = $Cupon['discount'];
                            }
                            else {
                            $discount = $tamtinh*($Cupon['discount']/100);
                            $discount = ceil($discount);
                            $thanhtien = $tamtinh-$discount;
                            }
                            }
                         ?>
                        <p class="tamtinh">
                            <span class="title">Tạm tính</span>
                            <span class="value"><?php echo Filter::NumberFormat($tamtinh) ?> đ</span>
                        </p>
                        <?php if($discount>0) { ?>
                            <p class="tamtinh">
                                <span class="title">Giảm giá theo mã</span>
                                <span class="value">-<?php echo Filter::NumberFormat($discount) ?> đ</span>
                            </p>
                        <?php } ?>
                        <p class="tamtinh">
                            <span class="title">Phí vận chuyển</span>
                            <span class="value">Chưa có</span>
                        </p>
                        <hr>
                        <p class="tamtinh">
                            <span class="title thanhtien">Thành tiền</span>
                            <span class="value thanhtien">
                                <?php echo Filter::NumberFormat($thanhtien) ?> đ
                            </span>
                        </p>
                        <a href="/gio-hang.html" title="Sửa đơn hàng" class="editCart">Sửa đơn hàng</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function getDistrict(e){
        var province = e.val();
        $.ajax({
            url: '/ServiceSys/getDistrict',
            type: 'post',
            dataType: 'json',
            data:{province:province},
            success: function(res){
                $('#district').html('');
                $.each(res,function(i,v) {
                    $('#district').append('<option value="'+ v.district_id+'">'+ v.title+'</option>');
                })
            }
        })
    }
    $.ajax({
        url: '/ServiceSys/getCurrentDistrict',
        type: 'post',
        dataType: 'html',
        data:{province:<?php echo $ship_address['province'] ?>,district:<?php echo $ship_address['district'] ?>},
        success: function(res){
            $('#district').html(res);
        }
    });
    $('#ship').validate({
        errorElement: "span"
    });
    $('#save_ship_infomation').click(function(){
        if($('#ship').valid()==true) {
            var name = $('#ship input[name="ship[name]"]').val();
            var phone = $('#ship input[name="ship[phone]"]').val();
            var address = $('#textarea_address').val();
            var province = $('#ship select[name="ship[province]"]').val();
            var district = $('#ship select[name="ship[district]"]').val();
            VNP.Ajax({
                url: 'ServiceSys/ship_address',
                type: 'post',
                data:{name:name,phone:phone,address:address,province:province,district:district},
                success: function (res) {
                    $.notify({
                        title: '<strong>Thông báo</strong>',
                        message: res.message
                    }, {
                        type: 'success'
                    });
                    VNP.Loader.hide();

                }
            }, 'json');
        }
    });
</script>