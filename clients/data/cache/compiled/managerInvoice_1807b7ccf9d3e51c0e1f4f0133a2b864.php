<div class="account">
    <div class="container">
        <div class="accountInner">
            <div class="row">
                <div class="col-md-3 no-padding-right">
                    <?php echo $sidebar ?>
                </div>
                <div class="col-md-9">
                    <div class="accountContent">
                        <h1>Danh sách đơn hàng của tôi</h1>
                        <table class="table table-hover table-condensed table-responsive">
                            <colgroup>
                                <col class="col-xs-2"/>
                                <col class="col-xs-2"/>
                                <col class="col-xs-4"/>
                                <col class="col-xs-2"/>
                                <col class="col-xs-2"/>
                            </colgroup>
                            <thead>
                                <tr>
                                   <th>Mã đơn hàng</th>
                                   <th>Ngày đặt hàng</th>
                                   <th>Sản phẩm</th>
                                   <th>Tổng tiền</th>
                                   <th>Trạng thái đơn hàng</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($invoice as $item) { ?>
                                    <tr>
                                        <td>
                                            <a href="/Account/detailInvoice/<?php echo $item['invoice_id'] ?>">
                                                <?php echo $item['code'] ?>
                                            </a>
                                        </td>
                                        <td><?php echo Filter::UnixTimeToDate($item['add_time'],true) ?></td>
                                        <td>
                                            <?php 
                                                $listName = array();
                                                $listP = explode(',',$item['product_id']);
                                                foreach($listP as $itemp){
                                                    $listName[] = $p[$itemp]['title'];
                                                }
                                                $listName = implode(',',$listName);
                                             ?>
                                            <?php echo Filter::SubString($listName,100) ?>
                                        </td>
                                        <td><?php echo Filter::NumberFormat($item['total']) ?> đ</td>
                                        <?php 
                                            $status = array(
                                                1=>'Đang chờ xác nhận',
                                                2=>'Đang chờ thanh toán',
                                                3=>'Đã thanh toán online',
                                                4=>'Đã xác nhận',
                                                5=>'Đang vận chuyển',
                                                6=>'Thành công'
                                            );
                                         ?>
                                        <td>
                                            <?php echo $status[$item['invoice_status']] ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>