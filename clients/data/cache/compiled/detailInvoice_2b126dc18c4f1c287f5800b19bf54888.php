<div class="account">
    <div class="container">
        <div class="accountInner">
            <div class="row">
                <div class="col-md-3 no-padding-right">
                    <?php echo $sidebar ?>
                </div>
                <div class="col-md-9">
                    <div class="accountContent">
                        <h1>Đơn hàng #<?php echo $invoice['code'] ?></h1>
                        <p class="date">Ngày đặt hàng:  <?php echo Filter::UnixTimeToFullDate($invoice['add_time'],true) ?></p>
                        <div class="address-1 accountBlock">
                            <h3>Địa chỉ người nhận</h3>
                            <p class="name_ship"><?php echo $invoice['name'] ?></p>
                            <p class="name_phone">Số điện thoại người nhận: <?php echo $invoice['phone'] ?></p>
                            <p class="addres_ship"><?php echo $invoice['address'] ?>, <?php echo $district[$invoice['district']]['title'] . ', '.$province[$invoice['province']]['title'] ?></p>
                        </div>
                        <div class="method">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="shipMethod accountBlock">
                                        <h3>Phương thức nhận hàng</h3>
                                        <p><?php echo $shipMethod['title'] ?></p>
                                        <p><?php echo $shipMethod['description'] ?></p>
                                        <p><?php echo Filter::NumberFormat($shipMethod['fee']) ?> đ</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="paymentMethod accountBlock">
                                        <h3>Phương thức thanh toán</h3>
                                        <p><?php echo $paymentMethod['title'] ?></p>
                                        <p><?php echo $paymentMethod['description'] ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="listOrder">
                            <table class="table table-bordered">
                                <colgroup>
                                    <col class="col-xs-4"/>
                                    <col class="col-xs-2"/>
                                    <col class="col-xs-2"/>
                                    <col class="col-xs-2"/>
                                    <col class="col-xs-2"/>
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th>Tên sản phẩm</th>
                                        <th>Giá gốc</th>
                                        <th>Giá khuyến mãi</th>
                                        <th>Số lượng</th>
                                        <th>Tổng cộng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $giagoc = 0 ?>
                                    <?php $dagiam=0 ?>
                                    <?php foreach($order as $item) { ?>
                                        <?php 
                                            $product = $p[$item['product_id']];
                                            $url = Router::Generate('ProductDetail',array('product'=>$product['url'],'pid'=>$product['product_id']));
                                            $giagoc = $giagoc+($item['price_old']*$item['qty']);
                                            $dagiam+=$item['total'];
                                         ?>
                                        <tr>
                                            <td>
                                                <a href="<?php echo $url ?>"><?php echo $product['title'] ?></a>
                                                <?php if($item['type']==2) { ?>
                                                    <i>Quà tặng</i>
                                                <?php } ?>
                                            </td>
                                            <td><?php echo Filter::NumberFormat($item['price_old']) ?> đ</td>
                                            <td><?php echo Filter::NumberFormat($item['price']) ?> đ</td>
                                            <td><?php echo $item['qty'] ?></td>
                                            <td><?php echo Filter::NumberFormat($item['total']) ?> đ</td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <?php 
                                            $giamgia = $giagoc-$dagiam;
                                            $giamgiacupon =0;
                                            if($invoice['cupon']!='' && $invoice['cupon_discount']>0 && $invoice['cupon_type']!=''){
                                                if($invoice['cupon_type']==1){
                                                    $giamgiacupon = $invoice['cupon_discount'];
                                                }
                                                else {
                                                    $giamgiacupon = $dagiam*$invoice['cupon_discount']/100; $giamgiacupon = ceil($giamgiacupon);
                                                }
                                            }
                                         ?>
                                        <td colspan="5" align="right" class="sumaryTable">
                                            <p>
                                                <span>Tổng chưa giảm:</span> <strong><?php echo Filter::NumberFormat($giagoc) ?> đ</strong>
                                            </p>
                                            <p>
                                                <span>Giảm giá:</span> <strong>-<?php echo Filter::NumberFormat($giamgia) ?> đ</strong>
                                            </p>
                                            <p>
                                                <span>Tổng đã giảm:</span> <strong><?php echo Filter::NumberFormat($dagiam) ?> đ</strong>
                                            </p>
                                            <p>
                                                <span>Giảm giá theo mã <?php echo $invoice['cupon'] ?>:</span> <strong>-<?php echo Filter::NumberFormat($giamgiacupon) ?> đ</strong>
                                            </p>
                                            <p>
                                                <span>Phí vận chuyển:</span> <strong><?php echo Filter::NumberFormat($shipMethod['fee']) ?> đ</strong>
                                            </p>
                                            <hr/>
                                            <p>
                                               <?php $tongcong = $dagiam-$giamgiacupon+$shipMethod['fee'] ?>
                                               Tổng cộng: <strong><?php echo Filter::NumberFormat($tongcong) ?> đ</strong>
                                            </p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="functionInvoice">
                            <?php if($paymentMethod['online']==1 && $invoice['invoice_status']<3) { ?>
                                <a href="/CheckOut/rePay/<?php echo $invoice['invoice_id'] ?>" class="thanhtoanlai">Thanh toán lại online</a>
                            <?php } ?>
                            <a href="" class="removedonhang">Xóa đơn hàng</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>