<div class="book-category-style-2">
    <div class="container">
        <div class="box-title">
            <div class="row">
                <div class="col-md-3 no-padding-right">
                    <h2 class="box-title-text"><?php echo $data['block_title'] ?></h2>
                </div>
                <div class="col-md-9" style="padding-left: 0">
                    <div class="box-title-line">
                        <ul class="sub-category">
                            <?php foreach($child as $cats) { ?>
                                <?php if($cats!=$category['product_category_id']) { ?>
                                    <li>
                                        <?php $url = Router::Generate('ProductAuthor',array('author'=>$cat[$cats]['url'])) ?>
                                        <a href="<?php echo $url ?>"><?php echo $cat[$cats]['title'] ?></a>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-book-style-3">
            <?php foreach($product as $book) { ?>
                <?php 
                    $read = Router::Generate('ProductReview',array('product'=>$book['url'],'pid'=>$book['product_id']));
                    $url  = Router::Generate('ProductDetail',array('product'=>$book['url'],'pid'=>$book['product_id']));
                 ?>
                <div class="book-item-style-3">
                    <div class="book-item-style-3-inner">
                        <div class="book-img pull-left">
                            <img src="<?php echo $book['image'] ?>" alt="<?php echo $book['title'] ?>" width="100%"/>
                        </div>
                        <div class="book-infomation pull-left">
                            <div class="book-title"><?php echo $book['title'] ?></div>
                            <div class="book-author">TG: <?php echo $author[$book['author']]['title'] ?></div>
                            <div class="book-price">
                                <?php if($book['price_sale']>0) { ?>
                                    <span class="newprice"><?php echo Filter::NumberFormat($book['price_sale']) ?> đ</span>
                                    <span class="oldprice"><?php echo Filter::NumberFormat($book['price']) ?> đ</span>
                                <?php }else{ ?>
                                    <span class="newprice"><?php echo Filter::NumberFormat($book['price']) ?> đ</span>
                                <?php } ?>
                            </div>
                            <div class="book-preview">
                                <div class="star-only" data-number="5" data-score="3"></div>
                                <span class="count-rate">(có 3 đánh giá)</span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="book-active">
                                <a href="<?php echo $read ?>" class="book-active-button read-frist fancybox">Đọc thử</a>
                                <a href="<?php echo $url ?>" class="book-active-button buy-now">Mua ngay</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>