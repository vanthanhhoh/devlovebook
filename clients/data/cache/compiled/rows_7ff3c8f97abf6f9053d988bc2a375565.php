<ul class="<?php echo $data['class'] ?>">
    <?php foreach($menu as $item) { ?>
        <li>
            <a href="<?php echo $item['url'] ?>" title="<?php echo $item['title'] ?>"><?php echo $item['title'] ?></a>
        </li>
    <?php } ?>
</ul>