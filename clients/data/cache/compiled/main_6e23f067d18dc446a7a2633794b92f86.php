<div class="box-highlight">
    <div class="container">
        <div class="box-title">
            <div class="row">
                <div class="col-md-3 no-padding-right">
                    <h2 class="box-title-text">Sách bán chạy trong tuần</h2>
                </div>
                <div class="col-md-9" style="padding-left: 0">
                    <div class="box-title-line"></div>
                </div>
            </div>
        </div>
        <div class="carousel-book">
            <div class="carousel-book-horizontal">
                <?php foreach($product as $item) { ?>
                    <?php 
                        $read = Router::Generate('ProductReview',array('product'=>$item['url'],'pid'=>$item['product_id']));
                        $url  = Router::Generate('ProductDetail',array('product'=>$item['url'],'pid'=>$item['product_id']));
                        $rate = 0;
                        if($item['total_rates']>0) $rate = ceil($item['rate_count']/$item['total_rates']);
                     ?>
                    <div class="book-item">
                        <div class="book-img pull-left">
                            <img src="<?php echo $item['image'] ?>" alt="" width="100%"/>
                        </div>
                        <div class="book-infomation pull-left">
                            <div class="book-title"><?php echo $item['title'] ?></div>
                            <div class="book-author">TG: <?php echo $author[$item['author']]['title'] ?></div>
                            <div class="book-price">
                                <?php if($item['price_sale']>0) { ?>
                                    <span class="newprice"><?php echo Filter::NumberFormat($item['price_sale']) ?> đ</span>
                                    <span class="oldprice"><?php echo Filter::NumberFormat($item['price']) ?> đ</span>
                                <?php }else{ ?>
                                    <span class="newprice"><?php echo Filter::NumberFormat($item['price']) ?> đ</span>
                                <?php } ?>
                            </div>
                            <div class="book-preview">
                                <div class="star-only" data-number="5" data-score="<?php echo $rate ?>"></div>
                                <span class="count-rate">(có <?php echo $item['total_rates'] ?> đánh giá)</span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="book-active">
                                <a href="<?php echo $read ?>" class="book-active-button read-frist fancybox">Đọc thử</a>
                                <a href="<?php echo $url ?>" class="book-active-button buy-now">Mua ngay</a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>