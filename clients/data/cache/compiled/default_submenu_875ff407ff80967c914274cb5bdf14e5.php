<ul>
    <?php foreach($Menus as $Menu) { ?>
        <?php $url = Router::Generate('ProductCategory',array('category'=>$Menu['url'])) ?>
        <li id="Menu-Item-<?php echo $Menu['product_category_id'] ?>" class="Menu-Item-<?php echo $Menu['product_category_id'] ?>">
            <a href="<?php echo $url ?>" title="<?php echo $Menu['title'] ?>"><?php echo $Menu['title'] ?></a>
        </li>
    <?php } ?>
</ul>