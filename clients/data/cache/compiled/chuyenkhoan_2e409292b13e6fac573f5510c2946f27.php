<div class="list_chuyenkhoan">
    <ul>
        <li>
            <div class="pull-left logo-brand">
                <div class="logo_bank">
                    <img src="https://www.nganluong.vn//css/newhome/img/brand/VCB.png" alt="vietcombank" width="100%"/>
                </div>
            </div>
            <div class="pull-left info-ck">
                Ngân hàng VIETCOMBANK<br>
                Người nhận : <strong>Lê Thanh Pôn</strong><br>
                Số tài khoản : 0071.000.974.786<br>
                Chi nhánh : PGD Trần Quốc Toản, Q.3 - TP.HCM<br>
            </div>
        </li>
        <li>
            <div class="pull-left logo-brand">
                <div class="logo_bank">
                    <img src="https://www.nganluong.vn//css/newhome/img/brand/BIDV.png" alt="bidv" width="100%"/>
                </div>
            </div>
            <div class="pull-left info-ck">
                Ngân hàng VIETCOMBANK<br>
                Người nhận : <strong>Lê Thanh Pôn</strong><br>
                Số tài khoản : 0071.000.974.786<br>
                Chi nhánh : PGD Trần Quốc Toản, Q.3 - TP.HCM<br>
            </div>
        </li>
        <li>
            <div class="pull-left logo-brand">
                <div class="logo_bank">
                    <img src="https://www.nganluong.vn//css/newhome/img/brand/ICB.png" alt="ICB" width="100%"/>
                </div>
            </div>
            <div class="pull-left info-ck">
                Ngân hàng VIETCOMBANK<br>
                Người nhận : <strong>Lê Thanh Pôn</strong><br>
                Số tài khoản : 0071.000.974.786<br>
                Chi nhánh : PGD Trần Quốc Toản, Q.3 - TP.HCM<br>
            </div>
        </li>
    </ul>
</div>