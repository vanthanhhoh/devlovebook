<div class="category">
    <div class="container">
        <div class="row">
            <div class="col-md-3 no-padding-right hidden-xs">
                <div class="category-left">
                    <div class="box-panel">
                        <div class="box-panel-title">
                            Danh mục sách
                        </div>
                        <div class="box-panel-body">
                            <ul class="categories-accordion">
                                <?php foreach($tree as $categorys) { ?>
                                    <?php $url=Router::Generate('ProductCategory',array('category'=>$categorys['url'])) ?>
                                    <li class="has-children active">
                                        <a href="<?php echo $url ?>" title="<?php echo $categorys['title'] ?>"><?php echo $categorys['title'] ?></a>
                                        <?php echo $categorys['sub'] ?>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="category-right">
                    <?php if($category['banner']!='') { ?>
                        <?php $banner = explode(',',$category['banner']) ?>
                    <div class="banner-category">
                        <ul id="banner-category-adapter">
                            <?php foreach($banner as $item) { ?>
                            <li>
                                <img src="<?php echo $item ?>" alt="<?php echo $category['title'] ?>" width="100%"/>
                            </li>
                            <?php } ?>
                        </ul>
                        <script>
                            $(document).ready(function(){
                                $('#banner-category-adapter').lightSlider({
                                    auto: true,
                                    adaptiveHeight:true,
                                    item:1,
                                    slideMargin:0,
                                    loop:true,
                                    controls: false,
                                    pager: false
                                });
                            })
                        </script>
                    </div>
                    <?php } ?>
                    <h1 class="category-title"><?php echo $category['title'] ?> (<?php echo $num ?>)</h1>

                    <div class="filter">
                        <form action="" method="post" id="filter">
                            <div class="view-box pull-right">
                                <div class="btn-group pull-left sort-box page-box hidden-xs">
                                    <select name="Filter[show]" class="filter-input" onchange="submitFilter()">
                                        <option value="16" <?php if($Filter['show']==16) { ?>selected="selected"<?php } ?>>16</option>
                                        <option value="24" <?php if($Filter['show']==24) { ?>selected="selected"<?php } ?>>24</option>
                                        <option value="32" <?php if($Filter['show']==32) { ?>selected="selected"<?php } ?>>32</option>
                                    </select>
                                </div>

                                <div class="btn-group btn-group-sm">
                                    <input type="hidden" name="Filter[mode]" value="gird" id="Mode"/>
                                    <button title="Lưới" type="button" class="product-mode-grid btn btn-default <?php if($Filter['mode']=='gird') { ?>active<?php } ?>" onclick="changeMode($(this))" data-value="gird"><i class="fa fa-th"></i></button>
                                    <button title="Danh sách" type="button" class="product-mode-list btn btn-default <?php if($Filter['mode']=='list') { ?>active<?php } ?>" onclick="changeMode($(this))" data-value="list"><i class="fa fa-th-list"></i></button>
                                </div>
                            </div>
                            <div class="sort-box-holder pull-left" onchange="submitFilter()">
                                <select name="Filter[sort]" class="filter-input">
                                    <option value="price|ASC" <?php if($Filter['sort']['0']=='price' && $Filter['sort']['1']=='ASC') { ?> selected="selected" <?php } ?>>Giá tăng dần</option>
                                    <option value="price|DESC" <?php if($Filter['sort']['0']=='price' && $Filter['sort']['1']=='DESC') { ?> selected="selected" <?php } ?>>Giá giảm dần</option>
                                    <option value="title|ASC" <?php if($Filter['sort']['0']=='title' && $Filter['sort']['1']=='ASC') { ?> selected="selected" <?php } ?>>A-Z</option>
                                    <option value="add_time|DESC" <?php if($Filter['sort']['0']=='add_time' && $Filter['sort']['1']=='DESC') { ?> selected="selected" <?php } ?>>Mới phát hành</option>
                                    <option value="price_sale|DESC" <?php if($Filter['sort']['0']=='price_sale' && $Filter['sort']['1']=='DESC') { ?> selected="selected" <?php } ?>>Giảm giá nhiều nhất</option>
                                </select>
                                <input type="hidden" name="Filter[filter]"  value="1"/>
                            </div>
                        </form>
                    </div>

                    <div class="listbook">
                        <div class="listbook-warpper" <?php if($Filter['mode']=='list') { ?>id="no-wapper"<?php } ?>>
                            <?php foreach($product as $item) { ?>
                                <?php 
                                    $au = $author[$item['author']];
                                    $url = Router::Generate('ProductDetail',array('product'=>$item['url'],'pid'=>$item['product_id']))
                                 ?>
                                <div class="book-item-category" <?php if($Filter['mode']=='list') { ?>id="list-hoz"<?php } ?>>
                                    <a href="<?php echo $url ?>" title="<?php echo $item['title'] ?>">
                                        <div class="book-image">
                                            <img src="<?php echo Output::GetThumbLink($item['image'],400,600) ?>" width="100%" alt="<?php echo $item['title'] ?>"/>
                                        </div>
                                        <div class="book-category-info">
                                            <h3><?php echo $item['title'] ?></h3>
                                            <p class="book-author-category">Tác giả: <?php echo $au['title'] ?></p>
                                            <div class="book-price-category">
                                                <div class="pull-left price_rule">
                                                    <?php if($item['price_sale']>0) { ?>
                                                        <span class="newprice"><?php echo Filter::NumberFormat($item['price_sale']) ?> đ</span>
                                                        <span class="oldprice"><?php echo Filter::NumberFormat($item['price']) ?> đ</span>
                                                    <?php }else{ ?>
                                                        <span class="newprice"><?php echo Filter::NumberFormat($item['price']) ?> đ</span>
                                                    <?php } ?>
                                                </div>
                                                <?php if($item['price_sale']>0) { ?>
                                                    <?php 
                                                        $disc = $item['price_sale']/$item['price']; $disc = ceil($disc*100);
                                                        $disc = 100-$disc;
                                                     ?>
                                                    <div class="pull-left price_discount_show">
                                                        <span class="discountceil">GIẢM GIÁ: <?php echo $disc ?>%</span>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <div class="description"><?php echo $item['discription'] ?></div>
                                            <div class="clearfix"></div>
                                            <div class="add_cart">
                                                <i class="fa fa-shopping-cart" aria-hidden="true"></i> Mua ngay
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="pagging">
                        <div id="page-selection"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/js/jquery.bootpag.min.js"></script>
<script>
    function submitFilter(){
        $('#filter').submit();
    }
    function changeMode(e){
        var va = e.data('value');
        $('#Mode').val(va);
        submitFilter();
    }
    <?php if($Pg['total_pages']>1) { ?>
    $('#page-selection').bootpag({
        total: <?php echo $Pg['total_pages'] ?>,
        page: <?php echo $Pg['current_page']+1 ?>,
        maxVisible: 5,
        next: '<i class="fa fa-chevron-right" aria-hidden="true"></i>',
        prev: '<i class="fa fa-chevron-left" aria-hidden="true"></i>'
    }).on('page', function(event, num){
        window.location.href = '<?php echo $currenturl ?>page-'+num+'/?<?php echo $getbackup ?>';
    })
    <?php } ?>
</script>

