<div class="login-require">
    <div class="container">
        <h1 class="title-login">Thông tin thanh toán</h1>

        <div class="row">
            <div class="col-md-9">
                <form id="shipMethod" method="post" action="">
                    <input type="hidden" name="submitShip" value="1"/>
                    <input type="hidden" name="ship_id" id="ship_id" value="1"/>
                </form>
                <form id="payment" method="post" action="">
                <div class="panel panel-default">
                    <div class="form-area">
                        <div class="selectShip">
                            <p>Chọn gói giao hàng:</p>
                            <ul>
                                <?php foreach($shipMethod as $key => $item) { ?>
                                <li>
                                    <input type="radio" class="method icheck ship" value="<?php echo $item['ship_id'] ?>" name="ship" <?php if($item['ship_id']==$ship_choose) { ?>checked<?php } ?>  <?php if($ship_address['province']!=2 && $item['ship_id']==2){ echo "disabled";} ?>/>
                                    <label><?php echo $item['title'] ?></label>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="selectPayment">
                            <p>Chọn phương thức thanh toán:</p>
                            <ul>
                                <?php foreach($payment as $key => $item) { ?>
                                    <li>
                                        <input type="radio" class="<?php if($item['module'] !='') { ?>payment
                                        <?php } ?> icheck" value="<?php echo $item['payment_id'] ?>" name="payment" <?php if($key==0) { ?>checked<?php } ?> data-method="<?php echo $item['module'] ?>"/>
                                        <label><?php echo $item['title'] ?></label>
                                        <div id="content_payment_<?php echo $item['payment_id'] ?>" class="content_payment">

                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <input type="hidden" value="1" name="finsh"/>
                        <button type="submit" class="finsh">Đặt mua</button>
                    </div>
                </div>
                </form>
            </div>
            <div class="col-md-3 nopadding">
                <?php if(isset($_SESSION['ship'])) { ?>
                <div class="panel panel-default">
                    <div class="review_cart">
                        <div class="review_cart_title">Địa chỉ nhận hàng</div>
                        <p class="recive_name"><?php echo $ship_address['name'] ?></p>
                        <p class="recive_address"><?php echo $ship_address['address'] ?> , <?php foreach($district as $ds) { if($ds['district_id']==$ship_address['district']) echo $ds['title']; }  ?>, <?php foreach($province as $prov) { if($prov['province_id']==$ship_address['province']) echo $prov['title']; }  ?></p>
                        <p class="recive_phone"><?php echo $ship_address['phone'] ?></p>
                        <a href="/CheckOut/checkShip" title="Sửa địa chỉ nhận hàng" class="editCart">Sửa địa chỉ</a>
                    </div>
                </div>
                <?php } ?>
                <div class="panel panel-default">
                    <div class="review_cart">
                        <div class="review_cart_title">Đơn hàng gồm có <strong><?php echo count($cart) ?></strong> sản phẩm</div>
                        <table>
                            <tbody>
                            <?php $tamtinh=0 ?>
                            <?php foreach($cart as $item) { ?>
                                <?php 
                                    $pitem = $p[$item['product_id']];
                                    $tamtinh = $tamtinh+$item['total'];
                                    $url = Router::Generate('ProductDetail',array('product'=>$pitem['url'],'pid'=>$pitem['product_id']));
                                 ?>
                                <tr>
                                    <td class="title">
                                        <strong><?php echo $item['qty'] ?>x</strong> <a href="<?php echo $url ?>"><?php echo $pitem['title'] ?></a>
                                    </td>
                                    <td class="total" align="right">
                                        <?php echo Filter::NumberFormat($item['total']) ?> đ
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <?php 
                            $discount = 0;
                            $thanhtien= $tamtinh;
                            if(!empty($Cupon)){
                            if($Cupon['discount_type']==1) {
                                $thanhtien = $tamtinh- $Cupon['discount'];
                                $discount = $Cupon['discount'];
                            }
                            else {
                                $discount = $tamtinh*($Cupon['discount']/100);
                                $discount = ceil($discount);
                                $thanhtien = $tamtinh-$discount;
                            }
                            }
                         ?>
                        <p class="tamtinh">
                            <span class="title">Tạm tính</span>
                            <span class="value"><?php echo Filter::NumberFormat($tamtinh) ?> đ</span>
                        </p>
                        <?php if($discount>0) { ?>
                            <p class="tamtinh">
                                <span class="title">Giảm giá theo mã</span>
                                <span class="value">-<?php echo Filter::NumberFormat($discount) ?> đ</span>
                            </p>
                        <?php } ?>
                        <p class="tamtinh">
                            <span class="title">Phí vận chuyển</span>
                            <span class="value"><?php if($ship>0) { ?> <?php echo Filter::NumberFormat($ship) ?> đ <?php }else{ ?>Chưa có<?php } ?></span>
                        </p>
                        <hr>
                        <p class="tamtinh">
                            <span class="title thanhtien">Thành tiền</span>
                            <span class="value thanhtien">
                                <?php if($ship>0) { ?>
                                    <?php $thanhtien = $thanhtien+$ship ?>
                                    <?php echo Filter::NumberFormat($thanhtien) ?> đ
                                    <?php }else{ ?>
                                    <?php echo Filter::NumberFormat($thanhtien) ?> đ
                                <?php } ?>
                            </span>
                        </p>
                        <a href="/gio-hang.html" title="Sửa đơn hàng" class="editCart">Sửa đơn hàng</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('input.icheck').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '20%' // optional,
        });
        $('input.icheck.payment').on('ifChecked', function(event){
            var method = event.delegateTarget.attributes;
            method =method['data-method']['value'];
            $.ajax({
                url: '/paymentModule/'+method,
                type:'post',
                dataType:'text',
                data:{get_content:1},
                success: function(res){
                    $('#content_payment_'+event.delegateTarget.value).html(res);
                }
            });
            var near = $(this).siblings();
            near.each(function() {
                console.log($(this).parents('li'));
            })
        });
        $('input.icheck.ship').on('ifChecked', function(event){
            var id =event.delegateTarget.value;
            $('#ship_id').val(id);
            $('#shipMethod').submit();
        });
    });
</script>