<form action="" class="" id="" method="post">
    <input type="hidden" name="saveOptions" value="1" />
    <?php foreach($options as $opName => $option) { ?>
        <div class="form-group FieldWrap FieldType_text Field_phone">
            <label class="col-sm-3 control-label" for="ID_Field[<?php echo $opName ?>]"><?php echo $option['title'] ?></label>
            <div class="col-sm-9">
                    <textarea type="text" name="Field[<?php echo $opName ?>]" class="transfer" class="form-control" >
                        <?php echo $option['value'] ?>
                    </textarea>
            </div>
        </div>
    <?php } ?>
    <input type="submit" value="Save" class="btn btn-primary">
</form>