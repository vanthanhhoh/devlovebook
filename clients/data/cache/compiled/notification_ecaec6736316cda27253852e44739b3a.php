
<div class="account">
    <div class="container">
        <div class="accountInner">
            <div class="row">
                <div class="col-md-3 no-padding-right">
                    <?php echo $sidebar ?>
                </div>
                <div class="col-md-9">
                    <div class="accountContent">
                        <h1>Thông báo của tôi</h1>
                        <table class="table table-hover table-condensed table-responsive">
                            <colgroup>
                                <col class="col-xs-2"/>
                                <col class="col-xs-7"/>
                                <col class="col-xs-3"/>
                            </colgroup>
                            <thead>
                            <tr>
                                <th>Thời gian</th>
                                <th>Nội dung</th>
                                <th class="text-center">Đánh dấu</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($message as $item) { ?>
                                <tr>
                                    <td>
                                        <?php echo Filter::UnixTimeToDate($item['add_time'],true) ?>
                                        <?php if($item['status']==0) { ?>
                                            <p class="new_notification">thông báo mới</p>
                                        <?php } ?>
                                    </td>
                                    <td><?php echo $item['content'] ?></td>
                                    <td class="text-center">
                                        <?php if($item['status']==0) { ?>
                                            <button class="readedNotification" onclick="readedNotification($(this))" data-id="<?php echo $item['message_id'] ?>">Đánh dấu đã đọc</button>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

