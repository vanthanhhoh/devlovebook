<div class="account">
    <div class="container">
        <div class="accountInner">
            <div class="row">
                <div class="col-md-3 no-padding-right">
                    <?php echo $sidebar ?>
                </div>
                <div class="col-md-9">
                    <div class="accountContent">
                        <script src="<?php echo APPLICATION_DATA_DIR ?>theme/<?php echo CURRENT_THEME ?>/js/jquery.validate.min.js"></script>
                        <h1 class="title_page">Thông tin tài khoản</h1>
                        <div class="profileArea">
                            <form action="" method="post" id="changeProfile" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label>Ảnh đại diện</label>
                                    <div class="row">
                                        <div class="col-md-3 col-xs-5">
                                            <div class="imgAvatar">
                                                <img src="<?php echo $User['avatar'] ?>" id="avarta_cur"/>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-xs-7">
                                            <div class="hidden">
                                                <input type="file" name="avatar" id="FileUpload"/>
                                            </div>
                                            <button type="button" class="btn btn-success buttonCart" onclick="ClickUpload()">Tải hình ảnh khác</button>
                                            <div id="viphamupload">

                                            </div>
                                            <div class="quydinhupload">
                                                Lưu ý : Dung lượng file tải lên chỉ được cho phép tối đa là 2Mb
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="require">Họ tên</label>
                                    <input type="text" class="form-control width-200" name="Profile[fullname]" value="<?php echo $User['fullname'] ?>" required=""/>
                                </div>

                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control width-200" value="<?php echo $User['email'] ?>" disabled/>
                                </div>
                                <div class="form-group">
                                    <label class="require">Số điện thoại</label>
                                    <input type="text" class="form-control width-200" name="Profile[phone]" value="<?php echo $User['phone'] ?>" required=""/>
                                </div>
                                <div class="form-group">
                                    <label class="require">Tỉnh, TP</label>
                                    <select class="form-control width-200" required="" name="Profile[province]" onchange="getDistrict($(this))">
                                        <?php foreach($province as $provinces) { ?>
                                            <option value="<?php echo $provinces['province_id'] ?>" <?php if($provinces['province_id']==$User['province']) { ?>selected="selected"<?php } ?>><?php echo $provinces['title'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="require">Huyện, Quận</label>
                                    <select class="form-control width-200" required="" name="Profile[district]" onchange="getWard($(this))" id="district">
                                        <option value="">Chọn Huyện, Quận</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="require">Địa chỉ</label>
                                    <input type="text" class="form-control width-200" name="Profile[address]" value="<?php echo $User['address'] ?>" required=""/>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="changeProfile" value="<?php echo $changeProfile ?>"/>
                                    <input type="hidden" name="submitProfile" value="1"/>
                                    <button type="submit" class="btn btn-success buttonCart">Lưu thông tin</button>
                                </div>
                            </form>
                        </div>

                        <script>
                            function ClickUpload() {
                                $("#FileUpload").trigger('click');
                            }
                            $(document).ready(function(){
                                $("#FileUpload").change(function() {
                                    var file = document.getElementById("FileUpload").files[0];
                                    console.log($.inArray(file.type,["image/jpeg","image/png","image/bmp"]));
                                    if(file.size<2100000 && $.inArray(file.type,["image/jpeg","image/png","image/bmp"])>=0){
                                        $('#viphamupload').html('');
                                        var oFReader = new FileReader();
                                        oFReader.readAsDataURL(document.getElementById("FileUpload").files[0]);
                                        oFReader.onload = function (oFREvent) {
                                            document.getElementById("avarta_cur").src = oFREvent.target.result;
                                        };
                                    }
                                    else {
                                        $('#viphamupload').html('Dung lượng vượt quá 2Mb hoặc không đúng định dạng');
                                    }

                                });
                                $('#changeProfile').validate({
                                    errorElement: "span"
                                });
                            });
                            function getDistrict(e){
                                var province = e.val();
                                $.ajax({
                                    url: '/ServiceSys/getDistrict',
                                    type: 'post',
                                    dataType: 'json',
                                    data:{province:province},
                                    success: function(res){
                                        $('#district').html('');
                                        $.each(res,function(i,v) {
                                            $('#district').append('<option value="'+ v.district_id+'">'+ v.title+'</option>');
                                        })
                                    }
                                })
                            }
                            $.ajax({
                                url: '/ServiceSys/getCurrentDistrict',
                                type: 'post',
                                dataType: 'html',
                                success: function(res){
                                    $('#district').html(res);
                                }
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>