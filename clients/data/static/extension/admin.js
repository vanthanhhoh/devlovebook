$(document).on('click', '.install-status .install', function(e) {
    e.preventDefault();
    if(confirm('Bạn có chắc chắn muốn cài đặt ứng dụng này?')) {
        var ext_name = $(this).data('ext');
        var action = {
            type: 'install',
            extension: ext_name
        };
        ext_function(action, function(res) {
            if(res.status == 'NOT') {
                alert('Không thể cài đặt ứng dụng này, vui lòng tải lại trang rồi thử lại sau!');
            }
            else {
                alert('Cài đặt ứng dụng thành công!');
            }
            window.location = window.location;
        });
    }
});

$(document).on('click', '.install-status .uninstall', function(e) {
    e.preventDefault();
    if(confirm('Bạn có chắc chắn muốn gỡ bỏ ứng dụng này?')) {
        var ext_name = $(this).data('ext');
        var action = {
            type: 'uninstall',
            extension: ext_name
        };
        ext_function(action, function(res) {
            //console.log(res);
            if(res.status == 'NOT') {
                alert('Không thể gỡ bỏ ứng dụng này, vui lòng tải lại trang rồi thử lại sau!');
            }
            else {
                alert('Gỡ bỏ ứng dụng thành công!');
            }
            window.location = window.location;
        });
    }
});

$(document).on('click', '.active-status .deactive', function(e) {
    e.preventDefault();
    if(confirm('Bạn có chắc chắn muốn tạm dừng ứng dụng này?')) {
        var ext_name = $(this).data('ext');
        var action = {
            type: 'deactive',
            extension: ext_name
        };
        ext_function(action, function(res) {
            if(res.status == 'NOT') {
                alert('Không thể tạm dừng ứng dụng này, vui lòng tải lại trang rồi thử lại sau!');
            }
            else {
                alert('Tạm dừng ứng dụng thành công!');
            }
            window.location = window.location;
        });
    }
});

$(document).on('click', '.active-status .active', function(e) {
    e.preventDefault();
    if(confirm('Bạn có chắc chắn muốn kích hoạt ứng dụng này?')) {
        var ext_name = $(this).data('ext');
        var action = {
            type: 'active',
            extension: ext_name
        };
        ext_function(action, function(res) {
            if(res.status == 'NOT') {
                alert('Không thể kích hoạt ứng dụng này, vui lòng tải lại trang rồi thử lại sau!');
            }
            else {
                alert('Kích hoạt ứng dụng thành công!');
            }
            window.location = window.location;
        });
    }
});

function ext_function(obj, callback) {
    var token = $('#dashboard_key').val();
    obj.token = token;
    VNP.Ajax({
        type: 'POST',
        url: 'extension/action',
        data: obj,
        success:function(res) {
            if (typeof(callback) === 'function') {
                callback(res);
            }
        },
        error: function(res) {
            console.log(res);
        },
        complete: function() {
            VNP.Loader.hide();
        }
    })
}