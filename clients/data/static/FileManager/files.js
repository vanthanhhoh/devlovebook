// JavaScript Document

function File_Manager(ManagerID, ActionsID, ListID, ItemSelector, ActionButtons)
{
	self = this;
	this.ManagerID		= ManagerID;
	this.ActionsID		= ActionsID;
	this.ListID			= ListID;
	this.ItemSelector	= ItemSelector;
	this.TotalSelected	= 0;
	this.DisableMultiSelect = false;
	this.Buttons		= ActionButtons;
	this.FileID			= 0;
	this.SelectedFiles	= [];
	this.TargetField	= '';
	this.MultiImagesField = '';
	this.Updating = [];
	self.MediaHasContextMenu = new Object();

	$(this.ItemSelector).each(function(index, element) {
		$(this).dblclick(function(e) {
			e.preventDefault();
			self.SetActive($(this), false);
			self.SendFile();
        });
        $(this).click(function(e) {
            e.preventDefault();
			var MultiSelect = false;
			if(e.ctrlKey && !self.DisableMultiSelect) MultiSelect = true;
			if(self.DisableMultiSelect) MultiSelect = false;
			self.SetActive($(this), MultiSelect);
			self.CheckButtonsStatus();
        });
    });
	this.SetActive = function(MediaItem, IsMultiSelect) {
		if(typeof IsMultiSelect === 'undefined') IsMultiSelect = false;
		if(MediaItem.attr('data-selected') == 1) {
			MediaItem.removeClass('selected');
			MediaItem.attr('data-selected', 0);
			//self.SelectedFiles.splice(self.SelectedFiles.indexOf(MediaItem.attr('data-file-id')), 1);
			self.TotalSelected--;
		}
		else {
			if(!IsMultiSelect) {
				self.DeactiveAll();
				self.FileID = MediaItem.attr('data-file-id');
			}
			/*self.SelectedFiles[MediaItem.attr('data-file-id')] = {
				location: MediaItem.attr('data-file-location'),
				title: MediaItem.attr('data-file-title'),
				alt: MediaItem.attr('data-file-alt')
			}*/
			MediaItem.addClass('selected');
			MediaItem.attr('data-selected', 1);
			self.TotalSelected++;
		}
	}
	this.DeactiveAll = function() {
		$(self.ItemSelector).removeClass('selected');
		$(self.ItemSelector).attr('data-selected', 0);
		self.TotalSelected = 0;
		self.SelectedFiles = new Array();
	}

	this.CheckButtonsStatus = function() {
		if(self.TotalSelected == 0) {
			self.DisableButton(self.Buttons.AddToEditor);
			self.DisableButton(self.Buttons.EditFile);
			self.DisableButton(self.Buttons.DeleteFile);
			self.DisableButton(self.Buttons.OpenImageEditor);
		}
		if(self.TotalSelected == 1) {
			self.EnableButton(self.Buttons.AddToEditor);
			self.EnableButton(self.Buttons.EditFile);
			self.EnableButton(self.Buttons.DeleteFile);
			self.EnableButton(self.Buttons.OpenImageEditor);
		}
		else if(self.TotalSelected > 1) {
			self.EnableButton(self.Buttons.AddToEditor);
			self.DisableButton(self.Buttons.EditFile);
			self.DisableButton(self.Buttons.DeleteFile);
			self.DisableButton(self.Buttons.OpenImageEditor);
		}
	}

	this.DisableButton = function(Btn) {
		$(Btn).addClass('disabled');
		$(Btn + ' a').attr('href', '');
	}

	this.EnableButton = function(Btn) {
		var ActionUrl = '';
		if(Btn == self.Buttons.EditFile) ActionUrl = VNP.BaseUrl + 'FileManager/manage/edit/' + self.FileID;
		else if(Btn == self.Buttons.DeleteFile) ActionUrl = VNP.BaseUrl + 'FileManager/manage/remove/' + self.FileID;
		else if(Btn == self.Buttons.OpenImageEditor) ActionUrl = VNP.BaseUrl + 'FileManager/manage/ImageEditor/' + self.FileID;
		$(Btn + ' a').attr('href', ActionUrl);
		$(Btn).removeClass('disabled');
	}

	this.SendFile = function() {
		var ImagesList = new Array();
		var File = {location: '', 'title': '', 'alt': ''};
		var ImageElement = ImageStyle = '';
		var ImagesLocations = new Array();

		var SelectedItemsSelector = self.ItemSelector + '[data-selected="1"]';
        console.log(SelectedItemsSelector);
		$(SelectedItemsSelector).each(function(index, element) {
			File = $(this);
			//File.location = VNP.Protocol + '//' + VNP.Domain + '/Thumbnail/686_686/' + $(this).attr('data-file-location');
			File.location = $(this).attr('data-file-location');
			File.title = $(this).attr('data-file-title');
			File.alt = $(this).attr('data-file-alt');
			if(FileManager.TargetField != '') {
				File.thumb = ThumBase + '80_80' + $(this).attr('data-file-location');
				File.location = $(this).attr('data-file-location');
				ImagesList = [File];
			}
			else if(FileManager.MultiImagesField != '') {
				File.thumb = '/Thumbnail/60_60' + $(this).attr('data-file-location');
				File.location = $(this).attr('data-file-location');
				ImageElement = '<div class="MultiImages_Item"><input type="hidden" name="Field[' + FileManager.MultiImagesField + '][]" value="' + File.location + '" /><img src="' + File.thumb + '" title="' + File.title + '" alt="' + File.alt + '" /><a href="#" onclick="this.parentNode.parentNode.removeChild(this.parentNode); return false;">Delete</a></div>';
				ImagesList.push(ImageElement);
				ImagesLocations.push(File.location);
			}
			else {
				ImageStyle = 'display: block; margin-left: auto; margin-right: auto;';
				ImageElement = '<img style="' + ImageStyle + '" src="' + File.location + '" title="' + File.title + '" alt="' + File.alt + '" />';
				ImagesList.push(ImageElement);
			}

        });
		if(FileManager.TargetField != '') {
			parent.document.getElementById(FileManager.TargetField).value = ImagesList[0].location;
			parent.document.getElementById('Thumb_' + FileManager.TargetField).setAttribute('src',ImagesList[0].thumb);
			top.VNP.SugarBox.Close();
		}
		else if(FileManager.MultiImagesField != '') {
			parent.document.getElementById('MultiImages_' + FileManager.MultiImagesField).innerHTML += ImagesList.join('');
			top.VNP.SugarBox.Close();
		}
		else {
			console.log('editor');
			window.parent.tinymce.activeEditor.execCommand('mceInsertContent', false, ImagesList.join('<br />'));
			top.tinymce.activeEditor.windowManager.close();
		}
	}

	this.DeactiveFile = function(FileIDs) {
		if(FileIDs.length == 0) alert('You must select at least one media');
		else {
			if(confirm('Do you want to do this?')) {
				VNP.Ajax({
					url: 'FileManager/Action',
					type: 'POST',
					data: {action: 'deactive',ids: FileIDs},
					success: function(res) {
						if(res.status == 'ok') {
							$.each(res.items, function(i,v) {
								$('#media-' + v).removeClass('Node_Active');
								$('#media-' + v).addClass('Node_Inactive');
							});
							VNP.Loader.TextNotify('Success deactive medias!');
						}
					}
				}, 'json');
			}
		}
	}
	this.ActiveFile = function(FileIDs) {
		if(FileIDs.length == 0) alert('You must select at least one media');
		else {
			if(confirm('Do you want to do this?')) {
				VNP.Ajax({
					url: 'FileManager/Action',
					type: 'POST',
					data: {action: 'active',ids: FileIDs},
					success: function(res) {
						if(res.status == 'ok') {
							$.each(res.items, function(i,v) {
								$('#media-' + v).addClass('Node_Active');
								$('#media-' + v).removeClass('Node_Inactive');
							});
							VNP.Loader.TextNotify('Success deactive medias!');
						}
					}
				}, 'json');
			}
		}
	}
	this.DeleteFile = function(FileIDs) {
		if(FileIDs.length == 0) alert('You must select at least one media');
		else {
			if(confirm('Do you want to do this?')) {
				var rt = false;
				VNP.Ajax({
					url: 'FileManager/Action',
					type: 'POST',
					data: {action: 'delete',ids: FileIDs},
					async: false,
					success: function(res) {
						if(res.status == 'ok') {
							$.each(res.items, function(i,v) {
								$('#media-' + v).remove();
							});
							VNP.Loader.TextNotify('Success delete medias!');
							rt = true;
						}
					}
				}, 'json');
				return rt;
			}
		}
	}

	this.QuickUpdate = function() {
		jQuery(document).ready(function($) {
			$(document).on('focus', '.QuickUpdateFile', function(event) {
				var currentLength = $(this).val().length;
				event.preventDefault();
				$(this).keypress(function(event) {
					//if(this.selectionStart == currentLength) {
						var FieldObj = $(this);
						self.Updating.push(FieldObj.attr('data-media'));
					//}
				});
			});
			$(document).on('keypress', '.QuickUpdateFile', function(event) {
				if(!event) event = window.event;
				var keyCode = event.keyCode || event.which;
				if(keyCode == '13') self.SaveFileUpdate();
			});
		});
	}

	this.SaveFileUpdate = function() {
		var UpdateDatas = new Object();
		var ChangedFileIDs = self.Updating.getUnique();
		$.each(ChangedFileIDs, function(index, FID) {
			var MediaObj = $('#media-' + FID);
			var media_title = $('input.media_title', MediaObj).val();
			var media_alt = $('input.media_alt', MediaObj).val();
			UpdateDatas[FID] = {'media_title':media_title,'media_alt':media_alt};
		});

		if(ChangedFileIDs.length > 0) {
			VNP.Ajax({
				url: 'FileManager/QuickUpdate',
				type: 'POST',
				data: {file_datas: JSON.stringify(UpdateDatas)},
				success: function(res) {
					$('.QuickUpdateFile').blur();
					VNP.Loader.TextNotify(res.content, res.status);
				},
				error: function(res) {
					//console.log(res);
					VNP.Loader.TextNotify('Cannot update medias!', 'error');
				},
				complete: function() {
					var UpdateDatas = new Object();
					self.Updating = [];
				}
			}, 'json');
		}
	}

	$(self.Buttons.AddToEditor + ' a').click(function(e) {
        e.preventDefault();
		self.SendFile();
    });
    return self;
}

var ActionButtons = {	AddToEditor: 	'#AddToEditor',
						EditFile:		'#EditFile',
						DeleteFile:		'#DeleteFile',
						OpenImageEditor:'#OpenImageEditor'}

var FileManager = new File_Manager('#Files_Container', '#File_Actions', '#Main_Files', '.Media_Item', ActionButtons);
if(typeof TargetField !== 'undefined' && TargetField != '') {
	FileManager.DisableMultiSelect = true;
	FileManager.TargetField = TargetField;
}
else if(typeof MultiImagesField !== 'undefined' && MultiImagesField != '') {
	FileManager.MultiImagesField = MultiImagesField;
}
function FileManager_SendFile() {
    FileManager.SendFile();
}
function FileManager_ViewFile() {
	window.open(FileManager.MediaHasContextMenu.attr('data-file-location'),'_blank');
}
function FileManager_FullEdit() {
	window.open(VNP.BaseUrl + 'FileManager/manage/edit/' + FileManager.MediaHasContextMenu.attr('data-file-id'),'_blank');
}
function FileManager_ImageEditor() {
	window.open(VNP.BaseUrl + 'FileManager/manage/image_edit/' + FileManager.MediaHasContextMenu.attr('data-file-id'),'_blank');
}
function FileManager_RemoveMedia() {
	var media = FileManager.MediaHasContextMenu.attr('data-file-id');
	if(FileManager.DeleteFile(media))
		FileManager.MediaHasContextMenu.remove();
}
function FileManager_UpdateMedia() {
	var UpdateDatas = new Object();
	var mid = FileManager.MediaHasContextMenu.attr('data-file-id');
	UpdateDatas[mid] = {'media_title':$('#VNP_MediaTitle').val(),'media_alt':$('#VNP_MediaAlt').val()};
	VNP.Ajax({
		url: 'FileManager/QuickUpdate',
		type: 'POST',
		data: {file_datas: JSON.stringify(UpdateDatas)},
		success: function(res) {
			VNP.Loader.TextNotify(res.content, res.status);
			FileManager.MediaHasContextMenu.attr('data-file-title', $('#VNP_MediaTitle').val());
			FileManager.MediaHasContextMenu.attr('data-file-alt', $('#VNP_MediaAlt').val());
			window.location = window.location;
		},
		error: function(res) {
			VNP.Loader.TextNotify('Cannot update medias!', 'error');
		},
		complete: function() {
			var UpdateDatas = new Object();
			self_Updating = [];
		}
	}, 'json');
}