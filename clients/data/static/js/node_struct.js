jQuery(document).ready(function($) {
	$('#NodeStructor').sortable({
		handle: '.Move_Option',
		items: '.NodeType_Field',
		containment: 'parent',
		start: function (event, ui) {
	        //SortAttributesField_Name = $(ui.item).attr('data-field-name');
	        //console.log(ui.item);
	    },
		stop: function(event, ui) {ReorderNodeFields()}
	});

	function ReorderNodeFields() {
		var SortedFields = new Array();
		$.each($('#NodeStructor .NodeType_Field'), function(index, field) {
			var FieldName = $(field).attr('data-field-name');
			SortedFields.push(FieldName);
		});
		console.log(SortedFields);
	}
});