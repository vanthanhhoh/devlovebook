$(document).ready(function() {
    $('.cmt-reply').on('click', function(e) {
        e.preventDefault();
        $('.cmt-active-reply-form').remove();
        var commentNode = $(this);
        var parentNode = $(this).parent().parent().parent();
        VNP.Ajax({
            url: 'comments/reply',
            type: 'POST',
            data: {node_type: commentNode.attr('data-nodetype'),node_id: commentNode.attr('data-nodeid'), parent_id:commentNode.attr('data-parent') },
            success: function(res) {
                parentNode.append(res);
            },
            error: function(res) {
                console.log(res);
                alert('Cannot load reply form!');
            },
            complete: function() {
                VNP.Loader.hide();
            }
        }, 'text');
    });
    $('.cmt-like').on('click', function(e) {
        e.preventDefault();
        var commentNode = $(this);
        if(typeof commentNode.attr('disable') == typeof undefined || commentNode.attr('disable') == 'false') {
            var parentNode = $(this).parent().parent().parent();
            VNP.Ajax({
                url: 'comments/like',
                type: 'POST',
                data: {cid:commentNode.attr('data-comment')},
                success: function(res) {
                    if(res.status == 'OK') {
                        $('span', commentNode).html(res.like);
                        commentNode.addClass('disabled');
                        commentNode.attr('disable', 'true');
                    }
                    else alert('An error has occured, please try again!');
                },
                error: function(res) {
                    console.log(res);
                    alert('An error has occured, please try again!');
                },
                complete: function() {
                    VNP.Loader.hide();
                }
            }, 'json');
        }
    });
});