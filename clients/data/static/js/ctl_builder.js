jQuery(document).ready(function($) {
	$('.CTL_ShowFields').click(function(event) {
		var Field = $(this);
		var FieldName = Field.val();
		var FieldLabel = Field.attr('data-field-label');
		if(Field.prop('checked')) {
			var s = '<div class="FieldOrderItem" id="FieldOrder_' + FieldName + '"><label><input style="width:50px" class="form-control" value="0" type="number" name="Settings[fields_order][' + FieldName + ']" />&nbsp&nbsp' + FieldLabel + '</label></div>';
			$('#FieldsOrder').append(s);
		}
		else {
			$('#FieldsOrder #FieldOrder_' + FieldName).remove();
		}
		ReOrderFields();
	});

	function ReOrderFields() {
		$.each($('.FieldOrderItem'), function(index, val) {
			$('input', $(val)).val(index);
		});
	}

	$('.VNP_FormPart').sortable({
		connectWith: ".VNP_FormPart",
		stop: function(event, ui) {ReorderNodeFields()}
	});

	function ReorderNodeFields() {
		var FormParts = new Array();
		$('.SortedFields').remove();
		$.each($('.VNP_FormPart'), function(index, Part) {
			var Cols = $(Part).attr('data-cols');
			var PartFields = new Array();
			$.each($('.FieldItem ', $(Part)), function(i, Field) {
				var FieldName = $(Field).attr('data-field-name');
				$('#Build_AddNodeForm').append('<input class="SortedFields" type="hidden" name="sort_data[' + Cols + '][]" value="' + FieldName + '" />');
			})
			//FormParts.push(PartFields);
		});
		$('.sort_data').val(JSON.stringify(FormParts));
		console.log(JSON.stringify(FormParts));
	}
});