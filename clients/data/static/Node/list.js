function haha() {

}
function VNP_DeactiveNode(checkedInputs, titleData) {
	if(checkedInputs.length == 0) alert('You must select at least one item');
	else {
		if(confirm('Do you want to do this?')) {
			VNP.Ajax({
				url: NodeController + '/Action',
				type: 'POST',
				data: {action: 'deactive',ids: checkedInputs},
				success: function(res) {
					if(res.status == 'ok') {
						$.each(res.items, function(i,v) {
							$('#node-' + v).removeClass('Node_Active');
							$('#node-' + v).addClass('Node_Inactive');
						});
						VNP.Loader.TextNotify('Success deactive users!');
					}
					//VNP.Loader.hide();
				}
			}, 'json');
		}
	}
}
function VNP_ActiveNode(checkedInputs, titleData) {
	if(checkedInputs.length == 0) alert('You must select at least one item');
	else {
		if(confirm('Do you want to do this?')) {
			VNP.Ajax({
				url: NodeController + '/Action',
				type: 'POST',
				data: {action: 'active',ids: checkedInputs},
				success: function(res) {
					if(res.status == 'ok') {
						$.each(res.items, function(i,v) {
							$('#node-' + v).addClass('Node_Active');
							$('#node-' + v).removeClass('Node_Inactive');
						});
						VNP.Loader.TextNotify('Success active users!');
					}
				}
			}, 'json');
		}
	}
}
function VNP_DeleteNode(checkedInputs, titleData) {
    if(checkedInputs.length == 0) alert('You must select at least one item');
    else {
        if(confirm('Do you want to do this?')) {
            VNP.Ajax({
                url: NodeController + '/Action',
                type: 'POST',
                data: {action: 'delete',ids: checkedInputs},
                success: function(res) {
                    if(res.status == 'ok') {
                        $.each(res.items, function(i,v) {
                            $('#node-' + v).remove();
                        })
                    }
                    VNP.Loader.TextNotify('Success delete users!');
                }
            }, 'json');
        }
    }
}
function VNP_sendToTargetField(checkedInputs, titleData) {
    if(checkedInputs.length == 0) alert('You must select at least one item');
    else {
        //if(confirm('Do you want to do this?')) {
            VNP.Ajax({
                url: 'Node/getNodesJson',
                type: 'POST',
                data: {ref_table: ref_table, ref_field: ref_field, nodeIds: checkedInputs},
                success: function(res) {
                    console.log(res);
                    var rt = '';
                    if(res.status == 'OK') {
                        $(res.nodes).each(function(i,e) {
                            rt += '<li class="ListSelect_Item">\
                            <input type="hidden" name="Field[' + targetField + '][]" value="' + e[ref_table + '_id'] + '">' + e[ref_field] + '<a href="#" onclick="this.parentNode.parentNode.removeChild(this.parentNode); return false;">Delete</a>\
                            </li>';
                        });
                        parent.document.getElementById('ListSelect_' + targetField).innerHTML += rt;
                        top.VNP.SugarBox.Close();
                    }
                    VNP.Loader.TextNotify('Loaded!');
                }
            }, 'json');
        //}
    }
}
$(document).ready(function() {
    var checkedInputs = '';
    var titleData = '';
    var _fa = new Array();
    if(typeof isGetNodesFrame !== 'undefined' && isGetNodesFrame == true) {
        $('#VNP_DeactiveNode,#VNP_DeleteNode').remove();
        $('#VNP_ActiveNode').html('Insert these nodes');
        _fa = [{container: '#VNP_ActiveNode', callback: "VNP_sendToTargetField(checkedInputs, titleData)" }];
    }
    else
        _fa = [
            {container: '#VNP_DeactiveNode', callback: "VNP_DeactiveNode(checkedInputs, titleData)" },
            {container: '#VNP_ActiveNode', callback: "VNP_ActiveNode(checkedInputs, titleData)" },
            {container: '#VNP_DeleteNode', callback: "VNP_DeleteNode(checkedInputs, titleData)" }
        ]
	$('#VNP_ToggleAll').InputToggle({
		childInput: '.VNP_ToggleItem',
		dataAttribute: 'data-title',
		storageVar: 'checkedInputs',
		titleData:	'titleData',
		featureAction: _fa,
		callBackFunction: 'haha(checkedInputs)',
		enableCookie: true
	});
});