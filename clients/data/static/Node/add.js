$(document).ready(function() {
	if($('.Field_Artibutes').length > 0) {
		var SortAttributesField_Name = '';
		$('.Field_Artibutes').sortable({
										handle: '.Move_Option',
										items: '.FieldAttributeCtner',
										containment: 'parent',
										start: function (event, ui) {
								            SortAttributesField_Name = $(ui.item).attr('data-field-name');
								            console.log(ui.item);
								        },
										stop: function(event, ui) {ReorderOptions()}
									});
		if($('.Field_Artibutes').length > 0) {
			$('.Add_Option').click(function(event) {
				event.preventDefault();
				var AttributesField_Name = $(this).attr('data-field-name');
				SortAttributesField_Name = AttributesField_Name;
				var FieldObject = $('#' + AttributesField_Name + '_FieldAttribute_Ctner');
				var OptionObj = $('.Option_To_Clone',FieldObject).html();
				$('.Option_To_Clone',OptionObj).addClass('Field_' + AttributesField_Name + '_attribute FieldAttribute').removeClass('Option_To_Clone').css('display', 'block');
				$('<div class="Field_' + AttributesField_Name + '_attribute FieldAttributeCtner" data-field-name="' + AttributesField_Name + '">' + OptionObj + '</div>').appendTo(FieldObject);
				ReorderOptions();
			});
		}

		function ReorderOptions() {
			if(SortAttributesField_Name != '') {
				var OptionNameTemplate = 'Field[' + SortAttributesField_Name + ']';
				$.each($('.Field_' + SortAttributesField_Name + '_attribute'), function(i, OptionElement) {
					$('input.FieldOption_Name', OptionElement).attr('name', OptionNameTemplate+'['+i+'][name]');
					$('input.FieldOption_Value', OptionElement).attr('name', OptionNameTemplate+'['+i+'][value]');
				});
			}
		}
		$(document).on('click', '.Remove_Option', function(e) {
		    e.preventDefault();
		    var SortAttributesField_Name = $(this).attr('data-field-name');
			if(confirm('Are you sure?')) {
				$(this).parent().parent().remove();
				ReOrderNodeFieldOptions();
			}
		});

		if($('.save_attr_tpl').length > 0) {
			$('.save_attr_tpl').click(function(e) {
				e.preventDefault();
				var AttributesTPLField_Name = $(this).attr('data-field-name');
				var AtrributesArray = new Array();
				$.each($('.Field_' + AttributesTPLField_Name + '_attribute '), function(i,e) {
					var _N = $('.FieldOption_Name', e).val();
					if(_N != '' && _N != 'undefined' && _N != null) AtrributesArray.push(_N);
				});
				var tpl_name = prompt("Please enter attributes template name", "");
				if(tpl_name != '' && tpl_name != null) SaveAttributesTemplate(tpl_name, AtrributesArray, 0);
			})
		}

		if($('.load_attr_tpl').length > 0) {
			$('.load_attr_tpl').click(function(e) {
				e.preventDefault();
				var AttributesTPLField_Name = $(this).attr('data-field-name');
				var AtrributesArray = new Array();
				VNP.Ajax({
					type: 'POST',
					url: 'Utility/LoadAttributesTemplates',
					success: function(res) {
						VNP.SugarBox.Open({title:'Choose Attributes Template', content: res, footer:'<button data-field-name="' + AttributesTPLField_Name + '" class="VNP_ChooseAttrsTPL btn btn-primary">Choose</button>&nbsp<button role="Close_SugarBox" class="btn btn-default">Cancel</button>'});
						VNP.Loader.TextNotify('Success load Attributes Templates', 'success');
					}
				}, 'text');
			})
		}

		function SaveAttributesTemplate(tpl_name, tpl_content, overwrite) {
			VNP.Ajax({
				type: 'POST',
				url: 'Utility/SaveAttributesTemplate',
				data: {tpl_name: tpl_name, attr_template: tpl_content, overwrite: overwrite},
				success: function(res) {
					if(res.status == 'existed') {
						if(confirm('Do you want to overwrite this Attributes template: ' + tpl_name))
							SaveAttributesTemplate(tpl_name, tpl_content, 1);
						else VNP.Loader.TextNotify('Saving canceled', 'success');
					}
					else VNP.Loader.TextNotify(res.content, res.status)
				}
			}, 'json');
		}

		jQuery(document).ready(function($) {
			$(document).on('click', '.VNP_ChooseAttrsTPL', function(event) {
				event.preventDefault();
				var AttributesTPLField_Name = $(this).attr('data-field-name');
				var ChoosedTPL = $('input[name="VNP_AttrsTPL"]:checked').val();
				var AttributesTemplates = $.parseJSON(VNP_AttributesTemplates);
				if(ChoosedTPL != '') {
					var AttrsObj = JSON.parse(eval('VNP_AttributesTemplates.' + ChoosedTPL));
					if(AttrsObj != null) {
						var AttrsTPLString = '';
						for(var i = 0; i < AttrsObj.length; i++) {
							if(i == 0) {
								var N_Class1 = 'btn-primary Add_Option';
								var N_Class2 = 'plus';
							}
							else {
								var N_Class1 = 'btn-danger Remove_Option';
								var N_Class2 = 'remove';
							}
							AttrsTPLString += '\
							<div class="Field_thuoc_tinh_attribute FieldAttributeCtner" data-field-name="' + AttributesTPLField_Name + '">\
					            <div class="input-group NodeField_Option">\
					                <span class="input-group-addon Move_Option">\
					                    <span class="glyphicon glyphicon-resize-vertical"></span>\
					                </span>\
					                <span class="input-group-addon">Name</span>\
					                <input type="text" class="form-control FieldOption_Name" name="Field[' + AttributesTPLField_Name + '][' + i + '][name]" value="' + AttrsObj[i] + '" placeholder="Attribute Name">\
					                <span class="input-group-addon">Value</span>\
					                <input type="text" class="form-control FieldOption_Value" name="Field[' + AttributesTPLField_Name + '][' + i + '][value]" placeholder="Attribute Value">\
					                <div class="input-group-btn">\
					                    <button class="btn ' + N_Class1 + '" data-field-name="' + AttributesTPLField_Name + '"><span class="glyphicon glyphicon-' + N_Class2 + '"></span></button>\
					                </div>\
					            </div>\
					        </div>';
						}
						$('.Field_' + AttributesTPLField_Name + '_attribute').remove();
						$('#' + AttributesTPLField_Name + '_FieldAttribute_Ctner').append(AttrsTPLString);
						VNP.SugarBox.Close();
					}
				}
			});
		});

		$(document).on('click', '.Remove_AttrsTemplate', function(event) {
			event.preventDefault();
			var AttrTPL = $(this).attr('data-attrs-tpl');
			if(confirm('Do you want to remove this attributes template?')) {
				VNP.Ajax({
					type: 'POST',
					url: 'Utility/RemoveAttributesTemplate',
					data: {tpl_name: AttrTPL},
					success: function(res) {
						if(res.status == 'success') {
							$('[data-attrs-tpl="' + AttrTPL + '"]').parent().parent().remove();
							VNP.Loader.TextNotify(res.content, res.status)
						}
						else VNP.Loader.TextNotify(res.content, res.status);
					},
					error: function(res) {
						console.log(res);
					}
				}, 'json');
			}
		});
	}
	if(jQuery().tabs) {$("#insert_node_tabs").tabs();};
	if(jQuery().select2) {
		$('select').select2();
	}
});