<ul class="articleList">
    {for $node in $nodes}
    <li>
		<div class="article-tags">{$node.tags}</div>
		<div class="article-main_catid">{$node.main_catid}</div>
		<div class="article-source">{$node.source}</div>
		<div class="article-image">{$node.image}</div>
		<div class="article-author">{$node.author}</div>
		<div class="article-body">{$node.body}</div>
		<div class="article-description">{$node.description}</div>
		<div class="article-url">{$node.url}</div>
		<div class="article-category">{$node.category}</div>
		<div class="article-title">{$node.title}</div>
    </li>
    {/for}
</ul>