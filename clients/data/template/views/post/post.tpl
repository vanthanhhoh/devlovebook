<ul class="postList">
    {for $node in $nodes}
    <li>
		<div class="post-title">{$node.title}</div>
		<div class="post-description">{$node.description}</div>
    </li>
    {/for}
</ul>