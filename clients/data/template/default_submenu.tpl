<ul>
    {for $Menu in $Menus}
        {function}$url = Router::Generate('ProductCategory',array('category'=>$Menu['url'])){/function}
        <li id="Menu-Item-{$Menu.product_category_id}" class="Menu-Item-{$Menu.product_category_id}">
            <a href="{$url}" title="{$Menu.title}">{$Menu.title}</a>
        </li>
    {/for}
</ul>