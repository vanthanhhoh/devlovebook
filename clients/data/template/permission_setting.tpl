<div class="table-responsive">
	<form class="form-horizontal" action="" method="post">
		<input type="hidden" name="SaveNodeSubmit" value="1">
		<input type="hidden" name="NodeID" value="0">
		<table class="table table-bordered table-striped table-hover">
	        <colgroup>
	        	<col class="col-xs-2">
	            <col class="col-xs-2">
	        </colgroup>
	    <thead>
	        <tr>
	        	<td><strong>Node type</strong></td>
	        	<td><strong>Add node</strong></td>
	        	<td><strong>Edit node</strong></td>
	        	<td><strong>Remove node</strong></td>
	        </tr>
	    </thead>
	        <tbody>
	        	{for $NodeType in $NodeTypeData.NodeTypes}
	        	<tr>
	        		<td>{$NodeType.NodeTypeInfo.title}</td>
	        		<td><input type="checkbox" /></td>
	        		<td><input type="checkbox" /></td>
	        		<td><input type="checkbox" /></td>
	        	</tr>
	        	{/for}
			</tbody>
		</table>
	</table>