<div class="VNP_NodeExtraSettings">
	<div class="SettingHeader" onclick="javascript:$('.VNP_NodeExtraSettings .SettingsContent').toggle();">
        <span>Settings</span>
    </div>
    <div class="SettingsContent">
    	<div class="form-group">
			<label class="control-label" for="ID_Field[status]" style="width:40%; float: left">Status</label>
		    <div style="width:60%; float: left">
				<select type="number" name="Field[status]" id="ID_Field[status]" class="form-control" value="{$Data.status}">
					<option value="1"{if($Data.status == 1)} selected{/if}>Active</option>
					<option value="0"{if($Data.status == 0)} selected{/if}>Inactive</option>
				</select>
		 	</div>
		</div>
    	<div class="form-group">
			<label class="control-label" for="ID_Field[priority]" style="width:40%; float: left">Piority</label>
		    <div style="width:60%; float: left">
				<input type="number" name="Field[priority]" id="ID_Field[priority]" class="form-control" value="{$Data.priority}" />
		 	</div>
		</div>
		<div class="form-group">
			<label class="control-label" for="ID_Field[schedule]" style="width:40%; float: left">Schedule</label>
		    <div class="DatePickerCtner">
		    	<div class="input-group date" style="width:60%;float: left;margin-right: 5px" id="VNP_Nodeschedule_time" data-date="{$Data.schedule.date}" data-date-format="dd/mm/yyyy" data-date-viewmode="date">
					<input type="text"  name="Field[schedule][date]" class="form-control input-append datepicker" value="{$Data.schedule.date}" />
					<span class="input-group-addon add-on" style="width: 28px;padding: 5px 10px;"><i class="glyphicon glyphicon-calendar"></i></span>
                    <input type="hidden" name="Field[schedule][hour]" value="0"/>
                    <input type="hidden" name="Field[schedule][minute]" value="0"/>
                </div>
		 	</div>
		</div>
		<div class="form-group">
			<label class="control-label" for="ID_Field[exprired]" style="width:40%; float: left">Expired</label>
		    <div class="DatePickerCtner">
		    	<div class="input-group date" style="width:60%;float: left;margin-right: 5px" id="VNP_Nodeexprired_time" data-date="{$Data.exprired.date}" data-date-format="dd/mm/yyyy" data-date-viewmode="years">
					<input type="text"  name="Field[exprired][date]" class="form-control input-append datepicker" value="{$Data.exprired.date}"/>
					<span class="input-group-addon add-on" style="width: 28px;padding: 5px 10px;"><i class="glyphicon glyphicon-calendar"></i></span>
                    <input type="hidden" name="Field[exprired][hour]" value="0"/>
                    <input type="hidden" name="Field[exprired][minute]" value="0"/>
				</div>
		 	</div>
		</div>
		<div class="form-group">
			<label class="control-label" for="ID_NodeSetting[allow_like]" style="width:40%; float: left">Allow like</label>
		    <div style="width:60%; float: left">
				<select name="VNP_Settings[allow_like]" id="ID_NodeSetting[allow_like]" data-groups="allow_like_groups" class="form-control VNP_Permission">
					{for $g in $PGs as $k}
					<option value="{$k}"{if($k == $Data.extra.allow_like.value || ($Data.extra.allow_like.type == $k && $k == 'groups'))} selected{/if}>{$g.name}</option>
					{/for}
				</select>
				<div class="VNP_GroupsCtner allow_like_groups"{if($Data.extra.allow_like.type == 'groups')} style="display:block"{/if}>
					{for $UG in $UserGroups}
					<label class="checkbox-inline">
						{$UG.prefix}
						<input class="VNP_CheckBox"{if($Data.extra.allow_like.type == 'groups' && in_array($UG.gid, $Data.extra.allow_like.value))} checked{/if} type="checkbox" name="VNP_Settings[allow_like_groups][]" value="{$UG.gid}">
						{$UG.name}
					</label>
					{/for}
				</div>
		 	</div>
		</div>
		<div class="form-group">
			<label class="control-label" for="ID_NodeSetting[allow_rating]" style="width:40%; float: left">Allow rating</label>
		    <div style="width:60%; float: left">
				<select name="VNP_Settings[allow_rating]" id="ID_NodeSetting[allow_rating]" data-groups="allow_rating_groups" class="form-control VNP_Permission">
					{for $g in $PGs as $k}
					<option value="{$k}"{if($k == $Data.extra.allow_rating.value || ($Data.extra.allow_rating.type == $k && $k == 'groups'))} selected{/if}>{$g.name}</option>
					{/for}
				</select>
				<div class="VNP_GroupsCtner allow_rating_groups"{if($Data.extra.allow_rating.type == 'groups')} style="display:block"{/if}>
					{for $UG in $UserGroups}
					<label class="checkbox-inline">
						{$UG.prefix}
						<input class="VNP_CheckBox"{if($Data.extra.allow_rating.type == 'groups' && in_array($UG.gid, $Data.extra.allow_rating.value))} checked{/if} type="checkbox" name="VNP_Settings[allow_rating_groups][]" value="{$UG.gid}">
						{$UG.name}
					</label>
					{/for}
				</div>
		 	</div>
		</div>
		<div class="form-group">
			<label class="control-label" for="ID_NodeSetting[allow_comment]" style="width:40%; float: left">Allow comment</label>
		    <div style="width:60%; float: left">
				<select name="VNP_Settings[allow_comment]" id="ID_NodeSetting[allow_comment]" data-groups="allow_comment_groups" class="form-control VNP_Permission">
					{for $g in $PGs as $k}
					<option value="{$k}"{if($k == $Data.extra.allow_comment.value || ($Data.extra.allow_comment.type == $k && $k == 'groups'))} selected{/if}>{$g.name}</option>
					{/for}
				</select>
				<div class="VNP_GroupsCtner allow_comment_groups"{if($Data.extra.allow_comment.type == 'groups')} style="display:block"{/if}>
					{for $UG in $UserGroups}
					<label class="checkbox-inline">
						{$UG.prefix}
						<input class="VNP_CheckBox"{if($Data.extra.allow_comment.type == 'groups' && in_array($UG.gid, $Data.extra.allow_comment.value))} checked{/if} type="checkbox" name="VNP_Settings[allow_comment_groups][]" value="{$UG.gid}">
						{$UG.name}
					</label>
					{/for}
				</div>
		 	</div>
		</div>
		<div class="form-group">
			<label class="control-label" for="ID_Field[review_options]" style="width:40%; float: left">Review Options</label>
		    <div style="width:60%; float: left">
				<select name="VNP_Settings[review_opts]" id="ID_NodeSetting[review_opts]" class="form-control">
					<option value="default"{if($Data.extra.review_opts == 'default')} selected{/if}>Default</option>
					<option value="no"{if($Data.extra.review_opts == 'no')} selected{/if}>No</option>
					<option value="star"{if($Data.extra.review_opts == 'star')} selected{/if}>Star rating</option>
					<option value="comment"{if($Data.extra.review_opts == 'comment')} selected{/if}>Comment</option>
					<option value="star_or_comment"{if($Data.extra.review_opts == 'star_or_comment')} selected{/if}>Comment or rating</option>
					<option value="star_and_comment"{if($Data.extra.review_opts == 'star_and_comment')} selected{/if}>Comment and rating</option>
				</select>
		 	</div>
		</div>
		<div class="form-group">
			<label class="control-label" for="ID_NodeSetting[comment_approve]" style="width:40%; float: left">Comment approve</label>
		    <div style="width:60%; float: left">
				<select name="VNP_Settings[comment_approve]" id="ID_NodeSetting[comment_approve]" class="form-control">
					<option value="default"{if($Data.extra.comment_approve == 'default')} selected{/if}>Default</option>
					<option value="auto"{if($Data.extra.comment_approve == 'auto')} selected{/if}>Auto</option>
					<option value="author"{if($Data.extra.comment_approve == 'author')} selected{/if}>Node Author</option>
					<option value="mod"{if($Data.extra.comment_approve == 'mod')} selected{/if}>Moderator</option>
					<option value="smod"{if($Data.extra.comment_approve == 'smod')} selected{/if}>Super Moderator</option>
					<option value="admin"{if($Data.extra.comment_approve == 'admin')} selected{/if}>Administrator</option>
				</select>
		 	</div>
		</div>
		<div class="form-group">
			<label class="control-label" for="ID_NodeSetting[view_permission]" style="width:40%; float: left">View permission</label>
		    <div style="width:60%; float: left">
				<select name="VNP_Settings[view_permission]" id="ID_NodeSetting[view_permission]" data-groups="view_permission_groups" class="form-control VNP_Permission">
					{for $g in $PGs as $k}
					<option value="{$k}"{if($k == $Data.extra.view_permission.value || ($Data.extra.view_permission.type == $k && $k == 'groups'))} selected{/if}>{$g.name}</option>
					{/for}
				</select>
				<div class="VNP_GroupsCtner view_permission_groups"{if($Data.extra.view_permission.type == 'groups')} style="display:block"{/if}>
					{for $UG in $UserGroups}
					<label class="checkbox-inline">
						{$UG.prefix}
						<input class="VNP_CheckBox"{if($Data.extra.view_permission.type == 'groups' && in_array($UG.gid, $Data.extra.view_permission.value))} checked{/if} type="checkbox" name="VNP_Settings[view_permission_groups][]" value="{$UG.gid}">
						{$UG.name}
					</label>
					{/for}
				</div>
		 	</div>
		</div>
    </div>
</div>
<style type="text/css">
.VNP_NodeExtraSettings {box-shadow: 0 0 3px #CCC;
-webkit-box-shadow: 0 0 3px #CCC;
-moz-box-shadow: 0 0 3px #CCC;
border: 1px solid #CCC;
margin: 10px 0;}
.VNP_NodeExtraSettings .SettingHeader {
	background: #ECECEC;
	padding: 5px 8px;
	font-weight: bold;
	cursor: pointer;
}
.VNP_NodeExtraSettings .SettingsContent {
	background: #FFF; padding: 8px;
	border-top: 1px solid #CCC;
	display: none
 }
 .DatePickerCtner {
 	width: 60% !important;
 	float: left !important;
 }
 .checkbox-inline {
 	padding: 3px 0 !important;
 	width: 100%;
 }
 .VNP_CheckBox {
 	margin: 0 !important;
 	float: none !important;
 	margin-right: 5px !important;
 }
 .VNP_GroupsCtner {
    margin: 5px 0;
    border: 2px solid #CCC;
    padding: 5px !important;
    display: none
}
</style>
<script type="text/javascript">
jQuery(document).ready(function($) {
    $( ".datepicker").datepicker({
        showOtherMonths: true,
        selectOtherMonths: false,
        dateFormat: 'dd/mm/yy'
    });

    $('.VNP_Permission').change(function(event) {
		var obj = $(this);
		var GroupsCtner = obj.attr('data-groups');
		if(obj.val() == 'groups') {
			$('.' + GroupsCtner).fadeIn();
			/*
			VNP.Ajax({
				url: 'User/GetGroups',
				type: 'POST',
				success: function(res) {
					var GroupsString = '<div class="VNP_GroupsCtner">';
					console.log(res);
					$.each($.parseJSON(res), function(i,v) {
						GroupsString += '<label class="checkbox-inline">\
  											' + v.prefix + '<input class="VNP_CheckBox" type="checkbox" name="VNP_Settings[' + FieldName + '][]" value="' + v.gid + '">' + v.name + '\
										</label>'
					});
					GroupsString += '</div>';
					obj.parent().append(GroupsString);
					VNP.Loader.hide();
				}
			})
			*/
		}
		else $('.' + GroupsCtner).fadeOut();
	});
});
</script>