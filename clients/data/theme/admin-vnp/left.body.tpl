<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>{$TITLE}</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="{#GLOBAL_DATA_DIR}library/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="{#APPLICATION_DATA_DIR}theme/admin-vnp/assets/css/bootstrap.min.css" />
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic&subset=latin,vietnamese' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="{#APPLICATION_DATA_DIR}theme/admin-vnp/assets/css/ace.min.css" />
        <link rel="stylesheet" href="{#APPLICATION_DATA_DIR}theme/admin-vnp/css/style.css" />
        <link rel="stylesheet" href="{#APPLICATION_DATA_DIR}theme/admin-vnp/assets/css/jquery-ui.min.css" />
        <!--[if lte IE 9]>
        <link rel="stylesheet" href="{#APPLICATION_DATA_DIR}theme/admin-vnp/assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
        <![endif]-->
        <!--[if lte IE 9]>
        <link rel="stylesheet" href="{#APPLICATION_DATA_DIR}theme/admin-vnp/assets/css/ace-ie.min.css" />
        <![endif]-->
        {$Hook.header}
        <script src="{#APPLICATION_DATA_DIR}theme/admin-vnp/assets/js/ace-extra.min.js"></script>

        <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

        <!--[if lte IE 8]>
        <script src="{#APPLICATION_DATA_DIR}theme/admin-vnp/assets/js/html5shiv.min.js"></script>
        <script src="{#APPLICATION_DATA_DIR}theme/admin-vnp/assets/js/respond.min.js"></script>

        <![endif]-->
        <script src="{#APPLICATION_DATA_DIR}theme/admin-vnp/assets/js/ace-extra.min.js"></script>
        <script src="{#APPLICATION_DATA_DIR}theme/admin-vnp/assets/js/bootstrap.min.js"></script>
    </head>
    <body class="no-skin">
        {$Hook.before_body}
        <div id="navbar" class="navbar navbar-default">
            <script type="text/javascript">
                try{ace.settings.check('navbar' , 'fixed')}catch(e){}
            </script>

            <div class="navbar-container" id="navbar-container">
                <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                    <span class="sr-only">Toggle sidebar</span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>
                </button>

                <div class="navbar-header pull-left">
                    <a href="index.html" class="navbar-brand">
                        <small>
                            <i class="fa fa-leaf"></i>
                            THIETKEWEB2S
                        </small>
                    </a>
                </div>
                <div class="navbar-buttons navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="light-blue">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                <img class="nav-user-photo" src="{$MyData.avatar|Output::GetThumbLink:60,60}" alt="{$MyData.fullname}" />
								<span class="user-info">
									<small>Welcome,</small>
                                    {$MyData.fullname}
								</span>
                                <i class="ace-icon fa fa-caret-down"></i>
                            </a>
                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="{#APP_DOMAIN}/admincp/logout">
                                        <i class="ace-icon fa fa-power-off"></i>
                                        Logout
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /.navbar-container -->
        </div>
        <div class="main-container" id="main-container">

                <div id="sidebar" class="sidebar responsive">

                    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
                        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
                            <button class="btn btn-success">
                                <i class="ace-icon fa fa-signal"></i>
                            </button>

                            <button class="btn btn-info">
                                <i class="ace-icon fa fa-pencil"></i>
                            </button>

                            <button class="btn btn-warning">
                                <i class="ace-icon fa fa-users"></i>
                            </button>

                            <button class="btn btn-danger">
                                <i class="ace-icon fa fa-cogs"></i>
                            </button>
                        </div>

                        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                            <span class="btn btn-success"></span>

                            <span class="btn btn-info"></span>

                            <span class="btn btn-warning"></span>

                            <span class="btn btn-danger"></span>
                        </div>
                    </div><!-- /.sidebar-shortcuts -->

                    <ul class="nav nav-list">
                        <li class="{if($Controller=='')}active{/if}">
                            <a href="index.html">
                                <i class="menu-icon fa fa-tachometer"></i>
                                <span class="menu-text"> Dashboard </span>
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <li class="{if($Controller=='product' || $Controller=='product_category' || $Controller=='product_group' || $Controller=='author')}active{/if}">
                            <a href="{#BASE_DIR}" class="dropdown-toggle">
                                <i class="menu-icon fa fa-book"></i>
                                    <span class="menu-text">
                                        Quản lý sách
                                    </span>
                                <b class="arrow fa fa-angle-down"></b>
                            </a>
                            <b class="arrow"></b>
                            <ul class="submenu">
                                <li>
                                    <a href="{#BASE_DIR}product">Danh sách</a>
                                    <a href="{#BASE_DIR}product/AddNode">Thêm mới</a>
                                    <a href="{#BASE_DIR}product_category">Danh mục sách</a>
                                    <a href="{#BASE_DIR}product_group">Nhóm sách</a>
                                    <a href="{#BASE_DIR}author">Tác giả</a>
                                </li>
                            </ul>
                        </li>
                        <li class="{if($Controller=='invoice'  )}active{/if}" id="invoice">
                            <a href="{#BASE_DIR}" class="dropdown-toggle">
                                <i class="menu-icon fa fa-area-chart"></i>
                                    <span class="menu-text">
                                        Quản lý đặt hàng
                                    </span>
                                <b class="arrow fa fa-angle-down"></b>
                            </a>
                            <b class="arrow"></b>
                            <ul class="submenu">
                                <li>
                                    <a href="{#BASE_DIR}invoice">Tất cả đơn hàng</a>
                                    <a href="{#BASE_DIR}invoice/exportInvoice">Export</a>
                                    <a href="{#BASE_DIR}invoice_log/listLog">Lịch sử xác nhận</a>
                                </li>
                            </ul>
                        </li>

                        <li class="{if($Controller=='payment')}active{/if}" id="invoice">
                            <a href="{#BASE_DIR}" class="dropdown-toggle">
                                <i class="menu-icon fa fa-credit-card"></i>
                                    <span class="menu-text">
                                        Thanh toán
                                    </span>
                                <b class="arrow fa fa-angle-down"></b>
                                <ul class="submenu">
                                    <li><a href="{#BASE_DIR}payment">Tất cả module</a></li>
                                </ul>
                            </a>
                            <b class="arrow"></b>
                        </li>

                        <li class="{if($Controller=='review'  )}active{/if}" id="review">
                            <a href="{#BASE_DIR}" class="dropdown-toggle">
                                <i class="menu-icon fa fa-comment"></i>
                                    <span class="menu-text">
                                        Nhận xét
                                    </span>
                                <b class="arrow fa fa-angle-down"></b>
                            </a>
                            <b class="arrow"></b>
                            <ul class="submenu">
                                <li>
                                    <a href="{#BASE_DIR}review">Danh sách</a>
                                    <a href="{#BASE_DIR}review/settingReview">Cài đặt</a>
                                </li>
                            </ul>
                        </li>
                        <li class="{if($Controller=='cupon')}active{/if}">
                            <a href="{#BASE_DIR}" class="dropdown-toggle">
                                <i class="menu-icon fa fa-ticket"></i>
                                    <span class="menu-text">
                                        Mã giảm giá
                                    </span>
                                <b class="arrow fa fa-angle-down"></b>
                            </a>
                            <b class="arrow"></b>
                            <ul class="submenu">
                                <li>
                                    <a href="{#BASE_DIR}cupon">Danh sách</a>
                                    <a href="{#BASE_DIR}cupon/AddNode">Thêm mới</a>
                                </li>
                            </ul>
                        </li>

                        <li class="{if($Controller=='page' )}active{/if}">
                            <a href="{#BASE_DIR}" class="dropdown-toggle">
                                <i class="menu-icon fa fa-paper-plane"></i>
                                    <span class="menu-text">
                                        Page
                                    </span>
                                <b class="arrow fa fa-angle-down"></b>
                            </a>
                            <b class="arrow"></b>
                            <ul class="submenu">
                                <li>
                                    <a href="{#BASE_DIR}page">Danh sách</a>
                                    <a href="{#BASE_DIR}page/AddNode">Thêm mới</a>
                                </li>
                            </ul>
                        </li>

                        <li class="{if($Controller=='slider')}active{/if}">
                            <a href="{#BASE_DIR}" class="dropdown-toggle">
                                <i class="menu-icon fa fa-play"></i>
                                    <span class="menu-text">
                                        Slider
                                    </span>
                                <b class="arrow fa fa-angle-down"></b>
                            </a>
                            <b class="arrow"></b>
                            <ul class="submenu">
                                <li>
                                    <a href="{#BASE_DIR}slider">Danh sách</a>
                                    <a href="{#BASE_DIR}slider/AddNode">Thêm mới</a>
                                </li>
                            </ul>
                        </li>

                        <li class="{if($Controller=='menu' || $Controller=='menu_group')}active{/if}">
                            <a href="{#BASE_DIR}" class="dropdown-toggle">
                                <i class="menu-icon fa fa-navicon"></i>
                                    <span class="menu-text">
                                        Menus
                                    </span>
                                <b class="arrow fa fa-angle-down"></b>
                            </a>
                            <b class="arrow"></b>
                            <ul class="submenu">
                                <li>
                                    <a href="{#BASE_DIR}menu">Danh sách</a>
                                    <a href="{#BASE_DIR}menu/AddNode">Thêm mới</a>
                                    <a href="{#BASE_DIR}menu_group">Nhóm menu</a>
                                </li>
                            </ul>
                        </li>

                        <li class="{if($Controller=='User')}active{/if}">
                            <a href="{#BASE_DIR}" class="dropdown-toggle">
                                <i class="menu-icon fa fa-user"></i>
                                    <span class="menu-text">
                                        Thành viên
                                    </span>
                                <b class="arrow fa fa-angle-down"></b>
                            </a>
                            <b class="arrow"></b>
                            <ul class="submenu">
                                <li>
                                    <a href="{#BASE_DIR}User">Danh sách</a>
                                    <a href="{#BASE_DIR}User/AddUser">Thêm mới</a>
                                </li>
                            </ul>
                        </li>
                        
                        <li class="{if($Controller=='FileManager')}active{/if}">
                            <a href="{#BASE_DIR}FileManager" class="dropdown-toggle">
                                <i class="menu-icon fa fa-picture-o"></i>
                                    <span class="menu-text">
                                        Quản lý file
                                    </span>
                                <b class="arrow fa fa-angle-down"></b>
                            </a>
                            <b class="arrow"></b>
                            <ul class="submenu">
                                <li>
                                    <a href="{#BASE_DIR}FileManager">Danh sách</a>
                                    <a href="{#BASE_DIR}FileManager/upload">Tải lên</a>
                                </li>
                            </ul>
                        </li>

                        <li class="{if($Controller=='Setting' || $Controller =='extension')}active{/if}">
                            <a href="{#BASE_DIR}FileManager" class="dropdown-toggle">
                                <i class="menu-icon fa fa-cog"></i>
                                    <span class="menu-text">
                                        Cài đặt chung
                                    </span>
                                <b class="arrow fa fa-angle-down"></b>
                            </a>
                            <b class="arrow"></b>
                            <ul class="submenu">
                                <li>
                                    <a href="{#BASE_DIR}Setting">Thông số hệ thống</a>
                                    <a href="{#BASE_DIR}Setting/theme/options">Thông số giao diện</a>
                                    <a href="{#BASE_DIR}extension/dashboard">Trình cắm phát triển</a>
                                </li>
                            </ul>
                        </li>
                    </ul><!-- /.nav-list -->

                <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
                    <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
                </div>


            </div>
            <div class="main-content">
                <div class="main-content-inner">
                    <div class="breadcrumbs" id="breadcrumbs">
                        <script type="text/javascript">
                            try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
                        </script>
                        <!-- /.breadcrumb -->
                        {if(!empty($Helper.States))}
                            <ul class="breadcrumb" id="board-breadcrumbs">
                                {for $StateItem in $Helper.States}
                                    <li><a href="{$StateItem.route}">{$StateItem.title}</a></li>
                                {/for}
                            </ul>
                        {/if}
                        <div class="nav-search" id="nav-search">

                        </div><!-- /.nav-search -->
                    </div>
                    <div class="page-content">
                        {if(!empty($Helper.PageInfo) || !empty($Helper.Featured))}
                        <div class="page-header">
                                <div class="FeaturedPanel clearfix">
                                    {if(!empty($Helper.Featured))}
                                        {for $FP in $Helper.Featured}
                                            <a href="{$FP.url}" title="{$FP.text}">
                                                <button type="button" class="btn btn-white btn-success">{$FP.text}</button>
                                            </a>
                                        {/for}
                                    {/if}
                                </div>
                        </div>
                        {/if}
                        <div class="page-inner">
                            {for $NT in $Helper.Notifies}
                                {for $n in $NT}
                                    <div class="alert alert-{$n.type}">{$n.content}</div>
                                {/for}
                            {/for}
                            {$BODY}
                            {$VNP_Paging}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{#APPLICATION_DATA_DIR}theme/admin-vnp/assets/js/jquery-ui.min.js"></script>
        <script src="{#APPLICATION_DATA_DIR}theme/admin-vnp/assets/js/jquery.ui.touch-punch.min.js"></script>
        <!-- ace scripts -->
        <script src="{#APPLICATION_DATA_DIR}theme/admin-vnp/assets/js/ace-elements.min.js"></script>
        <script src="{#APPLICATION_DATA_DIR}theme/admin-vnp/assets/js/ace.min.js"></script>
        {$Hook.footer}
    </body>
</html>