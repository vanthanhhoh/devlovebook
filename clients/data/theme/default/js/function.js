$(document).ready(function(){
    var lc_w = $('.all-category').width();
    var lc_h = $('.all-category').height();
    var c_w = $('.container').width();
    var mg_w = c_w-lc_w;
    $('.mega_menu').css('left',lc_w+'px');
    $('.mega_menu').css('width',mg_w+'px');
    $('.mega_menu').css('height',lc_h+'px');
    $('.star-only').raty({
        number: function() {
            return $(this).attr('data-number');
        },
        score: function() {
            return $(this).attr('data-score');
        },
        starOff : '/clients/data/theme/default/images/star-off.png',
        starOn  : '/clients/data/theme/default/images/star-on.png',
        readOnly   : true
    });
    $('.rate_jquery').raty({
        number: 5,
        score: 0,
        starOff : '/clients/data/theme/default/images/star-off.png',
        starOn  : '/clients/data/theme/default/images/star-on.png',
        target:     '#score',
        targetKeep: true,
        targetType: 'number'
    });
    $('.carousel-book-horizontal').lightSlider({
        item:4,
        loop:false,
        slideMove:2,
        pager: false,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed:600,
        responsive : [
            {
                breakpoint:1200,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:6
                }
            },
            {
                breakpoint:480,
                settings: {
                    item:2,
                    slideMove:1,
                    pager: true,
                    controls: false
                }
            }
        ]
    });

    initSlider('slider-ban-chay');
    initSlider('slider-ban-chay-hai');

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var s = $(this).attr('aria-controls');
        $('#slider-'+s).lightSlider().destroy();
        $('#slider-'+s).lightSlider({
            item:4,
            loop:false,
            slideMove:2,
            pager: false,
            easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
            speed:600,
            responsive : [
                {
                    breakpoint:1200,
                    settings: {
                        item:3,
                        slideMove:1,
                        slideMargin:6
                    }
                },
                {
                    breakpoint:480,
                    settings: {
                        item:2,
                        slideMove:1,
                        pager: true,
                        controls: false
                    }
                }
            ]
        }).refresh();
    });
    $('.slider-small-adapter').lightSlider({
        adaptiveHeight:true,
        item:1,
        slideMargin:0,
        loop:true,
        controls: false,
        pager: true
    });
    $('#category_list_show').click(function(){
        var status = $(this).attr('aria-expanded');
        if(status=='true'){
            $('#after_header_wapper').css('display','none');
        }
        else {
            $('#after_header_wapper').css('display','block');
            var lc_w = $('.menu-navigator').width();
            var c_w = $('.container').width();
            var mg_w = c_w-lc_w;
            $('.mega_menu').css('left',lc_w+'px');
            $('.mega_menu').css('width',mg_w+'px');
        }
    });
    $(document).click(function(e) {
        if( e.target.id != 'category_list_show') {
            $('#after_header_wapper').css('display','none');
        }
    });
    $("a.fancybox").fancybox({
        width    : '90%',
        height: '80%',
        minHeight : 520,
        autoSize: false,
        autoScale : false,
        autoResize  : false,
        transitionOut   : 'none',
        transitionIn    : 'none',
        type        : 'iframe'
    });
    $('#loginModal').validate({
        errorElement: "span"
    });
    $('#registerFormModal').validate({
        errorElement: "span"
    });
    $( ".login_modal" ).click(function() {
        if($('#loginModal').valid()==true){
            var email = $('#loginModal input[name="email"]').val();
            var password = $('#loginModal input[name="password"]').val();
            $('.wapper_loader').css('display','block');
            $.ajax({
                url:'/Auth/login',
                type:'post',
                dataType:'json',
                data:{email:email,password:password},
                success: function(res){
                    $('.wapper_loader').css('display','none');
                    if(res.status==1){
                        $.notify({
                            title: res.message,
                            message: res.message
                        },{
                            type: 'success',
                            z_index : 999999999
                        });

                        location.reload().delay(3000);
                    }
                    else {
                        $.notify({
                            title: res.message,
                            message: res.message
                        },{
                            type: 'danger',
                            z_index : 999999999
                        });
                    }
                }
            })
        }
    });
    $( ".register_modal" ).click(function() {
        if($('#registerFormModal').valid()==true){
            var email = $('#registerFormModal input[name="email"]').val();
            var password = $('#registerFormModal input[name="password"]').val();
            var fullname = $('#registerFormModal input[name="fullname"]').val();
            var tokenregister = $('#registerFormModal input[name="tokenregister"]').val();
            var phone = $('#registerFormModal input[name="phone"]').val();
            var birthday = $('#registerFormModal input[name="birthday"]').val();
            var gender = $('#registerFormModal select[name="gender"]').val();
            $('.wapper_loader').css('display','block');
            $.ajax({
                url:'/Auth/register',
                type:'post',
                dataType:'json',
                data:{email:email,password:password,fullname:fullname,tokenregister:tokenregister,phone:phone,birthday:birthday,gender:gender},
                success: function(res){
                    $('.wapper_loader').css('display','none');
                    if(res.status==1){
                        $.notify({
                            title: 'Thông báo',
                            message: res.message
                        },{
                            type: 'success',
                            z_index : 999999999
                        });

                        location.reload().delay(3000);
                    }
                    else {
                        $.notify({
                            title: 'Lỗi',
                            message: res.message
                        },{
                            type: 'danger',
                            z_index : 999999999
                        });
                    }
                }
            })
        }
    });
    $.fn.datepicker.dates['vi'] = {
        days: ["Chủ nhật","Thứ hai","Thứ ba","Thứ tư","Thứ năm","Thứ sáu","Thứ bảy"],
        daysShort: ["CN","Thứ 2","Thứ 3","Thứ 4","Thứ 5","Thứ 6","Thứ 7"],
        daysMin: ["CN","T2","T3","T4","T5","T6","T7"],
        months: ["Tháng 1","Tháng 2","Tháng 3","Tháng 4","Tháng 5","Tháng 6","Tháng 7","Tháng 8","Tháng 9","Tháng 10","Tháng 11","Tháng 12"],
        monthsShort: ["Th1","Th2","Th3","Th4","Th5","Th6","Th7","Th8","Th9","Th10","Th11","Th12"],
        today: "Hôm nay",
        clear: "Xóa",
        format: "dd/mm/yyyy",
        titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
        weekStart: 0
    };
    $('#datepicker').datepicker({
        format: "dd/mm/yyyy",
        language: "vi",
        orientation: "top right",
        autoclose: true
    });

    $('.autocompleteSearch').typeahead({
        source: function (q, process) {
            return $.post('/ServiceSys/searchSugget', {
                query: q
            }, function (response) {
                var data = [];
                for (var i in response) {
                    data.push(response[i].name + "#" +response[i].url+'#'+response[i].type);
                }
                data.pop();
                return process(data);
            });
        },
        highlighter: function (item) {
            var parts = item.split('#'),
                html = '<div class="typeahead">';
            html += '<div class="suggetItem">';
            html += '<div class="suggetName"><div class="boldName"> ' + parts[0] + '<div><div class="smallType">'+parts[2]+'</div></div>';
            html += '</div>';
            html += '<div class="clearfix"></div>';
            html += '</div>';
            return html;
        },
        updater: function (item) {
            var parts = item.split('#');
            window.location.href= parts[1];
        }

    });
    $('#nav_left').sidr({
        source : 'http://lovebook.local/ServiceSys/generateMobilemenu',
        displace : true,
        onOpen   : function(e){
            $('.wapper_sdir').show();
        }
    });
    $( window ).resize(function () {
        closeSdir();
    });

});

function initSlider(sliderId) {
    $('#'+sliderId).lightSlider({
        item:4,
        loop:false,
        slideMove:2,
        pager: false,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed:600,
        responsive : [
            {
                breakpoint:1200,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:6
                }
            },
            {
                breakpoint:480,
                settings: {
                    item:2,
                    slideMove:1,
                    pager: true,
                    controls: false
                }
            }
        ]
    });
}

function addCart(id,qty){
    VNP.Ajax({
        url:'Cart/addCart',
        type: 'post',
        data:{id:id,qty:qty},
        success: function(res){
            if(res.status==1){
                $.notify({
                    title: '<strong>Thông báo</strong>',
                    message: res.message
                },{delay: 1000,
                        timer: 1000},
                    {
                    type: 'success'
                });
                $('.addcart-goods-num').html(res.total);
            }
            else {
                $.notify({
                    title: '<strong>Thông báo</strong>',
                    message: res.message
                },{
                    type: 'success'
                });
            }
            VNP.Loader.hide();

        }
    },'json');
}
function quickBuy(id,qty){
    VNP.Ajax({
        url:'Cart/addCart',
        type: 'post',
        data:{id:id,qty:qty},
        success: function(res){
            if(res.status==1){
                window.location.href='/gio-hang.html';
            }
            else {
                $.notify({
                    title: '<strong>Thông báo</strong>',
                    message: res.message
                },{
                    type: 'success'
                });
            }
            VNP.Loader.hide();

        }
    },'json');
}
function removeItem(e){
    var id = e.data('id');
    VNP.Ajax({
        url:'Cart/removeItem',
        type: 'post',
        data:{id:id},
        success: function(res){
            if(res.status==1){
                $.notify({
                    title: '<strong>Thông báo</strong>',
                    message: res.message
                },{
                    type: 'success'
                });
                location.reload();
                $('.addcart-goods-num').html(res.total);
            }
            else {
                $.notify({
                    title: '<strong>Thông báo</strong>',
                    message: res.message
                },{
                    type: 'danger'
                });
            }
            VNP.Loader.hide();
        }
    },'json');
}

function changeItem(e){
    var id = e.data('id');
    var qty = e.val();
    VNP.Ajax({
        url:'Cart/changeItem',
        type: 'post',
        data:{id:id,qty:qty},
        success: function(res){
            if(res.status==1){
                $.notify({
                    title: '<strong>Thông báo</strong>',
                    message: res.message
                },{
                    type: 'success'
                });
                location.reload();
                $('.addcart-goods-num').html(res.total);
            }
            else {
                $.notify({
                    title: '<strong>Thông báo</strong>',
                    message: res.message
                },{
                    type: 'danger'
                });
            }
            VNP.Loader.hide();

        }
    },'json');
}

function addCupon(){
    var cupon = $('#coupon').val();
    VNP.Ajax({
        url:'Cart/addCupon',
        type: 'post',
        data:{cupon:cupon},
        success: function(res){
            if(res.status==1){
                $.notify({
                    title: '<strong>Thông báo</strong>',
                    message: res.message
                },{
                    type: 'success'
                });
                location.reload().delay(3000);
            }
            else {
                $.notify({
                    title: '<strong>Thông báo</strong>',
                    message: res.message
                },{
                    type: 'danger'
                });
            }
            if(res.status==-1){
                $('#login').modal('show');
            }
            VNP.Loader.hide();
        }
    },'json')
}
function sendReview(){
    var rate = $('#score').val();
    var title = $('#title_review').val();
    var comment = $('#comment_view').val();
    var product = $('#product_id').val();
    VNP.Ajax({
        url:'Review/addReview',
        type: 'post',
        data:{rate:rate,title:title,comment:comment,product:product},
        success: function(res){
            if(res.status==1){
                $.notify({
                    title: '<strong>Thông báo</strong>',
                    message: res.message
                },{
                    type: 'success'
                });
                location.reload().delay(3000);
            }
            else {
                $.notify({
                    title: '<strong>Thông báo</strong>',
                    message: res.message
                },{
                    type: 'danger'
                });
            }
            if(res.status==-1){
                $('#login').modal('show');
            }
            VNP.Loader.hide();
        }
    },'json')
}
function addFavorite(e){
    var id = e.data('product');
    VNP.Ajax({
        url:'Favorite/addFavorite',
        type: 'post',
        data:{product:id},
        success: function(res){
            if(res.status==1){
                $.notify({
                    title: '<strong>Thông báo</strong>',
                    message: res.message
                },{
                    type: 'success'
                });
                location.reload().delay(3000);
            }
            else {
                $.notify({
                    title: '<strong>Thông báo</strong>',
                    message: res.message
                },{
                    type: 'danger'
                });
            }
            if(res.status==-1){
                $('#login').modal('show');
            }
            VNP.Loader.hide();
        }
    },'json');
}
function readedNotification(e){
    var id = e.data('id');
    VNP.Ajax({
        url:'Notification/readedNotification',
        type: 'post',
        data:{id:id},
        success: function(res){
            if(res.status==1){
                $.notify({
                    title: '<strong>Thông báo</strong>',
                    message: res.message
                },{
                    type: 'success'
                });
                location.reload().delay(3000);
            }
            else {
                $.notify({
                    title: '<strong>Thông báo</strong>',
                    message: res.message
                },{
                    type: 'danger'
                });
            }
            VNP.Loader.hide();
        }
    },'json');
}
function removeFavorite(e){
    var id = e.data('id');
    VNP.Ajax({
        url:'Favorite/removeFavorite',
        type: 'post',
        data:{id:id},
        success: function(res){
            if(res.status==1){
                $.notify({
                    title: '<strong>Thông báo</strong>',
                    message: res.message
                },{
                    type: 'success'
                });
                location.reload().delay(3000);
            }
            else {
                $.notify({
                    title: '<strong>Thông báo</strong>',
                    message: res.message
                },{
                    type: 'danger'
                });
            }
            VNP.Loader.hide();
        }
    },'json');
}
function openPopup(){
    var w = window.open("/Auth/register_remote/facebook","_blank","toolbar=yes, location=yes, directories=no, status=no, menubar=yes, scrollbars=yes, resizable=no, copyhistory=yes, width=400, height=400");
}
function closeSdir(){
    $.sidr('close', 'sidr');
    $('.wapper_sdir').hide();
}