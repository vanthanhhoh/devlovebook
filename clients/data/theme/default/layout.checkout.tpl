<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>{$TITLE}</title>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="{function}echo APP_DOMAIN.Router::GenerateThisRoute(){/function}"/>
    <meta name="geo.region" content="VN-HN" />
    <meta name="geo.placename" content="Hà Nội" />
    {if(!empty($VNP_SiteImage))}<meta property="og:image" content="{$VNP_SiteImage}"/>{/if}
    <meta content="INDEX, FOLLOW" name="ROBOTS">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicons
    ================================================== -->
    {for $met in $META as $mk}<meta name="{$mk}" content="{$met}"/>{/for}
    {for $met in $META_PROPERTY as $mk}<meta property="{$mk}" content="{$met}"/>{/for}
    {$Hook.header}
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css"  href="{#APPLICATION_DATA_DIR}theme/{#CURRENT_THEME}/css/bootstrap.min.v3.css"/>
    <link rel="stylesheet" type="text/css" href="{#APPLICATION_DATA_DIR}theme/{#CURRENT_THEME}/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="{#APPLICATION_DATA_DIR}theme/{#CURRENT_THEME}/css/lightslider.min.css"/>
    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css"  href="{#APPLICATION_DATA_DIR}theme/{#CURRENT_THEME}/css/checkout.css"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="{#APPLICATION_DATA_DIR}theme/{#CURRENT_THEME}/js/lightslider.min.js"></script>
</head>
<body>
<div class="shipping-header">
    <div class="container">
        <div class="row">
            <div class="col-md-2 logo">
                <a href="/"><img src="{$Op.logo}" height="62" width="149" class="img-responsive" alt="Logo"></a>
            </div>
            <div class="col-md-8">
                <div class="row bs-wizard" style="border-bottom:0;">
                    <div class="col-xs-4 bs-wizard-step {if($step>1)} complete {else} active{/if}">
                        <div class="text-center bs-wizard-stepnum complete">Đăng nhập</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="bs-wizard-dot"></a>
                    </div>

                    <div class="col-xs-4 bs-wizard-step {function}if($step>2){echo "complete";} if($step==2){echo "active";} if($step<2){echo "disabled";}{/function}"><!-- complete -->
                        <div class="text-center bs-wizard-stepnum">Địa chỉ giao hàng</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="bs-wizard-dot"></a>
                    </div>

                    <div class="col-xs-4 bs-wizard-step {function}if($step>3){echo "complete";} if($step==3){echo "active";} if($step<3){echo "disabled";}{/function}"><!-- complete -->
                        <div class="text-center bs-wizard-stepnum">Thanh toán</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="bs-wizard-dot"></a>
                    </div>
                </div>
            </div>
            <div class="col-md-2 hotline">
                <div class="hotline_inner">
                    <div class="pull-left icon">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                    </div>
                    <div class="pull-left phone">
                        091.8226.017
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="notify">
    <div class="container">
        {$Notify}
    </div>
</div>
{$BODY}
<div class="shipping-footer">
    <div class="container">
        <p class="terms">
            <a href="#" target="_blank">Điều khoản sử dụng</a>|
            <a href="#" target="_blank">Chính sách đổi trả</a>|
            <a href="#" target="_blank">Chính sách bảo mật</a>
        </p>
        <p class="copyrightF">© 2016 - Bản quyền của Nhà sách giáo dục Lovebook</p>
        <div class="clearfix"></div>
    </div>
</div>
<div class="wapper_loader">

</div>
<script src="{#APPLICATION_DATA_DIR}theme/{#CURRENT_THEME}/js/bootstrap.min.js"></script>
<script src="{#APPLICATION_DATA_DIR}theme/{#CURRENT_THEME}/js/bootstrap-notify.min.js"></script>
{$Hook.footer}
<script>
    $(document).ready(function(){
        $.fn.datepicker.dates['vi'] = {
            days: ["Chủ nhật","Thứ hai","Thứ ba","Thứ tư","Thứ năm","Thứ sáu","Thứ bảy"],
            daysShort: ["CN","Thứ 2","Thứ 3","Thứ 4","Thứ 5","Thứ 6","Thứ 7"],
            daysMin: ["CN","T2","T3","T4","T5","T6","T7"],
            months: ["Tháng 1","Tháng 2","Tháng 3","Tháng 4","Tháng 5","Tháng 6","Tháng 7","Tháng 8","Tháng 9","Tháng 10","Tháng 11","Tháng 12"],
            monthsShort: ["Th1","Th2","Th3","Th4","Th5","Th6","Th7","Th8","Th9","Th10","Th11","Th12"],
            today: "Hôm nay",
            clear: "Xóa",
            format: "dd/mm/yyyy",
            titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
            weekStart: 0
        };
        $('#datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "vi",
            orientation: "top right",
            autoclose: true
        });
    })
</script>
</body>
</html>
