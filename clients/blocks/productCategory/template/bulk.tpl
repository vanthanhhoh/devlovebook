<div class="book-category-style-2">
    <div class="container">
        <div class="box-title">
            <div class="row">
                <div class="col-md-3 no-padding-right">
                    <h2 class="box-title-text">{$data.block_title}</h2>
                </div>
                <div class="col-md-9 no-padding-left-element" style="padding-left: 0">
                    <div class="box-title-line">
                        <ul class="sub-category hidden-xs">
                            {for $cats in $child}
                                {if($cats!=$category['product_category_id'])}
                                    <li>
                                        {function}$url = Router::Generate('ProductAuthor',array('author'=>$cat[$cats]['url'])){/function}
                                        <a href="{$url}">{function}echo $cat[$cats]['title']{/function}</a>
                                    </li>
                                {/if}
                            {/for}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-book-style-3">
            {for $book in $product}
                {function}
                    $read = Router::Generate('ProductReview',array('product'=>$book['url'],'pid'=>$book['product_id']));
                    $url  = Router::Generate('ProductDetail',array('product'=>$book['url'],'pid'=>$book['product_id']));
                {/function}
                <div class="book-item-style-3">
                    <div class="book-item-style-3-inner">
                        <div class="book-img pull-left">
                            <img src="{$book.image}" alt="{$book.title}" width="100%"/>
                        </div>
                        <div class="book-infomation pull-left">
                            <div class="book-title">{$book.title}</div>
                            <div class="book-author">TG: {function}echo $author[$book['author']]['title']{/function}</div>
                            <div class="book-price">
                                {if($book.price_sale>0)}
                                    <span class="newprice">{$book.price_sale|Filter::NumberFormat} đ</span>
                                    <span class="oldprice">{$book.price|Filter::NumberFormat} đ</span>
                                {else}
                                    <span class="newprice">{$book.price|Filter::NumberFormat} đ</span>
                                {/if}
                            </div>
                            <div class="book-preview hidden-xs">
                                <div class="star-only" data-number="5" data-score="3"></div>
                                <span class="count-rate">(có 3 đánh giá)</span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="book-active">
                                <a href="{$read}" class="book-active-button read-frist fancybox">Đọc thử</a>
                                <a href="{$url}" class="book-active-button buy-now">Mua ngay</a>
                            </div>
                        </div>
                    </div>
                </div>
            {/for}
        </div>
    </div>
</div>