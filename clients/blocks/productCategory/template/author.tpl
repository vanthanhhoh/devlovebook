<div class="book-category-style-1">
    <div class="container">
        <div class="box-title">
            <div class="row">
                <div class="col-md-3 no-padding-right">
                    <h2 class="box-title-text">{$data.block_title}</h2>
                </div>
                <div class="col-md-9 no-padding-left-element" style="padding-left: 0">
                    <div class="box-title-line">
                        <ul class="sub-category hidden-xs">
                            {for $cats in $child}
                                {if($cats!=$category['product_category_id'])}
                                    <li>
                                        <a href="/{function}echo $cat[$cats]['url']{/function}/">{function}echo $cat[$cats]['title']{/function}</a>
                                    </li>
                                {/if}
                            {/for}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="category-content">
            <div class="row">
                <div class="col-md-9 no-padding-right">
                    <div class="slider-category hidden-xs">
                        {function}
                            if($category['banner']!=''){
                                $banner = explode(',',$category['banner']);
                            }
                            else $banner = array();
                        {/function}
                        <ul class="slider-small-adapter">
                            {for $banners in $banner}
                                <li>
                                    <img src="{$banners}" width="100%" alt="{$category.title}"/>
                                </li>
                            {/for}
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="list-author">
                        <ul>
                            {for $item in $authorOf}
                                {function}
                                    $auth = $author[$item];
                                    $url = Router::Generate('ProductAuthor',array('author'=>$auth['url']));
                                {/function}
                                <li>
                                    <a href="{$url}">
                                        <div class="pull-left author-image">
                                            <img src="{$auth.image|Output::GetThumbLink:80,80}" alt="{$auth.title}" width="100%"/>
                                        </div>
                                        <div class="pull-left author-name">
                                            <p class="name">{$auth.title}</p>
                                            <p class="avchievement">{$auth.description}</p>
                                        </div>
                                    </a>
                                </li>
                            {/for}
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="book-in-category">
            <ul class="group-tabs" role="tablist">
                {for $item in $group as $gid}
                    <li role="presentation" {if($gid==0)}class="active"{/if}><a href="#{$item.url}" aria-controls="{$item.url}" role="tab" data-toggle="tab">{$item.title}</a></li>
                {/for}
            </ul>

            <div class="tab-content">
                {for $item in $group as $gid}
                    <div role="tabpanel" class="tab-pane {if($gid==0)}active in{/if}" id="{$item.url}">
                        <div id="slider-{$item.url}">
                            {for $book in $item.product}
                                {function}
                                    $read = Router::Generate('ProductReview',array('product'=>$book['url'],'pid'=>$book['product_id']));
                                    $url  = Router::Generate('ProductDetail',array('product'=>$book['url'],'pid'=>$book['product_id']));
                                {/function}
                                <div class="book-item">
                                    <a href="{$url}" title="{$book.title}">
                                        <div class="book-img pull-left">
                                            <img src="{$book.image|Output::GetThumbLink:300,425}" alt="{$book.title}" width="100%"/>
                                        </div>
                                        <div class="book-infomation pull-left">
                                            <div class="book-title">{$book.title}</div>
                                            <div class="book-author">TG: {function}echo $author[$book['author']]['title']{/function}</div>
                                            <div class="book-price">
                                                {if($book.price_sale>0)}
                                                    <span class="newprice">{$book.price_sale|Filter::NumberFormat} đ</span>
                                                    <span class="oldprice">{$book.price|Filter::NumberFormat} đ</span>
                                                {else}
                                                    <span class="newprice">{$book.price|Filter::NumberFormat} đ</span>
                                                {/if}
                                            </div>
                                            <div class="book-preview hidden-xs">
                                                <div class="star-only" data-number="5" data-score="3"></div>
                                                <span class="count-rate">(có 3 đánh giá)</span>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="book-active hidden-sm hidden-xs">
                                                <a href="{$read}" class="book-active-button read-frist fancybox">Đọc thử</a>
                                                <a href="{$url}" class="book-active-button buy-now">Mua ngay</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            {/for}
                        </div>
                    </div>
                {/for}
            </div>
        </div>
        <a href="" class="view_all">Xem tất cả</a>
    </div>
</div>