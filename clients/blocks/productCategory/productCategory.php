<?php
/**
 * Created by PhpStorm.
 * User: THANH
 * Date: 30/06/2016
 * Time: 3:04 CH
 */

class productCategory extends BaseBlock {

    private $blockContent;

    public function Main($params)
    {
        $catid = $params['catid'];
        $category = DB::Query('product_category')->Where('product_category_id','=',$catid)->Get();
        if($category->status && $category->num_rows==1){
            $category = $category->Result[0];
            $child = clients::GetChildID(frontend::$product_category,$catid);
            $product = frontend::$product;
            $group = frontend::$product_group;
            foreach($group as $gkey => $gitem){
                $group[$gkey]['product'] = array();
            }
            $authorOf = array();
            $productInCat = array();
            foreach($product as $item){
                if(in_array($item['product_category'],$child)){ // Incategory
                    $item['product_group']= explode(',',$item['product_group']);
                    foreach($item['product_group'] as $gs){
                        $kg = array_search($gs, array_column($group, 'product_group_id'));
                        if(is_numeric($kg)){
                            $group[$kg]['product'][]= $item;
                        }
                    }
                    if($item['author']>0){
                        $authorOf[] = $item['author'];
                    }
                    $productInCat[] = $item;
                }
            }
            $authorOf = array_unique($authorOf);
            $author = frontend::$author;

            $this->blockContent = $this->View($params['template'])
                                       ->Assign('category',$category)
                                       ->Assign('cat',frontend::$product_category)
                                       ->Assign('child',$child)
                                       ->Assign('group',$group)
                                       ->Assign('authorOf',$authorOf)
                                       ->Assign('author',$author)
                                       ->Assign('data',$params)
                                       ->Assign('product',$productInCat)
                                       ->Output();
        }
    }

    public function Output()
    {
        return $this->blockContent;
    }
}