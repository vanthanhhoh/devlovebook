<?php
/**
 * Created by PhpStorm.
 * User: THANH
 * Date: 30/06/2016
 * Time: 1:39 CH
 */

class groupProduct extends BaseBlock {

    private $blockContent;

    public function Main($params)
    {
        $gid = $params['gid'];
        $group = DB::Query('product_group')->Where('product_group_id','=',$gid)->Get();
        if($group->status && $group->num_rows==1){
            $group= $group->Result[0];
            $product = DB::Query('product')->Where('product_group','INCLUDE',$gid)->Limit(20)->Get()->Result;
            $this->blockContent = $this->View($params['template'])->Assign('data',$params)
                ->Assign('product',$product)->Assign('author',frontend::$author)->Output();
        }

    }

    public function Output()
    {
        return $this->blockContent;
    }
}