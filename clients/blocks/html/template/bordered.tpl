<style type="text/css">
    .bordered-block {
        border: 1px solid #CCC;
        border-radius: 3px;
        padding: 5px;
        margin: 10px;
    }
</style>
<div class="bordered-block">
    {$value}
</div>