<?php

class html extends BaseBlock {

    private $blockContent = '';

    public function Main($html) {
        $class = isset($html['block_class']) ? $html['block_class'] : '';
        $this->blockContent = $this->View($html['template'])
            ->Assign('value', $html['value'])
            ->Assign('class', $class)
            ->Output();
    }
    public function Output() {
        return $this->blockContent;
    }
}