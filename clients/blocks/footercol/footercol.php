<?php
/**
 * Created by PhpStorm.
 * User: THANH
 * Date: 01/07/2016
 * Time: 5:19 CH
 */

class footercol extends BaseBlock {
    private $blockContent;
    public function Main($params)
    {
        $gid = $params['gid'];
        $group = DB::Query('menu_group')->Where('menu_group_id','=',$gid)->Get();
        if($group->status && $group->num_rows==1){
            $menu = DB::Query('menu')->Where('menu_group','=',$gid)->Get()->Result;
            $this->blockContent = $this->View($params['template'])->Assign('menu',$menu)->Assign('data',$params)->Output();
        }
    }

    public function Output()
    {
        return $this->blockContent;
    }
}