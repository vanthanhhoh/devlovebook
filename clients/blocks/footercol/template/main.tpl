<div class="footerCol">
    <h5 class="footerColtitle">{$data.block_title}</h5>
    <ul>
        {for $item in $menu}
            <li>
                <a href="{$item.url}" title="{$item.title}">{$item.title}</a>
            </li>
        {/for}
    </ul>
</div>