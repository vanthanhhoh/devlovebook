<ul class="{$data.class}">
    {for $item in $menu}
        <li>
            <a href="{$item.url}" title="{$item.title}">{$item.title}</a>
        </li>
    {/for}
</ul>