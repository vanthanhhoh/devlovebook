<?php

if( !defined('VNP_SYSTEM') && !defined('VNP_APPLICATION') ) die('Access denied!');


define('APP_DOMAIN', 'http://lovebook.local');

define('DATA_DIR', 'data');
define('SESSION_DIR', 'session');
define('TEMPLATE_DIR', 'template');
define('CACHE_DIR', 'cache');
define('BLOCK_BASE_PATH', APPLICATION_PATH . 'blocks' . DIRECTORY_SEPARATOR);
define('SESSION_PATH', APPLICATION_PATH . DATA_DIR . DIRECTORY_SEPARATOR . SESSION_DIR . DIRECTORY_SEPARATOR);
define('TEMPLATE_PATH', APPLICATION_PATH . DATA_DIR . DIRECTORY_SEPARATOR . TEMPLATE_DIR . DIRECTORY_SEPARATOR);
define('CACHE_PATH', APPLICATION_PATH . DATA_DIR . DIRECTORY_SEPARATOR . CACHE_DIR . DIRECTORY_SEPARATOR);
define('EXTENSION_PATH', APPLICATION_PATH . 'extensions' . DIRECTORY_SEPARATOR);
define('APPLICATION_DATA_DIR', APPLICATION_BASE . '/' . DATA_DIR . '/');
define('THUMB_BASE', '/Thumbnail/');
define('NODE_CACHE_FILE', CACHE_PATH . 'NodeBuilder' . DIRECTORY_SEPARATOR . md5('VNP_MERGED_PROFILE_1906') . '_merge');
define('GLOBAL_VIEWS_PATH', APPLICATION_PATH . 'views' . DIRECTORY_SEPARATOR);
define('RECOMPILE_ROUTE', true);
define('RECOMPILE_CONFIG_FORM', true);

$AppConfig['secure']['sitekey']     = 's$xcv#245dijse4hb4#$5zbc';
$AppConfig['DB']['main']['host']    = 'localhost';
$AppConfig['DB']['main']['name']    = 'lovebook'; //	vanthanh1_uni
$AppConfig['DB']['main']['user']    = 'root';  //vanthanh1_uni
$AppConfig['DB']['main']['pass']    = ''; //CtdFAC1GlY
$AppConfig['DB']['main']['type']    = 'mysqli';
$AppConfig['DB']['main']['prefix']  = '';

$AppConfig['remote_login_services']  = array(
    'facebook' => array(
        'key' => '262927320745197',
        'secret' => '6b9b571ec9c941592362876d1729faa3'
    ),
    'google' => array(
        'key' => '446959693254-cicsfjcvgh3upe26uuj8fmdgscvda837o2.apps.gosogleusercontent.com',
        'secret' => '-8cu4TGcXw7ibwsMU1fbjnrjhjgr'
    )
);


$AppConfig['lang_default']      = 'vi';
$AppConfig['lang_support']['vi']['code'] = 'vi';
$AppConfig['lang_support']['vi']['name'] = 'Việt Nam';
$AppConfig['lang_support']['vi']['icon'] = 'vn.gif';

$AppConfig['lang_support']['en']['code'] = 'en';
$AppConfig['lang_support']['en']['name'] = 'English';
$AppConfig['lang_support']['en']['icon'] = 'en.gif';

//Do not change this

$AppConfig['ThemeConfig'] = array(	'theme_root'		=> DATA_PATH . 'theme' . DIRECTORY_SEPARATOR,
                                    'default_theme'		=> 'vn-travels',
                                    'default_layout'	=> 'left.body'
);

?>