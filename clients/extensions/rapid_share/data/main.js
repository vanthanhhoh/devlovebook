$(document).ready(function() {
    var nb_share_lib = new netbit_social_share(share_data)
    nb_share_lib.nb_fb_social(share_data.share_services.fb_app_id);
});
function netbit_social_share(obj) {
    var nb_share_lib = this;
    nb_share_lib.share_status = {
        facebook: {
            group   : [],
            page    : [],
            wall    : []
        },
        googleplus: {
        }
    };
    nb_share_lib.share_delay_time = {
        facebook: {
            group   : 1000,
            page    : 1000,
            wall    : 10000
        }
    };
    nb_share_lib.action_box = VNP.SugarBox;
    //var fb_page_ids = obj.share_services.fb_my_pages.split(',');
    //fb_page_ids.push(obj.share_services.fb_other_pages);
    var fb_page_ids = obj.share_services.fb_other_pages.split(',');

    nb_share_lib.nb_fb_social = function(fb_app_id) {
        var fb_sdk = new nb_facebook_sdk(fb_app_id, nb_share_lib.get_fb_pages);
        fb_sdk.init_fbjssdk();
    }
    nb_share_lib.get_fb_pages = function(fb_sdk) {
        fb_sdk.get_pages(fb_page_ids, nb_share_lib.get_fb_groups);
    }
    nb_share_lib.get_fb_groups = function(fb_sdk, my_pages, fb_pages) {
        fb_sdk.get_groups(my_pages, fb_pages, nb_share_lib.share_action);
    }
    nb_share_lib.share_action = function(fb_sdk, my_pages, fb_pages, fb_groups) {
        nb_share_lib.nb_share_box_tpl(fb_sdk, my_pages, fb_pages, fb_groups, nb_share_lib.check_share_action);
    }
    nb_share_lib.nb_share_box_tpl = function(fb_sdk, my_pages, fb_pages, fb_groups, callback) {
        var groups_tpl = '';
        var my_pages_tpl = '<h3>Đăng trên fanpage của bạn</h3>';
        var pages_tpl = '';

        $.each(fb_groups.data, function(gi, go) {
            var _checked = (go.administrator == true) ? 'checked="checked"' : '';
            groups_tpl += '\
                <label id="fb-group-' + go.id + '">\
                    <input data-admin="' + go.administrator + '" ' + _checked + ' class="share_groups" value="' + go.id + '" type="checkbox" />\
                    <span>' + go.name + '</span>\
                </label>';
        });

        $.each(my_pages, function(pi, po) {
            my_pages_tpl += '<label id="fb-page-' + pi + '"><input class="share_pages" value="' + pi + '" type="checkbox" /><span>' + po.name + '</span></label>';
        });

        $.each(fb_pages, function(pi, po) {
            if(po.can_post == true)
                pages_tpl += '<label id="fb-page-' + pi + '"><input class="share_pages" value="' + pi + '" type="checkbox" /><span>' + po.name + '</span></label>';
        });

        var tpl = '\
        <div class="netbit-share-box clearfix">\
            <div class="share-action-btns clearfix">\
                <button id="netbit-do-share-action">Chia sẻ</button>\
                <button id="netbit-cancel-share-action">Hủy</button>\
            </div>\
            <div class="netbit-share-block clearfix">\
                <div class="groups">\
                    <h3>Chọn nhóm để chia sẻ</h3>\
                    <div class="group-items">\
                        <label><input id="all_groups" value="1" type="checkbox" /><span>Tất cả các nhóm</span></label>' +
                        groups_tpl +
                    '</div>\
                </div>\
                <div class="groups">\
                    <div class="group-items">' +
                        my_pages_tpl +
                        '<label><input id="all_pages" value="1" type="checkbox" /><span>Tất cả fanpage</span></label>\
                        <h3>Chọn fanpage để chia sẻ</h3>\
                        <label class="post-to-wall" id="fb-wall-0">\
                            <input id="post_to_wall" value="1" type="checkbox" checked="checked" /><span>Đồng thời đăng lên tường</span>\
                        </label>' +
                        pages_tpl +
                    '</div>\
                </div>\
            </div>\
        </div>';

        nb_share_lib.action_box.BoxWidth = $(window).width()*0.9;
        nb_share_lib.action_box.BoxHeight = $(window).height()*0.9;
        nb_share_lib.action_box.Open({
            title:'Chia sẻ mạng xã hội ' + obj.node_info.name,
            content: tpl,
            footer:''
        });
        if(typeof callback == 'function') callback(fb_sdk, my_pages, fb_pages, fb_groups);
    }
    nb_share_lib.check_share_action = function(fb_sdk, my_pages, fb_pages, fb_groups) {
        $(document).on('click', '#netbit-do-share-action', function(e) {
            e.preventDefault();

            var fb_groups_to_share = [];
            var fb_pages_to_share = [];
            var fb_post_to_wall = false;
            if($('.netbit-share-box #all_groups:checked').length == 1) {
                $.each(fb_groups.data, function(gi, go) {
                    fb_groups_to_share.push(go.id);
                });
            }
            else {
                $.each($('.netbit-share-box .share_groups:checked'), function(gi, go) {
                    fb_groups_to_share.push($(this).val());
                })
            }

            if($('.netbit-share-box #post_to_wall:checked').length == 1) fb_post_to_wall = true;

            if($('.netbit-share-box #all_pages:checked').length == 1) {
                /**
                $.each(fb_pages, function(fi, fo) {
                    fb_pages_to_share.push(fi);
                });
                 **/
                $.each($('.netbit-share-box .share_pages'), function(gi, go) {
                    fb_pages_to_share.push($(this).val());
                });
            }
            else {
                $.each($('.netbit-share-box .share_pages:checked'), function(gi, go) {
                    fb_pages_to_share.push($(this).val());
                });
            }
            nb_share_lib.do_fb_share(fb_sdk, fb_groups_to_share, my_pages, fb_pages_to_share, fb_post_to_wall);
        });
    }

    nb_share_lib.do_fb_share = function(fb_sdk, fb_groups_to_share, my_pages, fb_pages_to_share, fb_post_to_wall) {
        VNP.Loader.show();
        VNP.Loader.TextNotify('Đang chia sẻ bài viêt', 'success', 500000);
        var self_share = this;
        var share_group_i = 0;
        var share_my_page_i = 0;
        var share_page_i = 0;

        self_share.wall_post_status = function(fb_sdk, feed_stt, obj_type_id, type, node_data) {
            self_share.fb_share_status(feed_stt, obj_type_id, type, node_data);
            self_share.fb_share_pages_groups(fb_sdk, fb_groups_to_share, my_pages, fb_pages_to_share);
        }

        if(fb_post_to_wall) {
            fb_sdk.post_to_wall(obj.node_info, self_share.wall_post_status);
        }
        else
            self_share.fb_share_pages_groups(fb_groups_to_share, my_pages, fb_pages_to_share);

        self_share.fb_share_pages_groups = function(fb_sdk, fb_groups_to_share, my_pages, fb_pages_to_share) {
            self_share.share_report();
            var fb_group_share_interval = setInterval(function() {
                if(share_group_i < fb_groups_to_share.length)
                    fb_sdk.post_to_group(fb_groups_to_share[share_group_i], obj.node_info, self_share.fb_share_status);
                else {
                    clearInterval(fb_group_share_interval);
                    var my_pages_interval = setInterval(function() {
                        console.log(my_pages);
                        console.log(fb_pages_to_share);
                        if(share_my_page_i < my_pages.length) {
                            console.log('ok');
                            if($.inArray(my_pages[share_my_page_i].id, fb_pages_to_share) > -1) {
                                var i = fb_pages_to_share.indexOf(my_pages[share_my_page_i].id);
                                fb_pages_to_share.splice(i, 1);
                                fb_sdk.post_to_page(my_pages[share_my_page_i].id, my_pages, obj.node_info, obj.share_services, self_share.fb_share_status);
                            }
                        }
                        else {
                            clearInterval(my_pages_interval);
                            var fb_page_share_interval = setInterval(function() {
                                if(share_page_i < fb_pages_to_share.length)
                                    fb_sdk.post_to_page(fb_pages_to_share[share_page_i], my_pages, obj.node_info, obj.share_services, self_share.fb_share_status);
                                else {
                                    clearInterval(fb_page_share_interval);
                                    self_share.share_report();
                                }
                                share_page_i++;
                            }, nb_share_lib.share_delay_time.facebook.page);
                        }
                        share_my_page_i++;
                    }, nb_share_lib.share_delay_time.facebook.page);
                }
                share_group_i++;
            }, nb_share_lib.share_delay_time.facebook.group);
        }

        self_share.fb_share_status = function(feed_stt, obj_type_id, type, node_data, stop) {
            if(typeof feed_stt.error != 'undefined')  {
                var _stt_class = 'error';
            }
            else {
                nb_share_lib.share_status.facebook[type].push(feed_stt.id);
                var _stt_class = 'success';
            }
            $('#fb-' + type + '-' + obj_type_id).addClass(_stt_class);
            if(typeof stop != 'undefined' && stop == true) self_share.share_report();
        }
        self_share.share_report = function() {
            console.log(nb_share_lib.share_status);
            VNP.Loader.hide();
        }
    }
}

/***** start facebook share functions *****/
function nb_facebook_sdk(_fb_app_id, ansync_callback) {
    var self_fb_sdk = this;
    self_fb_sdk.init_fbjssdk = function() {
        window.fbAsyncInit = function() {
            FB.init({
                appId      : _fb_app_id,
                xfbml      : true,
                version    : 'v2.3'
            });
            if(typeof ansync_callback == 'function') ansync_callback(self_fb_sdk);
        };
        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = '//connect.facebook.net/en_US/sdk.js';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }

    self_fb_sdk.get_pages = function(page_ids, callback) {
        FB.login(function () {
            FB.api('/me/accounts?limit=100000', function(fb_my_pages) {
                var my_pages = {};
                $.each(fb_my_pages.data, function(i, p) {
                    if($.inArray('CREATE_CONTENT', p.perms) > -1)
                        my_pages[p.id] = p;
                    else page_ids.push(p.id);
                });
                FB.api('/?ids=' + page_ids, function(fb_pages) {
                    if(typeof callback == 'function') callback(self_fb_sdk, my_pages, fb_pages);
                });
            });
        }, {scope: 'publish_stream,manage_pages,status_update,user_status,publish_pages,user_posts,user_status,read_stream,publish_actions,user_groups,friends_groups', auth_type: 'rerequest'});
    }

    self_fb_sdk.check_my_page = function(page_id, my_pages) {
        var _check = false;
        $.each(my_pages, function(pi, po) {
            if(page_id == pi) _check = true;
        });
        return _check;
    };

    self_fb_sdk.get_groups = function(my_pages, fb_pages, callback) {
        FB.api('/me/groups?limit=100000', function(fb_groups) {
            if(typeof callback == 'function') callback(self_fb_sdk, my_pages, fb_pages, fb_groups);
        });
    }

    self_fb_sdk.post_to_group = function(group_id, node_data, callback) {
        FB.api('/' + group_id + '/feed','post', node_data, function(feed_stt) {
            if(typeof callback == 'function') callback(feed_stt, group_id, 'group', node_data, false);
        });
    }

    self_fb_sdk.post_to_page = function(page_id, my_pages, node_data, share_services, callback) {
        if(self_fb_sdk.check_my_page(page_id, my_pages))
            node_data.access_token = my_pages[page_id].access_token;
        FB.api('/' + page_id + '/feed', 'post', node_data, function (feed_stt) {
            if (typeof callback == 'function') callback(feed_stt, page_id, 'page', node_data, false);
        });
    }
    self_fb_sdk.post_to_wall = function(node_data, callback) {
        FB.api('/me', {fields: 'last_name,id'}, function(u) {
            FB.api('/' + u.id + '/feed', 'post', node_data, function (feed_stt) {
                console.log(feed_stt); 
                if (typeof callback == 'function') callback(self_fb_sdk, feed_stt, 0, 'wall', node_data, true);
            });
        });
    }
    return self_fb_sdk;
}