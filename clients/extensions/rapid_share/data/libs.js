//init_fb_jssdk(share_data.share_services.fb_app_id, get_my_groups, select_fb_area_to_post);
//init_fb_jssdk(netBitRapidShareFbActions);
var netbit_share_box;

function select_fb_area_to_post(groups) {
    console.log(groups);
    var share_box = VNP.SugarBox;
    share_box.BoxWidth = $(window).width()*0.9;
    share_box.BoxHeight = $(window).height()*0.88;
    var BoxObject = {title:'Chia sẻ mạng xã hội', content: '', footer:''};

    BoxObject.content = '\
    <div class="netbit-share-box clearfix">\
        <div class="share-action-btns clearfix">\
            <button id="netbit-do-share-action">Chia sẻ</button>\
            <button id="netbit-cancel-share-action">Hủy</button>\
        </div>\
        <div class="netbit-share-block clearfix">\
            <div class="groups">\
                <h3>Chọn nhóm để chia sẻ</h3>\
                <div class="group-items">\
                    <label><input id="all_groups" value="1" type="checkbox" /><span>Tất cả các nhóm</span></label>';
    $.each(groups.data, function(gi, go) {
        var _checked = (go.administrator == true) ? 'checked="checked"' : '';
        BoxObject.content += '\
        <label><input data-admin="' + go.administrator + '" ' + _checked + ' class="share_groups" value="' + go.id + '" type="checkbox" /><span>' + go.name + '</span></label>';
    });
    BoxObject.content += '\
    </div></div>';

    netbit_share_box = share_box;

    var fanpage_ids = share_data.share_services.fb_my_pages + ',' + share_data.share_services.fb_other_pages;

    FB.api('/?ids=' + fanpage_ids, function(pages) {
        console.log(pages);
        BoxObject.content += '\
        <div class="groups">\
            <h3>Chọn fanpage để chia sẻ</h3>\
            <div class="group-items">\
                <label class="post-to-wall"><input id="post_to_wall" value="1" type="checkbox" checked="checked" /><span>Đồng thời đăng lên tường</span></label>\
                <label><input id="all_pages" value="1" type="checkbox" /><span>Tất cả fanpage</span></label>';
        $.each(pages, function(pi, po) {
            if(po.can_post == true)
                BoxObject.content += '\
                <label><input class="share_pages" value="' + pi + '" type="checkbox" /><span>' + po.name + '</span></label>';
        });
        BoxObject.content += '\
        </div></div></div></div>';
        share_box.Open(BoxObject);
        netbit_detect_share_action();
    });
}

function netbit_detect_share_action() {
    $(document).on('click', '#netbit-do-share-action', function(e) {
        e.preventDefault();
        console.log(FB);
    });
}

function get_my_groups(callback) {
    FB.login(function () {
        FB.api('/me/groups', function(response) {
            callback(response);
        });
    }, {scope: 'publish_actions,user_groups,friends_groups', auth_type: 'rerequest'});
}

function init_fb_jssdk(fb_app_id,callback, callback_callback) {
    window.fbAsyncInit = function() {
        FB.init({
            appId      : fb_app_id,
            xfbml      : true,
            version    : 'v2.3'
        });
        if(typeof callback == 'function') {
            if(typeof callback_callback == 'function') callback(callback_callback);
            else callback();
        }
        //netBitRapidShareFbActions();
    };
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = '//connect.facebook.net/en_US/sdk.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
}

function netBitRapidShareFbActions() {
    FB.getLoginStatus(function(response) {
        if(response.status === 'connected') {
            console.log(response);
            get_share_item_data(post_to_fb_group);
        }
        else {
            FB.login();
        }
    });
}

function get_share_item_data(callback) {
    callback(share_data.node_info);
}

function get_my_name() {
    FB.api('/me', {fields: 'last_name'}, function(response) {
        console.log(response);
    });
}

function post_to_fb_group(node_info) {
    FB.login(function () {
        FB.api('/983362488351975/feed','post', node_info, function(res) {
            console.log(res);
        });
    }, {scope: 'publish_actions,user_groups,friends_groups', auth_type: 'rerequest'});
}