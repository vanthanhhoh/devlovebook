<?php

class rapid_share extends BaseExtension {

    static function about() {
        return 'ok';
    }

    static function share_action() {
        echo json_encode(array('status' => 'OK', 'data' => 'I am from ajax'));
        die();
    }

    public function product_share($data) {
        $share_services = Option::getOptionsOfType('extension', 'rapid_share_services_setting');
        foreach($share_services as $ssk => $ss) {
            $share_services[$ssk] = $ss['value'];
        }
        $fb_app_id = Option::get('fb_app_id');
        Theme::JsHeader('product_id', 'var product_id=' . $data['product_id'] . ';', 'inline');
        Theme::JsHeader('fb-sdk-appId', 'var fb_sdk_appId=' . $fb_app_id . ';', 'inline');
        if(!Filter::startsWith('http', $data['image'])) $data['image'] = APP_DOMAIN . $data['image'];
        clients::register_routers();
        $node_info = array(
            'name'          => $data['title'],
            'picture'       => $data['image'],
            'link'          => APP_DOMAIN . clients::getProductUrl($data),
            'description'   => $data['description'],
            'message'       => strip_tags($data['body'])
        );
        $json_data = array(
            'node_info' => $node_info,
            'share_services' => $share_services
        );
        Theme::JsFooter('share_data', 'var share_data=' . json_encode($json_data) . ';', 'inline');
        Theme::JsFooter('hook_share', APPLICATION_BASE . '/extensions/rapid_share/data/main.js');
    }
    static function install() {
        //Option::set('rapid_share_target', serialize(array()), 'Share target', 'extension', 'rapid_share');
        return array('status' => 'OK');
    }

    static function uninstall() {
        return array('status' => 'OK');
    }

    static function update() {
        return array('status' => 'OK');
    }
}

?>