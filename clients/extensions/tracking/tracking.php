<?php

class tracking extends BaseExtension {
    public function google_analytics() {
        Theme::JsFooter('google-analytics', Option::get('google_analytics'), 'inline');
    }

    static function install() {
        return array('status' => 'OK');
    }

    static function uninstall() {
        return array('status' => 'OK');
    }
}