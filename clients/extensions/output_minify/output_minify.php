<?php

class output_minify extends BaseExtension {
    public function minify($tpl_output = '') {
        $tpl_output = Boot::$tpl_output;
        if(Boot::$ControllerGroup == 'frontend' . DIRECTORY_SEPARATOR) {
            ob_start(array($this, 'sanitize_output'));
            return;
            Boot::Library('simple_html_dom');
            $frontend_js_path = DATA_PATH . 'theme' . DIRECTORY_SEPARATOR . CURRENT_THEME . '/js/frontend.js';
            $frontend_css_path = DATA_PATH . 'theme' . DIRECTORY_SEPARATOR . CURRENT_THEME . '/css/frontend.css';
            $minified_script = '<script type="text/javascript" src="' . APPLICATION_DATA_DIR . 'theme/' . CURRENT_THEME . '/js/frontend.js"></script>';
            $minified_style = '<link rel="stylesheet" type="text/css" href="' . APPLICATION_DATA_DIR . 'theme/' . CURRENT_THEME . '/css/frontend.css" />';
            $init_script = '<script type="text/javascript">var BASE_DIR="' . BASE_DIR . '";var current_route="' . G::$Route['name'] . '";</script>';
            if(!file_exists($frontend_js_path) || !file_exists($frontend_css_path)) {
                //if(1) {
                $merged_script = $merged_style = array();
                $html = new simple_html_dom();
                $html->load($tpl_output);
                $path = rtrim(BASE_PATH, '\\');
                /*** Script ***/
                $scripts = $html->find('script');
                foreach ($scripts as $script) {
                    if (!empty($script->src)) {
                        $script_src = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $path . $script->src);
                        if (file_exists($script_src)) $merged_script[] = File::GetContent($script_src) . ';';
                        $script->outertext = '';
                    }
                    if (!empty($script->innertext)) {
                        if(!preg_match('/var BASE_DIR="[^\"]*";var current_route="[^\"]*";/', $script->innertext, $_m)) {
                            $merged_script[] = $script->innertext;
                        }
                        $script->outertext = '';
                    }
                }
                $merged_script = implode(';', $merged_script);
                /*** Css ***/
                $styles = $html->find('link[type="text/css"]');
                foreach($styles as $style) {
                    if(!empty($style->href)) {
                        $style_src = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $path . $style->href);
                        if (file_exists($style_src)) $merged_style[] = File::GetContent($style_src);
                    }
                    $style->outertext = '';
                }
                $styles = $html->find('style');
                foreach($styles as $style) {
                    if(!empty($style->href)) {
                        $merged_style[] = $style->innertext;
                    }
                    $style->outertext = '';
                }
                File::Create($frontend_js_path, $merged_script);
                File::Create($frontend_css_path, $merged_style);
                $body = $html->find("body", 0);
                $body->outertext = $body->makeup() . $body->innertext . $minified_style . $init_script . $minified_script . '</body>';
                $tpl_output = $html;
            }
            else {
                $tpl_output = preg_replace('/<script\\b[^>]*>(.*?)<\\/script>/is', "", $tpl_output);
                $tpl_output = preg_replace('/<link\\b[^>]+type="text\/css"[^>]+\\/>/is', '', $tpl_output);
                $tpl_output = preg_replace("/<style\\b[^>]*>(.*?)<\\/style>/s", "", $tpl_output);
                $tpl_output = str_replace('</body>', $minified_style . $init_script . $minified_script, $tpl_output);
            }
            Boot::$tpl_output = $tpl_output;
            unset($tpl_output);
            ob_start(array($this, 'sanitize_output'));
        }
    }
    private function sanitize_output($buffer) {
        $search = array(
            '/\>[^\S ]+/s',  // strip whitespaces after tags, except space
            '/[^\S ]+\</s',  // strip whitespaces before tags, except space
            '/(\s)+/s'       // shorten multiple whitespace sequences
        );
        $replace = array(
            '>',
            '<',
            '\\1'
        );
        $buffer = preg_replace($search, $replace, $buffer);
        return $buffer;
    }
    public function action_flush_static_file($params = array()) {
        //n($params);
    }
}