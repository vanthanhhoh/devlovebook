<?php

Router::setRoutePrefixRegExp('(?P<lang>[A-Za-z0-9]{2})');
Router::setRouteSuffixRegExp('(?P<page>page\\' . Output::$Paging['bridge'] . '[0-9]++)');
Router::UnMap('LoginAction');
Router::UnMap('LogoutAction');
Router::Map('LoginAction', '/login', 'User#Login', 'POST', 6);
Router::Map('LogoutAction', '/logout', 'User#Logout', 'GET|POST');
Router::Map('UserRegister', '/register', 'User#register', 'GET|POST');



Router::Map('short_route_captcha', '/captcha_image', 'Access::CaptchaGenerator', 'GET|POST');
Router::Map('short_route_post_rss', '/post_feed', 'NodeBase::getView:post,rss', 'GET|POST', 7);
Router::Map('short_route_post_sitemap', '/post_sitemap.xml', 'NodeBase::getView:post,xml_sitemap', 'GET|POST', 7);
Router::Map('short_route_post_sitemap_type', '/post_sitemap_[a:type].xml', 'NodeBase::getView:post,xml_sitemap_type', 'GET|POST', 7);


Router::Map('Home', '/', 'Home#Main', 'GET|POST');
Router::Map('Cart', '/gio-hang.html', 'Cart#viewCart', 'GET|POST');
Router::Map('Checkout', '/check-out.html', 'CheckOut#Main', 'GET|POST');
Router::Map('CheckInvoice', '/kiem-tra-don-hang.html', 'Invoice#CheckInvoice', 'GET|POST');
Router::Map('SearchMain', '/tim-kiem.html', 'Search#Main', 'GET|POST');
Router::Map('Page', '/page/[alias:page].html', 'Page#Detail', 'GET|POST');
Router::Map('ProductCategory', '/[:category]/', 'Product#Category', 'GET|POST');
Router::Map('ProductAuthor', '/tac-gia/[:author]/', 'Product#Author', 'GET|POST');
Router::Map('ProductDetail', '/[alias:product]-[a:pid].html', 'Product#Detail', 'GET|POST');
Router::Map('ProductReview', '/doc-thu/[alias:product]-[a:pid].html', 'Product#Review', 'GET|POST');

?>