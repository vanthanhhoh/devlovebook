<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="{#APP_DOMAIN}/data/static/main-sitemap.xsl"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <sitemap>
        <loc>{#APP_DOMAIN}{do}echo Router::Generate('short_route_post_sitemap_type', array('type' => 'category')){/do}</loc>
        <lastmod>{do}echo date('Y-m-d\TH:i:s+07:00', time()){/do}</lastmod>
    </sitemap>
    <sitemap>
        <loc>{#APP_DOMAIN}{do}echo Router::Generate('short_route_post_sitemap_type', array('type' => 'tag')){/do}</loc>
        <lastmod>{do}echo date('Y-m-d\TH:i:s+07:00', time()){/do}</lastmod>
    </sitemap>
    {$p = 1}
    {while($p <= $total_pages)}
    <sitemap>
        <loc>{#APP_DOMAIN}/post_sitemap/page-{$p}.xml</loc>
        <lastmod>{do}echo date('Y-m-d\TH:i:s+07:00', time()){/do}</lastmod>
    </sitemap>
    {$p = $p + 1}
    {/while}
</sitemapindex>