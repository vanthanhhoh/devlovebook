<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="{#APP_DOMAIN}/data/static/main-sitemap.xsl"?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    {for $tag in $tags}
    <url>
        <loc>{#APP_DOMAIN}{do}echo Router::Generate('TagDetail', array('tag' => urlencode($tag['title']))){/do}</loc>
        <lastmod>{do}echo date('Y-m-d\TH:i:s+07:00', time()){/do}</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.8</priority>
        {if(!empty($tag['image']))}
            <image:image>
                <image:loc>{#APP_DOMAIN}{$tag.image}</image:loc>
                <image:caption><![CDATA[{$tag.title}]]></image:caption>
            </image:image>
        {/if}
    </url>
    {/for}
</urlset>