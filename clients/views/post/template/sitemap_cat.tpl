<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="{#APP_DOMAIN}/data/static/main-sitemap.xsl"?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    {for $cat in $cats}
    <url>
        <loc>{#APP_DOMAIN}{do}echo clients::getPostCatUrl($cat){/do}</loc>
        <lastmod>{do}echo date('Y-m-d\TH:i:s+07:00', $cat['add_time']){/do}</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.8</priority>
        {if(!empty($cat['image']))}
            <image:image>
                <image:loc>{#APP_DOMAIN}{$cat.image}</image:loc>
                <image:caption><![CDATA[{$cat.title}]]></image:caption>
            </image:image>
        {/if}
    </url>
    {/for}
</urlset>