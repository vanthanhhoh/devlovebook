<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0"
   xmlns:content="http://purl.org/rss/1.0/modules/content/"
   xmlns:wfw="http://wellformedweb.org/CommentAPI/"
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:atom="http://www.w3.org/2005/Atom"
   xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
   xmlns:slash="http://purl.org/rss/1.0/modules/slash/">

    <channel>
        <title>{do}echo Option::get('site_name'){/do}</title>
        <atom:link href="{#APP_DOMAIN}/post_feed" rel="self" type="application/rss+xml" />
        <link>{#APP_DOMAIN}</link>
        <description>{do}echo Option::get('site_description'){/do}</description>
        <lastBuildDate>{do}echo date('D, d M Y H:i:s +0700', time()){/do}</lastBuildDate>
        <language>en-US</language>
        <sy:updatePeriod>hourly</sy:updatePeriod>
        <sy:updateFrequency>1</sy:updateFrequency>
        <!--Theme by Trulyvietnam.net-->
        {for $post in $posts}
            {do}
                $url = APP_DOMAIN . clients::getPostUrl($post);
                $cat = clients::getPostCatById($post['main_catid']);
            {/do}
            <item>
                <title>{$post.title}</title>
                <link>{$url}</link>
                <comments>{$url}#comments</comments>
                <pubDate>{do}echo date('D, d M Y H:i:s +0700', $post['add_time']){/do}</pubDate>
                <dc:creator><![CDATA[{do}echo Option::get('site_name'){/do}]]></dc:creator>
                <category><![CDATA[{$cat.title}]]></category>

                <guid isPermaLink="true">{$url}</guid>
                <description><![CDATA[{$post.description}]]></description>
                <slash:comments>{$post.total_comments}</slash:comments>
            </item>
        {/for}
    </channel>
</rss>