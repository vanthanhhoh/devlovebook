<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="{#APP_DOMAIN}/data/static/main-sitemap.xsl"?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>{#APP_DOMAIN}</loc>
        <changefreq>daily</changefreq>
        <priority>1</priority>
    </url>
    {for $post in $posts}
    <url>
        <loc>{#APP_DOMAIN}{do}echo clients::getPostUrl($post){/do}</loc>
        <lastmod>{do}echo date('Y-m-d\TH:i:s+07:00', $post['add_time']){/do}</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
        <image:image>
            <image:loc>{#APP_DOMAIN}{$post.image}</image:loc>
            <image:caption><![CDATA[{$post.title}]]></image:caption>
        </image:image>
        {do}
            $images = array();
            $doc = new DOMDocument();
            @$doc->loadHTML($post['body']);

            $tags = $doc->getElementsByTagName('img');

            foreach ($tags as $tag) $images[] = APP_DOMAIN . $tag->getAttribute('src');
        {/do}
        {for $img in $images as $i}
            {$j = $i + 1}
            <image:image>
                <image:loc>{$img}</image:loc>
                <image:caption><![CDATA[{$post.title} {$j}]]></image:caption>
            </image:image>
        {/for}
    </url>
    {/for}
</urlset>