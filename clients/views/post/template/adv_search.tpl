<div class="page-header">
    <header>
        <h1 class="page-title">{{lang::search_result_article}}</h1>
    </header>
</div>
<div class="advance-search search-post">
    <form action="{do}echo Router::Generate('Search'){/do}" method="post" id="adv-searchForm">
        <input type="hidden" value="1" name="searchSubmited" />
        <input type="text" name="q" placeholder="{{lang::search_holder}}..." id="searchKey" value="{$keyword}" />
        <select name="category">
            <option value="all">{{lang::all_categories}}</option>
            {for $c in $categories}
                <option value="{$c.url}"{if($search_cat == $c.url)} selected="selected" {/if}>{$c.prefix}{$c.title}</option>
            {/for}
        </select>
        <!--
        <select name="group">
            <option value="all">Tất cả nhóm</option>
            {for $g in $groups}
                <option value="{$g.url}"{if($search_group == $g.url)} selected="selected" {/if}>{$g.prefix}{$g.title}</option>
            {/for}
        </select>
        -->
        <input type="submit" value="{{lang::search_btn}}"  />
    </form>
    {if($keyword != '')}
        {if(!empty($search_cat) && $search_cat != 'all')}
            {do}$cat = clients::getPostCatByUrl($search_cat); $cat_title = $cat['title']{/do}
            <h1 class="search-title">{{lang::search_result_article_found_in_cat:$nums,$keyword, $cat_title}}</h1>
        {else}
            <h1 class="search-title">{{lang::search_result_article_found:$nums,$keyword}}</h1>
        {/if}
    {/if}
    {$searchResult}
</div>