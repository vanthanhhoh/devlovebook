<section class="cat-posts-wrapper post-category clearfix">
    {for $post in $posts as $pid}
        {do}$time = Filter::UnixTimeToDate($post['add_time']);{/do}
        <article class="cat-post cat-item post-{$post.post_id}">
            {if(isset($post['image']))}
                <a class="post-image" href="{$post|clients::getPostUrl}" title="{$post.title}">
                    <img src="{$post.image|Output::GetThumbLink:150,100}" alt="{$post.title}" />
                </a>
            {/if}
            <div class="post-info">
                <header><h4><a href="{$post|clients::getPostUrl}" title="{$post.title}">{$post.title}</a></h4></header>
                <time datetime="{do}echo date('Y-m-d H:i+0700', $post['add_time']){/do}"><i class="fa fa-clock-o"></i> {$time.hour}:{$time.minute} - {$time.date}</time>
                <p>{$post.description|Filter::SubString:180}</p>
            </div>
        </article>
    {/for}
</section>