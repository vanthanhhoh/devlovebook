<?php

if(!function_exists('rpl_searchkey')) {
    function rpl_searchkey($row, $keyword) {
        $row['title'] = Filter::replaceSearchKey($row['title'], $keyword);
        $row['description'] = Filter::replaceSearchKey($row['description'], $keyword);
        $row['body'] = Filter::replaceSearchKey($row['body'], $keyword);
        return $row;
    }
}

class post extends portalView {
    private $searchBy = array('title', 'description', 'body');
    public function search($keyword = '') {
        if(!empty($keyword)) {
            $getPost = DB::Query('post');
            foreach($this->searchBy as $i => $col) {
                $temp = array($col, 'LIKE', $keyword);
                $getPost = call_user_func_array(array($getPost, 'Where'), $temp);
                $getPost = $getPost->_OR();
                $temp = array($col, 'SEARCH', $keyword);
                $getPost = call_user_func_array(array($getPost, 'Where'), $temp);
                if($i < sizeof($this->searchBy) - 1)
                    $getPost = $getPost->_OR();
            }
            $getPost = $getPost->Adapter('rpl_searchkey', $keyword)
                ->Order('add_time', 'DESC')->Limit(5)->Columns('title,description,url,post_id,body,add_time,total_views,main_catid,image')->Get('post_id', Output::Paging());
            $return = array('nums' => 0, 'content' => '');
            if($getPost->status && $getPost->num_rows > 0) {
                $return['content'] = $this->View('post_search_result')
                            ->Assign(
                                array(  'posts' => $getPost->Result,
                                        'nums' => $getPost->total_rows)
                            )
                            ->Output();
                $return['nums'] = $getPost->total_rows;
            }
        }
        return $return;
    }
    private function advSearchTpl() {

    }
    public function advSearch($keyword = '') {
        Theme::SetTitle(lang('search_result_for_keyword') . $keyword . ' | ' . Option::get('site_name'));

        $postCategoryUrls = clients::getPostCatByUrl();
        $postCategories = Filter::BuildLevelList($postCategoryUrls, 'post_category_id', 'parent_id');
        $postGroupUrls = clients::getPostGroupByUrl();
        $groups = Filter::BuildLevelList($postGroupUrls, 'post_group_id', 'parent_id');
        $category = $group = 'all';
        $nums = 0;

        Helper::State(lang('search_btn'), Router::Generate('Search'));

        $searchResult = '';
        if(!empty($keyword)) {
            Helper::State($keyword, Router::GenerateThisRoute());
            if(Input::Post('searchSubmited', 0) == 1) {
                $category = Input::Post('category', 'all');
                $group = Input::Post('group', 'all');
                if($category == 'all' && $group == 'all')
                    header('LOCATION: ' . Router::Generate('SearchNode', array('keyword' => urlencode($keyword))));
                else
                    header('LOCATION: ' . Router::Generate('SearchNodeExtra', array('keyword' => urlencode($keyword), 'extra' => $category)));
            }
            $getPost = DB::Query('post');
            if(isset(G::$Route['params']['extra'])) {
                $category = G::$Route['params']['extra'];
            }

            if ($category != 'all' && isset($postCategoryUrls[$category]['post_category_id'])) {
                Theme::SetTitle(lang('search_result_for_keyword') . $keyword . ' in category ' . $postCategoryUrls[$category]['title'] . ' | ' . Option::get('site_name'));
                $getPost = $getPost->WhereGroupOpen()->Where('main_catid', '=', $postCategoryUrls[$category]['post_category_id'])->_OR()->Where('sub_cats', 'INCLUDE', (string)$category)->WhereGroupClose()->_AND();
            }

            if ($group != 'all' && isset($postGroupUrls[$group]['post_group_id']))
                $getPost = $getPost->Where('groups', 'INCLUDE', $postGroupUrls[$group]['post_group_id'])->_AND();

            $getPost = $getPost->WhereGroupOpen();
            foreach ($this->searchBy as $i => $col) {
                $temp = array($col, 'LIKE', $keyword);
                $getPost = call_user_func_array(array($getPost, 'Where'), $temp);
                $getPost = $getPost->_OR();
                $temp = array($col, 'SEARCH', $keyword);
                $getPost = call_user_func_array(array($getPost, 'Where'), $temp);
                if ($i < sizeof($this->searchBy) - 1) $getPost = $getPost->_OR();
            }
            $getPost = $getPost->WhereGroupClose()->_AND()
                ->Where('status', '=', 1)->_AND()
                ->Where('schedule', '<=', CURRENT_TIME)->_AND()
                ->WhereGroupOpen()
                    ->Where('exprired', '=', 0)->_OR()
                    ->Where('exprired', '>', CURRENT_TIME)
                ->WhereGroupClose();
            $getPost = $getPost->Adapter('rpl_searchkey', $keyword)->Limit(10)->Get('post_id', Output::Paging());
            if ($getPost->status && $getPost->num_rows > 0) {
                $nums = $getPost->total_rows;
                $searchResult = $this->View('post_search_result')->Assign(array('posts' => $getPost->Result, 'nums' => $nums))->Output();
            }
        }
       // Theme::SetTitle('Tìm kiếm bài viết ' . $keyword . ' | ' . Option::get('site_name'));
        return $this->View('adv_search')
                ->Assign(
                    array(
                        'categories' => $postCategories,
                        'groups' => $groups,
                        'keyword' => $keyword,
                        'search_cat' => $category,
                        'search_group' => $group,
                        'searchResult' => $searchResult,
                        'nums' => $nums
                    )
                )->Output();
    }

    public function listItemByIds($IDs = array()) {
        $getPosts = DB::Query('post')
            ->Columns('title,groups,tags,description,post_id,image,url,main_catid,add_time,total_views')
            ->Order('priority', 'DESC')->OrderNext('add_time', 'DESC')->Limit(16)
            ->Where('post_id', 'IN', implode(',', $IDs))->_AND()
            ->Where('status', '=', 1)->_AND()
            ->Where('schedule', '<=', CURRENT_TIME)->_AND()
            ->WhereGroupOpen()
                ->Where('exprired', '=', 0)->_OR()
                ->Where('exprired', '>', CURRENT_TIME)
            ->WhereGroupClose()
            ->Get('post_id');
        if($getPosts->status) {
            return $this->View('post_list')
                    ->Assign('posts', $getPosts->Result)
                    ->Output();
        }
        return '';
    }

    public function rss() {
        $days = 0;
        $get_time = CURRENT_TIME - $days*86400;
        $rss_posts = DB::Query('post')
            ->Where('schedule', '<=', CURRENT_TIME)->_AND()
            ->WhereGroupOpen()
                ->Where('exprired', '=', 0)->_OR()
                ->Where('exprired', '>', CURRENT_TIME)
            ->WhereGroupClose()->_AND()
            ->Where('add_time', '<', $get_time)
            ->Order('post_id', 'DESC')
            ->Limit(10)
            ->Get('post_id');

        header('Content-type: application/xml');
        $this->View('rss_post')
            ->Assign('posts', $rss_posts->Result)
            ->Output(false);

        die();
    }

    public function xml_sitemap() {
        $page = Output::$Paging['current_page'];
        $post_per_map = 1000;

        $sitemap_content = '';
        if($page == 0) {
            $sitemap_posts = DB::Query('post')
                ->Where('schedule', '<=', CURRENT_TIME)->_AND()
                ->WhereGroupOpen()
                    ->Where('exprired', '=', 0)->_OR()
                    ->Where('exprired', '>', CURRENT_TIME)
                ->Limit($post_per_map)
                ->WhereGroupClose()
                ->Get('post_id', Output::Paging());

            $total_page = Output::$Paging['total_pages'];

            $sitemap_content = $this->View('sitemap_index')
                ->Assign('total_pages', $total_page)
                ->Output();
        }
        else {
            $sitemap_posts = DB::Query('post')
                ->Where('schedule', '<=', CURRENT_TIME)->_AND()
                ->WhereGroupOpen()
                    ->Where('exprired', '=', 0)->_OR()
                    ->Where('exprired', '>', CURRENT_TIME)
                ->Limit($post_per_map)
                ->WhereGroupClose()
                ->Columns('post_id,url,main_catid,add_time,image,title,body')
                ->Order('post_id', 'ASC')
                ->Get('post_id', Output::Paging());

            $sitemap_content = $this->View('sitemap_post')
                ->Assign('posts', $sitemap_posts->Result)
                ->Output();
        }

        header('Content-type: application/xml');
        echo $sitemap_content;
        die();
    }

    public function xml_sitemap_type() {
        if(isset(G::$Route['params']['type']) && in_array(G::$Route['params']['type'], array('category','tag','page'))) {
            $sitemap_content = '';
            $sitemap_type = G::$Route['params']['type'];

            if($sitemap_type == 'category') {
                $cats = clients::getPostCatByID();
                $sitemap_content = $this->View('sitemap_cat')
                    ->Assign('cats', $cats)
                    ->Output();
            }
            elseif($sitemap_type == 'tag') {
                $tags = DB::Query('global_tags')->Order('tid', 'ASC')->Limit(10000)->Where('tid', '>', 0)->Get()->Result;
                $sitemap_content = $this->View('sitemap_tag')
                    ->Assign('tags', $tags)
                    ->Output();
            }

            header('Content-type: application/xml');
            echo $sitemap_content;
        }
        die();
    }
}