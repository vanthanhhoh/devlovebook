<?php
date_default_timezone_set('Asia/Bangkok');

define('MIN_PRODUCT_PRICE', 10000);
define('MAX_PRODUCT_PRICE', 50000000);
class clients {
    static $NodeProfile = array();
    static $NodeMapper = array();
    static $ControllerInfo = array();
    static function init() {
        if(!defined('ADMIN_AREA') || ADMIN_AREA != 1)
            self::register_routers();
        if(file_exists(APPLICATION_PATH . 'node_mapper.php')) {
            include APPLICATION_PATH . 'node_mapper.php';
            if(isset($NodeProfile)) self::$NodeProfile = $NodeProfile;
            if(isset($NodeMapper)) self::$NodeMapper = $NodeMapper;
        }
    }
    static function register_routers() {
        if(file_exists(APPLICATION_PATH . 'routes.php')) include_once(APPLICATION_PATH . '/routes.php');
    }
    static $shop_payment_methods = array(
        'bank' => array('title' => 'Chuyển khoản'),
        'cod'   => array('title' => 'Thanh toán khi nhận hàng')
    );
    static function getPostUrl($post) {
        if(!isset(G::$Global['post_category']))
            G::$Global['post_category'] = self::getPostCatByID();
        if(!empty($post) && isset($post['post_id']) && isset($post['url'])) {
            $category = G::$Global['post_category'][$post['main_catid']]['url'];

            include APPLICATION_PATH . 'node_mapper.php';
            $postHashID = Crypt::EncryptHashID($post['post_id'], $NodeProfile['post']['code']);
            return Router::Generate(
                'PostDetail',
                array(
                    'category' => $category,
                    'post' => $post['url'],
                    'pid' => $postHashID
                )
            );
        }
    }

    static function getPostCatUrl($cat) {
        return Router::Generate(
            'PostCategory',
            array(
                'category' => $cat['url']
            )
        );
    }

    static function getPostByCat($cat, $limit = 16, $paging = false, $order_by = 'product_id') {
        $subCatIds = array_keys(clients::getPostCatsOfParents($cat['post_category_id']));
        $getPosts = DB::Query('post')
            ->Columns('title,post_id,image,url,main_catid,description,add_time,total_comments')
            ->Order('post_id', 'DESC')->Limit($limit)
            ->WhereGroupOpen()
                ->Where('main_catid', '=', $cat['post_category_id'])->_OR()
                ->Where('sub_cats', 'INCLUDE', (string)$cat['post_category_id'])->_OR()
                ->Where('main_catid', 'IN', implode(',', $subCatIds))
            ->WhereGroupClose()->_AND()
            ->Where('status', '=', 1)->_AND()
            ->Where('schedule', '<=', CURRENT_TIME)->_AND()
            ->WhereGroupOpen()
                ->Where('exprired', '=', 0)->_OR()
                ->Where('exprired', '>', CURRENT_TIME)
            ->WhereGroupClose();
        if($paging) $getPosts = $getPosts->Get($order_by, Output::Paging());
        else $getPosts = $getPosts->Get($order_by);
        return $getPosts;
    }

    static function getPostByMainCat($cat, $limit = 16, $paging = false, $order_by = 'product_id') {
        $subCatIds = array_keys(clients::getPostCatsOfParents($cat['post_category_id']));
        $getPosts = DB::Query('post')
            ->Columns('title,post_id,image,url,main_catid,description,add_time,total_comments')
            ->Order('post_id', 'DESC')->Limit($limit)
            ->Where('main_catid', '=', $cat['post_category_id'])->_AND()
            ->Where('status', '=', 1)->_AND()
            ->Where('schedule', '<=', CURRENT_TIME)->_AND()
            ->WhereGroupOpen()
                ->Where('exprired', '=', 0)->_OR()
                ->Where('exprired', '>', CURRENT_TIME)
            ->WhereGroupClose();
        if($paging) $getPosts = $getPosts->Get($order_by, Output::Paging());
        else $getPosts = $getPosts->Get($order_by);
        return $getPosts;
    }
    /***** get post categories *****/
    static function getPostCatByID($cid = 0) {
        if(!isset(G::$Global['post_category']))
            G::$Global['post_category'] = NodeBase::getNodes('post_category','title,url,parent_id,desc_display,body,add_time,image,meta_title,meta_description');
        if($cid == 0) return G::$Global['post_category'];
        if(isset(G::$Global['post_category'][$cid]))
            return G::$Global['post_category'][$cid];
        else return false;
    }
    static function getPostCatByUrl($url = '') {
        self::getPostCatByID();
        $cats = array();
        foreach(G::$Global['post_category'] as $cat)
            $cats[$cat['url']] = $cat;
        if($url == '') return $cats;
        if(isset($cats[$url])) return $cats[$url];
        else return false;
    }
    static function getPostCatsOfParents($ids = '') {
        $ids = (string) $ids;
        $ids = array_filter(array_map('trim', explode(',', $ids)));
        self::getPostCatByID();
        $cats = array();
        foreach(G::$Global['post_category'] as $catid => $cat)
            if(in_array($cat['parent_id'], $ids))
                $cats[$catid] = $cat;
        return $cats;
    }
    /***** get post group *****/
    static function getPostGroupByID($gid = 0) {
        if(!isset(G::$Global['post_group']))
            G::$Global['post_group'] = NodeBase::getNodes('post_group','title,url,parent_id,body');
        if($gid == 0) return G::$Global['post_group'];
        if(isset(G::$Global['post_group'][$gid]))
            return G::$Global['post_group'][$gid];
        else return false;
    }
    static function getPostGroupByUrl($url = '') {
        self::getPostGroupByID();
        $groups = array();
        foreach(G::$Global['post_group'] as $group)
            $groups[$group['url']] = $group;
        if($url == '') return $groups;
        if(isset($groups[$url])) return $groups[$url];
        else return false;
    }

    static function getNodefromHashID($hashID, $columns = '') {
        $postHashID = Crypt::DecryptHashID($hashID);
        if(is_array($postHashID) && sizeof($postHashID) == 2) {
            include APPLICATION_PATH . 'node_mapper.php';
            $nodeID = $postHashID[0];
            $nodeCode = str_pad($postHashID[1], 4, '0', STR_PAD_LEFT);

            if(isset($NodeMapper[$nodeCode])) {
                $table = $controller = $NodeMapper[$nodeCode]['controller'];
                if($NodeMapper[$nodeCode]['split'] > 0)
                    $table .= '_' . ( floor($nodeID/$NodeMapper[$nodeCode]['split']) + 1);
                $getNode = DB::Query($table)->Where($controller . '_id', '=', $nodeID);
                if(!empty($columns)) {
                    $columns = array_filter(explode(',', $columns), 'trim');
                    $columns[] = $controller . '_id';
                    $columns = implode(',', $columns);
                    $getNode = $getNode->Columns($columns);
                }
                $getNode = $getNode->Get();
                if($getNode->status && $getNode->num_rows == 1)
                    return $getNode->Result[0];
            }
        }
        return false;
    }

    static function countNodeView($nodeName, $nodeID) {
        $sessKey = 'view_' . $nodeName . '_' . $nodeID;
        $TimeCheck = G::$Session->Get($sessKey);
        if($TimeCheck == NULL || !$TimeCheck) {
            G::$Session->Set($sessKey, CURRENT_TIME);
            $UpdateNode = DB::Query($nodeName)
                ->Where($nodeName . '_id', '=', $nodeID)
                ->Update(array('total_views' => 1), array('total_views' => 'INC'));

            include APPLICATION_PATH . 'node_mapper.php';
            if(isset($NodeProfile[$nodeName]) && $NodeProfile[$nodeName]['split'] > 0) {
                $table = $nodeName . '_' . (floor($nodeID/$NodeProfile[$nodeName]['split']) + 1);
                $UpdateNode = DB::Query($table)
                    ->Where($nodeName . '_id', '=', $nodeID)
                    ->Update(array('total_views' => 1), array('total_views' => 'INC'));
            }
        }
    }
	static function getModelCatByID($cid = 0) {
        if(!isset(G::$Global['model_type']))
            G::$Global['model_type'] = NodeBase::getNodes('model_type','title,url,parent_id');
        if($cid == 0) return G::$Global['model_type'];
        if(isset(G::$Global['model_type'][$cid]))
            return G::$Global['model_type'][$cid];
        else return false;
    }
    static function getModelCatsOfParents($ids = '') {
        $ids = (string) $ids;
        $ids = array_filter(array_map('trim', explode(',', $ids)));
        self::getModelCatByID();
        $cats = array();
        foreach(G::$Global['model_type'] as $catid => $cat)
            if(in_array($cat['parent_id'], $ids))
                $cats[$catid] = $cat;
        return $cats;
    }

    static function GetChildID($array,$id){
        self::Recursive($array,$id,$result);
        $result[] = $id;
        return $result;
    }
    static function GetParentID($array,$parent_id,$id){
        if($parent_id>0){
            $parent_ = $array[$parent_id]['parent_id'];
            $id_ = $array[$parent_id]['product_category_id'];
            self::GetParentID($array,$parent_,$id_);
        }
        else {
            return $array[$id]['product_category_id'];
        }
    }
    static function Recursive($arrData, $parent = 0, &$result) {
        if (count($arrData) > 0) {
            foreach ($arrData as $key => $val) {
                if ($parent == $val['parent_id']) {
                    $result[] = $val['product_category_id'];
                    $_parent = $val['product_category_id'];
                    unset($arrData[$key]);
                    self::Recursive($arrData, $_parent, $result);
                }
            }
        }
    }
}
?>