<?php

class Hook {
    static $hook_points = array(
        'before_boot_init',
            'before_app_init',
            'after_app_init',
                'before_controller_group_init',
                'after_controller_group_init',
                    'before_controller_init',
                        'before_action',
                            'before_action_rendered',
                            'after_action_rendered',
                        'after_action',
                'before_block_init',
                    'before_block_function',
                        'before_block_output',
                        'after_block_output',
                'before_extension_init',
                    'before_extension_action',
                    'after_extension_action',
            'before_template_output',
            'before_template_return',
            'after_template_return',
        'after_boot_init',


    );
    static $system_hook = array();

    static $loaded_extensions = array();

    static $current_ext = '';
    static $current_ext_action = '';

    static function init() {
        self::check_extension_hook();
        self::check_theme_hook();
    }

    static function _extends($hook_points = array()) {
        if(empty($hook_points)) return;
        if(is_array($hook_points)) self::$hook_points = array_merge(self::$hook_points, $hook_points);
        else self::$hook_points[] = $hook_points;
        self::$hook_points = array_unique(self::$hook_points);
        self::init();
    }

    static function load_extension($ext_name = '') {
        if($ext_name == '') return false;
        if(!in_array($ext_name, array_keys(self::$loaded_extensions))) {
            $ext_file = EXTENSION_PATH . $ext_name . DIRECTORY_SEPARATOR . $ext_name . '.php';
            if(file_exists($ext_file)) {
                include $ext_file;
                self::$loaded_extensions[$ext_name] = new $ext_name();
            }
        }

        if(isset(self::$loaded_extensions[$ext_name]) && is_object(self::$loaded_extensions[$ext_name]))
            return self::$loaded_extensions[$ext_name];
        else return false;
    }

    static function check_extension_hook() {
        $exts_inits = glob(EXTENSION_PATH . '*');
        foreach($exts_inits as $ext) {
            if(!file_exists($ext . DIRECTORY_SEPARATOR . 'config.ini')) continue;
            $ext_init = Input::parseInitFile($ext . DIRECTORY_SEPARATOR . 'config.ini', true);
            $ext_init['working_areas'] = array_map('trim', explode(',', $ext_init['working_areas']));
            $working_area = preg_replace('/[^a-zA-Z0-9\_\-\.]/', '', Boot::$ControllerGroup);
            $ext_name = basename($ext);
            if(
                !in_array($working_area, $ext_init['working_areas']) ||
                (
                    isset(Boot::$site_extensions[$ext_name]) &&
                    (
                        Boot::$site_extensions[$ext_name]['is_installed'] == 0 ||
                        Boot::$site_extensions[$ext_name]['is_activated'] == 0
                    )
                )
            )
                continue;
            foreach($ext_init['hooks'] as $ex_hook_action => $hook_point_data) {
                $hook_point = array_keys($hook_point_data);
                $hook_point = $hook_point[0];
                //n($hook_point);
                $hook_point_data = $hook_point_data[$hook_point];
                $hook_controller = isset($hook_point_data['controller']) ? $hook_point_data['controller'] : '*#*';
                $hook_theme = isset($hook_point_data['theme']) ? $hook_point_data['theme'] : '*#*';
                $hook_route = isset($hook_point_data['route']) ? $hook_point_data['route'] : '*|*';
                $hook_block = isset($hook_point_data['block']) ? $hook_point_data['block'] : '*';
                $hook_extension = isset($hook_point_data['extension']) ? $hook_point_data['extension'] : '*#*';
                if(in_array($hook_point, array_keys(self::$hook_points))) {
                    self::$system_hook[$hook_point][basename($ext)][$ex_hook_action] = array(
                        'controller'        => $hook_controller,
                        'theme'             => $hook_theme,
                        'route'             => $hook_route,
                        'block'             => $hook_block,
                        'extension'         => $hook_extension,
                    );
                }
            }
        }
        //n(self::$system_hook);
    }

    static function check_theme_hook() {

    }

    static function on($point_name = '', $params = array()) {
        if(empty($point_name)) return false;
        if(in_array($point_name, self::$hook_points)) {
            if(isset(self::$system_hook[$point_name])) {
                foreach(self::$system_hook[$point_name] as $ext_name => $_hook_data) {
                    foreach($_hook_data as $ext_action => $hook_data) {
                        if ($ext = self::load_extension($ext_name)) {
                            $ext_allow_exec = true;
                            $hook_controller = explode('#', $hook_data['controller']);
                            $hook_theme = explode('#', $hook_data['theme']);
                            $hook_route = explode('|', $hook_data['route']);
                            $hook_block = $hook_data['block'];
                            $hook_extension = explode('#', $hook_data['extension']);
                            if (((isset(G::$Controller) && !empty(G::$Controller)) && ($hook_controller[0] != '*' && $hook_controller[0] != G::$Controller)) || ((isset(G::$Action) && !empty(G::$Action)) && isset($hook_controller[1]) && ($hook_controller[1] != '*' && $hook_controller[1] != G::$Action)) || (isset(G::$Profile['route_theme']) && ($hook_theme[0] != '*' && $hook_theme[0] != G::$Profile['route_theme'])) || (isset(G::$Profile['route_layout']) && ($hook_theme[1] != '*' && $hook_theme[1] != G::$Profile['route_layout'])) || (isset(G::$Route['name']) && ($hook_route[0] != '*' && $hook_theme[0] != G::$Profile['route_layout'])) /**
                                 * ||
                                 * (
                                 * isset(G::$Route['name']) && isset($hook_route[1]) &&
                                 * ($hook_route[0] != '*' || $hook_theme[0] != G::$Profile['route_layout'])
                                 * )
                                 * **/ || (!empty(Theme::$CurrentBlock) && ($hook_block != '*' && $hook_block != Theme::$CurrentBlock)) || (!empty(self::$current_ext) && !empty($hook_extension[0]) && ($hook_extension[0] != '*' && $hook_extension[0] != self::$current_ext)) || (isset(self::$current_ext_action) && !empty(self::$current_ext_action) && isset($hook_extension[1]) && !empty($hook_extension[1]) && ($hook_extension[0] != '*' && $hook_extension[0] != self::$current_ext_action))
                            ) $ext_allow_exec = false;
                            if (!$ext_allow_exec) continue;
                            self::$current_ext = $ext;
                            self::$current_ext_action = $ext_action;
                            if (method_exists($ext, $ext_action)) {
                                $ext->Init();
                                Hook::on('before_extension_action');
                                $ext_rt = call_user_func_array(array($ext, $ext_action), $params);
                                Hook::on('after_extension_action');
                                $ext_rt;
                            } else Error::Set('Extension action not found: ' . $ext_name . '::' . $ext_action);
                        }
                        else Error::Set('Extension cannot be loaded: ' . $ext_name);
                    }
                }
            }
        }
    }
}