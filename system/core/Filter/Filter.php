<?php

/**
 * Filter Class
 *
 * Filter output, xss prevent
 *
 * @package		VNP
 * @subpackage	Base libraries
 * @author		VNP Dev team
 * @category	Base layer
 * @link		http://vnphp.com/docs/base-layer/libraries/DB-Wrapper.html
 */

if( !defined('VNP_SYSTEM') && !defined('VNP_APPLICATION') ) die('Access denied!');

class Filter
{
	static function CheckValidEmail($Email) {
		$Regular = '/([a-zA-Z0-9_]+)@([a-zA-Z0-9_]+)\.([a-zA-Z0-9_]+)/';
		if(preg_match($Regular, $Email, $Matches)) return true;
		else return false;
	}

	static function array_unshift_assoc(&$arr, $key, $val)
		{
			$arr = array_reverse($arr, true);
			$arr[$key] = $val;
			return array_reverse($arr, true);
	}
	static function CleanUrlString( $string, $spaceReplace = '-' ) {
		$string = preg_replace('/[\x{0300}\x{0301}\x{0303}\x{0309}\x{0323}]/u', '', $string); // fix unicode consortium for Vietnamese
		$search = array( '&amp;', '&#039;', '&quot;', '&lt;', '&gt;', '&#x005C;', '&#x002F;', '&#40;', '&#41;', '&#42;', '&#91;', '&#93;', '&#33;', '&#x3D;', '&#x23;', '&#x25;', '&#x5E;', '&#x3A;', '&#x7B;', '&#x7D;', '&#x60;', '&#x7E;' );
		$string = preg_replace( array( '/[^a-zA-Z0-9]/', '/[ ]+/', '/^[\-]+/', '/[\-]+$/' ), array( ' ', $spaceReplace, '', '' ), str_replace($search, ' ', self::utf8_EncString($string)));
		return $string;
	}

	static function strtolower($string) {
		include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'lookup.php';
		return strtr($string, $utf8_lookup['strtolower']);
	}

	static function strtoupper($string) {
		include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'lookup.php';
		return strtr($string, $utf8_lookup['strtoupper']);
	}

	static function utf8_EncString( $string ) {
		/*
		if( file_exists('lookup_' . LANG . '.php' ) ) {
			include 'lookup_' . LANG . '.php' ;
			$string = strtr( $string, $utf8_lookup_lang );
		}
		*/
		include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'lookup.php';
		return strtr( $string, $utf8_lookup['romanize'] );
	}

	static function urlTrueForm() {
		$backupUrl = $_SERVER['REQUEST_URI'];
		$thisUrl = preg_replace('/[\$\!\*\'\"\(\)]/', '', html_entity_decode($backupUrl, ENT_QUOTES));
		$thisUrl = str_replace('%22', '', $thisUrl);
		if($thisUrl != $backupUrl) header('Location: ' . $thisUrl);
	}

	static function SubString( $string, $num = 60 ) {
		$string = htmlspecialchars_decode( $string );
		$len = strlen( $string );
		if( $num and $num < $len ) {
			if( ord( substr( $string, $num, 1 ) ) == 32 )
				$string = substr( $string, 0, $num ) . '...';
			elseif( strpos( $string, " " ) === false )
				$string = substr( $string, 0, $num );
			else
				$string = Filter::SubString( $string, $num - 1 );
		}
		return $string;
	}

	static function VariableFilter($FunctionString, $Variable = '') {
		if(empty($Variable)) return '';
		$Functions = array_filter(explode('|', $FunctionString));
		foreach($Functions as $Function) {
			$Function = preg_split('/(\-\>)|(::)/', $Function, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
			$MainFunctions = array();
			$MainFunctions[] = $Function[0];
			foreach($Function as $i => $F) {
				if(!in_array($F, array('->','::'))) {
					$S = explode(':', $F);
					if(!isset($S[1])) {
						$S[1] = array($Variable);
					}
					else {
						$S[1] = array_map('trim',array_filter(explode(',', str_replace('[THIS_FIELD_VALUE]',$Variable,$S[1]))));
					}
					if(isset($Function[$i-1]))
						$MainFunctions[$i] = array('type' => $Function[$i-1], 'function' => $S[0], 'parameters' => $S[1]);
					else $MainFunctions[$i] = array('type' => '', 'function' => $S[0], 'parameters' => $S[1]);
				}
			}
			if(sizeof($MainFunctions) > 1) {
				$MainObj = $MainFunctions[0]['function'];
				array_shift($MainFunctions);
				foreach($MainFunctions as $F)
					$Variable = call_user_func_array(array($MainObj, $F['function']), $F['parameters']);
			}
			else $Variable = call_user_func_array($MainFunctions[0]['function'], $MainFunctions[0]['parameters']);
		}
		return $Variable;
	}

	static function FunctionBuilder($FunctionString, $VariableName) {
		$Functions = array_filter(explode('|', $FunctionString));
		$ReturnFunction = $VariableName;
		foreach($Functions as $Function) {
			if(preg_match('/([^:]+):([^:]+)/', $Function, $M)) {
				$M[2] = explode(',', $M[2]);
				$Ps = array();
				//foreach($M[2] as $P) $Ps[] = "'" . addslashes($P) . "'";
				foreach($M[2] as $P) $Ps[] = addslashes($P);
				$M[2] = implode(',', $Ps);
				$M[2] = str_replace("[THIS_FIELD_VALUE]", $ReturnFunction, $M[2]);
				$ReturnFunction = $M[1] . '(' . $M[2] . ')';
			}
			else $ReturnFunction = $Function . '(' . $ReturnFunction . ')';
		}
		return $ReturnFunction;
	}

	static function DateToUnixTime($DateString, $Hour = 0, $Minute = 0) {
		if(preg_match('/^([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{4})$/', $DateString, $m))
			return mktime( (int)$Hour, (int)$Minute, 0, (int)$m[2], (int)$m[1], (int)$m[3] );
		return CURRENT_TIME;
	}

	static function UnixTimeToDate($UnixTime = 0, $OnlyDate = false) {
		$tdate = date('H|i', $UnixTime);
		list($date['hour'], $date['minute']) = explode('|', $tdate);
		$date['date'] = date('d/m/Y', $UnixTime);
		return $OnlyDate ? $date['date'] : $date;
	}
    static function UnixTimeToFullDate($UnixTime = 0, $OnlyDate = false){
        $tdate = date('H|i', $UnixTime);
        list($date['hour'], $date['minute']) = explode('|', $tdate);
        $date['date'] = date('d/m/Y - H:i', $UnixTime);
        return $OnlyDate ? $date['date'] : $date;
    }

    static function UnixTimeToTextTime($UnixTime = 0, $OnlyDate = false){
        $tdate = date('H|i', $UnixTime);
        list($date['hour'], $date['minute']) = explode('|', $tdate);
        $date['date'] = date('d M', $UnixTime);
        return $OnlyDate ? $date['date'] : $date;
    }
	static function BuildLevelList($Items, $IDKey, $ParentKey = '') {
		if($ParentKey == '') {
            $Result = array();
			foreach($Items as $k => $Item) {
				$Item['prefix'] = $Item['suffix'] = '';
				$Result[] = $Item;
			}
			return $Result;
		}

		$Result = array();
		$BuItems = $Items;

		foreach($Items as $k => $Item) {
			if($Item[$ParentKey] == 0) {
				$Item['prefix'] = $Item['suffix'] = '';
				$Result[] = $Item;
				unset($BuItems[$k]);
				$Result = array_merge($Result, self::BuildSubItems($BuItems, $Item[$IDKey], 0, $IDKey, $ParentKey));
			}
		}
		return $Result;
	}
	static function BuildSubItems($Items, $ID, $Level, $IDKey, $ParentKey) {
		$Result = array();
		$BuItems = $Items;
		foreach($Items as $k => $Item) {
			if($Item[$ParentKey] == $ID && $ID > 0) {
				//$Level++;
				$Item['prefix'] = $Item['suffix'] = str_repeat('&nbsp&nbsp&nbsp&nbsp&nbsp', $Level + 1);
				$Result[] = $Item;
				unset($BuItems[$k]);
				$Result = array_merge($Result, self::BuildSubItems($BuItems, $Item[$IDKey], $Level + 1, $IDKey, $ParentKey));
			}
		}
		return $Result;
	}

    static function startsWith($needle, $haystack) {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
    }

    static function endsWith($needle, $haystack) {
        // search forward starting from end minus needle length characters
        return $needle === "" || strpos($haystack, $needle, strlen($haystack) - strlen($needle)) !== FALSE;
    }

	static function replaceSearchKey($str,$key, $class = 'search-key') {
		$array_keyword = array($key, Filter::utf8_EncString($key));
		$pattern = array();
		foreach( $array_keyword as $k ) $pattern[] = '/(' . preg_quote( $k ) . ')/uis';

		$str = preg_replace( $pattern, '{\\1}', $str);
		$str = str_replace( array( '{', '}' ), array( '<span class="' . $class . '">', '</span>' ), $str);
		return $str;
	}

	static function cleanString($string = '') {
		return strip_tags($string);
	}
    static function NumberFormat($number=0){
        return number_format($number);
    }
}

?>