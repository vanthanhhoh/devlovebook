<?php

/**
 * Router Class
 *
 * Application error handler
 *
 * @package		VNP
 * @subpackage	Base libraries
 * @author		VNP Dev team
 * @category	Base layer
 * @link		http://vnphp.com/docs/base-layer/libraries/Error.html
 */

if( !defined('VNP_SYSTEM') && !defined('VNP_APPLICATION') ) die('Access denied!');

//require SYSTEM_PATH . 'core/Router/AltoRouter.php';
require SYSTEM_PATH . 'core/Router/VNP_Router.php';

class Router
{
	const ROUTES_PROFILE_TABLE = 'route_profile';
	static $Router;
	static $BasePath;
    static $currentRoutePart = array();
	static $UriGetParams = array();

	static function Start($BasePath, $CachePath) {
        self::$BasePath = $BasePath;
		VNP_Router::Init();
		VNP_Router::$CompiledRoutesPath = $CachePath;
		Router::$Router = new VNP_Router();
		Router::$Router->SetBasePath($BasePath);
		return Router::$Router;
	}

	static function Map($Name = NULL, $Route, $Target, $Method = 'GET', $Priority = 0) {
		if($Name == 'page') trigger_error('Route name cannot be "page"');
		return Router::$Router->Map($Name, $Route, $Target, $Method, $Priority);
	}

	static function UnMap($Name) {
		return Router::$Router->UnMap($Name);
	}

	static function AddRule($RuleName, $Rule) {
		VNP_Router::AddRule($RuleName, $Rule);
	}

	static function Match($reComplie = false, $Url = NULL, $Priority = 0) {
		$ajax_methods = array('json','text','state');
		$Routes = Router::$Router->Match($reComplie, $Url);
		$_R = array();
		$_p = -1;
		if(strcmp($Priority, 'all') === 0 || !isset($_R[$Priority])) $matchRoute = $_R;
		else $matchRoute = $_R[$Priority];
		foreach($Routes as $r_name => $Route) {
			if(preg_match('/^Ajax_(.*)/', $Route['name'], $matchs)) {
				//n($Route);
				$Route['name'] = $matchs[1];
				$Route['ajax'] = $Route['params']['Ajax_Mod'];
				unset($Route['params']['Ajax_Mod']);
			}
			elseif(isset($Route['params']['controller']) && $Route['params']['controller'] == 'ajax' && isset($Route['params']['action']) && in_array($Route['params']['action'], $ajax_methods)) {
				if(isset($Route['params']['params']) && !empty($Route['params']['params'])) {
					$param_trans = explode('/', $Route['params']['params']);
					//n($param_trans);
					if(!empty($param_trans[0])) {
						$Route['params']['controller'] = $param_trans[0];
						array_shift($param_trans);
					}
					if(isset($param_trans[0]) && !empty($param_trans[0])) {
						$Route['ajax'] = $Route['params']['action'];
						$Route['params']['action'] = $param_trans[0];
						array_shift($param_trans);
					}
					if(isset($param_trans[0]) && !empty($param_trans[0])) {
						if(is_array($param_trans))
							$Route['params']['params'] = implode('/', $param_trans);
						else
							$Route['params']['params'] = $param_trans;
					}
					else $Route['params']['params'] = '';
				}
			}
			if(isset($Route['params']['action']) && $Route['params']['action'] == 'page') {
				$Route['params']['action'] = 'Main';
				$Route['page'] = $Route['params']['params'];
				unset($Route['params']['params']);
				$Route['name'] = 'Controller';
			}
			elseif(isset($Route['params']['params']) && preg_match('/\/page\/([0-9]+)/', $Route['params']['params'], $matchs)) {
				$Route['page'] = $matchs[1];
				$Route['params'] = str_replace($matchs[0], '', $Route['params']);
			}
			$_R[] = $Route;
			if(empty($Route['params'])) {
				$matchRoute = $Route;
				break;
			}
			if($Route['priority'] > $_p) {
				//n($Route);
				$_p = $Route['priority'];
				$matchRoute = $Route;
			}
		}
		//n($matchRoute);

        $a = Router::$Router->getCurrentRoutePart();
        $a['basePath'] = self::$BasePath;
        Router::$Router->setCurrentRoutePart($a);
        self::$currentRoutePart = $a;
		//n($matchRoute);
        return $matchRoute;
	}
	static function UriGetParams() {
		$getParams = explode('&', Router::$Router->BackupGetParams);
		$rt = array();
		foreach($getParams as $gp) {
			$gp = explode('=', $gp);
			$rt[$gp[0]] = isset($gp[1])? $gp[1] : '';
		}
		self::$UriGetParams = $rt;
		return $rt;
	}
	static function get($name = '', $default = '') {
		if($name == '') return self::$UriGetParams;
		if(isset(self::$UriGetParams[$name])) return self::$UriGetParams[$name];
		else return $default;
	}
	static function GetRoutes($Compiled = false, $Recomplide = false) {
		return Router::$Router->GetRoutes($Compiled, $Recomplide);
	}

	static function Generate($RouteName, array $Params = array(), $WithBase = true) {
        if(defined('IS_AJAX')) $addBase = '/ajax/' . IS_AJAX;
        else $addBase = '';
		return Router::$Router->Generate($RouteName, $Params, $WithBase, $addBase);
	}

	static function GenerateThisRoute($widthBase = true) {
        if(defined('IS_AJAX')) $addBase = '/ajax/' . IS_AJAX;
        else $addBase = '';
		if(!isset(G::$Route['params'])) G::$Route['params'] = array();
		$thisRoute = Router::$Router->Generate(G::$Route['name'], G::$Route['params'], $widthBase, $addBase);
		//if(empty($thisRoute)) $thisRoute = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		if(empty($thisRoute)) $thisRoute = $_SERVER['REQUEST_URI'];
		return $thisRoute;
	}

	static function BasePath() {
		return Router::$Router->GetBasePath();
	}

    static function setRoutePrefixRegExp($prefix = '') {
        Router::$Router->setRoutePrefixRegExp($prefix);
    }

    static function setRouteSuffixRegExp($prefix = '') {
        Router::$Router->setRouteSuffixRegExp($prefix);
    }

    static function getGlobalVars() {
        return Router::$Router->getGlobalVars();
    }

	static function ExtractParams($ParamsString) {
		if(!empty($ParamsString)) {
			$ExtractedParams = array();
			$_ParamsArray = explode('/', $ParamsString);
			//if( sizeof($_ParamsArray) % 2 != 0 ) array_unshift($_ParamsArray, ROUTER_EXTRA_KEY);
			if( sizeof($_ParamsArray) % 2 != 0 ) {
				$LastElement = array_pop($_ParamsArray);
				$_ParamsArray[] = ROUTER_EXTRA_KEY;
				$_ParamsArray[] = $LastElement;
				//array_unshift($_ParamsArray, ROUTER_EXTRA_KEY);
			}
			if(!empty($_ParamsArray)) {
				$KeyIndex = 0;
				while(isset($_ParamsArray[$KeyIndex])) {
					$ExtractedParams[$_ParamsArray[$KeyIndex]] = $_ParamsArray[$KeyIndex+1];
					$KeyIndex += 2;
				}
			}
			return $ExtractedParams;
		}
		else return array();
	}

	static function BuildParamsString($Params, $UseForCurrentRoute = false) {
		$Temp = array();
		foreach($Params as $k => $v) if($v != '') $Temp[] = $k . '/' . $v;
		if($UseForCurrentRoute) {
			G::$Route['name'] = 'ControllerParams';
			if(!isset(G::$Route['params']['action'])) G::$Route['params']['action'] = G::$Registry['ControllerAction'];
			G::$Route['params']['params'] = implode('/', $Temp);
		}
		else return implode('/', $Temp);
	}

	static function EditNode($NodeType, $NodeID) {
		return Router::$Router->Generate('ControllerParams', array('controller' => $NodeType, 'action' => 'AddNode', 'params' => $NodeID));
	}
	static function RemoveNode($NodeType, $NodeID) {
		return Router::$Router->Generate('ControllerParams', array('controller' => $NodeType, 'action' => 'RemoveNode', 'params' => $NodeID));
	}

    static function addProfile($profile) {
        $ap = DB::Query(self::ROUTES_PROFILE_TABLE)->Insert($profile);
        if($ap->status && $ap->insert_id > 0)
            return true;
        else return false;
    }

    static function getAllProfiles() {

        if(!function_exists('unserializeProfileParams')) {
            function unserializeProfileParams($profile) {
                $profile['params'] = unserialize($profile['params']);
                return $profile;
            }
        }
        $profiles = DB::Query(self::ROUTES_PROFILE_TABLE)->Adapter('unserializeProfileParams')->Order('rpid', 'DESC')->Get()->Result;
        return $profiles;
    }

    static function removeProfile($profileName) {
        $dp = DB::Query(self::ROUTES_PROFILE_TABLE)->Where('name', '=', $profileName)->Delete();
        if($dp->status && $dp->affected_rows > 0) return true;
        else return false;
    }

	static function getCurrentProfile() {
		if(isset(G::$Route['params']))
			$serializedParams = serialize(G::$Route['params']);
		else $serializedParams = serialize(array());
		$getProfiles = DB::Query(self::ROUTES_PROFILE_TABLE)
							->Where('route_name', '=', G::$Route['name'])->_OR()
							->WhereGroupOpen()
								->Where('route_name', '=', G::$Route['name'])->_AND()
								->Where('params', '=', $serializedParams)
							->WhereGroupClose()
							->Order('params', 'DESC', 'CHAR_LENGTH')
							->Get();
		if($getProfiles->status && $getProfiles->num_rows == 1)
			$profile = $getProfiles->Result[0];
		elseif($getProfiles->status && $getProfiles->num_rows > 1) {
			foreach($getProfiles->Result as $gp) {
				$profile = $gp;
				if($gp['params'] == $serializedParams) break;
			}
		}
		else {
			$profile['route_name'] = G::$Route['name'];
			$profile['route_theme'] = Option::get('default_theme');
			$profile['route_lang'] = Option::get('default_lang');
			$profile['route_layout'] = Option::get('default_layout');
		}
		if($profile['route_theme'] == 'default')
			$profile['route_theme'] = Option::get('default_theme');
		return $profile;
	}
}

?>
