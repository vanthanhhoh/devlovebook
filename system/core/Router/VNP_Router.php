<?php

class VNP_Router {
	static $Rules = array(
		'i' 	=> '[0-9]++',
		'a' 	=> '[a-zA-Z0-9]++',
		'as' 	=> '[a-zA-Z0-9\_]++',
		//'alias' => '[a-zA-Z0-9\-]++',
		'alias' => '.[^\/]+?',
		'h' 	=> '[a-fA-F0-9]++',
		'*'		=> '.+?',
		'**'	=> '.++',
		''		=> '[^/\.]++'
	);
	static $MatchPattern;
	static $MatchRuleKey;
	static $CompiledRoutesPath;
	static $CompiledRoutesCachedFile;
	public $BackupGetParams = '';
	private $BasePath = '';
    private $prefixRegExp = '';
    private $globalVars = array();
    private $suffixRegExp = '';
	private $Routes = array();
    private $currentRoutePart = array('prefix' => '', 'main' => '', 'suffix' => '', 'ext' => '');
	static function Init() {
		Filter::urlTrueForm();
		self::$MatchPattern = '/\[[' . join('|', array_keys(self::$Rules)) . ']\:([a-zA-Z0-9_\-]++)\]/';
		self::$MatchRuleKey = '/\[([' . join('|', array_keys(self::$Rules)) . '])\:([a-zA-Z0-9_\-]++)\]/';
		self::$CompiledRoutesPath = dirname(__FILE__);
		self::$CompiledRoutesCachedFile = 'routes_' . md5('VNP_CachedRoutes') . '.cache';
	}
	public function SetBasePath($Path = '') {
		$this->BasePath = $Path;
	}
	public function GetBasePath() {
		return $this->BasePath;
	}
    public function setRoutePrefixRegExp($prefixReg = '') {
        $this->prefixRegExp = $prefixReg;
    }
    public function setRouteSuffixRegExp($suffixReg = '') {
        $this->suffixRegExp = $suffixReg;
    }
	static function AddRule($Name, $Rule) {
		self::$Rules[$Name] = $Rule;
	}
	public function Map($Name, $Route, $Target, $Method = 'GET', $Priority = 0) {
		if(isset($this->Routes[$Name])) trigger_error('Route ' . $Name . ' existed!');
		else $this->Routes[$Name] = array(	'name'		=> $Name,
											'route'		=> $Route,
											'target'	=> $Target,
											'method'	=> $Method,
											'priority'	=> $Priority);
	}
	public function UnMap($Name) {
		if(isset($this->Routes[$Name])) unset($this->Routes[$Name]);
	}
	public function Generate($RouteName, $Parameters, $WithBase = true, $addbase = '') {
        $_base = $this->BasePath . $addbase;
		if(isset($this->Routes[$RouteName])) {
			//if($Count = preg_match_all(self::$MatchPattern, $this->Routes[$RouteName]['route'], $Matches)) {
			if($Count = preg_match_all('`\[([a-zA-Z0-9_\-\|\*]*)\:([a-zA-Z0-9_\-]+)\]`', $this->Routes[$RouteName]['route'], $Matches)) {
				$Url = $this->Routes[$RouteName]['route'];
				for($i = 0; $i < $Count; $i++) {
                    if(isset($Parameters[$Matches[2][$i]]) && !empty($Parameters[$Matches[2][$i]]))
                        $Url = str_replace($Matches[0][$i], $Parameters[$Matches[2][$i]], $Url);
                }
				return $WithBase ? $_base . $Url : $Url;
			}
			else return $WithBase ? $_base . $this->Routes[$RouteName]['route'] : $this->Routes[$RouteName]['route'];
		}
		else return $_base;
	}

    private function compileRouteBlock($routes) {
        $CompiledRoutes = array();
        foreach($routes as $Route) {
            $Route['origin'] = $Route['route'];
            if($Count = preg_match_all('`\[([a-zA-Z0-9_\-\|\*]*)\:([a-zA-Z0-9_\-]+)\]`', $Route['route'], $Matches)) {
                $Route['params'] = array();
                for($i = 0; $i < $Count; $i++) {
                    if(!in_array($Matches[1][$i], array_keys(self::$Rules))) {
                        $RuleKey = '*';
                        $Route['var_format'][$Matches[2][$i]] = array_map('trim', explode('|', $Matches[1][$i]));
                    }
                    else $RuleKey = $Matches[1][$i];

                    $Route['params'][$Matches[2][$i]] = $RuleKey;
                    $Route['route'] =
                        str_replace(
                            $Matches[0][$i],
                            '(?P<' . $Matches[2][$i] . '>' . self::$Rules[$RuleKey] . ')',
                            $Route['route']
                        );
                }
            }
            $CompiledRoutes[$Route['name']] = $Route;
        }
        return $CompiledRoutes;
    }
	private function ComplileRoutes($Recompile = false, $keepOriginal = false) {
		$RoutesCachedFile = self::$CompiledRoutesPath . self::$CompiledRoutesCachedFile;
		if(!file_exists($RoutesCachedFile) || $Recompile) {
			$CompiledRoutes = $this->compileRouteBlock($this->Routes);
			File::Create($RoutesCachedFile, serialize($CompiledRoutes));
		}
		else $CompiledRoutes = unserialize(File::GetContent($RoutesCachedFile));
		return $CompiledRoutes;
	}
	public function GetRoutes($Compiled = false, $Recomplide = false) {
		if($Compiled) return $this->ComplileRoutes($Recomplide, $Compiled);
		else return $this->Routes;
	}
    public function getCurrentRoutePart() {
        return $this->currentRoutePart;
    }
    public function setCurrentRoutePart($routePart = array()) {
        $this->currentRoutePart = $routePart;
    }
	public function Match($reCompile = false, $ThisUrl = NULL, $Method = NULL) {
		if($Method == NULL) $Method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD']: 'GET';
		$CompiledRoutes = $this->ComplileRoutes($reCompile);
		if($ThisUrl == NULL) {
			//$ThisUrl = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			$ThisUrl = $_SERVER['REQUEST_URI'];
			$_buUrl = explode('?', $ThisUrl);
			if(isset($_buUrl[1])) $this->BackupGetParams = $_buUrl[1];
			if(($strpos = strpos($ThisUrl, '?')) !== false) {
				$ThisUrl = substr($ThisUrl, 0, $strpos);
			}
			$ThisUrl = substr($ThisUrl, strlen($this->BasePath));
		}

        /*** Get route prefix like 'language', ex: /en/main/url/part ***/
        if(!empty($this->prefixRegExp)) {
            if( preg_match('/^\/' . $this->prefixRegExp . '\//', $ThisUrl, $preMatches) ||
                preg_match('/^\/' . $this->prefixRegExp . '$/', $ThisUrl, $preMatches) ||
                preg_match('/^\/' . $this->prefixRegExp . '\.[A-Za-z0-9]+$/', $ThisUrl, $preMatches)
            ) {
                $this->currentRoutePart['prefix'] = $preMatches[0];
                $ThisUrl = substr($ThisUrl, strlen($preMatches[0]));
                $ThisUrl = '/' . rtrim($ThisUrl, '/');
                foreach($preMatches as $_gk => $_gv) {
                    if(!is_numeric($_gk))
                        $this->globalVars[$_gk] = $_gv;
                }
            }
        }
        /*** End get route prefix ***/

        $routeExt = '';
        if(Filter::endsWith('/', $ThisUrl)) $routeExt = '/';
        elseif(preg_match('/\.[A-Za-z0-9]+$/', $ThisUrl, $extMatches)) {
            $routeExt = $extMatches[0];
            $ThisUrl = substr($ThisUrl, 0, -strlen($routeExt));
        }
        $this->currentRoutePart['ext'] = $routeExt;

        /*** Get route suffix like 'page', ex: main/url/part/page/2 ***/
        if(!empty($this->suffixRegExp)) {

            if( preg_match('/' . $this->suffixRegExp . '\/$/', $ThisUrl, $sufMatches) ||
                preg_match('/' . $this->suffixRegExp . '$/', $ThisUrl, $sufMatches) ||
                preg_match('/' . $this->suffixRegExp . '\.[A-Za-z0-9]+$/', $ThisUrl, $sufMatches)
            ) {
                $this->currentRoutePart['suf'] = $sufMatches[0];
                $ThisUrl = substr($ThisUrl, 0, -strlen($sufMatches[0]));
                $ThisUrl = rtrim($ThisUrl, '/');
                foreach($sufMatches as $_gk => $_gv) {
                    if(!is_numeric($_gk))
                        $this->globalVars[$_gk] = $_gv;
                }
            }
            else {
                $ThisUrl = rtrim($ThisUrl, '/');
            }
        }
        $this->currentRoutePart['main'] = $ThisUrl;
        $ThisUrl .= $routeExt;

		$MatchedRoutes = array();
		foreach($CompiledRoutes as $Route) {
			$MethodMatch = false;
			$RouteMethods = array_map('trim', explode('|', $Route['method']));
			foreach($RouteMethods as $RM) {
				if(strcasecmp($Method, $RM) === 0) {
					$MethodMatch = true;
					break;
				}
			}
			if(!$MethodMatch) continue;

			if($Count = preg_match_all('`^' . $Route['route'] . '$`', $ThisUrl, $Matches, PREG_SET_ORDER)) {
				$MatchRoute = true;
				foreach($Matches[0] as $MatchKey => $MatchVar) {
					if(is_numeric($MatchKey)) {
						unset($Matches[0][$MatchKey]);
						continue;
					}
					if(isset($Route['var_format'][$MatchKey]) && !in_array($MatchVar, $Route['var_format'][$MatchKey])) {
						$MatchRoute = false;
						break;
					}
				}
				$total_vars = sizeof($Matches);
				if($MatchRoute) {
					$MatchRoute = array('name'		=> $Route['name'],
										'target'	=> $Route['target'],
										'method'	=> $Route['method'],
										'priority'	=> $Route['priority'],
										'total_var'	=> $total_vars,
										'params'	=> $Matches[0]
									);
					$MatchedRoutes[] = 	$MatchRoute;
				}
			}
		}
		if(!function_exists('VNP_Router_SortMatchedRules')) {
			function VNP_Router_SortMatchedRules($Route1, $Route2) {
				return strcmp($Route1['priority'],$Route2['priority']);
			}
		}
		if(empty($MatchedRoutes)) $MatchedRoutes[] = array('name' => '', 'target' => '');
		usort($MatchedRoutes, 'VNP_Router_SortMatchedRules');
		return $MatchedRoutes;
	}
    public function getGlobalVars() {
        return $this->globalVars;
    }
}

?>