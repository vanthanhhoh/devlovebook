<?php

if( !defined('VNP_SYSTEM') && !defined('VNP_APPLICATION') ) die('Access denied!');

abstract class BaseBlock
{
    private $tplPath = '';
    abstract public function Main($params);
    abstract public function Output();
    public function __construct() {
    }
    public function InitBlock($blockFile) {
        Hook::on('before_block_init');
        $this->tplPath = dirname($blockFile) . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR;
        Hook::on('before_block_function');
    }
    public function View($File, $ReCompile = false, $IsCache = false) {
        Hook::on('before_block_output');
        $view = TPL::File($File, $ReCompile, $IsCache);
        $view->SetDir('TPLFileDir', $this->tplPath);
        Hook::on('after_block_output');
        return $view;
    }
    public function SetTitle($Title) {
        Theme::SetTitle($Title);
    }
    public function UseCssComponents($Components){
        Theme::AddCssComponent($Components);
    }
}

abstract class portalView {
    private $tplPath = '';
    public function __construct() {
    }

    public function InitPortalView($viewPath = '') {
        $this->tplPath = dirname($viewPath) . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR;
    }
    public function View($File, $ReCompile = false, $IsCache = false) {
        $view = TPL::File($File, $ReCompile, $IsCache);
        $view->SetDir('TPLFileDir', $this->tplPath);
        return $view;
    }
    public function SetTitle($Title) {
        Theme::SetTitle($Title);
    }
    public function UseCssComponents($Components){
        Theme::AddCssComponent($Components);
    }
}

?>