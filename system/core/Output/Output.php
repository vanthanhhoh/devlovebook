<?php

/**
 * Output Class
 *
 * Handle output data
 *
 * @package		VNP
 * @subpackage	Base libraries
 * @author		VNP Dev team
 * @category	Base layer
 * @link		http://vnphp.com/docs/base-layer/libraries/Helper.html
 */

if( !defined('VNP_SYSTEM') && !defined('VNP_APPLICATION') ) die('Access denied!');
define('VNP_OUTPUT_CLASS', true);

class Output
{
	static $CurrentPageUrl;
	static $TotalPages;
	static $Paging = array('total_pages' => 0, 'current_page' => 0, 'base_url' => '', 'ext' => '', 'bridge' => '-');

	static function Paging($PageParams = NULL)
	{
		if(!isset(G::$Global['page'])) $CurrentPage = 0;
		else {
			$CurrentPage = G::$Global['page'] - 1;
			//unset(G::$Route['params']['page']);
		}
		Output::$CurrentPageUrl = Router::GenerateThisRoute();
		//Output::$Paging['base_url']		= Output::$CurrentPageUrl;
		Output::$Paging['current_page'] = $CurrentPage;
		return $CurrentPage;
	}
	static function Page()
	{
		//Output::$Paging['base_url'] = rtrim(Output::$Paging['base_url'], '/');
		//Output::$Paging['base_url'] .= '/';
		$PageOutput = array();
		$Page = Output::$Paging;
		for($i = 1; $i <= $Page['total_pages']; $i++)
		{
			if($i == 1) $url = $Page['base_url'] . $Page['ext'];
			else $url = $Page['base_url'] . '/page' . $Page['bridge'] . $i . $Page['ext'];
			if($Page['current_page'] + 1 == $i)
				$PageOutput[] = '<li class="active"><a>' . $i . '</a></li>';
			else
				$PageOutput[] = '<li><a href="' . $url . '">' . $i . '</a></li>';
		}
		if($Page['current_page'] + 1 > 1)
		{
			if($Page['current_page']  == 1)
				$PrevPage = array($Page['base_url'] . $Page['ext'], '');
			else
				$PrevPage = array($Page['base_url'] . '/page' . $Page['bridge'] . $Page['current_page'] . $Page['ext'], '');
		}
		else
			$PrevPage = array('', 'class="disabled"');
		if($Page['current_page'] + 1 < $Page['total_pages'])
			$NextPage = array($Page['base_url'] . '/page' . $Page['bridge'] . intval($Page['current_page']+2) . $Page['ext'], '');
		else
			$NextPage = array('', 'class="disabled"');
		if(!empty($PageOutput) && $Page['total_pages'] > 1)
			$PageString = '
			<ul class="pagination pagination-sm">
				<li ' . $PrevPage[1] . '><a href="' . $PrevPage[0] . '">&laquo;</a></li>' .
				implode(PHP_EOL, $PageOutput) . '
				<li ' . $NextPage[1] . '><a href="' . $NextPage[0] . '">&raquo;</a></li>
			</ul>';
		else $PageString = '';
		return $PageString;
	}

	static function ThumbnailOutput($Params)
	{
		$Params = $Params['params'];
		$ThumbWidth = $Params['Width'];
		$ThumbHeight = $Params['Height'];
		$ImageLocation = SITE_BASE . $Params['ImageLocation'] . '.' . $Params['Ext'];
		define('FILE_CACHE_DIRECTORY', APPLICATION_PATH . 'data/cache/');
		require SYSTEM_PATH . 'core/Output/timthumb.php';
		timthumb::start($ThumbWidth, $ThumbHeight, $ImageLocation);
	}

	static function GetThumbLink($ImageLocation,$Width = 80, $Height = 80)
	{
		return '/Thumbnail/' . $Width . '_' . $Height . $ImageLocation;
	}

    static function GetSubMenus($ParentID, $Menus, $IDKey, $ParentKey) {
        $OutputMenus = array();
        $_Menus = $Menus;

        foreach($Menus as $MenuID => $Menu) {
            if($Menu[$ParentKey] == $ParentID) {
                unset($_Menus[$MenuID]);
                $_SubMenu = self::GetSubMenus($Menu[$IDKey], $Menus, $IDKey, $ParentKey);
                if(!empty($_SubMenu)) $Menu['sub'] = $_SubMenu;
                else $Menu['sub'] = '';
                $OutputMenus[] = $Menu;
            }
        }
        if(!empty($OutputMenus)) {
            $SubMenu = TPL::File('default_submenu')
                ->SetDir('TPLFileDir', TEMPLATE_PATH)
                ->SetDir('CompiledDir', CACHE_PATH . 'compiled' . DIRECTORY_SEPARATOR)
                ->SetDir('CacheDir', CACHE_PATH . 'html' . DIRECTORY_SEPARATOR);
            $SubMenu->Assign('Menus', $OutputMenus);
            return $SubMenu->Output();
        }
        return '';
    }
    static function GetSubMenusDeep($ParentID, $Menus, $IDKey, $ParentKey) {
        $OutputMenus = array();
        $_Menus = $Menus;

        foreach($Menus as $MenuID => $Menu) {
            if($Menu[$ParentKey] == $ParentID) {
                unset($_Menus[$MenuID]);
                $_SubMenu = self::GetSubMenusDeep($Menu[$IDKey], $Menus, $IDKey, $ParentKey);
                if(!empty($_SubMenu)) $Menu['sub'] = $_SubMenu;
                else $Menu['sub'] = '';
                $OutputMenus[] = $Menu;
            }
        }
        if(!empty($OutputMenus)) {
            $SubMenu = TPL::File('default_submenu_deep')
                ->SetDir('TPLFileDir', TEMPLATE_PATH)
                ->SetDir('CompiledDir', CACHE_PATH . 'compiled' . DIRECTORY_SEPARATOR)
                ->SetDir('CacheDir', CACHE_PATH . 'html' . DIRECTORY_SEPARATOR);
            $SubMenu->Assign('Menus', $OutputMenus);
            return $SubMenu->Output();
        }
        return '';
    }

    static function MenuGenerator($Items, $IDKey, $ParentKey = '', $Settings = array()) {
        $_Menus = $Items;
        $OutputMenus = array();
        foreach($Items as $MenuID => $Menu) {
            if($Menu[$ParentKey] == 0) {
                unset($_Menus[$MenuID]);
                $_SubMenu = self::GetSubMenus($Menu[$IDKey], $_Menus, $IDKey, $ParentKey);
                $Menu['sub'] = $_SubMenu;
                if(Router::GenerateThisRoute() == $Menu['url']) $Menu['active'] = ' current'; else $Menu['active'] = '';
                $OutputMenus[] = $Menu;
            }
        }
        return $OutputMenus;
    }

    static function MenuGeneratorDeep($Items, $IDKey, $ParentKey = '', $Settings = array()) {
        $_Menus = $Items;
        $OutputMenus = array();
        foreach($Items as $MenuID => $Menu) {
            if($Menu[$ParentKey] == 0) {
                unset($_Menus[$MenuID]);
                $_SubMenu = self::GetSubMenusDeep($Menu[$IDKey], $_Menus, $IDKey, $ParentKey);
                $Menu['sub'] = $_SubMenu;
                if(Router::GenerateThisRoute() == $Menu['url']) $Menu['active'] = ' current'; else $Menu['active'] = '';
                $OutputMenus[] = $Menu;
            }
        }
        return $OutputMenus;
    }
	static function PriceFormat($Number, $Sep = '.') {
		return number_format($Number, 0, ',', $Sep);
	}

	static function getCaptcha($type = 'image', $echo = true) {
		$cpt_id = 'captcha_' . str_replace('.', '-', microtime(true));
		$cpt = '
		<img id="' . $cpt_id . '" src="' . Router::Generate('short_route_captcha') . '" alt="Captcha" />
        <a href="javascript:d = new Date(); if(typeof cpt === \'undefined\') var cpt = $(\'#' . $cpt_id . '\').attr(\'src\'); $(\'#' . $cpt_id . '\').attr(\'src\', cpt + \'?\' + d.getTime())" title="Refresh captcha">Refresh captcha</a>';
		if($echo) echo $cpt; else return $cpt;
	}

    static function ErrorPage($Code = 404, $Content = '') {
        if($Code == 404) {
            header('HTTP/1.0 404 Not Found');
            Helper::Notify('error', $Content);
        }
        elseif($Code == 403) {
            header('HTTP/1.0 403 Forbidden');
			echo $Content;
			exit();
        }
        else {
            header('HTTP/1.0 ' . $Code . ' ' . $Code);
			echo $Content;
        }
    }
}

?>