<?php

class File
{
    static protected $mimeFromExt = array (
                                        'txt'   => 'text/plain',
                                        'html'  => 'text/html',
                                        'htm'   => 'text/html',
                                        'php'   => 'text/plain',
                                        'css'   => 'text/css',
                                        'js'    => 'application/x-javascript',
                                        'jpg'   => 'image/jpeg',
                                        'jpeg'  => 'image/jpeg',
                                        'gif'   => 'image/gif',
                                        'png'   => 'image/png',
                                        'bmp'   => 'image/bmp',
                                        'tif'   => 'image/tiff',
                                        'tiff'  => 'image/tiff',
                                        'doc'   => 'application/msword',
                                        'docx'  => 'application/msword',
                                        'xls'   => 'application/excel',
                                        'xlsx'  => 'application/excel',
                                        'ppt'   => 'application/powerpoint',
                                        'pptx'  => 'application/powerpoint',
                                        'pdf'   => 'application/pdf',
                                        'wmv'   => 'application/octet-stream',
                                        'mpg'   => 'video/mpeg',
                                        'mov'   => 'video/quicktime',
                                        'mp4'   => 'video/quicktime',
                                        'zip'   => 'application/zip',
                                        'rar'   => 'application/x-rar-compressed',
                                        'dmg'   => 'application/x-apple-diskimage',
                                        'exe'   => 'application/octet-stream'
                                    );
	static function Create($FilePath, $Content = '') {
		if(file_exists(dirname($FilePath))) {
			if(file_put_contents($FilePath, $Content)) return true;
		}
		return false;
	}
	static function UpdateFile($FilePath, $Content = '') {
		if(file_exists(dirname($FilePath))) {
			if(file_put_contents($FilePath, $Content)) return true;
		}
		return false;
	}
	static function CreateDirectory($pathname, $mode = 0755, $recursive = false, $context = NULL) {
		if(!file_exists($pathname)) {
			if($context == NULL)
				mkdir($pathname, $mode, $recursive);
			else mkdir($pathname, $mode, $recursive, $context);
			self::Create($pathname . DIRECTORY_SEPARATOR . 'index.html', 'hihi');
		}
	}
	static function RemoveDirectory($pathname) {
		$dir = opendir($pathname);
	    while(false !== ($file = readdir($dir))) {
	        if(($file != '.' ) && ($file != '..')) {
	            if(is_dir($pathname . DIRECTORY_SEPARATOR . $file) )
	            	self::RemoveDirectory($pathname . DIRECTORY_SEPARATOR . $file);
	            else unlink($pathname . DIRECTORY_SEPARATOR . $file); 
	        }
	    }
	    rmdir($pathname);
	    closedir($dir);
		return false;
	}
	static function GetContent($FilePath) {
		if(file_exists($FilePath)) return file_get_contents($FilePath);
		else throw new Exception('File::GetFileContent() File not found!');
	}
}


?>