<?php

class Lang {

    static $lang_vars = array();
    static function init($lang_file = '') {
        if(file_exists($lang_file)) include $lang_file;
        self::$lang_vars = $global_lang;
    }

    static function get_string($pharse) {
        $func_args = func_get_args();
        if(isset(self::$lang_vars[$func_args[0]])) {
            $func_args[0] = self::$lang_vars[$func_args[0]];
            return call_user_func_array('sprintf', $func_args);
        }
        else return implode('-', $func_args);
    }
}

?>