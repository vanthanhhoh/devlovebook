<?php

/**
 * Input Class
 *
 * Handle User request method
 *
 * @package		VNP
 * @subpackage	Base libraries
 * @author		VNP Dev team
 * @category	Base layer
 * @link		http://vnphp.com/docs/base-layer/libraries/Error.html
 */

if( !defined('VNP_SYSTEM') && !defined('VNP_APPLICATION') ) die('Access denied!');

class Input
{
	static function Post($Name = NULL, $Default = '')
	{
		if( $Name == NULL ) return $_POST;
		if( isset($_POST[$Name]) && !empty($_POST[$Name]) ) return $_POST[$Name];
		else return $Default;
	}

	static function Get($Name = NULL, $Default = '')
	{
		if( $Name == NULL ) return $_GET;
		if( isset($_GET[$Name]) && !empty($_GET[$Name]) ) return $_GET[$Name];
		else return $Default;
	}

    static function parseInitFile($file, $process_sections = false, $scanner_mode = INI_SCANNER_NORMAL) {
        $explode_str = '.';
        $escape_char = "'";
        // load ini file the normal way
        $data = parse_ini_file($file, $process_sections, $scanner_mode);
        if (!$process_sections) {
            $data = array($data);
        }
        foreach ($data as $section_key => $section) {
            // loop inside the section
            if(!is_array($section)) {
                if (strpos($section_key, $explode_str)) {
                    if (substr($section_key, 0, 1) !== $escape_char) {
                        $sub_keys = explode($explode_str, $section_key);
                        $data[$sub_keys[0]][$sub_keys[1]] = $section;
                        unset($data[$section_key]);
                    }
                }
            }
            else {
                foreach ($section as $key => $value) {
                    if (strpos($key, $explode_str)) {
                        if (substr($key, 0, 1) !== $escape_char) {
                            // key has a dot. Explode on it, then parse each subkeys
                            // and set value at the right place thanks to references
                            $sub_keys = explode($explode_str, $key);
                            $subs =& $data[$section_key];
                            foreach ($sub_keys as $sub_key) {
                                if (!isset($subs[$sub_key])) {
                                    $subs[$sub_key] = array();
                                }
                                $subs =& $subs[$sub_key];
                            }
                            // set the value at the right place
                            $subs = $value;
                            // unset the dotted key, we don't need it anymore
                            unset($data[$section_key][$key]);
                        }
                        // we have escaped the key, so we keep dots as they are
                        else {
                            $new_key = trim($key, $escape_char);
                            $data[$section_key][$new_key] = $value;
                            unset($data[$section_key][$key]);
                        }
                    }
                }
            }
        }
        if (!$process_sections) {
            $data = $data[0];
        }
        return $data;
    }
}

?>