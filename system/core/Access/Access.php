<?php

if( !defined('VNP_SYSTEM') && !defined('VNP_APPLICATION') ) die('Access denied!');
require dirname(__FILE__). DIRECTORY_SEPARATOR . 'NoCSRF.php';

class Access
{
	static function CheckUserPermission() {
		$check = true;
		if(G::$ValidWorkingSession == Boot::ADMIN_SESSION && G::$MyData['level'] < Authorized::USER_ADMIN_LEVEL) $check = false;
		if(G::$ValidWorkingSession == Boot::SMOD_SESSION && G::$MyData['level'] < Authorized::USER_SMOD_LEVEL) $check = false;
		if(G::$ValidWorkingSession == Boot::MOD_SESSION && G::$MyData['level'] < Authorized::USER_MOD_LEVEL) $check = false;
		if(G::$ValidWorkingSession == Boot::MEMBER_SESSION && G::$MyData['level'] < Authorized::USER_NORMAL_LEVEL) $check = false;
		if(!$check) {
			header('HTTP/1.0 403 Forbidden');
			echo 'You are forbidden!';
			die();
		}
	}
	static function RequirePermission($Session = Boot::ADMIN_SESSION) {
		G::$ValidWorkingSession = $Session;
	}
	static function GenerateToken($key = 'internal_token') {
		return $token = NoCSRF::generate($key);
	}
	static function CheckToken($key = 'internal_token', $show_error = true) {
		try {
		    // Run CSRF check, on POST data, in exception mode, with a validity of 10 minutes, in one-time mode.
		    NoCSRF::check($key, $_POST, true, 60*10, false );
		    return true;
		}
		catch ( Exception $e ) {
			if($show_error) Error::Set('Invalid token!');
			return false;
		}
	}
	static function Confirm($Name, $Config) {
		Theme::AddCssComponent('Panels,Buttons');
		$DefaultConfig = array(
							'template'	=> TEMPLATE_PATH,
							'method'	=> 'post'
						);
		$Config = array_merge($DefaultConfig, $Config);
		$Config['tokens'][] = array('name' => 'internal_token', 'value' => self::GenerateToken('internal_token'));
		$ConfirmBox = TPL::File('confirm_box')
						->SetDir('TPLFileDir', TEMPLATE_PATH)
						->SetDir('CompiledDir', CACHE_PATH . 'compiled' . DIRECTORY_SEPARATOR)
						->SetDir('CacheDir', CACHE_PATH . 'html' . DIRECTORY_SEPARATOR);
		return $ConfirmBox->Assign(array(	'name'		=> $Name,
											'config'	=> $Config
										)
									)->Output();
	}
	static function CaptchaGenerator() {
		require __DIR__ . DIRECTORY_SEPARATOR . 'vnpcaptcha' . DIRECTORY_SEPARATOR . 'securimage.php';

		$img = new Securimage();

		// You can customize the image by making changes below, some examples are included - remove the "//" to uncomment

		//$img->ttf_file        = './Quiff.ttf';
		//$img->captcha_type    = Securimage::SI_CAPTCHA_MATHEMATIC; // show a simple math problem instead of text
		//$img->case_sensitive  = true;                              // true to use case sensitve codes - not recommended
		//$img->image_height    = 90;                                // height in pixels of the image
		//$img->image_width     = $img->image_height * M_E;          // a good formula for image size based on the height
		//$img->perturbation    = .75;                               // 1.0 = high distortion, higher numbers = more distortion
		//$img->image_bg_color  = new Securimage_Color("#0099CC");   // image background color
		//$img->text_color      = new Securimage_Color("#EAEAEA");   // captcha text color
		//$img->num_lines       = 8;                                 // how many lines to draw over the image
		//$img->line_color      = new Securimage_Color("#0000CC");   // color of lines over the image
		//$img->image_type      = SI_IMAGE_JPEG;                     // render as a jpeg image
		//$img->signature_color = new Securimage_Color(rand(0, 64),
		//                                             rand(64, 128),
		//                                             rand(128, 255));  // random signature color

		// see securimage.php for more options that can be set

		$img->image_height = 60;
		$img->image_width = (int)($img->image_height * 2.875);
		$img->image_signature = 'Netbit.vn';
		$img->signature_color = new Securimage_Color('#ff7e00');


		// set namespace if supplied to script via HTTP GET
		if (!empty($_GET['namespace'])) $img->setNamespace($_GET['namespace']);

		$img->show();  // outputs the image and content headers to the browser
		exit();
	}
	static function CaptchaValidate($captcha) {
		require __DIR__ . DIRECTORY_SEPARATOR . 'vnpcaptcha' . DIRECTORY_SEPARATOR . 'securimage.php';
		$securimage = new Securimage();
		if($securimage->check($captcha) == false)
			return false;
		else return true;
	}
}


?>