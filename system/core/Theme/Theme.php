<?php

/**
 * Theme Class
 *
 * Theme Class
 *
 * @package		VNP
 * @subpackage	Base libraries
 * @author		VNP Dev team
 * @category	Base layer
 * @link		http://vnphp.com/docs/base-layer/libraries/DB-Driver.html
 */

if( !defined('VNP_SYSTEM') && !defined('VNP_APPLICATION') ) die('Access denied!');

class Theme
{
    static $BaseDir;
    static $Theme;
    static $Title;
    static $MetaTags = array();
    static $MetaProperty = array();
    static $Layout;
    static $BodyContent;
    static $CssComponents		= array();
    static $AjaxCssComponents	= array();
    static $BlockBasePath = '';
    static $IncludedBlocks = array();
    static $BlockContents = array();
    static $LayoutContent = '';
    static $AreaContents = array();
    static $CurrentBlock = '';

    static $Hook = array(
        'header'		=> '',
        'footer'		=> '',
        'before_body'	=> '',
        'after_body'	=> ''
    );
    static $JsHeader = array();
    static $JsFooter = array();
    static $AjaxJsHeader = array();
    static $AjaxJsFooter = array();

    static $CssHeader = array();
    static $CssFooter = array();
    static $AjaxCssHeader = array();
    static $AjaxCssFooter = array();

    static $ThemeVariables = array();
    static $Config = array();
    static $Working = array();
    static $FeaturedPanel = array();
    static $AjaxSession = false;

    public function __construct()
    {
    }

    /*static function Assign($Name, $Value = NULL) {
        is_array($Name) ? Theme::$VNP_ThemeVariables = array_merge(Theme::$VNP_ThemeVariables, $Name)
                        : Theme::$VNP_ThemeVariables[$Name] = $Value;
    }*/

    static function Config($ConfigArray = array()) {
        Theme::$Config = array(
            'theme_root'		=> dirname(__FILE__),
            'default_theme'		=> 'vnp',
            'default_layout'	=> 'left.body',
            'theme'				=> '',
            'layout'			=> '',
            're_compile'		=> false,
            'is_cache'			=> ''
        );
        Theme::$Config = array_merge(Theme::$Config, $ConfigArray);
        if(Theme::$Config['theme'] == '') Theme::$Config['theme'] = Theme::$Config['default_theme'];
        if(Theme::$Config['layout'] == '') Theme::$Config['layout'] = Theme::$Config['default_layout'];
        Theme::$Working = Theme::$Config;
        Theme::$Title = 'Nhà sách lovebook';
    }

    static function SetLayout($Layout) {
        Theme::$Config['layout'] = $Layout;
    }
    static function Layout($Layout) {
        Theme::$Config['layout'] = $Layout;
    }
    static function SetTitle($Title) {
        Theme::$Title = $Title;
    }
    static function MetaTag($Name, $Content) {
        Theme::$MetaTags[$Name] = $Content;
    }
    static function MetaProperty($Type, $Content) {
        Theme::$MetaProperty[$Type] = $Content;
    }
    static function ResetCssComponent() {
        Theme::$CssComponents = array();
    }
    static function AddCssComponent($Components = '') {
        if(!empty($Components))
            if(defined('IS_AJAX'))
                Theme::$AjaxCssComponents = array_unique(array_merge(Theme::$AjaxCssComponents, explode(',', $Components)));
            else
                Theme::$CssComponents = array_unique(array_merge(Theme::$CssComponents, explode(',', $Components)));

    }
    static function Assign($variable, $value = NULL) {
        if(is_array($variable)) Theme::$ThemeVariables += $variable;
        else Theme::$ThemeVariables[$variable] = $value;
    }
    static function JsHeader($Name, $JsString, $Type = 'file') {
        if(defined('IS_AJAX'))
            Theme::$AjaxJsHeader[$Name] = array('js_string' => $JsString, 'type' => $Type);
        else
            Theme::$JsHeader[$Name] = array('js_string' => $JsString, 'type' => $Type);
    }
    static function JsFooter($Name, $JsString, $Type = 'file') {
        if(defined('IS_AJAX'))
            Theme::$AjaxJsFooter[$Name] = array('js_string' => $JsString, 'type' => $Type);
        else
            Theme::$JsFooter[$Name] = array('js_string' => $JsString, 'type' => $Type);
    }

    static function CssHeader($Name, $CssString, $Type = 'file') {
        if(defined('IS_AJAX'))
            Theme::$AjaxCssHeader[$Name] = array('css_string' => $CssString, 'type' => $Type);
        else
            Theme::$CssHeader[$Name] = array('css_string' => $CssString, 'type' => $Type);
    }
    static function CssFooter($Name, $CssString, $Type = 'file') {
        if(defined('IS_AJAX'))
            Theme::$AjaxCssFooter[$Name] = array('css_string' => $CssString, 'type' => $Type);
        else
            Theme::$CssFooter[$Name] = array('css_string' => $CssString, 'type' => $Type);
    }
    static function GetJs($Area = 'JsHeader') {
        $Return = array();
        foreach(Theme::$$Area as $Js) {
            if($Js['type'] == 'file')
                $Return[] = "<script type=\"text/javascript\" src=\"" . $Js['js_string'] . "\"></script>";
            else
                $Return[] = "<script type=\"text/javascript\">" . $Js['js_string'] . "</script>";
        }
        return implode(PHP_EOL, $Return) . "\n";
    }
    static function GetCss($Area = 'CssHeader') {
        $Return = array();
        foreach(Theme::$$Area as $Css) {
            if($Css['type'] == 'file')
                $Return[] = '<link rel="stylesheet" type="text/css" href="' . $Css['css_string'] . '"/>';
        }
        return implode(PHP_EOL, $Return);
    }
    static function GenerateCssComponents($VariableName) {
        $GeneratedStyleSheet = array();
        foreach(Theme::$$VariableName as $_c) {
            $_c = trim($_c);
            $GeneratedStyleSheet[] = '<link rel="stylesheet" type="text/css" href="' . GLOBAL_DATA_DIR . 'library/bootstrap/' . $_c . '/css/bootstrap.min.css" />';
        }
        return implode(PHP_EOL, $GeneratedStyleSheet);
    }
    static function UseJquery($Version = '1.8.3') {
        if(defined('IS_AJAX')) {
            Theme::$AjaxJsHeader['jquery-' . $Version] = array('js_string' => GLOBAL_DATA_DIR . 'library/jquery-' . $Version . '.min.js', 'type' => 'file');

            //Theme::$AjaxJsFooter = Filter::array_unshift_assoc(Theme::$AjaxJsFooter, 'jquery-' . $Version, array('js_string' => GLOBAL_DATA_DIR . 'library/jquery-' . $Version . '.min.js', 'type' => 'file'));
        }
        else {
            Theme::$JsHeader['jquery-' . $Version] = array('js_string' => GLOBAL_DATA_DIR . 'library/jquery-' . $Version . '.min.js', 'type' => 'file');

            //Theme::$JsFooter = Filter::array_unshift_assoc(Theme::$JsFooter, 'jquery-' . $Version, array('js_string' => GLOBAL_DATA_DIR . 'library/jquery-' . $Version . '.min.js', 'type' => 'file'));
        }
    }
    static function JqueryUI($Component, $Version = '1.10.4', $JqueryVersion = '1.8.3') {
        if(!isset(Theme::$JsHeader['jquery-' . $JqueryVersion]))
            Theme::JsHeader('jquery-' . $JqueryVersion, GLOBAL_DATA_DIR . 'library/jquery-' . $JqueryVersion . '.min.js');
        Theme::JsFooter('jqueryUI-' . $Version . '-' . $Component, GLOBAL_DATA_DIR . 'library/jquery-ui/jquery-ui-' . $Version . '.' . $Component . '.min.js');
    }
    static function JqueryPlugin($Component, $JqueryVersion = '1.8.3') {
        if(!isset(Theme::$JsHeader['jquery-' . $JqueryVersion]))
            Theme::JsHeader('jquery-' . $JqueryVersion, GLOBAL_DATA_DIR . 'library/jquery-' . $JqueryVersion . '.min.js');
        Theme::JsFooter('jqueryPlugin-' . $Component, GLOBAL_DATA_DIR . 'library/jquery-plugins/' . $Component . '/' . $Component . '.min.js');
    }
    static function jsPlugin($plname, $useCss = true) {
        $pluginFile = GLOBAL_DATA_PATH . 'library' . DIRECTORY_SEPARATOR . 'js-plugins' . DIRECTORY_SEPARATOR . $plname . DIRECTORY_SEPARATOR . 'plugin.ini';
        if(!file_exists($pluginFile)) {
            Helper::Notify('warning', 'Javascript plugin missing: ' . $plname);
            return;
        }
        $pluginData = Input::parseInitFile($pluginFile, true);
        if(isset($pluginData['require']['jquery'])) {
            if($pluginData['require']['jquery'] == 1)
                self::UseJquery();
            elseif($pluginData['require']['jquery'] != 0 && $pluginData['require']['jquery'] == '')
                self::UseJquery($pluginData['require']['jquery']);
        }

        if(isset($pluginData['require']['libs']) && is_array($pluginData['require']['libs'])) {
            foreach($pluginData['require']['libs'] as $rqLibs) {
                if(isset($rqLibs['name'])) {
                    if(!isset($rqLibs['useCss'])) $rqLibs['useCss'] = 1;
                    self::jsPlugin($rqLibs['name'], $rqLibs['useCss']);
                }
            }
        }

        if(isset($pluginData['internal']['js']))
            foreach($pluginData['internal']['js'] as $pluginJs)
                self::JsFooter($plname, GLOBAL_DATA_DIR . 'library/js-plugins/' . $plname . '/' . $pluginJs);

        if(isset($pluginData['external']['js']) && is_array($pluginData['external']['js']))
            foreach($pluginData['external']['js'] as $pluginJs)
                self::JsFooter($plname, $pluginJs);

        if(isset($pluginData['internal']['css']) && is_array($pluginData['internal']['css']))
            foreach($pluginData['internal']['css'] as $pluginCss)
                self::CssFooter($plname, GLOBAL_DATA_DIR . 'library/js-plugins/' . $plname . '/' . $pluginCss);

        if(isset($pluginData['external']['css']) && is_array($pluginData['external']['css']))
            foreach($pluginData['external']['css'] as $pluginCss)
                self::CssFooter($plname, $pluginCss);
    }
    static function getStaticBlocks() {
        $layoutTplFile = self::$Working['theme_root'] . self::$Working['theme'] . DIRECTORY_SEPARATOR . self::$Working['layout'] . '.tpl';

        if(file_exists($layoutTplFile)) {
            self::$LayoutContent = $layoutContent = File::GetContent($layoutTplFile);
            $blockCheck = '/\{\{block\:\:([^0-9][a-zA-Z0-9\_]+)\s*\:{0,1}([a-zA-Z0-9\,\s]*)\}\}/';
            if(preg_match_all($blockCheck, $layoutContent, $matchBlocks)) {
                foreach($matchBlocks[0] as $blk => $bln) {
                    $blockNameKey = $matchBlocks[1][$blk] . ':' . $matchBlocks[2][$blk];
                    self::$BlockContents[$blockNameKey] = self::checkBlock($blockNameKey);
                }
            }
        }
    }
    static function checkBlock($block = '', $sendParams = array()) {
        if(!isset($sendParams['block_title'])) {
            $sendParams['block_title'] = isset($block['title']) ? $block['title'] : '';
        }
        if(!isset($sendParams['block_link'])) {
            $sendParams['block_link'] = isset($block['link']) ? $block['link'] : '';
        }
        $check = explode(':', $block);
        $params = isset($check[1]) ? array_filter(explode(',', $check[1])) : $sendParams;
        $block = $check[0];
        $blockFile = self::$BlockBasePath . $block . DIRECTORY_SEPARATOR . $block . '.php';
        if(!in_array($block, self::$IncludedBlocks)) {
            if(file_exists($blockFile)) {
                self::$CurrentBlock = basename($blockFile);
                include($blockFile);
                self::$IncludedBlocks[] = $block;
            }
            else {
                Helper::Notify('error', 'Missing block file: ' . $block);
                return '[' . $block . ']';
            }
        }
        $blockObj = new $block();
        $blockObj->InitBlock($blockFile);
        $blockObj->Main($params);
        return $blockObj->Output();
    }

    static function useBlock($block = '') {
        if(isset(self::$BlockContents[$block])) $blockContent = self::$BlockContents[$block];
        else $blockContent = '[' . $block . ']';
        return $blockContent;
    }

    static function getArea($area) {
        if(isset(self::$AreaContents[$area])) return implode('', self::$AreaContents[$area]);
        else return '';
    }

    static function hookString($position = '', $string = '') {
        if(!empty($position) && !empty($string)) {
            self::$Hook[$position] .= $string;
        }
    }

    static function getBlockOrders() {
        $currentSetting = unserialize(Option::get('blocks_order'));
        if(G::$Session->Get('designMode', 0) == 1) {
            $currentSetting = array_merge($currentSetting, G::$Session->Get('design_blockOrders', array()));
        }
        $currentPage = Router::GenerateThisRoute(false);
        $matches = Router::Match(RECOMPILE_ROUTE, $currentPage);
        if(isset($currentSetting[$currentPage])) return (array) $currentSetting[$currentPage];
        elseif(!empty($matches) && isset($matches['name']) && isset($currentSetting[$matches['name']])) return (array) $currentSetting[$matches['name']];
        else return array();
    }

    static function getThemeAreas() {
        $layoutContent = self::$LayoutContent;

        $routeName = isset(G::$Route['name']) ? G::$Route['name'] : '';
        $thisRoute = Router::GenerateThisRoute();

        $checkArea = '/\[\@([^0-9][a-zA-Z0-9\_]*)\@\]/';
        if(preg_match_all($checkArea, $layoutContent, $matchAreas)) {
            //n($matchAreas);
            $getBlocks = DB::Query('blocks')
                ->Where('area', 'IN', implode(',', $matchAreas[1]))->_AND()
                ->WhereGroupOpen()
                ->Where('include_urls', 'INCLUDE', $thisRoute)->_OR()
                ->Where('routes', 'INCLUDE', $routeName)->_OR()
                ->Where('routes', 'INCLUDE', 'all')
                ->WhereGroupClose()->_AND()
                ->WhereGroupOpen()
                ->Where('themes', 'INCLUDE', 'all')->_OR()
                ->Where('themes', 'INCLUDE', CURRENT_THEME)
                ->WhereGroupClose()->_AND()
                ->Where('exclude_urls', 'NOT_INCLUDE', $thisRoute)->_AND()
                ->Where('status', '=', 1)
                ->Get('bid');
            if($getBlocks->status && $getBlocks->num_rows > 0) {
                $readyBlocks = $getBlocks->Result;
                $blockOrders = self::getBlockOrders();
                /**** Split blocks into areas array ****/
                $prepareAreas = array();
                foreach($readyBlocks as $blockOrder => $blockData) {
                    $prepareAreas[$blockData['area']][$blockData['bid']] = $blockData;
                }
                /**** Get ordered blocks in to areas array ****/
                $finalBlockOrders = array();
                if(!empty($blockOrders)) {
                    foreach($blockOrders as $area => $areaBlockOrders) {
                        $finalBlockOrders[$area] = array();
                        foreach($areaBlockOrders as $bid) {
                            if(isset($readyBlocks[$bid])) {
                                $finalBlockOrders[$area][] = $readyBlocks[$bid];
                                unset($prepareAreas[$readyBlocks[$bid]['area']][$bid]);
                            }
                        }
                    }
                    foreach($blockOrders as $area => $areaBlockOrders) {
                        if(isset($prepareAreas[$area])) {
                            $finalBlockOrders[$area] += $prepareAreas[$area];
                            unset($prepareAreas[$area]);
                        }
                    }
                    $finalBlockOrders += $prepareAreas;
                }
                else $finalBlockOrders = $prepareAreas;
                foreach($finalBlockOrders as $area => $areaBlocks) {
                    foreach($areaBlocks as $blockOrder => $blockData) {
                        $blockFile = $blockData['file'];
                        $blockParams = unserialize($blockData['params']);
                        $blockParams['block_title'] = $blockData['title'];
                        $blockParams['block_link'] = $blockData['link'];
                        $blockContent = self::checkBlock($blockFile, $blockParams);
                        if(G::$Session->Get('designMode', 0) == 1)
                            $blockContent = '<div class="theme-block-mark-up block-type-' . $blockFile . ' block-id-' . $blockData['bid'] . '" id="block-' . $blockData['bid'] . '" data-block-id="' . $blockData['bid'] . '">' . $blockContent . '</div>';
                        self::$AreaContents[$area][] = $blockContent;
                    }
                }
            }
            if(G::$Session->Get('designMode', 0) == 1) {
                foreach($matchAreas[1] as $area) {
                    if(!isset(self::$AreaContents[$area]) || !is_array(self::$AreaContents[$area])) self::$AreaContents[$area] = array();

                    array_unshift(self::$AreaContents[$area], '<div class="theme-area-mark-up area-' . $area . '" data-area="' . $area . '">');
                    self::$AreaContents[$area][] = '</div>';
                }
            }
        }
    }

    static function PrepareTheme() {
        TPL::Config(	BASE_DIR,
            Theme::$Config['theme_root'] . Theme::$Working['theme'] . DIRECTORY_SEPARATOR,
            CACHE_PATH . 'compiled' . DIRECTORY_SEPARATOR,
            CACHE_PATH . 'html' . DIRECTORY_SEPARATOR,
            CACHE_PATH . 'compiled' . DIRECTORY_SEPARATOR
        );
    }
    static function PrepareVariables($Prefix = '', $state = false) {
        Theme::JsHeader('JsBaseFunctions', APPLICATION_DATA_DIR . 'static/js/base.js');
        Theme::JsHeader('VNP_BaseDir', 'var BASE_DIR="' . BASE_DIR . '";var current_route="' . G::$Route['name'] . '";', 'inline');
        Theme::JsHeader('VNP_Object', 'var VNP = new BaseObject(BASE_DIR);', 'inline');


        //Theme::$JsFooter = Filter::array_unshift_assoc(Theme::$JsFooter, 'VNP_Object', array('js_string' => 'var VNP = new BaseObject(BASE_DIR);', 'type' => 'inline'));

        //Theme::$JsFooter = Filter::array_unshift_assoc(Theme::$JsFooter, 'VNP_BaseDir', array('js_string' => 'var BASE_DIR="' . BASE_DIR . '";var current_route="' . G::$Route['name'] . '";', 'type' => 'inline'));

        //Theme::$JsFooter = Filter::array_unshift_assoc(Theme::$JsFooter, 'JsBaseFunctions', array('js_string' => APPLICATION_DATA_DIR . 'static/js/base.js', 'type' => 'file'));


        Theme::$Hook['header'] = Theme::GetJs($Prefix . 'JsHeader');
        Theme::$Hook['header'] .= Theme::GetCss($Prefix . 'CssHeader');
        Theme::$Hook['footer'] = Theme::GetJs($Prefix . 'JsFooter');
        Theme::$Hook['footer'] .= Theme::GetCss($Prefix . 'CssFooter');
        if(!$state) {
            $Helper = array('States'	=> Helper::GetState(),
                'Notifies'	=> Helper::GetNotify(),
                'Header'	=> Helper::GetPageHeader(),
                'Featured'	=> Helper::GetFeaturedPanel(),
                'PageInfo'	=> Helper::GetPageInfo());
            Theme::Assign('Helper', $Helper);
            Theme::Assign('Notify', Helper::getHtmlNotify());
        }
    }
    static function Output() {
        if(isset(G::$MyData['level']) && G::$MyData['level'] > 2) {
            Theme::CssHeader('admin_panel', GLOBAL_DATA_DIR . 'static/css/admin_panel.css');
            if(G::$Session->Get('designMode', 0) == 1) {
                Theme::JsFooter('design-mod', GLOBAL_DATA_DIR . 'static/js/design.js');
                Theme::JqueryUI('sortable');
                Theme::JqueryUI('draggable');
            }
        }
        if(defined('IS_AJAX')) {

            $ResponseString		= array();
            $ResponseString[]	= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            $ResponseString[]	= '<link rel="stylesheet" type="text/css" href="' . GLOBAL_DATA_DIR . 'library/bootstrap/normalize.min.css" />';
            $ResponseString[]	= '<link rel="stylesheet" type="text/css" href="' . GLOBAL_DATA_DIR . 'library/bootstrap/bootstrap-theme.min.css" />';
            $ResponseString[]	= Theme::GenerateCssComponents('AjaxCssComponents');
            $ResponseString[]	= '<link rel="stylesheet" type="text/css" href="/' . APPLICATION_NAME . '/' . DATA_DIR . '/theme/' . Theme::$Config['theme'] . '/css/style.css" />';
            if(IS_AJAX == 'text') self::PrepareVariables('Ajax', true);
            $ResponseString[]	= Theme::$Hook['header'];
            $ResponseString[]	= Helper::getHtmlNotify();
            $ResponseString[]	= Theme::$BodyContent;
            $ResponseString[]	= Theme::$Hook['footer'];
            if(IS_AJAX == 'text' || IS_AJAX == 'json')
                echo implode(PHP_EOL,$ResponseString);
            elseif(IS_AJAX == 'state') {
                $BodyData = implode(PHP_EOL,$ResponseString);
                echo json_encode(array('title' => Theme::$Title, 'data' => $BodyData));
            }
            exit();
        }
        if(in_array(Theme::$AjaxSession, array('text', 'json'))) {
            echo Theme::$BodyContent;
            exit();
        }
        else {
            Theme::PrepareTheme();
            Theme::PrepareVariables();
            $Output = TPL::File(Theme::$Working['layout'], Theme::$Working['re_compile'], Theme::$Working['is_cache']);
            $Output->Assign(Theme::$ThemeVariables);
            $Output->Assign('META', Theme::$MetaTags);
            $Output->Assign('META_PROPERTY', Theme::$MetaProperty);
            $Output->Assign('CssComponents', Theme::GenerateCssComponents('CssComponents'));
            $Output->Assign('TITLE', Theme::$Title);
            $Output->Assign('Hook', Theme::$Hook);
            $Output->Assign('BODY', Theme::$BodyContent);
            $Output->Assign('VNP_Paging', Output::Page());
            return $Output->Output();
        }
    }
}

?>