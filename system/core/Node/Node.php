<?php

/**
 * Node Class
 *
 * Boot up an application, detect running environment
 *
 * @package		VNP
 * @subpackage	Base libraries
 * @author		VNP Dev team
 * @category	Base layer
 * @link		http://vnphp.com/docs/base-layer/libraries/Boot.html
 */

if( !defined('VNP_SYSTEM') && !defined('VNP_APPLICATION') ) die('Access denied!');

class NodeBase
{
    static public $_currentTextField = '';
    static public $_currentValueField = '';
    static public $_currentRefField = '';
    static private $VStorage = array();
    static public $tags = array();
    static function getNodeTypes($nodeTypeName = '') {
        if(file_exists(NODE_CACHE_FILE . NODE_BUILDER_CACHE_EXT)) {
            $NodeTypes = unserialize(File::GetContent(NODE_CACHE_FILE . NODE_BUILDER_CACHE_EXT));
            return ($nodeTypeName == '') ? $NodeTypes : $NodeTypes['NodeTypes'][$nodeTypeName];
        }
        return false;
    }
    static function getNodes_Option($NodeTypeName, $textField, $valueField) {
        self::$_currentTextField = $textField;
        self::$_currentValueField = $valueField;
        if($NodeType = self::getNodeTypes($NodeTypeName)) {
            foreach($NodeType['NodeFields'] as $fieldName => $fieldData) {
                if ($fieldData['type'] == 'referer' && $fieldData['referer']['node_type'] == $NodeTypeName) {
                    self::$_currentRefField = $fieldName;
                    break;
                }
            }
            if(!function_exists('prepareAdt')) {
                function prepareAdt($Row) {
                    $textField = NodeBase::$_currentTextField;
                    $valueField = NodeBase::$_currentValueField;
                    $rt = array('text' => $Row[$textField],
                                'value' => $Row[$valueField],
                                $textField => $Row[$textField],
                                $valueField => $Row[$valueField]);
                    if(!empty(NodeBase::$_currentRefField))
                        $rt[NodeBase::$_currentRefField] = $Row[NodeBase::$_currentRefField];
                    return $rt;
                }
            }

            $columns = array($textField, $valueField);
            if(!empty(NodeBase::$_currentRefField)) $columns[] = NodeBase::$_currentRefField;
            self::$VStorage[$NodeTypeName . '_' . $textField . '_' . $valueField] = $v = DB::Query($NodeTypeName)->Columns($columns)->Adapter('prepareAdt')->Get($valueField)->Result;

            array_unshift($v, array(  'text' => '---- None ----',
                'value' => 0,
                $textField => '---- None ----',
                $valueField => 0));

            if(!empty(NodeBase::$_currentRefField)) {
                $v[0][NodeBase::$_currentRefField] = 0;
            }
            $v = Filter::BuildLevelList($v, $valueField, NodeBase::$_currentRefField);
            self::$_currentRefField = NodeBase::$_currentTextField = NodeBase::$_currentValueField = '';
            ksort($v);
            return $v;
        }
        else {
            Helper::Notify('error', 'Nodetype not found!');
            return array();
        }
    }

    static function getNodes_IDKey($NodeTypeName, $textField, $valueField) {
        if(isset(self::$VStorage[$NodeTypeName . '_' . $textField . '_' . $valueField])) {
            self::$VStorage[$NodeTypeName . '_' . $textField . '_' . $valueField][0][$textField] = 'None';
            return self::$VStorage[$NodeTypeName . '_' . $textField . '_' . $valueField];
        }
        else {
            return array();
        }
    }

    static function getNodes($table, $field = '', $limit = array()) {
        if($field != '') {
            $field = array_filter(explode(',', $field));
            $field[] = $table . '_id';
            $gns = DB::Query($table)->Columns($field);
        }
        else $gns = DB::Query($table);
        if(!empty($limit)) $gns = $gns->Limit($limit);
        return $gns->Get($table . '_id')->Result;
    }

    static function getNodesIn($table, $field, $search, $limit = 0) {
        $search = array_filter(array_unique(array_map('trim', explode(',', $search))));
        if(empty($search)) return array();
        $field = array_filter(explode(',', $field));
        $field[] = $table . '_id';
        $rt = DB::Query($table)
                    ->Columns($field)
                    ->Where($table . '_id', 'IN', $search);
        if($limit > 0) $rt = $rt->limit($limit);
        return $rt->Get($table . '_id')->Result;
    }

    static function getTags($tagString = '') {
        if(empty(self::$tags))
            self::$tags = DB::Query('global_tags')->Get('title')->Result;
        if($tagString == '') return self::$tags;
        elseif(isset(self::$tags[$tagString])) return self::$tags[$tagString];
        else return false;
    }

    static function getView($nodetype = '', $method = '') {
        $viewResult = array();
        include APPLICATION_PATH . 'node_mapper.php';
        if($nodetype == '') {
            if(isset($NodeProfile) && !empty($NodeProfile) && is_array($NodeProfile)) {
                foreach($NodeProfile as $NodeType => $NodeData) {
                    $viewPath = GLOBAL_VIEWS_PATH . $NodeType . DIRECTORY_SEPARATOR . $NodeType . '.php';
                    if(file_exists($viewPath)) {
                        include $viewPath;
                        $viewObj = new $NodeType();
                        $viewObj->InitPortalView($viewPath);
                        if(method_exists($viewObj, $method))
                            $viewResult[$NodeType] = $viewObj->$method();
                    }
                }

            }
        }
        else {
            $viewPath = GLOBAL_VIEWS_PATH . $nodetype . DIRECTORY_SEPARATOR . $nodetype . '.php';
            if(file_exists($viewPath)) {
                include $viewPath;
                $viewObj = new $nodetype();
                $viewObj->InitPortalView($viewPath);
                if(method_exists($viewObj, $method))
                    $viewResult[$nodetype] = $viewObj->$method();
            }
        }
        n($viewResult);
        return $viewResult;
    }
}

?>