<?php

class Request {
    static function getEnv( $rkeys ) {
        if(!is_array($rkeys))
        {
            $rkeys = array($rkeys);
        }
        foreach($rkeys as $k)
        {
            if(isset($_SERVER[$k])) return $_SERVER[$k];
            elseif(isset($_ENV[$k])) return $_ENV[$k];
            elseif(@getenv($k)) return @getenv($k);
            elseif(function_exists('apache_getenv') && apache_getenv($k, true)) return apache_getenv($k, true);
        }
        return '';
    }
    static function getClientIP() {
        if(self::getEnv('HTTP_CLIENT_IP' != ''))
            return self::getEnv('HTTP_CLIENT_IP');
        elseif(self::getEnv('HTTP_X_FORWARDED_FOR') != '')
            return self::getEnv('HTTP_X_FORWARDED_FOR');
        else return self::getEnv('REMOTE_ADDR');
    }
}