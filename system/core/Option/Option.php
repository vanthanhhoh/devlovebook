<?php

class Option {
    const OPTION_TABLE = 'options';
    static $Options = array();
    function __construct() {

    }
    static function getOptionsOfType($type = 'global', $extra = '') {
        //if(isset(self::$Options[$type])) return self::$Options[$type];
        if($type != '') {
            $getOptions = DB::Query(self::OPTION_TABLE);
            $getOptions = $getOptions->Where('type', '=', $type);
            if(!empty($extra))
                $getOptions = $getOptions->_AND()->Where('extra', '=', $extra);
            $getOptions = $getOptions->Get('name');
        }
        else
            $getOptions = DB::Query(self::OPTION_TABLE)->Get('name');
        if($getOptions->status && $getOptions->num_rows > 0)
            self::$Options = array_merge(self::$Options, $getOptions->Result);
        return $getOptions->Result;
    }
    static function getFull() {
        return self::$Options;
    }
    static function getPairs() {
        $pairs = array();
        foreach(self::$Options as $opt)
            $pairs[$opt['name']] = $opt['value'];
        return $pairs;
    }
    static function get($name, $type = 'global', $extra = '') {
        if(empty(self::$Options)) {
            self::getOptionsOfType();
            if(!empty($type) && $type != 'global')
                self::getOptionsOfType($type, $extra);
        }
        if(isset(self::$Options[$name]))
            return self::$Options[$name]['value'];
        else {
            $getOption = DB::Query(self::OPTION_TABLE)->Where('name', '=', $name)->Get();
            if($getOption->status && $getOption->num_rows > 0) {
                self::$Options[$name] = $getOption->Result[0];
                return $getOption->Result[0]['value'];
            }
            else return false;
        }
    }
    static function add($name = '', $value = '', $title = '', $type = '', $extra = '') {
        if(empty($name)) {
            Error::Set('Option name không thể để trống');
            return false;
        }
        $addOpts = DB::Query(self::OPTION_TABLE)
                        ->Insert(
                            array(  'name' => $name,
                                    'value' => $value,
                                    'title' => $title,
                                    'type' => $type,
                                    'extra' => $extra)
                        );
        if($addOpts->status && $addOpts->insert_id) return true;
        else {
            Error::Set('Cannot add option ' . $name);
            return false;
        }
    }
    static function set($name = '', $value = '', $title = '', $type = '', $extra = '', $status = 1) {
        if(is_array($name))
            $name = array_merge(
                array(  'name'  => '',
                        'value' => '',
                        'title' => '',
                        'type'  => '',
                        'extra' => '',
                        'status'=> ''
                ),
                $name
            );
        else
            $name = array(
                'name'  => $name,
                'value' => $value,
                'title' => $title,
                'type'  => $type,
                'extra' => $extra,
                'status'=> $status
            );
        if(empty($name) || (isset($name['name']) && empty($name['name']))) {
            Error::Set('Option name không thể để trống');
            return false;
        }
        $setOpts = DB::Query(self::OPTION_TABLE)
            ->Replace($name);
        if($setOpts->status) return true;
        else {
            Error::Set('Cannot add option ' . $name);
            return false;
        }
    }
    static function update($name = '', $data = array()) {
        if(empty($name)) {
            Error::Set('Option name không thể để trống');
            return false;
        }
        $delOpts = DB::Query(self::OPTION_TABLE)->Where('name', '=', $name)->Update($data);
        if($delOpts->status) return true;
        else {
            Error::Set('Cannot update option ' . $name);
            return false;
        }
    }
    static function delete($name = '') {
        if(empty($name)) {
            Error::Set('Option name không thể để trống');
            return false;
        }
        $delOpts = DB::Query(self::OPTION_TABLE)->Where('name', '=', $name)->Delete();
        if($delOpts->status) return true;
        else {
            Error::Set('Cannot delete option ' . $name);
            return false;
        }
    }
    static function formFromConfig($formName, $fields = array(), $hook = array(), $rebuild = false) {

        $hook = array_merge(array(
            'form_action' => '',
            'form_name' => '',
            'form_class' => '',
            'form_id' => '',
            'form_before' => '',
            'form_after' => ''
        ), $hook);

        $return = array('submitedValues' => '', 'form' => '');

        if(Input::Post('saveOptions') == 1) {
            //$return['submitedValues'] = Input::Post('Field');
            $__fs = Input::Post('Field');
            foreach($__fs as $_fk => $_fv) {
                if($fields[$_fk]['type'] == 'referer' && in_array($fields[$_fk]['display'], array('multi_selectbox', 'checkbox')))
                    $return['submitedValues'][$_fk] = implode(',', array_filter($_fv));
                elseif($fields[$_fk]['type'] == 'attributes')
                    $return['submitedValues'][$_fk] = serialize(array_filter($_fv));
                elseif($fields[$_fk]['type'] == 'multi_image')
                    $return['submitedValues'][$_fk] = implode(',', array_filter($_fv));
                elseif($fields[$_fk]['type'] == 'tags')
                    $return['submitedValues'][$_fk] = implode(',', array_filter($_fv));
                elseif($fields[$_fk]['type'] == 'list')
                    $return['submitedValues'][$_fk] = implode(',', array_filter($_fv));
                elseif($fields[$_fk]['type'] == 'multi_value')
                    $return['submitedValues'][$_fk] = implode(',', array_filter($_fv));
                else $return['submitedValues'][$_fk] = $_fv;

                $fields[$_fk]['value'] = $return['submitedValues'][$_fk];
            }
        }

        Theme::AddCssComponent('Forms,InputGroups');

        $FormElements = array(
            'text'			=> 'input',
            'number'		=> 'input',
            'textarea'		=> 'textarea',
            'html'			=> 'html',
            'image'			=> 'image',
            'multi_image'	=> 'multi_image',
            'attributes'	=> 'attributes',
            'tags'			=> 'tags',
            'list'          => 'list_select',
            'meta_description' => 'textarea'
        );
        $AdapterFunctions = array();
        $FormVariables = $PrepareOptions = array();
        $Sortable = $TagsTextText = '';

        $Form = new Form($formName, $rebuild);
        $Form->SetArrayVar('Field');

        foreach($fields as $fname => $Field) {

            if(empty($fname) || empty($Field['label'])) continue;
            if(!isset($Field['require'])) $Field['require'] = false;

            $FieldTemplate = isset($FormElements[$Field['type']]) ? $FormElements[$Field['type']] : 'input';
            $FieldObj = $Form->$FieldTemplate($fname)
                ->Label($Field['label'])
                ->Value($Field['value'])
                ->Required($Field['require'])
                ->FieldClass('FieldType_' . $Field['type'] . ' Field_' . $fname);
            if(in_array($Field['type'], array('number', 'text', 'file'))) {
                $FieldObj->Type($Field['type']);
            }
            if(in_array($Field['type'], array('single_value', 'multi_value'))) {
                if(!isset($Field['display'])) $Field['display'] = 'single_selectbox';
                if($Field['display'] == 'single_selectbox') $T = 'select';
                if($Field['display'] == 'multi_selectbox') $T = 'multi_select';
                if($Field['display'] == 'radio') $T = 'radio';
                if($Field['display'] == 'checkbox') $T = 'checkbox';
                foreach($Field['options'] as $fk => $fv) {
                    $Field['options'][] = array('value' => $fk, 'text' => $fv);
                    unset($Field['options'][$fk]);
                }
                if(!$Field['require']) array_unshift($Field['options'], array('text' => 'Select', 'value' => ''));
                $FieldObj = $Form->$T($fname)
                    ->Label($Field['label'])
                    ->Value($Field['value'])
                    ->FieldClass('FieldType_' . $Field['type'] . ' Field_' . $fname)
                ;
                $FieldObj->Options($Field['options'])->StaticOptions(true);
            }
            if($Field['type'] == 'referer') {
                if(!isset($Field['display'])) $Field['display'] = 'single_selectbox';
                $T = $Field['display'];
                if($Field['display'] == 'single_selectbox') $T = 'select';
                if($Field['display'] == 'multi_selectbox') $T = 'multi_select';
                $FieldObj = $Form->$T($fname)
                    ->Label($Field['label'])
                    ->Value($Field['value'])
                    ->FieldClass('FieldType_' . $Field['type'] . ' Field_' . $fname);
                $RefererTable = $Field['referer']['node_type'];
                $DisplayField = $Field['referer']['node_field'];
                $AdapterFunctionName = 'vnp_' . $DisplayField . '_' . $RefererTable;
                if(!in_array($AdapterFunctionName, $AdapterFunctions)) {
                    $AdapterFunctions[] = $AdapterFunctionName;
                    $PrepareOptions[] = '$' . $AdapterFunctionName . ' = NodeBase::getNodes_Option(\'' . $RefererTable . '\', \'' . $DisplayField . '\', \'' . $RefererTable . '_id\');';

                }

                $PrepareOptions[] = '$Fields[\'' . $fname . '\'][\'Options\'] = $' . $AdapterFunctionName .';';
                $FieldObj->Options(array());
                $FieldObj->StaticOptions(false);
            }

            if($Field['type'] == 'list') {
                $RefererTable = $Field['referer']['node_type'];
                $DisplayField = $Field['referer']['node_field'];
                $FieldObj->AddVar(array('ref_table' => $RefererTable, 'ref_field' => $DisplayField));
            }

            if($Field['type'] == 'html')
                $HtmlEditor[] = 'Editor::AddEditor(\'#ID_Field_' . $fname . '\');';
            if($Field['type'] == 'attributes')
                $Sortable = 'Theme::JqueryUI(\'sortable\');';
            if($Field['type'] == 'tags') {
                $TagsTextText = 'Theme::JqueryUI(\'autocomplete\');' . PHP_EOL;
                $TagsTextText .= "\t\t" . 'Theme::JqueryPlugin(\'TagsInput\');' . PHP_EOL;
                $TagsTextText .= "\t\t" . 'Theme::CssHeader(\'jquery-ui-autocomplete\', GLOBAL_DATA_DIR . \'library/jquery-ui/jquery-ui-1.10.4.autocomplete.css\');' . PHP_EOL;
                $TagsTextText .= "\t\t" . 'Theme::CssHeader(\'jquery-plugin-TagsInput\', GLOBAL_DATA_DIR . \'library/jquery-plugins/TagsInput/jquery.tagsinput.css\');' . PHP_EOL;
                $TagsTextText .= "\t\t" . '$TagsIniStr = \'$(\\\'.VNP_TagsField\\\').tagsInput({
		  					autocomplete_url: function(request, response) {
	  							console.log(request.term);
						        VNP.Ajax({
						          type: \\\'post\\\',
						          url: \\\'Node/GetTags\\\',
						          dataType: "jsonp",
						          data: {
						            TagQ: request.term
						          },
						          success: function( data ) {
						            response( data );
						          },
						          error: function( data ) {
						          	console.log(data);
						          },
						          complete: function() {
						          	VNP.Loader.hide();
						          }
						        }, \\\'json\\\');
							}
						});\';' . PHP_EOL;

                $TagsTextText .= "\t\t" . 'Theme::JsFooter(\'IniTagsObject-\', $TagsIniStr, \'inline\');';
            }
            $Form->AddFormElement($FieldObj);
        }

        if(!empty($HtmlEditor)) {
            $PrepareOptions[] = 'Boot::Library(\'Editor\');';
            $PrepareOptions = array_merge($PrepareOptions, $HtmlEditor);
            $PrepareOptions[] = 'Editor::Replace();';
        }
        $PrepareOptions[] = $Sortable;
        $PrepareOptions[] = $TagsTextText;

        $returnString = '<?php ' . implode(PHP_EOL, $PrepareOptions) . '?>
        <form action="' . $hook['form_action'] . '" class="' . $hook['form_class'] . '" id="' . $hook['form_id'] . '" method="post">
            <input type="hidden" name="saveOptions" value="1" />' .
            $hook['form_before'] .
            $Form->Render() .
            $hook['form_after'] . '
            <div style="text-align:center"><input type="submit" value="Lưu lại" class="btn btn-primary" /></div>
        </form>';
        $optionFile = CACHE_PATH . 'compiled' . DIRECTORY_SEPARATOR . $formName . '.php';
        if($rebuild) File::Create($optionFile, $returnString);

        ob_start();
        $Fields = $fields;
        include($optionFile);
        $return['form'] = ob_get_clean();

        return $return;

    }
}