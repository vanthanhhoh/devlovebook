<?php

class BaseExtension {
    public function Init() {
        Hook::on('before_extension_init');
    }

    static function load_extension($ext_name = '') {
        return Hook::load_extension($ext_name);
    }

    static function get_all_extensions($ext_name = '') {
        $valid_extensions = array();
        $exts_inits = glob(EXTENSION_PATH . '*');
        foreach($exts_inits as $ext) {
            if (!file_exists($ext . DIRECTORY_SEPARATOR . 'config.ini')) continue;
            else {
                $ext_init = Input::parseInitFile($ext . DIRECTORY_SEPARATOR . 'config.ini', true);
                $valid_extensions[basename($ext)] = $ext_init;
            }
        }
        return $valid_extensions;
    }
    static function replace_ext_url_shortcode($shortcode_str = '') {
        if($shortcode_str == '') return '';
        if(preg_match('/\[extension\=([a-zA-Z0-9\_]+)\]([a-zA-Z0-9\_\s]+)\[\/extension\]/', $shortcode_str, $m)) {
            $shortcode_str = str_replace($m[0], '<a href="' . self::get_ext_url($m[1]) . '" title="' . $m[2] . '">' . $m[2] . '</a>', $shortcode_str);
            return $shortcode_str;
        }
    }
    static function get_ext_url($ext_name) {
        return Router::Generate('ControllerParams', array('controller' => 'extension', 'action' => 'info', 'params' => $ext_name));
    }
}