<?php

if( !defined('VNP_SYSTEM') && !defined('VNP_APPLICATION') ) die('Access denied!');

abstract class BaseController
{
	private $tplPath = '';
	public $Registry;
	abstract public function Main();
	public function __construct() {
		//$this->Registry = G::$Registry;
	}
	public function ControllerInitializer($ControllerFile) {
		$this->UseCssComponents('BreadCrums');
		if(defined('ADMIN_AREA')) Helper::State('Bảng điều khiển', BASE_DIR);
		$controller_name = CONTROLLER_NAME;
		$application_name = APPLICATION_NAME;
		if(isset($application_name::$NodeProfile) && isset($application_name::$NodeProfile[CONTROLLER_NAME]))
			$controller_name = $application_name::$NodeProfile[CONTROLLER_NAME]['title'];
		elseif(isset($application_name::$ControllerInfo) && isset($application_name::$ControllerInfo[CONTROLLER_NAME]))
			$controller_name = $application_name::$ControllerInfo[CONTROLLER_NAME]['title'];
		if(defined('ADMIN_AREA'))
			Helper::State($controller_name, Router::Generate('Controller', array('controller' => CONTROLLER_NAME)));
		$this->ControllerAction = G::$Registry['ControllerAction'];
		$this->Registry = G::$Registry;
		$this->tplPath = dirname($ControllerFile) . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR;
	}
	public function ControllerHookAfter() {
	}
	public function View($File, $ReCompile = false, $IsCache = false) {
		$view = TPL::File($File, $ReCompile, $IsCache);
		$view->SetDir('TPLFileDir', $this->tplPath);
		return $view;
	}
	public function SetTitle($Title) {
		Theme::SetTitle($Title);
	}
	public function HookBefore($Target, $Action) {
	}
	public function HookAfter($Target, $Action) {
	}
	public function UseCssComponents($Components){
		Theme::AddCssComponent($Components);
	}
	public function Render($Content, $ConfigArray = array()) {
		Hook::on('before_action_rendered');
		Theme::$BodyContent .= $Content;
		$Config = array(	'layout'		=> Theme::$Config['layout'],
							'theme'			=> Theme::$Config['theme'],
							're_compile'	=> false,
							'is_cache'		=> false,
						);
		$Config = array_merge($Config, $ConfigArray);
		if($Config['layout'] != '') Theme::$Working['layout'] = $Config['layout'];
		//else Theme::$Working['layout'] = Theme::$Config['layout'];
		if($Config['theme'] != '') Theme::$Working['theme']	= $Config['theme'];
		//else Theme::$Working['theme'] = Theme::$Config['theme'];
		Theme::$Working['re_compile']	= $Config['re_compile'];
		Theme::$Working['is_cache']	= $Config['is_cache'];
		//Helper::State(basename(Router::GenerateThisRoute()), Router::GenerateThisRoute());
		Hook::on('after_action_rendered');
	}
	public function ControllerAddress($FilePath, $Type = 'url') {
		$PDPath = realpath(dirname($FilePath));
		if($Type == 'url')
			return GLOBAL_BASE_URL . APPLICATION_NAME . '/' . CONTROLLER_DIR . '/' . rtrim(Boot::$ControllerGroup, DIRECTORY_SEPARATOR) . '/' . basename($PDPath) . '/';
		elseif($Type == 'path') return $PDPath;
		else return '';
	}
}

?>