<div class="form-group FieldWrap [@@FieldClass@@] Field_Artibutes">
	<label class="col-sm-3 control-label" for="ID_[@@FieldName@@]">[@@FieldLabel@@]</label>
    <div class="col-sm-9 clearfix" id="[@@FieldRealName@@]_FieldAttribute_Ctner">
    	<button class="btn btn-success save_attr_tpl" data-field-name="[@@FieldRealName@@]" type="button"><span class="glyphicon glyphicon-floppy-save"></span>&nbsp Save Attributes template</button>
    	<button class="btn btn-primary load_attr_tpl" data-field-name="[@@FieldRealName@@]" type="button"><span class="glyphicon glyphicon-floppy-open"></span>&nbsp Load Attributes template</button>
    	<input type="hidden" name="[@@FieldName@@]" id="ID_[@@FieldName@@]" class="form-control [@@FieldClass@@]" value=""/>
    	<!-- Option to clone -->
        <div class="Option_To_Clone" style="display:none" data-field-name="[@@FieldRealName@@]">
            <div class="input-group NodeField_Option">
                <span class="input-group-addon Move_Option">
                    <span class="glyphicon glyphicon-resize-vertical"></span>
                </span>
                <span class="input-group-addon">Name</span>
                <input type="text" class="form-control FieldOption_Name" name="" placeholder="Attribute Name">
                <span class="input-group-addon">Value</span>
                <input type="text" class="form-control FieldOption_Value" name="" placeholder="Attribute Value">
                <div class="input-group-btn">
                    <button class="btn btn-danger Remove_Option" data-field-name="[@@FieldRealName@@]"><span class="glyphicon glyphicon-remove"></span></button>
                </div>
            </div>
        </div>
        <!-- End option to clone -->
        <?php if(empty([@@FieldValue@@])): ?>
        <div class="Field_[@@FieldRealName@@]_attribute FieldAttributeCtner" data-field-name="[@@FieldRealName@@]">
            <div class="input-group NodeField_Option">
                <span class="input-group-addon Move_Option">
                    <span class="glyphicon glyphicon-resize-vertical"></span>
                </span>
                <span class="input-group-addon">Name</span>
                <input type="text" class="form-control FieldOption_Name" name="[@@FieldName@@][0][name]" placeholder="Attribute Name">
                <span class="input-group-addon">Value</span>
                <input type="text" class="form-control FieldOption_Value" name="[@@FieldName@@][0][value]" placeholder="Attribute Value">
                <div class="input-group-btn">
                    <button class="btn btn-primary Add_Option" data-field-name="[@@FieldRealName@@]"><span class="glyphicon glyphicon-plus"></span></button>
                </div>
            </div>
        </div>
    	<?php else:
        $Attributes = unserialize([@@FieldValue@@]); foreach($Attributes as $i => $att) { ?>
	        <div class="Field_[@@FieldRealName@@]_attribute FieldAttributeCtner" data-field-name="[@@FieldRealName@@]">
	            <div class="input-group NodeField_Option">
	                <span class="input-group-addon Move_Option">
	                    <span class="glyphicon glyphicon-resize-vertical"></span>
	                </span>
	                <span class="input-group-addon">Name</span>
	                <input type="text" class="form-control FieldOption_Name" name="[@@FieldName@@][<?php echo $i ?>][name]" value="<?php echo $att['name'] ?>" placeholder="Attribute Name">
	                <span class="input-group-addon">Value</span>
	                <input type="text" class="form-control FieldOption_Value" name="[@@FieldName@@][<?php echo $i ?>][value]" value="<?php echo $att['value'] ?>" placeholder="Attribute Value">
	                <div class="input-group-btn">
	                	<?php if($i == 0): ?>
	                    <button class="btn btn-primary Add_Option" data-field-name="[@@FieldRealName@@]"><span class="glyphicon glyphicon-plus"></span></button>
	                	<?php else: ?>
	                    <button class="btn btn-danger Remove_Option" data-field-name="[@@FieldRealName@@]"><span class="glyphicon glyphicon-remove"></span></button>
	                	<?php endif ?>
	                </div>
	            </div>
	        </div>
        <?php } endif ?>
   	</div>
</div>