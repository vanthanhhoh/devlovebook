<div class="form-group FieldWrap [@@FieldClass@@]">
    <label for="ID_[@@FieldName@@]" class="col-sm-3 control-label">[@@FieldLabel@@]</label>
    <input data-field-type="list_select" type="hidden" class="form-control NodeField_ListSelect [@@FieldClass@@]" name="[@@FieldName@@][]" id="ID_[@@FieldRealName@@]" value=""/>
    <div class="col-sm-9">
        <div class="clearfix">
            <button class="btn btn-primary" onclick="VNP.SugarBox.Open('ListNodesSelect',{ref_table: [@@RefTable@@], ref_field: [@@RefField@@], current_nodes: '<?php echo [@@FieldValue@@] ?>', field: '[@@FieldRealName@@]'}); return false;">Browse</button>
            <button class="btn btn-danger" onclick="document.getElementById('ListSelect_[@@FieldRealName@@]').innerHTML = ''; return false;">Delete all</button>
        </div>
        <ul class="clearfix ListSelect_Container" id="ListSelect_[@@FieldRealName@@]">
            <?php $refNodes = NodeBase::getNodesIn([@@RefTable@@], [@@RefField@@], [@@FieldValue@@]) ?>
            <?php foreach($refNodes as $_nodeID => $_node) {
                if(!empty($_node)): ?>
                    <li class="ListSelect_Item">
                        <input type="hidden" name="Field[[@@FieldRealName@@]][]" value="<?php echo $_nodeID ?>" />
                        <?php echo $_node[[@@RefField@@]] ?>
                        <a href="#" onclick="this.parentNode.parentNode.removeChild(this.parentNode); return false;">Delete</a>
                    </li>
                <?php endif;
            } ?>
        </ul>
    </div>
</div>