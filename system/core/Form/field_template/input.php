<div class="form-group FieldWrap [@@FieldClass@@]">
	<label class="col-sm-3 control-label" for="ID_[@@FieldName@@]">[@@FieldLabel@@]</label>
    <div class="col-sm-9">
		<input type="[@@FieldType@@]" name="[@@FieldName@@]" id="ID_[@@FieldName@@]" class="form-control [@@FieldClass@@]" value="[@@FieldValue@@]" />
 	</div>
</div>