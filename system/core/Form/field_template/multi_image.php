<div class="form-group FieldWrap [@@FieldClass@@]">
    <label for="ID_[@@FieldName@@]" class="col-sm-3 control-label">[@@FieldLabel@@]</label>
   	<input data-field-type="multi_image" type="hidden" class="form-control NodeField_MultiImages [@@FieldClass@@]" name="[@@FieldName@@][]" id="ID_[@@FieldRealName@@]" value=""/>
    <div class="col-sm-9">
    	<div class="clearfix">
            <button class="btn btn-primary" onclick="VNP.SugarBox.Open('MultiImages','[@@FieldRealName@@]'); return false;">Browse</button>
            <button class="btn btn-danger" onclick="document.getElementById('MultiImages_[@@FieldRealName@@]').innerHTML = ''; return false;">Delete all</button>
      	</div>
        <div class="clearfix MultiImages_Container" id="MultiImages_[@@FieldRealName@@]">
        	<?php $Images = explode(',', [@@FieldValue@@]); foreach($Images as $_Img) {
	            if(!empty($_Img)): ?>
	            <div class="MultiImages_Item">
	            	<input type="hidden" name="Field[[@@FieldRealName@@]][]" value="<?php echo $_Img ?>" />
	                <img src="<?php echo THUMB_BASE ?>60_60<?php echo $_Img ?>" width="60"/>
	                <a href="#" onclick="this.parentNode.parentNode.removeChild(this.parentNode); return false;">Delete</a>
	          	</div>
	            <?php endif;
            } ?>
        </div>
    </div>
</div>