<div class="form-group FieldWrap [@@FieldClass@@]">
	<label class="col-sm-3 control-label" for="ID_[@@FieldRealName@@]">[@@FieldLabel@@]</label>
    <div class="col-sm-9">
		<textarea name="[@@FieldName@@]" id="ID_[@@FieldRealName@@]" class="form-control [@@FieldClass@@] VNP_TagsField">[@@FieldValue@@]</textarea>
 	</div>
</div>