<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_[@@FieldID@@]">[@@FieldLabel@@]</label>
    <div class="col-sm-9">
    	<textarea name="[@@FieldName@@]" id="ID_[@@FieldID@@]" class="form-control [@@FieldClass@@]">[@@FieldValue@@]</textarea>
  	</div>
</div>