<div class="form-group FieldWrap FieldType_html Field_[@@FieldRealName@@]">
    <div class="col-sm-12">
    	<label class="col-sm-12 control-label" for="ID_[@@FieldID@@]">[@@FieldLabel@@]</label>
    	<textarea name="[@@FieldName@@]" id="ID_[@@FieldID@@]" class="form-control [@@FieldClass@@]">[@@FieldValue@@]</textarea>
  	</div>
</div>