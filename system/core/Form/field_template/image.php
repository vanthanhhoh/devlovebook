<div class="form-group FieldWrap [@@FieldClass@@] clearfix">
	<label class="col-sm-3 control-label" for="ID_[@@FieldName@@]">[@@FieldLabel@@]</label>
    <div class="col-sm-6">
    	<input type="text" name="[@@FieldName@@]" id="ID_[@@FieldName@@]" class="form-control [@@FieldClass@@]" value="[@@FieldValue@@]"/>
   	</div>
    <div class="col-sm-3">
		<button class="btn btn-primary" type="button" onclick="VNP.SugarBox.Open('FileManager','ID_[@@FieldName@@]'); return false;">Browse</button>
		<button class="btn btn-danger" onclick="document.getElementById('ID_[@@FieldName@@]').value = ''; document.getElementById('Thumb_ID_[@@FieldName@@]').setAttribute('src', 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=='); return false;">Delete</button>
	</div>
    <div class="col-sm-8 FieldImagePreviewer"><img id="Thumb_ID_[@@FieldName@@]"></div>
</div>