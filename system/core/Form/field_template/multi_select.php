<div class="form-group">
	<label class="col-sm-3 control-label" for="ID_[@@FieldName@@]">[@@FieldLabel@@]</label>
    <div class="col-sm-9">
        <select name="[@@FieldName@@][]" id="ID_[@@FieldName@@]" multiple="multiple" class="form-control [@@FieldClass@@]" [@@FieldMultiple@@]>
            [@@FieldOptions@@]
        </select>
    </div>
</div>