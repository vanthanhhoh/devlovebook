<?php

if( !defined('VNP_SYSTEM') && !defined('VNP_APPLICATION') ) die('Access denied!');

class Form
{
	private $FormName = '';
	private $FormAction = '';
	private $FormMethod = '';
	private $FormElements = array();
	static	$CompiledPath = '';
	static	$BasePath = __DIR__;
	static	$ArrayVar = '';
	private	$Rebuild = false;
	static	$TemplateDir = __DIR__;

	public	$button, $fieldset, $input, $textarea, $html, $isindex, $label, $legend, $select, $multi_select, $radio, $checkbox, $file, $image, $custom, $list_select;

	public function __construct($FormName, $Rebuild = false) {
		self::$TemplateDir = rtrim(Form::$BasePath, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . 'field_template' . DIRECTORY_SEPARATOR;
		$this->Rebuild = $Rebuild;
		$this->FormName = $FormName;
		Form::$BasePath = dirname(realpath(__FILE__)) . DIRECTORY_SEPARATOR;
	}

	public function SetArrayVar($Name = '') {
		Form::$ArrayVar = $Name;
	}

	public function TemplateDir($Path) {
		self::$TemplateDir = $Path;
	}

	public function button($Name) {
		return $this->button = new FormButtonElement($Name);
	}
	public function fieldset($Name) {
		return $this->fieldset	= new FormFieldsetElement($Name);
	}
	public function input($Name) {
		return $this->input = new FormInputElement($Name);
	}
	public function textarea($Name) {
		return $this->textarea = new FormTextareaElement($Name);
	}
	public function html($Name) {
		return $this->html = new FormHtmlElement($Name);
	}
	public function isindex($Name) {
		return $this->isindex = new FormIsindexElement($Name);
	}
	public function label($Name) {
		return $this->label = new FormLabelElement($Name);
	}
	public function legend($Name) {
		return $this->legend = new FormLegendElement($Name);
	}
	public function select($Name) {
		return $this->select = new FormSelectElement($Name);
	}
	public function multi_select($Name) {
		return $this->multi_select = new FormMultiSelectElement($Name);
	}
	public function radio($Name) {
		return $this->radio = new FormRadioElement($Name);
	}
	public function checkbox($Name) {
		return $this->checkbox	= new FormCheckboxElement($Name);
	}
	public function file($Name) {
		return $this->file	= new FormFileElement($Name);
	}
	public function image($Name) {
		return $this->image	= new FormImageElement($Name);
	}
	public function multi_image($Name) {
		return $this->image	= new FormMultiImageElement($Name);
	}
	public function attributes($Name) {
		return $this->image	= new FormAttributesElement($Name);
	}
	public function tags($Name) {
		return $this->input = new FormTagsElement($Name);
	}
	public function custom($Name) {
		return $this->custom = new FormCustomElement($Name);
	}
	public function SetFormName($FormName = '') {
		$this->FormName = $FormName;
		return $this;
	}
    public function list_select($Name) {
        return $this->list_select	= new FormListSelectElement($Name);
    }

	public function Action($FormAction = '') {
		$this->FormAction = $FormAction;
		return $this;
	}

	public function Method($FormMethod = 'GET') {
		$this->FormMethod = $FormMethod;
		return $this;
	}

	public function AddFormElement($FormElementObject) {
		$this->FormElements[] = $FormElementObject;
	}

	public function Render($Return = true, $onlyCreate = false) {
		$Fields = array();
		$RenderedForm = '';
		$i = 0;
		foreach($this->FormElements as $ElementName => $Element) {
			$i++;
			//$Vars['var' . $i] = $Element->Element;
			$Fields[$Element->Element['Name']] = $Element->Element;
		}
		if(file_exists(Form::$CompiledPath . $this->FormName . '.php') && !$this->Rebuild) {
			//include(Form::$CompiledPath . $this->FormName . '.php');
		}
		else {
			$i = 0;
			foreach($this->FormElements as $ElementName => $Element) {
				$i++;
				/*$RenderedForm .= '<?php $Field = $Vars[\'var' . $i . '\']; ?>' . PHP_EOL;*/
				$RenderedForm .= $Element->FieldContent();
			}
			file_put_contents(Form::$CompiledPath . $this->FormName . '.php', $RenderedForm);
		}
		if($Return) {
			/*
			$FormStr = '';
			ob_start();
			include(Form::$CompiledPath . $this->FormName . '.php');
			$FormStr = ob_get_clean();
			return $FormStr;
			*/
			return File::GetContent(Form::$CompiledPath . $this->FormName . '.php');
		}
        elseif($onlyCreate) {
            return '';
        }
		else include(Form::$CompiledPath . $this->FormName . '.php');
	}
}

class FormBaseElement {
	public $Element = array('ElementType' => '',
							'Name' => '',
							'value' => '',
							'Type' => '',
							'Class' => '',
							'Style' => '',
							'Content' => '',
							'Label'	=> '',
							'Prompt' => '',
							'Cols' => '',
							'Rows' => '',
							'Required' => false,
							'Multiple' => false,
							'Options' => array(),
							'StaticOptions' => false);
    public $ExtraVars = array();
	public function __construct($Name) {
		$this->Element['Name'] = $Name;
		return $this;
	}
	public function ElementType($Name, $Type) {
		$this->Element['Name'] = $Name;
		$this->Element['ElementType'] = $Type;
		return $this;
	}
	public function Name($Name) {
		$this->Element['Name'] = $Name;
		return $this;
	}
	public function Value($Value = '') {
		$this->Element['value'] = $Value;
		return $this;
	}
	protected function FieldType($Type, $ValidTypes, $DefaultType) {
		if(!in_array(strtolower($Type), $ValidTypes)) $Type = $DefaultType;
		$this->Element['Type'] = $Type;
		return $this;
	}
	public function Content($Value = '') {
		$this->Element['Content'] = $Value;
		return $this;
	}
	public function Label($Value = '') {
		$this->Element['Label'] = $Value;
		return $this;
	}
	public function Required($Value = true) {
		$this->Element['Required'] = $Value;
		return $this;
	}
	public function FieldClass($Value = '') {
		$this->Element['Class'] = $Value;
		return $this;
	}
	public function Style($Value = '') {
		$this->Element['Style'] = $Value;
		return $this;
	}
	public function Prompt($Value = '') {
		$this->Element['Prompt'] = $Value;
		return $this;
	}
	public function Cols($Value = '') {
		$this->Element['Cols'] = $Value;
		return $this;
	}
	public function Rows($Value = '') {
		$this->Element['Rows'] = $Value;
		return $this;
	}
	public function Multiple($Value = true) {
		$this->Element['Multiple'] = $Value;
		return $this;
	}
	public function Options($Options = array()) {
		$this->Element['Options'] += $Options;
		return $this;
	}
	public function StaticOptions($S = false) {
		$this->Element['StaticOptions'] += $S;
		return $this;
	}
	public function ResetOptions() {
		$this->Element['Options'] = array();
		return $this;
	}
	public function DeleteOptions($Name) {
		unset($this->Element['Options'][$Name]);
		return $this;
	}
    public function AddVar($name, $value = '') {
        if(is_array($name)) $this->ExtraVars += $name;
        else $this->ExtraVars[$name] = $value;
        return $this;
    }
	public function FieldOutput() {
		$FieldContent = file_get_contents(Form::$TemplateDir . $this->Element['ElementType'] . '.php');
		if($this->Element['Required']) {
			$this->Element['Class'] .= ' RequiredField';
			$this->Element['Label'] .= '<span class="RequireField">*</span>';
		}
		if(!empty(Form::$ArrayVar)) $FieldName = Form::$ArrayVar . '[' . $this->Element['Name'] . ']';
		else $FieldName = $this->Element['Name'];
		$FieldContent = str_replace('[@@FieldName@@]', $FieldName, $FieldContent);
		$FieldContent = str_replace('[@@FieldLabel@@]', $this->Element['Label'], $FieldContent);
		$FieldContent = str_replace('[@@FieldClass@@]', $this->Element['Class'], $FieldContent);
		$FieldContent = str_replace('[@@FieldStyle@@]', $this->Element['Style'], $FieldContent);
		return $FieldContent;
	}
	public function FieldContent() {
		$FieldContent = $this->FieldOutput();
		return $FieldContent . PHP_EOL;
	}
}

class FormButtonElement extends FormBaseElement {
	public function __construct($Name) {$this->ElementType($Name, 'button');}
	public function Type($Type) {
		$ValidTypes = array('button', 'submit', 'reset');
		$this->FieldType($Type, $ValidTypes, 'button');
		return $this;
	}
	public function FieldContent() {
		$FieldContent = $this->FieldOutput();
		$FieldContent = str_replace('[@@FieldValue@@]', '<?php echo $Fields[\'' . $this->Element['Name'] . '\'][\'value\'] ?>', $FieldContent);
		return $FieldContent . PHP_EOL;
	}
}
class FormInputElement extends FormBaseElement {
	public function __construct($Name) {$this->ElementType($Name, 'input');}
	public function Type($Type) {
		$ValidTypes = array('button','checkbox','file','hidden','image','password','radio','reset','submit','text','number');
		$this->FieldType($Type, $ValidTypes, 'text');
		return $this;
	}
	public function FieldContent() {
		$FieldContent = $this->FieldOutput();
		$FieldContent = str_replace('[@@FieldValue@@]', '<?php echo $Fields[\'' . $this->Element['Name'] . '\'][\'value\'] ?>', $FieldContent);
		$FieldContent = str_replace('[@@FieldType@@]', $this->Element['Type'], $FieldContent);
		return $FieldContent . PHP_EOL;
	}
}
class FormTextareaElement extends FormBaseElement {
	public function __construct($Name) {$this->ElementType($Name, 'textarea');}
	public function FieldContent() {
		$FieldContent = $this->FieldOutput();
		$FieldContent = str_replace('[@@FieldID@@]', '<?php echo \'Field_' . $this->Element['Name'] . '\' ?>', $FieldContent);
		$FieldContent = str_replace('[@@FieldValue@@]', '<?php echo $Fields[\'' . $this->Element['Name'] . '\'][\'value\'] ?>', $FieldContent);
		return $FieldContent . PHP_EOL;
	}
}
class FormHtmlElement extends FormBaseElement {
	public function __construct($Name) {$this->ElementType($Name, 'html');}
	public function FieldContent() {
		$FieldContent = $this->FieldOutput();
		$FieldContent = str_replace('[@@FieldRealName@@]', $this->Element['Name'], $FieldContent);
		$FieldContent = str_replace('[@@FieldID@@]', '<?php echo \'Field_' . $this->Element['Name'] . '\' ?>', $FieldContent);
		$FieldContent = str_replace('[@@FieldValue@@]', '<?php echo $Fields[\'' . $this->Element['Name'] . '\'][\'value\'] ?>', $FieldContent);
		return $FieldContent . PHP_EOL;
	}
}
class FormFieldsetElement extends FormBaseElement {
	public function __construct($Name) {$this->ElementType($Name, 'fieldset');return $this;}
	public function FieldContent() {
		$FieldContent = $this->FieldOutput();
		$FieldContent = str_replace('[@@FieldContent@@]', $this->Element['Content'], $FieldContent);
		return $FieldContent . PHP_EOL;
	}
}
class FormIsindexElement extends FormBaseElement {
	public function __construct($Name) {$this->ElementType($Name, 'insindex');return $this;}
}
class FormLabelElement extends FormBaseElement {
	public function __construct($Name) {$this->ElementType($Name, 'label');return $this;}
}
class FormLegendElement extends FormBaseElement {
	public function __construct($Name) {$this->ElementType($Name, 'legend');return $this;}
}
class FormSelectElement extends FormBaseElement {
	public function __construct($Name) {$this->ElementType($Name, 'select');return $this;}
	public function FieldContent() {
		$FieldContent = $this->FieldOutput();
		if(!$this->Element['StaticOptions']) {
			$options = '<?php foreach($Fields[\'' . $this->Element['Name'] . '\'][\'Options\'] as $Options):?>
			<option value="<?php echo $Options[\'value\'] ?>"<?php if($Options[\'value\'] == $Fields[\'' . $this->Element['Name'] . '\'][\'value\']): ?> selected="selected"<?php endif ?>><?php echo $Options[\'prefix\'] . $Options[\'text\'] ?></option>
			<?php endforeach ?>';
		}
		else {
			$options = array();
			foreach($this->Element['Options'] as $Opt) {
				$options[] = '<option value="' . $Opt['value'] . '"<?php if(\'' . $Opt['value'] . '\' == $Fields[\'' . $this->Element['Name'] . '\'][\'value\']): ?> selected="selected"<?php endif ?>>' . $Opt['text'] . '</option>';
			}
			$options = implode(PHP_EOL, $options);
		}

		$FieldContent = str_replace('[@@FieldOptions@@]', $options, $FieldContent);
		$FieldContent = str_replace('[@@FieldMultiple@@]', $this->Element['Multiple'] ? 'multiple' : '', $FieldContent);
		return $FieldContent . PHP_EOL;
	}
}
class FormMultiSelectElement extends FormBaseElement {
	public function __construct($Name) {$this->ElementType($Name, 'multi_select');return $this;}
	public function FieldContent() {
		$FieldContent = $this->FieldOutput();
		if(!$this->Element['StaticOptions']) {
			$options = '<?php $ArrValue = explode(\',\',$Fields[\'' . $this->Element['Name'] . '\'][\'value\']); foreach($Fields[\'' . $this->Element['Name'] . '\'][\'Options\'] as $Options):?>
			<option value="<?php echo $Options[\'value\'] ?>"<?php if(in_array($Options[\'value\'],$ArrValue)): ?> selected="selected"<?php endif ?>><?php echo $Options[\'prefix\'] . $Options[\'text\'] ?></option>
			<?php endforeach ?>';
		}
		else {
			$options = array();
			foreach($this->Element['Options'] as $Opt) {
				$options[] = '<option value="' . $Opt['value'] . '"<?php if(in_array(\'' . $Opt['value'] . '\',$ArrValue)): ?> selected="selected"<?php endif ?>>' . $Opt['text'] . '</option>';
			}
			$options = '<?php $ArrValue = explode(\',\',$Fields[\'' . $this->Element['Name'] . '\'][\'value\']); ?>' . implode(PHP_EOL, $options);
		}

		$FieldContent = str_replace('[@@FieldOptions@@]', $options, $FieldContent);
		$FieldContent = str_replace('[@@FieldMultiple@@]', $this->Element['Multiple'] ? 'multiple' : '', $FieldContent);
		return $FieldContent . PHP_EOL;
	}
}
class FormRadioElement extends FormBaseElement {
	public function __construct($Name) {$this->ElementType($Name, 'radio');return $this;}
	public function FieldContent() {
		$FieldContent = $this->FieldOutput();
		if(!$this->Element['StaticOptions']) {
			$options = '
			<div class="VNP_RadioCheckOpts">
			<?php foreach($Fields[\'' . $this->Element['Name'] . '\'][\'Options\'] as $Options):?>
			<div class="radio radio-success">
				<input type="radio" name="Field[' . $this->Element['Name'] . ']" id="ID_Field_' . $this->Element['Name'] . '_<?php echo $Options[\'value\'] ?>" class="' . $this->Element['Name'] . '" value="<?php echo $Options[\'value\'] ?>"<?php if($Options[\'value\'] == $Fields[\'' . $this->Element['Name'] . '\'][\'value\']): ?> checked="checked"<?php endif ?>/>
				<?php echo $Options[\'prefix\'] ?><label for="ID_Field_' . $this->Element['Name'] . '_<?php echo $Options[\'value\'] ?>">
					<?php echo $Options[\'text\'] ?>
				</label>
			</div>
			<?php endforeach ?>
			</div>';
		}
		else {
			$options = array();
			$options[] = '<div class="VNP_RadioCheckOpts">';
			foreach($this->Element['Options'] as $Opt) {
				$options[] = '
				<div class="radio radio-success">
					<input type="radio" name="Field[' . $this->Element['Name'] . ']" id="ID_' . $this->Element['Name'] . '_' . $Opt['value'] . '" class="' . $this->Element['Class'] . '" value="' . $Opt['value'] . '"<?php if(\'' . $Opt['value'] . '\' == $Fields[\'' . $this->Element['Name'] . '\'][\'value\']): ?> checked="checked"<?php endif ?>/>
					<label for="ID_' . $this->Element['Name'] . '_' . $Opt['value'] . '">
						' . $Opt['text'] . '
					</label>
				</div>';
			}
			$options[] = '</div>';
			$options = implode(PHP_EOL, $options);
		}

		$FieldContent = str_replace('[@@FieldOptions@@]', $options, $FieldContent);
		return $FieldContent . PHP_EOL;
	}
}
class FormCheckboxElement extends FormBaseElement {
	public function __construct($Name) {$this->ElementType($Name, 'checkbox');return $this;}
	public function FieldContent() {
		$FieldContent = $this->FieldOutput();
		if(!$this->Element['StaticOptions']) {
			$options = '
			<div class="VNP_CheckBoxOpts">
			<?php $ArrValue = explode(\',\',$Fields[\'' . $this->Element['Name'] . '\'][\'value\']); foreach($Fields[\'' . $this->Element['Name'] . '\'][\'Options\'] as $Options):?>
			<div class="checkbox checkbox-success">
				<input type="checkbox" name="Field[' . $this->Element['Name'] . '][]" id="ID_Field_' . $this->Element['Name'] . '_<?php echo $Options[\'value\'] ?>" class="Fields_' . $this->Element['Name'] . '" value="<?php echo $Options[\'value\'] ?>"<?php if(in_array($Options[\'value\'],$ArrValue)): ?> checked="checked"<?php endif ?>/>
				<?php echo $Options[\'prefix\'] ?><label for="ID_Field_' . $this->Element['Name'] . '_<?php echo $Options[\'value\'] ?>">
					<?php echo $Options[\'text\'] ?>
				</label>
			</div>
			<?php endforeach ?>
			</div>';
		}
		else {
			$options = array();
			$options[] = '<div class="VNP_CheckBoxOpts">';
			foreach($this->Element['Options'] as $Opt) {
				$options[] = '
				<div class="checkbox checkbox-success">
					<input type="checkbox" name="Field[' . $this->Element['Name'] . '][]" id="ID_' . $this->Element['Name'] . '_' . $Opt['value'] . '" class="' . $this->Element['Class'] . '" value="' . $Opt['value'] . '"<?php if(in_array(\'' . $Opt['value'] . '\', $ArrValue)): ?> checked="checked"<?php endif ?>/>
					<label for="ID_' . $this->Element['Name'] . '_' . $Opt['value'] . '">
					' . $Opt['text'] . '</label>
				</div>';
			}
			$options[] = '</div>';
			$options = '<?php $ArrValue = explode(\',\',$Fields[\'' . $this->Element['Name'] . '\'][\'value\']); ?>' . implode(PHP_EOL, $options);
		}

		$FieldContent = str_replace('[@@FieldOptions@@]', $options, $FieldContent);
		return $FieldContent . PHP_EOL;
	}
}
class FormFileElement extends FormBaseElement {
	public function __construct($Name) {$this->ElementType($Name, 'file'); return $this;}
}
class FormImageElement extends FormBaseElement {
	public function __construct($Name) {$this->ElementType($Name, 'image'); return $this;}
	public function FieldContent() {
		$FieldContent = $this->FieldOutput();
		$FieldContent = str_replace('[@@FieldValue@@]', '<?php echo $Fields[\'' . $this->Element['Name'] . '\'][\'value\'] ?>', $FieldContent);
		return $FieldContent . PHP_EOL;
	}
}
class FormMultiImageElement extends FormBaseElement {
	public function __construct($Name) {$this->ElementType($Name, 'multi_image'); return $this;}
	public function FieldContent() {
		$FieldContent = $this->FieldOutput();
		$FieldContent = str_replace('[@@FieldRealName@@]', $this->Element['Name'], $FieldContent);
		$FieldContent = str_replace('[@@FieldValue@@]', '$Fields[\'' . $this->Element['Name'] . '\'][\'value\']', $FieldContent);
		return $FieldContent . PHP_EOL;
	}
}
class FormAttributesElement extends FormBaseElement {
	public function __construct($Name) {$this->ElementType($Name, 'attributes'); return $this;}
	public function FieldContent() {
		$FieldContent = $this->FieldOutput();
		$FieldContent = str_replace('[@@FieldRealName@@]', $this->Element['Name'], $FieldContent);
		$FieldContent = str_replace('[@@FieldValue@@]', '$Fields[\'' . $this->Element['Name'] . '\'][\'value\']', $FieldContent);
		return $FieldContent . PHP_EOL;
	}
}
class FormTagsElement extends FormBaseElement {
	public function __construct($Name) {$this->ElementType($Name, 'tags'); return $this;}
	public function FieldContent() {
		$FieldContent = $this->FieldOutput();
		$FieldContent = str_replace('[@@FieldRealName@@]', $this->Element['Name'], $FieldContent);
		$FieldContent = str_replace('[@@FieldValue@@]', '<?php echo $Fields[\'' . $this->Element['Name'] . '\'][\'value\'] ?>', $FieldContent);
		$FieldContent = str_replace('[@@FieldType@@]', $this->Element['Type'], $FieldContent);
		return $FieldContent . PHP_EOL;
	}
}

class FormCustomElement extends FormBaseElement {
	public function FieldContent() {
		$FieldContent = $this->Element['Content'];
		return $FieldContent . PHP_EOL;
	}
}

class FormListSelectElement extends FormBaseElement {
    public function __construct($Name) {$this->ElementType($Name, 'list_select'); return $this;}
    public function FieldContent() {
        $FieldContent = $this->FieldOutput();
        $FieldContent = str_replace('[@@FieldRealName@@]', $this->Element['Name'], $FieldContent);
        $FieldContent = str_replace('[@@FieldValue@@]', '$Fields[\'' . $this->Element['Name'] . '\'][\'value\']', $FieldContent);
        $FieldContent = str_replace('[@@RefTable@@]', '\'' . $this->ExtraVars['ref_table'] . '\'', $FieldContent);
        $FieldContent = str_replace('[@@RefField@@]', '\'' . $this->ExtraVars['ref_field'] . '\'', $FieldContent);
        return $FieldContent . PHP_EOL;
    }
}

?>