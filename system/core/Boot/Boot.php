<?php

/**
 * Boot up Class
 *
 * Boot up an application, detect running environment
 *
 * @package		VNP
 * @subpackage	Base libraries
 * @author		VNP Dev team
 * @category	Base layer
 * @link		http://vnphp.com/docs/base-layer/libraries/Boot.html
 */

//namespace System;
if( !defined('VNP_SYSTEM') && !defined('VNP_APPLICATION') ) die('Access denied!');

class G
{
    static $WorkingSession;
    static $ValidWorkingSession = Boot::GUEST_SESSION;
    static $Loader;
    static $Session;
    static $Authorized;
    static $Theme;
    static $RouteObj;
    static $Route;
    static $Config;
    static $Error;
    static $Registry;
    static $MyData;
    static $Controller;
    static $Action;
    static $Params;
    static $Global = array();
    static $Profile = array();
    static $UriGetParams = array();
}

class Boot {
    //System message//
    const ERROR_CONTROLLER_NOT_FOUND	= 'Error: Controller not found!';
    const ADMIN_SESSION		= '_AdminSession';
    const SMOD_SESSION		= '_SModSession';
    const MOD_SESSION		= '_ModSession';
    const MEMBER_SESSION	= '_MemberSession';
    const GUEST_SESSION		= '_GuestSession';
    const WORKING_SESSION	= '_WorkingSession';

    static $App = array();
    static $Config = array();
    static $ControllerGroup = '';
    static $site_extensions = array();
    static $tpl_output = '';

    /**
     * Application initializer
     *
     * @return no
     */
    static function ApplicationConfig($AppConfig = array())
    {
        G::$Config = $AppConfig;
    }

    static function RequirePermision($Session) {
        G::$ValidWorkingSession = $Session;
    }

    static function ControllerGroup($Group = '') {
        if($Group != '') self::$ControllerGroup = $Group . DIRECTORY_SEPARATOR;
    }

    static function Start() {
        require SYSTEM_PATH . 'core/Access/Access.php';
        require SYSTEM_PATH . 'core/Error/Error.php';
        require SYSTEM_PATH . 'core/Output/Output.php';
        require SYSTEM_PATH . 'core/Helper/Helper.php';
        require SYSTEM_PATH . 'core/Filter/Filter.php';
        require SYSTEM_PATH . 'core/Request/Request.php';
        require SYSTEM_PATH . 'core/Router/Router.php';
        require SYSTEM_PATH . 'core/Template/Template.php';
        require SYSTEM_PATH . 'core/Session/Session.php';
        require SYSTEM_PATH . 'core/Authorized/Authorized.php';
        require SYSTEM_PATH . 'core/Theme/Theme.php';
        require SYSTEM_PATH . 'core/Crypt/Crypt.php';
        require SYSTEM_PATH . 'core/Form/Form.php';
        require SYSTEM_PATH . 'core/Input/Input.php';
        require SYSTEM_PATH . 'core/Option/Option.php';
        require SYSTEM_PATH . 'core/Block/Block.php';
        require SYSTEM_PATH . 'core/Extension/Extension.php';
        require SYSTEM_PATH . 'core/DB/DB.php';
        require SYSTEM_PATH . 'core/File/File.php';
        require SYSTEM_PATH . 'core/Node/Node.php';
        require SYSTEM_PATH . 'core/Hook/Hook.php';
        require SYSTEM_PATH . 'core/Lang/Lang.php';
        require SYSTEM_PATH . 'core/DB/' . G::$Config['DB']['main']['type'] . 'DBWrapper.php';

        ini_set("mysql.trace_mode", "0");

        DB::$Config		= G::$Config['DB']['main'];
        G::$Error		= new Error();
        G::$RouteObj	= Router::Start(rtrim(BASE_DIR, '/'), CACHE_PATH . 'router' . DIRECTORY_SEPARATOR);
        G::$Session		= new Session();
        G::$Authorized	= new Authorized();
        Theme::Config(array(
            'theme_root'		=> DATA_PATH . 'theme' . DIRECTORY_SEPARATOR,
            'default_theme'		=> 'vnp',
            'default_layout'	=> 'left.body'
        ));
        Theme::$BlockBasePath = BLOCK_BASE_PATH;
        TPL::Config(	BASE_DIR,
            TEMPLATE_PATH,
            CACHE_PATH . 'compiled' . DIRECTORY_SEPARATOR,
            CACHE_PATH . 'html' . DIRECTORY_SEPARATOR,
            CACHE_PATH . 'compiled' . DIRECTORY_SEPARATOR
        );

        /*** Load extensions & hooks ***/
        self::$site_extensions = unserialize(Option::get('site_extensions'));
        Hook::init();
        Hook::on('before_boot_init');

        G::$WorkingSession = Boot::GUEST_SESSION;
        G::$Session->Set(Boot::WORKING_SESSION, G::$WorkingSession);
        Form::$CompiledPath = CACHE_PATH . 'form' . DIRECTORY_SEPARATOR;

        Router::Map('LoginAction', '/login', 'User#Login', 'POST');
        Router::Map('LogoutAction', '/logout', 'User#Logout', 'GET|POST');

        /* Ajax working Mapper */
        Router::Map('Ajax_Controller',
            '/ajax/[json|text|state:Ajax_Mod]/[:controller]',
            'ControllerSection', 'GET|POST', 2);
        Router::Map('Ajax_ControllerAction',
            '/ajax/[json|text|state:Ajax_Mod]/[:controller]/[:action]',
            'ControllerSection', 'GET|POST', 1);
        Router::Map('Ajax_ControllerParams',
            '/ajax/[json|text|state:Ajax_Mod]/[:controller]/[:action]/[*:params]',
            'ControllerSection', 'GET|POST', 0);
        /* End ajax working Mapper */
        Router::Map('ThumbnailHandler', '/[i:Width]_[i:Height]/[*:ImageLocation]', 'ThumbnailHandler', 'GET|POST', -1);
        Router::Map('Controller', '/[:controller]', 'ControllerSection', 'GET|POST', 5);
        Router::Map('ControllerAction', '/[:controller]/[:action]', 'ControllerSection', 'GET|POST', 4);
        Router::Map('ControllerParams', '/[:controller]/[:action]/[*:params]', 'ControllerSection', 'GET|POST', 3);
        Router::setRoutePrefixRegExp('(?P<lang>[A-Za-z0-9]{2})');
        Router::setRouteSuffixRegExp('(?P<page>page\\' . Output::$Paging['bridge'] . '[0-9]++)');

        Router::Map('ExtensionRestApi', '/extension/[as:ext_name]/[as:ext_action].ext', 'extension#rest_api', 'GET|POST', 6);
        Router::Map('ExtensionRestApiParams', '/extension/[as:ext_name]/[as:ext_action]/[*:params].ext', 'extension#rest_api', 'GET|POST', 6);
        Hook::on('after_boot_init');
    }

    static function Library($LibrariesUsing)
    {
        $l = explode(',', $LibrariesUsing);
        foreach($l as $_lib) {
            $_libFile1 = SYSTEM_PATH . 'core' . DIRECTORY_SEPARATOR . $_lib . DIRECTORY_SEPARATOR . $_lib . '.php';
            $_libFile2 = SYSTEM_PATH . 'libs' . DIRECTORY_SEPARATOR . $_lib . DIRECTORY_SEPARATOR . $_lib . '.php';
            if(file_exists($_libFile1) && is_readable($_libFile1) ) include_once $_libFile1;
            elseif(file_exists($_libFile2) && is_readable($_libFile2) ) include_once $_libFile2;
            else trigger_error($_lib . ' does not existed!');
        }
    }

    static function LoadRouteProfile() {
        G::$Profile = Router::getCurrentProfile();
        Theme::Config(
            array_merge(
                array(
                    'theme_root'		=> DATA_PATH . 'theme' . DIRECTORY_SEPARATOR,
                    'default_theme'		=> 'vnp',
                    'default_layout'	=> 'left.body'
                ),
                array(
                    'default_theme'		=> G::$Profile['route_theme'],
                    'default_layout'	=> G::$Profile['route_layout']
                )
            )
        );
        define('CURRENT_THEME', G::$Profile['route_theme']);
    }

    static function Dispatcher() {
        G::$Route = Router::Match(RECOMPILE_ROUTE);
        //n(G::$Route);
        G::$UriGetParams = Router::UriGetParams();
        /*** Get global variables from route ***/
        G::$Global += Router::getGlobalVars();
        /*** Set paging parameters ***/
        Output::$Paging['ext'] = Router::$currentRoutePart['ext'];
        Output::$Paging['base_url'] = Router::$currentRoutePart['basePath'] . rtrim(Router::$currentRoutePart['prefix'], '/') . Router::$currentRoutePart['main'];
        if(isset(G::$Global['page'])) {
            $paging = explode(Output::$Paging['bridge'], G::$Global['page']);
            Output::$Paging['current_page'] = G::$Global['page'] = intval($paging[1]);
        }
        /*** End set paging parameters ***/
        /*** Define working languale ***/
        if(isset(G::$Global['lang'])) define('LANG', G::$Global['lang']);
        else define('LANG', G::$Config['lang_default']);
        Lang::init(APPLICATION_PATH . 'lang' . DIRECTORY_SEPARATOR . LANG . '.php');
        /*** End define working language ***/
        if(isset(G::$Route['ajax'])) define('IS_AJAX', G::$Route['ajax']);
        G::$Params = isset(G::$Route['params']['params']) ? Router::ExtractParams(G::$Route['params']['params']) : array();
        if(!empty(G::$Route['target']) && G::$Route['target'] != 'ControllerSection') {
            if(Filter::startsWith('short_route_', G::$Route['name'])) {
                $functions = G::$Route['target'];
                $functions = explode('::', $functions);
                if(isset($functions[1])) {
                    $method_params = explode(':', $functions[1]);
                    if(!isset($method_params[1]))
                        call_user_func_array(array($functions[0], $method_params[0]), array());
                    else {
                        $m_params = array_map('trim', explode(',', $method_params[1]));
                        call_user_func_array(array($functions[0], $method_params[0]), $m_params);
                    }
                }
                else call_user_func($functions[0]);
                die();
            }
            else {
                $Dispatchers = array_map('trim',explode('#', G::$Route['target']));
                if(isset($Dispatchers[0])) G::$Controller = $Dispatchers[0];
                G::$Action = isset($Dispatchers[1]) ? $Dispatchers[1] : 'Main';
                if(isset($Dispatchers[2])) G::$Params = array_merge(Router::ExtractParams($Dispatchers[2], G::$Params));
                G::$Route['target'] = 'ControllerSection';
            }
        }
        else {
            if(isset(G::$Route['params'])) {
                G::$Controller = G::$Route['params']['controller'];
                G::$Action = isset(G::$Route['params']['action']) ? G::$Route['params']['action'] : 'Main';
            }
        }
    }

    static function Run() {
        Hook::on('before_app_init');
        call_user_func(array(APPLICATION_NAME, 'init'));
        Hook::on('after_app_init');
        self::Dispatcher();
        self::LoadRouteProfile();
        if(G::$Route['name'] == 'LoginAction') {
            $check = G::$Authorized->CheckLogin(Input::Post('username'), Input::Post('password'), true);
            if(G::$Authorized->IsLogged()) header('LOCATION:' . BASE_DIR);
        }
        if(G::$Route['name'] == 'LogoutAction') {
            G::$Authorized->Logout();
            header('LOCATION:' . BASE_DIR);
        }

        if(isset($_SESSION['UserInfo'])) {
            G::$MyData = $_SESSION['UserInfo'];
            Access::CheckUserPermission();
            define('USER_ID', G::$MyData['userid']);
        }
        elseif(G::$ValidWorkingSession == Boot::GUEST_SESSION) {
            define('USER_ID', 0);
        }
        else {
            //if(!G::$User->IsLogged() || G::$WorkingSession !=  Boot::ADMIN_SESSION) {
            if(!G::$Authorized->IsLogged()) {
                TPL::File('login')
                    ->SetDir('TPLFileDir', TEMPLATE_PATH)
                    ->SetDir('CompiledDir', CACHE_PATH . 'compiled' . DIRECTORY_SEPARATOR)
                    ->SetDir('CacheDir', CACHE_PATH . 'html' . DIRECTORY_SEPARATOR)
                    ->Output(false);
                exit();
            }
            else {
                G::$MyData = $_SESSION['UserInfo'];
                Access::CheckUserPermission();
                define('USER_ID', G::$MyData['userid']);
            }
        }
        Boot::ControllerAction();
        Theme::$AjaxSession = G::$Registry['AjaxRequest'];
        if(!defined('IS_AJAX')) {
            Theme::getStaticBlocks();
            Theme::getThemeAreas();
        }
        Hook::on('before_template_output');
        self::$tpl_output = Theme::Output();
        Hook::on('before_template_return');
        echo self::$tpl_output;
        Hook::on('after_template_return');
    }

    static function ControllerAction() {
        require SYSTEM_PATH . 'core/Controller/Controller.php';
        require CONTROLLER_PATH . 'app_controller.php';
        $GroupCtlFile = '';
        if(!empty(self::$ControllerGroup) && file_exists(CONTROLLER_PATH . self::$ControllerGroup . rtrim(self::$ControllerGroup, DIRECTORY_SEPARATOR) . '.php')) {
            $GroupCtlFile = rtrim(self::$ControllerGroup, DIRECTORY_SEPARATOR);
            include CONTROLLER_PATH . self::$ControllerGroup . $GroupCtlFile . '.php';
            Hook::on('before_controller_group_init');
            $GroupCtlFile::Init();
            Hook::on('after_controller_group_init');
        }

        if(empty(G::$Route) || G::$Route['target'] == 'ControllerSection') {
            //if(isset(G::$Route['ajax'])) define('IS_AJAX', G::$Route['ajax']);

            $ControllerFile = '';
            if(file_exists(CONTROLLER_PATH . G::$Controller . DIRECTORY_SEPARATOR . G::$Controller . '.php'))
                $ControllerFile = CONTROLLER_PATH . G::$Controller . DIRECTORY_SEPARATOR . G::$Controller . '.php';
            elseif(file_exists(CONTROLLER_PATH . self::$ControllerGroup . G::$Controller . DIRECTORY_SEPARATOR . G::$Controller . '.php'))
                $ControllerFile = CONTROLLER_PATH . self::$ControllerGroup . G::$Controller . DIRECTORY_SEPARATOR . G::$Controller . '.php';
            if($ControllerFile != '') {
                define('CURRENT_CONTROLLER_PATH', dirname(G::$Controller) . DIRECTORY_SEPARATOR);
                define('CONTROLLER_NAME', G::$Controller);
                require $ControllerFile;
                Access::CheckUserPermission();
                G::$Registry['AjaxRequest']			= isset(G::$Route['ajax']) ? G::$Route['ajax'] : false;
                G::$Registry['Params']				= G::$Params;
                G::$Registry['ControllerAction']	= G::$Action;
                G::$Registry['ControllerPath']		= dirname($ControllerFile) . DIRECTORY_SEPARATOR;
                G::$Registry['Controller']			= new G::$Controller;
                /*** Hook ***/
                Hook::on('before_controller_init');
                /*** Hook ***/
                G::$Registry['Controller']->ControllerInitializer($ControllerFile);
                if(method_exists(G::$Registry['Controller'], 'Construct')) G::$Registry['Controller']->Construct();
                /*** Hook ***/
                Hook::on('before_action');
                /*** Hook ***/
                $Action = G::$Action;
                G::$Registry['Controller']->$Action();
                G::$Registry['Controller']->ControllerHookAfter();
                if(!empty($GroupCtlFile)) {
                    $GroupCtlFile::Complete();
                }
                /*** Hook ***/
                Hook::on('after_action');
                /*** Hook ***/
            }
            else {
                //trigger_error(Boot::ERROR_CONTROLLER_NOT_FOUND);
                Output::ErrorPage(404, 'Đường dẫn bạn truy cập không tồn tại');
            }
        }
    }
}

?>