<?php

function n($str)
{
	echo '<pre>';
	echo '<code class="VNP_CodeBlock">';
	print_r($str);
	echo '</code>';
	echo '</pre>';
}

function p($data)
{
	$str = '<pre><code class="VNP_CodeBlock">';
	$str .= print_r($data, true);
	$str .= '</code></pre>';
	Theme::$Hook['before_body'] .= $str;
}

function lang() {
    $func_args = func_get_args();
    return call_user_func_array(array('Lang', 'get_string'), $func_args);
   // return Lang::get_string($func_args);
}

function getMonth() {
    $now   = new DateTime();
    return(int)$now->format("m");
}
function getYear() {
    return date("Y");
}
function phpDisabledFunction($functionName) {
    $disabled = explode(',', ini_get('disable_functions'));
    return !in_array($functionName, $disabled);
}
function getBlocks() {
    $blockPath = APPLICATION_PATH . 'blocks' . DIRECTORY_SEPARATOR . '*';
    $listBlocks = array_filter(glob($blockPath), 'is_dir');
    $blocksData = array();
    foreach($listBlocks as $block) {
        $blockName = basename($block);
        $blockInfo = Input::parseInitFile($block . DIRECTORY_SEPARATOR . $blockName . '.ini', true);
        $blocksData[basename($block)] = $blockInfo;
    }
    return $blocksData;
}

function VNP_AdminLogPanel($time, $mem) {
    if(!isset(G::$MyData['level']) || G::$MyData['level'] <= 2) return '';
    ?>

    <div id="bottom-admin-bar">
        <?php $DBLog = DB::Log() ?>
        <?php if(!defined('ADMIN_AREA') || ADMIN_AREA == false):?>
        <div id="top-design-blocks" style="display: <?php echo (G::$Session->Get('designMode', 0) == 1) ? 'block' : 'none' ?>">
            <?php if(G::$Session->Get('designMode', 0) == 1): $blocks = getBlocks() ?>
            <ul class="list-blocks">
                <?php foreach($blocks as $blFile => $block):?>
                <li class="vnp-ready-block" data-block="<?php echo $blFile ?>"><?php echo $block['info']['name'] ?></li>
                <?php endforeach ?>
            </ul>
            <?php endif ?>
        </div>
        <?php endif ?>
        <?php if($DBLog['total_query'] > 0): ?>
            <pre id="VNP_DBQueryStatistics" style="display:none">
                <?php foreach($DBLog['query'] as $q):
                    if($q['status'] == 1) $Class = array('label' => 'success', 'icon' => 'ok');
                    else $Class = array('label' => 'danger', 'icon' => 'ban') ?>
                    <div class="label label-<?php echo $Class['label']?>">
                        <span class="glyphicon glyphicon-<?php echo $Class['icon']?>-circle"></span>&nbsp;<?php echo $q['sql'] . ' - ' . $q['error'] ?>
                    </div>
                <?php endforeach ?>
                <script type="text/javascript">
                    function VNP_ToggleDBQueryLog() {
                        var list = document.getElementById("VNP_DBQueryStatistics");
                        if (list.style.display == "none") {
                            list.style.display = "block";
                            list.style.opacity = 1;
                        }
                        else {
                            list.style.display = "none";
                            list.style.opacity = 0;
                        }
                        return false;
                    }
                </script>
            </pre>
        <?php endif ?>
        <div class="VNP_SysStat">
            <?php if(!defined('ADMIN_AREA') || ADMIN_AREA == false):?>
            <form class="switchModForm" id="designFormActions" action="<?php echo Router::Generate('ControllerAction', array('controller' => 'nTheme', 'action' => 'enableDesign'))?>" method="post">
                <input type="hidden" name="currentState" id="currentState" value="<?php echo Router::GenerateThisRoute() ?>" />
                <input type="submit" name="removePageDesign" id="removePageDesign" value="Remove page design" />
                <input type="submit" name="saveChanged" id="saveDesignOptions" value="Save changed" />
                <input type="submit" name="discardChanged" value="Discard changed" />
                <input type="submit" name="switchMod" value="Design on/off" />
            </form>
            <?php endif ?>
            <span class="glyphicon glyphicon-time"></span>&nbsp;Time:&nbsp;<?php echo $time ?> s&nbsp;&nbsp;&nbsp;
            <span class="glyphicon glyphicon-stats"></span>&nbsp;Memory:&nbsp;<?php echo SizeConverter($mem) ?>
            &nbsp;&nbsp;&nbsp;
            <a href="#" onclick="javascript: VNP_ToggleDBQueryLog(); return false;" title="Show/Hide DB Query log">
                <span class="glyphicon glyphicon-share-alt"></span>&nbsp;DB Query:&nbsp;<?php echo $DBLog['total_query'] ?>
            </a>
        </div>
    </div>
<?php }
function SizeConverter($size) {
	$unit=array('b','kb','mb','gb','tb','pb');
	return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}
function getParentID($array,$parent_id,$id){
    if($parent_id>0){
        $parent_ = $array[$parent_id]['parent_id'];
        $id_ = $array[$parent_id]['product_category_id'];
        return getParentID($array,$parent_,$id_);
    }
    else {
        return $array[$id]['product_category_id'];
    }
}
function ShowMenu($menus = array(),$child,$parrent = NULL)
{
    $dmenu = $menus;
    if($child){
        echo '<ol class="dd-list">';
    }
    foreach ($menus as $key => $val)
    {
        if ($val['parent_id'] == $parrent)
        {
            unset($menus[$key]);
            echo '<li class="dd-item" data-id="'.$val['menus_id'].'"><div class="dd-handle">'. $val['menu_name']. '</div>';
            if(count(Filter::BuildSubItems($dmenu,$val['menus_id'],0,'menus_id','parent_id'))){
                ShowMenu($menus,true,$val['menus_id']);
            }
            else {
                ShowMenu($menus,false,$val['menus_id']);
            }
            echo '<div class="pull-right action-buttons"><a class="blue" href="'.APP_DOMAIN.'/admincp/menus/AddNode/'.$val['menus_id'].'"><i class="ace-icon fa fa-pencil bigger-130"></i></a><a href="#" class="delete-menu" data-id="'.$val['menus_id'].'"><i class="ace-icon fa fa-trash-o bigger-130"></i></a></div></li>';
        }
    }
    if($child) {
        echo '</ol>';
    }
}
function getLocation($name=''){
    $file = APPLICATION_PATH.'data'.DIRECTORY_SEPARATOR.'location'.DIRECTORY_SEPARATOR.$name.'.json';
    $data = file_get_contents($file);
    $data =json_decode($data,true);
    $result = array();
    foreach($data as $item){
        $result[$item[$name.'_id']] = $item;
    }
    return $result;
}
function getNextNumber() {
    $count = (int)file_get_contents($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'clients'.DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.'code'.DIRECTORY_SEPARATOR.'code.log');
    $count+=1;
    file_put_contents($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'clients'.DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.'code'.DIRECTORY_SEPARATOR.'code.log',$count);
    return $count;
}
?>