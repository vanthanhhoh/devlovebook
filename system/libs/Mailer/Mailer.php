<?php
$php_mailer = __DIR__ . DIRECTORY_SEPARATOR . 'PHPMailerAutoload.php';
if(file_exists($php_mailer) && is_readable($php_mailer))
    require $php_mailer;

class Mailer {
    static function sendMail($from = '', $to = array(), $replies = array(), $subject = '', $message = '', $files = '') {
        $mailConfig = array();
        $mailConfig['method'] = strtolower(Option::get('email_method'));
        $mailConfig['smtp_port'] = Option::get('email_smtp_port');
        $mailConfig['smtp_host'] = Option::get('email_smtp_host');
        $mailConfig['smtp_username'] = Option::get('email_smtp_username');
        $mailConfig['smtp_password'] = Option::get('email_smtp_password');
        $mailConfig['smtp_secure'] = strtolower(Option::get('email_smtp_secure'));
        if(empty($from))
            $mailConfig['email_send_from_id'] = strtolower(Option::get('email_send_from_id'));
        else
            $mailConfig['email_send_from_id'] = strtolower($from);
        $mailConfig['email_send_from_name'] = Option::get('email_send_from_name');
        try {
            $mail = new PHPMailer;
            //$mail->SMTPDebug = 2;
            $mail->setLanguage(Option::get('default_lang'), __DIR__ . 'language' . DIRECTORY_SEPARATOR);
            $mail->CharSet = 'UTF-8';

            if($mailConfig['method'] == 'smtp') {
                $mail->isSMTP();
                $mail->SMTPAuth = true;
                $mail->Port = $mailConfig['smtp_port'];
                $mail->Host = $mailConfig['smtp_host'];
                $mail->Username = $mailConfig['smtp_username'];
                $mail->Password = $mailConfig['smtp_password'];

                switch($mailConfig['smtp_secure']) {
                    case 'ssl':
                        $mail->SMTPSecure = 'ssl';
                        break;
                    case 'tls':
                        $mail->SMTPSecure = 'tls';
                        break;
                    default:
                        $mail->SMTPSecure = '';
                }
            }
            elseif($mailConfig['method'] == 'sendmail') $mail->IsSendmail();
            elseif(!phpDisabledFunction('mail')) $mail->IsMail();
            else return false;

            $mail->From = $mailConfig['email_send_from_id'];
            $mail->FromName = $mailConfig['email_send_from_name'];

            if(!empty($replies)) {
                foreach($replies as $rep) {
                    if(is_array($rep)) {
                        if(isset($rep[1]))
                            $mail->addReplyTo($rep[0], $rep[1]);
                        else $mail->addReplyTo($rep[0]);
                    }
                    else $mail->addReplyTo($rep);
                }
            }

            if(empty($to)) return false;

            if(!is_array($to)) $to = array($to);

            foreach( $to as $_to )
                $mail->addAddress($_to);

            $mail->Subject = strip_tags($subject);
            $mail->WordWrap = 120;
            $mail->Body = $message;
            $mail->AltBody = strip_tags($message);
            $mail->IsHTML(true);

            if(!empty($files)) {
                $files = array_map( 'trim', explode( ',', $files ) );
                foreach($files as $file)
                    $mail->addAttachment($file);
            }

            if(!$mail->Send()) {
                Error::Set($mail->ErrorInfo, 1);
                return false;
            }
            return true;
        }
        catch(phpmailerException $e) {
            Error::Set($e->errorMessage(), 1);
            return false;
        }
    }
}