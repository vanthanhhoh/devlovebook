<?php

if( !defined('VNP_SYSTEM') && !defined('VNP_APPLICATION') ) die('Access denied!');

//spl_autoload_register('NBC_ControllerAutoloader');

function NBC_ControllerAutoloader($ClassName)
{
    $filePath = CONTROLLER_PATH . $ClassName . DIRECTORY_SEPARATOR . $ClassName . '.php';
    if(file_exists($filePath) && is_readable($filePath))
        include $filePath;
	
	//throw new Exception('SystemBaseAutoloader: Unable to load ' . $ClassName . ' class!');
}

if(version_compare(PHP_VERSION, '5.1.2', '>=')) {
    //SPL autoloading was introduced in PHP 5.1.2
    if (version_compare(PHP_VERSION, '5.3.0', '>=')) {
        spl_autoload_register('NBC_ControllerAutoloader', true, true);
    } else {
        spl_autoload_register('NBC_ControllerAutoloader');
    }
}
else {
    /**
     * Fall back to traditional autoload for old PHP versions
     * @param string $classname The name of the class to load
     */
    function __autoload($classname)
    {
        NBC_ControllerAutoloader($classname);
    }
}

?>