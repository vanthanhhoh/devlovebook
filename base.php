<?php

if( !defined('VNP_APPLICATION') && !defined('APPLICATION_NAME') ) die('Application not found!');

define('VNP_SYSTEM', true);
define('CURRENT_TIME', time());

$base_siteurl = pathinfo( $_SERVER['PHP_SELF'], PATHINFO_DIRNAME );
if( $base_siteurl == DIRECTORY_SEPARATOR ) $base_siteurl = '';
if( ! empty( $base_siteurl ) ) $base_siteurl = str_replace( DIRECTORY_SEPARATOR, '/', $base_siteurl );
if( ! empty( $base_siteurl ) ) $base_siteurl = preg_replace( "/[\/]+$/", '', $base_siteurl );
if( ! empty( $base_siteurl ) ) $base_siteurl = preg_replace( "/^[\/]*(.*)$/", '/\\1', $base_siteurl );
$base_siteurl .= '/';

define('BASE_DIR', $base_siteurl);
define('APP_DIR', $app_dir = str_replace_last(INIT_DIR, '', $base_siteurl));
define('CONTROLLER_DIR', 'controller');
if(!defined('GLOBAL_BASE_URL')) define('GLOBAL_BASE_URL', APP_DIR);
define('APPLICATION_BASE', GLOBAL_BASE_URL . APPLICATION_NAME);
if(!defined('GLOBAL_DATA_DIR')) define('GLOBAL_DATA_DIR', APP_DIR . 'data/');
define('GLOBAL_DATA_PATH', BASE_PATH . 'data' . DIRECTORY_SEPARATOR);

define('SYSTEM_PATH', BASE_PATH . 'system' . DIRECTORY_SEPARATOR);
define('DATA_PATH', APPLICATION_PATH . 'data' . DIRECTORY_SEPARATOR);
define('CONTROLLER_PATH', APPLICATION_PATH . CONTROLLER_DIR . DIRECTORY_SEPARATOR);
if(!defined('ADMIN_SECTION')) define('ADMIN_SECTION', false);
define('ROUTER_EXTRA_KEY', md5('vnp_extra_key_' . microtime()));
define('NODE_BUILDER_CACHE_EXT', '_node_builder.txt');

if(ENVIRONMENT == 'develop') {
	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);
}

if(file_exists(SYSTEM_PATH . 'functions.php'))
	require SYSTEM_PATH . 'functions.php';
else die('Main functions file doesn\'t existed!');
if(file_exists(SYSTEM_PATH . 'core/Boot/Boot.php'))
	require SYSTEM_PATH . 'core/Boot/Boot.php';
else die('Bootstrap load failed!');
if(file_exists(APPLICATION_PATH . APPLICATION_NAME . '.php'))
	require APPLICATION_PATH . APPLICATION_NAME . '.php';
else die('Application main file doesn\'t existed!');
if(file_exists(APPLICATION_PATH . 'config.php'))
	require APPLICATION_PATH . 'config.php';
else die('Application config file doesn\'t existed!');

if(file_exists(SYSTEM_PATH . 'autoload.php'))
	require SYSTEM_PATH . 'autoload.php';

define('UPLOAD_PATH', DATA_PATH . 'uploads' . DIRECTORY_SEPARATOR . getYear() . '-' . getMonth() . DIRECTORY_SEPARATOR);
define('UPLOAD_BASE', APPLICATION_DATA_DIR . 'uploads/' . getYear() . '-' . getMonth() . '/');

Boot::ApplicationConfig($AppConfig);

if(!defined('STATIC_FILE_SERVER')) define('STATIC_FILE_SERVER', BASE_DIR);

function str_replace_last($search, $replace, $subject) {
    $pos = strrpos($subject, $search);
    if($pos !== false)
        $subject = substr_replace($subject, $replace, $pos, strlen($search));
    return $subject;
}

?>